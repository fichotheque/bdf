/* BdfServer_Email - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.email;

import fr.exemole.bdfserver.api.exportation.table.TableExportParameters;
import net.fichotheque.corpus.Corpus;


/**
 *
 * @author Vincent Calame
 */
public class TableExportEmail {

    private TableExportParameters tableExportParameters;
    private Corpus corpus;
    private String extension;
    private String charset;

    public TableExportEmail(TableExportParameters tableExportParameters, Corpus corpus, String extension, String charset) {
        this.tableExportParameters = tableExportParameters;
        this.corpus = corpus;
        this.extension = extension;
        this.charset = charset;
    }

    public TableExportParameters getTableExportParameters() {
        return tableExportParameters;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public String getExtension() {
        return extension;
    }

    public String getCharset() {
        return charset;
    }

}
