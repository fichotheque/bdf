/* BdfServer_Email - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.email;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class SendReport {

    public final static short OK_STATE = 1;
    public final static short EMAILBUILD_ERROR_STATE = -1;
    public final static short SEND_ERROR_STATE = -2;
    private final short state;
    private final List<String> attachmentList = new ArrayList<String>();
    private final String errorMessage;


    private SendReport(short state) {
        this.state = state;
        this.errorMessage = null;
    }

    private SendReport(short state, String errorMessage) {
        this.state = state;
        this.errorMessage = errorMessage;
    }

    public static SendReport okReport() {
        return new SendReport(OK_STATE);
    }

    public static SendReport sendErrorReport(String errorMessage) {
        return new SendReport(SEND_ERROR_STATE, errorMessage);
    }

    public static SendReport emailBuildErrorReport(String errorMessage) {
        return new SendReport(EMAILBUILD_ERROR_STATE, errorMessage);
    }

    public short getState() {
        return state;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void addAttachment(String fileName) {
        attachmentList.add(fileName);
    }

    public List<String> getAttachmentList() {
        return attachmentList;
    }

}
