/* BdfServer_Email - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.email;

import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class WrongAddress {

    private final String errorValue;
    private final Message errorMessage;

    private WrongAddress(String errorValue, String errorMessageKey, Object... values) {
        this.errorValue = errorValue;
        this.errorMessage = LocalisationUtils.toMessage(errorMessageKey, values);
    }

    public static WrongAddress redacteurNoAdressError(String errorValue) {
        return new WrongAddress(errorValue, "_ error.unknown.redacteuraddress");
    }

    public static WrongAddress redacteurUnknownError(String errorValue) {
        return new WrongAddress(errorValue, "_ error.unknown.redacteur", errorValue);
    }

    public static WrongAddress malformedAddressError(String errorValue) {
        return new WrongAddress(errorValue, "_ error.wrong.address");
    }

    public String getErrorValue() {
        return errorValue;
    }

    public Message getErrorMessage() {
        return errorMessage;
    }

}
