/* BdfServer_Email - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.email.tools;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.email.EmailBuffer;
import fr.exemole.bdfserver.email.SendEngine;
import fr.exemole.bdfserver.email.ValidAddress;
import fr.exemole.bdfserver.tools.instruction.DefaultBdfParameters;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.metadata.FieldKey;


/**
 *
 * @author Vincent Calame
 */
public final class FicheSendTools {

    private FicheSendTools() {
    }

    public static void sendFiche(BdfServer bdfServer, BdfUser expediteur, FicheMeta ficheMeta, FieldKey courrielKey, String subject, String message) {
        BdfParameters expediteurParameters = new DefaultBdfParameters(bdfServer, expediteur);
        EmailBuffer emailBuffer = EmailBuffer.buildForFiche(bdfServer, expediteur, ficheMeta)
                .setSubject(subject)
                .setWithRedacteurBcc(true)
                .setMessage(message);
        if (courrielKey != null) {
            Object obj = ficheMeta.getFicheAPI(false).getValue(courrielKey);
            if (obj instanceof Courriel) {
                emailBuffer.addValidAddress(EmailBuffer.TO_FIELD, new ValidAddress(((Courriel) obj).getEmailCore()));
            } else if (obj instanceof FicheItems) {
                FicheItems ficheItems = (FicheItems) obj;
                int count = ficheItems.size();
                for (int i = 0; i < count; i++) {
                    FicheItem ficheItem = ficheItems.get(i);
                    if (ficheItem instanceof Courriel) {
                        emailBuffer.addValidAddress(EmailBuffer.TO_FIELD, new ValidAddress(((Courriel) ficheItem).getEmailCore()));
                    }
                }
            }
        }
        if (emailBuffer.isWithoutField(EmailBuffer.TO_FIELD)) {
            emailBuffer.addValidAddress(EmailBuffer.TO_FIELD, new ValidAddress(emailBuffer.getFromEmail()));
            emailBuffer.setWithRedacteurBcc(false);
        }
        SendEngine sendEngine = SendEngine.build(expediteurParameters);
        sendEngine.sendEmail(emailBuffer);
    }

}
