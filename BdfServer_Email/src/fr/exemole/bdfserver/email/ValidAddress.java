/* BdfServer_Email - Copyright (c) 2011-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.email;

import net.mapeadores.util.models.EmailCore;


/**
 *
 * @author Vincent Calame
 */
public class ValidAddress {

    private final EmailCore emailCore;
    private final String orginalString;

    public ValidAddress(EmailCore emailCore) {
        this.emailCore = emailCore;
        this.orginalString = emailCore.toString();
    }

    public ValidAddress(EmailCore emailCore, String originalString) {
        this.emailCore = emailCore;
        this.orginalString = originalString;
    }

    public EmailCore getEmailCore() {
        return emailCore;
    }

    public String getOrginalString() {
        return orginalString;
    }

}
