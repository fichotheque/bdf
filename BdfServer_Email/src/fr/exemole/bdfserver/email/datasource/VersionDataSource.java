/* BdfServer_Email - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.email.datasource;

import fr.exemole.bdfserver.api.BdfServer;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.activation.DataSource;
import net.fichotheque.addenda.Version;
import net.mapeadores.util.mimetype.MimeTypeResolver;
import net.mapeadores.util.mimetype.MimeTypeUtils;


/**
 *
 * @author Vincent Calame
 */
public class VersionDataSource implements DataSource {

    private final Version version;
    private final String versionName;
    private final String contentType;

    private VersionDataSource(String versionName, Version version, String contentType) {
        this.version = version;
        this.contentType = contentType;
        this.versionName = versionName;
    }

    @Override
    public String getContentType() {
        return contentType;
    }

    @Override
    public String getName() {
        return versionName;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return version.getInputStream();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
        throw new IOException("no editable source");
    }

    public static VersionDataSource newInstance(BdfServer bdfServer, Version version) {
        String versionName = version.getFileName();
        MimeTypeResolver mimeTypeResolver = bdfServer.getMimeTypeResolver();
        String contentType = MimeTypeUtils.getMimeType(mimeTypeResolver, versionName);
        return new VersionDataSource(versionName, version, contentType);
    }

}
