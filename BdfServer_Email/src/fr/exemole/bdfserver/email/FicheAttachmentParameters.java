/* BdfServer_Email - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.email;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import net.fichotheque.addenda.Version;
import net.fichotheque.utils.AddendaUtils;


/**
 *
 * @author Vincent Calame
 */
public class FicheAttachmentParameters {

    private final Map<String, Version> versionMap = new LinkedHashMap<String, Version>();
    private boolean withHtml;
    private boolean withOdt;
    private String htmlTemplateName = null;
    private String odtTemplateName = null;


    public FicheAttachmentParameters() {
    }

    public boolean isWithHtml() {
        return withHtml;
    }

    public FicheAttachmentParameters setWithHtml(boolean withHtml) {
        this.withHtml = withHtml;
        return this;
    }

    public String getHtmlTemplateName() {
        return htmlTemplateName;
    }

    public FicheAttachmentParameters setHtmlTemplateName(String htmlTemplateName) {
        this.htmlTemplateName = htmlTemplateName;
        return this;
    }

    public boolean isWithOdt() {
        return withOdt;
    }

    public FicheAttachmentParameters setWithOdt(boolean withOdt) {
        this.withOdt = withOdt;
        return this;
    }

    public String getOdtTemplateName() {
        return odtTemplateName;
    }

    public FicheAttachmentParameters setOdtTemplateName(String odtTemplateName) {
        this.odtTemplateName = odtTemplateName;
        return this;
    }

    public boolean containsVersion(String versionKey) {
        return versionMap.containsKey(versionKey);
    }

    public boolean containsVersion(Version version) {
        return versionMap.containsKey(AddendaUtils.toVersionKey(version));
    }

    public FicheAttachmentParameters addVersion(Version version) {
        versionMap.put(AddendaUtils.toVersionKey(version), version);
        return this;
    }

    public Collection<Version> getVersionCollection() {
        return versionMap.values();
    }

}
