/* BdfServer_HtmlProducers - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusHtmlUtils;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class HistoryHtmlProducer extends BdfServerHtmlProducer {

    private final Subset subset;
    private final int id;

    public HistoryHtmlProducer(BdfParameters bdfParameters, Subset subset, int id) {
        super(bdfParameters);
        this.subset = subset;
        this.id = id;
        addThemeCss("history.css");
        addJsLib(MiscJsLibs.HISTORY);
    }

    @Override
    public void printHtml() {
        SubsetKey subsetKey = subset.getSubsetKey();
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("subsetKey", subsetKey.getKeyString())
                .put("subsetCategory", subsetKey.getCategoryString())
                .put("subsetName", subsetKey.getSubsetName())
                .put("id", id);
        start();
        this
                .SCRIPT()
                .__jsObject("History.ARGS", args)
                ._SCRIPT()
                .__(printHeader())
                .DIV(HA.id(clientId))
                ._DIV();
        end();
    }

    private boolean printHeader() {
        SubsetItem subsetItem = subset.getSubsetItemById(id);
        if (subsetItem != null) {
            if (subsetItem instanceof FicheMeta) {
                CorpusHtmlUtils.printFicheHeader(this, (FicheMeta) subsetItem, workingLang, formatLocale);
                return true;
            } else {
                return false;
            }
        } else if (subset instanceof Corpus) {
            Corpus corpus = (Corpus) subset;
            String title = FichothequeUtils.getNumberPhrase(corpus, id, FichothequeConstants.FICHE_PHRASE, workingLang, formatLocale, null);
            SubsetItem masterItem = null;
            Subset masterSubset = corpus.getMasterSubset();
            if (masterSubset != null) {
                masterItem = masterSubset.getSubsetItemById(id);
            }
            CorpusHtmlUtils.printFicheHeader(this, title, true, masterItem, workingLang, formatLocale);
            return true;
        } else {
            return false;
        }
    }

}
