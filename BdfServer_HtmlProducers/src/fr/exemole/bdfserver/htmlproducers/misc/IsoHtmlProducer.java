/* BdfServer_HtmlProducers - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.api.interaction.domains.MiscDomain;
import fr.exemole.bdfserver.api.managers.L10nManager;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.SubsetIcon;
import java.util.Set;
import java.util.function.Consumer;
import net.mapeadores.util.html.HtmlPrinter;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class IsoHtmlProducer extends BdfServerHtmlProducer {

    private Set<String> keySet;
    private final String titleMessageKey;
    private final Consumer<HtmlPrinter> icon;

    public IsoHtmlProducer(BdfParameters bdfParameters, String pageName) {
        super(bdfParameters);
        L10nManager l10nManager = bdfServer.getL10nManager();
        if (pageName.equals(MiscDomain.ISO_LANGUAGES_PAGE)) {
            keySet = l10nManager.getCodeCatalog().getLangCodeSet();
            titleMessageKey = "_ title.misc.lang_list";
            icon = SubsetIcon.COUNTRY;
        } else if (pageName.equals(MiscDomain.ISO_COUNTRIES_PAGE)) {
            keySet = l10nManager.getCodeCatalog().getCountryCodeSet();
            titleMessageKey = "_ title.misc.country_list";
            icon = SubsetIcon.LANG;
        } else {
            titleMessageKey = "";
            icon = null;
        }
        addThemeCss("misc.css");
    }

    @Override
    public void printHtml() {
        startLoc(titleMessageKey);
        this
                .__(PageUnit.start(titleMessageKey).sectionCss("unit-Unit misc-iso-Unit").icon(icon))
                .TABLE("misc-iso-Table");
        for (String key : keySet) {
            this
                    .TR()
                    .TD()
                    .__escape(key)
                    ._TD()
                    .TD()
                    .__localize(key)
                    ._TD()
                    ._TR();
        }
        this
                ._TABLE()
                .__(PageUnit.END);
        end();
    }

}
