/* BdfServer_HtmlProducers - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.subsettree.GroupNode;
import fr.exemole.bdfserver.api.subsettree.SubsetNode;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.tools.subsettree.TreeUtils;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlConstants;


/**
 *
 * @author Vincent Calame
 */
public class DiagramsHtmlProducer extends BdfServerHtmlProducer {

    private final SubsetTree corpusTree;

    public DiagramsHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        this.corpusTree = bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_CORPUS);
        addThemeCss("diagram.css", "misc.css");
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        if (corpusTree.getNodeList().isEmpty()) {
            end();
            return;
        }
        this
                .__(PageUnit.start("action-Diagrams", "_ title.misc.diagrams"))
                .__(printGlobalDiagram());
        for (SubsetTree.Node node : corpusTree.getNodeList()) {
            if (node instanceof SubsetNode) {
                SubsetNode subsetNode = (SubsetNode) node;
                Corpus corpus = (Corpus) fichotheque.getSubset(subsetNode.getSubsetKey());
                this
                        .__(printDiagram(corpus));
            } else if (node instanceof GroupNode) {
                this
                        .__(printGroupNodeForState((GroupNode) node));
            }
        }
        this
                .__(PageUnit.END);
        end();
    }

    private boolean printGroupNodeForState(GroupNode groupNode) {
        this
                .P("diagram-GroupTitle")
                .__escape(TreeUtils.getTitle(bdfServer, groupNode, workingLang))
                ._P()
                .DIV("diagram-GroupBlock");
        for (SubsetTree.Node node : groupNode.getSubnodeList()) {
            if (node instanceof SubsetNode) {
                SubsetNode subsetNode = (SubsetNode) node;
                Corpus corpus = (Corpus) fichotheque.getSubset(subsetNode.getSubsetKey());
                this
                        .__(printDiagram(corpus));
            } else if (node instanceof GroupNode) {
                this
                        .__(printGroupNodeForState((GroupNode) node));
            }
        }
        this
                ._DIV();
        return true;
    }


    private boolean printDiagram(Corpus corpus) {
        String src = "diagrams/" + corpus.getSubsetKeyString();
        this
                .DIV("diagram-Corpus")
                .DIV("diagram-Diagram")
                .IMG(HA.src(src + ".png"))
                ._DIV()
                .P("diagram-Links")
                .A(HA.href(src + ".png").target(HtmlConstants.BLANK_TARGET))
                .__escape("png")
                ._A()
                .__dash()
                .A(HA.href(src + ".svg").target(HtmlConstants.BLANK_TARGET))
                .__escape("svg")
                ._A()
                .__dash()
                .A(HA.href(src + ".puml").target(HtmlConstants.BLANK_TARGET))
                .__escape("puml")
                ._A()
                ._P()
                ._DIV();
        return true;
    }

    private boolean printGlobalDiagram() {
        String src = "diagrams/fichotheque";
        HtmlAttributes ha = HA.href("").target(HtmlConstants.BLANK_TARGET);
        this
                .P()
                .__localize("_ label.misc.globaldiagram")
                .__colon()
                .A(ha.href(src + ".png").target(HtmlConstants.BLANK_TARGET))
                .__escape("png")
                ._A()
                .__dash()
                .A(ha.href(src + ".svg").target(HtmlConstants.BLANK_TARGET))
                .__escape("svg")
                ._A()
                .__dash()
                .A(ha.href(src + ".puml").target(HtmlConstants.BLANK_TARGET))
                .__escape("puml")
                ._A()
                ._P();
        return true;
    }

}
