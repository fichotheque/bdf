/* BdfServer_HtmlProducers - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.utils.Comparators;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.buildinfo.BuildInfo;


/**
 *
 * @author Vincent Calame
 */
public class GlobalStatsHtmlProducer extends BdfServerHtmlProducer {

    public GlobalStatsHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addThemeCss("misc.css");
    }

    @Override
    public void printHtml() {
        start();
        this
                .__(PageUnit.start("action-Stats", "_ title.misc.globalstats"))
                .__(printSoftwareVersion())
                .__(printMemoryUsage())
                .__(printSubsetTable(SubsetKey.CATEGORY_CORPUS))
                .__(printSubsetTable(SubsetKey.CATEGORY_THESAURUS))
                .__(printSubsetTable(SubsetKey.CATEGORY_SPHERE))
                .__(printSubsetTable(SubsetKey.CATEGORY_ALBUM))
                .__(printSubsetTable(SubsetKey.CATEGORY_ADDENDA))
                .__(PageUnit.END);
        end();
    }

    private boolean printSoftwareVersion() {
        this
                .P()
                .__localize("_ label.misc.softwareversion")
                .__colon();
        BuildInfo buildInfo = bdfServer.getBuildInfo();
        if (buildInfo != null) {
            this
                    .__escape(buildInfo.getVersion());
        }
        this
                ._P();
        return true;
    }

    private boolean printMemoryUsage() {
        Runtime run = Runtime.getRuntime();
        this
                .P()
                .__localize("_ label.misc.memoryusage")
                .__colon()
                .__append((int) Math.floor(((double) run.freeMemory()) / (1024 * 1024)))
                .__escape('/')
                .__append((int) Math.ceil(((double) run.totalMemory()) / (1024 * 1024)))
                .__escape('/')
                .__append((int) Math.ceil(((double) run.maxMemory()) / (1024 * 1024)))
                ._P();
        return true;
    }

    private boolean printSubsetTable(short subsetCategory) {
        List<Subset> subsetList = FichothequeUtils.getSubsetList(fichotheque, subsetCategory);
        subsetList.sort(Comparators.SIZE);
        this
                .H2()
                .__localize(BdfHtmlUtils.getSubsetCollectionLocKey(subsetCategory))
                .__colon()
                .__escape(bdfUser.format(subsetList.size()))
                ._H2();
        if (!subsetList.isEmpty()) {
            this
                    .TABLE("misc-stats-Table");
            for (Subset subset : subsetList) {
                this
                        .TR()
                        .TD()
                        .__escape(subset.getSubsetKeyString())
                        ._TD()
                        .TD()
                        .__escape(FichothequeUtils.getTitle(subset, workingLang))
                        ._TD()
                        .TD("misc-stats-Number")
                        .__append(subset.size())
                        ._TD()
                        ._TR();
            }
            this
                    ._TABLE();
        }
        return true;
    }

}
