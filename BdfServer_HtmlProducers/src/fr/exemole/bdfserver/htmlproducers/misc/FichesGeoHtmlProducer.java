/* BdfServer_HtmlProducers - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.GeoJsLibs;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class FichesGeoHtmlProducer extends BdfServerHtmlProducer {

    public FichesGeoHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addThemeCss("misc.css");
        addJsLib(GeoJsLibs.FICHES);
    }

    @Override
    public void printHtml() {
        String mapId = generateId();
        JsObject piocheArgs = JsObject.init()
                .put("mapId", mapId)
                .put("geojsonUrl", "misc?json=fichesgeo");
        startLoc("_ title.misc.fichesgeo");
        this
                .SCRIPT()
                .__jsObject("Geo.Fiches.ARGS", piocheArgs)
                ._SCRIPT();
        this
                .DIV("misc-fichesgeo-Page")
                .__(PageUnit.start("action-Geo", "_ title.misc.fichesgeo"))
                .DIV(HA.id(mapId).classes("misc-fichesgeo-Map"))
                ._DIV()
                .__(PageUnit.END)
                ._DIV();
        end();
    }

}
