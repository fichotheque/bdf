/* BdfServer_HtmlProducers - Copyright (c) 2014-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.MiscDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class MementoHtmlProducer extends BdfServerHtmlProducer {

    public MementoHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(MiscJsLibs.MEMENTO);
        addThemeCss("memento.css");
        setMainStorageKey(Domains.MISC, MiscDomain.MEMENTO_PAGE);
    }

    @Override
    public void printHtml() {
        start();
        end();
    }

}
