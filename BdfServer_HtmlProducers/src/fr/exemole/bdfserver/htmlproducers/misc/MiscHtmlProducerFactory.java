/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.MiscDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.PermissionChecker;
import fr.exemole.bdfserver.tools.instruction.RequestHandler;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class MiscHtmlProducerFactory {

    private MiscHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        switch (page) {
            case MiscDomain.BLANK_PAGE: {
                return new BlankHtmlProducer(parameters, getBodyCssClass(requestHandler));
            }
            case MiscDomain.DIAGRAMS_PAGE: {
                parameters.checkFichothequeAdmin();
                return new DiagramsHtmlProducer(parameters);
            }
            case MiscDomain.GLOBALSTATS_PAGE: {
                parameters.checkFichothequeAdmin();
                return new GlobalStatsHtmlProducer(parameters);
            }
            case MiscDomain.HISTORY_PAGE: {
                Subset subset;
                if (requestHandler.hasParameter(SubsetKey.CATEGORY_CORPUS_STRING)) {
                    subset = requestHandler.getMandatoryCorpus();
                } else {
                    subset = requestHandler.getMandatorySubset();
                }
                int id = requestHandler.getMandatoryId();
                PermissionChecker.init(parameters).checkHistory(subset, id);
                return new HistoryHtmlProducer(parameters, subset, id);
            }
            case MiscDomain.ISO_COUNTRIES_PAGE:
            case MiscDomain.ISO_LANGUAGES_PAGE: {
                return new IsoHtmlProducer(parameters, page);
            }
            case MiscDomain.FICHESGEO_PAGE: {
                return new FichesGeoHtmlProducer(parameters);
            }
            case MiscDomain.MEMENTO_PAGE: {
                return new MementoHtmlProducer(parameters);
            }
            case MiscDomain.TABLEEXPORTFORM_PAGE: {
                return new TableExportFormHtmlProducer(parameters);
            }
            default:
                return null;
        }
    }

    private static String getBodyCssClass(RequestHandler requestHandler) throws ErrorMessageException {
        String backgroundType = requestHandler.getTrimedParameter(InteractionConstants.BACKGROUND_PARAMNAME);
        switch (backgroundType) {
            case "listframe":
                return "global-body-ListFrame";
            case "toolwindow":
                return "global-body-ToolWindow";
            case "transparent":
                return "global-body-Transparent";
            case "topborder":
                return "global-body-TopBorder";
            case "white":
                return "global-body-White";
            default:
                return "global-body-Default";
        }
    }

}
