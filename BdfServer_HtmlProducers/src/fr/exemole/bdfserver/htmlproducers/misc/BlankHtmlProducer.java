/* BdfServer_HtmlProducers - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.misc;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class BlankHtmlProducer extends BdfServerHtmlProducer {

    public BlankHtmlProducer(BdfParameters bdfParameters, String bodyCssClass) {
        super(bdfParameters);
        setBodyCssClass(bodyCssClass);
    }

    @Override
    public void printHtml() {
        start();
        end();
    }

}
