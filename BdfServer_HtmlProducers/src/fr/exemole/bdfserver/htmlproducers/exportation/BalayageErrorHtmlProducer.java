/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.ExportationDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import net.fichotheque.exportation.balayage.BalayageDef;
import net.mapeadores.util.html.HtmlPrinterUtils;
import net.mapeadores.util.logging.CommandMessage;


/**
 *
 * @author Vincent Calame
 */
public class BalayageErrorHtmlProducer extends BdfServerHtmlProducer implements ExportationDomain {

    private final BalayageDef balayageDef;

    public BalayageErrorHtmlProducer(BdfParameters bdfParameters, BalayageDef balayageDef) {
        super(bdfParameters);
        this.balayageDef = balayageDef;
    }

    @Override
    public void printHtml() {
        boolean initError = false;
        CommandMessage commandMessage = getCommandMessage();
        if ((commandMessage != null) && (!commandMessage.getMessageKey().equals("_ error.existing.balayagesession"))) {
            initError = true;
        }
        start();
        if (initError) {
            this
                    .__(PageUnit.SIMPLE, () -> {
                        this
                                .H1()
                                .__localize("_ title.exportation.balayageerror", balayageDef.getName())
                                ._H1();
                        HtmlPrinterUtils.printCommandMessage(this, commandMessage, "global-DoneMessage", "global-ErrorMessage");
                    });

        } else {
            printCommandMessageUnit();
        }
        end();
    }

}
