/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ExportationDomain;
import fr.exemole.bdfserver.commands.exportation.BalayageRunCommand;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfHref;
import java.util.List;
import net.fichotheque.exportation.balayage.BalayageDef;
import net.fichotheque.exportation.balayage.BalayageMode;
import net.fichotheque.utils.BalayageUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class ExportationHtmlUtils {

    private ExportationHtmlUtils() {
    }

    public static boolean printBalayageLinks(HtmlPrinter hp, BalayageDef balayageDef, String target) {
        HtmlAttributes aAttr = HA.href("").target(target);
        String balayageName = balayageDef.getName();
        hp
                .A(aAttr.href(balayageHref(balayageName)))
                .__escape(balayageDef.getName())
                ._A();
        List<BalayageMode> balayageModeList = BalayageUtils.getBalayageModeList(balayageDef);
        if (!balayageModeList.isEmpty()) {
            hp
                    .__space()
                    .SMALL()
                    .__escape('[');
            boolean next = false;
            for (BalayageMode balayageMode : balayageModeList) {
                String modeName = balayageMode.getName();
                if (next) {
                    hp
                            .__escape(" | ");
                } else {
                    next = true;
                }
                hp
                        .A(aAttr.href(balayageHref(balayageName).param(BalayageRunCommand.MODE_PARAMNAME, modeName)))
                        .__escape(modeName)
                        ._A();
            }
            hp
                    .__escape(']')
                    ._SMALL();
        }
        return true;
    }

    private static BdfHref balayageHref(String balayageName) {
        return BH.domain(Domains.EXPORTATION).page(ExportationDomain.BALAYAGE_RESULT_PAGE).command(BalayageRunCommand.COMMANDNAME).pageErr(ExportationDomain.BALAYAGE_ERROR_PAGE).balayage(balayageName);
    }

}
