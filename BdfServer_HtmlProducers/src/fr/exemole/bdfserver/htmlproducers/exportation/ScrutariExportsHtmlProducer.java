/* BdfServer_HtmlProducers - Copyright (c) 2014-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.AdminJsLibs;
import net.mapeadores.util.html.HA;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariExportsHtmlProducer extends BdfServerHtmlProducer {

    public ScrutariExportsHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(AdminJsLibs.SCRUTARIEXPORT);
        addThemeCss("pane.css", "memento.css", "scrutariexport.css");
    }

    @Override
    public void printHtml() {
        startLoc("_ link.exportation.scrutariexportadmin", true);
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

    @Override
    protected void setIcons() {
        addIconPng("theme/icons/16x16/admin-scrutariexports.png", "16");
    }


}
