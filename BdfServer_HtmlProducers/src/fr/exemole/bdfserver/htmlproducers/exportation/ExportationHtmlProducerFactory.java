/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.ExportationDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageLog;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class ExportationHtmlProducerFactory {

    private ExportationHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        switch (page) {
            case ExportationDomain.ACCESSES_PAGE: {
                return new AccessesHtmlProducer(parameters);
            }
            case ExportationDomain.BALAYAGE_INDEX_PAGE: {
                parameters.checkFichothequeAdmin();
                return new BalayageIndexHtmlProducer(parameters);
            }
            case ExportationDomain.BALAYAGE_ERROR_PAGE: {
                BalayageDescription balayageDescription = requestHandler.getBalayage();
                return new BalayageErrorHtmlProducer(parameters, balayageDescription.getBalayageDef());
            }
            case ExportationDomain.BALAYAGE_RESULT_PAGE: {
                BalayageDescription balayageDescription = requestHandler.getBalayage();
                BalayageLog balayageLog = (BalayageLog) parameters.getResultObject(BdfInstructionConstants.BALAYAGELOG_OBJ);
                String mode = (String) parameters.getResultObject(BdfInstructionConstants.MODE_OBJ);
                return new BalayageResultHtmlProducer(parameters, balayageDescription.getBalayageDef(), mode, balayageLog);
            }
            case ExportationDomain.TABLEEXPORTS_PAGE: {
                parameters.checkFichothequeAdmin();
                return new TableExportsHtmlProducer(parameters);
            }
            case ExportationDomain.SCRUTARIEXPORTS_PAGE: {
                parameters.checkFichothequeAdmin();
                return new ScrutariExportsHtmlProducer(parameters);
            }
            case ExportationDomain.TRANSFORMATIONS_PAGE: {
                parameters.checkFichothequeAdmin();
                return new TransformationsHtmlProducer(parameters);
            }
            case ExportationDomain.TRANSFORMATION_BINARYUPDATE_PAGE: {
                parameters.checkFichothequeAdmin();
                TemplateContentDescription templateContentDescription = requestHandler.getTemplateContentDescription();
                return new TemplateBinaryUpdateHtmlProducer(parameters, templateContentDescription);
            }
            case ExportationDomain.SQLEXPORTS_PAGE: {
                parameters.checkFichothequeAdmin();
                return new SqlExportsHtmlProducer(parameters);
            }
            case ExportationDomain.BALAYAGES_PAGE: {
                parameters.checkFichothequeAdmin();
                return new BalayagesHtmlProducer(parameters);
            }
            default:
                return null;
        }
    }

}
