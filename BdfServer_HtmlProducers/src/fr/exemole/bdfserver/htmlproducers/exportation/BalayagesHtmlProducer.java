/* BdfServer_HtmlProducers - Copyright (c) 2018-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.AdminJsLibs;
import net.mapeadores.util.html.HA;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class BalayagesHtmlProducer extends BdfServerHtmlProducer {

    public BalayagesHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(AdminJsLibs.BALAYAGE);
        addThemeCss("pane.css", "balayage.css");
    }

    @Override
    public void printHtml() {
        startLoc("_ link.exportation.balayageadmin", true);
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

    @Override
    protected void setIcons() {
        addIconPng("theme/icons/16x16/admin-balayages.png", "16");
    }

}
