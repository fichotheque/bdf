/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.managers.BalayageManager;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.Tree;
import net.fichotheque.exportation.balayage.BalayageDescription;


/**
 *
 * @author Vincent Calame
 */
public class BalayageIndexHtmlProducer extends BdfServerHtmlProducer {

    public BalayageIndexHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
    }

    @Override
    public void printHtml() {
        BalayageManager balayageManager = bdfServer.getBalayageManager();
        start();
        printCommandMessageUnit();
        this
                .__(PageUnit.start("action-Balayages", "_ title.exportation.balayageindex"))
                .__(Tree.TREE, () -> {
                    for (BalayageDescription balayageDescription : balayageManager.getBalayageDescriptionList()) {
                        this
                                .__(Tree.LEAF, () -> {
                                    ExportationHtmlUtils.printBalayageLinks(this, balayageDescription.getBalayageDef(), null);
                                });
                    }
                })
                .__(PageUnit.END);
        end();
    }

}
