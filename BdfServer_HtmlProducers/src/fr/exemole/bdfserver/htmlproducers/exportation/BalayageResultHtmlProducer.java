/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageLog;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import net.fichotheque.exportation.balayage.BalayageDef;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.LocalisationUtils;


/**
 *
 * @author Vincent Calame
 */
public class BalayageResultHtmlProducer extends BdfServerHtmlProducer {

    private final BalayageDef balayageDef;
    private final BalayageLog balayageLog;
    private String mode;

    public BalayageResultHtmlProducer(BdfParameters bdfParameters, BalayageDef balayageDef, String mode, BalayageLog balayageLog) {
        super(bdfParameters);
        this.balayageDef = balayageDef;
        this.balayageLog = balayageLog;
        this.mode = mode;
    }

    @Override
    public void printHtml() {
        String balayageName = balayageDef.getName();
        String title = balayageName;
        if (mode != null) {
            title = title + " [" + mode + "]";
        }
        start();
        printCommandMessageUnit();
        this
                .__(PageUnit.start("action-Balayages", LocalisationUtils.toMessage("_ title.exportation.balayage", title)))
                .P()
                .SMALL()
                .__localize("_ link.exportation.balayageagain")
                .__colon()
                ._SMALL()
                .__(ExportationHtmlUtils.printBalayageLinks(this, balayageDef, null))
                ._P();
        if (balayageLog != null) {
            this
                    .PRE();
            balayageLog.printLog(getPrintWriter());
            this
                    ._PRE();
            if (mode == null) {
                mode = "";
            } else {
                mode = "_" + mode;
            }
            this
                    .P()
                    .A(HA.href(ConfigurationUtils.getLogRelativeUrl("balayage_" + balayageName + mode + ".txt")))
                    .__localize("_ link.exportation.balayagelog")
                    ._A()
                    ._P();
        }
        this
                .__(PageUnit.END);
        end();
    }

}
