/* BdfServer_HtmlProducers - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.exportation;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.ExportationDomain;
import fr.exemole.bdfserver.commands.exportation.TemplateBinaryUpdateCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.mapeadores.util.html.HtmlConstants;
import net.mapeadores.util.request.RequestConstants;


/**
 *
 * @author Vincent Calame
 */
public class TemplateBinaryUpdateHtmlProducer extends BdfServerHtmlProducer {

    private final TemplateContentDescription templateContentDescription;

    public TemplateBinaryUpdateHtmlProducer(BdfParameters bdfParameters, TemplateContentDescription templateContentDescription) {
        super(bdfParameters);
        addThemeCss("transformation.css");
        setBodyCssClass("global-body-Transparent");
        this.templateContentDescription = templateContentDescription;
    }

    @Override
    public void printHtml() {
        start();
        this
                .FORM_post(Domains.EXPORTATION, null, HtmlConstants.MULTIPART_ENCTYPE)
                .INPUT_hidden(RequestConstants.COMMAND_PARAMETER, TemplateBinaryUpdateCommand.COMMANDNAME)
                .INPUT_hidden(RequestConstants.PAGE_PARAMETER, ExportationDomain.TRANSFORMATION_BINARYUPDATE_PAGE)
                .INPUT_hidden(InteractionConstants.TEMPLATE_PARAMNAME, templateContentDescription.getTemplateKey().getKeyString())
                .INPUT_hidden(InteractionConstants.PATH_PARAMNAME, templateContentDescription.getPath())
                .DIV("transformation-iframe-Body")
                .__(printCommandMessageUnit())
                .H3()
                .__localize("_ title.exportation.templatebinaryupdate")
                ._H3()
                .P("transformation-iframe-FileInput")
                .INPUT(name(TemplateBinaryUpdateCommand.FILE_PARAMNAME).type(HtmlConstants.FILE_TYPE).size("45").classes("global-FileInput"))
                ._P()
                .__(Button.COMMAND,
                        Button.submit("action-Save", "_ submit.exportation.templatebinaryupdate"))
                ._DIV()
                ._FORM();
        end();
    }

}
