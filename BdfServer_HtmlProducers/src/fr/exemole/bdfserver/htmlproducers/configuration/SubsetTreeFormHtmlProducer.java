/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.configuration;

import fr.exemole.bdfserver.api.groups.GroupDef;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ConfigurationDomain;
import fr.exemole.bdfserver.api.subsettree.GroupNode;
import fr.exemole.bdfserver.api.subsettree.SubsetNode;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.commands.configuration.SubsetTreeCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.commandbox.Flag;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.subsettree.TreeUtils;
import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class SubsetTreeFormHtmlProducer extends BdfServerHtmlProducer {

    public SubsetTreeFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("configuration.css");
    }

    @Override
    public void printHtml() {
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CONFIGURATION)
                .family("CNF")
                .veil(true)
                .page(ConfigurationDomain.SUBSETTREEFORM_PAGE);

        start();
        printCommandMessageUnit();
        this
                .DIV("configuration-TreeBoxes")
                .__(printGroupDefList())
                .__(printForm(commandBox, SubsetKey.CATEGORY_CORPUS, SubsetTreeCommand.CORPUS_COMMANDKEY, fichotheque.getCorpusList().size()))
                .__(printForm(commandBox, SubsetKey.CATEGORY_THESAURUS, SubsetTreeCommand.THESAURUS_COMMANDKEY, fichotheque.getThesaurusList().size()))
                .__(printForm(commandBox, SubsetKey.CATEGORY_SPHERE, SubsetTreeCommand.SPHERE_COMMANDKEY, fichotheque.getSphereList().size()))
                .__(printForm(commandBox, SubsetKey.CATEGORY_ALBUM, SubsetTreeCommand.ALBUM_COMMANDKEY, fichotheque.getAlbumList().size()))
                .__(printForm(commandBox, SubsetKey.CATEGORY_ADDENDA, SubsetTreeCommand.ADDENDA_COMMANDKEY, fichotheque.getAddendaList().size()))
                ._DIV();
        end();
    }

    private boolean printGroupDefList() {
        List<GroupDef> groupDefList = bdfServer.getGroupManager().getGroupDefList();
        if (groupDefList.isEmpty()) {
            return false;
        }
        this
                .DIV("configuration-GroupList")
                .__(PageUnit.start("_ title.configuration.availablegroups"));
        for (GroupDef groupDef : groupDefList) {
            String title = groupDef.getTitleLabels().seekLabelString(workingLang, null);
            this
                    .CODE("cm-operator")
                    .__escape('+')
                    ._CODE()
                    .__space()
                    .CODE("cm-component-group")
                    .__escape(groupDef.getName())
                    ._CODE();
            if (title != null) {
                this
                        .__space()
                        .CODE("cm-comment")
                        .__escape('(')
                        .__escape(title)
                        .__escape(')')
                        ._CODE();
            }
            this
                    .BR();
        }
        this
                .__(PageUnit.END)
                ._DIV();
        return true;
    }

    private boolean printForm(CommandBox commandBox, short category, String commandKey, int subsetCount) {
        SubsetTree subsetTree = bdfServer.getTreeManager().getSubsetTree(category);
        commandBox = commandBox.derive(SubsetTreeCommand.COMMANDNAME, commandKey)
                .hidden(SubsetTreeCommand.CATEGORY_PARAMNAME, SubsetKey.categoryToString(category))
                .submitLocKey("_ submit.configuration.subsettree")
                .__(Flag.UPDATE_COLLECTIONS);
        if (category == SubsetKey.CATEGORY_CORPUS) {
            commandBox.__(Flag.UPDATE_CORPUS_TREE);
        }
        this
                .__start(commandBox)
                .TEXTAREA(HA.name(SubsetTreeCommand.TREE_PARAMNAME).cols(75).rows(subsetCount + 3).attr("data-codemirror-mode", "subsettree"));
        for (SubsetTree.Node node : subsetTree.getNodeList()) {
            if (node instanceof SubsetNode) {
                appendSubsetNode((SubsetNode) node, 0);
            } else if (node instanceof GroupNode) {
                appendGroupNode((GroupNode) node, 1);
            }
        }
        this
                ._TEXTAREA()
                .__end(commandBox);
        return true;
    }

    private void appendSubsetNode(SubsetNode subsetNode, int indent) {
        for (int i = 0; i < indent; i++) {
            this
                    .__space()
                    .__space();
        }
        SubsetKey subsetKey = subsetNode.getSubsetKey();
        this
                .__escape(subsetKey);
        Subset subset = fichotheque.getSubset(subsetKey);
        if (subset != null) {
            this
                    .__space()
                    .__escape('(')
                    .__escape(FichothequeUtils.getTitle(subset, workingLang))
                    .__escape(')');
        }
        this
                .__newLine();
    }

    private void appendGroupNode(GroupNode groupNode, int level) {
        this
                .__newLine();
        String name = groupNode.getName();
        for (int i = 0; i < (level - 1); i++) {
            this
                    .__space()
                    .__space();
        }
        for (int i = 0; i < level; i++) {
            this
                    .__escape('+');
        }
        this
                .__space()
                .__escape(name)
                .__space()
                .__escape('(')
                .__escape(TreeUtils.getTitle(bdfServer, groupNode, workingLang))
                .__escape(')')
                .__newLine();
        for (SubsetTree.Node subnode : groupNode.getSubnodeList()) {
            if (subnode instanceof SubsetNode) {
                appendSubsetNode((SubsetNode) subnode, level + 1);
            } else if (subnode instanceof GroupNode) {
                appendGroupNode((GroupNode) subnode, level + 1);

            }
        }
        for (int i = 0; i < (level - 1); i++) {
            this
                    .__space()
                    .__space();
        }
        for (int i = 0; i < level; i++) {
            this
                    .__escape('-');
        }
        this
                .__space()
                .__escape(name)
                .__newLine()
                .__newLine();
    }

}
