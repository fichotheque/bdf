/* BdfServer_HtmlProducers - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.configuration;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ConfigurationDomain;
import fr.exemole.bdfserver.commands.configuration.FichothequePhrasesCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.MetadataPhrases;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.apps.AppConf;
import fr.exemole.bdfserver.tools.apps.AppConfUtils;
import fr.exemole.bdfserver.tools.ui.MetadataPhraseDefBuilder;
import fr.exemole.bdfserver.tools.ui.MetadataPhraseDefCatalog;
import java.util.Map;
import net.fichotheque.metadata.FichothequeMetadata;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class PhrasesFormHtmlProducer extends BdfServerHtmlProducer {

    public PhrasesFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
    }

    @Override
    public void printHtml() {
        FichothequeMetadata fichothequeMetadata = bdfServer.getFichotheque().getFichothequeMetadata();
        MetadataPhrases metadataPhrases = MetadataPhrases.init(fichothequeMetadata, bdfServer.getLangConfiguration().getWorkingLangs(), "_ label.configuration.title")
                .addDef(MetadataPhraseDefCatalog.LONG_PHRASEDEF)
                .populateFromAttributes(null)
                .addExtensionPhraseDefList(bdfServer);
        initAppPhrases(metadataPhrases);
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CONFIGURATION)
                .family("CNF")
                .veil(true)
                .name(FichothequePhrasesCommand.COMMANDNAME)
                .lockey(FichothequePhrasesCommand.COMMANDKEY)
                .page(ConfigurationDomain.PHRASESFORM_PAGE)
                .actionCssClass("action-Labels")
                .submitLocKey("_ submit.configuration.fichothequephrases");
        this
                .__start(commandBox)
                .__(metadataPhrases)
                .__end(commandBox);
        end();
    }

    private void initAppPhrases(MetadataPhrases metadataPhrases) {
        Map<String, AppConf> appConfMap = AppConfUtils.getAppConfList(bdfServer.getResourceStorages());
        for (AppConf appConf : appConfMap.values()) {
            if (appConf.getBoolean(AppConf.ACTIVE)) {
                initAppPhrase(metadataPhrases, appConf.getString(AppConf.CORE_TITLEPHRASENAME));
                initAppPhrase(metadataPhrases, appConf.getString(AppConf.LOGIN_TITLEPHRASENAME));
            }
        }
    }

    private void initAppPhrase(MetadataPhrases metadataPhrases, String name) {
        if (name != null) {
            if (!metadataPhrases.containsPhrase(name)) {
                metadataPhrases.addDef(MetadataPhraseDefBuilder.init(name).toMetadataPhraseDef());
            }
        }
    }

}
