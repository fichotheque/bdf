/* BdfServer_HtmlProducers - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.configuration;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.ConfigurationDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class ConfigurationHtmlProducerFactory {

    private ConfigurationHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        String page = parameters.getOutput();
        parameters.checkFichothequeAdmin();
        switch (page) {
            case ConfigurationDomain.COREFORM_PAGE: {
                return new CoreFormHtmlProducer(parameters);
            }
            case ConfigurationDomain.PHRASESFORM_PAGE: {
                return new PhrasesFormHtmlProducer(parameters);
            }
            case ConfigurationDomain.LOGOSFORM_PAGE: {
                return new LogosFormHtmlProducer(parameters);
            }
            case ConfigurationDomain.SUBSETTREEFORM_PAGE: {
                return new SubsetTreeFormHtmlProducer(parameters);
            }
            case ConfigurationDomain.ETCCONFIG_PAGE: {
                return new EtcConfigHtmlProducer(parameters);
            }
            case ConfigurationDomain.EXTENSIONSFORM_PAGE: {
                return new ExtensionsFormHtmlProducer(parameters);
            }
            case ConfigurationDomain.CONFIGADVANCEDCOMMANDS_PAGE: {
                return new ConfigurationAdvancedCommandsHtmlProducer(parameters);
            }
            case ConfigurationDomain.GROUPFORM_PAGE: {
                return new GroupFormHtmlProducer(parameters);
            }
            default:
                return null;
        }
    }

}
