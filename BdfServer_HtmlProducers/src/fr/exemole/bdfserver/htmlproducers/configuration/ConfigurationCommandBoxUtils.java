/* BdfServer_HtmlProducers - Copyright (c) 2014-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.configuration;

import fr.exemole.bdfserver.api.BdfExtensionReference;
import fr.exemole.bdfserver.api.groups.GroupDef;
import fr.exemole.bdfserver.api.managers.ExtensionManager;
import fr.exemole.bdfserver.commands.configuration.CustomIconUploadCommand;
import fr.exemole.bdfserver.commands.configuration.ExtensionActivationCommand;
import fr.exemole.bdfserver.commands.configuration.GroupChangeCommand;
import fr.exemole.bdfserver.commands.configuration.GroupCreationCommand;
import fr.exemole.bdfserver.commands.configuration.GroupRemoveCommand;
import fr.exemole.bdfserver.html.consumers.Choices;
import fr.exemole.bdfserver.html.consumers.Common;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.LangRows;
import fr.exemole.bdfserver.html.consumers.attributes.InputPattern;
import fr.exemole.bdfserver.html.consumers.commandbox.Flag;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.Litteral;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public final class ConfigurationCommandBoxUtils {

    private final static Litteral ICON = new Litteral("icon");
    private final static Litteral TOUCH = new Litteral("apple-touch-icon");
    private final static Litteral SHORTCUT = new Litteral("shortcut icon");

    private ConfigurationCommandBoxUtils() {
    }

    public static boolean printCustomIconUploadBox(HtmlPrinter hp, CommandBox commandBox) {
        commandBox = commandBox.derive(CustomIconUploadCommand.COMMANDNAME, CustomIconUploadCommand.COMMANDKEY)
                .actionCssClass("action-Save")
                .submitLocKey("_ submit.administration.resourceupload");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.fileInputRow("_ label.configuration.customiconfile", hp.name(CustomIconUploadCommand.FILE_PARAMNAME).size("75").classes("global-FileInput")))
                .__(Grid.textInputRow("_ label.configuration.sizes", hp.name(CustomIconUploadCommand.SIZES_PARAMNAME).size("20").populate(InputPattern.SIZES)))
                .__(Grid.choiceSetRow("_ label.configuration.rel",
                        () -> {
                            hp
                                    .__(Grid.radioCell(ICON, hp.name(CustomIconUploadCommand.REL_PARAMNAME).value(CustomIconUploadCommand.ICON_REL_PARAMVALUE).checked(true)))
                                    .__(Grid.radioCell(TOUCH, hp.name(CustomIconUploadCommand.REL_PARAMNAME).value(CustomIconUploadCommand.TOUCH_REL_PARAMVALUE)))
                                    .__(Grid.radioCell(SHORTCUT, hp.name(CustomIconUploadCommand.REL_PARAMNAME).value(CustomIconUploadCommand.SHORTCUT_REL_PARAMVALUE)));

                        }))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printGroupCreationBox(HtmlPrinter hp, CommandBox commandBox, Langs langs) {
        commandBox = commandBox.derive(GroupCreationCommand.COMMANDNAME, GroupCreationCommand.COMMANDKEY)
                .actionCssClass("action-New")
                .submitLocKey("_ submit.configuration.groupcreation");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.configuration.newgroupname", hp.name(GroupCreationCommand.NAME_PARAMNAME).size("30")))
                .__(Grid.END)
                .DL("global-DL")
                .DT()
                .__localize("_ label.configuration.grouptitle")
                ._DT()
                .DD()
                .__(Grid.START)
                .__(LangRows.init(GroupCreationCommand.TITLE_PARAMPREFIX, null, langs).cols(30))
                .__(Grid.END)
                ._DD()
                ._DL()
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.attributes", hp.name(GroupCreationCommand.ATTRIBUTES_PARAMNAME).rows(8).cols(75).attr("data-codemirror-mode", "attributes")))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printGroupChangeBox(HtmlPrinter hp, CommandBox commandBox, GroupDef groupDef, Langs langs) {
        commandBox = commandBox.derive(GroupChangeCommand.COMMANDNAME, GroupChangeCommand.COMMANDKEY)
                .hidden(GroupChangeCommand.NAME_PARAMNAME, groupDef.getName())
                .__(Flag.UPDATE_COLLECTIONS)
                .__(Flag.UPDATE_CORPUS_TREE)
                .submitLocKey("_ submit.configuration.groupchange");
        hp
                .__start(commandBox)
                .DL("global-DL")
                .DT()
                .__localize("_ label.configuration.grouptitle")
                ._DT()
                .DD()
                .__(Grid.START)
                .__(LangRows.init(GroupCreationCommand.TITLE_PARAMPREFIX, groupDef.getTitleLabels(), langs).cols(30))
                .__(Grid.END)
                ._DD()
                ._DL()
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.attributes", hp.name(GroupChangeCommand.ATTRIBUTES_PARAMNAME).rows(8).cols(75).attr("data-codemirror-mode", "attributes"),
                        () -> {
                            hp
                                    .__escape(AttributeParser.toString(groupDef.getAttributes()), true);
                        }))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printGroupRemoveBox(HtmlPrinter hp, CommandBox commandBox, GroupDef groupDef) {
        commandBox = commandBox.derive(GroupRemoveCommand.COMMANDNAME, GroupRemoveCommand.COMMANDKEY)
                .hidden(GroupRemoveCommand.NAME_PARAMNAME, groupDef.getName())
                .actionCssClass("action-Delete")
                .submitLocKey("_ submit.configuration.groupremove");
        hp
                .__start(commandBox)
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printExtensionActivationBox(HtmlPrinter hp, CommandBox commandBox, ExtensionManager extensionManager) {
        commandBox = commandBox.derive(ExtensionActivationCommand.COMMANDNAME, ExtensionActivationCommand.COMMANDKEY)
                .submitLocKey("_ submit.configuration.extensionactivation");
        hp
                .__start(commandBox)
                .__(Choices.SPACED_LIST, () -> {
                    for (BdfExtensionReference reference : extensionManager.getBdfExtensionReferenceList()) {
                        String registrationName = reference.getRegistrationName();
                        Message titleMessage = reference.getTitleMessage();
                        hp
                                .__(Choices.checkboxLi(hp.name(ExtensionActivationCommand.ACTIVATION_PARAMNAME).value(registrationName).checked(reference.isActive()),
                                        () -> {
                                            hp
                                                    .__escape('[')
                                                    .__escape(registrationName)
                                                    .__escape(']');
                                            if (titleMessage != null) {
                                                hp
                                                        .__dash()
                                                        .__localize(titleMessage);
                                            }
                                        }));
                    }
                })
                .__end(commandBox);
        return true;
    }

}
