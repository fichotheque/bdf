/* BdfServer_HtmlProducers - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.configuration;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ConfigurationDomain;
import fr.exemole.bdfserver.api.policies.UserAllow;
import fr.exemole.bdfserver.commands.configuration.IdentificationCommand;
import fr.exemole.bdfserver.commands.configuration.LangConfigCommand;
import fr.exemole.bdfserver.commands.configuration.UserAllowChangeCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.attributes.Deploy;
import fr.exemole.bdfserver.html.consumers.attributes.InputPattern;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.metadata.FichothequeMetadata;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class CoreFormHtmlProducer extends BdfServerHtmlProducer {

    public CoreFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.DEPLOY);
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CONFIGURATION)
                .family("CNF")
                .veil(true)
                .page(ConfigurationDomain.COREFORM_PAGE);
        printIdentification(commandBox);
        printLangConfig(commandBox);
        printUserAllow(commandBox);
        end();
    }

    private void printIdentification(CommandBox commandBox) {
        FichothequeMetadata fichothequeMetadata = bdfServer.getFichotheque().getFichothequeMetadata();
        commandBox = commandBox.derive(IdentificationCommand.COMMANDNAME, IdentificationCommand.COMMANDKEY)
                .submitLocKey("_ submit.configuration.identification");
        this
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.configuration.authority", name(IdentificationCommand.AUTHORITY_PARAMNAME).value(fichothequeMetadata.getAuthority()).size("60").populate(InputPattern.AUTHORITY).required(true)))
                .__(Grid.textInputRow("_ label.configuration.basename_fichotheque", name(IdentificationCommand.BASENAME_PARAMNAME).value(fichothequeMetadata.getBaseName()).size("30").populate(InputPattern.TECHNICAl_UNDERSCORE).required(true)))
                .__(Grid.END)
                .__end(commandBox);
    }

    private void printLangConfig(CommandBox commandBox) {
        LangConfiguration langConfiguration = bdfServer.getLangConfiguration();
        boolean allLanguages = langConfiguration.isAllLanguages();
        String supplementaryId = generateId();
        commandBox = commandBox.derive(LangConfigCommand.COMMANDNAME, LangConfigCommand.COMMANDKEY)
                .submitLocKey("_ submit.configuration.langconfig");
        this
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.configuration.workinglang", name(LangConfigCommand.WORKINGLANG_PARAMNAME).value(langConfiguration.getWorkingLangs().toString(";")).size("15")))
                .__(Grid.radioRow("_ label.configuration.alllang_no", name(LangConfigCommand.ALLLANG_PARAMNAME).value("0").checked(!allLanguages).populate(Deploy.radio(supplementaryId)),
                        () -> {
                            this
                                    .DIV(Grid.detailPanelTable().id(supplementaryId).addClass(allLanguages, "hidden"))
                                    .__(Grid.textInputRow("_ label.configuration.supplementarylang", name(LangConfigCommand.SUPPLEMENTARYLANG_PARAMNAME).value(langConfiguration.getSupplementaryLangs().toString(";")).size("15")))
                                    ._DIV();
                        }))
                .__(Grid.radioRow("_ label.configuration.alllang_yes", name(LangConfigCommand.ALLLANG_PARAMNAME).value("1").checked(allLanguages)))
                .__(Grid.checkboxRow("_ label.configuration.withnonlatin", name(LangConfigCommand.WITHNONLATIN_PARAMNAME).value("1").checked(langConfiguration.isWithNonlatin())))
                .__(Grid.checkboxRow("_ label.configuration.withoutsurnamefirst", name(LangConfigCommand.WITHOUTSURNAMEFIRST).value("1").checked(langConfiguration.isWithoutSurnameFirst())))
                .__(Grid.END)
                .__end(commandBox);
    }

    private void printUserAllow(CommandBox commandBox) {
        UserAllow userAllow = bdfServer.getPolicyManager().getUserAllow();
        commandBox = commandBox.derive(UserAllowChangeCommand.COMMANDNAME, UserAllowChangeCommand.COMMANDKEY)
                .submitLocKey("_ submit.configuration.userallowchange");

        this
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.checkboxRow("_ label.configuration.userallow_data", name(UserAllowChangeCommand.DATA_PARAMNAME).value("1").checked(userAllow.isDataChangeAllowed())))
                .__(Grid.checkboxRow("_ label.configuration.userallow_password", name(UserAllowChangeCommand.PASSWORD_PARAMNAME).value("1").checked(userAllow.isPasswordChangeAllowed())))
                .__(Grid.END)
                .__end(commandBox);
    }

}
