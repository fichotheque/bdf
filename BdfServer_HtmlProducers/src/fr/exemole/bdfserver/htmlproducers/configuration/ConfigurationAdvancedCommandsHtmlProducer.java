/* BdfServer_HtmlProducers - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.configuration;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ConfigurationDomain;
import fr.exemole.bdfserver.commands.configuration.FichothequeAttributeChangeCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.AttributesText;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.metadata.FichothequeMetadata;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public class ConfigurationAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    public ConfigurationAdvancedCommandsHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("configuration.css");
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CONFIGURATION)
                .family("CNF")
                .veil(true)
                .page(ConfigurationDomain.CONFIGADVANCEDCOMMANDS_PAGE);
        printFichothequeAttributeChangeBox(this, commandBox, fichotheque.getFichothequeMetadata());
        end();
    }

    private static void printFichothequeAttributeChangeBox(HtmlPrinter hp, CommandBox commandBox, FichothequeMetadata fichothequeMetadata) {
        commandBox = commandBox.derive(FichothequeAttributeChangeCommand.COMMANDNAME, FichothequeAttributeChangeCommand.COMMANDKEY)
                .actionCssClass("action-Attributes")
                .submitLocKey("_ submit.global.attributechange");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.attributes", hp.name(FichothequeAttributeChangeCommand.ATTRIBUTES_PARAMNAME).rows(8).cols(75).attr("data-codemirror-mode", "attributes"),
                        new AttributesText(fichothequeMetadata.getAttributes())))
                .__(Grid.END)
                .__end(commandBox);
    }

}
