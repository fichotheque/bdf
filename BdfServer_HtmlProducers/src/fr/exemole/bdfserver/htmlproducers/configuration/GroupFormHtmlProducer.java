/* BdfServer_HtmlProducers - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.configuration;

import fr.exemole.bdfserver.api.groups.GroupDef;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.ConfigurationDomain;
import fr.exemole.bdfserver.api.managers.GroupManager;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import java.util.List;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.localisation.Langs;


/**
 *
 * @author Vincent Calame
 */
public class GroupFormHtmlProducer extends BdfServerHtmlProducer {

    public GroupFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
    }

    @Override
    public void printHtml() {
        GroupManager groupManager = bdfServer.getGroupManager();
        Langs workingLangs = bdfServer.getLangConfiguration().getWorkingLangs();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CONFIGURATION)
                .family("CNF")
                .veil(true)
                .page(ConfigurationDomain.GROUPFORM_PAGE);
        start();
        printCommandMessageUnit();
        printGroupListUnit(groupManager.getGroupDefList(), commandBox.mode(InteractionConstants.SUBUNIT_MODE), workingLangs);
        ConfigurationCommandBoxUtils.printGroupCreationBox(this, commandBox.mode(""), workingLangs);
        end();
    }

    private boolean printGroupListUnit(List<GroupDef> groupDefList, CommandBox commandBox, Langs langs) {
        if (groupDefList.isEmpty()) {
            return false;
        }
        this
                .__(PageUnit.start("action-Groups", "_ link.configuration.groupform"))
                .DL("global-DL");
        for (GroupDef groupDef : groupDefList) {
            String title = groupDef.getTitleLabels().seekLabelString(workingLang, null);
            this
                    .DT()
                    .SPAN("cm-component-group")
                    .__escape(groupDef.getName())
                    ._SPAN()
                    .__if((title != null), () -> {
                        this
                                .__space()
                                .SPAN("cm-comment")
                                .__escape('(')
                                .__escape(title)
                                .__escape(')')
                                ._SPAN();
                    })
                    ._DT()
                    .DD()
                    .__(printGroupChangeDetails(groupDef, commandBox, langs))
                    .__(printGroupRemoveDetails(groupDef, commandBox))
                    ._DD();
        }
        this
                ._DL()
                .__(PageUnit.END);
        return true;
    }

    private boolean printGroupChangeDetails(GroupDef groupDef, CommandBox commandBox, Langs langs) {
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.change")
                ._SUMMARY()
                .__(ConfigurationCommandBoxUtils.printGroupChangeBox(this, commandBox, groupDef, langs))
                ._DETAILS();
        return true;
    }

    private boolean printGroupRemoveDetails(GroupDef groupDef, CommandBox commandBox) {
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.delete")
                ._SUMMARY()
                .__(ConfigurationCommandBoxUtils.printGroupRemoveBox(this, commandBox, groupDef))
                ._DETAILS();
        return true;
    }


}
