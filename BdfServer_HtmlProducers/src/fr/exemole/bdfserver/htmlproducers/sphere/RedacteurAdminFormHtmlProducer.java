/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.commands.sphere.LoginChangeCommand;
import fr.exemole.bdfserver.commands.sphere.RedacteurChangeCommand;
import fr.exemole.bdfserver.commands.sphere.RedacteurPasswordCommand;
import fr.exemole.bdfserver.commands.sphere.RedacteurStatusCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.roles.PermissionCheck;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurAdminFormHtmlProducer extends BdfServerHtmlProducer {

    private final PermissionSummary permissionSummary;
    private final Redacteur redacteur;

    public RedacteurAdminFormHtmlProducer(BdfParameters bdfParameters, Redacteur redacteur) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.redacteur = redacteur;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("sphere.css");
    }

    @Override
    public void printHtml() {
        start();
        SphereHtmlUtils.printRedacteurToolbar(this, SphereDomain.REDACTEUR_ADMINFORM_PAGE, redacteur);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("USR")
                .veil(true)
                .page(SphereDomain.REDACTEUR_ADMINFORM_PAGE);
        boolean sphereSupervisorAllowed = PermissionCheck.isAllowedBySphereSupervisor(bdfServer, bdfUser, redacteur.getSubsetName());
        if (sphereSupervisorAllowed) {
            SphereCommandBoxUtils.printRedacteurChangeBox(this, commandBox, redacteur, bdfServer);
            SphereCommandBoxUtils.printLoginChangeBox(this, commandBox, redacteur);
        } else {
            SphereCommandBoxUtils.printNotAllowed(this, commandBox, RedacteurChangeCommand.COMMANDKEY);
            SphereCommandBoxUtils.printNotAllowed(this, commandBox, LoginChangeCommand.COMMANDKEY);
        }
        if (!bdfServer.getPermissionManager().isAdmin(redacteur)) {
            SphereCommandBoxUtils.printRedacteurStatusBox(this, commandBox, redacteur);
        } else {
            SphereCommandBoxUtils.printNotAvailableCommand(this, commandBox, RedacteurStatusCommand.COMMANDKEY, "_ info.sphere.adminactive");
        }
        if (!redacteur.isInactive()) {
            CommandBox roleCommandBox = CommandBox.init()
                    .action(Domains.SPHERE)
                    .family("ROL")
                    .veil(true)
                    .page(SphereDomain.REDACTEUR_ADMINFORM_PAGE);
            if (permissionSummary.isFichothequeAdmin()) {
                SphereCommandBoxUtils.printRedacteurRoleBox(this, roleCommandBox, bdfServer, bdfUser.getWorkingLang(), redacteur);
            }
            if (sphereSupervisorAllowed) {
                SphereCommandBoxUtils.printRedacteurPasswordBox(this, commandBox, redacteur);
            } else {
                SphereCommandBoxUtils.printNotAllowed(this, commandBox, RedacteurPasswordCommand.COMMANDKEY);
            }
        }
        end();
    }

}
