/* BdfServer_HtmlProducers - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurPreferencesHtmlProducer extends BdfServerHtmlProducer {

    private final Redacteur redacteur;
    private final BdfUser redacteurBdfUser;

    public RedacteurPreferencesHtmlProducer(BdfParameters bdfParameters, Redacteur redacteur) {
        super(bdfParameters);
        this.redacteur = redacteur;
        if (BdfUserUtils.isSame(bdfUser, redacteur)) {
            redacteurBdfUser = bdfUser;
        } else {
            redacteurBdfUser = bdfServer.createBdfUser(redacteur);
        }
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("sphere.css");
    }

    @Override
    public void printHtml() {
        start();
        SphereHtmlUtils.printRedacteurToolbar(this, SphereDomain.REDACTEUR_PREFERENCES_PAGE, redacteur);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("USR")
                .veil(true)
                .page(SphereDomain.REDACTEUR_PREFERENCES_PAGE);
        SphereCommandBoxUtils.printLangBox(this, commandBox, redacteur, redacteurBdfUser, bdfServer, formatLocale);
        SphereCommandBoxUtils.printCustomizeUIBox(this, commandBox, redacteur, redacteurBdfUser);
        SphereCommandBoxUtils.printTemplatesBox(this, commandBox, bdfServer, bdfUser.getWorkingLang(), redacteur, redacteurBdfUser);
        SphereCommandBoxUtils.printDefaultTableExportParametersBox(this, commandBox, redacteur, redacteurBdfUser);
        end();
    }

}
