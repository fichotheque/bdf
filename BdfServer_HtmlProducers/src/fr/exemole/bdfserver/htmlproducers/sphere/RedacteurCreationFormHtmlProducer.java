/* BdfServer_HtmlProducers - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.commands.sphere.RedacteurCreationCommand;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.roles.PermissionCheck;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurCreationFormHtmlProducer extends BdfServerHtmlProducer {

    private final Sphere sphere;

    public RedacteurCreationFormHtmlProducer(BdfParameters bdfParameters, Sphere sphere) {
        super(bdfParameters);
        this.sphere = sphere;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("sphere.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, sphere, SphereDomain.REDACTEUR_CREATIONFORM_PAGE);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("SPH")
                .veil(true)
                .page(SphereDomain.REDACTEUR_ADMINFORM_PAGE);
        boolean sphereSupervisorAllowed = PermissionCheck.isAllowedBySphereSupervisor(bdfServer, bdfUser, sphere.getSubsetName());
        if (sphereSupervisorAllowed) {
            SphereCommandBoxUtils.printRedacteurCreationBox(this, commandBox, sphere);
        } else {
            SphereCommandBoxUtils.printNotAllowed(this, commandBox, RedacteurCreationCommand.COMMANDKEY);
        }
        end();
    }

}
