/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.SubsetIcon;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfHref;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class SphereHtmlUtils {

    private SphereHtmlUtils() {
    }

    public static boolean printSphereToolbar(HtmlPrinter hp, String pageActu, Sphere sphere) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-Metadata", sphere, SphereDomain.SPHERE_METADATAFORM_PAGE, "_ link.sphere.spheremetadataform", pageActu))
                .__(link(button, "action-Advanced", sphere, SphereDomain.SPHERE_ADVANCEDCOMMANDS_PAGE, "_ link.sphere.sphereadvancedcommands", pageActu))
                .__(refresh(button, sphere))
                ._DIV()
                ._NAV();
        return true;
    }

    public static boolean printRedacteurToolbar(HtmlPrinter hp, String currentPage, Redacteur redacteur) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .HEADER("subset-ItemHeader")
                .H1()
                .__(SubsetIcon.SPHERE)
                .__escape(redacteur.getBracketStyle())
                .__dash()
                .__escape(redacteur.getCompleteName())
                ._H1()
                ._HEADER();
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-UserEdit", redacteur, SphereDomain.REDACTEUR_ADMINFORM_PAGE, "_ link.sphere.redacteuradminform", currentPage))
                .__if((!redacteur.isInactive()), link(button, "action-Preferences", redacteur, SphereDomain.REDACTEUR_PREFERENCES_PAGE, "_ link.sphere.userprefs", currentPage))
                .__(link(button, "action-UserStats", redacteur, SphereDomain.REDACTEUR_STATS_PAGE, "_ link.sphere.userstats", currentPage))
                .__(link(button, "action-Advanced", redacteur, SphereDomain.REDACTEUR_ADVANCEDCOMMANDS_PAGE, "_ link.sphere.redacteuradvancedcommands", currentPage))
                .__(refresh(button, redacteur.getSphere()))
                ._DIV()
                ._NAV();
        return true;
    }

    private static Button link(Button button, String action, Object object, String page, String titleLocKey, String currentPage) {
        if (currentPage.equals(page)) {
            return button.current(true).href(null).action(action).tooltip(null);
        } else {
            BdfHref href = BH.domain(Domains.SPHERE).page(page);
            if (object instanceof Sphere) {
                href.subset((Sphere) object);
            } else if (object instanceof Redacteur) {
                href.subsetItem((Redacteur) object);
            }
            return button.current(false).href(href).action(action).tooltipMessage(titleLocKey);
        }
    }

    private static Button refresh(Button button, Sphere sphere) {
        return button.current(false)
                .href(BH.domain(Domains.SPHERE).page(SphereDomain.SPHERE_PAGE).subset(sphere))
                .action("action-Refresh")
                .tooltipMessage("_ link.global.reload")
                .target(BdfHtmlConstants.LIST_FRAME);
    }

}
