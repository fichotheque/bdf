/* BdfServer_HtmlProducers - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurAvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Redacteur redacteur;

    public RedacteurAvancedCommandsHtmlProducer(BdfParameters bdfParameters, Redacteur redacteur) {
        super(bdfParameters);
        this.redacteur = redacteur;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("sphere.css");
    }

    @Override
    public void printHtml() {
        start();
        SphereHtmlUtils.printRedacteurToolbar(this, SphereDomain.REDACTEUR_ADVANCEDCOMMANDS_PAGE, redacteur);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("SPH")
                .veil(true)
                .page(SphereDomain.REDACTEUR_ADVANCEDCOMMANDS_PAGE);
        SphereCommandBoxUtils.printRedacteurAttributeChangeBox(this, commandBox, redacteur);
        end();
    }

}
