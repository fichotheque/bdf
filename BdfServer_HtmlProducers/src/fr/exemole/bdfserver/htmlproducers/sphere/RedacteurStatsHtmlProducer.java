/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.SubsetTitle;
import fr.exemole.bdfserver.html.consumers.Tree;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.BH;
import java.util.List;
import java.util.function.BiConsumer;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.tools.sphere.RedacteurStats;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurStatsHtmlProducer extends BdfServerHtmlProducer {

    private final PermissionSummary permissionSummary;
    private final Redacteur redacteur;

    public RedacteurStatsHtmlProducer(BdfParameters bdfParameters, Redacteur redacteur) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.redacteur = redacteur;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(BdfJsLibs.APPELANT);
        addThemeCss("sphere.css");
    }

    @Override
    public void printHtml() {
        start();
        SphereHtmlUtils.printRedacteurToolbar(this, SphereDomain.REDACTEUR_STATS_PAGE, redacteur);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("SPH")
                .veil(true)
                .page(SphereDomain.REDACTEUR_STATS_PAGE);
        RedacteurStats redacteurStats = new RedacteurStats(redacteur);
        if (!redacteurStats.isEmpty()) {
            printRedacteurStats(redacteurStats);
            if (permissionSummary.isFichothequeAdmin()) {
                SphereCommandBoxUtils.printRedacteurReplaceBox(this, commandBox, redacteur);
            }
        } else {
            this
                    .__(PageUnit.start("_ title.sphere.stats"))
                    .P()
                    .__localize("_ info.sphere.fiche_none")
                    ._P()
                    .__(PageUnit.END);
            commandBox.page(InteractionConstants.MESSAGE_PAGE);
            SphereCommandBoxUtils.printRedacteurRemoveBox(this, commandBox, redacteur);
        }
        end();
    }

    private void printRedacteurStats(RedacteurStats redacteurStats) {
        int count = redacteurStats.getFicheCount();
        ByFicheBranch byFicheBranch = new ByFicheBranch();
        List<RedacteurStats.ByCorpusStats> byCorpusStatsList = redacteurStats.getByCorpusStatsList();
        this
                .__(PageUnit.start("_ title.sphere.stats"))
                .P()
                .__(() -> {
                    if (count == 1) {
                        this
                                .__localize("_ info.sphere.fiche_one");
                    } else {
                        this
                                .__localize("_ info.sphere.fiche_many", count);
                    }
                })
                ._P()
                .__(Tree.TREE, () -> {
                    for (RedacteurStats.ByCorpusStats byCorpusStats : byCorpusStatsList) {
                        this.__(byFicheBranch, byCorpusStats);
                    }
                })
                .__(PageUnit.END);
    }


    private class ByFicheBranch implements BiConsumer<HtmlPrinter, Object> {

        private ByFicheBranch() {

        }

        @Override
        public void accept(HtmlPrinter hp, Object object) {
            RedacteurStats.ByCorpusStats byCorpusStats = (RedacteurStats.ByCorpusStats) object;
            Corpus corpus = byCorpusStats.getCorpus();
            SubsetKey corpusKey = corpus.getSubsetKey();
            hp.__(Tree.OPEN_NODE, () -> {
                hp
                        .P()
                        .__(SubsetTitle.init(corpus, workingLang).subsetIcon(true).subsetName(true))
                        ._P()
                        .__(Tree.BRANCH, () -> {
                            List<RedacteurStats.ByFicheStats> list = byCorpusStats.getByFicheStatsList();
                            for (RedacteurStats.ByFicheStats byFicheStats : list) {
                                int id = byFicheStats.getId();
                                hp.__(Tree.LEAF, () -> {
                                    hp
                                            .__escape(corpusKey.getSubsetName())
                                            .__escape('/')
                                            .__append(id)
                                            .__space()
                                            .SPAN("global-SmallLinks")
                                            .__escape('[');
                                    int poids = byFicheStats.getPoids();
                                    boolean premier = true;
                                    if ((poids & 1) != 0) {
                                        premier = false;
                                        hp
                                                .__escape("redacteurs");
                                    }
                                    if ((poids & 2) != 0) {
                                        if (premier) {
                                            premier = false;
                                        } else {
                                            hp
                                                    .__escape(" - ");
                                        }
                                        hp
                                                .__escape("propriete");
                                    }
                                    if ((poids & 4) != 0) {
                                        if (premier) {
                                            premier = false;
                                        } else {
                                            hp
                                                    .__escape(" - ");
                                        }
                                        hp
                                                .__escape("information");
                                    }
                                    hp
                                            .__escape(']')
                                            .__space()
                                            .__escape('[')
                                            .A(HA.href(BH.domain(Domains.EDITION).page(EditionDomain.FICHE_CHANGE_PAGE).subset(corpus).param(InteractionConstants.ID_PARAMNAME, String.valueOf(id))))
                                            .__localize("_ link.edition.fichechange_short")
                                            ._A()
                                            .__dash()
                                            .A(HA.href("fiches/" + corpusKey.getSubsetName() + "-" + id + ".html"))
                                            .__localize("_ link.global.display")
                                            ._A()
                                            .__escape(']')
                                            ._SPAN();
                                });
                            }
                        });
            });
        }

    }

}
