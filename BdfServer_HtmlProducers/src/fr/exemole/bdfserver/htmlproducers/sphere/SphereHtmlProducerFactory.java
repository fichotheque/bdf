/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class SphereHtmlProducerFactory {

    private final static int SPHERE_ADMIN = 1;
    private final static int REDACTEUR_ADMIN = 2;

    private SphereHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    public static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        switch (getPageType(page)) {
            case SPHERE_ADMIN: {
                Sphere sphere = requestHandler.getSphere();
                parameters.checkSubsetAdmin(sphere);
                switch (page) {
                    case SphereDomain.SPHERE_ADVANCEDCOMMANDS_PAGE: {
                        return new SphereAdvancedCommandsHtmlProducer(parameters, sphere);
                    }
                    case SphereDomain.SPHERE_METADATAFORM_PAGE: {
                        return new SphereMetadataFormHtmlProducer(parameters, sphere);
                    }
                    case SphereDomain.REDACTEUR_CREATIONFORM_PAGE: {
                        return new RedacteurCreationFormHtmlProducer(parameters, sphere);
                    }
                    default:
                        return null;
                }
            }
            case REDACTEUR_ADMIN: {
                Redacteur redacteur = requestHandler.getRedacteur();
                parameters.checkSubsetAdmin(redacteur.getSphere());
                switch (page) {
                    case SphereDomain.REDACTEUR_ADVANCEDCOMMANDS_PAGE: {
                        return new RedacteurAvancedCommandsHtmlProducer(parameters, redacteur);
                    }
                    case SphereDomain.REDACTEUR_ADMINFORM_PAGE: {
                        return new RedacteurAdminFormHtmlProducer(parameters, redacteur);
                    }
                    case SphereDomain.REDACTEUR_PREFERENCES_PAGE: {
                        return new RedacteurPreferencesHtmlProducer(parameters, redacteur);
                    }
                    case SphereDomain.REDACTEUR_STATS_PAGE: {
                        return new RedacteurStatsHtmlProducer(parameters, redacteur);
                    }
                    default:
                        return null;
                }
            }
            default:
                switch (page) {
                    case SphereDomain.REDACTEUR_USERFORM_PAGE: {
                        return new RedacteurUserFormHtmlProducer(parameters);
                    }
                    case SphereDomain.SPHERE_PAGE: {
                        Sphere sphere = requestHandler.getSphere();
                        return new SphereHtmlProducer(parameters, sphere);
                    }
                    case SphereDomain.SPHERE_CREATIONFORM_PAGE: {
                        parameters.checkFichothequeAdmin();
                        return new SphereCreationFormHtmlProducer(parameters);
                    }
                    default:
                        return null;
                }
        }
    }

    private static int getPageType(String page) {
        switch (page) {
            case SphereDomain.SPHERE_METADATAFORM_PAGE:
            case SphereDomain.SPHERE_ADVANCEDCOMMANDS_PAGE:
            case SphereDomain.REDACTEUR_CREATIONFORM_PAGE:
                return SPHERE_ADMIN;
            case SphereDomain.REDACTEUR_ADMINFORM_PAGE:
            case SphereDomain.REDACTEUR_PREFERENCES_PAGE:
            case SphereDomain.REDACTEUR_STATS_PAGE:
            case SphereDomain.REDACTEUR_ADVANCEDCOMMANDS_PAGE:
                return REDACTEUR_ADMIN;
            default:
                return 0;
        }
    }


}
