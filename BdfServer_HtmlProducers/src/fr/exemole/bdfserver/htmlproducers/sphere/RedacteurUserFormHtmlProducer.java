/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.api.policies.UserAllow;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.roles.PermissionCheck;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurUserFormHtmlProducer extends BdfServerHtmlProducer implements SphereDomain {

    private final Redacteur redacteur;

    public RedacteurUserFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        this.redacteur = bdfUser.getRedacteur();
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("USR")
                .veil(true)
                .page(REDACTEUR_USERFORM_PAGE);
        UserAllow userAllow = bdfServer.getPolicyManager().getUserAllow();
        boolean sphereSupervisorAllowed = PermissionCheck.isAllowedBySphereSupervisor(bdfServer, bdfUser, redacteur.getSubsetName());
        if ((userAllow.isDataChangeAllowed()) && (sphereSupervisorAllowed)) {
            SphereCommandBoxUtils.printRedacteurChangeBox(this, commandBox, redacteur, bdfServer);
        }
        SphereCommandBoxUtils.printLangBox(this, commandBox, redacteur, bdfUser, bdfServer, formatLocale);
        if ((userAllow.isPasswordChangeAllowed()) && (sphereSupervisorAllowed)) {
            SphereCommandBoxUtils.printRedacteurPasswordBox(this, commandBox, redacteur);
        }
        SphereCommandBoxUtils.printTemplatesBox(this, commandBox, bdfServer, bdfUser.getWorkingLang(), redacteur, bdfUser);
        SphereCommandBoxUtils.printDefaultTableExportParametersBox(this, commandBox, redacteur, bdfUser);
        SphereCommandBoxUtils.printCustomizeUIBox(this, commandBox, redacteur, bdfUser);
        end();
    }

}
