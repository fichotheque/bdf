/* BdfServer_HtmlProducers - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class SphereAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Sphere sphere;

    public SphereAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Sphere sphere) {
        super(bdfParameters);
        this.sphere = sphere;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("sphere.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, sphere, SphereDomain.SPHERE_ADVANCEDCOMMANDS_PAGE);
        SphereHtmlUtils.printSphereToolbar(this, SphereDomain.SPHERE_ADVANCEDCOMMANDS_PAGE, sphere);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("SPH")
                .veil(true);
        if ((bdfParameters.isFichothequeAdmin()) && (sphere.isRemoveable())) {
            commandBox
                    .errorPage(SphereDomain.SPHERE_ADVANCEDCOMMANDS_PAGE)
                    .page(InteractionConstants.MESSAGE_PAGE);
            SphereCommandBoxUtils.printSphereRemoveBox(this, commandBox, sphere);
        }
        commandBox
                .errorPage(null)
                .page(SphereDomain.SPHERE_ADVANCEDCOMMANDS_PAGE);
        SphereCommandBoxUtils.printSphereAttributeChangeBox(this, commandBox, sphere);
        end();
    }

}
