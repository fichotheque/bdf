/* BdfServer_HtmlProducers - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.sphere;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class SphereMetadataFormHtmlProducer extends BdfServerHtmlProducer {

    private final Sphere sphere;

    public SphereMetadataFormHtmlProducer(BdfParameters bdfParameters, Sphere sphere) {
        super(bdfParameters);
        this.sphere = sphere;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addThemeCss("sphere.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, sphere, SphereDomain.SPHERE_METADATAFORM_PAGE);
        SphereHtmlUtils.printSphereToolbar(this, SphereDomain.SPHERE_METADATAFORM_PAGE, sphere);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.SPHERE)
                .family("SPH")
                .veil(true)
                .page(SphereDomain.SPHERE_METADATAFORM_PAGE);
        SphereCommandBoxUtils.printSpherePhrasesBox(this, commandBox, sphere, bdfServer);
        end();
    }

}
