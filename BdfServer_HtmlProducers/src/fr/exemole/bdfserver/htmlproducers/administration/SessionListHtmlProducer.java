/* BdfServer_HtmlProducers - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.session.SessionObserver;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.api.users.BdfUserConstants;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.Tree;
import java.text.DateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;
import net.fichotheque.sphere.Redacteur;


/**
 *
 * @author Vincent Calame
 */
public class SessionListHtmlProducer extends BdfServerHtmlProducer {

    public SessionListHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
    }

    @Override
    public void printHtml() {
        SessionObserver sessionObserver = (SessionObserver) bdfServer.getContextObject(BdfServerConstants.SESSIONOBSERVER_CONTEXTOBJECT);
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, getFormatLocale());
        dateFormat.setTimeZone(new SimpleTimeZone(0, "GMT"));
        BdfUser[] array = sessionObserver.getCurrentBdfUserArray();
        start();
        this
                .__(PageUnit.start("action-Sessions", "_ title.administration.sessionlist"))
                .H2()
                .__escape('(')
                .__escape(bdfUser.format(array.length))
                .__escape(')')
                ._H2()
                .__(Tree.TREE, () -> {
                    for (BdfUser currentBdfUser : array) {
                        Long creationTime = (Long) currentBdfUser.getParameterValue(BdfUserConstants.SESSION_CREATIONTIME);
                        if (creationTime == null) {
                            continue;
                        }
                        Redacteur redacteur = currentBdfUser.getRedacteur();
                        this
                                .__(Tree.LEAF, () -> {
                                    Long lastAccessedTime = (Long) currentBdfUser.getParameterValue(BdfUserConstants.SESSION_LASTACCESSEDTIME);
                                    if (lastAccessedTime == null) {
                                        lastAccessedTime = creationTime;
                                    }
                                    this
                                            .P()
                                            .__escape(redacteur.getSubsetName())
                                            .__dash()
                                            .__escape(redacteur.getLogin())
                                            ._P()
                                            .P()
                                            .SMALL()
                                            .__localize("_ label.administration.time_sessioncreation")
                                            .__space()
                                            .__escape(dateFormat.format(new Date(creationTime)))
                                            .__space()
                                            .__localize("_ label.global.gmt")
                                            .__dash()
                                            .__localize("_ label.administration.time_lastaccessed")
                                            .__space()
                                            .__escape(dateFormat.format(new Date(lastAccessedTime)))
                                            .__space()
                                            .__localize("_ label.global.gmt")
                                            ._SMALL()
                                            ._P();
                                });
                    }
                })
                .__(PageUnit.END);

        end();

    }

}
