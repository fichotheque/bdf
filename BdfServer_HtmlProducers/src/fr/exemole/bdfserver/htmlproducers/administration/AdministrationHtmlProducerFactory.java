/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.AdministrationDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public final class AdministrationHtmlProducerFactory {

    private AdministrationHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        switch (page) {
            case AdministrationDomain.INDEX_PAGE: {
                return new IndexHtmlProducer(parameters);
            }
            case AdministrationDomain.BACKUP_PAGE: {
                parameters.checkFichothequeAdmin();
                return new BackupHtmlProducer(parameters);
            }
            case AdministrationDomain.BACKUPRESULT_PAGE: {
                parameters.checkFichothequeAdmin();
                String backupFileName = (String) parameters.getResultObject(BdfInstructionConstants.BACKUPFILENAME_OBJ);
                if (backupFileName == null) {
                    throw BdfErrors.missingCommandResultPage(AdministrationDomain.BACKUPRESULT_PAGE);
                }
                return new BackupResultHtmlProducer(parameters, backupFileName);
            }
            case AdministrationDomain.INITLOG_PAGE: {
                parameters.checkFichothequeAdmin();
                return new InitLogHtmlProducer(parameters);
            }
            case AdministrationDomain.RESOURCES_PAGE: {
                parameters.checkFichothequeAdmin();
                return new ResourcesHtmlProducer(parameters);
            }
            case AdministrationDomain.RESOURCE_UPLOAD_PAGE: {
                parameters.checkFichothequeAdmin();
                RelativePath path = requestHandler.getRelativePath();
                return new ResourceUploadHtmlProducer(parameters, path);
            }
            case AdministrationDomain.ROLES_PAGE: {
                parameters.checkFichothequeAdmin();
                return new RolesHtmlProducer(parameters);
            }
            case AdministrationDomain.SESSIONLIST_PAGE: {
                parameters.checkFichothequeAdmin();
                return new SessionListHtmlProducer(parameters);
            }
            case AdministrationDomain.DIAGNOSTIC_PAGE: {
                parameters.checkFichothequeAdmin();
                return new DiagnosticHtmlProducer(parameters);
            }
            default:
                return null;
        }
    }

}
