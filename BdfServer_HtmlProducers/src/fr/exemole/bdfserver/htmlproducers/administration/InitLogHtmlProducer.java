/* BdfServer_HtmlProducers - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.MessageLog;


/**
 *
 * @author Vincent Calame
 */
public class InitLogHtmlProducer extends BdfServerHtmlProducer {

    public InitLogHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addThemeCss("misc.css");
    }

    @Override
    public void printHtml() {
        MessageLog messageLog = bdfServer.getInitMessageLog();
        start();
        if (!messageLog.isEmpty()) {
            this
                    .__(PageUnit.start("action-Logs", "_ title.administration.storageprocesslog"))
                    .__(LogUtils.printHtml(this, messageLog, "global-Logs"))
                    .__(PageUnit.END);
        }
        end();
    }

}
