/* BdfServer_HtmlProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AdministrationDomain;
import fr.exemole.bdfserver.commands.administration.ResourceUploadCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import net.mapeadores.util.html.HtmlConstants;
import net.mapeadores.util.request.RequestConstants;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class ResourceUploadHtmlProducer extends BdfServerHtmlProducer {

    private final RelativePath path;

    public ResourceUploadHtmlProducer(BdfParameters bdfParameters, RelativePath path) {
        super(bdfParameters);
        this.path = path;
        addThemeCss("resource.css");
        setBodyCssClass("global-body-Transparent");
    }

    @Override
    public void printHtml() {
        start();
        this
                .FORM_post(Domains.ADMINISTRATION, null, HtmlConstants.MULTIPART_ENCTYPE)
                .INPUT_hidden(RequestConstants.COMMAND_PARAMETER, ResourceUploadCommand.COMMANDNAME)
                .INPUT_hidden(RequestConstants.PAGE_PARAMETER, AdministrationDomain.RESOURCE_UPLOAD_PAGE)
                .INPUT_hidden(InteractionConstants.PATH_PARAMNAME, path.toString())
                .DIV("resource-iframe-Body")
                .__(printCommandMessageUnit())
                .H3()
                .__localize("_ title.administration.resourceupload")
                ._H3()
                .P("resource-iframe-FileInput")
                .INPUT(name(ResourceUploadCommand.FILE_PARAMNAME).type(HtmlConstants.FILE_TYPE).size("45").classes("global-FileInput"))
                ._P()
                .__(Button.COMMAND,
                        Button.submit("_ submit.administration.resourceupload"))
                ._DIV()
                ._FORM();
        end();
    }


}
