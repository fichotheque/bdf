/* BdfServer_HtmlProducers - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.AdminJsLibs;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class DiagnosticHtmlProducer extends BdfServerHtmlProducer {

    public DiagnosticHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(AdminJsLibs.DIAGNOSTIC);
        addThemeCss("pane.css", "diagnostic.css");
    }

    @Override
    public void printHtml() {
        startLoc("_ link.administration.diagnostic", true);
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

    @Override
    protected void setIcons() {
        addIconPng("theme/icons/16x16/admin-diagnostic.png", "16");
    }

}
