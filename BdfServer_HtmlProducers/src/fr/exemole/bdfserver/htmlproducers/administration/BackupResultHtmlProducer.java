/* BdfServer_HtmlProducers - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class BackupResultHtmlProducer extends BdfServerHtmlProducer {

    private final String backupFileName;

    public BackupResultHtmlProducer(BdfParameters bdfParameters, String backupFileName) {
        super(bdfParameters);
        this.backupFileName = backupFileName;
        addJsLib(BdfJsLibs.COMMANDWAIT);
    }

    @Override
    public void printHtml() {
        start();
        __(PageUnit.SIMPLE, () -> {
            this
                    .__(printCommandMessage())
                    .P()
                    .__localize("_ link.administration.backupfile")
                    .__colon()
                    .A(HA.href(ConfigurationUtils.getBackupRelativeUrl(backupFileName)))
                    .__escape(backupFileName)
                    ._A()
                    ._P();
        });
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ADMINISTRATION)
                .family("ADM")
                .page(InteractionConstants.MESSAGE_PAGE);
        AdministrationCommandBoxUtils.printBackupDeleteBox(this, commandBox, backupFileName);
        end();
    }

}
