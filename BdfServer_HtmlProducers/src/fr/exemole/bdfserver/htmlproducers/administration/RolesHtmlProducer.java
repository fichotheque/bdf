/* BdfServer_HtmlProducers - Copyright (c) 2015-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.AdminJsLibs;
import net.mapeadores.util.html.HA;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class RolesHtmlProducer extends BdfServerHtmlProducer {

    public RolesHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(AdminJsLibs.ROLE);
        addThemeCss("pane.css", "role.css");
    }

    @Override
    public void printHtml() {
        startLoc("_ link.administration.roleadmin", true);
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

    @Override
    protected void setIcons() {
        addIconPng("theme/icons/16x16/admin-roles.png", "16");
    }

}
