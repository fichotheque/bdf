/* BdfServer_HtmlProducers - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.administration;

import fr.exemole.bdfserver.commands.administration.BackupCommand;
import fr.exemole.bdfserver.commands.administration.BackupDeleteCommand;
import fr.exemole.bdfserver.html.consumers.Grid;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class AdministrationCommandBoxUtils {

    private AdministrationCommandBoxUtils() {
    }

    public static boolean printBackupBox(HtmlPrinter hp, CommandBox commandBox) {
        commandBox = commandBox.derive(BackupCommand.COMMANDNAME, BackupCommand.COMMANDKEY)
                .submitLocKey("_ submit.administration.backup")
                .submitProcess("wait");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.checkboxRow("_ label.administration.selectionbackup", hp.name(BackupCommand.SELECTION_PARAMNAME).value("1")))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printBackupDeleteBox(HtmlPrinter hp, CommandBox commandBox, String backupFileName) {
        commandBox = commandBox.derive(BackupDeleteCommand.COMMANDNAME, BackupDeleteCommand.COMMANDKEY)
                .hidden(BackupDeleteCommand.BACKUP_PARAMNAME, backupFileName)
                .submitLocKey("_ submit.administration.backupdelete")
                .submitProcess("wait");
        hp
                .__start(commandBox)
                .__end(commandBox);
        return true;
    }

}
