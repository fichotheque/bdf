/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.main;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.MainDomain;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfHrefProvider;
import net.fichotheque.utils.FichothequeMetadataUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class FramesetHtmlProducer extends BdfServerHtmlProducer {

    private final BdfHrefProvider bdfHrefProvider;

    public FramesetHtmlProducer(BdfParameters bdfParameters, BdfHrefProvider bdfHrefProvider) {
        super(bdfParameters);
        this.bdfHrefProvider = bdfHrefProvider;
        scanIcons();
    }

    @Override
    public void printHtml() {
        Lang preferredLang = langPreference.getFirstLang();
        this
                .DOCTYPE_frameset()
                .HTML(preferredLang);
        this
                .HEAD()
                .TITLE()
                .__escape(FichothequeMetadataUtils.getLongTitle(bdfServer.getFichotheque(), workingLang))
                ._TITLE()
                .META_htmlcontenttype()
                .LINK(HA.type("image/png").href("images/icon.png").rel("icon"))
                ._HEAD();
        this
                .FRAMESET(HA.attr("rows", "55,*"));
        this
                .FRAME(HA.name(BdfHtmlConstants.MENU_FRAME).src(BH.domain(Domains.MAIN).page(MainDomain.MENU_PAGE)));
        String listeWidth = "25%";
        if (preferredLang.isRTLScript()) {
            this
                    .FRAMESET(HA.attr("cols", "*," + listeWidth))
                    .__(appendFrame(BdfHtmlConstants.EDITION_FRAME))
                    .__(appendFrame(BdfHtmlConstants.LIST_FRAME))
                    ._FRAMESET();
        } else {
            this
                    .FRAMESET(HA.attr("cols", listeWidth + ",*"))
                    .__(appendFrame(BdfHtmlConstants.LIST_FRAME))
                    .__(appendFrame(BdfHtmlConstants.EDITION_FRAME))
                    ._FRAMESET();
        }
        this
                ._FRAMESET();
        this
                ._HTML();
    }

    private boolean appendFrame(String name) {
        this
                .FRAME(HA.name(name).src(bdfHrefProvider.getSrc(name)));
        return true;
    }

}
