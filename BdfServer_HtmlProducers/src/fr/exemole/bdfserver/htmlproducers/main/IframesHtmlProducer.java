/* BdfServer_HtmlProducers - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.main;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.MainDomain;
import fr.exemole.bdfserver.api.menu.MenuGroup;
import fr.exemole.bdfserver.api.menu.MenuLink;
import fr.exemole.bdfserver.api.menu.MenuNode;
import fr.exemole.bdfserver.api.providers.MenuLinkProvider;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import fr.exemole.bdfserver.json.BdfUserJson;
import fr.exemole.bdfserver.tools.BdfHrefProvider;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.utils.FichothequeMetadataUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class IframesHtmlProducer extends BdfServerHtmlProducer {

    private final BdfHrefProvider bdfHrefProvider;

    public IframesHtmlProducer(BdfParameters bdfParameters, BdfHrefProvider bdfHrefProvider) {
        super(bdfParameters);
        this.bdfHrefProvider = bdfHrefProvider;
        addThemeCss("main.css", "menu.css", "ficheframe.css");
        addJsLib(MiscJsLibs.MAIN);
        addJsLib(MiscJsLibs.MENU);
        scanIcons();
        setMainStorageKey(Domains.MAIN, MainDomain.IFRAMES_PAGE);
    }

    @Override
    public void printHtml() {
        JsObject args = JsObject.init()
                .put("bdfUser", getBdfUserObject())
                .put("actionGroupArray", getActionGroupArray())
                .put("links", getLinksObject());
        start(FichothequeMetadataUtils.getLongTitle(bdfServer.getFichotheque(), workingLang));
        this
                .SCRIPT()
                .__jsObject("Menu.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id("main"))
                .DIV(HA.id("main_menu").classes("menu-Container"))
                ._DIV()
                .DIV(HA.id("main_body"))
                .DIV(HA.id("main_body_tool"))
                ._DIV()
                .DIV(HA.id("main_body_list"))
                .__(appendIFrame(BdfHtmlConstants.LIST_FRAME, "main_body_list_iframe"))
                ._DIV()
                .DIV(HA.id("main_body_edition"))
                .DIV(HA.id("main_body_edition_toolbar").classes("hidden"))
                ._DIV()
                .DIV(HA.id("main_body_edition_fieldedit").classes("hidden"))
                ._DIV()
                .__(appendIFrame(BdfHtmlConstants.EDITION_FRAME, "main_body_edition_iframe"))
                ._DIV()
                ._DIV()
                ._DIV();
        end();
    }

    private boolean appendIFrame(String name, String id) {
        this
                .IFRAME(HA.name(name).id(id).src(bdfHrefProvider.getSrc(name)))
                ._IFRAME();
        return true;
    }

    private JsObject getBdfUserObject() {
        JsObject bdfUserJsObject = JsObject.init()
                .put("admin", bdfParameters.isFichothequeAdmin());
        BdfUserJson.populate(bdfUserJsObject, bdfUser);
        return bdfUserJsObject;
    }

    private JsObject getLinksObject() {
        JsObject linksJsObject = new JsObject();
        if ((bdfParameters.isFichothequeAdmin()) || (bdfParameters.getPermissionSummary().isPartialAdmin())) {
            linksJsObject.put("administration", true);
        }
        return linksJsObject;
    }

    private List<JsObject> getActionGroupArray() {
        List<JsObject> list = new ArrayList<JsObject>();
        List<MenuGroup> menuGroupList = BdfServerUtils.getExtensionMenuGroupList(bdfServer, bdfUser, MenuLinkProvider.MAINMENU_KEY);
        for (MenuGroup menuGroup : menuGroupList) {
            list.add(getActionGroupObject(menuGroup));
        }
        return list;
    }

    private JsObject getActionGroupObject(MenuGroup menuGroup) {
        JsObject actionGroupObject = new JsObject();
        actionGroupObject.put("title", getTitle(menuGroup));
        List<JsObject> actionList = new ArrayList<JsObject>();
        for (MenuNode menuNode : menuGroup.getMenuNodeList()) {
            if (menuNode instanceof MenuLink) {
                actionList.add(getActionMenuLink((MenuLink) menuNode));
            }
        }
        actionGroupObject.put("actionArray", actionList);
        return actionGroupObject;
    }

    private JsObject getActionMenuLink(MenuLink menuLink) {
        JsObject actionLinkObject = new JsObject();
        String target = menuLink.getTarget();
        if (target == null) {
            target = BdfHtmlConstants.EDITION_FRAME;
        }
        String url = menuLink.getUrl();
        String iconPath = menuLink.getIconPath();
        if (iconPath == null) {
            iconPath = "";
        }
        actionLinkObject
                .put("target", target)
                .put("url", url)
                .put("icon", iconPath)
                .put("title", getTitle(menuLink));
        return actionLinkObject;
    }

    private String getTitle(MenuNode menuNode) {
        Message titleMessage = menuNode.getTitleMessage();
        if (titleMessage != null) {
            return getLocalization(titleMessage);
        } else {
            return menuNode.getTitle();
        }
    }


}
