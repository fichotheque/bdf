/* BdfServer_HtmlProducers - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.main;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class TableDisplayHtmlProducer extends BdfServerHtmlProducer {

    private final TableExportDescription tableExportDescription;
    private final Subset subset;

    public TableDisplayHtmlProducer(BdfParameters bdfParameters, TableExportDescription tableExportDescription, Subset subset) {
        super(bdfParameters);
        this.tableExportDescription = tableExportDescription;
        this.subset = subset;
        addJsLib(MiscJsLibs.TABLEDISPLAY);
        addThemeCss("tabledisplay.css");
    }

    @Override
    public void printHtml() {
        String name = "";
        if (tableExportDescription != null) {
            name = tableExportDescription.getName();
        }
        SubsetKey subsetKey = subset.getSubsetKey();
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("subsetKey", subsetKey.getKeyString())
                .put("subsetCategory", subsetKey.getCategoryString())
                .put("subsetName", subsetKey.getSubsetName())
                .put("tableExportName", name)
                .put("headerType", bdfUser.getPrefs().getDefaultHeaderType());

        start(FichothequeUtils.getTitle(subset, workingLang), true);
        this
                .SCRIPT()
                .__jsObject("TableDisplay.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId))
                ._DIV();
        end();
    }

}
