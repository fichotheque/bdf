/* BdfServer_HtmlProducers - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.main;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.MessageLog;


/**
 *
 * @author Vincent Calame
 */
public class StartHtmlProducer extends BdfServerHtmlProducer {

    public StartHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
    }

    @Override
    public void printHtml() {
        MessageLog messageLog = bdfServer.getInitMessageLog();
        start();
        if (!isWithJavascript()) {
            __(PageUnit.SIMPLE, () -> {
                this
                        .P("global-Warning")
                        .__localize("_ warning.session.noscript")
                        ._P()
                        ._DIV();
            });
        }
        if (BdfUserUtils.isAdmin(bdfServer, bdfUser)) {
            if (!messageLog.isEmpty()) {
                this
                        .__(PageUnit.start("action-Logs", "_ title.administration.storageprocesslog"))
                        .__(LogUtils.printHtml(this, messageLog, "global-Logs"))
                        .__(PageUnit.END);
            }
        }
        end();
    }

}
