/* BdfServer_HtmlProducers - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.main;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AdministrationDomain;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import fr.exemole.bdfserver.api.interaction.domains.MainDomain;
import fr.exemole.bdfserver.api.interaction.domains.MiscDomain;
import fr.exemole.bdfserver.api.interaction.domains.SelectionDomain;
import fr.exemole.bdfserver.api.interaction.domains.SphereDomain;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.SubsetTreeOptions;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.subsettree.TreeFilterEngine;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlConstants;
import net.mapeadores.util.request.RequestConstants;


/**
 *
 * @author Vincent Calame
 */
public class NoScriptMenuHtmlProducer extends BdfServerHtmlProducer {

    private final Button linkButton = Button.link();

    public NoScriptMenuHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addThemeCss("menu.css");
        setBodyCssClass("global-body-TopBorder");
    }

    @Override
    public void printHtml() {
        start();
        this
                .DIV("menu-Container menu-NoScript")
                .__(printLogoBlock())
                .__(printListBlock())
                .__(printAdminBlock())
                .__(printActionBlock())
                .__(printFicheBlock())
                .__(printGlue())
                .__(printSessionBlock())
                ._DIV();
        end();
    }

    private boolean printLogoBlock() {
        String sessionString = getSessionString();
        this
                .DIV("menu-LogoBlock")
                .IMG(HA.src("dyn-pub/images/icon32.svg").title(sessionString).alt(sessionString).width("32").height("32"))
                ._DIV();
        return true;
    }

    private boolean printListBlock() {
        String fichesHref = BH.domain(Domains.MAIN).page(MainDomain.FICHES_PAGE).param(MainDomain.FICHES_RELOAD_PARAMNAME, "1").toString();
        String collectionsHref = BH.domain(Domains.MAIN).page(MainDomain.COLLECTIONS_PAGE).toString();
        this
                .DIV("menu-ListBlock")
                .__(listLink(fichesHref, "action-Fiches", "_ link.main.fiches"))
                .__(listLink(collectionsHref, "action-Collections", "_ link.main.collections"))
                ._DIV();
        return true;
    }

    private boolean printAdminBlock() {
        String selectionHref = BH.domain(Domains.SELECTION).page(SelectionDomain.SELECTFORM_PAGE).toString();
        String adminHref = BH.domain(Domains.ADMINISTRATION).page(AdministrationDomain.INDEX_PAGE).toString();
        this
                .DIV("menu-AdminBlock")
                .__(editionLink(selectionHref, "action-Selection", "_ link.selection.form"));
        if ((bdfParameters.isFichothequeAdmin()) || (bdfParameters.getPermissionSummary().isPartialAdmin())) {
            this
                    .__(listLink(adminHref, "action-Admin", "_ link.administration.index"));
        } else {
            this
                    .SPAN("menu-EmptySpace")
                    ._SPAN();
        }
        this
                ._DIV();
        return true;
    }

    private boolean printActionBlock() {
        this
                .DIV("menu-ActionBlock")
                .__(editionLink(BH.domain(Domains.MISC).page(MiscDomain.TABLEEXPORTFORM_PAGE).toString(), "action-Ods", "_ link.misc.ods_export"))
                .__(editionLink(BH.domain(Domains.SPHERE).page(SphereDomain.REDACTEUR_USERFORM_PAGE).toString(), "action-Preferences", "_ link.sphere.userprefs"))
                ._DIV();
        return true;
    }

    private boolean printFicheBlock() {
        SubsetTree filteredSubsetTree = TreeFilterEngine.read(bdfParameters.getPermissionSummary(), bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_CORPUS));
        if (filteredSubsetTree.getNodeList().isEmpty()) {
            return false;
        }
        this
                .DIV("menu-FicheBlock")
                .DIV("menu-fiche-CreationForm")
                .FORM_get(Domains.EDITION, BdfHtmlConstants.EDITION_FRAME)
                .SELECT(HA.id("corpusSelect").name("corpus"))
                .__(SubsetTreeOptions.init(filteredSubsetTree, bdfServer, workingLang)
                        .onlyNames(true)
                        .withKeys(false)
                )
                ._SELECT()
                .__space()
                .INPUT_hidden(RequestConstants.PAGE_PARAMETER, EditionDomain.FICHE_CREATION_PAGE)
                .__(Button.submit("_ link.edition.fichecreation"))
                ._FORM()
                ._DIV()
                .DIV("menu-EmptySpace")
                ._DIV()
                ._DIV();
        return true;
    }

    private boolean printGlue() {
        this
                .DIV("global-Glue")
                ._DIV();
        return true;
    }

    private boolean printSessionBlock() {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        this
                .DIV("menu-SessionBlock")
                .__(button.href(BH.domain(Domains.SESSION).param(InteractionConstants.BDF_EXIT_PARAMNAME, "1").param(InteractionConstants.BDF_DEFAULT_SPHERE_PARAMNAME, bdfUser.getRedacteur().getSubsetName()))
                        .action("action-SessionClose")
                        .tooltipMessage("_ link.session.new")
                        .target(HtmlConstants.TOP_TARGET))
                .__(button.href(BH.domain(Domains.SESSION))
                        .action("action-SessionDuplicate")
                        .tooltipMessage("_ link.session.duplicate")
                        .target(HtmlConstants.BLANK_TARGET))
                ._DIV();
        return true;
    }


    private Button listLink(String href, String action, String messageKey) {
        return linkButton.href(href).action(action).textL10nObject(messageKey).target(BdfHtmlConstants.LIST_FRAME);
    }

    private Button editionLink(String href, String action, String messageKey) {
        return linkButton.href(href).action(action).textL10nObject(messageKey).target(BdfHtmlConstants.EDITION_FRAME);
    }

    private String getSessionString() {
        Redacteur redacteur = bdfUser.getRedacteur();
        StringBuilder buf = new StringBuilder();
        buf.append(getLocalization("_ label.menu.openby"));
        buf.append(" ");
        buf.append(redacteur.getCompleteName());
        buf.append(" – ");
        buf.append(redacteur.getBracketStyle());
        return buf.toString();
    }

}
