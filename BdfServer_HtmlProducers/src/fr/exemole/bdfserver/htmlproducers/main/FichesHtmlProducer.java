/* BdfServer_HtmlProducers - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.main;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import fr.exemole.bdfserver.api.interaction.domains.MailingDomain;
import fr.exemole.bdfserver.api.interaction.domains.MiscDomain;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.api.managers.TransformationManager;
import fr.exemole.bdfserver.api.roles.SatelliteOpportunities;
import fr.exemole.bdfserver.commands.corpus.CorpusCreationCommand;
import fr.exemole.bdfserver.email.SendEngine;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.SatelliteTree;
import fr.exemole.bdfserver.html.consumers.Tree;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfHref;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.exportation.transformation.BdfTransformationUtils;
import fr.exemole.bdfserver.tools.exportation.transformation.ExtensionInfo;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import fr.exemole.bdfserver.tools.ui.components.UiSummary;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.include.IconSources;
import net.fichotheque.namespaces.IconSpace;
import net.fichotheque.namespaces.TransformationSpace;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.MotcleIcon;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.exportation.transformation.TransformationCheck;
import net.fichotheque.tools.thesaurus.IconSourcesParser;
import net.fichotheque.utils.CorpusUtils;
import net.fichotheque.utils.FichothequeUtils;
import net.fichotheque.utils.pointeurs.PointeurFactory;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class FichesHtmlProducer extends BdfServerHtmlProducer {

    private final TransformationManager transformationManager;
    private final DateFormat dateFormat;
    private final MessageFormat idFormat;
    private final boolean withMailing;
    private final boolean withGeolocalisation;
    private final Redacteur redacteur;
    private final PermissionSummary permissionSummary;
    private final boolean isEmpty;
    private final boolean isUnique;
    private final SatelliteTree satelliteTree;
    private final Set<String> corpusNameSet = new HashSet<String>();
    private final Button locLink = Button.link().target(BdfHtmlConstants.EDITION_FRAME);
    private final Button titleLink = Button.link().target(BdfHtmlConstants.EDITION_FRAME);
    private final Button messageLink = Button.link().target(BdfHtmlConstants.EDITION_FRAME);
    private final Fiches fiches;

    public FichesHtmlProducer(BdfParameters bdfParameters, Fiches fiches) {
        super(bdfParameters);
        this.fiches = fiches;
        this.permissionSummary = bdfParameters.getPermissionSummary();
        transformationManager = bdfServer.getTransformationManager();
        dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, getFormatLocale());
        idFormat = new MessageFormat(getLocalization("_ info.main.fichenumber"), getFormatLocale());
        this.redacteur = bdfUser.getRedacteur();
        withMailing = SendEngine.canSend(bdfParameters);
        boolean geolocalisation = false;
        for (Fiches.Entry entry : fiches.getEntryList()) {
            Corpus corpus = entry.getCorpus();
            corpusNameSet.add(corpus.getSubsetName());
            if (corpus.getCorpusMetadata().getGeolocalisationField() != null) {
                geolocalisation = true;
            }
        }
        withGeolocalisation = geolocalisation;
        int ficheCount = fiches.getFicheCount();
        this.isEmpty = (ficheCount == 0);
        this.isUnique = (ficheCount == 1);
        satelliteTree = new SatelliteTree(workingLang, formatLocale);
        addJsLib(BdfJsLibs.DEPLOY);
        addThemeCss("fiches.css");
        checkFontAwesome();
    }

    @Override
    public void printHtml() {
        start();
        if (isEmpty) {
            this
                    .__(printEmpty());
        } else {
            this
                    .__(printToolbar());
            for (Fiches.Entry entry : fiches.getEntryList()) {
                Corpus corpus = entry.getCorpus();
                List<FicheMeta> ficheMetaList = entry.getFicheMetaList();
                int corpusFicheCount = ficheMetaList.size();
                CorpusInfo corpusInfo = new CorpusInfo(corpus);
                this
                        .DIV("fiches-Corpus")
                        .DETAILS(HA.open(true))
                        .SUMMARY()
                        .__escape(FichothequeUtils.getTitle(corpus, workingLang))
                        .__space()
                        .__(BdfHtmlUtils.printItemCount(this, bdfUser, corpusFicheCount))
                        ._SUMMARY()
                        .__(MainHtmlUtils.printTableDisplayDetails(this, bdfServer, bdfUser, corpus, withMailing, HA.classes("tools-Details fiches-corpus-TableDetails")))
                        .UL("subsetitem-List");
                for (FicheMeta ficheMeta : ficheMetaList) {
                    corpusInfo.setCurrentFicheMeta(ficheMeta);
                    this
                            .LI()
                            .__(printFicheMeta(corpusInfo))
                            ._LI();
                }
                this
                        ._UL()
                        ._DETAILS()
                        ._DIV();

            }
        }
        end();
    }

    private boolean printEmpty() {
        String messageKey;
        boolean emptyCorpus = fichotheque.getCorpusList().isEmpty();
        if (emptyCorpus) {
            messageKey = "_ info.main.none_corpus";
        } else if (CorpusUtils.getFicheCount(bdfServer.getFichotheque()) == 0) {
            messageKey = "_ info.main.none_fiche";
        } else {
            messageKey = "_ info.main.none_selection";
        }
        this
                .DIV("unit-SimpleUnit fiches-EmptyUnit")
                .P()
                .__localize(messageKey)
                ._P()
                ._DIV();
        if ((emptyCorpus) && (BdfUserUtils.isAdmin(bdfServer, bdfUser))) {
            this
                    .DIV("tools-List")
                    .__(Button.link(BH.domain(Domains.CORPUS).page(CorpusDomain.CORPUS_CREATIONFORM_PAGE).toString()).action("action-New").target(BdfHtmlConstants.EDITION_FRAME).textL10nObject(CorpusCreationCommand.COMMANDKEY))
                    ._DIV();

        }
        return true;
    }

    private boolean printToolbar() {
        TransformationCheck compilationTransformationCheck = TransformationCheck.check(transformationManager.getTransformationDescription(TransformationKey.COMPILATION_INSTANCE), true);
        ExtensionInfo[] compilationExtensionInfoArray = BdfTransformationUtils.getExtensionInfoArray(bdfServer, compilationTransformationCheck);
        boolean withIndexationSelection = isWithIndexationSelection();
        boolean withSelection = ((withIndexationSelection) || (withMailing) || (withGeolocalisation));
        this
                .DIV("fiches-Toolbar");
        this
                .DETAILS("tools-Details")
                .SUMMARY()
                .__localize("_ title.fiches.compilation")
                ._SUMMARY()
                .DIV("tools-List")
                .__(printPreferredCompilationTemplateLinks(compilationExtensionInfoArray))
                .__if(withMailing, link(BH.domain(Domains.MAILING).page(MailingDomain.SEND_FORM_PAGE).param("type", MailingDomain.COMPILATION_SENDTYPE), "action-Send", "_ link.global.send", "_ link.mailing.send_compilation"))
                .__(printTransformationDetails(compilationTransformationCheck, compilationExtensionInfoArray, null))
                ._DIV()
                ._DETAILS();
        if (withSelection) {
            this
                    .DETAILS("tools-Details")
                    .SUMMARY()
                    .__localize("_ title.fiches.selection")
                    ._SUMMARY()
                    .DIV("tools-List")
                    .__if(withIndexationSelection, link(BH.domain(Domains.THESAURUS).page(ThesaurusDomain.SELECTIONINDEXATION_CHOICE_PAGE), "action-FicheIndexation", "_ link.edition.ficheindexation_short", "_ link.edition.selectionindexation"))
                    .__if(withMailing, link(BH.domain(Domains.MAILING).page(MailingDomain.SEND_FORM_PAGE).param("type", MailingDomain.SELECTION_SENDTYPE), "action-Send", "_ link.global.send", "_ link.mailing.send_selection"))
                    .__if(withGeolocalisation, link(BH.domain(Domains.MISC).page(MiscDomain.FICHESGEO_PAGE), "action-Geo", "_ link.misc.fichesgeo", null))
                    ._DIV()
                    ._DETAILS();
        }
        this
                ._DIV();
        return true;
    }


    private boolean printFicheMeta(CorpusInfo corpusInfo) {
        FicheMeta ficheMeta = corpusInfo.getCurrentFicheMeta();
        boolean discarded = ficheMeta.isDiscarded();
        String titre = ficheMeta.getTitre();
        if (titre.length() == 0) {
            titre = "??";
        }
        this
                .DIV("subsetitem-Title")
                .P()
                .A(HA.href(BdfInstructionUtils.getFicheGetLink(ficheMeta, "html")).target(BdfHtmlConstants.EDITION_FRAME).classes("fiches-DisplayLink").addClass(discarded, "fiches-Discarded"))
                .__escape(titre)
                ._A()
                .__(corpusInfo.printIcons())
                ._P();
        if (corpusInfo.canWriteCurrent()) {
            this
                    .A(HA.href(BH.domain(Domains.EDITION).page(EditionDomain.FICHE_CHANGE_PAGE).subsetItem(ficheMeta)).titleLocKey("_ link.edition.fichechange_short").target(BdfHtmlConstants.EDITION_FRAME).classes("button-Circle action-FicheEdit"))
                    .__(Button.ICON)
                    ._A();
        }
        this
                ._DIV();
        this
                .DIV("subsetitem-Infos");
        Object[] objs = new Object[1];
        objs[0] = ficheMeta.getId();
        this
                .__escape(idFormat.format(objs))
                .__dash();
        FuzzyDate creationDate = ficheMeta.getCreationDate();
        if (creationDate != null) {
            this
                    .__escape(dateFormat.format(creationDate.toDate()));
            FuzzyDate modificationDate = ficheMeta.getModificationDate();
            if (!modificationDate.equals(creationDate)) {
                this
                        .__escape(" / ")
                        .__escape(dateFormat.format(modificationDate.toDate()));
            }
        }
        this
                ._DIV();
        this
                .__(printCommandDetails(corpusInfo))
                .__((corpusInfo.withSatellite) ? printSatelliteDetails(ficheMeta) : false)
                .__(printSupplementaryDetails(ficheMeta));
        return true;
    }

    /**
     * Sert pour la surcharge
     */
    protected boolean printSupplementaryCommands(FicheMeta ficheMeta) {
        return true;
    }

    protected boolean printSupplementaryDetails(FicheMeta ficheMeta) {
        return true;
    }

    private boolean printCommandDetails(CorpusInfo corpusInfo) {
        boolean writePermission = corpusInfo.canWriteCurrent();
        FicheMeta ficheMeta = corpusInfo.getCurrentFicheMeta();
        UiSummary uiSummary = corpusInfo.uiSummary;
        boolean isAdmin = corpusInfo.isAdmin;
        this
                .DETAILS(HA.open(isUnique).classes("tools-Details"))
                .SUMMARY()
                .__localize("_ title.global.commands")
                ._SUMMARY()
                .DIV("tools-List")
                .__(printPreferredFicheTemplateLinks(ficheMeta, corpusInfo.extensionInfoArray))
                .__((withMailing) ? printCommand(getSendFicheHref(ficheMeta), "action-Send", "_ link.global.send") : false)
                .__((isAdmin) ? printCommand(BH.domain(Domains.EDITION).page(EditionDomain.FICHE_INDEXATION_PAGE).subsetItem(ficheMeta), "action-FicheIndexation", "_ link.edition.ficheindexation_short") : false)
                .__((writePermission && uiSummary.withAddendaInclude()) ? printCommand(BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_ADDENDA_PAGE).subsetItem(ficheMeta), "action-FicheAddenda", "_ link.edition.ficheaddenda_short") : false)
                .__((writePermission && uiSummary.withAlbumInclude()) ? printCommand(BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_ALBUM_PAGE).subsetItem(ficheMeta), "action-FicheAlbum", "_ link.edition.fichealbum_short") : false)
                .__((writePermission && ficheMeta.isDiscarded()) ? printCommand(BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_RETRIEVEFORM_PAGE).subsetItem(ficheMeta), "action-Retrieve", "_ link.corpus.ficheretrieve") : false)
                .__((writePermission && !ficheMeta.isDiscarded()) ? printCommand(BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_DISCARDFORM_PAGE).subsetItem(ficheMeta), "action-Discard", "_ link.corpus.fichediscard") : false)
                .__((corpusInfo.duplicable) ? printCommand(BH.domain(Domains.EDITION).page(EditionDomain.FICHE_CREATION_PAGE).subsetItem(ficheMeta).param(EditionDomain.PREFILL_PARAMNAME, "duplicate"), "action-Duplicate", "_ link.corpus.ficheduplicate") : false)
                .__((isAdmin) ? printCommand(BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_REMOVEFORM_PAGE).subsetItem(ficheMeta), "action-Delete", "_ link.corpus.ficheremove") : false)
                .__(printCommand(BH.domain(Domains.MISC).page(MiscDomain.HISTORY_PAGE).subsetItem(ficheMeta), "action-History", "_ link.misc.history"))
                .__((isAdmin) ? printCommand(BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_ADVANCEDCOMMANDS_PAGE).subsetItem(ficheMeta), "action-Advanced", "_ link.corpus.ficheadvancedcommands") : false)
                .__(printSupplementaryCommands(ficheMeta))
                .__(printTransformationDetails(corpusInfo))
                ._DIV()
                ._DETAILS();
        return true;
    }


    private boolean printPreferredFicheTemplateLinks(FicheMeta ficheMeta, ExtensionInfo[] extensionInfoArray) {
        for (ExtensionInfo extensionInfo : extensionInfoArray) {
            String href = BdfInstructionUtils.getFicheGetLink(ficheMeta, extensionInfo.getExtension());
            this
                    .__(messageLink(href, extensionInfo.getAction(), extensionInfo.getLinkMessage()));
        }
        return true;
    }


    private boolean printSatelliteDetails(FicheMeta ficheMeta) {
        SatelliteOpportunities satelliteOpportunities = BdfServerUtils.getSatelliteOpportunities(ficheMeta, permissionSummary);
        List<SatelliteOpportunities.Entry> entryList = satelliteOpportunities.getEntryList();
        if (entryList.isEmpty()) {
            return false;
        }
        this
                .DETAILS(HA.open(isUnique).classes("tools-Details"))
                .SUMMARY()
                .__localize("_ link.global.satellites")
                ._SUMMARY()
                .__(satelliteTree.current(ficheMeta, satelliteOpportunities))
                ._DETAILS();
        return true;
    }


    private boolean isWithIndexationSelection() {
        if (permissionSummary.isFichothequeAdmin()) {
            return true;
        }
        for (Thesaurus thesaurus : fichotheque.getThesaurusList()) {
            if (permissionSummary.isSubsetAdmin(thesaurus.getSubsetKey())) {
                return true;
            }
        }
        return false;
    }

    private boolean printPreferredCompilationTemplateLinks(ExtensionInfo[] compilationExtensionInfoArray) {
        this
                .__(link(BdfInstructionUtils.getCompilationGetLink("html"), "action-FicheDisplay", "_ link.global.display", null));
        for (ExtensionInfo extensionInfo : compilationExtensionInfoArray) {
            String href = BdfInstructionUtils.getCompilationGetLink(extensionInfo.getExtension());
            this
                    .__(messageLink(href, extensionInfo.getAction(), extensionInfo.getLinkMessage()));
        }
        return true;
    }

    private Button link(CharSequence href, String action, String titleLocKey, String tooltipLocKey) {
        return locLink.href(href.toString()).action(action).textL10nObject(titleLocKey).tooltipMessage(tooltipLocKey);
    }

    private Button titleLink(CharSequence href, String action, String title) {
        return titleLink.href(href.toString()).action(action).text(title);
    }

    private Button messageLink(CharSequence href, String action, Message titleMessage) {
        return messageLink.href(href.toString()).action(action).textL10nObject(titleMessage);
    }

    private void disabledSpan(String action, String title) {
        this
                .SPAN(HA.classes("global-button-Link global-button-Disabled " + action).title(getLocalization("_ info.fiches.uselesstransformation")))
                .SPAN("global-button-Icon")
                ._SPAN()
                .SPAN("global-button-Text")
                .__escape(title)
                ._SPAN()
                ._SPAN();
    }

    private boolean isDisabled(TemplateDescription templateDescription) {
        Attribute corpusTokens = templateDescription.getTemplateDef().getAttributes().getAttribute(TransformationSpace.CORPUS_KEY);
        if (corpusTokens == null) {
            return false;
        }
        for (String token : corpusTokens) {
            if (corpusNameSet.contains(token)) {
                return false;
            }
        }
        return true;
    }

    protected boolean printCommand(CharSequence href, String action, String titleLocKey) {
        return printCommand(href, action, titleLocKey, null);
    }

    protected boolean printCommand(CharSequence href, String action, String titleLocKey, String tooltipLocKey) {
        this
                .__(link(href, action, titleLocKey, tooltipLocKey));
        return true;
    }

    private static BdfHref getSendFicheHref(FicheMeta ficheMeta) {
        return BH.domain(Domains.MAILING).page(MailingDomain.SEND_FORM_PAGE).subsetItem(ficheMeta).param("type", MailingDomain.FICHE_SENDTYPE);
    }

    private boolean printTransformationDetails(CorpusInfo corpusInfo) {
        return printTransformationDetails(corpusInfo.corpusCheck, corpusInfo.extensionInfoArray, corpusInfo.getCurrentFicheMeta());
    }

    private boolean printTransformationDetails(TransformationCheck transformationCheck, ExtensionInfo[] extensionInfoArray, FicheMeta ficheMeta) {
        boolean checkDisable = (ficheMeta == null);
        if (transformationCheck.withSimpleTemplate()) {
            this
                    .DETAILS("tools-SubDetails")
                    .SUMMARY("action-FicheDisplay")
                    .__(Button.ICON)
                    .__localize("_ link.main.templates_display")
                    ._SUMMARY()
                    .__(Tree.TREE, () -> {
                        this
                                .__(Tree.LEAF, () -> {
                                    this
                                            .__(link(getHref("html", TemplateKey.DEFAULT_NAME, ficheMeta), "action-FicheDisplay", "_ label.transformation.defaulttemplate", null));
                                });
                        for (TemplateDescription templateDescription : transformationCheck.getSimpleTemplateDescriptionArray()) {
                            TemplateKey templateKey = templateDescription.getTemplateKey();
                            String title = templateDescription.getTitle(workingLang);
                            boolean disabled = (checkDisable) ? isDisabled(templateDescription) : false;
                            this
                                    .__(Tree.LEAF, () -> {
                                        if (disabled) {
                                            disabledSpan("action-FicheDisplay", title);
                                        } else {
                                            this
                                                    .__(titleLink(getHref("html", templateKey.getName(), ficheMeta), "action-FicheDisplay", title));
                                        }
                                    });
                        }
                    })
                    ._DETAILS();
        }
        for (ExtensionInfo extensionInfo : extensionInfoArray) {
            if (extensionInfo.getTemplateCount() > 1) {
                String action = extensionInfo.getAction();
                String extension = extensionInfo.getExtension();
                this
                        .DETAILS("tools-SubDetails")
                        .SUMMARY(action)
                        .__(Button.ICON)
                        .__localize("_ link.main.templates_version", extension.toUpperCase())
                        ._SUMMARY()
                        .__(Tree.TREE, () -> {
                            if (extensionInfo.isDefaultTemplateAvailable()) {
                                this
                                        .__(Tree.LEAF, () -> {
                                            this
                                                    .__(link(getHref(extension, TemplateKey.DEFAULT_NAME, ficheMeta), action, "_ label.transformation.defaulttemplate", null));
                                        });
                            }
                            for (TemplateDescription templateDescription : extensionInfo.getTemplateDescriptionArray()) {
                                TemplateKey templateKey = templateDescription.getTemplateKey();
                                String title = templateDescription.getTitle(workingLang);
                                boolean disabled = (checkDisable) ? isDisabled(templateDescription) : false;
                                this
                                        .__(Tree.LEAF, () -> {
                                            if (disabled) {
                                                disabledSpan(action, title);
                                            } else {
                                                this
                                                        .__(titleLink(getHref(extension, templateKey.getName(), ficheMeta), action, title));
                                            }
                                        });
                            }
                        })
                        ._DETAILS();
            }
        }
        return true;
    }

    private static String getHref(String extension, String templateName, FicheMeta ficheMeta) {
        if (ficheMeta == null) {
            return BdfInstructionUtils.getCompilationGetLink(extension, templateName);
        } else {
            return BdfInstructionUtils.getFicheGetLink(ficheMeta, extension, templateName);
        }
    }


    private class CorpusInfo {

        private final Corpus corpus;
        private final UiSummary uiSummary;
        private final TransformationCheck corpusCheck;
        private final ExtensionInfo[] extensionInfoArray;
        private final boolean withSatellite;
        private final boolean duplicable;
        private final boolean isAdmin;
        private final FichePointeur fichePointeur;
        private final IconSources iconSources;
        private boolean writePermission = false;

        private CorpusInfo(Corpus corpus) {
            this.corpus = corpus;
            this.uiSummary = UiUtils.summarize(bdfServer.getUiManager().getMainUiComponents(corpus));
            this.corpusCheck = TransformationCheck.check(transformationManager.getTransformationDescription(new TransformationKey(corpus.getSubsetKey())), true);
            this.extensionInfoArray = BdfTransformationUtils.getExtensionInfoArray(bdfServer, corpusCheck);
            this.withSatellite = (!corpus.getSatelliteCorpusList().isEmpty());
            if ((corpus.getMasterSubset() == null) && (permissionSummary.canCreate(corpus))) {
                this.duplicable = true;
            } else {
                this.duplicable = false;
            }
            this.isAdmin = permissionSummary.isSubsetAdmin(corpus.getSubsetKey());
            this.fichePointeur = PointeurFactory.newFichePointeur(corpus, false);
            Attribute iconSourcesAttribute = corpus.getCorpusMetadata().getAttributes().getAttribute(IconSpace.SOURCES_KEY);
            if (iconSourcesAttribute != null) {
                IconSources parsedIconSources = IconSourcesParser.parse(fichotheque, iconSourcesAttribute);
                if (parsedIconSources.isEmpty()) {
                    this.iconSources = null;
                } else {
                    this.iconSources = parsedIconSources;
                }
            } else {
                this.iconSources = null;
            }
        }

        private void setCurrentFicheMeta(FicheMeta ficheMeta) {
            this.fichePointeur.setCurrentFicheMeta(ficheMeta);
            this.writePermission = permissionSummary.canWrite(ficheMeta);
        }

        private FicheMeta getCurrentFicheMeta() {
            return this.fichePointeur.getCurrentFicheMeta();
        }

        public boolean canWriteCurrent() {
            return this.writePermission;
        }

        private boolean printIcons() {
            if (iconSources == null) {
                return false;
            }
            for (IconSources.Entry entry : iconSources.getEntryList()) {
                for (Liaison liaison : fichePointeur.getLiaisons(entry.getThesaurus(), entry.getIncludeMode())) {
                    Motcle motcle = (Motcle) liaison.getSubsetItem();
                    MotcleIcon motcleIcon = motcle.getMotcleIcon();
                    if (motcleIcon != null) {
                        __space();
                        __escape(motcleIcon.getCharIcon("\u23F9"));
                    }
                }
            }
            return true;
        }

    }

}
