/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.main;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.MainDomain;
import fr.exemole.bdfserver.api.namespaces.BdfUserSpace;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.api.users.BdfUserPrefsEditor;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.RequestHandler;
import net.fichotheque.Subset;
import net.fichotheque.exportation.table.TableExportDescription;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class MainHtmlProducerFactory {

    private MainHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        switch (page) {
            case MainDomain.IFRAMES_PAGE: {
                return new IframesHtmlProducer(parameters, MainHtmlUtils.getMainProvider(parameters.getRequestMap()));
            }
            case MainDomain.MENU_PAGE: {
                return new NoScriptMenuHtmlProducer(parameters);
            }
            case MainDomain.START_PAGE: {
                return new StartHtmlProducer(parameters);
            }
            case MainDomain.COLLECTIONS_PAGE: {
                return new CollectionsHtmlProducer(parameters);
            }
            case MainDomain.FICHES_PAGE: {
                testFichesReload(requestHandler);
                return new FichesHtmlProducer(parameters, MainHtmlUtils.getFiches(requestHandler));
            }
            case MainDomain.TABLEDISPLAY_PAGE: {
                TableExportDescription tableExportDescription = null;
                String tableExportName = requestHandler.getTrimedParameter(InteractionConstants.TABLEEXPORT_PARAMNAME);
                if (!tableExportName.isEmpty()) {
                    tableExportDescription = requestHandler.getMandatoryTableExportDescription();
                }
                Subset subset = requestHandler.getMandatorySubset();
                if (requestHandler.isTrue(MainDomain.REMEMBER_ACTION_PARAMNAME)) {
                    saveTableDisplayPrefs(parameters, tableExportDescription, subset);
                }
                return new TableDisplayHtmlProducer(parameters, tableExportDescription, subset);
            }
            default:
                return null;
        }
    }

    public static void testFichesReload(RequestHandler requestHandler) {
        if (requestHandler.isTrue(MainDomain.FICHES_RELOAD_PARAMNAME)) {
            requestHandler.getBdfServer().getSelectionManager().updateFicheSelection(requestHandler.getBdfUser());
        }
    }

    private static void saveTableDisplayPrefs(BdfParameters bdfParameters, TableExportDescription tableExportDescription, Subset subset) {
        BdfUser bdfUser = bdfParameters.getBdfUser();
        AttributeKey attributeKey = BdfUserSpace.toTableAttributeKey(subset);
        Attribute currentAttribute = bdfUser.getPrefs().getAttributes().getAttribute(attributeKey);
        try (EditSession session = bdfParameters.getBdfServer().initEditSession(bdfUser, Domains.MAIN, MainDomain.TABLEDISPLAY_PAGE)) {
            BdfUserPrefsEditor bdfUserPrefsEditor = session.getBdfServerEditor().getBdfUserPrefsEditor(bdfUser.getPrefs());
            if (tableExportDescription == null) {
                if (currentAttribute != null) {
                    bdfUserPrefsEditor.removeAttribute(attributeKey);
                }
            } else {
                Attribute newAttribute = AttributeBuilder.toAttribute(attributeKey, tableExportDescription.getName());
                boolean equality = false;
                if (currentAttribute != null) {
                    equality = AttributeUtils.isEqual(currentAttribute, newAttribute);
                }
                if (!equality) {
                    bdfUserPrefsEditor.putAttribute(newAttribute);
                }
            }
        }
    }

}
