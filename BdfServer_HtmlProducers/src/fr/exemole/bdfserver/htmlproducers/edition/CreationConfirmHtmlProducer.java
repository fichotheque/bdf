/* BdfServer_HtmlProducers - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.edition;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.ParameterMap;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public class CreationConfirmHtmlProducer extends BdfServerHtmlProducer {

    private final static Button STANDARD_BUTTON = Button.submit("action-FicheCreate", "_ link.edition.fichecreation");
    private final static Button SATELLITE_BUTTON = Button.submit("action-FicheCreate", "_ label.edition.satellite_creation");
    private final Corpus corpus;
    private final int ficheId;
    private final PermissionSummary permissionSummary;
    private final SubsetItem masterSubsetItem;
    private final String corpusTitle;

    public CreationConfirmHtmlProducer(BdfParameters bdfParameters, Corpus corpus, int ficheId, @Nullable SubsetItem masterSubsetItem) {
        super(bdfParameters);
        this.corpus = corpus;
        this.ficheId = ficheId;
        this.masterSubsetItem = masterSubsetItem;
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.corpusTitle = FichothequeUtils.getTitle(corpus, workingLang);
    }

    @Override
    public void printHtml() {
        start();
        __(PageUnit.SIMPLE, () -> {
            printUnknownMessage();
            printCreationMessage();
        });
        end();
    }

    private boolean printUnknownMessage() {
        this
                .P()
                .SPAN("global-ErrorMessage")
                .__localize("_ error.unknown.fiche", corpusTitle, ficheId)
                ._SPAN()
                ._P();
        return true;
    }

    private boolean printCreationMessage() {
        if (!permissionSummary.canCreate(corpus)) {
            return false;
        }
        if (masterSubsetItem != null) {
            return printSatelliteCreationMessage();
        } else {
            return printStandardCreationMessage();
        }
    }

    private boolean printStandardCreationMessage() {
        this
                .P()
                .__localize("_ label.edition.withid", ficheId)
                ._P()
                .__(printForm(STANDARD_BUTTON));
        return true;
    }


    private boolean printSatelliteCreationMessage() {
        Subset masterSubset = masterSubsetItem.getSubset();
        short masterCategory = masterSubset.getSubsetKey().getCategory();
        this
                .P()
                .__localize(EditionHtmlUtils.getSatelliteInfoMessageKey(masterCategory), corpusTitle, FichothequeUtils.getTitle(masterSubset, workingLang))
                ._P()
                .P()
                .__localize(EditionHtmlUtils.getMasterSubsetItemMessageKey(masterCategory))
                .__colon()
                ._P()
                .UL()
                .LI()
                .__(printTitle(masterSubsetItem))
                ._LI()
                ._UL()
                .__(printForm(SATELLITE_BUTTON));
        return true;
    }

    private boolean printForm(Button submitButton) {
        this
                .FORM_get(Domains.EDITION)
                .INPUT_hidden(ParameterMap.init()
                        .subset(corpus)
                        .page(EditionDomain.FICHE_CHANGE_PAGE)
                        .param(EditionDomain.FORCE_PARAMNAME, "1")
                        .param(InteractionConstants.ID_PARAMNAME, String.valueOf(ficheId)))
                .__(Button.COMMAND, submitButton)
                ._FORM();
        return true;
    }

    private boolean printTitle(SubsetItem subsetItem) {
        if (subsetItem instanceof FicheMeta) {
            this
                    .__escape(CorpusMetadataUtils.getFicheTitle((FicheMeta) subsetItem, workingLang, formatLocale));
            return true;
        } else if (subsetItem instanceof Motcle) {
            this
                    .__(BdfHtmlUtils.printMotcleTitle(this, (Motcle) subsetItem, workingLang));
            return true;
        } else {
            return false;
        }
    }


}
