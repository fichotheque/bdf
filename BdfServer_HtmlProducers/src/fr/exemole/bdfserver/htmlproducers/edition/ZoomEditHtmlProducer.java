/* BdfServer_HtmlProducers - Copyright (c) 2010-2022s Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.edition;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.FicheJsLibs;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class ZoomEditHtmlProducer extends BdfServerHtmlProducer {

    private final String appelant;
    private final String corpusName;

    public ZoomEditHtmlProducer(BdfParameters bdfParameters, String appelant, String corpusName) {
        super(bdfParameters);
        this.appelant = appelant;
        this.corpusName = corpusName;
        addJsLib(FicheJsLibs.ZOOMEDIT);
        addThemeCss("ficheform.css", "zoomedit.css");
        setBodyCssClass("global-body-TopBorder");
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("appelant", appelant)
                .put("corpus", corpusName);
        start();
        this
                .SCRIPT()
                .__jsObject("Zoomedit.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId).classes("zoomedit-Client"))
                ._DIV();
        end();
    }

}
