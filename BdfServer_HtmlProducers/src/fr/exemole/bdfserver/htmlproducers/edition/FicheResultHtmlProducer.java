/* BdfServer_HtmlProducers - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.edition;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import fr.exemole.bdfserver.api.interaction.domains.MainDomain;
import fr.exemole.bdfserver.api.roles.SatelliteOpportunities;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.SatelliteTree;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.html.jslib.FicheJsLibs;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import fr.exemole.bdfserver.tools.ui.components.UiSummary;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlConstants;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class FicheResultHtmlProducer extends BdfServerHtmlProducer {

    private final static int DEFAULT_TYPE = 1;
    private final static int STANDALONE_TYPE = 2;
    private final static int OVERLAY_TYPE = 3;
    private final static Button REFRESH_BUTTON = Button.link(BH.domain(Domains.MAIN).page(MainDomain.FICHES_PAGE).param(MainDomain.FICHES_RELOAD_PARAMNAME, "1").toString()).target(BdfHtmlConstants.LIST_FRAME).action("action-Refresh").textL10nObject("_ link.global.refresh").tooltipMessage("_ link.main.fiches_update");
    private final static Button OVERLAY_CLOSE_BUTTON = Button.button().style(Button.LINK_STYLE).textL10nObject("_ link.global.close").action("action-Close").buttonId("button_overlay_close").shortcutKey("Escape").shortcutTooltip("Esc");
    private final PermissionSummary permissionSummary;
    private final FicheMeta ficheMeta;
    private final String options;
    private final String gotoValue;
    private final int toolbarType;

    public FicheResultHtmlProducer(BdfParameters bdfParameters, FicheMeta ficheMeta, String options, String gotoValue) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.ficheMeta = ficheMeta;
        addJsLib(BdfJsLibs.SHORTCUT);
        addJsLib(FicheJsLibs.FICHEFRAME_INIT);
        addThemeCss("edition.css", "ficheframe.css");
        if (options.isEmpty()) {
            options = null;
        }
        this.options = options;
        this.gotoValue = gotoValue;
        this.toolbarType = checkToolbarType(options);
    }

    @Override
    public void printHtml() {
        String fichePath = BdfInstructionUtils.getFicheGetLink(ficheMeta, "html");
        JsObject resultItem = JsObject.init()
                .put("type", "fiche")
                .put("corpus", ficheMeta.getCorpus().getSubsetName())
                .put("id", ficheMeta.getId());
        start();
        this
                .SCRIPT()
                .__jsObject("var", "RESULT_ITEM", resultItem)
                .__jsObject("Ficheframe.ARGS", JsObject.init()
                        .put("goto", gotoValue)
                        .put("pageResultOptions", options))
                ._SCRIPT();
        this
                .DIV("edition-Page edition-result-Page")
                .__(EditionHtmlUtils.printFicheframeFieldedit(this))
                .__(printCommandMessageUnit())
                .NAV("edition-Toolbar")
                .__(printToolbar())
                .__(EditionHtmlUtils.printFicheframeToolbar(this))
                ._NAV()
                .IFRAME(HA.name("ficheframe").src(fichePath).classes("edition-Unit").attr("data-shortcut-role", "iframe").attr("data-ficheframe-role", "iframe"))
                .P()
                .A(HA.href(fichePath))
                .__localize("_ link.fiches.fiche_long")
                ._A()
                ._P()
                ._IFRAME()
                ._DIV();
        end();
    }

    private boolean printToolbar() {
        switch (toolbarType) {
            case STANDALONE_TYPE:
                return printStandaloneToolbar();
            case OVERLAY_TYPE:
                return printOverlayToolbar();
            default:
                return printDefaultToolbar();
        }
    }

    private boolean printStandaloneToolbar() {
        this
                .__(editLink())
                .__(displayLink())
                .__(odtLink());

        return true;
    }

    private boolean printOverlayToolbar() {
        this
                .__(editLink())
                .__(displayLink())
                .__(odtLink())
                .SPAN("global-Glue")
                ._SPAN()
                .__(OVERLAY_CLOSE_BUTTON);
        return true;
    }

    private boolean printDefaultToolbar() {
        SatelliteOpportunities satelliteOpportunities = BdfServerUtils.getSatelliteOpportunities(ficheMeta, permissionSummary);
        boolean withSatellites = hasSatellites(satelliteOpportunities);
        UiSummary uiSummary = UiUtils.summarize(bdfServer.getUiManager().getMainUiComponents(ficheMeta.getCorpus()));
        this
                .__(REFRESH_BUTTON)
                .__(editLink())
                .__(addendaLink(uiSummary))
                .__(displayLink())
                .__(odtLink())
                .__(printSatelliteButton(withSatellites, satelliteOpportunities));
        return true;

    }

    private boolean hasSatellites(SatelliteOpportunities satelliteOpportunities) {
        if (satelliteOpportunities.onlyReadAllowed()) {
            return false;
        }
        return !satelliteOpportunities.getEntryList().isEmpty();
    }

    private boolean printSatelliteButton(boolean withSatellites, SatelliteOpportunities satelliteOpportunities) {
        if (!withSatellites) {
            return false;
        }
        this
                .DETAILS("tools-Details")
                .SUMMARY("edition-result-Summary")
                .SPAN("edition-result-SummaryContent")
                .__localize("_ link.global.satellites")
                ._SPAN()
                ._SUMMARY()
                .__(SatelliteTree.init(workingLang, formatLocale).current(ficheMeta, satelliteOpportunities))
                ._DETAILS();
        return true;
    }

    private Button editLink() {
        String editionHref = BH.domain(Domains.EDITION).page(EditionDomain.FICHE_CHANGE_PAGE).subsetItem(ficheMeta).param(InteractionConstants.PAGE_RESULT_OPTIONS_PARAMNAME, options).toString();
        return Button.link().href(editionHref).action("action-FicheEdit").textL10nObject("_ link.edition.fichechange_short").tooltipMessage("_ link.edition.fichechange_back").shortcutKey("F2").shortcutTooltip("F2");
    }

    private Button addendaLink(UiSummary uiSummary) {
        if (!uiSummary.withAddendaInclude()) {
            return null;
        }
        String addendaHref = BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_ADDENDA_PAGE).subsetItem(ficheMeta).toString();
        return Button.link().href(addendaHref).action("action-FicheAddenda").textL10nObject("_ link.edition.ficheaddenda_short").tooltipMessage("_ link.edition.ficheaddenda_long");
    }

    private Button displayLink() {
        String fichePath = BdfInstructionUtils.getFicheGetLink(ficheMeta, "html");
        return Button.link().href(fichePath).action("action-FicheDisplay").textL10nObject("_ link.global.display").tooltipMessage("_ link.fiches.fiche_blank").target(HtmlConstants.BLANK_TARGET);
    }

    private Button odtLink() {
        String fichePath = BdfInstructionUtils.getFicheGetLink(ficheMeta, "odt");
        return Button.link().href(fichePath).action("action-Odt").textMessage("_ link.global.version", "ODT");
    }

    private static int checkToolbarType(String options) {
        if (options == null) {
            return DEFAULT_TYPE;
        }
        if (options.contains("standalone")) {
            return STANDALONE_TYPE;
        }
        if (options.contains("overlay")) {
            return OVERLAY_TYPE;
        }
        return DEFAULT_TYPE;
    }


}
