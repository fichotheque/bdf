/* BdfServer_HtmlProducers - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.edition;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.FicheJsLibs;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class SectionPreviewHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public SectionPreviewHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        this.corpus = corpus;
        addJsLib(FicheJsLibs.SECTIONPREVIEW);
        setBodyCssClass("preview-Body");
        addFicheCss(true);
        ignoreThemeElementCss();
        addThemeCss("preview.css");
    }

    @Override
    public void printHtml() {
        start();
        this
                .DIV(HA.id("preview_container").classes("preview-Container"))
                .DIV(HA.id("preview_section").classes("preview-Section"))
                ._DIV()
                ._DIV();
        end();
    }

}
