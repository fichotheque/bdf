/* BdfServer_HtmlProducers - Copyright (c) 2019-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.edition;

import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.Button;
import java.util.Locale;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class EditionHtmlUtils {

    private EditionHtmlUtils() {

    }

    public static boolean printFicheHeader(HtmlPrinter hp, FicheMeta ficheMeta, Lang workingLang, Locale formatLocale, String formId) {
        SubsetItem masterItem = null;
        Subset masterSubset = ficheMeta.getCorpus().getMasterSubset();
        if (masterSubset != null) {
            masterItem = masterSubset.getSubsetItemById(ficheMeta.getId());
        }
        String title = CorpusMetadataUtils.getFicheTitle(ficheMeta, workingLang, formatLocale);
        return printFicheHeader(hp, title, ficheMeta.isDiscarded(), masterItem, workingLang, formatLocale, formId);
    }

    public static boolean printFicheHeader(HtmlPrinter hp, String title, boolean discarded, SubsetItem masterItem, Lang workingLang, Locale formatLocale, String formId) {
        hp
                .HEADER("subset-ItemHeader edition-Header")
                .__(BdfHtmlUtils.printFicheTitle(hp, title, discarded, masterItem, workingLang, formatLocale))
                .DIV("edition-Submit")
                .__(Button.submit("action-Save", "_ link.global.ok").formId(formId).ficheForm(true).shortcutTooltip("MOD+S"))
                ._DIV()
                ._HEADER();
        return true;
    }

    public static String getSatelliteAllErrMessageKey(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return "_ error.edition.satelliteall_corpus";
            case SubsetKey.CATEGORY_THESAURUS:
                return "_ error.edition.satelliteall_thesaurus";
            default:
                throw new SwitchException("Unknown masterCategory: " + subsetCategory);
        }
    }

    public static String getSatelliteInfoMessageKey(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return "_ info.edition.satellite_corpus";
            case SubsetKey.CATEGORY_THESAURUS:
                return "_ info.edition.satellite_thesaurus";
            default:
                throw new SwitchException("Unknown masterCategory: " + subsetCategory);
        }
    }

    public static String getMasterSelectMessageKey(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return "_ label.edition.masterselect_corpus";
            case SubsetKey.CATEGORY_THESAURUS:
                return "_ label.edition.masterselect_thesaurus";
            default:
                throw new SwitchException("Unknown masterCategory: " + subsetCategory);
        }
    }

    public static String getMasterSubsetItemMessageKey(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return "_ label.edition.master_fiche";
            case SubsetKey.CATEGORY_THESAURUS:
                return "_ label.edition.master_motcle";
            default:
                throw new SwitchException("Unknown masterCategory: " + subsetCategory);
        }
    }

    public static boolean printFicheframeToolbar(HtmlPrinter hp) {
        hp
                .DIV(HA.id(hp.generateId()).classes("edition-FicheframeToolbar").attr("data-ficheframe-role", "toolbar"))
                ._DIV();
        return true;
    }

    public static boolean printFicheframeFieldedit(HtmlPrinter hp) {
        hp
                .DIV(HA.id(hp.generateId()).classes("edition-FicheframeFieldedit hidden").attr("data-ficheframe-role", "fieldedit"))
                ._DIV();
        return true;
    }


}
