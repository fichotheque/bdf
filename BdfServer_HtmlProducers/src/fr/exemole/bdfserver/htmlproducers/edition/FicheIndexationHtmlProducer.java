/* BdfServer_HtmlProducers - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.edition;

import fr.exemole.bdfserver.api.ficheform.FormElement;
import fr.exemole.bdfserver.api.ficheform.FormElementProvider;
import fr.exemole.bdfserver.api.ficheform.ThesaurusIncludeElement;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import fr.exemole.bdfserver.api.subsettree.GroupNode;
import fr.exemole.bdfserver.api.subsettree.SubsetNode;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.commands.edition.FicheIndexationChangeCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.ParameterMap;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.forms.FormHandler;
import fr.exemole.bdfserver.html.forms.IncludeFormHtml;
import fr.exemole.bdfserver.html.jslib.FicheJsLibs;
import fr.exemole.bdfserver.tools.subsettree.TreeFilterEngine;
import fr.exemole.bdfserver.tools.subsettree.TreeUtils;
import fr.exemole.bdfserver.tools.ui.components.IncludeUiBuilder;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.include.ExtendedIncludeKey;
import net.fichotheque.include.IncludeKey;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class FicheIndexationHtmlProducer extends BdfServerHtmlProducer {

    private final FichePointeur fichePointeur;
    private final FicheMeta ficheMeta;
    private final FormElementProvider formElementProvider;
    private final FormHandler formHandler;

    public FicheIndexationHtmlProducer(BdfParameters bdfParameters, FichePointeur fichePointeur, FormElementProvider formElementProvider) {
        super(bdfParameters);
        this.fichePointeur = fichePointeur;
        this.ficheMeta = (FicheMeta) fichePointeur.getCurrentSubsetItem();
        this.formElementProvider = formElementProvider;
        this.formHandler = FormHandler.build(bdfParameters);
        addJsLib(FicheJsLibs.FORM);
        addThemeCss("ficheform.css", "edition.css");
    }

    @Override
    public void printHtml() {
        String formId = generateId();
        start();
        this
                .DIV("edition-Page edition-indexation-Page")
                .__(EditionHtmlUtils.printFicheHeader(this, ficheMeta, workingLang, formatLocale, formId))
                .__(printCommandMessageUnit())
                .MAIN("edition-Unit")
                .FORM_post(HA.action(Domains.EDITION).id(formId)
                        .attr("data-ficheform-role", "form")
                        .attr("data-ficheform-corpus", ficheMeta.getSubsetName())
                        .attr("data-submit-process", "wait"))
                .INPUT_hidden(ParameterMap.init()
                        .command(FicheIndexationChangeCommand.COMMANDNAME)
                        .page(EditionDomain.FICHE_INDEXATION_PAGE)
                        .subset(ficheMeta.getCorpus())
                        .subsetItem(ficheMeta))
                .__(printThesaurusTree())
                .__(Button.COMMAND,
                        Button.submit("action-Save", "_ link.global.ok").formId(formId).ficheForm(true).shortcutKey("mod+s").shortcutTooltip("MOD+S"))
                ._FORM()
                ._MAIN()
                ._DIV();
        end();
    }

    private boolean printThesaurusTree() {
        SubsetTree subsetTree = TreeFilterEngine.read(bdfParameters.getPermissionSummary(), bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_THESAURUS));
        for (SubsetTree.Node node : subsetTree.getNodeList()) {
            if (node instanceof SubsetNode) {
                this
                        .__(printSubsetNode((SubsetNode) node));
            } else if (node instanceof GroupNode) {
                this
                        .__(printGroupNode((GroupNode) node));
            }
        }
        return true;
    }

    private boolean printGroupNode(GroupNode groupNode) {
        this
                .H2()
                .__escape(TreeUtils.getTitle(bdfServer, groupNode, workingLang))
                ._H2();
        for (SubsetTree.Node subnode : groupNode.getSubnodeList()) {
            if (subnode instanceof SubsetNode) {
                this
                        .__(printSubsetNode((SubsetNode) subnode));
            } else if (subnode instanceof GroupNode) {
                this
                        .__(printGroupNode((GroupNode) subnode));

            }
        }
        this
                .P()
                .__doublespace()
                ._P();
        return true;
    }

    private boolean printSubsetNode(SubsetNode subsetNode) {
        Thesaurus thesaurus = (Thesaurus) fichotheque.getSubset(subsetNode.getSubsetKey());
        if (thesaurus != null) {
            FormElement.Include includeElement = formElementProvider.getFormElement(fichePointeur, toSubsetIncludeUi(thesaurus));
            this
                    .__(IncludeFormHtml.printTextThesaurusInclude(this, (ThesaurusIncludeElement.Text) includeElement, formHandler));
        }
        return true;
    }

    private SubsetIncludeUi toSubsetIncludeUi(Thesaurus thesaurus) {
        ExtendedIncludeKey includeKey = ExtendedIncludeKey.newInstance(IncludeKey.newInstance(thesaurus.getSubsetKey()));
        return (SubsetIncludeUi) IncludeUiBuilder.initSubset(includeKey).toIncludeUi();
    }

}
