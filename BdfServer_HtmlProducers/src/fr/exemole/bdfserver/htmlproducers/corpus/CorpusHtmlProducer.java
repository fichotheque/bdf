/* BdfServer_HtmlProducers - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.commands.corpus.FieldOptionsCommand;
import fr.exemole.bdfserver.commands.corpus.FieldRemoveCommand;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.attributes.Deploy;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.BH;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class CorpusHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;
    private final boolean isSubsetAdmin;
    private final Button linkButton = Button.link().target(BdfHtmlConstants.EDITION_FRAME);

    public CorpusHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        this.corpus = corpus;
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        this.isSubsetAdmin = bdfParameters.getPermissionSummary().isSubsetAdmin(corpus.getSubsetKey());
    }

    @Override
    public void printHtml() {
        start(FichothequeUtils.getTitle(corpus, workingLang), true);
        this
                .__(BdfHtmlUtils.startSubsetUnit(this, bdfParameters, corpus, CorpusDomain.CORPUS_PAGE))
                .__(printCommands())
                .__(printInclude())
                .__(printOverview())
                .__(PageUnit.END);
        end();
    }


    private boolean printCommands() {
        if (!isSubsetAdmin) {
            return false;
        }
        boolean withRemoveField = (CorpusMetadataUtils.containsRemoveableField(corpus.getCorpusMetadata()));
        this
                .DETAILS(HA.id("details_commands").open(true).classes("tools-Details").populate(Deploy.DETAILS))
                .SUMMARY()
                .__localize("_ title.global.commands")
                ._SUMMARY()
                .DIV("tools-List")
                .__(link("action-Labels", CorpusDomain.CORPUS_PHRASESFORM_PAGE, "_ link.corpus.corpusphrasesform"))
                .__(link("action-FieldCreate", CorpusDomain.FIELD_CREATIONFORM_PAGE, "_ link.corpus.fieldcreationform"))
                .__(link("action-Options", CorpusDomain.FIELD_OPTIONSFORM_PAGE, FieldOptionsCommand.COMMANDKEY))
                .__if(withRemoveField, link("action-FieldRemove", CorpusDomain.FIELD_REMOVEFORM_PAGE, FieldRemoveCommand.COMMANDKEY))
                .__(link("action-ComponentPosition", CorpusDomain.UI_COMPONENTPOSITIONFORM_PAGE, "_ link.corpus.uicomponentpositionform"))
                .__(link("action-ComponentOptions", CorpusDomain.UI_COMPONENTOPTIONSFORM_PAGE, "_ link.corpus.uicomponentoptionsform"))
                .__(link("action-ComponentAttributes", CorpusDomain.UI_COMPONENTATTRIBUTESFORM_PAGE, "_ link.corpus.uicomponentattributesform"))
                .__(link("action-Advanced", CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE, "_ link.corpus.corpusadvancedcommands"))
                .__(link("action-History", CorpusDomain.REMOVEDLIST_PAGE, "_ link.corpus.removedlist"))
                ._DIV()
                ._DETAILS();
        return true;
    }

    private boolean printInclude() {
        if (!isSubsetAdmin) {
            return false;
        }
        boolean withExternalSources = (bdfServer.getContextObject(BdfServerConstants.FICHOTHEQUESHARING_CONTEXTOBJECT) != null);
        this
                .DETAILS(HA.id("details_include").open(true).classes("tools-Details").populate(Deploy.DETAILS))
                .SUMMARY()
                .__localize("_ title.corpus.include")
                ._SUMMARY()
                .DIV("tools-List")
                .__(link("action-Special", CorpusDomain.INCLUDE_SPECIAL_PAGE, "_ link.corpus.corpusinclude_specialform"))
                .__(link("action-Thesaurus", CorpusDomain.INCLUDE_THESAURUS_PAGE, "_ link.corpus.corpusinclude_thesaurusform"))
                .__(link("action-Corpus", CorpusDomain.INCLUDE_CORPUS_PAGE, "_ link.corpus.corpusinclude_corpusform"))
                .__(link("action-Album", CorpusDomain.INCLUDE_ALBUM_PAGE, "_ link.corpus.corpusinclude_albumform"))
                .__(link("action-Addenda", CorpusDomain.INCLUDE_ADDENDA_PAGE, "_ link.corpus.corpusinclude_addendaform"))
                .__(link("action-Comment", CorpusDomain.INCLUDE_COMMENT_PAGE, "_ link.corpus.corpusinclude_commentform"))
                .__if(withExternalSources, link("action-ExternalSource", CorpusDomain.INCLUDE_EXTERNALSOURCE_PAGE, "_ link.corpus.corpusinclude_externalsourceform"))
                ._DIV()
                ._DETAILS();
        return true;
    }

    private boolean printOverview() {
        this
                .DETAILS(HA.id("details_overview").open(true).classes("tools-Details").populate(Deploy.DETAILS))
                .SUMMARY()
                .__localize("_ title.corpus.overview")
                ._SUMMARY()
                .DIV("tools-List")
                .__(link("action-Diagrams", CorpusDomain.CORPUS_DIAGRAM_PAGE, "_ link.corpus.diagram"))
                .__(link("action-Conf", CorpusDomain.CORPUS_CONF_PAGE, "_ link.corpus.conf"))
                ._DIV()
                ._DETAILS();
        return true;
    }

    private Button link(String action, String pageName, String messageKey) {
        String href = BH.domain(Domains.CORPUS).subset(corpus).page(pageName).toString();
        return linkButton.href(href).action(action).textL10nObject(messageKey);
    }

}
