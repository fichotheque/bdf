/* BdfServer_HtmlProducers - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class UiComponentAttributesFormHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public UiComponentAttributesFormHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("corpus.css");
        this.corpus = corpus;
    }

    @Override
    public void printHtml() {
        UiComponents uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, CorpusDomain.UI_COMPONENTATTRIBUTESFORM_PAGE);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, CorpusDomain.UI_COMPONENTATTRIBUTESFORM_PAGE, bdfServer);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CORPUS)
                .family("CRP")
                .veil(true)
                .page(CorpusDomain.UI_COMPONENTATTRIBUTESFORM_PAGE);
        CorpusCommandBoxUtils.printUiComponentAttributesBox(this, commandBox, corpus, uiComponents, bdfServer, workingLang);
        end();
    }

}
