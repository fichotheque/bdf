/* BdfServer_HtmlProducers - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlConstants;


/**
 *
 * @author Vincent Calame
 */
public class CorpusDiagramHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public CorpusDiagramHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        this.corpus = corpus;
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addThemeCss("diagram.css", "corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, CorpusDomain.CORPUS_DIAGRAM_PAGE);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, CorpusDomain.CORPUS_DIAGRAM_PAGE, bdfServer);
        printCommandMessageUnit();
        this
                .__(PageUnit.start("action-Diagrams", "_ link.corpus.diagram"))
                .__(printDiagram())
                .__(PageUnit.END);
    }

    private boolean printDiagram() {
        String src = "diagrams/" + corpus.getSubsetKeyString();
        this
                .DIV("diagram-Corpus")
                .DIV("diagram-Diagram")
                .IMG(HA.src(src + ".png"))
                ._DIV()
                .P("diagram-Links")
                .A(HA.href(src + ".png").target(HtmlConstants.BLANK_TARGET))
                .__escape("png")
                ._A()
                .__dash()
                .A(HA.href(src + ".svg").target(HtmlConstants.BLANK_TARGET))
                .__escape("svg")
                ._A()
                .__dash()
                .A(HA.href(src + ".puml").target(HtmlConstants.BLANK_TARGET))
                .__escape("puml")
                ._A()
                ._P()
                ._DIV();
        return true;
    }

}
