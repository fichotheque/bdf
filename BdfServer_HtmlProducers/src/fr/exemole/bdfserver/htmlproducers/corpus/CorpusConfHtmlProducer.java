/* BdfServer_HtmlProducers - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.overview.CorpusOverview;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class CorpusConfHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public CorpusConfHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        this.corpus = corpus;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, CorpusDomain.CORPUS_CONF_PAGE);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, CorpusDomain.CORPUS_CONF_PAGE, bdfServer);
        printCommandMessageUnit();
        this
                .__(PageUnit.start("action-Conf", "_ link.corpus.conf"))
                .__(printConf())
                .__(PageUnit.END);
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CORPUS)
                .family("CRP")
                .veil(true)
                .page(CorpusDomain.CORPUS_CONF_PAGE);
        CorpusCommandBoxUtils.printConfChangeBox(this, commandBox, corpus);
    }

    private boolean printConf() {
        this
                .TEXTAREA(HA.classes("corpus-conf-Block").cols(60).rows(50).attr("data-codemirror-mode", "properties").disabled(true))
                .__escape(CorpusOverview.run(bdfServer, corpus), true)
                ._TEXTAREA();
        return true;
    }

}
