/* BdfServer_HtmlProducers - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.commands.corpus.FieldOptionsCommand;
import fr.exemole.bdfserver.commands.corpus.FieldRemoveCommand;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.tools.BH;
import java.util.Locale;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusHtmlUtils {

    private CorpusHtmlUtils() {
    }

    public static boolean printCorpusToolbar(HtmlPrinter hp, Corpus corpus, String currentPage, BdfServer bdfServer) {
        boolean withExternalSources = (bdfServer.getContextObject(BdfServerConstants.FICHOTHEQUESHARING_CONTEXTOBJECT) != null);
        hp
                .NAV("subset-Toolbar")
                .__(printCommands(hp, corpus, currentPage))
                .__(printInclude(hp, corpus, currentPage, withExternalSources))
                .__(printOverview(hp, corpus, currentPage))
                ._NAV();
        return true;
    }

    private static boolean printCommands(HtmlPrinter hp, Corpus corpus, String currentPage) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        boolean withRemoveField = (CorpusMetadataUtils.containsRemoveableField(corpus.getCorpusMetadata()));
        hp
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-Labels", corpus, CorpusDomain.CORPUS_PHRASESFORM_PAGE, "_ link.corpus.corpusphrasesform", currentPage))
                .__(link(button, "action-FieldCreate", corpus, CorpusDomain.FIELD_CREATIONFORM_PAGE, "_ link.corpus.fieldcreationform", currentPage))
                .__(link(button, "action-Options", corpus, CorpusDomain.FIELD_OPTIONSFORM_PAGE, FieldOptionsCommand.COMMANDKEY, currentPage))
                .__if(withRemoveField, link(button, "action-FieldRemove", corpus, CorpusDomain.FIELD_REMOVEFORM_PAGE, FieldRemoveCommand.COMMANDKEY, currentPage))
                .__(link(button, "action-ComponentPosition", corpus, CorpusDomain.UI_COMPONENTPOSITIONFORM_PAGE, "_ link.corpus.uicomponentpositionform", currentPage))
                .__(link(button, "action-ComponentOptions", corpus, CorpusDomain.UI_COMPONENTOPTIONSFORM_PAGE, "_ link.corpus.uicomponentoptionsform", currentPage))
                .__(link(button, "action-ComponentAttributes", corpus, CorpusDomain.UI_COMPONENTATTRIBUTESFORM_PAGE, "_ link.corpus.uicomponentattributesform", currentPage))
                .__(link(button, "action-Advanced", corpus, CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE, "_ link.corpus.corpusadvancedcommands", currentPage))
                .__(link(button, "action-History", corpus, CorpusDomain.REMOVEDLIST_PAGE, "_ link.corpus.removedlist", currentPage))
                .__(refresh(button, corpus))
                ._DIV();
        return true;
    }

    private static boolean printInclude(HtmlPrinter hp, Corpus corpus, String currentPage, boolean withExternalSources) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.corpus.include")
                .__colon()
                ._SPAN()
                .__(link(button, "action-Special", corpus, CorpusDomain.INCLUDE_SPECIAL_PAGE, "_ link.corpus.corpusinclude_specialform", currentPage))
                .__(link(button, "action-Thesaurus", corpus, CorpusDomain.INCLUDE_THESAURUS_PAGE, "_ link.corpus.corpusinclude_thesaurusform", currentPage))
                .__(link(button, "action-Corpus", corpus, CorpusDomain.INCLUDE_CORPUS_PAGE, "_ link.corpus.corpusinclude_corpusform", currentPage))
                .__(link(button, "action-Album", corpus, CorpusDomain.INCLUDE_ALBUM_PAGE, "_ link.corpus.corpusinclude_albumform", currentPage))
                .__(link(button, "action-Addenda", corpus, CorpusDomain.INCLUDE_ADDENDA_PAGE, "_ link.corpus.corpusinclude_addendaform", currentPage))
                .__(link(button, "action-Comment", corpus, CorpusDomain.INCLUDE_COMMENT_PAGE, "_ link.corpus.corpusinclude_commentform", currentPage))
                .__if(withExternalSources, link(button, "action-ExternalSource", corpus, CorpusDomain.INCLUDE_EXTERNALSOURCE_PAGE, "_ link.corpus.corpusinclude_externalsourceform", currentPage))
                ._DIV();
        return true;
    }

    private static boolean printOverview(HtmlPrinter hp, Corpus corpus, String currentPage) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.corpus.overview")
                .__colon()
                ._SPAN()
                .__(link(button, "action-Diagrams", corpus, CorpusDomain.CORPUS_DIAGRAM_PAGE, "_ link.corpus.diagram", currentPage))
                .__(link(button, "action-Conf", corpus, CorpusDomain.CORPUS_CONF_PAGE, "_ link.corpus.conf", currentPage))
                ._DIV();
        return true;
    }

    public static boolean printFicheHeader(HtmlPrinter hp, FicheMeta ficheMeta, Lang workingLang, Locale formatLocale) {
        SubsetItem masterItem = null;
        Subset masterSubset = ficheMeta.getCorpus().getMasterSubset();
        if (masterSubset != null) {
            masterItem = masterSubset.getSubsetItemById(ficheMeta.getId());
        }
        String title = CorpusMetadataUtils.getFicheTitle(ficheMeta, workingLang, formatLocale);
        hp
                .HEADER("subset-ItemHeader")
                .__(BdfHtmlUtils.printFicheTitle(hp, title, ficheMeta.isDiscarded(), masterItem, workingLang, formatLocale))
                ._HEADER();
        return true;
    }

    public static boolean printFicheHeader(HtmlPrinter hp, String title, boolean discarded, SubsetItem masterItem, Lang workingLang, Locale formatLocale) {
        hp
                .HEADER("subset-ItemHeader")
                .__(BdfHtmlUtils.printFicheTitle(hp, title, discarded, masterItem, workingLang, formatLocale))
                ._HEADER();
        return true;
    }

    public static String getFicheItemMessageKey(short ficheItemType) {
        switch (ficheItemType) {
            case CorpusField.PERSONNE_FIELD:
                return "_ label.corpus.ficheitemtype_personne";
            case CorpusField.LANGUE_FIELD:
                return "_ label.corpus.ficheitemtype_langue";
            case CorpusField.DATATION_FIELD:
                return "_ label.corpus.ficheitemtype_datation";
            case CorpusField.PAYS_FIELD:
                return "_ label.corpus.ficheitemtype_pays";
            case CorpusField.COURRIEL_FIELD:
                return "_ label.corpus.ficheitemtype_courriel";
            case CorpusField.LINK_FIELD:
                return "_ label.corpus.ficheitemtype_link";
            case CorpusField.ITEM_FIELD:
                return "_ label.corpus.ficheitemtype_item";
            case CorpusField.NOMBRE_FIELD:
                return "_ label.corpus.ficheitemtype_nombre";
            case CorpusField.MONTANT_FIELD:
                return "_ label.corpus.ficheitemtype_montant";
            case CorpusField.GEOPOINT_FIELD:
                return "_ label.corpus.ficheitemtype_geopoint";
            case CorpusField.PARA_FIELD:
                return "_ label.corpus.ficheitemtype_para";
            case CorpusField.IMAGE_FIELD:
                return "_ label.corpus.ficheitemtype_image";
            default:
                throw new SwitchException("Unknown type: " + ficheItemType);
        }
    }

    private static Button link(Button button, String action, Corpus corpus, String page, String titleLocKey, String currentPage) {
        if (currentPage.equals(page)) {
            return button.current(true).href(null).action(action).tooltip(null);
        } else {
            return button.current(false).href(BH.domain(Domains.CORPUS).subset(corpus).page(page)).action(action).tooltipMessage(titleLocKey);
        }
    }

    private static Button refresh(Button button, Corpus corpus) {
        return button.current(false)
                .href(BH.domain(Domains.CORPUS).page(CorpusDomain.CORPUS_PAGE).subset(corpus))
                .action("action-Refresh")
                .tooltipMessage("_ link.global.reload")
                .target(BdfHtmlConstants.LIST_FRAME);
    }

}
