/* BdfServer_HtmlProducers - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus.include;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.ui.CommentUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.LangRows;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusCommandBoxUtils;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;


/**
 *
 * @author Vincent Calame
 */
public class CommentIncludeHtmlProducer extends AbstractIncludeHtmlProducer {

    public CommentIncludeHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters, corpus, CorpusDomain.INCLUDE_COMMENT_PAGE);
    }

    @Override
    public void printHtml() {
        Lang[] langArray = LangsUtils.toArray(bdfServer.getLangConfiguration().getWorkingLangs());
        commonStart();
        printList(langArray);
        CorpusCommandBoxUtils.printCommentCreationBox(this, initCommandBox(), corpus, langArray);
        end();
    }

    private void printList(Lang[] langArray) {
        UiComponents uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        List<CommentUi> list = UiUtils.getCommentUiList(uiComponents);
        CommandBox commandBox = initCommandBox()
                .mode(InteractionConstants.SUBUNIT_MODE);
        if (list.size() > 0) {
            this
                    .__(PageUnit.start("action-Comment", "_ title.corpus.comment_list"))
                    .DL("global-DL");
            for (CommentUi commentUi : list) {
                this
                        .DT()
                        .__(BdfHtmlUtils.printCodeMirrorSpan(this, commentUi, workingLang))
                        ._DT()
                        .DD()
                        .__(printCommentChangeDetails(commandBox, corpus, commentUi, langArray))
                        .__(printCommentRemoveDetails(commandBox, corpus, commentUi))
                        ._DD();
            }
            this
                    ._DL()
                    .__(PageUnit.END);
        }

    }

    private boolean printCommentChangeDetails(CommandBox commandBox, Corpus corpus, CommentUi commentUi, Lang[] workingLangArray) {
        String[] htmlArray = UiUtils.toStringArray(commentUi, workingLangArray);
        HtmlPrinter hp = this;
        this
                .P("global-SubTitle")
                .__localize("_ label.corpus.commentlocation")
                .__colon()
                .__(() -> {
                    boolean suivant = false;
                    if (commentUi.isForm()) {
                        suivant = true;
                        hp
                                .__localize("_ label.corpus.commentlocation_form");
                    }
                    if (commentUi.isTemplate()) {
                        if (suivant) {
                            hp
                                    .__escape(", ");
                        }
                        hp
                                .__localize("_ label.corpus.commentlocation_template");
                    }
                })
                ._P()
                .__(Grid.START)
                .__(LangRows.init("", workingLangArray, htmlArray).displayOnly(true).preformatted(true))
                .__(Grid.END)
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.change")
                ._SUMMARY()
                .__(CorpusCommandBoxUtils.printCommentChangeBox(hp, commandBox, corpus, commentUi, workingLangArray))
                ._DETAILS();
        return true;
    }

    private boolean printCommentRemoveDetails(CommandBox commandBox, Corpus corpus, CommentUi commentUi) {
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.delete")
                ._SUMMARY()
                .__(CorpusCommandBoxUtils.printCommentRemoveBox(this, commandBox, corpus, commentUi))
                ._DETAILS();
        return true;
    }

}
