/* BdfServer_HtmlProducers - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus.include;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.ui.IncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.LangRows;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusCommandBoxUtils;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusHtmlUtils;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractIncludeHtmlProducer extends BdfServerHtmlProducer {

    protected final PermissionSummary permissionSummary;
    protected final Corpus corpus;
    protected final String currentPage;

    public AbstractIncludeHtmlProducer(BdfParameters bdfParameters, Corpus corpus, String currentPage) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.corpus = corpus;
        this.currentPage = currentPage;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("corpus.css");
    }

    public void commonStart() {
        super.start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, currentPage);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, currentPage, bdfServer);
        printCommandMessageUnit();
    }

    public CommandBox initCommandBox() {
        return CommandBox.init()
                .action(Domains.CORPUS)
                .family("CRP")
                .veil(true)
                .page(currentPage);
    }

    public boolean printIncludeChangeDetails(CommandBox commandBox, IncludeUi includeUi) {
        Langs workingLangs = bdfServer.getLangConfiguration().getWorkingLangs();
        Labels labels = includeUi.getCustomLabels();
        if (labels == null) {
            this
                    .P("global-SubTitle")
                    .__localize("_ info.corpus.defaultlabelusage")
                    ._P();
        } else {
            this
                    .__(Grid.START)
                    .__(LangRows.init("", labels, workingLangs).displayOnly(true))
                    .__(Grid.END);
        }
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.change")
                ._SUMMARY()
                .__(CorpusCommandBoxUtils.printIncludeChangeBox(this, commandBox, corpus, includeUi, workingLangs))
                ._DETAILS();
        return true;
    }

    public boolean printComponentRemoveDetails(CommandBox commandBox, UiComponent component) {
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.delete")
                ._SUMMARY()
                .__(CorpusCommandBoxUtils.printComponentRemoveBox(this, commandBox, corpus, component))
                ._DETAILS();
        return true;
    }

}
