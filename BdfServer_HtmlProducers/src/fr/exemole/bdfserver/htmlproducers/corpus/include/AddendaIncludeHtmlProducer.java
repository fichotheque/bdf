/* BdfServer_HtmlProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus.include;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusCommandBoxUtils;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class AddendaIncludeHtmlProducer extends AbstractIncludeHtmlProducer {


    public AddendaIncludeHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters, corpus, CorpusDomain.INCLUDE_ADDENDA_PAGE);
    }

    @Override
    public void printHtml() {
        commonStart();
        printList();
        CorpusCommandBoxUtils.printSubsetIncludeCreationBox(this, initCommandBox(), corpus, bdfServer, workingLang, SubsetKey.CATEGORY_ADDENDA);
        end();
    }

    private void printList() {
        UiComponents uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        List<SubsetIncludeUi> list = UiUtils.getSubsetIncludeUiList(uiComponents, SubsetKey.CATEGORY_ADDENDA);
        CommandBox commandBox = initCommandBox()
                .mode(InteractionConstants.SUBUNIT_MODE);
        this
                .__(PageUnit.start("action-Addenda", "_ title.corpus.addendainclude_list"));
        if (list.isEmpty()) {
            this
                    .P()
                    .__localize("_ info.corpus.empty_addendainclude")
                    ._P();
        } else {
            this
                    .DL("global-DL");
            for (SubsetIncludeUi includeUi : list) {
                this
                        .DT()
                        .__(BdfHtmlUtils.printCodeMirrorSpan(this, includeUi, bdfServer, workingLang))
                        ._DT()
                        .DD()
                        .__(printIncludeChangeDetails(commandBox, includeUi))
                        .__(printComponentRemoveDetails(commandBox, includeUi))
                        ._DD();
            }
            this
                    ._DL();
        }
        this
                .__(PageUnit.END);
    }

}
