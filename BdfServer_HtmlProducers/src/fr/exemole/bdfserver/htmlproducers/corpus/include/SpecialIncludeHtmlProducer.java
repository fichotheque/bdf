/* BdfServer_HtmlProducers - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus.include;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.ui.SpecialIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.SelectOption;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusCommandBoxUtils;
import fr.exemole.bdfserver.tools.L10nUtils;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 *
 * @author Vincent Calame
 */
public class SpecialIncludeHtmlProducer extends AbstractIncludeHtmlProducer {

    public SpecialIncludeHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters, corpus, CorpusDomain.INCLUDE_SPECIAL_PAGE);
    }

    @Override
    public void printHtml() {
        UiComponents uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        Check check = check(uiComponents);
        commonStart();
        printList(check);
        if (!check.isAvailableEmpty()) {
            List<SelectOption> list = check.getAvailableOptionList();
            String firstName = list.get(0).value();
            CorpusCommandBoxUtils.printSpecialIncludeAddBox(this, initCommandBox(), corpus, list, firstName);
        }
        end();
    }

    private void printList(Check check) {
        CommandBox commandBox = initCommandBox()
                .mode(InteractionConstants.SUBUNIT_MODE);
        this
                .__(PageUnit.start("action-Special", "_ link.corpus.corpusinclude_specialform"));
        if (check.isExistingEmpty()) {
            this
                    .P()
                    .__localize("_ info.corpus.empty_specialinclude")
                    .P();
        } else {
            this
                    .DL("global-DL");
            for (SpecialIncludeUi includeUi : check.existingList) {
                this
                        .DT()
                        .__(BdfHtmlUtils.printCodeMirrorSpan(this, includeUi, bdfServer, workingLang))
                        ._DT()
                        .DD()
                        .__(printIncludeChangeDetails(commandBox, includeUi))
                        .__(printComponentRemoveDetails(commandBox, includeUi))
                        ._DD();
            }
            this
                    ._DL()
                    .__(PageUnit.END);
        }
        this
                .__(PageUnit.END);
    }

    private Check check(UiComponents uiComponents) {
        Check check = new Check(uiComponents);
        check.check(FichothequeConstants.LIAGE_NAME);
        if (corpus.getMasterSubset() != null) {
            check.check(FichothequeConstants.PARENTAGE_NAME);
        } else {
            List<Corpus> satelliteCorpusList = corpus.getSatelliteCorpusList();
            if (!satelliteCorpusList.isEmpty()) {
                check.check(FichothequeConstants.PARENTAGE_NAME);
            }
        }
        return check;
    }


    private class Check {

        private final UiComponents uiComponents;
        private final List<SpecialIncludeUi> existingList = new ArrayList<SpecialIncludeUi>();
        private final List<String> availableList = new ArrayList<String>();

        private Check(UiComponents uiComponents) {
            this.uiComponents = uiComponents;
        }

        private void check(String name) {
            UiComponent uiComponent = uiComponents.getUiComponent(name);
            if (uiComponent != null) {
                existingList.add((SpecialIncludeUi) uiComponent);
            } else {
                availableList.add(name);
            }
        }

        private boolean isExistingEmpty() {
            return existingList.isEmpty();
        }

        private boolean isAvailableEmpty() {
            return availableList.isEmpty();
        }

        private List<SelectOption> getAvailableOptionList() {
            MessageLocalisation messageLocalisation = getMessageLocalisation();
            List<SelectOption> list = new ArrayList<SelectOption>();
            for (String name : availableList) {
                String title = messageLocalisation.toString(L10nUtils.getMessageKey(name));
                if (title == null) {
                    title = "[" + name + "]";
                } else {
                    title = "[" + name + "] " + title;
                }
                list.add(SelectOption.init(name).text(title));
            }
            return list;
        }

    }

}
