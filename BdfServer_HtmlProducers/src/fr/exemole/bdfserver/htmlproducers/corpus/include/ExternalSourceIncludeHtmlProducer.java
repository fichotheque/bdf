/* BdfServer_HtmlProducers - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus.include;

import fr.exemole.bdfserver.api.externalsource.ExternalSourceType;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.ui.DataUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.LangRows;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusCommandBoxUtils;
import fr.exemole.bdfserver.tools.externalsource.CoreExternalSourceCatalog;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.Litteral;


/**
 *
 * @author Vincent Calame
 */
public class ExternalSourceIncludeHtmlProducer extends AbstractIncludeHtmlProducer {


    public ExternalSourceIncludeHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters, corpus, CorpusDomain.INCLUDE_EXTERNALSOURCE_PAGE);
    }

    @Override
    public void printHtml() {
        commonStart();
        printList();
        CorpusCommandBoxUtils.printDataCreationBox(this, initCommandBox(), corpus, bdfServer);
        end();
    }

    private void printList() {
        UiComponents uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        List<DataUi> list = UiUtils.getDataUiList(uiComponents);
        CommandBox commandBox = initCommandBox()
                .mode(InteractionConstants.SUBUNIT_MODE);
        this
                .__(PageUnit.start("action-ExternalSource", "_ title.corpus.externalsourceinclude_list"));
        if (list.isEmpty()) {
            this
                    .P()
                    .__localize("_ info.corpus.empty_externalsourceinclude")
                    .P();
        } else {
            this
                    .DL("global-DL");
            for (DataUi dataUi : list) {
                this
                        .DT()
                        .__(BdfHtmlUtils.printCodeMirrorSpan(this, dataUi, workingLang))
                        ._DT()
                        .DD()
                        .__(printDataChangeDetails(commandBox, dataUi))
                        .__(printComponentRemoveDetails(commandBox, dataUi))
                        ._DD();
            }
            this
                    ._DL();
        }
        this
                .__(PageUnit.END);
    }

    private boolean printDataChangeDetails(CommandBox commandBox, DataUi dataUi) {
        String type = dataUi.getExternalSourceDef().getType();
        Langs workingLangs = bdfServer.getLangConfiguration().getWorkingLangs();
        this
                .P("global-SubTitle")
                .__localize("_ label.corpus.externalsourcetype")
                .__colon()
                .__localize(getL10nObject(type))
                ._P();
        this
                .__(Grid.START)
                .__(LangRows.init("", dataUi.getLabels(), workingLangs).displayOnly(true))
                .__(Grid.END);
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.change")
                ._SUMMARY()
                .__(CorpusCommandBoxUtils.printDataChangeBox(this, commandBox, corpus, dataUi, workingLangs))
                ._DETAILS();
        return true;
    }


    private static Object getL10nObject(String type) {
        ExternalSourceType externalSourceType = CoreExternalSourceCatalog.getExternalSourceType(type);
        if (externalSourceType != null) {
            return externalSourceType.getL10nObject();
        } else {
            return new Litteral(type);
        }
    }


}
