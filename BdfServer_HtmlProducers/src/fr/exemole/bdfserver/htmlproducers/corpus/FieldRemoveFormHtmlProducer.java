/* BdfServer_HtmlProducers - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class FieldRemoveFormHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public FieldRemoveFormHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        this.corpus = corpus;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addThemeCss("corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, CorpusDomain.FIELD_REMOVEFORM_PAGE);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, CorpusDomain.FIELD_REMOVEFORM_PAGE, bdfServer);
        printCommandMessageUnit();
        if (CorpusMetadataUtils.containsRemoveableField(corpus.getCorpusMetadata())) {
            CommandBox commandBox = CommandBox.init()
                    .action(Domains.CORPUS)
                    .family("CRP")
                    .veil(true)
                    .page(CorpusDomain.FIELD_REMOVEFORM_PAGE);
            CorpusCommandBoxUtils.printFieldRemoveBox(this, commandBox, corpus, workingLang);
        }
        end();
    }

}
