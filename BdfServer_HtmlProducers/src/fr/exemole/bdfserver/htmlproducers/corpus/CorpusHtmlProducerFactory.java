/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.ficheform.FormElementProvider;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.managers.UiManager;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.corpus.include.AddendaIncludeHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.corpus.include.AlbumIncludeHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.corpus.include.CommentIncludeHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.corpus.include.CorpusIncludeHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.corpus.include.ExternalSourceIncludeHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.corpus.include.SpecialIncludeHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.corpus.include.ThesaurusIncludeHtmlProducer;
import fr.exemole.bdfserver.tools.ficheform.FicheFormParametersBuilder;
import fr.exemole.bdfserver.tools.ficheform.FormElementProviderFactory;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.tools.duplication.SubsetMatch;
import net.fichotheque.tools.reponderation.ReponderationLog;
import net.fichotheque.utils.pointeurs.PointeurFactory;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusHtmlProducerFactory {

    private final static int CORPUS_ADMIN = 1;
    private final static int FICHE = 3;

    private CorpusHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        BdfServer bdfServer = parameters.getBdfServer();
        RequestMap requestMap = parameters.getRequestMap();
        switch (getPageType(page)) {
            case CORPUS_ADMIN: {
                Corpus corpus = requestHandler.getCorpus();
                parameters.checkSubsetAdmin(corpus);
                switch (page) {
                    case CorpusDomain.CORPUS_CONF_PAGE: {
                        return new CorpusConfHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.CORPUS_PAGE: {
                        return new CorpusHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE: {
                        return new CorpusAdvancedCommandsHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.CORPUS_DIAGRAM_PAGE: {
                        return new CorpusDiagramHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.CORPUS_PHRASESFORM_PAGE: {
                        return new CorpusPhrasesFormHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.FIELD_CREATIONFORM_PAGE: {
                        return new FieldCreationFormHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.FIELD_OPTIONSFORM_PAGE: {
                        return new FieldOptionsFormHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.FIELD_REMOVEFORM_PAGE: {
                        return new FieldRemoveFormHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.INCLUDE_ADDENDA_PAGE: {
                        return new AddendaIncludeHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.INCLUDE_ALBUM_PAGE: {
                        return new AlbumIncludeHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.INCLUDE_COMMENT_PAGE: {
                        return new CommentIncludeHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.INCLUDE_CORPUS_PAGE: {
                        return new CorpusIncludeHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.INCLUDE_EXTERNALSOURCE_PAGE: {
                        return new ExternalSourceIncludeHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.INCLUDE_SPECIAL_PAGE: {
                        return new SpecialIncludeHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.INCLUDE_THESAURUS_PAGE: {
                        return new ThesaurusIncludeHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.REMOVEDLIST_PAGE: {
                        return new RemovedListHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.UI_COMPONENTATTRIBUTESFORM_PAGE: {
                        return new UiComponentAttributesFormHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.UI_COMPONENTOPTIONSFORM_PAGE: {
                        return new UiComponentOptionsFormHtmlProducer(parameters, corpus);
                    }
                    case CorpusDomain.UI_COMPONENTPOSITIONFORM_PAGE: {
                        return new UiComponentPositionFormHtmlProducer(parameters, corpus);
                    }
                    default:
                        return null;
                }
            }
            case FICHE: {
                FicheMeta ficheMeta = requestHandler.getFicheMeta();
                switch (page) {
                    case CorpusDomain.FICHE_ADVANCEDCOMMANDS_PAGE: {
                        parameters.checkSubsetAdmin(ficheMeta.getCorpus());
                        return new FicheAdvancedCommandsHtmlProducer(parameters, ficheMeta);
                    }
                    case CorpusDomain.FICHE_REMOVEFORM_PAGE: {
                        parameters.checkSubsetAdmin(ficheMeta.getCorpus());
                        return new FicheRemoveFormHtmlProducer(parameters, ficheMeta);
                    }
                    case CorpusDomain.FICHE_DISCARDFORM_PAGE: {
                        parameters.checkWrite(ficheMeta);
                        return new FicheDiscardFormHtmlProducer(parameters, ficheMeta);
                    }
                    case CorpusDomain.FICHE_RETRIEVEFORM_PAGE: {
                        parameters.checkWrite(ficheMeta);
                        return new FicheRetrieveFormHtmlProducer(parameters, ficheMeta);
                    }
                    case CorpusDomain.FICHE_ADDENDA_PAGE: {
                        parameters.checkWrite(ficheMeta);
                        FichePointeur fichePointeur = toFichePointeur(ficheMeta);
                        FormElementProvider formElementProvider = getFormElementProvider(parameters);
                        UiComponents uiComponents = getUiComponents(bdfServer, requestMap, ficheMeta.getCorpus());
                        return new FicheAddendaHtmlProducer(parameters, fichePointeur, formElementProvider, uiComponents);
                    }
                    case CorpusDomain.FICHE_ALBUM_PAGE: {
                        parameters.checkWrite(ficheMeta);
                        FichePointeur fichePointeur = toFichePointeur(ficheMeta);
                        FormElementProvider formElementProvider = getFormElementProvider(parameters);
                        UiComponents uiComponents = getUiComponents(bdfServer, requestMap, ficheMeta.getCorpus());
                        return new FicheAlbumHtmlProducer(parameters, fichePointeur, formElementProvider, uiComponents);
                    }
                    default:
                        return null;
                }
            }
            default: {
                switch (page) {
                    case CorpusDomain.CORPUS_CREATIONFORM_PAGE: {
                        parameters.checkFichothequeAdmin();
                        return new CorpusCreationFormHtmlProducer(parameters);
                    }
                    case CorpusDomain.DUPLICATIONLOG_PAGE: {
                        SubsetMatch subsetMatch = (SubsetMatch) parameters.getResultObject(BdfInstructionConstants.SUBSETMATCH_OBJ);
                        return new DuplicationLogHtmlProducer(parameters, subsetMatch);
                    }
                    case CorpusDomain.REPONDERATIONLOG_PAGE: {
                        ReponderationLog reponderationLog = (ReponderationLog) parameters.getResultObject(BdfInstructionConstants.REPONDERATIONLOG_OBJ);
                        return new ReponderationLogHtmlProducer(parameters, reponderationLog);
                    }
                    default:
                        return null;
                }
            }

        }
    }

    private static int getPageType(String page) {
        switch (page) {
            case CorpusDomain.CORPUS_PAGE:
            case CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE:
            case CorpusDomain.CORPUS_CONF_PAGE:
            case CorpusDomain.CORPUS_DIAGRAM_PAGE:
            case CorpusDomain.CORPUS_PHRASESFORM_PAGE:
            case CorpusDomain.FIELD_CREATIONFORM_PAGE:
            case CorpusDomain.FIELD_OPTIONSFORM_PAGE:
            case CorpusDomain.FIELD_REMOVEFORM_PAGE:
            case CorpusDomain.INCLUDE_ADDENDA_PAGE:
            case CorpusDomain.INCLUDE_ALBUM_PAGE:
            case CorpusDomain.INCLUDE_COMMENT_PAGE:
            case CorpusDomain.INCLUDE_CORPUS_PAGE:
            case CorpusDomain.INCLUDE_EXTERNALSOURCE_PAGE:
            case CorpusDomain.INCLUDE_SPECIAL_PAGE:
            case CorpusDomain.INCLUDE_THESAURUS_PAGE:
            case CorpusDomain.UI_COMPONENTATTRIBUTESFORM_PAGE:
            case CorpusDomain.UI_COMPONENTOPTIONSFORM_PAGE:
            case CorpusDomain.UI_COMPONENTPOSITIONFORM_PAGE:
            case CorpusDomain.REMOVEDLIST_PAGE:
                return CORPUS_ADMIN;
            case CorpusDomain.FICHE_ADVANCEDCOMMANDS_PAGE:
            case CorpusDomain.FICHE_REMOVEFORM_PAGE:
            case CorpusDomain.FICHE_DISCARDFORM_PAGE:
            case CorpusDomain.FICHE_RETRIEVEFORM_PAGE:
            case CorpusDomain.FICHE_ADDENDA_PAGE:
            case CorpusDomain.FICHE_ALBUM_PAGE:
                return FICHE;
            default:
                return 0;
        }
    }

    private static UiComponents getUiComponents(BdfServer bdfServer, RequestMap requestMap, Corpus corpus) {
        UiManager uiManager = bdfServer.getUiManager();
        return uiManager.getMainUiComponents(corpus);
    }

    private static FichePointeur toFichePointeur(FicheMeta ficheMeta) {
        FichePointeur fichePointeur = PointeurFactory.newFichePointeur(ficheMeta.getCorpus());
        fichePointeur.setCurrentSubsetItem(ficheMeta);
        return fichePointeur;
    }

    private static FormElementProvider getFormElementProvider(BdfParameters bdfParameters) {
        return FormElementProviderFactory.newInstance(FicheFormParametersBuilder.build(bdfParameters)
                .toFicheFormParameters());
    }

}
