/* BdfServer_HtmlProducers - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class CorpusAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public CorpusAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        this.corpus = corpus;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE, bdfServer);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CORPUS)
                .family("CRP")
                .veil(true);
        printCorpusRemoveBox(commandBox);
        printCorpusAttributeChangeBox(commandBox);
        printDuplicationBox(commandBox);
        printReloadBox(commandBox);
        printCloneBox(commandBox);
        printConversionBox(commandBox);
        printReponderationBox(commandBox);
        printImageToIllustrationBox(commandBox);
        printSectionMergeBox(commandBox);
        end();
    }

    private boolean printCorpusRemoveBox(CommandBox commandBox) {
        if (!bdfParameters.isFichothequeAdmin()) {
            return false;
        }
        if (!corpus.isRemoveable()) {
            return false;
        }
        commandBox
                .errorPage(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE)
                .page(InteractionConstants.MESSAGE_PAGE);
        CorpusCommandBoxUtils.printCorpusRemoveBox(this, commandBox, corpus);
        return true;
    }

    private boolean printCorpusAttributeChangeBox(CommandBox commandBox) {
        commandBox
                .errorPage(null)
                .page(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE);
        CorpusCommandBoxUtils.printCorpusAttributeChangeBox(this, commandBox, corpus);
        return true;
    }

    private boolean printDuplicationBox(CommandBox commandBox) {
        commandBox
                .errorPage(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE)
                .page(CorpusDomain.DUPLICATIONLOG_PAGE);
        CorpusCommandBoxUtils.printDuplicationBox(this, commandBox, corpus, bdfServer, workingLang);
        return true;
    }

    private boolean printReloadBox(CommandBox commandBox) {
        commandBox
                .errorPage(null)
                .page(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE);
        CorpusCommandBoxUtils.printReloadBox(this, commandBox, corpus);
        return true;
    }

    private boolean printCloneBox(CommandBox commandBox) {
        if (!bdfParameters.isFichothequeAdmin()) {
            return false;
        }
        commandBox
                .errorPage(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE)
                .page(CorpusDomain.CORPUS_PHRASESFORM_PAGE);
        CorpusCommandBoxUtils.printCloneBox(this, commandBox, corpus, bdfServer, workingLang);
        return true;
    }

    private boolean printConversionBox(CommandBox commandBox) {
        commandBox
                .errorPage(null)
                .page(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE);
        CorpusCommandBoxUtils.printConversionBox(this, commandBox, corpus, workingLang);
        return true;
    }

    private boolean printReponderationBox(CommandBox commandBox) {
        commandBox
                .errorPage(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE)
                .page(CorpusDomain.REPONDERATIONLOG_PAGE);
        CorpusCommandBoxUtils.printReponderationBox(this, commandBox, corpus);
        return true;
    }

    private boolean printImageToIllustrationBox(CommandBox commandBox) {
        commandBox
                .errorPage(null)
                .page(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE);
        CorpusCommandBoxUtils.printImageToIllustrationBox(this, commandBox, corpus, bdfServer, workingLang);
        return true;
    }

    private boolean printSectionMergeBox(CommandBox commandBox) {
        if (corpus.getCorpusMetadata().getSectionList().size() < 2) {
            return false;
        }
        commandBox
                .errorPage(null)
                .page(CorpusDomain.CORPUS_ADVANCEDCOMMANDS_PAGE);
        CorpusCommandBoxUtils.printSectionMergeBox(this, commandBox, corpus, workingLang);
        return true;
    }

}
