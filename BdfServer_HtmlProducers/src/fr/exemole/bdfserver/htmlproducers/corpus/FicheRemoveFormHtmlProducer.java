/* BdfServer_HtmlProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class FicheRemoveFormHtmlProducer extends BdfServerHtmlProducer {

    private final PermissionSummary permissionSummary;
    private final FicheMeta ficheMeta;

    public FicheRemoveFormHtmlProducer(BdfParameters bdfParameters, FicheMeta ficheMeta) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.ficheMeta = ficheMeta;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        CorpusHtmlUtils.printFicheHeader(this, ficheMeta, workingLang, formatLocale);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CORPUS)
                .family("CRP")
                .veil(true);
        if (permissionSummary.isSubsetAdmin(ficheMeta.getSubsetKey())) {
            commandBox
                    .errorPage(CorpusDomain.FICHE_REMOVEFORM_PAGE)
                    .page(InteractionConstants.MESSAGE_PAGE);
            CorpusCommandBoxUtils.printFicheRemoveBox(this, commandBox, ficheMeta, workingLang, formatLocale);
        }
        end();
    }

}
