/* BdfServer_HtmlProducers - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import net.fichotheque.tools.reponderation.ReponderationLog;
import net.fichotheque.tools.reponderation.ReponderationParameters;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class ReponderationLogHtmlProducer extends BdfServerHtmlProducer {

    private final ReponderationLog reponderationLog;

    public ReponderationLogHtmlProducer(BdfParameters bdfParameters, ReponderationLog reponderationLog) {
        super(bdfParameters);
        this.reponderationLog = reponderationLog;
        addThemeCss("corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        this
                .__(PageUnit.start("_ title.corpus.reponderationlog"))
                .__(printCommandMessage());
        if (reponderationLog != null) {
            ReponderationParameters reponderationDef = reponderationLog.getReponderationParameters();
            int resultCount = reponderationLog.getResultCount();
            this
                    .TABLE();
            this
                    .TR();
            this
                    .TH()
                    .__escape(reponderationDef.getOriginSubsetKey())
                    ._TH();
            this
                    .TH()
                    .__escape(reponderationDef.getCroisementSubsetKey())
                    ._TH();
            this
                    ._TR();
            for (int i = 0; i < resultCount; i++) {
                ReponderationLog.ReponderationResult result = reponderationLog.getResult(i);
                this
                        .TR();
                this
                        .TD()
                        .__append(result.getOriginId())
                        ._TD();
                this
                        .TD()
                        .__append(result.getCroisementId())
                        ._TD();
                this
                        ._TR();
            }
            this
                    ._TABLE();
        }
        this
                .__(PageUnit.END);
        end();
    }

}
