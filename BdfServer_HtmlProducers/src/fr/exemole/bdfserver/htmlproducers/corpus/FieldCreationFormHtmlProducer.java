/* BdfServer_HtmlProducers - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class FieldCreationFormHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public FieldCreationFormHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        this.corpus = corpus;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, CorpusDomain.FIELD_CREATIONFORM_PAGE);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, CorpusDomain.FIELD_CREATIONFORM_PAGE, bdfServer);
        printCommandMessageUnit();
        this
                .DIV("corpus-fields-Grid")
                .DIV("corpus-fields-ListArea")
                .__(PageUnit.start("_ title.corpus.currentstate"))
                .__(printCurrentFields())
                .__(PageUnit.END)
                ._DIV()
                .DIV("corpus-fields-CommandArea");
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CORPUS)
                .family("CRP")
                .veil(true)
                .page(CorpusDomain.FIELD_CREATIONFORM_PAGE);
        CorpusMetadata corpusMetadata = corpus.getCorpusMetadata();
        CorpusCommandBoxUtils.printFieldCreationBox(this, commandBox, corpus, FieldKey.PROPRIETE_CATEGORY);
        CorpusCommandBoxUtils.printFieldCreationBox(this, commandBox, corpus, FieldKey.INFORMATION_CATEGORY);
        CorpusCommandBoxUtils.printFieldCreationBox(this, commandBox, corpus, FieldKey.SECTION_CATEGORY);
        if (!corpusMetadata.isWithSoustitre()) {
            CorpusCommandBoxUtils.printSoustitreActivationBox(this, commandBox, corpus);
        }
        this
                ._DIV()
                ._DIV();
        end();
    }

    private boolean printCurrentFields() {
        CorpusMetadata currentCorpusMetadata = corpus.getCorpusMetadata();
        CorpusField soustitreCorpusField = currentCorpusMetadata.getCorpusField(FieldKey.SOUSTITRE);
        List<CorpusField> proprieteList = currentCorpusMetadata.getProprieteList();
        List<CorpusField> informationList = currentCorpusMetadata.getInformationList();
        List<CorpusField> sectionList = currentCorpusMetadata.getSectionList();
        if ((soustitreCorpusField == null) && (proprieteList.isEmpty()) && (sectionList.isEmpty()) && (informationList.isEmpty())) {
            this
                    .P()
                    .__localize("_ info.corpus.onlymandatoryfields")
                    ._P();
            return true;
        }
        if (soustitreCorpusField != null) {
            this
                    .UL("corpus-fields-List")
                    .LI()
                    .__(BdfHtmlUtils.printCodeMirrorSpan(this, soustitreCorpusField, workingLang))
                    .__(printFicheItemType(CorpusField.PARA_FIELD))
                    ._LI()
                    ._UL();
        }
        this
                ._UL()
                .UL("corpus-fields-List");
        for (CorpusField corpusField : proprieteList) {
            this
                    .LI()
                    .__(BdfHtmlUtils.printCodeMirrorSpan(this, corpusField, workingLang))
                    .__(printFicheItemType(corpusField.getFicheItemType()))
                    ._LI();

        }
        this
                ._UL()
                .UL("corpus-fields-List");
        for (CorpusField corpusField : informationList) {
            this
                    .LI()
                    .__(BdfHtmlUtils.printCodeMirrorSpan(this, corpusField, workingLang))
                    .__(printFicheItemType(corpusField.getFicheItemType()))
                    ._LI();

        }
        this
                ._UL()
                .UL("corpus-fields-List");
        for (CorpusField corpusField : sectionList) {
            this
                    .LI()
                    .__(BdfHtmlUtils.printCodeMirrorSpan(this, corpusField, workingLang))
                    ._LI();
        }
        this
                ._UL();
        return true;
    }

    private boolean printFicheItemType(short itemType) {
        this
                .SPAN("corpus-fields-Type")
                .__localize(CorpusHtmlUtils.getFicheItemMessageKey(itemType))
                ._SPAN();
        return true;
    }

}
