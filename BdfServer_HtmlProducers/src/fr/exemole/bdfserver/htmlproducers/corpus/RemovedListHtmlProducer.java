/* BdfServer_HtmlProducers - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class RemovedListHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;

    public RemovedListHtmlProducer(BdfParameters bdfParameters, Corpus corpus) {
        super(bdfParameters);
        addThemeCss("restore.css");
        addJsLib(MiscJsLibs.RESTORE);
        this.corpus = corpus;
    }

    @Override
    public void printHtml() {

        SubsetKey subsetKey = corpus.getSubsetKey();
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("subsetKey", subsetKey.getKeyString())
                .put("subsetCategory", subsetKey.getCategoryString())
                .put("subsetName", subsetKey.getSubsetName());
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, corpus, CorpusDomain.CORPUS_PHRASESFORM_PAGE);
        CorpusHtmlUtils.printCorpusToolbar(this, corpus, CorpusDomain.REMOVEDLIST_PAGE, bdfServer);
        printCommandMessageUnit();
        this
                .SCRIPT()
                .__jsObject("Restore.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId))
                ._DIV();
        end();
    }

}
