/* BdfServer_HtmlProducers - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.BH;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class FicheAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final FicheMeta ficheMeta;

    public FicheAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, FicheMeta ficheMeta) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        this.ficheMeta = ficheMeta;
    }

    @Override
    public void printHtml() {
        start();
        CorpusHtmlUtils.printFicheHeader(this, ficheMeta, workingLang, formatLocale);
        printMessages();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.CORPUS)
                .family("CRP")
                .veil(true)
                .page(CorpusDomain.FICHE_ADVANCEDCOMMANDS_PAGE);
        CorpusCommandBoxUtils.printFicheAttributeChangeBox(this, commandBox, ficheMeta);
        CorpusCommandBoxUtils.printCroisementCopyBox(this, commandBox, bdfServer, ficheMeta, workingLang, formatLocale);
        end();
    }

    private boolean printMessages() {
        BdfCommandResult bdfCommandResult = getBdfCommandResult();
        if (bdfCommandResult != null) {
            Object obj = bdfCommandResult.getResultObject(BdfInstructionConstants.DESTINATION_OBJ);
            if ((obj != null) && (obj instanceof FicheMeta)) {
                return printMessages((FicheMeta) obj);
            }
        }
        return printCommandMessageUnit();
    }

    private boolean printMessages(FicheMeta destination) {
        __(PageUnit.SIMPLE, () -> {
            this
                    .__(printCommandMessage())
                    .P()
                    .A(HA.href(BH.domain(Domains.CORPUS).page(CorpusDomain.FICHE_ADVANCEDCOMMANDS_PAGE).subsetItem(destination)))
                    .__escape(CorpusMetadataUtils.getFicheTitle(destination, workingLang, formatLocale))
                    ._A()
                    ._P();
        });
        return true;
    }

}
