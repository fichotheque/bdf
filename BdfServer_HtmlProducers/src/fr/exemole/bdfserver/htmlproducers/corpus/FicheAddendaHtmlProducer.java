/* BdfServer_HtmlProducers - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.api.ficheform.AddendaIncludeElement;
import fr.exemole.bdfserver.api.ficheform.FormElement;
import fr.exemole.bdfserver.api.ficheform.FormElementProvider;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.CorpusDomain;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.commands.edition.FicheAddendaChangeCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.ParameterMap;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.forms.FormHandler;
import fr.exemole.bdfserver.html.forms.IncludeFormHtml;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.html.jslib.FicheJsLibs;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.pointeurs.FichePointeur;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlConstants;


/**
 *
 * @author Vincent Calame
 */
public class FicheAddendaHtmlProducer extends BdfServerHtmlProducer {

    private final BdfParameters bdfParameters;
    private final FichePointeur fichePointeur;
    private final FicheMeta ficheMeta;
    private final UiComponents uiComponents;
    private final FormElementProvider ficheForm;
    private final FormHandler formHandler;

    public FicheAddendaHtmlProducer(BdfParameters bdfParameters, FichePointeur fichePointeur, FormElementProvider formElementProvider, UiComponents uiComponents) {
        super(bdfParameters);
        this.bdfParameters = bdfParameters;
        this.fichePointeur = fichePointeur;
        this.ficheMeta = (FicheMeta) fichePointeur.getCurrentSubsetItem();
        this.ficheForm = formElementProvider;
        this.uiComponents = uiComponents;
        this.formHandler = FormHandler.build(bdfParameters);
        addJsLib(FicheJsLibs.FORM);
        addJsLib(BdfJsLibs.APPELANT);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addThemeCss("ficheform.css", "corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        CorpusHtmlUtils.printFicheHeader(this, ficheMeta, workingLang, formatLocale);
        printCommandMessageUnit();
        printFormUnit();
        end();
    }

    private boolean printFormUnit() {
        List<AddendaIncludeElement> list = getAddendaIncludeList();
        if (list.isEmpty()) {
            return false;
        }
        this
                .__(PageUnit.start("_ link.edition.ficheaddenda_short"))
                .FORM_post(HA.action(Domains.CORPUS)
                        .enctype(HtmlConstants.MULTIPART_ENCTYPE)
                        .attr("data-ficheform-role", "form")
                        .attr("data-ficheform-corpus", ficheMeta.getSubsetName())
                        .attr("data-submit-process", "wait"))
                .INPUT_hidden(ParameterMap.init()
                        .command(Domains.EDITION, FicheAddendaChangeCommand.COMMANDNAME)
                        .page(CorpusDomain.FICHE_ADDENDA_PAGE)
                        .subset(ficheMeta.getCorpus())
                        .subsetItem(ficheMeta))
                .P("ficheform-DragAndDropInfo")
                .__localize("_ info.edition.draganddrop_many")
                .P();
        for (AddendaIncludeElement element : list) {
            this
                    .__(IncludeFormHtml.printAddendaInclude(this, element, formHandler, true));
        }
        this
                .__(Button.COMMAND,
                        Button.submit("action-Save", "_ link.global.ok").ficheForm(true))
                ._FORM()
                .__(PageUnit.END);
        return true;
    }

    private List<AddendaIncludeElement> getAddendaIncludeList() {
        List<AddendaIncludeElement> list = new ArrayList<AddendaIncludeElement>();
        for (UiComponent uiComponent : uiComponents.getUiComponentList()) {
            if (uiComponent instanceof SubsetIncludeUi) {
                SubsetIncludeUi includeUi = (SubsetIncludeUi) uiComponent;
                if (includeUi.getCategory() == SubsetKey.CATEGORY_ADDENDA) {
                    FormElement.Include includeElement = ficheForm.getFormElement(fichePointeur, includeUi);
                    if ((includeElement != null) && (includeElement instanceof AddendaIncludeElement)) {
                        list.add((AddendaIncludeElement) includeElement);
                    }
                }
            }
        }
        return list;
    }


}
