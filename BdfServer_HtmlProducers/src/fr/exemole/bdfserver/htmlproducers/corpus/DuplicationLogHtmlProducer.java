/* BdfServer_HtmlProducers - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.corpus;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import net.fichotheque.tools.duplication.SubsetMatch;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class DuplicationLogHtmlProducer extends BdfServerHtmlProducer {

    private final SubsetMatch subsetMatch;

    public DuplicationLogHtmlProducer(BdfParameters bdfParameters, SubsetMatch subsetMatch) {
        super(bdfParameters);
        this.subsetMatch = subsetMatch;
        addThemeCss("corpus.css");
    }

    @Override
    public void printHtml() {
        start();
        this
                .__(PageUnit.start("_ title.corpus.duplicationlog"))
                .__(printCommandMessage());
        if (subsetMatch != null) {
            this
                    .TABLE("corpus-log-DuplicateTable");
            this
                    .TR();
            this
                    .TH()
                    .__escape(subsetMatch.getOriginSubset().getSubsetKey())
                    .__space()
                    .__escape('\u2799')
                    ._TH();
            this
                    .TH()
                    .__escape(subsetMatch.getDestinationSubset().getSubsetKey())
                    ._TH();
            this
                    ._TR();
            for (SubsetMatch.Entry entry : subsetMatch.getEntryList()) {
                this
                        .TR();
                this
                        .TD()
                        .__append(entry.getOrigin().getId())
                        ._TD();
                this
                        .TD()
                        .__append(entry.getDestination().getId())
                        ._TD();
                this
                        ._TR();
            }
            this
                    ._TABLE()
                    .__(PageUnit.END);
        }
        end();
    }

}
