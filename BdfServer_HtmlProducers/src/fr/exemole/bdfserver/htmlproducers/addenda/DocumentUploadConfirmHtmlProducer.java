/* BdfServer_HtmlProducers - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.DocumentJsLibs;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import java.text.NumberFormat;
import net.fichotheque.tools.parsers.DocumentChangeInfo;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.primitives.FileLength;
import net.mapeadores.util.text.FileName;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class DocumentUploadConfirmHtmlProducer extends BdfServerHtmlProducer {

    private final String appelant;
    private final DocumentChangeInfo documentChangeInfo;
    private final FileLength fileLength;

    public DocumentUploadConfirmHtmlProducer(BdfParameters bdfParameters, String appelant, DocumentChangeInfo documentChangeInfo, FileLength fileLength) {
        super(bdfParameters);
        this.appelant = appelant;
        this.documentChangeInfo = documentChangeInfo;
        this.fileLength = fileLength;
        addThemeCss("document.css");
        setBodyCssClass("global-body-Transparent");
        addJsLib(DocumentJsLibs.UPLOADCONFIRM);
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        FileName fileName = documentChangeInfo.getTmpFileName(0);
        String fileNameString = fileName.toString();
        String href = ConfigurationUtils.getTmpRelativeUrl(fileNameString);
        NumberFormat format = NumberFormat.getInstance(getFormatLocale());
        String sizeUnit;
        if (fileLength.getRoundType() == FileLength.KB_ROUND) {
            sizeUnit = LocalisationUtils.getKibiOctet(getFormatLocale());
        } else {
            sizeUnit = LocalisationUtils.getMebiOctet(getFormatLocale());
        }
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("appelant", appelant)
                .put("tmpFileName", fileNameString)
                .put("extension", fileName.getExtension())
                .put("tmpUrl", href)
                .put("size", format.format(fileLength.getRoundedValue()))
                .put("sizeUnit", sizeUnit);

        start();
        this
                .SCRIPT()
                .__jsObject("AddendaDoc.UploadConfirm.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId).classes("document-Client"))
                ._DIV();
        /*this
                .SCRIPT()
                .__newLine()
                .__escape("AddendaDoc.Confirm.appelant=")
                .__scriptLiteral(appelant)
                .__scriptLineEnd()
                .__escape("AddendaDoc.Confirm.tmpFileName=")
                .__scriptLiteral(fileNameString)
                .__scriptLineEnd()
                .__escape("AddendaDoc.Confirm.extension=")
                .__scriptLiteral(fileName.getExtension())
                .__scriptLineEnd()
                ._SCRIPT();
        this
                .DIV("addendadoc-GlobalIFrame");
        this
                .P()
                .__localize("_ info.addenda.uploadconfirm")
                ._P();
        this
                .P()
                .__localize("_ label.addenda.tmpfile")
                .__colon();
        this
                .A(HA.href(href).target(HtmlConstants.BLANK_TARGET))
                .__escape(href)
                ._A();

        this
                .SMALL()
                .__space()
                .__escape('(');
        this
                .__escape(format.format(value))
                .__nonBreakableSpace();
        if (fileLength.getRoundType() == FileLength.KB_ROUND) {
            this
                    .__escape(LocalisationUtils.getKibiOctet(getFormatLocale()));
        } else {
            this
                    .__escape(LocalisationUtils.getMebiOctet(getFormatLocale()));
        }
        this
                .__escape(')')
                ._SMALL()
                ._P();
        this
                .DIV("addendadoc-Buttons")
                .INPUT_button(HA.id("okButton").value(getLocalization("_ link.global.ok")))
                .__space()
                .INPUT_button(HA.id("cancelButton").value(getLocalization("_ link.global.cancel")))
                ._DIV();
        this
                ._DIV();*/
        end();
    }

}
