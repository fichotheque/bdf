/* BdfServer_HtmlProducers - Copyright (c) 2015-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.AddendaDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.addenda.Document;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class DocumentAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Document document;

    public DocumentAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Document document) {
        super(bdfParameters);
        this.document = document;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("addenda.css");
    }

    @Override
    public void printHtml() {
        start();
        AddendaHtmlUtils.printDocumentToolbar(this, AddendaDomain.DOCUMENT_ADVANCEDCOMMANDS_PAGE, document, true);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ADDENDA)
                .family("ADD")
                .veil(true)
                .page(AddendaDomain.DOCUMENT_ADVANCEDCOMMANDS_PAGE);
        AddendaCommandBoxUtils.printDocumentAttributeChangeBox(this, commandBox, document);
        end();
    }

}
