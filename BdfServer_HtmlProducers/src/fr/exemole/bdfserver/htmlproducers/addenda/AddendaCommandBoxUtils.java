/* BdfServer_HtmlProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.commands.addenda.AddendaAttributeChangeCommand;
import fr.exemole.bdfserver.commands.addenda.AddendaCreationCommand;
import fr.exemole.bdfserver.commands.addenda.AddendaPhrasesCommand;
import fr.exemole.bdfserver.commands.addenda.AddendaRemoveCommand;
import fr.exemole.bdfserver.commands.addenda.CroisementAddCommand;
import fr.exemole.bdfserver.commands.addenda.CroisementRemoveCommand;
import fr.exemole.bdfserver.commands.addenda.DocumentAttributeChangeCommand;
import fr.exemole.bdfserver.commands.addenda.DocumentNameChangeCommand;
import fr.exemole.bdfserver.commands.addenda.DocumentRemoveCommand;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.AttributesText;
import fr.exemole.bdfserver.html.consumers.Common;
import fr.exemole.bdfserver.html.consumers.CroisementSelection;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.MetadataPhrases;
import fr.exemole.bdfserver.html.consumers.attributes.Appelant;
import fr.exemole.bdfserver.html.consumers.attributes.InputPattern;
import fr.exemole.bdfserver.html.consumers.commandbox.Flag;
import static fr.exemole.bdfserver.htmlproducers.CommandBoxUtils.insert;
import fr.exemole.bdfserver.tools.ui.MetadataPhraseDefCatalog;
import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.addenda.metadata.AddendaMetadata;
import net.fichotheque.croisement.CroisementsBySubset;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class AddendaCommandBoxUtils {

    private AddendaCommandBoxUtils() {

    }

    public static boolean printAddendaCreationBox(HtmlPrinter hp, CommandBox commandBox) {
        commandBox = commandBox.derive(AddendaCreationCommand.COMMANDNAME, AddendaCreationCommand.COMMANDKEY)
                .actionCssClass("action-New")
                .__(Flag.UPDATE_COLLECTIONS)
                .submitLocKey("_ submit.addenda.addendacreation");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.addenda.newaddendaname", hp.name(AddendaCreationCommand.NEWADDENDA_PARAMNAME).size("30").populate(InputPattern.TECHNICAL_STRICT).required(true)))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printAddendaPhrasesBox(HtmlPrinter hp, CommandBox commandBox, Addenda addenda, BdfServer bdfServer) {
        AddendaMetadata addendaMetadata = addenda.getAddendaMetadata();
        MetadataPhrases metadataPhrases = MetadataPhrases.init(addendaMetadata, bdfServer.getLangConfiguration().getWorkingLangs(), "_ label.addenda.title")
                .addDef(MetadataPhraseDefCatalog.getMetadataPhraseDef(FichothequeConstants.NAMING_PHRASE))
                .populateFromAttributes(addenda)
                .addExtensionPhraseDefList(bdfServer);
        commandBox = commandBox.derive(AddendaPhrasesCommand.COMMANDNAME, AddendaPhrasesCommand.COMMANDKEY)
                .__(insert(addenda))
                .actionCssClass("action-Labels")
                .submitLocKey("_ submit.addenda.addendaphrases");
        hp
                .__start(commandBox)
                .__(metadataPhrases)
                .__end(commandBox);
        return true;
    }

    public static boolean printAddendaRemoveBox(HtmlPrinter hp, CommandBox commandBox, Addenda addenda) {
        commandBox = commandBox.derive(AddendaRemoveCommand.COMMANDNAME, AddendaRemoveCommand.COMMANDKEY)
                .__(insert(addenda))
                .__(Flag.UPDATE_COLLECTIONS)
                .actionCssClass("action-Delete")
                .submitLocKey("_ submit.addenda.addendaremove");
        hp
                .__start(commandBox)
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printAddendaAttributeChangeBox(HtmlPrinter hp, CommandBox commandBox, Addenda addenda) {
        commandBox = commandBox.derive(AddendaAttributeChangeCommand.COMMANDNAME, AddendaAttributeChangeCommand.COMMANDKEY)
                .__(insert(addenda))
                .actionCssClass("action-Attributes")
                .submitLocKey("_ submit.global.attributechange");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.attributes", hp.name(AddendaAttributeChangeCommand.ATTRIBUTES_PARAMNAME).rows(8).cols(75).attr("data-codemirror-mode", "attributes"),
                        new AttributesText(addenda.getAddendaMetadata().getAttributes())))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printDocumentAttributeChangeBox(HtmlPrinter hp, CommandBox commandBox, Document document) {
        commandBox = commandBox.derive(DocumentAttributeChangeCommand.COMMANDNAME, DocumentAttributeChangeCommand.COMMANDKEY)
                .__(insert(document))
                .actionCssClass("action-Attributes")
                .submitLocKey("_ submit.global.attributechange");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.attributes", hp.name(DocumentAttributeChangeCommand.ATTRIBUTES_PARAMNAME).rows(8).cols(75).attr("data-codemirror-mode", "attributes"),
                        new AttributesText(document.getAttributes())))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printDocumentNameChangeBox(HtmlPrinter hp, CommandBox commandBox, Document document, Lang workingLang) {
        commandBox = commandBox.derive(DocumentNameChangeCommand.COMMANDNAME, DocumentNameChangeCommand.COMMANDKEY)
                .__(insert(document))
                .submitLocKey("_ submit.addenda.documentnamechange");
        hp
                .__start(commandBox);
        String namingConvention = FichothequeUtils.getPhraseLabel(document.getAddenda().getMetadata().getPhrases(), FichothequeConstants.NAMING_PHRASE, workingLang);
        if (namingConvention != null) {
            hp
                    .P("global-SubTitle")
                    .__escape(namingConvention)
                    ._P();
        }
        hp
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.addenda.documentname", hp.name(DocumentNameChangeCommand.NAME_PARAMNAME).value(document.getBasename()).size("30").populate(InputPattern.DOCUMENTNAME).required(true)))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printDocumentRemoveBox(HtmlPrinter hp, CommandBox commandBox, Document document) {
        commandBox = commandBox.derive(DocumentRemoveCommand.COMMANDNAME, DocumentRemoveCommand.COMMANDKEY)
                .__(insert(document))
                .actionCssClass("action-Delete")
                .submitLocKey("_ submit.addenda.documentremove");
        hp
                .__start(commandBox)
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printCroisementRemoveBox(HtmlPrinter hp, CommandBox commandBox, Document document, List<CroisementsBySubset> croisementsBySubsetList, PermissionSummary permissionSummary, BdfUser bdfUser) {
        commandBox = commandBox.derive(CroisementRemoveCommand.COMMANDNAME, CroisementRemoveCommand.COMMANDKEY)
                .__(insert(document))
                .actionCssClass("action-Delete")
                .submitLocKey("_ submit.global.croisementremove");
        hp
                .__start(commandBox)
                .__(new CroisementSelection(CroisementRemoveCommand.REMOVE_PARAMNAME, croisementsBySubsetList, bdfUser.getWorkingLang(), bdfUser.getFormatLocale()))
                .__(BdfHtmlUtils.printCroisementRemoveWarning(hp, document.getSubsetKey(), permissionSummary))
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printCroisementAddBox(HtmlPrinter hp, CommandBox commandBox, Document document) {
        commandBox = commandBox.derive(CroisementAddCommand.COMMANDNAME, CroisementAddCommand.COMMANDKEY)
                .__(insert(document))
                .actionCssClass("action-New")
                .submitLocKey("_ submit.global.croisementadd");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.croisementadd", hp.name(CroisementAddCommand.ADD_PARAMNAME).cols(60).rows(3).populate(Appelant.fiche())))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

}
