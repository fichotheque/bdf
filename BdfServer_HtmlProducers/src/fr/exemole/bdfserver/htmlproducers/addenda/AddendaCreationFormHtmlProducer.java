/* BdfServer_HtmlProducers  - Copyright (c) 2015-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.AddendaDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class AddendaCreationFormHtmlProducer extends BdfServerHtmlProducer {

    public AddendaCreationFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("addenda.css");
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ADDENDA)
                .family("ADD")
                .veil(true)
                .page(AddendaDomain.ADDENDA_METADATAFORM_PAGE)
                .errorPage(AddendaDomain.ADDENDA_CREATIONFORM_PAGE);
        AddendaCommandBoxUtils.printAddendaCreationBox(this, commandBox);
        end();
    }

}
