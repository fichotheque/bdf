/* BdfServer_HtmlProducers - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.api.interaction.domains.AddendaDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.DocumentJsLibs;
import net.fichotheque.addenda.Addenda;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.logging.CommandMessage;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class DocumentUploadHtmlProducer extends BdfServerHtmlProducer {

    private final String appelant;
    private final Addenda addenda;
    private final boolean newUpload;

    private DocumentUploadHtmlProducer(BdfParameters bdfParameters, String appelant, Addenda addenda, boolean newUpload) {
        super(bdfParameters);
        this.appelant = appelant;
        this.addenda = addenda;
        this.newUpload = newUpload;
        addJsLib(DocumentJsLibs.UPLOAD);
        addThemeCss("document.css");
    }

    public static DocumentUploadHtmlProducer newNewUpload(BdfParameters bdfParameters, String appelant, Addenda addenda) {
        DocumentUploadHtmlProducer producer = new DocumentUploadHtmlProducer(bdfParameters, appelant, addenda, true);
        return producer;
    }

    public static DocumentUploadHtmlProducer newVersionUpload(BdfParameters bdfParameters, String appelant, Addenda addenda) {
        DocumentUploadHtmlProducer producer = new DocumentUploadHtmlProducer(bdfParameters, appelant, addenda, false);
        producer.setBodyCssClass("global-body-Transparent");
        return producer;
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("appelant", appelant)
                .put("newUpload", newUpload)
                .put("addenda", addenda.getSubsetName());
        if (newUpload) {
            args
                    .put("errorPage", AddendaDomain.DOCUMENT_UPLOAD_NEW_PAGE)
                    .put("resultPage", AddendaDomain.DOCUMENT_CHANGE_PAGE);
        } else {
            args
                    .put("errorPage", AddendaDomain.DOCUMENT_UPLOAD_VERSION_PAGE)
                    .put("resultPage", AddendaDomain.DOCUMENT_UPLOAD_CONFIRM_PAGE);
        }
        CommandMessage cm = getCommandMessage();
        if (cm != null) {
            args
                    .put("errorMessage", getLocalization(cm));
        }
        startLoc((newUpload) ? "_ title.addenda.documentcreate" : "_ title.addenda.documentchange");
        this
                .SCRIPT()
                .__jsObject("AddendaDoc.Upload.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId).classes("document-Client"))
                ._DIV();
        end();
    }


}
