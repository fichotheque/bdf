/* BdfServer_HtmlProducers - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AddendaDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Document;
import net.fichotheque.croisement.CroisementsBySubset;
import net.fichotheque.eligibility.SubsetEligibility;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.tools.permission.PermissionSubsetEligibility;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class DocumentAdminFormHtmlProducer extends BdfServerHtmlProducer {

    private final Document document;
    private final PermissionSummary permissionSummary;
    private final boolean isSubsetAdmin;

    public DocumentAdminFormHtmlProducer(BdfParameters bdfParameters, Document document) {
        super(bdfParameters);
        this.document = document;
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.isSubsetAdmin = permissionSummary.isSubsetAdmin(document.getSubsetKey());
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.APPELANT);
        addThemeCss("addenda.css");
    }

    @Override
    public void printHtml() {
        SubsetEligibility subsetEligibity = PermissionSubsetEligibility.read(permissionSummary);
        List<CroisementsBySubset> croisementsBySubsetList = CroisementUtils.filterCroisements(document, subsetEligibity, SubsetKey.CATEGORY_CORPUS);
        start();
        AddendaHtmlUtils.printDocumentToolbar(this, AddendaDomain.DOCUMENT_ADMINFORM_PAGE, document, isSubsetAdmin);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ADDENDA)
                .family("ADD")
                .veil(true)
                .page(AddendaDomain.DOCUMENT_ADMINFORM_PAGE);
        AddendaCommandBoxUtils.printDocumentNameChangeBox(this, commandBox, document, workingLang);
        if (!croisementsBySubsetList.isEmpty()) {
            AddendaCommandBoxUtils.printCroisementRemoveBox(this, commandBox, document, croisementsBySubsetList, permissionSummary, bdfUser);
        }
        AddendaCommandBoxUtils.printCroisementAddBox(this, commandBox, document);
        commandBox
                .errorPage(AddendaDomain.DOCUMENT_ADMINFORM_PAGE)
                .page(InteractionConstants.MESSAGE_PAGE);
        AddendaCommandBoxUtils.printDocumentRemoveBox(this, commandBox, document);
        end();
    }

}
