/* BdfServer_HtmlProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.AddendaDomain;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.SubsetIcon;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfHref;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class AddendaHtmlUtils {

    private AddendaHtmlUtils() {

    }

    public static boolean printAddendaToolbar(HtmlPrinter hp, String pageActu, Addenda addenda) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-Metadata", addenda, AddendaDomain.ADDENDA_METADATAFORM_PAGE, "_ link.addenda.addendametadataform", pageActu))
                .__(link(button, "action-Advanced", addenda, AddendaDomain.ADDENDA_ADVANCEDCOMMANDS_PAGE, "_ link.addenda.addendaadvancedcommands", pageActu))
                .__(refresh(button, addenda))
                ._DIV()
                ._NAV();
        return true;
    }

    public static boolean printDocumentToolbar(HtmlPrinter hp, String pageActu, Document document, boolean isSubsetAdmin) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .HEADER("subset-ItemHeader")
                .H1()
                .__(SubsetIcon.ADDENDA)
                .SPAN()
                .__escape(document.getGlobalId())
                .__dash()
                .__escape(document.getBasename())
                ._SPAN()
                ._H1()
                ._HEADER();
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-DocumentEdit", document, AddendaDomain.DOCUMENT_ADMINFORM_PAGE, "_ link.addenda.documentadminform", pageActu))
                .__(link(button, "action-Advanced", document, AddendaDomain.DOCUMENT_ADVANCEDCOMMANDS_PAGE, "_ link.addenda.documentadvancedcommands", pageActu))
                .__(refresh(button, document.getAddenda()))
                ._DIV()
                ._NAV();
        return true;
    }

    private static Button link(Button button, String action, Object object, String page, String titleLocKey, String currentPage) {
        if (currentPage.equals(page)) {
            return button.current(true).href(null).action(action).tooltip(null);
        } else {
            BdfHref href = BH.domain(Domains.ADDENDA).page(page);
            if (object instanceof Addenda) {
                href.subset((Addenda) object);
            } else if (object instanceof Document) {
                href.subsetItem((Document) object);
            }
            return button.current(false).href(href).action(action).tooltipMessage(titleLocKey);
        }
    }


    private static Button refresh(Button button, Addenda addenda) {
        return button.current(false)
                .href(BH.domain(Domains.ADDENDA).page(AddendaDomain.ADDENDA_PAGE).subset(addenda))
                .action("action-Refresh")
                .tooltipMessage("_ link.global.reload")
                .target(BdfHtmlConstants.LIST_FRAME);
    }


}
