/* BdfServer_HtmlProducers - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.addenda;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AddendaDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.addenda.Addenda;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class AddendaAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Addenda addenda;

    public AddendaAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Addenda addenda) {
        super(bdfParameters);
        this.addenda = addenda;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("addenda.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, addenda, AddendaDomain.ADDENDA_ADVANCEDCOMMANDS_PAGE);
        AddendaHtmlUtils.printAddendaToolbar(this, AddendaDomain.ADDENDA_ADVANCEDCOMMANDS_PAGE, addenda);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ADDENDA)
                .family("ADD")
                .veil(true);
        if ((bdfParameters.isFichothequeAdmin()) && (addenda.isRemoveable())) {
            commandBox
                    .errorPage(AddendaDomain.ADDENDA_ADVANCEDCOMMANDS_PAGE)
                    .page(InteractionConstants.MESSAGE_PAGE);
            AddendaCommandBoxUtils.printAddendaRemoveBox(this, commandBox, addenda);
        }
        commandBox
                .errorPage(null)
                .page(AddendaDomain.ADDENDA_ADVANCEDCOMMANDS_PAGE);
        AddendaCommandBoxUtils.printAddendaAttributeChangeBox(this, commandBox, addenda);
    }

}
