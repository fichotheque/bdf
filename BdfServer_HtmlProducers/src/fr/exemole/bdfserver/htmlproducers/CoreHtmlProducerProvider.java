/* BdfServer_HtmlProducers - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.providers.HtmlProducerProvider;
import fr.exemole.bdfserver.html.MessageHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.addenda.AddendaHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.administration.AdministrationHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.album.AlbumHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.configuration.ConfigurationHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.corpus.CorpusHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.edition.EditionHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.exportation.ExportationHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.importation.ImportationHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.mailing.MailingHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.main.MainHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.misc.MiscHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.pioche.PiocheHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.selection.SelectionHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.sphere.SphereHtmlProducerFactory;
import fr.exemole.bdfserver.htmlproducers.thesaurus.ThesaurusHtmlProducerFactory;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class CoreHtmlProducerProvider implements HtmlProducerProvider {

    public final static HtmlProducerProvider UNIQUE_INSTANCE = new CoreHtmlProducerProvider();

    private CoreHtmlProducerProvider() {
    }

    @Override
    public HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        if (parameters.getOutput().equals(InteractionConstants.MESSAGE_PAGE)) {
            return MessageHtmlProducer.init(parameters)
                    .setBdfCommandResult(parameters.getBdfCommandResult());
        } else {
            switch (parameters.getDomain().getFirstPart()) {
                case Domains.ADDENDA:
                    return AddendaHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.ADMINISTRATION:
                    return AdministrationHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.ALBUM:
                    return AlbumHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.CONFIGURATION:
                    return ConfigurationHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.CORPUS:
                    return CorpusHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.EDITION:
                    return EditionHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.EXPORTATION:
                    return ExportationHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.IMPORTATION:
                    return ImportationHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.MAILING:
                    return MailingHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.MAIN:
                    return MainHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.MISC:
                    return MiscHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.PIOCHE:
                    return PiocheHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.SELECTION:
                    return SelectionHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.SPHERE:
                    return SphereHtmlProducerFactory.getHtmlProducer(parameters);
                case Domains.THESAURUS:
                    return ThesaurusHtmlProducerFactory.getHtmlProducer(parameters);
                default:
                    return null;
            }
        }
    }

}
