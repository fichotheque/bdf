/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.commands.album.AlbumAttributeChangeCommand;
import fr.exemole.bdfserver.commands.album.AlbumCreationCommand;
import fr.exemole.bdfserver.commands.album.AlbumDimChangeCommand;
import fr.exemole.bdfserver.commands.album.AlbumDimCreationCommand;
import fr.exemole.bdfserver.commands.album.AlbumDimRemoveCommand;
import fr.exemole.bdfserver.commands.album.AlbumPhrasesCommand;
import fr.exemole.bdfserver.commands.album.AlbumRemoveCommand;
import fr.exemole.bdfserver.commands.album.CroisementAddCommand;
import fr.exemole.bdfserver.commands.album.CroisementRemoveCommand;
import fr.exemole.bdfserver.commands.album.IllustrationAttributeChangeCommand;
import fr.exemole.bdfserver.commands.album.IllustrationFileUploadCommand;
import fr.exemole.bdfserver.commands.album.IllustrationRemoveCommand;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.AttributesText;
import fr.exemole.bdfserver.html.consumers.Common;
import fr.exemole.bdfserver.html.consumers.CroisementSelection;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.MetadataPhrases;
import fr.exemole.bdfserver.html.consumers.SelectOption;
import fr.exemole.bdfserver.html.consumers.attributes.Appelant;
import fr.exemole.bdfserver.html.consumers.attributes.InputPattern;
import fr.exemole.bdfserver.html.consumers.commandbox.Flag;
import static fr.exemole.bdfserver.htmlproducers.CommandBoxUtils.insert;
import java.util.List;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumConstants;
import net.fichotheque.album.Illustration;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.album.metadata.AlbumMetadata;
import net.fichotheque.croisement.CroisementsBySubset;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.awt.ResizeInfo;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class AlbumCommandBoxUtils {

    private final static SelectOption[] DIMTYPE_OPTIONS = {
        SelectOption.init(AlbumConstants.FIXEDWIDTH_DIMTYPE).textL10nObject("_ label.album.albumdimtype_fixedwidth"),
        SelectOption.init(AlbumConstants.FIXEDHEIGHT_DIMTYPE).textL10nObject("_ label.album.albumdimtype_fixedheight"),
        SelectOption.init(AlbumConstants.MAXWIDTH_DIMTYPE).textL10nObject("_ label.album.albumdimtype_maxwidth"),
        SelectOption.init(AlbumConstants.MAXHEIGHT_DIMTYPE).textL10nObject("_ label.album.albumdimtype_maxheight"),
        SelectOption.init(AlbumConstants.MAXDIM_DIMTYPE).textL10nObject("_ label.album.albumdimtype_maxdim")
    };

    private AlbumCommandBoxUtils() {
    }

    public static boolean printAlbumCreationBox(HtmlPrinter hp, CommandBox commandBox) {
        commandBox = commandBox.derive(AlbumCreationCommand.COMMANDNAME, AlbumCreationCommand.COMMANDKEY)
                .actionCssClass("action-New")
                .__(Flag.UPDATE_COLLECTIONS)
                .submitLocKey("_ submit.album.albumcreation");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.album.newalbumname", hp.name(AlbumCreationCommand.NEWALBUM_PARAMNAME).size("30").populate(InputPattern.TECHNICAL_STRICT).required(true)))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printAlbumPhrasesBox(HtmlPrinter hp, CommandBox commandBox, Album album, BdfServer bdfServer) {
        AlbumMetadata albumMetadata = album.getAlbumMetadata();
        commandBox = commandBox.derive(AlbumPhrasesCommand.COMMANDNAME, AlbumPhrasesCommand.COMMANDKEY)
                .__(insert(album))
                .actionCssClass("action-Labels")
                .submitLocKey("_ submit.album.albumphrases");
        hp
                .__start(commandBox)
                .__(MetadataPhrases.init(albumMetadata, bdfServer.getLangConfiguration().getWorkingLangs(), "_ label.album.title").populateFromAttributes(album).addExtensionPhraseDefList(bdfServer))
                .__end(commandBox);
        return true;
    }

    public static boolean printAlbumRemoveBox(HtmlPrinter hp, CommandBox commandBox, Album album) {
        commandBox = commandBox.derive(AlbumRemoveCommand.COMMANDNAME, AlbumRemoveCommand.COMMANDKEY)
                .__(insert(album))
                .actionCssClass("action-Delete")
                .__(Flag.UPDATE_COLLECTIONS)
                .submitLocKey("_ submit.album.albumremove");
        hp
                .__start(commandBox)
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printAlbumAttributeChangeBox(HtmlPrinter hp, CommandBox commandBox, Album album) {
        commandBox = commandBox.derive(AlbumAttributeChangeCommand.COMMANDNAME, AlbumAttributeChangeCommand.COMMANDKEY)
                .__(insert(album))
                .actionCssClass("action-Attributes")
                .submitLocKey("_ submit.global.attributechange");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.attributes", hp.name(AlbumAttributeChangeCommand.ATTRIBUTES_PARAMNAME).rows(8).cols(75).attr("data-codemirror-mode", "attributes"),
                        new AttributesText(album.getAlbumMetadata().getAttributes())))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printAlbumDimChangeBox(HtmlPrinter hp, CommandBox commandBox, AlbumDim albumDim) {
        ResizeInfo resizeInfo = albumDim.getResizeInfo();
        String dimType = albumDim.getDimType();
        commandBox = commandBox.derive(AlbumDimChangeCommand.COMMANDNAME, AlbumDimChangeCommand.COMMANDKEY)
                .__(insert(albumDim))
                .submitLocKey("_ submit.album.albumdimchange");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(() -> {
                    if (AlbumUtils.needWidth(dimType)) {
                        hp
                                .__(Grid.textInputRow("_ label.album.width", hp.name(AlbumDimChangeCommand.ALBUMDIM_WIDTH_PARAMNAME).value(String.valueOf(resizeInfo.getWidth())).size("15")));
                    }
                })
                .__(() -> {
                    if (AlbumUtils.needHeight(dimType)) {
                        hp
                                .__(Grid.textInputRow("_ label.album.height", hp.name(AlbumDimChangeCommand.ALBUMDIM_HEIGHT_PARAMNAME).value(String.valueOf(resizeInfo.getHeight())).size("15")));

                    }
                })
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printAlbumDimRemoveBox(HtmlPrinter hp, CommandBox commandBox, AlbumDim albumDim) {
        commandBox = commandBox.derive(AlbumDimRemoveCommand.COMMANDNAME, AlbumDimRemoveCommand.COMMANDKEY)
                .__(insert(albumDim))
                .submitLocKey("_ submit.album.albumdimremove");
        hp
                .__start(commandBox)
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printAlbumDimCreationBox(HtmlPrinter hp, CommandBox commandBox, Album album) {
        commandBox = commandBox.derive(AlbumDimCreationCommand.COMMANDNAME, AlbumDimCreationCommand.COMMANDKEY)
                .__(insert(album))
                .submitLocKey("_ submit.album.albumdimcreation");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.album.newalbumdim", hp.name(AlbumDimCreationCommand.NEWALBUMDIM_PARAMNAME).size("30").populate(InputPattern.TECHNICAl_UNDERSCORE).required(true)))
                .__(Grid.selectRow("_ label.album.albumdimtype", hp.name(AlbumDimCreationCommand.ALBUMDIMTYPE_PARAMNAME), SelectOption.consumer(DIMTYPE_OPTIONS, AlbumConstants.FIXEDWIDTH_DIMTYPE)))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }


    public static boolean printIllustrationFileUploadBox(HtmlPrinter hp, CommandBox commandBox, Album album, Illustration illustration) {
        Object insertObject;
        if (illustration != null) {
            insertObject = illustration;
        } else {
            insertObject = album;
        }
        commandBox = commandBox.derive(IllustrationFileUploadCommand.COMMANDNAME, IllustrationFileUploadCommand.COMMANDKEY)
                .multipart(true)
                .__(insert(insertObject))
                .submitLocKey("_ submit.album.illustrationfileupload");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.fileInputRow("_ label.album.illustrationfile", hp.name(IllustrationFileUploadCommand.FILE_PARAMNAME).size("40").classes("global-FileInput")))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printIllustrationAttributeChangeBox(HtmlPrinter hp, CommandBox commandBox, Illustration illustration) {
        commandBox = commandBox.derive(IllustrationAttributeChangeCommand.COMMANDNAME, IllustrationAttributeChangeCommand.COMMANDKEY)
                .__(insert(illustration))
                .actionCssClass("action-Attributes")
                .submitLocKey("_ submit.global.attributechange");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.attributes", hp.name(IllustrationAttributeChangeCommand.ATTRIBUTES_PARAMNAME).rows(8).cols(75).attr("data-codemirror-mode", "attributes"),
                        new AttributesText(illustration.getAttributes())))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

    public static boolean printIllustrationRemoveBox(HtmlPrinter hp, CommandBox commandBox, Illustration illustration) {
        commandBox = commandBox.derive(IllustrationRemoveCommand.COMMANDNAME, IllustrationRemoveCommand.COMMANDKEY)
                .__(insert(illustration))
                .actionCssClass("action-Delete")
                .submitLocKey("_ submit.album.illustrationremove");
        hp
                .__start(commandBox)
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printCroisementRemoveBox(HtmlPrinter hp, CommandBox commandBox, Illustration illustration, List<CroisementsBySubset> croisementsBySubsetList, PermissionSummary permissionSummary, BdfUser bdfUser) {
        commandBox = commandBox.derive(CroisementRemoveCommand.COMMANDNAME, CroisementRemoveCommand.COMMANDKEY)
                .__(insert(illustration))
                .actionCssClass("action-Delete")
                .submitLocKey("_ submit.global.croisementremove");
        hp
                .__start(commandBox)
                .__(new CroisementSelection(CroisementRemoveCommand.REMOVE_PARAMNAME, croisementsBySubsetList, bdfUser.getWorkingLang(), bdfUser.getFormatLocale()))
                .__(BdfHtmlUtils.printCroisementRemoveWarning(hp, illustration.getSubsetKey(), permissionSummary))
                .__(Common.REMOVE_CONFIRM_CHECK)
                .__end(commandBox);
        return true;
    }

    public static boolean printCroisementAddBox(HtmlPrinter hp, CommandBox commandBox, Illustration illustration) {
        commandBox = commandBox.derive(CroisementAddCommand.COMMANDNAME, CroisementAddCommand.COMMANDKEY)
                .__(insert(illustration))
                .actionCssClass("action-New")
                .submitLocKey("_ submit.global.croisementadd");
        hp
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.textAreaBlockRow("_ label.global.croisementadd", hp.name(CroisementAddCommand.ADD_PARAMNAME).cols(60).rows(3).populate(Appelant.fiche())))
                .__(Grid.END)
                .__end(commandBox);
        return true;
    }

}
