/* BdfServer_HtmlProducers - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import java.util.List;
import net.fichotheque.album.Album;
import net.fichotheque.album.metadata.AlbumDim;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class AlbumMetadataFormHtmlProducer extends BdfServerHtmlProducer {

    private final Album album;

    public AlbumMetadataFormHtmlProducer(BdfParameters bdfParameters, Album album) {
        super(bdfParameters);
        this.album = album;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addThemeCss("album.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, album, AlbumDomain.ALBUM_METADATAFORM_PAGE);
        AlbumHtmlUtils.printAlbumToolbar(this, AlbumDomain.ALBUM_METADATAFORM_PAGE, album);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ALBUM)
                .family("ALB")
                .veil(true)
                .page(AlbumDomain.ALBUM_METADATAFORM_PAGE);
        AlbumCommandBoxUtils.printAlbumPhrasesBox(this, commandBox, album, bdfServer);
        List<AlbumDim> albumDimList = album.getAlbumMetadata().getAlbumDimList();
        if (!albumDimList.isEmpty()) {
            this
                    .__(PageUnit.start("_ title.album.albumdim_list"))
                    .DL("global-DL")
                    .__(printAlbumDimDL(albumDimList, commandBox.mode(InteractionConstants.SUBUNIT_MODE)))
                    ._DL()
                    .__(PageUnit.END);
            commandBox.mode("");
        }
        AlbumCommandBoxUtils.printAlbumDimCreationBox(this, commandBox, album);
        end();
    }

    private boolean printAlbumDimDL(List<AlbumDim> albumDimList, CommandBox commandBox) {
        for (AlbumDim albumDim : albumDimList) {
            this
                    .DL()
                    .__escape(albumDim.getName())
                    .__escape(" / ")
                    .__localize(AlbumHtmlUtils.getAlbumDimMessageKey(albumDim.getDimType()))
                    ._DL()
                    .DD()
                    .__(printAlbumDimChangeDetails(albumDim, commandBox))
                    .__(printAlbumDimRemoveDetails(albumDim, commandBox))
                    ._DD();
        }
        return true;
    }

    private boolean printAlbumDimChangeDetails(AlbumDim albumDim, CommandBox commandBox) {
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.change")
                ._SUMMARY()
                .__(AlbumCommandBoxUtils.printAlbumDimChangeBox(this, commandBox, albumDim))
                ._DETAILS();
        return true;
    }

    private boolean printAlbumDimRemoveDetails(AlbumDim albumDim, CommandBox commandBox) {
        this
                .DETAILS("command-Details")
                .SUMMARY()
                .__localize("_ link.global.delete")
                ._SUMMARY()
                .__(AlbumCommandBoxUtils.printAlbumDimRemoveBox(this, commandBox, albumDim))
                ._DETAILS();
        return true;
    }

}
