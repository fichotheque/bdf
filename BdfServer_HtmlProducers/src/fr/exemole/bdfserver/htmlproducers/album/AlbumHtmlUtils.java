/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfHref;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumConstants;
import net.fichotheque.album.Illustration;
import net.fichotheque.album.metadata.AlbumMetadata;
import net.mapeadores.util.awt.ResizeInfo;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.TrustedHtml;
import net.mapeadores.util.html.TrustedHtmlFactory;


/**
 *
 * @author Vincent Calame
 */
public final class AlbumHtmlUtils {

    private AlbumHtmlUtils() {
    }

    public static TrustedHtml getThumbnailSize(AlbumMetadata albumMetadata) {
        ResizeInfo resizeInfo = albumMetadata.getResizeInfo(AlbumConstants.MINI_SPECIALDIM);
        StringBuilder buf = new StringBuilder();
        buf.append(".album-Thumbnail {");
        int width = resizeInfo.getWidth();
        if (width > 0) {
            buf.append("width: ");
            buf.append(width);
            buf.append("px;");
        }
        int height = resizeInfo.getHeight();
        if (height > 0) {
            buf.append("height: ");
            buf.append(height);
            buf.append("px;");
        }
        buf.append('}');
        return TrustedHtmlFactory.UNCHECK.build(buf.toString());
    }

    public static boolean printAlbumToolbar(HtmlPrinter hp, String pageActu, Album album) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-Metadata", album, AlbumDomain.ALBUM_METADATAFORM_PAGE, "_ link.album.albummetadataform", pageActu))
                .__(link(button, "action-Advanced", album, AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE, "_ link.album.albumadvancedcommands", pageActu))
                .__(refresh(button, album))
                ._DIV()
                ._NAV();
        return true;
    }

    public static boolean printIllustrationToolbar(HtmlPrinter hp, String pageActu, Illustration illustration, boolean isSubsetAdmin) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        String fileName = illustration.getFileName();
        hp
                .HEADER("subset-ItemHeader album-IllustrationHeader")
                .DIV("album-IllustrationThumbnail")
                .A(HA.href("illustrations/" + fileName))
                .IMG(HA.src("illustrations/_mini/" + fileName))
                ._A()
                ._DIV()
                .H1()
                .__escape(fileName)
                ._H1()
                ._HEADER();
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-IllustrationEdit", illustration, AlbumDomain.ILLUSTRATION_ADMINFORM_PAGE, "_ link.album.illustrationadminform", pageActu))
                .__(link(button, "action-Advanced", illustration, AlbumDomain.ILLUSTRATION_ADVANCEDCOMMANDS_PAGE, "_ link.album.illustrationadvancedcommands", pageActu))
                .__(refresh(button, illustration.getAlbum()))
                ._DIV()
                ._NAV();
        return true;
    }

    public static String getAlbumDimMessageKey(String dimType) {
        switch (dimType) {
            case AlbumConstants.FIXEDWIDTH_DIMTYPE:
                return "_ label.album.albumdimtype_fixedwidth";
            case AlbumConstants.FIXEDHEIGHT_DIMTYPE:
                return "_ label.album.albumdimtype_fixedheight";
            case AlbumConstants.MAXWIDTH_DIMTYPE:
                return "_ label.album.albumdimtype_maxwidth";
            case AlbumConstants.MAXHEIGHT_DIMTYPE:
                return "_ label.album.albumdimtype_maxheight";
            case AlbumConstants.MAXDIM_DIMTYPE:
                return "_ label.album.albumdimtype_maxdim";
            default:
                throw new IllegalArgumentException("unknown type " + dimType);
        }
    }

    private static Button link(Button button, String action, Object object, String page, String titleLocKey, String currentPage) {
        if (currentPage.equals(page)) {
            return button.current(true).href(null).action(action).tooltip(null);
        } else {
            BdfHref href = BH.domain(Domains.ALBUM).page(page);
            if (object instanceof Album) {
                href.subset((Album) object);
            } else if (object instanceof Illustration) {
                href.subsetItem((Illustration) object);
            }
            return button.current(false).href(href).action(action).tooltipMessage(titleLocKey);
        }
    }

    private static Button refresh(Button button, Album album) {
        return button.current(false)
                .href(BH.domain(Domains.ALBUM).page(AlbumDomain.ALBUM_PAGE).subset(album))
                .action("action-Refresh")
                .tooltipMessage("_ link.global.reload")
                .target(BdfHtmlConstants.LIST_FRAME);
    }

}
