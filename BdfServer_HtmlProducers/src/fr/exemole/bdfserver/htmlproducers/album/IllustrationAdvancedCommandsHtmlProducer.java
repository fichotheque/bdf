/* BdfServer_HtmlProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.album.Illustration;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Illustration illustration;

    public IllustrationAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Illustration illustration) {
        super(bdfParameters);
        this.illustration = illustration;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("album.css");
        addStyle(AlbumHtmlUtils.getThumbnailSize(illustration.getAlbum().getAlbumMetadata()));
    }

    @Override
    public void printHtml() {
        start();
        AlbumHtmlUtils.printIllustrationToolbar(this, AlbumDomain.ILLUSTRATION_ADVANCEDCOMMANDS_PAGE, illustration, true);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ALBUM)
                .family("ALB")
                .veil(true)
                .page(AlbumDomain.ILLUSTRATION_ADVANCEDCOMMANDS_PAGE);
        AlbumCommandBoxUtils.printIllustrationAttributeChangeBox(this, commandBox, illustration);
        end();
    }

}
