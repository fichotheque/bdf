/* BdfServer_HtmlProducers - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.html.jslib.IllustrationJsLibs;
import net.fichotheque.album.Album;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.logging.CommandMessage;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationUploadHtmlProducer extends BdfServerHtmlProducer {

    private final Album album;
    private final String appelant;
    private final boolean newUpload;


    private IllustrationUploadHtmlProducer(BdfParameters bdfParameters, Album album, String appelant, boolean newUpload) {
        super(bdfParameters);
        this.album = album;
        this.appelant = appelant;
        this.newUpload = newUpload;
        addJsLib(IllustrationJsLibs.UPLOAD);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addThemeCss("illustration.css");
    }

    public static IllustrationUploadHtmlProducer newNewUpload(BdfParameters bdfParameters, Album album, String appelant) {
        IllustrationUploadHtmlProducer producer = new IllustrationUploadHtmlProducer(bdfParameters, album, appelant, true);
        return producer;
    }

    public static IllustrationUploadHtmlProducer newReplaceUpload(BdfParameters bdfParameters, Album album, String appelant) {
        IllustrationUploadHtmlProducer producer = new IllustrationUploadHtmlProducer(bdfParameters, album, appelant, false);
        producer.setBodyCssClass("global-body-Transparent");
        return producer;
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("appelant", appelant)
                .put("newUpload", newUpload)
                .put("album", album.getSubsetName());
        if (newUpload) {
            args
                    .put("errorPage", AlbumDomain.ILLUSTRATION_UPLOAD_NEW_PAGE)
                    .put("resultPage", AlbumDomain.ILLUSTRATION_CHANGE_PAGE);
        } else {
            args
                    .put("errorPage", AlbumDomain.ILLUSTRATION_UPLOAD_REPLACE_PAGE)
                    .put("resultPage", AlbumDomain.ILLUSTRATION_UPLOAD_CONFIRM_PAGE);
        }
        CommandMessage cm = getCommandMessage();
        if (cm != null) {
            args
                    .put("errorMessage", getLocalization(cm));
        }
        startLoc((newUpload) ? "_ title.album.newupload" : "_ title.album.replaceupload");
        this
                .SCRIPT()
                .__jsObject("Illustration.Upload.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId).classes("illustration-Client"))
                ._DIV();
        end();
    }

}
