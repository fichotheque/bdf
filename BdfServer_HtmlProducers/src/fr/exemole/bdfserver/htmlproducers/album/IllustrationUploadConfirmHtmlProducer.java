/* BdfServer_HtmlProducers - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.IllustrationJsLibs;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationUploadConfirmHtmlProducer extends BdfServerHtmlProducer {

    private final String appelant;
    private final String tmpFileName;

    public IllustrationUploadConfirmHtmlProducer(BdfParameters bdfParameters, String appelant, String tmpFileName) {
        super(bdfParameters);
        this.appelant = appelant;
        this.tmpFileName = tmpFileName;
        addThemeCss("illustration.css");
        setBodyCssClass("global-body-ToolWindow");
        addJsLib(IllustrationJsLibs.UPLOADCONFIRM);
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("appelant", appelant)
                .put("tmpFileName", tmpFileName)
                .put("imagePath", ConfigurationUtils.getTmpRelativeUrl("mini-" + tmpFileName));
        startLoc("_ title.album.replaceconfirm");
        this
                .SCRIPT()
                .__jsObject("Illustration.UploadConfirm.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId).classes("illustration-Client"))
                ._DIV();
        end();
    }

}
