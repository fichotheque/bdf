/* BdfServer_HtmlProducers - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.album.Illustration;
import net.fichotheque.croisement.CroisementsBySubset;
import net.fichotheque.eligibility.SubsetEligibility;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.tools.permission.PermissionSubsetEligibility;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationAdminFormHtmlProducer extends BdfServerHtmlProducer {


    private final Illustration illustration;
    private final PermissionSummary permissionSummary;
    private final boolean isSubsetAdmin;

    public IllustrationAdminFormHtmlProducer(BdfParameters bdfParameters, Illustration illustration) {
        super(bdfParameters);
        this.illustration = illustration;
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.isSubsetAdmin = permissionSummary.isSubsetAdmin(illustration.getSubsetKey());
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.APPELANT);
        addThemeCss("album.css");
        addStyle(AlbumHtmlUtils.getThumbnailSize(illustration.getAlbum().getAlbumMetadata()));
    }

    @Override
    public void printHtml() {
        SubsetEligibility subsetEligibity = PermissionSubsetEligibility.read(permissionSummary);
        List<CroisementsBySubset> croisementsBySubsetList = CroisementUtils.filterCroisements(illustration, subsetEligibity, SubsetKey.CATEGORY_CORPUS);
        start();
        AlbumHtmlUtils.printIllustrationToolbar(this, AlbumDomain.ILLUSTRATION_ADMINFORM_PAGE, illustration, isSubsetAdmin);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ALBUM)
                .family("ALB")
                .veil(true)
                .page(AlbumDomain.ILLUSTRATION_ADMINFORM_PAGE);
        if (!croisementsBySubsetList.isEmpty()) {
            AlbumCommandBoxUtils.printCroisementRemoveBox(this, commandBox, illustration, croisementsBySubsetList, permissionSummary, bdfUser);
        }
        AlbumCommandBoxUtils.printCroisementAddBox(this, commandBox, illustration);
        commandBox
                .errorPage(AlbumDomain.ILLUSTRATION_ADMINFORM_PAGE)
                .page(InteractionConstants.MESSAGE_PAGE);
        AlbumCommandBoxUtils.printIllustrationRemoveBox(this, commandBox, illustration);
        end();
    }

}
