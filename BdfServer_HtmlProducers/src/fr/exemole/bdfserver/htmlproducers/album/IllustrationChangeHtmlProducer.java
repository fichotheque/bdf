/* BdfServer_HtmlProducers - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.commands.album.AlbumCommandUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.IllustrationJsLibs;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationChangeHtmlProducer extends BdfServerHtmlProducer {

    private final Album album;
    private Illustration illustration;
    private final String appelant;
    private String tmpFileName;
    private String changeType;

    private IllustrationChangeHtmlProducer(BdfParameters bdfParameters, Album album, String appelant) {
        super(bdfParameters);
        addThemeCss("illustration.css");
        setBodyCssClass("global-body-ToolWindow");
        this.album = album;
        this.appelant = appelant;
        addJsLib(IllustrationJsLibs.CHANGE);
    }

    public static IllustrationChangeHtmlProducer newTmpFileCreate(BdfParameters bdfParameters, Album album, String appelant, String tmpFileName) {
        IllustrationChangeHtmlProducer htmlProducer = new IllustrationChangeHtmlProducer(bdfParameters, album, appelant);
        htmlProducer.tmpFileName = tmpFileName;
        htmlProducer.changeType = AlbumDomain.CREATION_CHANGETYPE;
        return htmlProducer;
    }

    public static IllustrationChangeHtmlProducer newTmpFileChange(BdfParameters bdfParameters, Album album, String appelant, String tmpFileName) {
        IllustrationChangeHtmlProducer htmlProducer = new IllustrationChangeHtmlProducer(bdfParameters, album, appelant);
        htmlProducer.tmpFileName = tmpFileName;
        htmlProducer.changeType = AlbumDomain.TMPFILE_CHANGETYPE;
        return htmlProducer;
    }

    public static IllustrationChangeHtmlProducer newIllustrationChange(BdfParameters bdfParameters, Album album, String appelant, Illustration illustration, String tmpFileName) {
        IllustrationChangeHtmlProducer htmlProducer = new IllustrationChangeHtmlProducer(bdfParameters, album, appelant);
        htmlProducer.illustration = illustration;
        htmlProducer.tmpFileName = tmpFileName;
        htmlProducer.changeType = AlbumDomain.ILLUSTRATION_CHANGETYPE;
        return htmlProducer;
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("clientId", clientId)
                .put("changeType", changeType)
                .put("appelant", appelant)
                .put("tmpFileName", getTmpFileName())
                .put("album", album.getSubsetName())
                .put("illustrationId", getIllustrationId())
                .put("imagePath", getImagePath())
                .put("originalFileName", getOriginalFileName());
        startLoc("_ title.album.illustration");
        this
                .SCRIPT()
                .__jsObject("Illustration.Change.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId).classes("illustration-Client"))
                ._DIV();
        end();
    }

    private Integer getIllustrationId() {
        if (illustration != null) {
            return illustration.getId();
        } else {
            return null;
        }
    }

    private String getImagePath() {
        if (tmpFileName != null) {
            return ConfigurationUtils.getTmpRelativeUrl(tmpFileName);
        } else {
            return "illustrations/" + illustration.getFileName();
        }
    }

    private String getTmpFileName() {
        if (tmpFileName != null) {
            return tmpFileName;
        }
        return AlbumCommandUtils.ORIGINAL_TMPFILENAME;
    }

    private String getOriginalFileName() {
        if (illustration != null) {
            return illustration.getFileName();
        } else {
            return null;
        }
    }


}
