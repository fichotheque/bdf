/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.roles.PermissionCheck;
import java.io.File;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class AlbumHtmlProducerFactory {

    private final static int ALBUM_ADMIN = 1;
    private final static int ALBUM_READ = 2;
    private final static int ILLUSTRATION = 3;

    private AlbumHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        RequestMap requestMap = parameters.getRequestMap();
        switch (getPageType(page)) {
            case ALBUM_ADMIN: {
                Album album = requestHandler.getAlbum();
                parameters.checkSubsetAdmin(album);
                switch (page) {
                    case AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE: {
                        return new AlbumAdvancedCommandsHtmlProducer(parameters, album);
                    }
                    case AlbumDomain.ALBUM_METADATAFORM_PAGE: {
                        return new AlbumMetadataFormHtmlProducer(parameters, album);
                    }
                    default:
                        return null;
                }
            }
            case ALBUM_READ: {
                Album album = requestHandler.getAlbum();
                switch (page) {
                    case AlbumDomain.ALBUM_PAGE: {
                        return new AlbumHtmlProducer(parameters, album);
                    }
                    case AlbumDomain.ILLUSTRATION_UPLOAD_NEW_PAGE: {
                        String appelant = getAppelant(requestMap);
                        return IllustrationUploadHtmlProducer.newNewUpload(parameters, album, appelant);
                    }
                    case AlbumDomain.ILLUSTRATION_UPLOAD_REPLACE_PAGE: {
                        String appelant = getAppelant(requestMap);
                        return IllustrationUploadHtmlProducer.newReplaceUpload(parameters, album, appelant);
                    }
                    case AlbumDomain.ILLUSTRATION_CHANGE_PAGE: {
                        String appelant = getAppelant(requestMap);
                        File tmpFile = (File) parameters.getResultObject(BdfInstructionConstants.TMPFILE_OBJ);
                        if (tmpFile != null) {
                            return IllustrationChangeHtmlProducer.newTmpFileCreate(parameters, album, appelant, tmpFile.getName());
                        }
                        String changeType = requestHandler.getMandatoryParameter("changetype");
                        if (changeType.equals(AlbumDomain.TMPFILE_CHANGETYPE)) {
                            String tmpFileName = requestHandler.getMandatoryParameter("tmpfile");
                            return IllustrationChangeHtmlProducer.newTmpFileChange(parameters, album, appelant, tmpFileName);
                        } else if (changeType.equals(AlbumDomain.ILLUSTRATION_CHANGETYPE)) {
                            Illustration illustration = (Illustration) requestHandler.getMandatorySubsetItem(album);
                            String tmpFileName = requestMap.getParameter("tmpfile");
                            return IllustrationChangeHtmlProducer.newIllustrationChange(parameters, album, appelant, illustration, tmpFileName);
                        } else {
                            throw BdfErrors.unknownParameterValue("changetype", changeType);
                        }
                    }
                    default:
                        return null;
                }
            }
            case ILLUSTRATION: {
                Illustration illustration = requestHandler.getIllustration();
                switch (page) {
                    case AlbumDomain.ILLUSTRATION_ADMINFORM_PAGE: {
                        PermissionCheck.checkWrite(parameters.getPermissionSummary(), illustration);
                        return new IllustrationAdminFormHtmlProducer(parameters, illustration);
                    }
                    case AlbumDomain.ILLUSTRATION_ADVANCEDCOMMANDS_PAGE: {
                        parameters.checkSubsetAdmin(illustration.getAlbum());
                        return new IllustrationAdvancedCommandsHtmlProducer(parameters, illustration);
                    }
                    default:
                        return null;
                }
            }
            default: {
                switch (page) {
                    case AlbumDomain.ALBUM_CREATIONFORM_PAGE: {
                        parameters.checkFichothequeAdmin();
                        return new AlbumCreationFormHtmlProducer(parameters);
                    }
                    case AlbumDomain.ILLUSTRATION_UPLOAD_CONFIRM_PAGE: {
                        String appelant = getAppelant(requestMap);
                        File tmpFile = (File) parameters.getResultObject(BdfInstructionConstants.TMPFILE_OBJ);
                        if (tmpFile != null) {
                            return new IllustrationUploadConfirmHtmlProducer(parameters, appelant, tmpFile.getName());
                        } else {
                            return null;
                        }
                    }
                    default:
                        return null;
                }
            }
        }
    }

    private static int getPageType(String page) {
        switch (page) {
            case AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE:
            case AlbumDomain.ALBUM_METADATAFORM_PAGE:
                return ALBUM_ADMIN;
            case AlbumDomain.ALBUM_PAGE:
            case AlbumDomain.ILLUSTRATION_UPLOAD_NEW_PAGE:
            case AlbumDomain.ILLUSTRATION_UPLOAD_REPLACE_PAGE:
            case AlbumDomain.ILLUSTRATION_CHANGE_PAGE:
                return ALBUM_READ;
            case AlbumDomain.ILLUSTRATION_ADMINFORM_PAGE:
            case AlbumDomain.ILLUSTRATION_ADVANCEDCOMMANDS_PAGE:
                return ILLUSTRATION;
            default:
                return 0;
        }
    }

    private static String getAppelant(RequestMap requestMap) {
        return requestMap.getParameter("appelant");
    }

}
