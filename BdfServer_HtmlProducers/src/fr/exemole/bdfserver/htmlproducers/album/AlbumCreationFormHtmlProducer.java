/* BdfServer_HtmlProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class AlbumCreationFormHtmlProducer extends BdfServerHtmlProducer {

    public AlbumCreationFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("album.css");
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ALBUM)
                .family("ALB")
                .veil(true)
                .page(AlbumDomain.ALBUM_METADATAFORM_PAGE)
                .errorPage(AlbumDomain.ALBUM_CREATIONFORM_PAGE);
        AlbumCommandBoxUtils.printAlbumCreationBox(this, commandBox);
        end();
    }

}
