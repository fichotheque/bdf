/* BdfServer_HtmlProducers - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.attributes.Deploy;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.BH;
import java.util.List;
import net.fichotheque.SubsetItem;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.permission.PermissionEntry;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.tools.permission.ListFilterEngine;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class AlbumHtmlProducer extends BdfServerHtmlProducer {

    private final PermissionSummary permissionSummary;
    private final Album album;
    private final boolean isSubsetAdmin;
    private final Button linkButton = Button.link();

    public AlbumHtmlProducer(BdfParameters bdfParameters, Album album) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.album = album;
        this.isSubsetAdmin = permissionSummary.isSubsetAdmin(album.getSubsetKey());
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addThemeCss("album.css");
        setBodyCssClass("global-body-ListFrame");
        addStyle(AlbumHtmlUtils.getThumbnailSize(album.getAlbumMetadata()));
    }

    @Override
    public void printHtml() {
        List<PermissionEntry> permissionEntryList = null;
        int count;
        List<SubsetItem> illustrationList = album.getSubsetItemList();
        if (!illustrationList.isEmpty()) {
            permissionEntryList = ListFilterEngine.filter(album, illustrationList, permissionSummary);
            count = permissionEntryList.size();
        } else {
            count = 0;
        }
        start(FichothequeUtils.getTitle(album, workingLang), true);
        this
                .__(BdfHtmlUtils.startSubsetUnit(this, bdfParameters, album, AlbumDomain.ALBUM_PAGE))
                .__(printCommands())
                .__(PageUnit.END);
        if (count > 0) {
            this
                    .__(PageUnit.start().sectionCss("unit-Unit album-IllustrationListUnit").title(() -> {
                        this
                                .__localize("_ title.album.illustrations")
                                .__space()
                                .__(BdfHtmlUtils.printItemCount(this, bdfUser, count));
                    }))
                    .UL("subsetitem-List")
                    .__(printIllustrationList(permissionEntryList))
                    ._UL()
                    .__(PageUnit.END);
        }
        end();
    }

    private boolean printCommands() {
        if (!isSubsetAdmin) {
            return false;
        }
        this
                .DETAILS(HA.id("details_commands").open(true).classes("tools-Details").populate(Deploy.DETAILS))
                .SUMMARY()
                .__localize("_ title.global.commands")
                ._SUMMARY()
                .DIV("tools-List")
                .__(link("action-Metadata", AlbumDomain.ALBUM_METADATAFORM_PAGE, "_ link.album.albummetadataform"))
                .__(link("action-Advanced", AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE, "_ link.album.albumadvancedcommands"))
                .__(link("action-Refresh", AlbumDomain.ALBUM_PAGE, "_ link.global.reload"))
                ._DIV()
                ._DETAILS();
        return true;
    }

    private Button link(String action, String page, String messageKey) {
        String href = BH.domain(Domains.ALBUM).subset(album).page(page).toString();
        String target;
        if (page.equals(AlbumDomain.ALBUM_PAGE)) {
            target = null;
        } else {
            target = BdfHtmlConstants.EDITION_FRAME;
        }
        return linkButton.href(href).action(action).textL10nObject(messageKey).target(target);
    }

    private boolean printIllustrationList(List<PermissionEntry> permissionEntryList) {
        for (PermissionEntry permissionEntry : permissionEntryList) {
            this
                    .LI()
                    .__(printIllustration((Illustration) permissionEntry.getSubsetItem(), permissionEntry.isEditable()))
                    ._LI();
        }
        return true;
    }

    private boolean printIllustration(Illustration illustration, boolean editable) {
        String fileName = illustration.getFileName();
        this
                .DIV("album-Thumbnail")
                .IMG(HA.src("illustrations/_mini/" + fileName))
                ._DIV()
                .DIV("album-Text")
                .DIV("subsetitem-Title")
                .P()
                .A(HA.href("illustrations/" + fileName).target(BdfHtmlConstants.EDITION_FRAME))
                .__escape(fileName)
                ._A()
                ._P()
                .__(printIllustrationEdit(editable, illustration))
                ._DIV()
                .__(printIllustrationInfo(illustration))
                ._DIV();
        return true;
    }

    private boolean printIllustrationEdit(boolean editable, Illustration illustration) {
        if (!editable) {
            return false;
        }
        this
                .A(HA.href(BH.domain(Domains.ALBUM).subsetItem(illustration).page(AlbumDomain.ILLUSTRATION_ADMINFORM_PAGE)).target(BdfHtmlConstants.EDITION_FRAME).classes("button-Circle action-IllustrationEdit"))
                .__(Button.ICON)
                ._A();
        return true;
    }

    private boolean printIllustrationInfo(Illustration illustration) {
        this
                .DIV("subsetitem-Infos")
                .__escape(String.valueOf(illustration.getOriginalWidth()))
                .__escape("\u00d7")
                .__escape(String.valueOf(illustration.getOriginalHeight()))
                ._DIV();
        return true;
    }

}
