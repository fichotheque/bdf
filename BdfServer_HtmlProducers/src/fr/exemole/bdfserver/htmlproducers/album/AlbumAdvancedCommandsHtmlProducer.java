/* BdfServer_HtmlProducers - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.album;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AlbumDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.album.Album;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class AlbumAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Album album;

    public AlbumAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Album album) {
        super(bdfParameters);
        this.album = album;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("album.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, album, AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE);
        AlbumHtmlUtils.printAlbumToolbar(this, AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE, album);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.ALBUM)
                .family("ALB")
                .veil(true);
        if ((bdfParameters.isFichothequeAdmin()) && (album.isRemoveable())) {
            commandBox
                    .errorPage(AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE)
                    .page(InteractionConstants.MESSAGE_PAGE);
            AlbumCommandBoxUtils.printAlbumRemoveBox(this, commandBox, album);
        }
        commandBox
                .errorPage(null)
                .page(AlbumDomain.ALBUM_ADVANCEDCOMMANDS_PAGE);
        AlbumCommandBoxUtils.printAlbumAttributeChangeBox(this, commandBox, album);
    }

}
