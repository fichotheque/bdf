/* BdfServer_HtmlProducers - Copyright (c) 2016-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class FieldPiocheHtmlProducer extends BdfServerHtmlProducer {

    private final PiocheParameters piocheParameters;
    private final String type;

    public FieldPiocheHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.piocheParameters = piocheParameters;
        addThemeCss("pioche.css");
        addJsLib(MiscJsLibs.PIOCHE);
        Object fieldtypeParam = piocheParameters.getParameter(PiocheParameters.FIELDTYPE);
        if (fieldtypeParam != null) {
            this.type = fieldtypeParam.toString();
        } else {
            this.type = "all";
        }
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        int count;
        switch (type) {
            case PiocheDomain.DATATION_FIELDTYPE:
                count = getDatationCount();
                break;
            default:
                count = getAllCount();
        }
        startLoc("_ title.pioche.field");
        PiocheArgs piocheArgs = PiocheArgs.init()
                .clientId(clientId)
                .populate(piocheParameters)
                .count(count)
                .separator(" = ")
                .wanted(PiocheDomain.CODE_ID_WANTED)
                .json(PiocheDomain.FIELD_JSON)
                .fieldtype(type);
        this
                .__(piocheArgs)
                .DIV(HA.id(clientId).classes("pioche-Client"))
                ._DIV();
        end();
    }

    private int getDatationCount() {
        Set<FieldKey> fieldSet = new LinkedHashSet();
        for (Corpus corpus : fichotheque.getCorpusList()) {
            CorpusMetadata corpusMetadata = corpus.getCorpusMetadata();
            for (CorpusField corpusField : corpusMetadata.getProprieteList()) {
                if (corpusField.getFicheItemType() == CorpusField.DATATION_FIELD) {
                    fieldSet.add(corpusField.getFieldKey());
                }
            }
            for (CorpusField corpusField : corpusMetadata.getInformationList()) {
                if (corpusField.getFicheItemType() == CorpusField.DATATION_FIELD) {
                    fieldSet.add(corpusField.getFieldKey());
                }
            }
        }
        return fieldSet.size();
    }

    private int getAllCount() {
        Set<FieldKey> fieldSet = new LinkedHashSet();
        boolean withSoustitre = false;
        for (Corpus corpus : fichotheque.getCorpusList()) {
            CorpusMetadata corpusMetadata = corpus.getCorpusMetadata();
            for (CorpusField corpusField : corpusMetadata.getProprieteList()) {
                fieldSet.add(corpusField.getFieldKey());
            }
            for (CorpusField corpusField : corpusMetadata.getInformationList()) {
                fieldSet.add(corpusField.getFieldKey());
            }
            for (CorpusField corpusField : corpusMetadata.getSectionList()) {
                fieldSet.add(corpusField.getFieldKey());
            }
            if (!withSoustitre) {
                if (corpusMetadata.isWithSoustitre()) {
                    withSoustitre = true;
                }
            }
        }
        int size = 3 + fieldSet.size();
        if (withSoustitre) {
            size = size + 1;
        }
        return size;
    }

}
