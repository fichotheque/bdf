/* BdfServer_HtmlProducers - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.Subset;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class FichePiocheHtmlProducer extends BdfServerHtmlProducer {

    private final PiocheParameters piocheParameters;
    private final PermissionSummary permissionSummary;

    public FichePiocheHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.piocheParameters = piocheParameters;
        addThemeCss("pioche.css");
        addJsLib(MiscJsLibs.PIOCHE);
    }

    @Override
    public void printHtml() {
        List<Subset> corpusList = piocheParameters.getSubsetList();
        Set<String> allowedButtonSet = null;
        String clientId = generateId();
        String wanted = PiocheDomain.CODE_ID_WANTED;
        String newficheLabel = null;
        if ((corpusList != null) && (corpusList.size() == 1)) {
            wanted = PiocheDomain.ID_WANTED;
            Corpus corpus = (Corpus) corpusList.get(0);
            if (permissionSummary.canCreate(corpus)) {
                allowedButtonSet = Collections.singleton("fichecreation");
                newficheLabel = FichothequeUtils.getPhraseLabel(corpus.getCorpusMetadata().getPhrases(), FichothequeConstants.NEWFICHE_PHRASE, workingLang);
            }
        }
        PiocheArgs piocheArgs = PiocheArgs.init()
                .clientId(clientId)
                .populate(piocheParameters, allowedButtonSet)
                .count(getCount(corpusList))
                .separator(" – ")
                .wanted(wanted)
                .json(PiocheDomain.FICHE_JSON)
                .subsets(corpusList)
                .newfiche(newficheLabel);
        startLoc("_ title.pioche.fiche");
        this
                .__(piocheArgs)
                .DIV(HA.id(clientId).classes("pioche-Client"))
                ._DIV();
        end();
    }

    private int getCount(List<Subset> corpusList) {
        int count = 0;
        if (corpusList == null) {
            for (Corpus corpus : fichotheque.getCorpusList()) {
                count = count + corpus.size();
            }
        } else {
            for (Subset subset : corpusList) {
                count = count + subset.size();
            }
        }
        return count;
    }

}
