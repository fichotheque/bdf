/* BdfServer_HtmlProducers - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.addenda.Addenda;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class DocumentPiocheHtmlProducer extends BdfServerHtmlProducer {

    private final PiocheParameters piocheParameters;

    public DocumentPiocheHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.piocheParameters = piocheParameters;
        addThemeCss("pioche.css");
        addJsLib(MiscJsLibs.PIOCHE);
    }

    @Override
    public void printHtml() {
        List<Subset> addendaList = piocheParameters.getSubsetList();
        String clientId = generateId();
        String wanted = PiocheDomain.CODE_ID_WANTED;
        if ((addendaList != null) && (addendaList.size() == 1)) {
            wanted = PiocheDomain.ID_WANTED;
        }
        PiocheArgs piocheArgs = PiocheArgs.init()
                .clientId(clientId)
                .populate(piocheParameters)
                .count(getCount(addendaList))
                .separator(" - ")
                .wanted(wanted)
                .json(PiocheDomain.DOCUMENT_JSON)
                .subsets(addendaList);
        startLoc("_ title.pioche.document");
        this
                .__(piocheArgs)
                .DIV(HA.id(clientId).classes("pioche-Client"))
                ._DIV();
        end();
    }

    private int getCount(List<Subset> addendaList) {
        int count = 0;
        if (addendaList == null) {
            for (Addenda addenda : fichotheque.getAddendaList()) {
                count = count + addenda.size();
            }
        } else {
            for (Subset subset : addendaList) {
                count = count + subset.size();
            }
        }
        return count;
    }

}
