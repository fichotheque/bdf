/* BdfServer_HtmlProducers - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class LanguePiocheHtmlProducer extends BdfServerHtmlProducer {

    private final PiocheParameters piocheParameters;

    public LanguePiocheHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.piocheParameters = piocheParameters;
        addThemeCss("pioche.css");
        addJsLib(MiscJsLibs.PIOCHE);
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        String wanted = PiocheDomain.CODE_ID_WANTED;
        int count = bdfServer.getL10nManager().getCodeCatalog().getLangCodeSet().size();
        PiocheArgs piocheArgs = PiocheArgs.init()
                .clientId(clientId)
                .populate(piocheParameters)
                .count(count)
                .separator(" = ")
                .wanted(PiocheDomain.CODE_ID_WANTED)
                .json(PiocheDomain.LANGUE_JSON);
        startLoc("_ title.pioche.langue");
        this
                .__(piocheArgs)
                .DIV(HA.id(clientId).classes("pioche-Client"))
                ._DIV();
        end();
    }

}
