/* BdfServer_HtmlProducers - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class MotclePiocheHtmlProducer extends BdfServerHtmlProducer {

    private final PiocheParameters piocheParameters;
    private final String wanted;

    public MotclePiocheHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.piocheParameters = piocheParameters;
        String wantedParam = (String) piocheParameters.getParameter(PiocheParameters.WANTED);
        if (wantedParam != null) {
            switch (wantedParam) {
                case PiocheDomain.ID_WANTED:
                case PiocheDomain.CODE_ID_WANTED:
                    wanted = wantedParam;
                    break;
                default:
                    wanted = PiocheDomain.CODE_TITLE_WANTED;
            }
        } else {
            wanted = PiocheDomain.CODE_TITLE_WANTED;
        }
        addThemeCss("pioche.css");
        addJsLib(MiscJsLibs.PIOCHE);
    }

    @Override
    public void printHtml() {
        List<Subset> thesaurusList = piocheParameters.getSubsetList();
        String clientId = generateId();
        String title;
        if ((thesaurusList != null) && (thesaurusList.size() == 1)) {
            title = getLocalization("_ title.pioche.motcle_one", FichothequeUtils.getTitle(thesaurusList.get(0), workingLang));
        } else {
            title = getLocalization("_ title.pioche.motcle");
        }
        start(title);
        PiocheArgs piocheArgs = PiocheArgs.init()
                .clientId(clientId)
                .populate(piocheParameters)
                .count(getTotal(thesaurusList))
                .separator(" – ")
                .wanted(wanted)
                .json(PiocheDomain.MOTCLE_JSON)
                .subsets(thesaurusList);
        this
                .__(piocheArgs)
                .DIV(HA.id(clientId).classes("pioche-Client"))
                ._DIV();
        end();
    }

    private int getTotal(List<Subset> thesaurusList) {
        int count = 0;
        if (thesaurusList == null) {
            for (Thesaurus thesaurus : fichotheque.getThesaurusList()) {
                count = count + thesaurus.size();
            }
        } else {
            for (Subset subset : thesaurusList) {
                count = count + subset.size();
            }
        }
        return count;
    }

}
