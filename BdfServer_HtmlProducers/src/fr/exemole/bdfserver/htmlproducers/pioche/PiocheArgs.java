/* BdfServer_HtmlProducers - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class PiocheArgs implements Consumer<HtmlPrinter> {

    private final JsObject args;
    private final JsObject buttons;
    private final List<JsObject> hiddenValues = new ArrayList<JsObject>();

    public PiocheArgs() {
        this.args = new JsObject();
        this.buttons = new JsObject();
    }

    public PiocheArgs clientId(String clientId) {
        args.put("clientId", clientId);
        return this;
    }

    public PiocheArgs json(String json) {
        args.put("json", json);
        return this;
    }

    public PiocheArgs count(int count) {
        args.put("count", count);
        return this;
    }

    public PiocheArgs wanted(String wanted) {
        args.put("wanted", wanted);
        return this;
    }

    public PiocheArgs separator(String separator) {
        args.put("separator", separator);
        return this;
    }

    public PiocheArgs newfiche(String newficheLabel) {
        if (newficheLabel != null) {
            args.put("newfiche", newficheLabel);
        }
        return this;
    }

    public PiocheArgs subsets(String subsets) {
        if (subsets != null) {
            addHiddenValue(PiocheDomain.SUBSETS_PARAMNAME, subsets);
        }
        return this;
    }


    public PiocheArgs subsets(List<Subset> subsetList) {
        if ((subsetList != null) && (!subsetList.isEmpty())) {
            StringBuilder buf = new StringBuilder();
            int size = subsetList.size();
            for (int i = 0; i < size; i++) {
                Subset subset = subsetList.get(i);
                if (i > 0) {
                    buf.append(",");
                }
                buf.append(subset.getSubsetName());
            }
            addHiddenValue(PiocheDomain.SUBSETS_PARAMNAME, buf.toString());
        }
        return this;
    }

    public PiocheArgs defaultsphere(String defaultSphere) {
        if (defaultSphere != null) {
            addHiddenValue(PiocheDomain.DEFAULSPHERE_PARAMNAME, defaultSphere);
        }
        return this;
    }

    public PiocheArgs fieldtype(String fieldType) {
        if (fieldType != null) {
            addHiddenValue(PiocheDomain.FIELDTYPE_PARAMNAME, fieldType);
        }
        return this;
    }

    public PiocheArgs populate(PiocheParameters piocheParameters) {
        return populate(piocheParameters, null);
    }

    public PiocheArgs populate(PiocheParameters piocheParameters, Set<String> allowedButtonSet) {
        args
                .put("appelant", piocheParameters.getAppelant())
                .put("limit", piocheParameters.getLimit());
        SubsetKey croisementKey = (SubsetKey) piocheParameters.getParameter(PiocheParameters.CROISEMENT);
        if (croisementKey != null) {
            addHiddenValue(PiocheDomain.CROISEMENT_PARAMNAME, croisementKey.getKeyString());
        }
        if (allowedButtonSet != null) {
            String buttonsString = (String) piocheParameters.getParameter(PiocheParameters.BUTTONS);
            if (buttonsString != null) {
                for (String token : StringUtils.getTechnicalTokens(buttonsString, true)) {
                    if (allowedButtonSet.contains(token)) {
                        buttons.put(token, Boolean.TRUE);
                    }
                }
            }
        }
        return this;
    }

    private void addHiddenValue(String name, String value) {
        hiddenValues.add(JsObject.init().put("name", name).put("value", value));
    }

    public static PiocheArgs init() {
        return new PiocheArgs();
    }


    @Override
    public void accept(HtmlPrinter hp) {
        args.put("buttons", buttons);
        args.put("hiddenValues", hiddenValues);
        hp
                .SCRIPT()
                .__jsObject("Pioche.ARGS", args)
                ._SCRIPT();
    }

}
