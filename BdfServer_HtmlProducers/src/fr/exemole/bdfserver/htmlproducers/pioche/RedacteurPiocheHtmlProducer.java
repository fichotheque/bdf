/* BdfServer_HtmlProducers - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurPiocheHtmlProducer extends BdfServerHtmlProducer {

    private final PiocheParameters piocheParameters;
    private final String defaultSphere;

    public RedacteurPiocheHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.piocheParameters = piocheParameters;
        SubsetKey defaultSphereKey = (SubsetKey) piocheParameters.getParameter(PiocheParameters.DEFAULTSPHEREKEY);
        if (defaultSphereKey != null) {
            defaultSphere = defaultSphereKey.getKeyString();
        } else {
            defaultSphere = null;
        }
        addThemeCss("pioche.css");
        addJsLib(MiscJsLibs.PIOCHE);
    }

    @Override
    public void printHtml() {
        String clientId = generateId();
        startLoc("_ title.pioche.redacteur");
        PiocheArgs piocheArgs = PiocheArgs.init()
                .clientId(clientId)
                .populate(piocheParameters)
                .count(getCount())
                .separator(" = ")
                .wanted(PiocheDomain.CODE_ID_WANTED)
                .json(PiocheDomain.REDACTEUR_JSON)
                .defaultsphere(defaultSphere);
        this
                .__(piocheArgs)
                .DIV(HA.id(clientId).classes("pioche-Client"))
                ._DIV();
        end();
    }

    private int getCount() {
        int count = 0;
        for (Sphere sphere : fichotheque.getSphereList()) {
            count = count + sphere.size();
        }
        return count;
    }

}
