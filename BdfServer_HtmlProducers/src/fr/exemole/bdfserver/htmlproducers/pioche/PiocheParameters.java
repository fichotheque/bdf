/* BdfServer_HtmlProducers - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.Subset;


/**
 *
 * @author Vincent Calame
 */
public class PiocheParameters {

    public final static String EXTERNALSOURCE = "externalsource";
    public final static String CROISEMENT = "croisement";
    public final static String DEFAULTSPHEREKEY = "defaultspherekey";
    public final static String LAT = "lat";
    public final static String LON = "lon";
    public final static String ZOOM = "zoom";
    public final static String NOCURRENT = "nocurrent";
    public final static String WANTED = "wanted";
    public final static String ADDRESSFIELDS = "addressfields";
    public final static String FIELDTYPE = "fieldtype";
    public final static String BUTTONS = "buttons";
    private String appelant;
    private List<Subset> subset;
    private int limit;
    private short getType;
    private final Map<String, Object> parameterMap = new HashMap<String, Object>();

    public PiocheParameters() {
    }

    public String getAppelant() {
        return appelant;
    }

    public PiocheParameters setAppelant(String appelant) {
        this.appelant = appelant;
        return this;
    }

    public List<Subset> getSubsetList() {
        return subset;
    }

    public PiocheParameters setSubsetList(List<Subset> subset) {
        this.subset = subset;
        return this;
    }

    public int getLimit() {
        return limit;
    }

    public PiocheParameters setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    public short getGetType() {
        return getType;
    }

    public PiocheParameters setGetType(short getType) {
        this.getType = getType;
        return this;
    }

    public Object getParameter(String paramName) {
        return parameterMap.get(paramName);
    }

    public PiocheParameters putParameter(String paramName, Object paramValue) {
        if ((paramValue == null) || (paramValue.equals(""))) {
            parameterMap.remove(paramName);
        } else {
            parameterMap.put(paramName, paramValue);
        }
        return this;
    }

    public static PiocheParameters init() {
        return new PiocheParameters();
    }

}
