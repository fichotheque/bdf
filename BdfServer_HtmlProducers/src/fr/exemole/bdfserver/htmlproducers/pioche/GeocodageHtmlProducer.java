/* BdfServer_HtmlProducers - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.GeoJsLibs;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class GeocodageHtmlProducer extends BdfServerHtmlProducer {

    private final static String[] EMPTY_ARRAY = new String[0];
    private final PiocheParameters piocheParameters;
    private final DegreDecimal latDecimal;
    private final DegreDecimal lonDecimal;
    private final String appelant;
    private final String[] addressArray;

    public GeocodageHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.piocheParameters = piocheParameters;
        appelant = piocheParameters.getAppelant();
        latDecimal = (DegreDecimal) piocheParameters.getParameter(PiocheParameters.LAT);
        lonDecimal = (DegreDecimal) piocheParameters.getParameter(PiocheParameters.LON);
        String adressFields = (String) piocheParameters.getParameter(PiocheParameters.ADDRESSFIELDS);
        if (adressFields != null) {
            addressArray = StringUtils.getTechnicalTokens(adressFields, true);
        } else {
            addressArray = EMPTY_ARRAY;
        }
        addThemeCss("geocodage.css");
        addJsLib(GeoJsLibs.PIOCHE);
    }

    @Override
    public void printHtml() {
        String latitudeId;
        String longitudeId;
        int idx = appelant.indexOf(',');
        if (idx > 0) {
            latitudeId = appelant.substring(0, idx);
            longitudeId = appelant.substring(idx + 1);
        } else {
            latitudeId = appelant;
            longitudeId = "";
        }
        String clientId = generateId();
        JsObject piocheArgs = JsObject.init()
                .put("clientId", clientId)
                .put("latitudeId", latitudeId)
                .put("longitudeId", longitudeId)
                .put("addressArray", addressArray);
        if ((latDecimal != null) && (lonDecimal != null)) {
            piocheArgs.put("start", getStartObject());
        }
        startLoc("_ title.pioche.geocodage");
        this
                .SCRIPT()
                .__jsObject("Geo.Geocodage.ARGS", piocheArgs)
                ._SCRIPT();
        this
                .DIV(HA.id(clientId).classes("geocodage-Client"))
                ._DIV();
        end();
    }

    private JsObject getStartObject() {
        JsObject startObject = JsObject.init()
                .put("latitude", latDecimal.toString())
                .put("longitude", lonDecimal.toString());
        String zoom = (String) piocheParameters.getParameter(PiocheParameters.ZOOM);
        if (zoom != null) {
            try {
                int zoomInt = Integer.parseInt(zoom);
                if (zoomInt > 0) {
                    startObject.put("zoom", zoomInt);
                }
            } catch (NumberFormatException nfe) {

            }
        }
        Boolean noCurrent = (Boolean) piocheParameters.getParameter(PiocheParameters.NOCURRENT);
        if (noCurrent != null) {
            startObject.put("noCurrent", noCurrent);
        }
        return startObject;
    }

}
