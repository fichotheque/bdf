/* BdfServer_HtmlProducers - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.pioche;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import net.fichotheque.externalsource.ExternalSource;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class ExternalSourceHtmlProducer extends BdfServerHtmlProducer {

    private final PiocheParameters piocheParameters;
    private final ExternalSource externalSource;
    private final Thesaurus thesaurus;

    public ExternalSourceHtmlProducer(BdfParameters bdfParameters, PiocheParameters piocheParameters) {
        super(bdfParameters);
        this.externalSource = (ExternalSource) piocheParameters.getParameter(PiocheParameters.EXTERNALSOURCE);
        this.thesaurus = (Thesaurus) piocheParameters.getSubsetList().get(0);
        this.piocheParameters = piocheParameters;
        addThemeCss("pioche.css");
        addJsLib(MiscJsLibs.PIOCHE);
    }

    @Override
    public void printHtml() {
        String wanted;
        if (externalSource.isWithId()) {
            wanted = PiocheDomain.ID_WANTED;
        } else {
            wanted = PiocheDomain.CODE_ID_WANTED;
        }
        String clientId = generateId();
        String title = getLocalization("_ link.pioche.externalsource");
        PiocheArgs piocheArgs = PiocheArgs.init()
                .clientId(clientId)
                .populate(piocheParameters)
                .count(-1)
                .separator(" – ")
                .wanted(wanted)
                .json(PiocheDomain.EXTERNALITEM_JSON)
                .subsets(thesaurus.getSubsetName());
        start(title);
        this
                .__(piocheArgs)
                .DIV(HA.id(clientId).classes("pioche-Client"))
                ._DIV();
        end();
    }

}
