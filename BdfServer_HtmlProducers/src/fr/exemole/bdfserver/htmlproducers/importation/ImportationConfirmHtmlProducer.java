/* BdfServer_HtmlProducers - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.importation;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ImportationDomain;
import fr.exemole.bdfserver.commands.importation.ImportationEngineCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.ParameterMap;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import fr.exemole.bdfserver.tools.importation.ImportationEngine;
import java.io.File;
import java.util.function.Consumer;
import net.fichotheque.importation.ParseResult;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.html.HtmlPrinter;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class ImportationConfirmHtmlProducer extends BdfServerHtmlProducer {

    private final static Consumer<HtmlPrinter> CONFIRM_SUBMIT = Button.submit("_ submit.importation.confirm");
    private final ParseResult parseResult;

    public ImportationConfirmHtmlProducer(BdfParameters bdfParameters, ParseResult parseResult) {
        super(bdfParameters);
        this.parseResult = parseResult;
        addJsLib(MiscJsLibs.IMPORTATION);
        addThemeCss("importation.css");
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        if (parseResult != null) {
            this
                    .__(PageUnit.start("_ title.importation.confirm"));
            ImportationHtmlUtils.printParseResult(this, parseResult);
            File tmpFile = parseResult.getTmpFile();
            File tmpDir = parseResult.getTmpDir();
            if (((tmpFile != null) || (tmpDir != null)) && (isAllCorrect(parseResult))) {
                this
                        .P()
                        .__localize("_ info.importation.allcorrect")
                        ._P();
                String type = parseResult.getType();
                ParameterMap parameterMap = ParameterMap.init()
                        .command(ImportationEngineCommand.COMMANDNAME)
                        .page(getPage(type))
                        .param(ImportationEngineCommand.TYPE_PARAMNAME, type);
                if (tmpFile != null) {
                    parameterMap.param(ImportationEngineCommand.TMPFILE_PARAMNAME, tmpFile.getName());
                } else {
                    parameterMap.param(ImportationEngineCommand.TMPDIR_PARAMNAME, tmpDir.getName());
                }
                this
                        .FORM_post(Domains.IMPORTATION)
                        .INPUT_hidden(parameterMap)
                        .__(Button.COMMAND, CONFIRM_SUBMIT)
                        ._FORM();
            }
            this
                    .__(PageUnit.END);
        }
        end();
    }

    private static boolean isAllCorrect(ParseResult parseResult) {
        if (parseResult.getInitErrorCount() > 0) {
            return false;
        }
        if (parseResult.getInitWarningCount() > 0) {
            return false;
        }
        if (parseResult.getParseErrorCount() > 0) {
            return false;
        }
        if (parseResult.getBdfErrorCount() > 0) {
            return false;
        }
        return true;
    }

    private static String getPage(String type) {
        if (type.equals(ImportationEngine.LABEL_IMPORT)) {
            return ImportationDomain.LABELIMPORT_PAGE;
        }
        if (type.equals(ImportationEngine.THESAURUS_IMPORT)) {
            return ImportationDomain.THESAURUSIMPORT_PAGE;
        }
        if (type.equals(ImportationEngine.CORPUS_IMPORT)) {
            return ImportationDomain.CORPUSIMPORT_PAGE;
        }
        throw new SwitchException("Unknown type");
    }

}
