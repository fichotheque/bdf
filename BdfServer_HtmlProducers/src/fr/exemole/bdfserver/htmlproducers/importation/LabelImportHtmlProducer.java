/* BdfServer_HtmlProducers - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.importation;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ImportationDomain;
import fr.exemole.bdfserver.commands.importation.LabelImportParseCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlConstants;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.coupleparser.CoupleParser;
import fr.exemole.bdfserver.api.instruction.BdfParameters;


/**
 *
 * @author Vincent Calame
 */
public class LabelImportHtmlProducer extends BdfServerHtmlProducer {

    public LabelImportHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(MiscJsLibs.IMPORTATION);
        addThemeCss("importation.css");
        setMainStorageKey(Domains.IMPORTATION, ImportationDomain.LABELIMPORT_PAGE);
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.IMPORTATION)
                .family("CNF")
                .page(ImportationDomain.IMPORTATION_CONFIRM_PAGE).target(HtmlConstants.BLANK_TARGET)
                .name(LabelImportParseCommand.COMMANDNAME).lockey(LabelImportParseCommand.COMMANDKEY)
                .submitLocKey("_ submit.importation.parse");
        this
                .__start(commandBox)
                .__(printOdsLink())
                .__(Grid.START)
                .__(Grid.selectRow("_ label.importation.lang", name(LabelImportParseCommand.LANG_PARAMNAME),
                        () -> {
                            Lang currentLang = bdfUser.getWorkingLang();
                            for (Lang lang : bdfServer.getLangConfiguration().getWorkingLangs()) {
                                String langString = lang.toString();
                                boolean isSelected = false;
                                if (lang.equals(currentLang)) {
                                    isSelected = true;
                                }
                                this
                                        .OPTION(langString, isSelected)
                                        .__escape(langString)
                                        .__dash()
                                        .__localize(langString)
                                        ._OPTION();
                            }
                        }))
                .__(Grid.selectRow("_ label.importation.separator", name(LabelImportParseCommand.SEPARATOR_PARAMNAME),
                        () -> {
                            for (String val : CoupleParser.getAllowedCoupleSeparateurArrayToString()) {
                                this
                                        .OPTION(val, val.equals(""))
                                        .__escape(val)
                                        ._OPTION();
                            }
                        }))
                .__(Grid.textAreaBlockRow("_ label.importation.content", name(LabelImportParseCommand.CONTENT_PARAMNAME).cols(100).rows(30)))
                .__(Grid.END)
                .__end(commandBox);
        end();
    }

    private boolean printOdsLink() {
        this
                .P()
                .A(HA.href("admin/labels.ods"))
                .__localize("_ link.importation.labelimportods")
                ._A()
                ._P();
        return true;
    }

}
