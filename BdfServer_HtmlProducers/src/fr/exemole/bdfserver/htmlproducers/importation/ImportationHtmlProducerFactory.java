/* BdfServer_HtmlProducers - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.importation;

import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import static fr.exemole.bdfserver.api.instruction.BdfInstructionConstants.PARSERESULT_OBJ;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.ImportationDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import net.fichotheque.importation.ParseResult;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class ImportationHtmlProducerFactory {

    private ImportationHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        String page = parameters.getOutput();
        BdfCommandResult bdfCommandResult = parameters.getBdfCommandResult();
        parameters.checkFichothequeAdmin();
        switch (page) {
            case ImportationDomain.LABELIMPORT_PAGE: {
                return new LabelImportHtmlProducer(parameters);
            }
            case ImportationDomain.THESAURUSIMPORT_PAGE: {
                return new ThesaurusImportHtmlProducer(parameters);
            }
            case ImportationDomain.CORPUSIMPORT_PAGE: {
                return new CorpusImportHtmlProducer(parameters);
            }
            case ImportationDomain.IMPORTATION_CONFIRM_PAGE: {
                if (bdfCommandResult != null) {
                    ParseResult parseResult = (ParseResult) bdfCommandResult.getResultObject(PARSERESULT_OBJ);
                    return new ImportationConfirmHtmlProducer(parameters, parseResult);
                }
                return null;
            }
            default:
                return null;
        }
    }

}
