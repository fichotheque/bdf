/* BdfServer_HtmlProducers - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.importation;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ImportationDomain;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.commands.importation.CorpusImportParseCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.SubsetTreeOptions;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import java.util.Map;
import net.fichotheque.SubsetKey;
import net.fichotheque.importation.CorpusImport;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlConstants;


/**
 *
 * @author Vincent Calame
 */
public class CorpusImportHtmlProducer extends BdfServerHtmlProducer {

    public CorpusImportHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(MiscJsLibs.IMPORTATION);
        addThemeCss("importation.css");
    }

    @Override
    public void printHtml() {
        Map<String, String> storeMap = getStoredValues("form_import_corpus");
        String currentType = getCurrentType(storeMap);
        start();
        printCommandMessageUnit();
        SubsetTree corpusTree = bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_CORPUS);
        CommandBox commandBox = CommandBox.init()
                .action(Domains.IMPORTATION)
                .family("CRP")
                .page(ImportationDomain.IMPORTATION_CONFIRM_PAGE)
                .target(HtmlConstants.BLANK_TARGET)
                .multipart(true)
                .name(CorpusImportParseCommand.COMMANDNAME)
                .lockey(CorpusImportParseCommand.COMMANDKEY)
                .submitLocKey("_ submit.importation.parse");
        this
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.selectRow("_ label.importation.corpus", name(SubsetKey.CATEGORY_CORPUS_STRING).attr("data-subset", "selection"),
                        SubsetTreeOptions.init(corpusTree, bdfServer, workingLang)
                                .onlyNames(true)
                                .selectedSubsetKey(SubsetKey.CATEGORY_CORPUS, storeMap)
                                .withKeys(true))
                )
                .__(Grid.choiceSetRow("_ label.importation.corpusimporttype", () -> {
                    this
                            .__(printTypeRadio(CorpusImport.CREATION_TYPE, "_ label.importation.corpusimporttype_creation", currentType))
                            .__(printTypeRadio(CorpusImport.CHANGE_TYPE, "_ label.importation.corpusimporttype_change", currentType))
                            .__(printTypeRadio(CorpusImport.REMOVE_TYPE, "_ label.importation.corpusimporttype_remove", currentType));
                }))
                .__(ImportationHtmlUtils.printImportParseRows(this, storeMap))
                .__(Grid.END)
                .__end(commandBox);
        end();
    }

    private boolean printTypeRadio(String type, String messageKey, String current) {
        boolean checked = (type.equals(current));
        HtmlAttributes radio = name(CorpusImportParseCommand.TYPE_PARAMNAME).value(type).checked(checked);
        this
                .__(Grid.radioCell(messageKey, radio));
        return true;
    }

    private static String getCurrentType(Map<String, String> storeMap) {
        String defaultType = CorpusImport.CREATION_TYPE;
        String current = storeMap.get(CorpusImportParseCommand.TYPE_PARAMNAME);
        if (current == null) {
            return defaultType;
        } else {
            try {
                return CorpusImport.checkType(current);
            } catch (IllegalArgumentException iae) {
                return defaultType;
            }
        }
    }

}
