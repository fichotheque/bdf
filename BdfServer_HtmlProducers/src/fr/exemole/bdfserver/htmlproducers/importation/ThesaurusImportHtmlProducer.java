/* BdfServer_HtmlProducers - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.importation;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ImportationDomain;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.commands.importation.ThesaurusImportParseCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.SubsetTreeOptions;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import java.util.Map;
import net.fichotheque.SubsetKey;
import net.fichotheque.importation.ThesaurusImport;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlConstants;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusImportHtmlProducer extends BdfServerHtmlProducer {

    public ThesaurusImportHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(MiscJsLibs.IMPORTATION);
        addThemeCss("importation.css");
    }

    @Override
    public void printHtml() {
        Map<String, String> storeMap = getStoredValues("form_import_thesaurus");
        String currentType = getCurrentType(storeMap);
        start();
        printCommandMessageUnit();
        SubsetTree thesaurusTree = bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_THESAURUS);
        CommandBox commandBox = CommandBox.init()
                .action(Domains.IMPORTATION)
                .family("THS")
                .page(ImportationDomain.IMPORTATION_CONFIRM_PAGE).target(HtmlConstants.BLANK_TARGET)
                .name(ThesaurusImportParseCommand.COMMANDNAME).lockey(ThesaurusImportParseCommand.COMMANDKEY)
                .submitLocKey("_ submit.importation.parse");
        this
                .__start(commandBox)
                .__(Grid.START)
                .__(Grid.selectRow("_ label.importation.thesaurus", name(SubsetKey.CATEGORY_THESAURUS_STRING).attr("data-subset", "selection"),
                        SubsetTreeOptions.init(thesaurusTree, bdfServer, workingLang)
                                .onlyNames(true)
                                .selectedSubsetKey(SubsetKey.CATEGORY_THESAURUS, storeMap)
                                .withKeys(true))
                )
                .__(Grid.choiceSetRow("_ label.importation.thesaurusimporttype", () -> {
                    this
                            .__(printTypeRadio(ThesaurusImport.CREATION_TYPE, "_ label.importation.thesaurusimporttype_creation", currentType))
                            .__(printTypeRadio(ThesaurusImport.CHANGE_TYPE, "_ label.importation.thesaurusimporttype_change", currentType))
                            .__(printTypeRadio(ThesaurusImport.REMOVE_TYPE, "_ label.importation.thesaurusimporttype_remove", currentType))
                            .__(printTypeRadio(ThesaurusImport.MERGE_TYPE, "_ label.importation.thesaurusimporttype_merge", currentType))
                            .__(printMoveRadio(thesaurusTree, currentType));
                }))
                .__(ImportationHtmlUtils.printImportParseRows(this, storeMap))
                .__(Grid.END)
                .__end(commandBox);
        end();
    }

    private boolean printTypeRadio(String type, String messageKey, String current) {
        boolean checked = (current.equals(type));
        HtmlAttributes radio = name(ThesaurusImportParseCommand.TYPE_PARAMNAME).value(type).checked(checked);
        this
                .__(Grid.radioCell(messageKey, radio));
        return true;
    }

    private boolean printMoveRadio(SubsetTree thesaurusTree, String current) {
        boolean checked = (current.equals(ThesaurusImport.MOVE_TYPE));
        HtmlAttributes radio = name(ThesaurusImportParseCommand.TYPE_PARAMNAME).value(ThesaurusImport.MOVE_TYPE).checked(checked);
        this
                .__(Grid.radioCell("_ label.importation.thesaurusimporttype_move", radio,
                        () -> {
                            this
                                    .__(Grid.START)
                                    .__(Grid.selectRow("_ label.importation.destinationthesaurus", name(ThesaurusImportParseCommand.DESTINATIONTHESAURUS_PARAMNAME),
                                            SubsetTreeOptions.init(thesaurusTree, bdfServer, workingLang)
                                                    .onlyNames(true)
                                                    .withKeys(true)))
                                    .__(Grid.END);
                        }));
        return true;
    }

    private static String getCurrentType(Map<String, String> storeMap) {
        String defaultType = ThesaurusImport.CREATION_TYPE;
        String current = storeMap.get(ThesaurusImportParseCommand.TYPE_PARAMNAME);
        if (current == null) {
            return defaultType;
        }
        try {
            return ThesaurusImport.checkType(current);
        } catch (IllegalArgumentException iae) {
            return defaultType;
        }
    }


}
