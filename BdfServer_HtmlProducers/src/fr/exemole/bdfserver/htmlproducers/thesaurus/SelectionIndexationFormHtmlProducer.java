/* BdfServer_HtmlProducers - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.commands.thesaurus.SelectionIndexationCommand;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.ParameterMap;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import fr.exemole.bdfserver.tools.BH;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class SelectionIndexationFormHtmlProducer extends BdfServerHtmlProducer {

    private final Motcle motcle;
    private final String mode;
    private final PermissionSummary permissionSummary;

    public SelectionIndexationFormHtmlProducer(BdfParameters bdfParameters, Motcle motcle, String mode) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.motcle = motcle;
        this.mode = mode;
        addThemeCss("selectionindexation.css");
        addJsLib(MiscJsLibs.SELECTIONINDEXATION);
    }

    @Override
    public void printHtml() {
        SelectionIndexation selectionIndexation = SelectionIndexation.build(bdfServer, bdfUser, motcle, bdfUser.getSelectedFiches(), mode);
        start();
        this
                .__(SelectionIndexation.printCommandResult(this))
                .__(PageUnit.start("action-FicheIndexation", "_ title.edition.selectionindexation"));
        this
                .P()
                .__localize("_ label.global.motcle")
                .__colon()
                .__space()
                .__(printMotcleTitle())
                ._P();
        this
                .P()
                .__localize("_ label.global.thesaurus")
                .__colon()
                .__space()
                .__escape(FichothequeUtils.getTitle(motcle.getThesaurus(), workingLang))
                ._P();
        if (!mode.isEmpty()) {
            this
                    .P()
                    .__localize("_ label.global.mode")
                    .__colon()
                    .__space()
                    .__escape(mode)
                    ._P();
        }
        this
                .DIV(HA.id("selectionindexationToolbar"))
                ._DIV();
        this
                .FORM_post(HA.action(Domains.THESAURUS))
                .INPUT_hidden(ParameterMap.init()
                        .command(SelectionIndexationCommand.COMMANDNAME)
                        .page(ThesaurusDomain.SELECTIONINDEXATION_FORM_PAGE)
                        .subset(motcle.getThesaurus())
                        .subsetItem(motcle)
                        .param(InteractionConstants.MODE_PARAMNAME, mode))
                .__(selectionIndexation)
                .__(Button.COMMAND, Button.submit("_ submit.thesaurus.motcleselectionindexation"))
                ._FORM();
        this
                .__(PageUnit.END);
        end();
    }


    private boolean printMotcleTitle() {
        if (permissionSummary.isSubsetAdmin(motcle.getSubsetKey())) {
            this
                    .A(HA.href(BH.domain(Domains.THESAURUS).page(ThesaurusDomain.MOTCLE_CHANGEFORM_PAGE).subsetItem(motcle)))
                    .__(BdfHtmlUtils.printMotcleTitle(this, motcle, workingLang))
                    ._A();
        } else {
            this
                    .__(BdfHtmlUtils.printMotcleTitle(this, motcle, workingLang));
        }
        return true;
    }

}
