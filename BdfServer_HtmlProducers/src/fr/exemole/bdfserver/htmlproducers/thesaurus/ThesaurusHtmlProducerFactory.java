/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.commands.thesaurus.MotcleCreationCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusHtmlProducerFactory {

    private final static int THESAURUS_ADMIN = 1;
    private final static int MOTCLE_ADMIN = 3;

    private ThesaurusHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String page = parameters.getOutput();
        BdfServer bdfServer = parameters.getBdfServer();
        RequestMap requestMap = parameters.getRequestMap();
        switch (getPageType(page)) {
            case THESAURUS_ADMIN: {
                Thesaurus thesaurus = requestHandler.getThesaurus();
                parameters.checkSubsetAdmin(thesaurus);
                switch (page) {
                    case ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE: {
                        return new ThesaurusAdvancedCommandsHtmlProducer(parameters, thesaurus);
                    }
                    case ThesaurusDomain.THESAURUS_METADATAFORM_PAGE: {
                        return new ThesaurusMetadataFormHtmlProducer(parameters, thesaurus);
                    }
                    case ThesaurusDomain.MOTCLE_CREATIONFORM_PAGE: {
                        Motcle parentMotcle = (Motcle) requestHandler.getOptionnalSubsetItem(thesaurus, MotcleCreationCommand.PARENT_PARAMNAME);
                        return new MotcleCreationFormHtmlProducer(parameters, thesaurus, parentMotcle);
                    }
                    default:
                        return null;
                }
            }
            case MOTCLE_ADMIN: {
                Motcle motcle = requestHandler.getMotcle();
                parameters.checkSubsetAdmin(motcle.getThesaurus());
                switch (page) {
                    case ThesaurusDomain.MOTCLE_ADVANCEDCOMMANDS_PAGE: {
                        return new MotcleAdvancedCommandsHtmlProducer(parameters, motcle);
                    }
                    case ThesaurusDomain.MOTCLE_CHANGEFORM_PAGE: {
                        return new MotcleChangeFormHtmlProducer(parameters, motcle);
                    }
                    case ThesaurusDomain.MOTCLE_INDEXATIONFORM_PAGE: {
                        return new MotcleIndexationFormHtmlProducer(parameters, motcle);
                    }
                    case ThesaurusDomain.MOTCLE_PONDERATIONFORM_PAGE: {
                        String sortValue = requestHandler.getTrimedParameter(ThesaurusDomain.SORT_PARAMNAME, ThesaurusDomain.CORPUSASC_TRI_PARAMVALUE);
                        return new MotclePonderationFormHtmlProducer(parameters, motcle, sortValue);
                    }
                    case ThesaurusDomain.MOTCLE_SELECTIONINDEXATIONFORM_PAGE: {
                        String mode = requestHandler.getMode();
                        return new MotcleSelectionIndexationFormHtmlProducer(parameters, motcle, mode);
                    }
                    case ThesaurusDomain.SELECTIONINDEXATION_FORM_PAGE: {
                        String mode = requestHandler.getMode();
                        return new SelectionIndexationFormHtmlProducer(parameters, motcle, mode);
                    }
                    default:
                        return null;
                }
            }
            default: {
                switch (page) {
                    case ThesaurusDomain.THESAURUS_CREATIONFORM_PAGE: {
                        parameters.checkFichothequeAdmin();
                        return new ThesaurusCreationFormHtmlProducer(parameters);
                    }
                    case ThesaurusDomain.THESAURUS_PAGE: {
                        Thesaurus thesaurus = requestHandler.getThesaurus();
                        ThesaurusParameters thesaurusParameters = ThesaurusParameters.buildFromRequest(requestMap);
                        BdfUser bdfUser = parameters.getBdfUser();
                        if (thesaurusParameters == null) {
                            thesaurusParameters = ThesaurusParameters.buildFromThesaurus(bdfUser, thesaurus);
                        } else {
                            try (EditSession session = bdfServer.initEditSession(bdfUser, Domains.THESAURUS, page)) {
                                thesaurusParameters.save(session.getBdfServerEditor(), bdfUser, thesaurus);
                            }
                        }
                        return new ThesaurusHtmlProducer(parameters, thesaurus, thesaurusParameters);
                    }
                    case ThesaurusDomain.SELECTIONINDEXATION_CHOICE_PAGE: {
                        return new SelectionIndexationChoiceHtmlProducer(parameters);
                    }
                    default:
                        return null;
                }
            }
        }
    }

    private static int getPageType(String page) {
        switch (page) {
            case ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE:
            case ThesaurusDomain.THESAURUS_METADATAFORM_PAGE:
            case ThesaurusDomain.MOTCLE_CREATIONFORM_PAGE:
                return THESAURUS_ADMIN;
            case ThesaurusDomain.MOTCLE_ADVANCEDCOMMANDS_PAGE:
            case ThesaurusDomain.MOTCLE_CHANGEFORM_PAGE:
            case ThesaurusDomain.MOTCLE_INDEXATIONFORM_PAGE:
            case ThesaurusDomain.MOTCLE_PONDERATIONFORM_PAGE:
            case ThesaurusDomain.MOTCLE_SELECTIONINDEXATIONFORM_PAGE:
            case ThesaurusDomain.SELECTIONINDEXATION_FORM_PAGE:
                return MOTCLE_ADMIN;
            default:
                return 0;
        }
    }

}
