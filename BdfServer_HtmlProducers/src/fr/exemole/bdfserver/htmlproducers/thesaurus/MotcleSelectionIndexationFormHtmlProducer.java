/* BdfServer_HtmlProducers - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.commands.thesaurus.SelectionIndexationCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import fr.exemole.bdfserver.htmlproducers.CommandBoxUtils;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class MotcleSelectionIndexationFormHtmlProducer extends BdfServerHtmlProducer {

    private final Thesaurus thesaurus;
    private final Motcle motcle;
    private final String mode;
    private final boolean withFontAwesome;

    public MotcleSelectionIndexationFormHtmlProducer(BdfParameters bdfParameters, Motcle motcle, String mode) {
        super(bdfParameters);
        this.thesaurus = motcle.getThesaurus();
        this.motcle = motcle;
        this.mode = mode;
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(MiscJsLibs.SELECTIONINDEXATION);
        addThemeCss("thesaurus.css", "selectionindexation.css");
        this.withFontAwesome = checkFontAwesome();
    }

    @Override
    public void printHtml() {
        Lang thesaurusLang = BdfServerUtils.checkLangDisponibility(bdfServer, thesaurus, workingLang);
        SelectionIndexation selectionIndexation = SelectionIndexation.build(bdfServer, bdfUser, motcle, bdfUser.getSelectedFiches(), mode);
        start();
        ThesaurusHtmlUtils.printMotcleToolBar(this, bdfServer, bdfUser, motcle, ThesaurusDomain.MOTCLE_SELECTIONINDEXATIONFORM_PAGE, thesaurusLang, withFontAwesome);
        SelectionIndexation.printCommandResult(this);
        CommandBox commandBox = CommandBox.init()
                .action(Domains.THESAURUS)
                .family("THS")
                .veil(true)
                .page(ThesaurusDomain.MOTCLE_SELECTIONINDEXATIONFORM_PAGE)
                .name(SelectionIndexationCommand.COMMANDNAME)
                .lockey(SelectionIndexationCommand.COMMANDKEY)
                .__(CommandBoxUtils.insert(motcle))
                .hidden(InteractionConstants.MODE_PARAMNAME, mode)
                .submitLocKey("_ submit.thesaurus.motcleselectionindexation");
        this
                .__start(commandBox)
                .__if(!mode.isEmpty(), () -> {
                    this
                            .P()
                            .__localize("_ label.global.mode")
                            .__colon()
                            .__space()
                            .__escape(mode)
                            ._P();
                })
                .DIV(HA.id("selectionindexationToolbar"))
                ._DIV()
                .__(selectionIndexation)
                .__end(commandBox);
        end();
    }

}
