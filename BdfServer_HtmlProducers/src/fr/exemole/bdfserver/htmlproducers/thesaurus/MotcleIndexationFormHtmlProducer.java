/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class MotcleIndexationFormHtmlProducer extends BdfServerHtmlProducer {

    private final Thesaurus thesaurus;
    private final Motcle motcle;
    private final PermissionSummary permissionSummary;
    private final boolean withFontAwesome;


    public MotcleIndexationFormHtmlProducer(BdfParameters bdfParameters, Motcle motcle) {
        super(bdfParameters);
        this.thesaurus = motcle.getThesaurus();
        this.motcle = motcle;
        this.permissionSummary = bdfParameters.getPermissionSummary();
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("thesaurus.css");
        this.withFontAwesome = checkFontAwesome();
    }

    @Override
    public void printHtml() {
        Lang thesaurusLang = BdfServerUtils.checkLangDisponibility(bdfServer, thesaurus, workingLang);
        start();
        ThesaurusHtmlUtils.printMotcleToolBar(this, bdfServer, bdfUser, motcle, ThesaurusDomain.MOTCLE_INDEXATIONFORM_PAGE, thesaurusLang, withFontAwesome);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.THESAURUS)
                .family("THS")
                .veil(true)
                .page(ThesaurusDomain.MOTCLE_INDEXATIONFORM_PAGE);
        ThesaurusCommandBoxUtils.printMotcleIndexationBox(this, commandBox, motcle, workingLang, permissionSummary);
        end();
    }

}
