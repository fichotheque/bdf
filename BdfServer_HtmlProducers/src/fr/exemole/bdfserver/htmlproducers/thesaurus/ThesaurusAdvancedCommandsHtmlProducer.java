/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Thesaurus thesaurus;

    public ThesaurusAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Thesaurus thesaurus) {
        super(bdfParameters);
        this.thesaurus = thesaurus;
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("thesaurus.css");
    }

    @Override
    public void printHtml() {
        start();
        BdfHtmlUtils.printSubsetHeader(this, bdfParameters, thesaurus, ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE);
        ThesaurusHtmlUtils.printThesaurusToolbar(this, ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE, thesaurus);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.THESAURUS)
                .family("THS")
                .veil(true);
        if ((bdfParameters.isFichothequeAdmin()) && (thesaurus.isRemoveable())) {
            commandBox
                    .errorPage(ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE)
                    .page(InteractionConstants.MESSAGE_PAGE);
            ThesaurusCommandBoxUtils.printThesaurusRemoveBox(this, commandBox, thesaurus);
        }
        commandBox
                .errorPage(null)
                .page(ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE);
        ThesaurusCommandBoxUtils.printThesaurusAttributeChangeBox(this, commandBox, thesaurus);
        if (thesaurus.isIdalphaType()) {
            ThesaurusCommandBoxUtils.printIdalphaSortBox(this, commandBox, thesaurus, null);
        }
        ThesaurusCommandBoxUtils.printChildrenReorderBox(this, commandBox, thesaurus, null);
        if (bdfParameters.isFichothequeAdmin()) {
            commandBox
                    .page(ThesaurusDomain.THESAURUS_METADATAFORM_PAGE)
                    .errorPage(ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE);
            ThesaurusCommandBoxUtils.printThesaurusCloneBox(this, commandBox, thesaurus, workingLang);
        }
        ThesaurusCommandBoxUtils.printThesaurusCleaningBox(this, commandBox, thesaurus, workingLang);
        end();
    }

}
