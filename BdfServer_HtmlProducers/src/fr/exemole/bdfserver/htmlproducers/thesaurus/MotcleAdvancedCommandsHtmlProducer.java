/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.api.users.BdfUserConstants;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.subsettree.TreeFilterEngine;
import net.fichotheque.SubsetKey;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.CommandBox;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class MotcleAdvancedCommandsHtmlProducer extends BdfServerHtmlProducer {

    private final Thesaurus thesaurus;
    private final Motcle motcle;
    private final PermissionSummary permissionSummary;
    private final boolean withFontAwesome;

    public MotcleAdvancedCommandsHtmlProducer(BdfParameters bdfParameters, Motcle motcle) {
        super(bdfParameters);
        this.thesaurus = motcle.getThesaurus();
        this.motcle = motcle;
        this.permissionSummary = bdfParameters.getPermissionSummary();
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addJsLib(BdfJsLibs.APPELANT);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        addThemeCss("thesaurus.css");
        this.withFontAwesome = checkFontAwesome();
    }

    @Override
    public void printHtml() {
        Lang thesaurusLang = BdfServerUtils.checkLangDisponibility(bdfServer, thesaurus, workingLang);
        start();
        ThesaurusHtmlUtils.printMotcleToolBar(this, bdfServer, bdfUser, motcle, ThesaurusDomain.MOTCLE_ADVANCEDCOMMANDS_PAGE, thesaurusLang, withFontAwesome);
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.THESAURUS)
                .family("THS")
                .veil(true)
                .page(ThesaurusDomain.MOTCLE_ADVANCEDCOMMANDS_PAGE);
        if ((thesaurus.isIdalphaType()) && (!motcle.getChildList().isEmpty())) {
            ThesaurusCommandBoxUtils.printIdalphaSortBox(this, commandBox, thesaurus, motcle);
        }
        if (!motcle.getChildList().isEmpty()) {
            ThesaurusCommandBoxUtils.printChildrenReorderBox(this, commandBox, thesaurus, motcle);
        }
        ThesaurusCommandBoxUtils.printMotcleAttributeChangeBox(this, commandBox, motcle);
        commandBox
                .page(ThesaurusDomain.MOTCLE_CHANGEFORM_PAGE)
                .errorPage(ThesaurusDomain.MOTCLE_ADVANCEDCOMMANDS_PAGE);
        SubsetTree originalSubsetTree = bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_THESAURUS);
        SubsetTree adminSubsetTree = TreeFilterEngine.admin(permissionSummary, originalSubsetTree);
        ThesaurusCommandBoxUtils.printMotcleMoveBox(this, commandBox, motcle, bdfServer, workingLang, adminSubsetTree, (SubsetKey) bdfUser.getParameterValue(BdfUserConstants.THESAURUSMOVELAST_SUBSETKEY_OBJ));
        SubsetTree readSubsetTree = TreeFilterEngine.read(permissionSummary, originalSubsetTree);
        ThesaurusCommandBoxUtils.printMotcleMergeBox(this, commandBox, motcle, bdfServer, workingLang, readSubsetTree, (SubsetKey) bdfUser.getParameterValue(BdfUserConstants.THESAURUSMERGELAST_SUBSETKEY_OBJ));
        end();
    }

}
