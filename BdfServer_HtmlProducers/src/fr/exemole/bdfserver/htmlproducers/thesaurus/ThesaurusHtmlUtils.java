/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.api.menu.MenuGroup;
import fr.exemole.bdfserver.api.menu.MenuLink;
import fr.exemole.bdfserver.api.menu.MenuNode;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.SubsetIcon;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfHref;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import java.util.List;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.MotcleIcon;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusHtmlUtils {

    private ThesaurusHtmlUtils() {
    }

    public static String getThesaurusTypeMessageKey(short type) {
        switch (type) {
            case ThesaurusMetadata.BABELIEN_TYPE:
                return "_ label.thesaurus.type_babelien";
            case ThesaurusMetadata.IDALPHA_TYPE:
                return "_ label.thesaurus.type_idalpha";
            case ThesaurusMetadata.MULTI_TYPE:
                return "_ label.thesaurus.type_multi";
            default:
                throw new IllegalArgumentException("wrong type");
        }
    }

    public static boolean printThesaurusToolbar(HtmlPrinter hp, String pageActu, Thesaurus thesaurus) {
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-Metadata", thesaurus, ThesaurusDomain.THESAURUS_METADATAFORM_PAGE, "_ link.thesaurus.thesaurusmetadataform", pageActu))
                .__(link(button, "action-Advanced", thesaurus, ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE, "_ link.thesaurus.thesaurusadvancedcommands", pageActu))
                .__(refresh(button, thesaurus, -1))
                ._DIV()
                ._NAV();
        return true;
    }

    public static boolean printMotcleToolBar(HtmlPrinter hp, BdfServer bdfServer, BdfUser bdfUser, Motcle motcle, String currentPage, Lang thesaurusLang, boolean withFontAwesome) {
        MotcleIcon motcleIcon = motcle.getMotcleIcon();
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        int anchorId = -1;
        Motcle previous = ThesaurusUtils.getPrevious(motcle);
        if (previous != null) {
            anchorId = previous.getId();
        }
        hp
                .HEADER("subset-ItemHeader")
                .H1();
        if (motcleIcon != null) {
            hp
                    .SPAN()
                    .__(BdfHtmlUtils.printMotcleIcon(hp, motcleIcon, withFontAwesome))
                    .__space()
                    .__(BdfHtmlUtils.printMotcleTitle(hp, motcle, thesaurusLang))
                    ._SPAN();
        } else {
            hp
                    .__(SubsetIcon.THESAURUS)
                    .SPAN()
                    .__(BdfHtmlUtils.printMotcleTitle(hp, motcle, thesaurusLang))
                    ._SPAN();
        }
        hp
                ._H1()
                ._HEADER();
        hp
                .NAV("subset-Toolbar")
                .DIV("subset-Buttons")
                .SPAN("subset-SmallLabel")
                .__localize("_ title.global.commands")
                .__colon()
                ._SPAN()
                .__(link(button, "action-MotcleEdit", motcle, ThesaurusDomain.MOTCLE_CHANGEFORM_PAGE, "_ link.thesaurus.motclechangeform", currentPage))
                .__(link(button, "action-Advanced", motcle, ThesaurusDomain.MOTCLE_ADVANCEDCOMMANDS_PAGE, "_ link.thesaurus.motcleadvancedcommands", currentPage))
                .__(link(button, "action-MotcleIndexation", motcle, ThesaurusDomain.MOTCLE_INDEXATIONFORM_PAGE, "_ link.thesaurus.motcleindexationform", currentPage))
                .__(link(button, "action-MotclePonderation", motcle, ThesaurusDomain.MOTCLE_PONDERATIONFORM_PAGE, "_ link.thesaurus.motcleponderationform", currentPage))
                .__(link(button, "action-MotcleSelectionIndexation", motcle, ThesaurusDomain.MOTCLE_SELECTIONINDEXATIONFORM_PAGE, "_ link.thesaurus.selectionindexationform", currentPage));
        List<MenuGroup> menuGroupList = BdfServerUtils.getExtensionMenuGroupList(bdfServer, bdfUser, motcle);
        for (MenuGroup menuGroup : menuGroupList) {
            for (MenuNode menuNode : menuGroup.getMenuNodeList()) {
                if (menuNode instanceof MenuLink) {
                    hp
                            .__(menuLink(button, (MenuLink) menuNode, currentPage));
                }
            }
        }
        hp
                .__(refresh(button, motcle.getThesaurus(), anchorId))
                ._DIV()
                ._NAV();
        return true;
    }

    private static Button link(Button button, String action, Object object, String page, String titleLocKey, String currentPage) {
        if (currentPage.equals(page)) {
            return button.current(true).href(null).action(action).tooltip(null);
        } else {
            BdfHref href = BH.domain(Domains.THESAURUS).page(page);
            if (object instanceof Thesaurus) {
                href.subset((Thesaurus) object);
            } else if (object instanceof Motcle) {
                href.subsetItem((Motcle) object);
            }
            return button.current(false).href(href).action(action).tooltipMessage(titleLocKey);
        }
    }

    private static Button refresh(Button button, Thesaurus thesaurus, int motcleId) {
        BdfHref href = BH.domain(Domains.THESAURUS).page(ThesaurusDomain.THESAURUS_PAGE).subset(thesaurus);
        String hrefString;
        if (motcleId > 0) {
            hrefString = href.refresh().toString() + "#motcle_" + motcleId;
        } else {
            hrefString = href.toString();
        }
        return button.current(false)
                .href(hrefString)
                .action("action-Refresh")
                .tooltipMessage("_ link.global.reload")
                .target(BdfHtmlConstants.LIST_FRAME);
    }

    private static Button menuLink(Button button, MenuLink menuLink, String currentPage) {
        String pageName = menuLink.getPageName();
        String action = "action-MotcleEdit";
        if (pageName.equals(currentPage)) {
            return button.current(true).href(null).action(action).tooltip(null);
        } else {
            Message titleMessage = menuLink.getTitleMessage();
            if (titleMessage != null) {
                button.tooltipMessage(titleMessage);
            } else {
                button.tooltip(menuLink.getTitle());
            }
            return button.current(false).href(menuLink.getUrl()).action(action);
        }
    }

}
