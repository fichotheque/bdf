/* BdfServer_HtmlProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.api.roles.SatelliteOpportunities;
import fr.exemole.bdfserver.commands.thesaurus.MotcleCreationCommand;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.html.BdfHtmlUtils;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.SatelliteTree;
import fr.exemole.bdfserver.html.consumers.SelectOption;
import fr.exemole.bdfserver.html.consumers.Tree;
import fr.exemole.bdfserver.html.consumers.attributes.Appelant;
import fr.exemole.bdfserver.html.consumers.attributes.Deploy;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.htmlproducers.main.MainHtmlUtils;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.fichotheque.tools.eligibility.LabelPredicate;
import net.fichotheque.tools.eligibility.MultiPredicateBuilder;
import net.fichotheque.tools.eligibility.SatellitePredicate;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.HtmlWrapper;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.request.RequestConstants;
import net.mapeadores.util.text.Idalpha;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusHtmlProducer extends BdfServerHtmlProducer {

    private final static HtmlWrapper CHILD_TREE = Tree.tree("thesaurus-ChildTree");
    private final static HtmlWrapper CHILD_LEAF = Tree.leaf("thesaurus-ChildLeaf");
    private final static Button GO_BUTTON = Button.submit().action("action-Go").tooltipMessage("_ link.global.go");
    private final static Button FILTER_BUTTON = Button.submit("_ submit.global.apply");
    private final static SelectOption EMPTY_OPTION = SelectOption.init("").text("---");
    private final static SelectOption[] LABELFILTER_OPTIONS = {
        EMPTY_OPTION,
        SelectOption.init(ThesaurusDomain.WITH_FILTERVALUE).textL10nObject("_ label.thesaurus.labelfilter_with"),
        SelectOption.init(ThesaurusDomain.WITHOUT_FILTERVALUE).textL10nObject("_ label.thesaurus.labelfilter_without")
    };
    private final static SelectOption[] SATELLITEFILTER_OPTIONS = {
        EMPTY_OPTION,
        SelectOption.init(ThesaurusDomain.WITH_FILTERVALUE).textL10nObject("_ label.thesaurus.satellitefilter_with"),
        SelectOption.init(ThesaurusDomain.WITHOUT_FILTERVALUE).textL10nObject("_ label.thesaurus.satellitefilter_without")
    };
    private final PermissionSummary permissionSummary;
    private final Thesaurus thesaurus;
    private final ThesaurusParameters thesaurusParameters;
    private final Lang thesaurusLang;
    private final Predicate<SubsetItem> motclePredicate;
    private final boolean isSubsetAdmin;
    private final MessageFormat idFormat;
    private final MotcleBlock motcleBlock = new MotcleBlock();
    private final Button linkButton = Button.link();
    private final Consumer<HtmlPrinter> listTitle = new ListTitle();
    private final Corpus[] satelliteCorpusArray;
    private final Corpus[] filterSatelliteCorpusArray;
    private final SatelliteTree satelliteTree;
    private final boolean withFontAwesome;

    public ThesaurusHtmlProducer(BdfParameters bdfParameters, Thesaurus thesaurus, ThesaurusParameters thesaurusParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.DEPLOY);
        addJsLib(BdfJsLibs.SUBSETCHANGE);
        addJsLib(BdfJsLibs.APPELANT);
        addThemeCss("thesaurus.css");
        setBodyCssClass("global-body-ListFrame");
        this.permissionSummary = bdfParameters.getPermissionSummary();
        this.thesaurus = thesaurus;
        this.thesaurusParameters = thesaurusParameters;
        this.satelliteCorpusArray = initSatelliteArray();
        this.thesaurusLang = initThesaurusLang(thesaurusParameters);
        this.filterSatelliteCorpusArray = initFilterSatelliteArray(thesaurusParameters);
        this.motclePredicate = initMotclePredicate(thesaurusParameters);
        this.isSubsetAdmin = permissionSummary.isSubsetAdmin(thesaurus.getSubsetKey());
        setMainStorageKey(Domains.THESAURUS, ThesaurusDomain.THESAURUS_PAGE, thesaurus.getSubsetName());
        this.idFormat = new MessageFormat(getLocalization("_ info.main.motclenumber"), getFormatLocale());
        if (satelliteCorpusArray.length > 0) {
            this.satelliteTree = new SatelliteTree(workingLang, formatLocale);
        } else {
            this.satelliteTree = null;
        }
        ThesaurusMetadata thesaurusMetadata = thesaurus.getThesaurusMetadata();
        motcleBlock
                .longStyle(thesaurusMetadata.isLongIdalphaStyle())
                .bracketsStyle(thesaurusMetadata.isBracketsIdalphaStyle());
        this.withFontAwesome = checkFontAwesome();
    }

    @Override
    public void printHtml() {
        start();
        this
                .__(BdfHtmlUtils.startSubsetUnit(this, bdfParameters, thesaurus, ThesaurusDomain.THESAURUS_PAGE))
                .__(printCommands())
                .__(printMisc())
                .__(PageUnit.END)
                .__(printMotcleList());
        end();
    }

    private Lang initThesaurusLang(ThesaurusParameters thesaurusParameters) {
        Lang labelLang = thesaurusParameters.getLabelLang();
        if (labelLang != null) {
            return labelLang;
        }
        return BdfServerUtils.checkLangDisponibility(bdfServer, thesaurus, workingLang);
    }

    private Corpus[] initSatelliteArray() {
        List<Corpus> satelliteCorpusList = thesaurus.getSatelliteCorpusList();
        List<Corpus> result = new ArrayList<Corpus>();
        for (Corpus corpus : satelliteCorpusList) {
            if (permissionSummary.hasAccess(corpus)) {
                result.add(corpus);
            }
        }
        return result.toArray(new Corpus[result.size()]);
    }

    private Corpus[] initFilterSatelliteArray(ThesaurusParameters thesaurusParameters) {
        if (satelliteCorpusArray.length == 0) {
            return null;
        }
        SubsetKey satelliteCorpusKey = thesaurusParameters.getSatelliteCorpusKey();
        if (satelliteCorpusKey == null) {
            return null;
        }
        for (Corpus corpus : satelliteCorpusArray) {
            if (corpus.getSubsetKey().equals(satelliteCorpusKey)) {
                Corpus[] array = new Corpus[1];
                array[0] = corpus;
                return array;
            }
        }
        return null;
    }

    private Predicate<SubsetItem> initMotclePredicate(ThesaurusParameters thesaurusParameters) {
        MultiPredicateBuilder builder = new MultiPredicateBuilder();
        String labelFilter = thesaurusParameters.getLabelFilter();
        if (labelFilter != null) {
            switch (labelFilter) {
                case ThesaurusDomain.WITH_FILTERVALUE:
                    builder.add(new LabelPredicate(thesaurusLang, true));
                    break;
                case ThesaurusDomain.WITHOUT_FILTERVALUE:
                    builder.add(new LabelPredicate(thesaurusLang, false));
                    break;
            }
        }
        String satelliteFilter = thesaurusParameters.getSatelliteFilter();
        if ((satelliteFilter != null) && (satelliteCorpusArray.length > 0)) {
            Corpus[] array;
            if (filterSatelliteCorpusArray != null) {
                array = filterSatelliteCorpusArray;
            } else {
                array = satelliteCorpusArray;
            }
            switch (satelliteFilter) {
                case ThesaurusDomain.WITH_FILTERVALUE:
                    builder.add(new SatellitePredicate(thesaurus, array, true));
                    break;
                case ThesaurusDomain.WITHOUT_FILTERVALUE:
                    builder.add(new SatellitePredicate(thesaurus, array, false));
                    break;
            }
        }
        if (builder.isEmpty()) {
            return null;
        } else {
            return builder.toPredicate();
        }
    }


    private boolean printCommands() {
        if (!isSubsetAdmin) {
            return false;
        }
        this
                .DETAILS(HA.id("details_commands").open(true).classes("tools-Details").populate(Deploy.DETAILS))
                .SUMMARY()
                .__localize("_ title.global.commands")
                ._SUMMARY()
                .DIV("tools-List")
                .__(link("action-New", ThesaurusDomain.MOTCLE_CREATIONFORM_PAGE, "_ link.thesaurus.motclecreationform"))
                .__(link("action-Metadata", ThesaurusDomain.THESAURUS_METADATAFORM_PAGE, "_ link.thesaurus.thesaurusmetadataform"))
                .__(link("action-Advanced", ThesaurusDomain.THESAURUS_ADVANCEDCOMMANDS_PAGE, "_ link.thesaurus.thesaurusadvancedcommands"))
                .__(link("action-Refresh", ThesaurusDomain.THESAURUS_PAGE, "_ link.global.reload"))
                ._DIV()
                ._DETAILS();
        return true;
    }

    private boolean printMisc() {
        if (isSubsetAdmin) {
            this
                    .DETAILS(HA.id("details_misc").classes("tools-Details").populate(Deploy.DETAILS))
                    .SUMMARY()
                    .__localize("_ title.thesaurus.misc")
                    ._SUMMARY()
                    .DIV("tools-List")
                    .__(MainHtmlUtils.printTableDisplayDetails(this, bdfServer, bdfUser, thesaurus, false, HA.id("details_tabledisplay").classes("tools-Details").populate(Deploy.DETAILS)))
                    .__(printStatsLinks())
                    .__(printGoForm())
                    ._DIV()
                    ._DETAILS();
        } else {
            this
                    .__(MainHtmlUtils.printTableDisplayDetails(this, bdfServer, bdfUser, thesaurus, false, HA.id("details_tabledisplay").classes("tools-Details").populate(Deploy.DETAILS)))
                    .__(printStatsLinks());
        }
        return true;
    }

    private Button link(String action, String pageName, String messageKey) {
        String href = BH.domain(Domains.THESAURUS).subset(thesaurus).page(pageName).toString();
        String target;
        if (pageName.equals(ThesaurusDomain.THESAURUS_PAGE)) {
            target = null;
        } else {
            target = BdfHtmlConstants.EDITION_FRAME;
        }
        return linkButton.href(href).action(action).textL10nObject(messageKey).target(target);
    }

    private boolean printStatsLinks() {
        String thesaurusName = thesaurus.getSubsetName();
        this
                .DETAILS(HA.id("details_stats").classes("tools-Details").populate(Deploy.DETAILS))
                .SUMMARY()
                .__localize("_ link.global.stats")
                ._SUMMARY()
                .__(MainHtmlUtils.DISPLAYTABLE_TREE, () -> {
                    this
                            .__(printStatLeaf(thesaurusName, "stats", "_ link.motcles.stats"))
                            .__(printStatLeaf(thesaurusName, "statssel", "_ link.motcles.statssel"))
                            .__(printStatLeaf(thesaurusName, "inverse", "_ link.motcles.inverse"));
                })
                ._DETAILS();
        return true;
    }

    private boolean printStatLeaf(String thesaurusName, String option, String messageKey) {
        String href = "motcles/" + thesaurusName + "-" + option;
        Button button = Button.link().style(Button.TRANSPARENT_STYLE);
        this
                .__(MainHtmlUtils.DISPLAYTABLE_LEAF, () -> {
                    this
                            .SPAN("displaytable-Title")
                            .__localize(messageKey)
                            .__colon()
                            ._SPAN()
                            .__(button.href(href + ".html").action("action-TableDisplay").tooltipMessage("_ link.global.version", "HTML").target(BdfHtmlConstants.EDITION_FRAME))
                            .__(button.href(href + ".ods").action("action-Ods").tooltipMessage("_ link.global.version", "ODS").target(null));
                });
        return true;
    }

    private boolean printGoForm() {
        if (!isSubsetAdmin) {
            return false;
        }
        this
                .DETAILS(HA.id("details_go").classes("tools-Details").populate(Deploy.DETAILS))
                .SUMMARY()
                .__localize("_ label.menu.goto")
                ._SUMMARY()
                .FORM_get(Domains.THESAURUS, BdfHtmlConstants.EDITION_FRAME)
                .DIV("thesaurus-GoBlock")
                .INPUT_hidden(RequestConstants.PAGE_PARAMETER, ThesaurusDomain.MOTCLE_CHANGEFORM_PAGE)
                .INPUT_hidden(SubsetKey.CATEGORY_THESAURUS_STRING, thesaurus.getSubsetName())
                .INPUT_text(name(InteractionConstants.IDMTCL_PARAMNAME)
                        .size("8")
                        .populate(Appelant.motcle().wanted_code_id().limit(1).subsets(thesaurus.getSubsetKey())))
                .__(GO_BUTTON)
                ._DIV()
                ._FORM()
                ._DETAILS();

        return true;
    }

    private boolean printFilterDetails() {
        if (!withFilter(thesaurus)) {
            return false;
        }
        this
                .DETAILS("tools-Details thesaurus-FilterDetails")
                .SUMMARY()
                .__localize("_ label.thesarus.filtersandoptions")
                ._SUMMARY()
                .DIV("global-DetailPanel")
                .FORM_get(Domains.THESAURUS)
                .INPUT_hidden(RequestConstants.PAGE_PARAMETER, ThesaurusDomain.THESAURUS_PAGE)
                .INPUT_hidden(SubsetKey.CATEGORY_THESAURUS_STRING, thesaurus.getSubsetName())
                .__(Grid.START)
                .__(printLangSelect())
                .__(printLabelFilterSelect())
                .__(printSatelliteCorpus())
                .__(printSatelliteFilterSelect())
                .__(Grid.END)
                .__(FILTER_BUTTON)
                ._FORM()
                ._DIV()
                ._DETAILS();
        return true;
    }

    private boolean printLabelFilterSelect() {
        String currentFilter = thesaurusParameters.getLabelFilter();
        this
                .__(Grid.selectRow("_ label.thesaurus.labelfilter", name(ThesaurusDomain.LABELFILTER_PARAMNAME), () -> {
                    for (SelectOption option : LABELFILTER_OPTIONS) {
                        this
                                .__(option, currentFilter);
                    }
                }));
        return true;
    }

    private boolean printSatelliteFilterSelect() {
        if (satelliteCorpusArray.length == 0) {
            return false;
        }
        String currentFilter = thesaurusParameters.getSatelliteFilter();
        this
                .__(Grid.selectRow("_ label.thesaurus.satellitefilter", name(ThesaurusDomain.SATELLITEFILTER_PARAMNAME), () -> {
                    for (SelectOption option : SATELLITEFILTER_OPTIONS) {
                        this
                                .__(option, currentFilter);
                    }
                }));
        return true;
    }

    private boolean printLangSelect() {
        Langs langs = bdfServer.getThesaurusLangChecker().getAuthorizedLangs(thesaurus);
        if (langs.size() < 2) {
            return false;
        }
        String selected;
        Lang labelLang = thesaurusParameters.getLabelLang();
        if (labelLang != null) {
            selected = labelLang.toString();
        } else {
            selected = "";
        }
        this
                .__(Grid.selectRow("_ label.thesaurus.labellang", name(ThesaurusDomain.LABELLANG_PARAMNAME), () -> {
                    this
                            .__(EMPTY_OPTION, selected.isEmpty());
                    for (Lang lang : langs) {
                        String langString = lang.toString();
                        this
                                .OPTION(langString, (langString.equals(selected)))
                                .__escape(langString)
                                .__dash()
                                .__localize(langString)
                                ._OPTION();
                    }
                }));
        return true;
    }

    private boolean printSatelliteCorpus() {
        if (satelliteCorpusArray.length == 0) {
            return false;
        }
        SubsetKey satelliteCorpusKey = thesaurusParameters.getSatelliteCorpusKey();
        this
                .__(Grid.selectRow("_ link.global.satellites", name(ThesaurusDomain.SATELLITECORPUS_PARAMNAME), () -> {
                    this
                            .__(EMPTY_OPTION, (satelliteCorpusKey == null));
                    for (Corpus corpus : satelliteCorpusArray) {
                        SubsetKey currentCorpusKey = corpus.getSubsetKey();
                        boolean selected = false;
                        if ((satelliteCorpusKey != null) && (currentCorpusKey.equals(satelliteCorpusKey))) {
                            selected = true;
                        }
                        this
                                .OPTION(currentCorpusKey.getSubsetName(), selected)
                                .__escape(FichothequeUtils.getTitle(fichotheque, currentCorpusKey, workingLang, permissionSummary.isSubsetAdmin(currentCorpusKey)))
                                ._OPTION();
                    }
                }));
        return true;
    }

    private boolean printMotcleList() {
        List<Motcle> firstLevel = thesaurus.getFirstLevelList();
        if (firstLevel.isEmpty()) {
            return false;
        }
        int eligibleCount = -1;
        if (motclePredicate != null) {
            eligibleCount = 0;
            for (Motcle motcle : thesaurus.getMotcleList()) {
                if (motclePredicate.test(motcle)) {
                    eligibleCount++;
                }
            }
        }
        if ((eligibleCount == -1) && (firstLevel.size() != thesaurus.size())) {
            printTree(firstLevel);
        } else {
            printList(firstLevel, eligibleCount);
        }
        return true;
    }

    private boolean printTree(List<Motcle> firstLevel) {
        motcleBlock.treeMode(true);
        this
                .__(PageUnit.start(listTitle).sectionCss("unit-Unit thesaurus-MotcleListUnit"))
                .__(printFilterDetails())
                .UL("subsetitem-List");
        for (Motcle motcle : firstLevel) {
            this
                    .LI()
                    .__(motcleBlock.current(motcle))
                    ._LI();
        }
        this
                ._UL()
                .__(PageUnit.END);
        return true;
    }

    private boolean printList(List<Motcle> motcleList, int eligibleCount) {
        this
                .__(PageUnit.start(listTitle).sectionCss("unit-Unit thesaurus-MotcleListUnit"))
                .__(printFilterDetails());
        if (eligibleCount != -1) {
            this
                    .P("thesaurus-FilterResult")
                    .__localize("_ label.thesaurus.filterresult")
                    .__colon()
                    .__escape(bdfUser.format(eligibleCount))
                    .__escape(" / ")
                    .__escape(bdfUser.format(thesaurus.size()))
                    ._P();
        }
        if (eligibleCount != 0) {
            this
                    .UL("subsetitem-List");
            for (Motcle motcle : motcleList) {
                this
                        .__(printListMotcle(motcle));
            }
            this
                    ._UL();
        }
        this
                .__(PageUnit.END);
        return true;
    }


    private boolean printListMotcle(Motcle motcle) {
        boolean include = true;
        if (motclePredicate != null) {
            include = motclePredicate.test(motcle);
        }
        if (include) {
            this
                    .LI()
                    .__(motcleBlock.current(motcle))
                    ._LI();
        }
        List<Motcle> childList = motcle.getChildList();
        if (!childList.isEmpty()) {
            for (Motcle child : childList) {
                printListMotcle(child);
            }
        }
        return true;
    }

    private boolean printChildList(List<Motcle> childList) {
        if (childList.isEmpty()) {
            return false;
        }
        this
                .__(CHILD_TREE, () -> {
                    for (Motcle child : childList) {
                        this
                                .__(CHILD_LEAF, motcleBlock.current(child));
                    }
                });
        return true;
    }


    private class MotcleBlock implements Consumer<HtmlPrinter> {

        private final HtmlAttributes editButton = HA.href("").target(BdfHtmlConstants.EDITION_FRAME).classes("button-Circle action-MotcleEdit");
        private final Button motcleLinkButton = Button.link().target(BdfHtmlConstants.EDITION_FRAME);
        private Motcle motcle;
        private boolean treeMode = false;
        private boolean longStyle = false;
        private boolean bracketsStyle = false;

        private MotcleBlock() {

        }

        private MotcleBlock current(Motcle motcle) {
            this.motcle = motcle;
            return this;
        }

        private MotcleBlock treeMode(boolean treeMode) {
            this.treeMode = treeMode;
            return this;
        }

        private MotcleBlock longStyle(boolean longStyle) {
            this.longStyle = longStyle;
            return this;
        }

        private MotcleBlock bracketsStyle(boolean bracketsStyle) {
            this.bracketsStyle = bracketsStyle;
            return this;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            Object[] objs = new Object[1];
            objs[0] = motcle.getId();
            hp
                    .DIV(HA.id("motcle_" + motcle.getId()).classes("subsetitem-Title"))
                    .P()
                    .__(BdfHtmlUtils.printMotcleIcon(hp, motcle, withFontAwesome))
                    .__(printMotcleLabel(hp, thesaurusLang))
                    ._P()
                    .__(printMotcleEdit(hp, isSubsetAdmin))
                    ._DIV()
                    .DIV("subsetitem-Infos")
                    .__escape(idFormat.format(objs))
                    ._DIV()
                    .__(printCommandDetails(hp, isSubsetAdmin))
                    .__(printSatelliteDetails(hp));
            if (treeMode) {
                printChildList(motcle.getChildList());
            }
        }

        private boolean printSatelliteDetails(HtmlPrinter hp) {
            if (satelliteCorpusArray.length == 0) {
                return false;
            }
            boolean open = false;
            Corpus[] array;
            if (filterSatelliteCorpusArray != null) {
                array = filterSatelliteCorpusArray;
                open = true;
            } else {
                array = satelliteCorpusArray;
            }
            SatelliteOpportunities satelliteOpportunities = BdfServerUtils.getSatelliteOpportunities(motcle, permissionSummary, array);
            List<SatelliteOpportunities.Entry> entryList = satelliteOpportunities.getEntryList();
            if (entryList.isEmpty()) {
                return false;
            }
            hp
                    .DETAILS(HA.open(open).classes("tools-Details"))
                    .SUMMARY()
                    .__localize("_ link.global.satellites")
                    ._SUMMARY()
                    .__(satelliteTree.current(motcle, satelliteOpportunities))
                    ._DETAILS();
            return true;
        }

        private boolean printCommandDetails(HtmlPrinter hp, boolean editable) {
            if (!editable) {
                return false;
            }
            hp
                    .DETAILS("tools-Details")
                    .SUMMARY()
                    .__localize("_ title.global.commands")
                    ._SUMMARY()
                    .DIV("tools-List")
                    .__(childLink())
                    .__(motcleLink("action-Advanced", ThesaurusDomain.MOTCLE_ADVANCEDCOMMANDS_PAGE, "_ link.thesaurus.motcleadvancedcommands"))
                    .__(motcleLink("action-MotcleIndexation", ThesaurusDomain.MOTCLE_INDEXATIONFORM_PAGE, "_ link.thesaurus.motcleindexationform"))
                    .__(motcleLink("action-MotclePonderation", ThesaurusDomain.MOTCLE_PONDERATIONFORM_PAGE, "_ link.thesaurus.motcleponderationform"))
                    .__(motcleLink("action-MotcleSelectionIndexation", ThesaurusDomain.MOTCLE_SELECTIONINDEXATIONFORM_PAGE, "_ link.thesaurus.selectionindexationform"))
                    ._DIV()
                    ._DETAILS();
            return true;
        }

        private Button childLink() {
            if (motcle.getStatus().equals(FichothequeConstants.OBSOLETE_STATUS)) {
                return null;
            }
            String childHref = BH.domain(Domains.THESAURUS).page(ThesaurusDomain.MOTCLE_CREATIONFORM_PAGE).subset(thesaurus).param(MotcleCreationCommand.PARENT_PARAMNAME, String.valueOf(motcle.getId())).toString();
            return motcleLinkButton.href(childHref).action("action-New").textL10nObject("_ link.thesaurus.motclecreationform_child");
        }

        private Button motcleLink(String action, String page, String titleLocKey) {
            if ((!action.equals("action-Advanced")) && (!motcle.getStatus().equals(FichothequeConstants.ACTIVE_STATUS))) {
                return null;
            }
            String href = BH.domain(Domains.THESAURUS).page(page).subsetItem(motcle).toString();
            return motcleLinkButton.href(href).action(action).textL10nObject(titleLocKey);
        }

        private boolean printMotcleLabel(HtmlPrinter hp, Lang lang) {
            String idalpha = motcle.getIdalpha();
            String labelString;
            if (motcle.isBabelienType()) {
                labelString = motcle.getBabelienLabel().getLabelString();
            } else {
                Label label = motcle.getLabels().getLangPartCheckedLabel(lang);
                if (label != null) {
                    labelString = label.getLabelString();
                } else {
                    labelString = "?";
                }
            }
            String statusClass = getStatusClass();
            if (!statusClass.isEmpty()) {
                hp
                        .SPAN(statusClass);
            }
            if (idalpha != null) {
                boolean significant = Idalpha.isSignificant(idalpha);
                if (longStyle) {
                    if (!significant) {
                        hp
                                .SPAN("thesaurus-LongIdalpha")
                                .SMALL()
                                .__escape("(")
                                .__escape(idalpha)
                                .__escape(")")
                                ._SMALL()
                                ._SPAN()
                                .__space();
                    } else if (bracketsStyle) {
                        hp
                                .SPAN("thesaurus-LongIdalpha")
                                .__escape("[")
                                .__escape(idalpha)
                                .__escape("]")
                                ._SPAN()
                                .__space();
                    } else {
                        hp
                                .SPAN("thesaurus-LongIdalpha")
                                .__escape(idalpha)
                                ._SPAN()
                                .__space();
                    }
                    hp
                            .SPAN("thesaurus-LongLabel")
                            .__escape(labelString)
                            ._SPAN();
                } else {
                    if (!significant) {
                        hp
                                .SMALL()
                                .__escape("(")
                                .__escape(idalpha)
                                .__escape(")")
                                ._SMALL()
                                .__space();
                    } else if (bracketsStyle) {
                        hp
                                .__escape("[")
                                .__escape(idalpha)
                                .__escape("]")
                                .__space();
                    } else {
                        hp
                                .__escape(idalpha)
                                .__dash();
                    }
                    hp
                            .__escape(labelString);
                }
            } else {
                hp
                        .__escape(labelString);
            }
            if (!statusClass.isEmpty()) {
                hp
                        ._SPAN();
            }
            return true;
        }

        private boolean printMotcleEdit(HtmlPrinter hp, boolean editable) {
            if (!editable) {
                return false;
            }
            hp
                    .A(editButton.href(BH.domain(Domains.THESAURUS).page(ThesaurusDomain.MOTCLE_CHANGEFORM_PAGE).subsetItem(motcle)))
                    .__(Button.ICON)
                    ._A();
            return true;
        }

        private String getStatusClass() {
            switch (motcle.getStatus()) {
                case FichothequeConstants.GROUP_STATUS:
                    return "thesaurus-Group";
                case FichothequeConstants.OBSOLETE_STATUS:
                    return "thesaurus-Obsolete";
                default:
                    return "";
            }
        }

    }


    private class ListTitle implements Consumer<HtmlPrinter> {

        private ListTitle() {

        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .SPAN("thesaurus-MotcleListTitle")
                    .__localize("_ title.thesaurus.motcles")
                    .__space()
                    .__(BdfHtmlUtils.printItemCount(hp, bdfUser, thesaurus.size()))
                    ._SPAN();
            if (isSubsetAdmin) {
                hp
                        .__(Button.link().style(Button.TRANSPARENT_STYLE)
                                .href(BH.domain(Domains.THESAURUS).subset(thesaurus).page(ThesaurusDomain.MOTCLE_CREATIONFORM_PAGE))
                                .action("action-New")
                                .tooltipMessage("_ link.thesaurus.motclecreationform")
                                .target(BdfHtmlConstants.EDITION_FRAME));
            }
        }

    }

    private static boolean withFilter(Thesaurus thesaurus) {
        if (!thesaurus.isBabelienType()) {
            return true;
        }
        return false;
    }


}
