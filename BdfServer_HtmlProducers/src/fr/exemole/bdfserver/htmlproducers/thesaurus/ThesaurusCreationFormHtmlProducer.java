/* BdfServer_HtmlProducers - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusCreationFormHtmlProducer extends BdfServerHtmlProducer {

    public ThesaurusCreationFormHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.COMMANDTEST);
        addJsLib(BdfJsLibs.COMMANDVEIL);
        addThemeCss("thesaurus.css");
    }

    @Override
    public void printHtml() {
        start();
        printCommandMessageUnit();
        CommandBox commandBox = CommandBox.init()
                .action(Domains.THESAURUS)
                .family("THS")
                .veil(true)
                .page(ThesaurusDomain.THESAURUS_METADATAFORM_PAGE)
                .errorPage(ThesaurusDomain.THESAURUS_CREATIONFORM_PAGE);
        ThesaurusCommandBoxUtils.printThesaurusCreationBox(this, commandBox, bdfServer);
        end();
    }

}
