/* BdfServer_HtmlProducers - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.thesaurus;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.commands.thesaurus.MotcleTestCommand;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.ParameterMap;
import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.SubsetTreeOptions;
import fr.exemole.bdfserver.html.consumers.attributes.Appelant;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.tools.subsettree.TreeFilterEngine;
import java.util.Map;
import net.fichotheque.SubsetKey;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;


/**
 *
 * @author Vincent Calame
 */
public class SelectionIndexationChoiceHtmlProducer extends BdfServerHtmlProducer {

    private final PermissionSummary permissionSummary;

    public SelectionIndexationChoiceHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        this.permissionSummary = bdfParameters.getPermissionSummary();
        addJsLib(BdfJsLibs.APPELANT);
        addJsLib(BdfJsLibs.COMMANDTEST);
    }

    @Override
    public void printHtml() {
        Map<String, String> storeMap = getStoredValues("command_motcletest");
        HtmlAttributes inputAttributes = name(InteractionConstants.IDMTCL_PARAMNAME)
                .value(storeMap)
                .size("15")
                .populate(Appelant.motcle().wanted_code_id().limit(1).subsets(fichotheque.getThesaurusList().get(0).getSubsetKey()));
        start();
        printCommandMessageUnit();
        this
                .__(PageUnit.start("action-FicheIndexation", "_ title.edition.selectionindexation_choice"))
                .FORM_get(HA.action(Domains.THESAURUS).attr("data-formstorage-key", "selectionIndexationChoice"))
                .INPUT_hidden(ParameterMap.init()
                        .command(MotcleTestCommand.COMMANDNAME)
                        .page(ThesaurusDomain.SELECTIONINDEXATION_FORM_PAGE)
                        .errorPage(ThesaurusDomain.SELECTIONINDEXATION_CHOICE_PAGE))
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.edition.pioche_idmotcle", inputAttributes))
                .__(Grid.selectRow("_ label.edition.pioche_thesaurus", name(SubsetKey.CATEGORY_THESAURUS_STRING).attr("data-appelant-role", "subset-select").attr("data-appelant-target", "#" + inputAttributes.id()),
                        SubsetTreeOptions.init(TreeFilterEngine.admin(permissionSummary, bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_THESAURUS)), bdfServer, workingLang)
                                .onlyNames(true)
                                .withKeys(false)
                                .selectedSubsetKey(SubsetKey.CATEGORY_THESAURUS, storeMap)
                ))
                .__(Grid.textInputRow("_ label.global.mode", name(InteractionConstants.MODE_PARAMNAME).value(storeMap).size("15")))
                .__(Grid.END)
                .__(Button.COMMAND,
                        Button.submit("action-FicheIndexation", "_ link.global.ok"))
                ._FORM()
                .__(PageUnit.END);
        end();
    }

}
