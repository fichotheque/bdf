/*  BdfServer_HtmlProducers - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers;

import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.roles.Role;
import java.util.function.Consumer;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public final class CommandBoxUtils {

    private CommandBoxUtils() {
    }

    public static Consumer<CommandBox> insert(Object object) {
        return new ObjectInsertion(object);
    }

    public static boolean insertThesaurus(CommandBox commandBox, Thesaurus thesaurus) {
        putSubset(commandBox, thesaurus);
        return true;
    }

    public static boolean insertMotcle(CommandBox commandBox, Motcle motcle) {
        insertThesaurus(commandBox, motcle.getThesaurus());
        commandBox.hidden(InteractionConstants.ID_PARAMNAME, String.valueOf(motcle.getId()));
        return true;
    }

    private static void putSubset(CommandBox commandBox, Subset subset) {
        putSubset(commandBox, subset.getSubsetKey());
    }

    private static void putSubset(CommandBox commandBox, SubsetKey subsetKey) {
        commandBox.hidden(subsetKey.getCategoryString(), subsetKey.getSubsetName());
    }


    private static class ObjectInsertion implements Consumer<CommandBox> {

        private final Object object;

        private ObjectInsertion(Object object) {
            this.object = object;
        }

        @Override
        public void accept(CommandBox commandBox) {
            if (object instanceof SubsetItem) {
                SubsetItem subsetItem = (SubsetItem) object;
                putSubset(commandBox, subsetItem.getSubset());
                commandBox.hidden(InteractionConstants.ID_PARAMNAME, String.valueOf(subsetItem.getId()));
            } else if (object instanceof Subset) {
                putSubset(commandBox, (Subset) object);
            } else if (object instanceof Role) {
                commandBox.hidden("role", ((Role) object).getName());
            } else if (object instanceof AlbumDim) {
                AlbumDim albumDim = (AlbumDim) object;
                putSubset(commandBox, albumDim.getAlbumMetadata().getAlbum());
                commandBox.hidden("albumdim", albumDim.getName());
            } else if (object instanceof ScrutariExportDef) {
                commandBox.hidden(InteractionConstants.SCRUTARIEXPORT_PARAMNAME, ((ScrutariExportDef) object).getName());
            } else if (object instanceof SqlExportDef) {
                commandBox.hidden(InteractionConstants.SQLEXPORT_PARAMNAME, ((SqlExportDef) object).getName());
            }
        }

    }

}
