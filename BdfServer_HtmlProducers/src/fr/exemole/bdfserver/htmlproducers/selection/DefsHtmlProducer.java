/* BdfServer_HtmlProducers - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.selection;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.AdminJsLibs;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class DefsHtmlProducer extends BdfServerHtmlProducer {

    public DefsHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addJsLib(AdminJsLibs.SELECTIONDEF);
        addThemeCss("pane.css", "selectiondef.css");
    }

    @Override
    public void printHtml() {
        startLoc("_ link.selection.selectiondefadmin", true);
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

    @Override
    protected void setIcons() {
        addIconPng("theme/icons/16x16/menu-selection.png", "16");
    }

}
