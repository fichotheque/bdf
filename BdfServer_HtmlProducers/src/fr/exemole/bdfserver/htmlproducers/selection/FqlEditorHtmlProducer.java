/* BdfServer_HtmlProducers - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.selection;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.MiscJsLibs;
import net.mapeadores.util.html.HA;


/**
 *
 * @author Vincent Calame
 */
public class FqlEditorHtmlProducer extends BdfServerHtmlProducer {

    public FqlEditorHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
        addThemeCss("fqleditor.css");
        addJsLib(MiscJsLibs.FQLEDITOR);
        setBodyCssClass("global-body-Transparent");
    }

    @Override
    public void printHtml() {
        start();
        this
                .DIV(HA.id("fqleditor").classes("fqleditor-Client"))
                ._DIV();
        end();
    }

}
