/* BdfServer_HtmlProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.selection;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.SelectionDomain;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.tools.selection.FichothequeQueriesBuilder;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class SelectionHtmlProducerFactory {

    private SelectionHtmlProducerFactory() {
    }

    public static HtmlProducer getHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        BdfServerHtmlProducer htmlProducer = getBdfHtmlProducer(parameters);
        if (htmlProducer != null) {
            htmlProducer.setBdfCommandResult(parameters.getBdfCommandResult());
        }
        return htmlProducer;
    }

    private static BdfServerHtmlProducer getBdfHtmlProducer(OutputParameters parameters) throws ErrorMessageException {
        String page = parameters.getOutput();
        RequestMap requestMap = parameters.getRequestMap();
        switch (page) {
            case SelectionDomain.DEFS_PAGE: {
                return new DefsHtmlProducer(parameters);
            }
            case SelectionDomain.SELECTFORM_PAGE: {
                String selectionName = requestMap.getParameter(SelectionDomain.SELECTIONNAME_PARAMNAME);
                return new SelectFormHtmlProducer(parameters, selectionName);
            }
            case SelectionDomain.FQLEDITOR_PAGE: {
                return new FqlEditorHtmlProducer(parameters);
            }
            case SelectionDomain.FQLXML_PAGE: {
                return new FqlXmlHtmlProducer(parameters, getFichothequeQueries(parameters));
            }
            default:
                return null;
        }
    }

    private static FichothequeQueries getFichothequeQueries(OutputParameters parameters) {
        return FichothequeQueriesBuilder.init().addFicheQuery(parameters.getBdfUser().getFicheQuery()).toFichothequeQueries();
    }

}
