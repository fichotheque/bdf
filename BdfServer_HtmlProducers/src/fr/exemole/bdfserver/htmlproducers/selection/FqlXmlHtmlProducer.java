/* BdfServer_HtmlProducers - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.htmlproducers.selection;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import java.io.IOException;
import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class FqlXmlHtmlProducer extends BdfServerHtmlProducer {

    private final FichothequeQueries fichothequeQueries;

    public FqlXmlHtmlProducer(BdfParameters bdfParameters, FichothequeQueries fichothequeQueries) {
        super(bdfParameters);
        addJsLib(BdfJsLibs.CODEMIRRORMODE);
        this.fichothequeQueries = fichothequeQueries;
    }

    @Override
    public void printHtml() {
        start();
        TEXTAREA(name("").rows(20).cols(100).attr("data-codemirror-mode", "xml"));
        __escape(getXml(), true);
        _TEXTAREA();
        end();
    }

    private String getXml() {
        StringBuilder buf = new StringBuilder();
        XMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
        try {
            FichothequeXMLUtils.writeFichothequeQueries(xmlWriter, fichothequeQueries);
        } catch (IOException ioe) {
        }
        return buf.toString();
    }

}
