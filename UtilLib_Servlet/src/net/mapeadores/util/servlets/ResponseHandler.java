/* UtilLib_Servlet - Copyright (c) 2006-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Vincent Calame
 */
public interface ResponseHandler {

    public static final long LASTMODIFIED_NOCACHE = -1;

    /**
     * Indique la date de dernière modification de la ressource.
     * Retourne LASTMODIFIED_NOCACHE si la date n'est pas connue et que la ressource
     * ne doit pas être mise en cache par le navigateur (cas de la plupart des ressources
     * dynamiques)
     *
     * @return
     */
    public long getLastModified();

    /**
     * Écrit la réponse. L'entêtte de la réponse a pu être modifié
     * suivant la valeur de getLastModified()
     *
     * @param response
     * @throws IOException
     */
    public void handleResponse(HttpServletResponse response) throws IOException;

}
