/* UtilLib_Servlet - Copyright (c) 2009 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author Vincent Calame
 */
public interface MultiPartParser {

    public boolean isMultiPartContentRequest(HttpServletRequest request);

    public Map<String, Object> parseMultiPartContentRequest(HttpServletRequest request);

}
