/* UtilLib_Servlet - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets.handlers;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public class RedirectResponseHandler implements ResponseHandler {

    private final String path;

    public RedirectResponseHandler(String path) {
        this.path = path;
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.sendRedirect(path);
    }

    public static RedirectResponseHandler init(String path) {
        return new RedirectResponseHandler(path);
    }

}
