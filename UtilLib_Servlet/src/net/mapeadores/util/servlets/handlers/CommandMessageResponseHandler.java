/* UtilLib_Servlet  - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets.handlers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LineMessage;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public class CommandMessageResponseHandler implements ResponseHandler {


    private final MessageLocalisation messageLocalisation;
    private final CommandMessage[] commandMessages;
    private long lastModified = ResponseHandler.LASTMODIFIED_NOCACHE;

    public CommandMessageResponseHandler(MessageLocalisation messageLocalisation, CommandMessage... commandMessages) {
        this.commandMessages = commandMessages;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public long getLastModified() {
        return lastModified;
    }

    public CommandMessageResponseHandler lastModified(long lastModified) {
        this.lastModified = lastModified;
        return this;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType(MimeTypeConstants.HTML + ";charset=UTF-8");
        try (PrintWriter pw = response.getWriter()) {
            HtmlPrinter hp = new HtmlPrinter();
            hp.initPrinter(pw);
            hp.addMessageLocalisation(messageLocalisation);
            hp
                    .DOCTYPE_html5()
                    .HTML();
            hp
                    .HEAD()
                    .TITLE()
                    .__escape("CommandMessage")
                    ._TITLE()
                    .META_htmlcontenttype()
                    ._HEAD();
            hp
                    .BODY();
            for (CommandMessage commandMessage : commandMessages) {
                printMessage(hp, commandMessage);
            }
            hp
                    ._BODY();
            hp
                    ._HTML();
        }
    }

    private void printMessage(HtmlPrinter hp, CommandMessage commandMessage) {
        if (commandMessage == null) {
            return;
        }
        hp
                .COMMENT()
                .__escape("messageKey: ")
                .__escape(commandMessage.getMessageKey())
                ._COMMENT()
                .__newLine();
        HtmlAttributes spanAttributes;
        if (commandMessage.isErrorMessage()) {
            spanAttributes = HA.style("color: red");
        } else {
            spanAttributes = HA.style("color: green");
        }
        hp
                .P()
                .SPAN(spanAttributes)
                .__localize(commandMessage)
                ._SPAN()
                ._P();
        if (commandMessage.hasMultiError()) {
            hp
                    .UL();
            for (Message errorMessage : commandMessage.getMultiErrorList()) {
                hp
                        .LI();
                if (errorMessage instanceof LineMessage) {
                    hp
                            .__append(((LineMessage) errorMessage).getLineNumber())
                            .__escape(": ");
                }
                hp
                        .__localize(errorMessage)
                        ._LI();
            }
            hp
                    ._UL();
        }
        if (commandMessage.hasLog()) {
            hp
                    .PRE()
                    .__escape(LogUtils.toString(commandMessage.getMessageLog()), true)
                    ._PRE();
        }
    }

}
