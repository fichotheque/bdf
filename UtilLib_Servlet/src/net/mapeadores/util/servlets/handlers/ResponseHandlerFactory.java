/* UtilLib_Servlet - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets.handlers;

import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.CommandMessageJsonProducer;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.request.RequestConstants;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.request.RequestUtils;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class ResponseHandlerFactory {

    private ResponseHandlerFactory() {

    }

    public static ResponseHandler getInstance(MessageLocalisation messageLocalisation, RequestMap requestMap, CommandMessage... commandMessages) {
        if (RequestUtils.isJsonDefined(requestMap)) {
            return getJsonInstance(messageLocalisation, requestMap, commandMessages);
        } else {
            return getHtmlInstance(messageLocalisation, commandMessages);
        }
    }

    public static ResponseHandler getJsonInstance(MessageLocalisation messageLocalisation, RequestMap requestMap, CommandMessage... commandMessages) {
        JsonProducer jsonProducer = new CommandMessageJsonProducer(messageLocalisation, commandMessages);
        short jsonType = RequestUtils.getJsonType(requestMap);
        return new JsonResponseHandler(jsonProducer, jsonType, requestMap.getParameter(RequestConstants.CALLBACK_PARAMETER));
    }

    public static ResponseHandler getHtmlInstance(MessageLocalisation messageLocalisation, CommandMessage... commandMessages) {
        return new CommandMessageResponseHandler(messageLocalisation, commandMessages);
    }

    public static ResponseHandler getHtmlErrorInstance(MessageLocalisation messageLocalisation, String messageKey, Object... messageValues) {
        return new CommandMessageResponseHandler(messageLocalisation, LogUtils.error(messageKey, messageValues));
    }

    public static ResponseHandler getTextInstance(MessageLocalisation messageLocalisation, CommandMessage... commandMessages) {
        StringBuilder buf = new StringBuilder();
        for (CommandMessage commandMessage : commandMessages) {
            String loc = messageLocalisation.toString(commandMessage);
            if (loc == null) {
                loc = "?" + commandMessage.getMessageKey() + "?";
            }
            buf.append(loc);
            buf.append("\n");
        }
        return SimpleResponseHandler.init(buf.toString());
    }

    public static ResponseHandler getTextErrorInstance(MessageLocalisation messageLocalisation, String messageKey, Object... messageValues) {
        return getTextInstance(messageLocalisation, LogUtils.error(messageKey, messageValues));
    }

}
