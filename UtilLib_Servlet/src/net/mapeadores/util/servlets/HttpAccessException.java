/* UtilLib_Servlet - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets;

import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;


/**
 *
 * @author Vincent Calame
 */
public class HttpAccessException extends RuntimeException {

    private final int httpType;
    private final CommandMessage commandMessage;
    private String messageText;

    public HttpAccessException(int httpType) {
        this.httpType = httpType;
        this.commandMessage = null;
        this.messageText = null;
    }

    public HttpAccessException(int httpType, CommandMessage commandMessage) {
        this.httpType = httpType;
        this.commandMessage = commandMessage;
        this.messageText = null;
    }

    public HttpAccessException(int httpType, String messageText) {
        this.httpType = httpType;
        this.commandMessage = null;
        this.messageText = messageText;
    }

    public int getHttpType() {
        return httpType;
    }

    @Override
    public String getMessage() {
        if (messageText != null) {
            return messageText;
        } else if (commandMessage != null) {
            return commandMessage.getMessageKey();
        } else {
            return null;
        }
    }

    public CommandMessage getCommandMessage() {
        return commandMessage;
    }

    public boolean isUnresolvedMessage() {
        return ((commandMessage != null) && (messageText == null));
    }

    public void resolveMessage(MessageLocalisation messageLocalisation) {
        this.messageText = messageLocalisation.toString(commandMessage);
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

}
