/* FichothequeLib_Impl - Copyright (c) 2006-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import net.fichotheque.EditOrigin;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.addenda.VersionInfo;


/**
 *
 * @author Vincent Calame
 */
public interface AddendaDataSource {

    public void saveMetadata(Addenda addenda, EditOrigin editOrigin);

    public void saveDocument(Document document, EditOrigin editOrigin);

    public void removeDocument(Addenda addenda, int id, EditOrigin editOrigin);

    public InputStream getInputStream(Document document, String extension);

    public boolean deleteVersion(Document document, String extension);

    /**
     *
     * @throws IllegalArgumentException si l'extension est incorrecte.
     */
    public VersionInfo saveVersion(Document document, String extension, InputStream inputStream);

    public boolean linkTo(Document document, String extension, Path destination, boolean symbolicLink) throws IOException;

}
