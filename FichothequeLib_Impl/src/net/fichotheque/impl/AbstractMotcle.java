/* FichothequeLib_Impl - Copyright (c) 2006-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.Subset;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.MotcleIcon;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 * Usage interne : implémente toutes les méthodes d'un mot-clé hors de ce qui
 * touche à la langue.
 *
 * @author Vincent Calame
 */
abstract class AbstractMotcle extends AbstractSubsetItem implements Motcle {

    private final ThesaurusImpl thesaurus;
    private final int id;
    private String idalpha;
    private int level = 1;
    private AbstractMotcle parent;
    private int childIndex;
    private Motcles motcles;
    private String status = FichothequeConstants.ACTIVE_STATUS;
    private MotcleIcon motcleIcon;
    private boolean motcleInit = false;

    public AbstractMotcle(ThesaurusImpl thesaurus, int id) {
        this.thesaurus = thesaurus;
        this.id = id;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public Motcle getParent() {
        return parent;
    }

    @Override
    public MotcleIcon getMotcleIcon() {
        if (motcleInit) {
            return motcleIcon;
        }
        motcleIcon = MotcleIconImpl.build(getAttributes());
        motcleInit = true;
        return motcleIcon;
    }

    @Override
    public Subset getSubset() {
        return thesaurus;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getIdalpha() {
        return idalpha;
    }

    @Override
    public int getChildIndex() {
        return childIndex;
    }

    @Override
    public List<Motcle> getChildList() {
        if (motcles == null) {
            return ThesaurusUtils.EMPTY_MOTCLELIST;
        } else {
            return motcles.getCache();
        }
    }

    @Override
    public String getStatus() {
        return status;
    }

    abstract boolean putLabel(Label label);

    abstract boolean removeLabel(Lang lang);

    @Override
    void fireAttributeChange() {
        motcleIcon = null;
        motcleInit = false;
    }

    boolean setIdalpha(String newIdalpha) {
        if (idalpha == null) {
            idalpha = newIdalpha;
            return true;
        } else {
            if (newIdalpha.equals(idalpha)) {
                return false;
            }
            idalpha = newIdalpha;
            return true;
        }
    }

    boolean setParent(AbstractMotcle newParent) {
        boolean todo = parentTest(newParent);
        if (!todo) {
            return false;
        }
        removeFromParent();
        if (newParent != null) {
            level = newParent.getLevel() + 1;
            newParent.addChild(this);
        } else {
            level = 1;
            thesaurus.addInFirstLevel(this);
        }
        parent = newParent;
        return true;
    }

    boolean setStatus(String newStatus) {
        if (newStatus.equals(status)) {
            return false;
        }
        status = FichothequeConstants.checkMotcleStatus(newStatus);
        return true;
    }

    void removeFromParent() {
        if (parent != null) {
            parent.removeChild(childIndex);
        } else {
            thesaurus.removeFromFirstLevel(childIndex);
        }
        childIndex = -1;
        level = -1;
    }

    private void removeChild(int index) {
        if (motcles != null) {
            motcles.remove(index);
        }
    }

    private void addChild(AbstractMotcle motcle) {
        if (motcles == null) {
            motcles = new Motcles();
        }
        motcles.add(motcle);
    }

    void decreaseIndex() {
        childIndex = childIndex - 1;
    }

    void increaseIndex() {
        childIndex = childIndex + 1;
    }

    void setChildIndex(int childIndex) {
        this.childIndex = childIndex;
    }

    boolean changeIndex(int newIndex) {
        if (newIndex == childIndex) {
            return false;
        }
        Motcles list = (parent != null) ? parent.motcles : thesaurus.getFirstLevelMotcleList();
        list.permute(childIndex, newIndex);
        this.childIndex = newIndex;
        return true;
    }

    boolean parentTest(AbstractMotcle newParent) {
        if (newParent == null) {
            return (parent != null);
        } else if (parent == null) {
            return true;
        } else {
            return !parent.equals(newParent);
        }
    }

}
