/* FichothequeLib_Impl - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.FichothequeEditor;
import net.fichotheque.utils.DefaultMetadata;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
abstract class AbstractMetadata extends DefaultMetadata {


    AbstractMetadata() {
    }

    boolean innerRemoveAttribute(AttributeKey attributeKey) {
        return removeAttribute(attributeKey);
    }

    boolean innerPutAttribute(Attribute attribute) {
        return putAttribute(attribute);
    }

    boolean innerPutLabel(String name, Label label) {
        return putLabel(name, label);
    }

    boolean innerRemoveLabel(String name, Lang lang) {
        return removeLabel(name, lang);
    }

    protected abstract void fireChange(FichothequeEditor fichothequeEditor);


}
