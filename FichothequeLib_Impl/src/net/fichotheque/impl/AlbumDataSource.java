/* FichothequeLib_Impl - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.SubsetKey;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.mapeadores.util.awt.ResizeInfo;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumDataSource {

    public void saveMetadata(Album album, EditOrigin editOrigin);

    public void saveIllustration(Illustration illustration, EditOrigin editOrigin);

    public void removeIllustration(Album album, int id, EditOrigin editOrigin);

    public InputStream getInputStream(Illustration illustration, String dimName, ResizeInfo resizeInfo);

    public void update(Illustration illustration, InputStream inputStream);

    public void clearDimCache(SubsetKey albumKey, String dimName);

}
