/* FichothequeLib_Impl - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.namespaces.IconSpace;
import net.fichotheque.thesaurus.MotcleIcon;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
class MotcleIconImpl implements MotcleIcon {

    private final String color;
    private final String charIcon;
    private final String fontAwesomeIcon;

    MotcleIconImpl(String color, String charIcon, String fontAwesomeIcon) {
        this.color = color;
        this.charIcon = charIcon;
        this.fontAwesomeIcon = fontAwesomeIcon;
    }

    @Override
    public String getColor() {
        return color;
    }

    @Override
    public String getCharIcon() {
        return charIcon;
    }

    @Override
    public String getFontAwesomeIcon() {
        return fontAwesomeIcon;
    }


    static MotcleIconImpl build(Attributes attributes) {
        String color = attributes.getFirstValue(IconSpace.COLOR_KEY);
        String charIcon = attributes.getFirstValue(IconSpace.CHAR_KEY);
        String fontAwesomeIcon = attributes.getFirstValue(IconSpace.FONTAWESOME_KEY);
        if ((color == null) && (charIcon == null) && (fontAwesomeIcon == null)) {
            return null;
        }
        return new MotcleIconImpl(color, charIcon, fontAwesomeIcon);
    }

}
