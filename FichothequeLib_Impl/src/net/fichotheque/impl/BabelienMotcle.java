/* FichothequeLib_Impl - Copyright (c) 2006-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
class BabelienMotcle extends AbstractMotcle {

    private final static Lang DEFAULT_LANG = Lang.build("zxx");
    private final static Label DEFAULT_LABEL = new NoLinguisticContentLabel();
    private final static Labels DEFAULT_LABELS = LabelUtils.toSingletonLabels(DEFAULT_LABEL);
    private Labels labels = DEFAULT_LABELS;

    public BabelienMotcle(ThesaurusImpl thesaurus, int id) {
        super(thesaurus, id);
    }

    @Override
    public Labels getLabels() {
        return labels;
    }

    @Override
    boolean putLabel(Label label) {
        if (LabelUtils.areEquals(this.labels.get(0), label)) {
            return false;
        }
        if (label.getLang().equals(DEFAULT_LANG)) {
            this.labels = DEFAULT_LABELS;
        } else {
            this.labels = LabelUtils.toSingletonLabels(label);
        }
        return true;
    }

    @Override
    boolean removeLabel(Lang lang) {
        if (this.labels == DEFAULT_LABELS) {
            return false;
        }
        this.labels = DEFAULT_LABELS;
        return true;
    }


    private static class NoLinguisticContentLabel implements Label {

        private NoLinguisticContentLabel() {

        }

        @Override
        public String getLabelString() {
            return "?";
        }

        @Override
        public Lang getLang() {
            return DEFAULT_LANG;
        }

    }

}
