/* FichothequeLib_Impl - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.EditOrigin;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.metadata.FichothequeMetadata;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeDataSource {

    public AddendaDataSource getAddendaDataSource();

    public AlbumDataSource getAlbumDataSource();

    public CorpusDataSource getCorpusDataSource();

    public SphereDataSource getSphereDataSource();

    public ThesaurusDataSource getThesaurusDataSource();

    public CroisementDataSource getCroisementDataSource();

    public void saveFichothequeMetadata(FichothequeMetadata fichothequeMetadata, EditOrigin editOrigin);

    public void removeAddenda(SubsetKey addendaKey, EditOrigin editOrigin);

    public void removeAlbum(SubsetKey albumKey, EditOrigin editOrigin);

    public void removeCorpus(SubsetKey corpusKey, Subset masterSubset, EditOrigin editOrigin);

    public void removeSphere(SubsetKey sphereKey, EditOrigin editOrigin);

    public void removeThesaurus(SubsetKey thesaurusKey, EditOrigin editOrigin);

}
