/* FichothequeLib_Impl - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import java.util.List;
import net.fichotheque.EditOrigin;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.history.CroisementHistory;
import net.fichotheque.history.FicheHistory;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusDataSource {

    public void saveMetadata(Corpus corpus, EditOrigin editOrigin);

    public Fiche getFiche(FicheMeta ficheMeta, boolean ficheComplete);

    public void saveFiche(FicheMeta ficheMeta, FicheAPI fiche, EditOrigin editOrigin);

    public void saveChrono(FicheMeta ficheMeta, EditOrigin editOrigin);

    public void saveAttributes(FicheMeta ficheMeta, EditOrigin editOrigin);

    public void removeFiche(Corpus corpus, int id, EditOrigin editOrigin);

    public void commitChanges();

    public FicheHistory getFicheHistory(SubsetKey corpusKey, int id);

    public Fiche getFicheRevision(SubsetKey corpusKey, int id, String revisionName);

    public List<FicheHistory> getRemovedFicheHistoryList(Corpus corpus);

    public List<CroisementHistory> getCroisementHistoryList(SubsetKey corpusKey, int id, SubsetKey thesaurusKey);

}
