/* FichothequeLib_Impl - Copyright (c) 2006-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.FichothequeEditor;
import net.fichotheque.FichothequeListener;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.misc.ArrayUtils;


/**
 *
 * @author Vincent Calame
 */
class Listeners {

    private FichothequeListener[] fichothequeListenerArray;

    Listeners() {
        fichothequeListenerArray = new FichothequeListener[0];
    }

    void addFichothequeListener(FichothequeListener fichothequeListener) {
        int length = fichothequeListenerArray.length;
        fichothequeListenerArray = (FichothequeListener[]) ArrayUtils.addUnique(fichothequeListenerArray, fichothequeListener, new FichothequeListener[length + 1]);
    }

    void removeFichothequeListener(FichothequeListener fichothequeListener) {
        int length = fichothequeListenerArray.length;
        if (length > 0) {
            fichothequeListenerArray = (FichothequeListener[]) ArrayUtils.removeUnique(fichothequeListenerArray, fichothequeListener, new FichothequeListener[length - 1]);
        }
    }

    void fireSubsetCreated(FichothequeEditor fichothequeEditor, Subset subset) {
        for (FichothequeListener fichothequeListener : fichothequeListenerArray) {
            fichothequeListener.subsetCreated(fichothequeEditor, subset);
        }
    }

    void fireSubsetRemoved(FichothequeEditor fichothequeEditor, SubsetKey corpusKey, Subset masterSubset) {
        for (FichothequeListener fichothequeListener : fichothequeListenerArray) {
            fichothequeListener.subsetRemoved(fichothequeEditor, corpusKey, masterSubset);
        }
    }

    void fireCorpusFieldCreated(FichothequeEditor fichothequeEditor, Corpus corpus, CorpusField corpusField) {
        for (FichothequeListener fichothequeListener : fichothequeListenerArray) {
            fichothequeListener.corpusFieldCreated(fichothequeEditor, corpus, corpusField);
        }
    }

    void fireCorpusFieldRemoved(FichothequeEditor fichothequeEditor, Corpus corpus, FieldKey fieldKey) {
        for (FichothequeListener fichothequeListener : fichothequeListenerArray) {
            fichothequeListener.corpusFieldRemoved(fichothequeEditor, corpus, fieldKey);
        }
    }

    void fireSubsetItemCreated(FichothequeEditor fichothequeEditor, SubsetItem subsetItem) {
        for (FichothequeListener fichothequeListener : fichothequeListenerArray) {
            fichothequeListener.subsetItemCreated(fichothequeEditor, subsetItem);
        }
    }

    void fireFicheSaved(FichothequeEditor fichothequeEditor, FicheMeta ficheMeta, FicheAPI fiche) {
        for (FichothequeListener fichothequeListener : fichothequeListenerArray) {
            fichothequeListener.ficheSaved(fichothequeEditor, ficheMeta, fiche);
        }
    }

    void fireSubsetItemRemoved(FichothequeEditor fichothequeEditor, Subset subset, int id) {
        for (FichothequeListener fichothequeListener : fichothequeListenerArray) {
            fichothequeListener.subsetItemRemoved(fichothequeEditor, subset, id);
        }
    }

}
