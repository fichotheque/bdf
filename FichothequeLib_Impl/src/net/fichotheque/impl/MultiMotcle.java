/* FichothequeLib_Impl - Copyright (c) 2006-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.LabelsCache;


/**
 *
 * @author Vincent Calame
 */
class MultiMotcle extends AbstractMotcle {

    private final LabelsCache labelsCache = new LabelsCache();

    public MultiMotcle(ThesaurusImpl thesaurus, int id) {
        super(thesaurus, id);
    }

    @Override
    public Labels getLabels() {
        return labelsCache.getLabels();
    }

    @Override
    boolean putLabel(Label label) {
        return labelsCache.putLabel(label);
    }

    @Override
    boolean removeLabel(Lang lang) {
        return labelsCache.removeLabel(lang);
    }

}
