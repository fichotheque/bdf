/* FichothequeLib_Impl - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.EditOrigin;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusDataSource {

    public void saveMetadata(Thesaurus thesaurus, EditOrigin editOrigin);

    public void saveTree(Thesaurus thesaurus, EditOrigin editOrigin);

    public void saveMotcle(Motcle motcle, EditOrigin editOrigin);

    public void removeMotcle(Thesaurus thesaurus, int id, EditOrigin editOrigin);

}
