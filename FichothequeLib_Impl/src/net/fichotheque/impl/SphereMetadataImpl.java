/* FichothequeLib_Impl - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.FichothequeEditor;
import net.fichotheque.Metadata;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.sphere.SphereEditor;
import net.fichotheque.sphere.metadata.SphereMetadata;
import net.fichotheque.sphere.metadata.SphereMetadataEditor;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
class SphereMetadataImpl extends AbstractMetadata implements SphereMetadata {

    private final SphereImpl sphere;

    private SphereMetadataImpl(SphereImpl sphere) {
        this.sphere = sphere;
    }

    static SphereMetadataImpl.InitEditor fromInit(SphereImpl.InitEditor sphereInitEditor) {
        SphereMetadataImpl metadata = new SphereMetadataImpl((SphereImpl) sphereInitEditor.getSphere());
        return new SphereMetadataImpl.InitEditor(metadata, sphereInitEditor);
    }

    static SphereMetadataImpl fromNew(SphereImpl sphere) {
        return new SphereMetadataImpl(sphere);
    }

    SphereMetadataEditor getSphereMetadataEditor(SphereImpl.SphereEditorImpl sphereEditor) {
        return new SphereMetadataEditorImpl(sphereEditor);
    }

    @Override
    public Sphere getSphere() {
        return sphere;
    }

    @Override
    protected void fireChange(FichothequeEditor fichothequeEditor) {
        SphereImpl.SphereEditorImpl sphereEditor = (SphereImpl.SphereEditorImpl) fichothequeEditor.getSphereEditor(sphere.getSubsetKey());
        sphereEditor.setMetadataChange();
    }


    static class InitEditor implements SphereMetadataEditor {

        private final SphereMetadataImpl sphereMetadata;
        private final SphereEditor sphereEditor;

        private InitEditor(SphereMetadataImpl sphereMetadata, SphereEditor sphereEditor) {
            this.sphereMetadata = sphereMetadata;
            this.sphereEditor = sphereEditor;
        }

        @Override
        public SphereEditor getSphereEditor() {
            return sphereEditor;
        }

        @Override
        public Metadata getMetadata() {
            return sphereMetadata;
        }

        @Override
        public boolean putLabel(String name, Label label) {
            return sphereMetadata.innerPutLabel(name, label);
        }

        @Override
        public boolean removeLabel(String name, Lang lang) {
            return sphereMetadata.innerRemoveLabel(name, lang);
        }

    }


    private class SphereMetadataEditorImpl implements SphereMetadataEditor {

        private final SphereImpl.SphereEditorImpl sphereEditor;

        private SphereMetadataEditorImpl(SphereImpl.SphereEditorImpl sphereEditor) {
            this.sphereEditor = sphereEditor;
        }

        @Override
        public SphereEditor getSphereEditor() {
            return sphereEditor;
        }

        @Override
        public Metadata getMetadata() {
            return SphereMetadataImpl.this;
        }

        @Override
        public boolean putLabel(String name, Label label) {
            boolean done = innerPutLabel(name, label);
            if (done) {
                fireChange();
            }
            return done;
        }

        @Override
        public boolean removeLabel(String name, Lang lang) {
            boolean done = innerRemoveLabel(name, lang);
            if (done) {
                fireChange();
            }
            return done;
        }

        private void fireChange() {
            sphereEditor.setMetadataChange();
        }

    }

}
