/* FichothequeLib_Impl - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.FichothequeEditor;
import net.fichotheque.Metadata;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.AddendaEditor;
import net.fichotheque.addenda.metadata.AddendaMetadata;
import net.fichotheque.addenda.metadata.AddendaMetadataEditor;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
class AddendaMetadataImpl extends AbstractMetadata implements AddendaMetadata {

    private final AddendaImpl addenda;

    private AddendaMetadataImpl(AddendaImpl addenda) {
        this.addenda = addenda;
    }

    static AddendaMetadataImpl.InitEditor fromInit(AddendaImpl.InitEditor addendaInitEditor) {
        AddendaMetadataImpl metadata = new AddendaMetadataImpl((AddendaImpl) addendaInitEditor.getAddenda());
        return new AddendaMetadataImpl.InitEditor(metadata, addendaInitEditor);
    }

    static AddendaMetadataImpl fromNew(AddendaImpl addenda) {
        return new AddendaMetadataImpl(addenda);
    }

    @Override
    public Addenda getAddenda() {
        return addenda;
    }

    @Override
    protected void fireChange(FichothequeEditor fichothequeEditor) {
        AddendaImpl.AddendaEditorImpl addendaEditor = (AddendaImpl.AddendaEditorImpl) fichothequeEditor.getAddendaEditor(addenda);
        addendaEditor.setMetadataChange();
    }

    AddendaMetadataEditor getAddendaMetadataEditor(AddendaImpl.AddendaEditorImpl addendaEditor) {
        return new AddendaMetadataEditorImpl(addendaEditor);
    }


    static class InitEditor implements AddendaMetadataEditor {

        private final AddendaMetadataImpl addendaMetadata;
        private final AddendaEditor addendaEditor;

        private InitEditor(AddendaMetadataImpl addendaMetadata, AddendaEditor addendaEditor) {
            this.addendaMetadata = addendaMetadata;
            this.addendaEditor = addendaEditor;
        }

        @Override
        public AddendaMetadata getAddendaMetadata() {
            return addendaMetadata;
        }

        @Override
        public AddendaEditor getAddendaEditor() {
            return addendaEditor;
        }

        @Override
        public Metadata getMetadata() {
            return addendaMetadata;
        }

        @Override
        public boolean putLabel(String name, Label label) {
            return addendaMetadata.innerPutLabel(name, label);
        }

        @Override
        public boolean removeLabel(String name, Lang lang) {
            return addendaMetadata.innerRemoveLabel(name, lang);
        }

    }


    private class AddendaMetadataEditorImpl implements AddendaMetadataEditor {

        private final AddendaImpl.AddendaEditorImpl addendaEditor;


        private AddendaMetadataEditorImpl(AddendaImpl.AddendaEditorImpl addendaEditor) {
            this.addendaEditor = addendaEditor;
        }

        @Override
        public AddendaMetadata getAddendaMetadata() {
            return AddendaMetadataImpl.this;
        }

        @Override
        public AddendaEditor getAddendaEditor() {
            return addendaEditor;
        }

        @Override
        public Metadata getMetadata() {
            return AddendaMetadataImpl.this;
        }

        @Override
        public boolean putLabel(String name, Label label) {
            boolean done = innerPutLabel(name, label);
            if (done) {
                fireChange();
            }
            return done;
        }

        @Override
        public boolean removeLabel(String name, Lang lang) {
            boolean done = innerRemoveLabel(name, lang);
            if (done) {
                fireChange();
            }
            return done;
        }


        private void fireChange() {
            addendaEditor.setMetadataChange();
        }

    }

}
