/* FichothequeLib_Impl - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.EditOrigin;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;


/**
 *
 * @author Vincent Calame
 */
public interface SphereDataSource {

    public void saveMetadata(Sphere sphere, EditOrigin editOrigin);

    public void saveList(Sphere sphere, EditOrigin editOrigin);

    public void saveRedacteur(Redacteur redacteur, EditOrigin editOrigin);

    public void removeRedacteur(Sphere sphere, int id, EditOrigin editOrigin);

}
