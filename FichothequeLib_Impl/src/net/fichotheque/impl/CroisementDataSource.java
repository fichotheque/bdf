/* FichothequeLib_Impl - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import net.fichotheque.EditOrigin;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.croisement.CroisementKey;
import net.fichotheque.croisement.CroisementRevision;


/**
 *
 * @author Vincent Calame
 */
public interface CroisementDataSource {

    public void saveCroisement(Croisement croisement, EditOrigin editOrigin);

    public void removeCroisement(CroisementKey croisementKey, EditOrigin editOrigin);

    public CroisementRevision getCroisementRevision(CroisementKey croisementKey, String revisionName);

}
