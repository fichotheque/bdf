/* FichothequeLib_Impl - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.impl;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.utils.FichothequeUtils;


/**
 *
 * @author Vincent Calame
 */
class Satellites {

    private final List<Corpus> list = new ArrayList<Corpus>();
    private List<Corpus> cacheList = null;

    Satellites() {
    }

    List<Corpus> getCache() {
        List<Corpus> result = cacheList;
        if (result == null) {
            result = initCache();
        }
        return result;
    }

    synchronized void add(CorpusImpl corpus) {
        list.add(corpus);
        clearCache();
    }

    synchronized void remove(CorpusImpl corpus) {
        list.remove(corpus);
        clearCache();
    }

    private synchronized List<Corpus> initCache() {
        if (cacheList != null) {
            return cacheList;
        }
        List<Corpus> newCacheList = FichothequeUtils.wrap(list.toArray(new Corpus[list.size()]));
        cacheList = newCacheList;
        return newCacheList;
    }

    private void clearCache() {
        cacheList = null;
    }

}
