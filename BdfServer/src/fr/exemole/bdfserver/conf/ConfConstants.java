/* BdfServer - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.conf;


/**
 *
 * @author Vincent Calame
 */
public interface ConfConstants {

    public final static String UNIQUE_NAME = "_unique";
    public final static String ETC_ROOT = "etc.";
    public final static String ETC_CONFIG = "etc.config";
    public final static String ETC_CONFIG_DEFAULT = "etc.config.default";
    public final static String ETC_SCRIPTS = "etc.scripts";
    public final static String ETC_RESOURCES = "etc.resources";
    public final static String VAR_ROOT = "var.";
    public final static String VAR_DATA = "var.data";
    public final static String VAR_OUTPUT = "var.output";
    public final static String VAR_CACHE = "var.cache";
    public final static String VAR_BACKUP = "var.backup";
    public final static String VAR_RUN = "var.run";
    public final static String LIB_EXTENSIONS = "lib.extensions";
}
