/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.conf;

import java.io.File;
import java.nio.file.Path;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface ConfDirs {

    /**
     *
     * Jamais nul pour les valeurs BdfConf.ETC_* et BdfConf.VAR_*
     *
     * @param dirKey
     * @return
     */
    @Nullable
    public File getDir(String dirKey);

    /**
     *
     * Jamais nul pour les valeurs BdfConf.ETC_* et BdfConf.VAR_*
     *
     * @param dirKey
     * @return
     */
    @Nullable
    public default File getSubPath(String dirKey, String subPath) {
        File dir = getDir(dirKey);
        if (dir == null) {
            throw new IllegalArgumentException("Unkown dirKey: " + dirKey);
        }
        return new File(dir, subPath);
    }

    @Nullable
    public default Path getDirPath(String dirKey) {
        File file = getDir(dirKey);
        if (file == null) {
            return null;
        }
        return file.toPath();
    }

    @Nullable
    public default Path resolveSubPath(String dirKey, String subPath) {
        Path dirPath = getDirPath(dirKey);
        if (dirPath == null) {
            throw new IllegalArgumentException("Unkown dirKey: " + dirKey);
        }
        return dirPath.resolve(subPath);
    }

    @Nullable
    public default Path resolveSubPath(String dirKey, RelativePath relativePath) {
        return resolveSubPath(dirKey, relativePath.toString());
    }

}
