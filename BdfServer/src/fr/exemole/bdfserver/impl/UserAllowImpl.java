/* BdfServer - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.policies.UserAllow;
import fr.exemole.bdfserver.api.storage.PolicyStorage;


/**
 *
 * @author Vincent Calame
 */
class UserAllowImpl implements UserAllow {
    
    private final PolicyStorage policyStorage;
    private boolean dataChangeAllowed;
    private boolean passwordChangeAllowed;
    
    UserAllowImpl(PolicyStorage policyStorage) {
        this.policyStorage = policyStorage;
    }
    
    @Override
    public boolean isDataChangeAllowed() {
        return dataChangeAllowed;
    }
    
    @Override
    public boolean isPasswordChangeAllowed() {
        return passwordChangeAllowed;
    }
    
    boolean update(UserAllow userAllow) {
        boolean done = false;
        if (userAllow.isDataChangeAllowed() != dataChangeAllowed) {
            dataChangeAllowed = userAllow.isDataChangeAllowed();
            done = true;
        }
        if (userAllow.isPasswordChangeAllowed() != passwordChangeAllowed) {
            passwordChangeAllowed = userAllow.isPasswordChangeAllowed();
            done = true;
        }
        return done;
    }
    
    void save() {
        policyStorage.saveUserAllow(this);
    }
    
}
