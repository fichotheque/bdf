/* BdfServer - Copyright (c) 2016-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusLangChecker;
import net.mapeadores.util.localisation.Langs;


/**
 *
 * @author Vincent Calame
 */
class ThesaurusLangCheckerImpl implements ThesaurusLangChecker {

    ThesaurusLangCheckerImpl() {

    }

    @Override
    public Langs getAuthorizedLangs(Thesaurus thesaurus) {
        return thesaurus.getThesaurusMetadata().getAuthorizedLangs();
    }

}
