/* BdfServer - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.managers.ScrutariExportManager;
import fr.exemole.bdfserver.api.storage.ScrutariExportStorage;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;


/**
 *
 * @author Vincent Calame
 */
class ScrutariExportManagerImpl implements ScrutariExportManager {

    private final static List<ScrutariExportDef> EMPTY_LIST = Collections.emptyList();
    private final ScrutariExportStorage scrutariExportStorage;
    private final SortedMap<String, ScrutariExportDef> scrutariExportDefMap = new TreeMap<String, ScrutariExportDef>();
    private List<ScrutariExportDef> scrutariExportDefList = EMPTY_LIST;

    ScrutariExportManagerImpl(ScrutariExportStorage scrutariExportStorage) {
        this.scrutariExportStorage = scrutariExportStorage;
    }

    synchronized void check(List<ScrutariExportDef> scrutariExportDefList) {
        scrutariExportDefMap.clear();
        for (ScrutariExportDef scrutariExportDef : scrutariExportDefList) {
            scrutariExportDefMap.put(scrutariExportDef.getName(), scrutariExportDef);
        }
        updateList();
    }

    @Override
    public List<ScrutariExportDef> getScrutariExportDefList() {
        return scrutariExportDefList;
    }

    @Override
    public ScrutariExportDef getScrutariExportDef(String name) {
        return scrutariExportDefMap.get(name);
    }

    @Override
    public synchronized void putScrutariExportDef(ScrutariExportDef scrutariExportDef) {
        String name = scrutariExportDef.getName();
        scrutariExportDefMap.put(name, scrutariExportDef);
        updateList();
        scrutariExportStorage.saveScrutariExportDef(scrutariExportDef);
    }

    @Override
    public synchronized void removeScrutariExportDef(String name) {
        boolean here = scrutariExportDefMap.containsKey(name);
        if (!here) {
            return;
        }
        scrutariExportDefMap.remove(name);
        scrutariExportStorage.removeScrutariExportDef(name);
        updateList();
    }

    private void updateList() {
        this.scrutariExportDefList = new ScrutariExportDefList(scrutariExportDefMap.values().toArray(new ScrutariExportDef[scrutariExportDefMap.size()]));
    }


    private static class ScrutariExportDefList extends AbstractList<ScrutariExportDef> implements RandomAccess {

        private final ScrutariExportDef[] array;

        private ScrutariExportDefList(ScrutariExportDef[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ScrutariExportDef get(int index) {
            return array[index];
        }

    }

}
