/* BdfServer - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import fr.exemole.bdfserver.api.storage.ConfigurationStorage;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;


/**
 *
 * @author Vincent Calame
 */
class LangConfigurationImpl implements LangConfiguration {

    private final ConfigurationStorage configurationStorage;
    private Langs workingLangs = LangsUtils.toCleanLangs("fr");
    private Langs supplementaryLangs = LangsUtils.EMPTY_LANGS;
    private boolean allLanguages = false;
    private boolean withNonlatin = false;
    private boolean withoutSurnameFirst = false;

    LangConfigurationImpl(ConfigurationStorage configurationStorage) {
        this.configurationStorage = configurationStorage;
    }

    @Override
    public Langs getWorkingLangs() {
        return workingLangs;
    }

    @Override
    public Langs getSupplementaryLangs() {
        return supplementaryLangs;
    }

    @Override
    public boolean isAllLanguages() {
        return allLanguages;
    }

    @Override
    public boolean isWithNonlatin() {
        return withNonlatin;
    }

    @Override
    public boolean isWithoutSurnameFirst() {
        return withoutSurnameFirst;
    }

    void update(LangConfiguration langConfiguration) {
        this.workingLangs = LangsUtils.fromCollection(langConfiguration.getWorkingLangs());
        this.supplementaryLangs = LangsUtils.fromCollection(langConfiguration.getSupplementaryLangs());
        this.allLanguages = langConfiguration.isAllLanguages();
        this.withNonlatin = langConfiguration.isWithNonlatin();
        this.withoutSurnameFirst = langConfiguration.isWithoutSurnameFirst();
    }

    void save() {
        configurationStorage.saveLangConfiguration(this);
    }

}
