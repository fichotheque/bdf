/* BdfServer - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import fr.exemole.bdfserver.api.users.BdfUserPrefsEditor;
import java.util.Locale;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesCache;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;


/**
 *
 * @author Vincent Calame
 */
class BdfUserPrefsImpl implements BdfUserPrefs {

    private final BdfUserImpl bdfUserImpl;
    private final AttributesCache attributesCache = new AttributesCache();
    private Lang workingLang;
    private Locale customFormatLocale;
    private LangPreference customLangPreference;
    FicheQuery defaultFicheQuery;

    BdfUserPrefsImpl(BdfUserImpl bdfUserImpl) {
        this.bdfUserImpl = bdfUserImpl;
    }

    @Override
    public Lang getWorkingLang() {
        return workingLang;
    }

    @Override
    public Locale getCustomFormatLocale() {
        return customFormatLocale;
    }

    @Override
    public LangPreference getCustomLangPreference() {
        return customLangPreference;
    }

    @Override
    public FicheQuery getDefaultFicheQuery() {
        return defaultFicheQuery;
    }

    @Override
    public Attributes getAttributes() {
        return attributesCache.getAttributes();
    }

    Redacteur getRedacteur() {
        return bdfUserImpl.getRedacteur();
    }

    BdfUserPrefsEditor getBdfUserPrefsEditor(BdfServer bdfServer) {
        return new BdfUserPrefsEditorImpl(bdfServer);
    }

    boolean removeAttribute(AttributeKey attributeKey) {
        return attributesCache.removeAttribute(attributeKey);
    }

    boolean putAttribute(Attribute attribute) {
        return attributesCache.putAttribute(attribute);
    }

    void setWorkingLang(BdfServer bdfServer, Lang workingLang) {
        this.workingLang = workingLang;
        bdfUserImpl.updateLocalisation(bdfServer);
    }

    void setCustomFormatLocale(BdfServer bdfServer, Locale customFormatLocale) {
        this.customFormatLocale = customFormatLocale;
        bdfUserImpl.updateLocalisation(bdfServer);
    }

    void setCustomLangPreference(BdfServer bdfServer, LangPreference customLangPreference) {
        this.customLangPreference = customLangPreference;
        bdfUserImpl.updateLocalisation(bdfServer);
    }

    void init(BdfUserPrefs bdfUserPrefs) {
        attributesCache.clear();
        attributesCache.putAttributes(bdfUserPrefs.getAttributes());
        this.workingLang = bdfUserPrefs.getWorkingLang();
        this.customFormatLocale = bdfUserPrefs.getCustomFormatLocale();
        this.customLangPreference = bdfUserPrefs.getCustomLangPreference();
        this.defaultFicheQuery = bdfUserPrefs.getDefaultFicheQuery();
    }


    private class BdfUserPrefsEditorImpl implements BdfUserPrefsEditor {

        private final BdfServer bdfServer;

        private BdfUserPrefsEditorImpl(BdfServer bdfServer) {
            this.bdfServer = bdfServer;
        }

        @Override
        public BdfUserPrefsEditor setWorkingLang(Lang workingLang) {
            BdfUserPrefsImpl.this.workingLang = workingLang;
            bdfUserImpl.updateLocalisation(bdfServer);
            return this;
        }

        @Override
        public BdfUserPrefsEditor setCustomFormatLocale(Locale customFormatLocale) {
            BdfUserPrefsImpl.this.customFormatLocale = customFormatLocale;
            bdfUserImpl.updateLocalisation(bdfServer);
            return this;
        }

        @Override
        public BdfUserPrefsEditor setCustomLangPreference(LangPreference customLangPreference) {
            BdfUserPrefsImpl.this.customLangPreference = customLangPreference;
            bdfUserImpl.updateLocalisation(bdfServer);
            return this;
        }

        @Override
        public BdfUserPrefsEditor setDefaultFicheQuery(FicheQuery defaultFicheQuery) {
            BdfUserPrefsImpl.this.defaultFicheQuery = defaultFicheQuery;
            return this;
        }

        @Override
        public BdfUserPrefsEditor removeAttribute(AttributeKey attributeKey) {
            attributesCache.removeAttribute(attributeKey);
            return this;
        }

        @Override
        public BdfUserPrefsEditor putAttribute(Attribute attribute) {
            attributesCache.putAttribute(attribute);
            return this;
        }

    }

}
