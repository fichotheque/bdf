/* BdfServer - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerEditor;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.tools.L10nUtils;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.FichothequeListener;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.LabelUtils;


/**
 * Signale à l'instance de BdfServer à laquelle est rattachée l'instance de
 * FichothequeListener les événements les plus importants sur la base de fiches.
 *
 * @author Vincent Calame
 */
class BdfServerFichothequeListener implements FichothequeListener {

    public final static int CLEAR_DEFAULTTEMPLATES = 16;
    private final BdfServer bdfServer;
    private boolean onMultiChange = false;
    private int multiChangeMask = 0;

    BdfServerFichothequeListener(BdfServer bdfServer) {
        this.bdfServer = bdfServer;
    }

    void buffer() {
        if (!onMultiChange) {
            onMultiChange = true;
            multiChangeMask = 0;
        }
    }

    private void clearDefaultTemplates() {
        if (onMultiChange) {
            multiChangeMask = multiChangeMask | CLEAR_DEFAULTTEMPLATES;
        } else {
            bdfServer.getTransformationManager().clearDistTransformer();
        }
    }

    void flush() {
        if ((multiChangeMask & CLEAR_DEFAULTTEMPLATES) != 0) {
            bdfServer.getTransformationManager().clearDistTransformer();
        }
        onMultiChange = false;
        multiChangeMask = 0;
    }

    @Override
    public void subsetCreated(FichothequeEditor fichothequeEditor, Subset subset) {
        if (subset instanceof Corpus) {
            Corpus corpus = (Corpus) subset;
            initLabel(fichothequeEditor.getCorpusEditor(corpus));
        }
    }

    @Override
    public void subsetRemoved(FichothequeEditor fichothequeEditor, SubsetKey subsetKey, Subset masterSubset) {
        try (EditSession session = bdfServer.initEditSession(fichothequeEditor.getEditOrigin())) {
            BdfServerEditor bdfServerEditor = session.getBdfServerEditor();
            switch (subsetKey.getCategory()) {
                case SubsetKey.CATEGORY_THESAURUS:
                    PolicyManagerImpl.subsetRemoved(bdfServerEditor, subsetKey);
                    clearDefaultTemplates();
                    break;
                case SubsetKey.CATEGORY_SPHERE:
                    bdfServer.getBdfUserStorage().removeSphere(subsetKey);
                    break;
                case SubsetKey.CATEGORY_CORPUS:
                    clearDefaultTemplates();
                    break;
            }
        };
    }

    @Override
    public void subsetItemRemoved(FichothequeEditor fichothequeEditor, Subset subset, int id) {
        try (EditSession session = bdfServer.initEditSession(fichothequeEditor.getEditOrigin())) {
            if (subset instanceof Sphere) {
                Sphere sphere = (Sphere) subset;
                SubsetKey sphereKey = sphere.getSubsetKey();
                String globalId = FichothequeUtils.toGlobalId(sphereKey, id);
                bdfServer.getBdfUserStorage().removeBdfUser(sphere, id);
                bdfServer.getPasswordManager().setPassword(globalId, null);
                ((PermissionManagerImpl) bdfServer.getPermissionManager()).redacteurRemoved(session.getBdfServerEditor(), globalId);
            }
        }
    }

    @Override
    public void corpusFieldCreated(FichothequeEditor fichothequeEditor, Corpus corpus, CorpusField corpusField) {
        switch (corpusField.getFieldString()) {
            case FieldKey.SPECIAL_SOUSTITRE:
                initLabel(fichothequeEditor.getCorpusEditor(corpus), FieldKey.SOUSTITRE);
                break;
        }
    }

    @Override
    public void corpusFieldRemoved(FichothequeEditor fichothequeEditor, Corpus corpus, FieldKey fieldKey) {

    }

    @Override
    public void ficheSaved(FichothequeEditor fichothequeEditor, FicheMeta ficheMeta, FicheAPI fiche) {
    }

    @Override
    public void subsetItemCreated(FichothequeEditor fichothequeEditor, SubsetItem subsetItem) {
    }

    private void initLabel(CorpusEditor corpusEditor) {
        initLabel(corpusEditor, FieldKey.ID);
        initLabel(corpusEditor, FieldKey.LANG);
        initLabel(corpusEditor, FieldKey.TITRE);
        initLabel(corpusEditor, FieldKey.REDACTEURS);
    }

    private void initLabel(CorpusEditor corpusEditor, FieldKey fieldKey) {
        CorpusField corpusField = corpusEditor.getCorpus().getCorpusMetadata().getCorpusField(fieldKey);
        String messageKey = L10nUtils.getMessageKey(fieldKey);
        for (Lang lang : bdfServer.getLangConfiguration().getWorkingLangs()) {
            MessageLocalisation messageLocalisation = bdfServer.getL10nManager().getMessageLocalisation(lang);
            String labelString = messageLocalisation.toString(messageKey);
            if (labelString != null) {
                CleanedString cs = CleanedString.newInstance(labelString);
                if (cs != null) {
                    corpusEditor.getCorpusMetadataEditor().putFieldLabel(corpusField, LabelUtils.toLabel(lang, cs));
                }
            }
        }
    }

}
