/* BdfServer - Copyright (c) 2014-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.groups.GroupDef;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesCache;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.LabelsCache;


/**
 *
 * @author Vincent Calame
 */
class GroupDefImpl implements GroupDef {

    private final String name;
    private final LabelsCache labelsCache = new LabelsCache();
    private final AttributesCache attributesCache = new AttributesCache();

    GroupDefImpl(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Labels getTitleLabels() {
        return labelsCache.getLabels();
    }

    @Override
    public Attributes getAttributes() {
        return attributesCache.getAttributes();
    }

    boolean removeAttribute(AttributeKey attributeKey) {
        return attributesCache.removeAttribute(attributeKey);
    }

    boolean putAttribute(Attribute attribute) {
        return attributesCache.putAttribute(attribute);
    }

    boolean putLabel(Label label) {
        return labelsCache.putLabel(label);
    }

    boolean removeLabel(Lang lang) {
        return labelsCache.removeLabel(lang);
    }

    void init(GroupDef groupDef) {
        labelsCache.clear();
        labelsCache.putLabels(groupDef.getTitleLabels());
        attributesCache.clear();
        attributesCache.putAttributes(groupDef.getAttributes());
    }

}
