/* BdfServer - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.L10nManager;
import fr.exemole.bdfserver.api.managers.TransformationManager;
import fr.exemole.bdfserver.api.managers.UiManager;
import fr.exemole.bdfserver.tools.exportation.transformation.TransformationFormatterFactory;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import java.util.Map;
import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.format.FormatContext;
import net.fichotheque.format.formatters.ExtractionFormatter;
import net.fichotheque.format.formatters.FicheBlockFormatter;
import net.fichotheque.include.LiageTest;
import net.mapeadores.util.instruction.InstructionResolverProvider;
import net.mapeadores.util.localisation.MessageLocalisationProvider;


/**
 *
 * @author Vincent Calame
 */
class FormatContextImpl implements FormatContext {

    private final BdfServer bdfServer;
    private final InstructionResolverProvider instructionResolverProvider;
    private final TransformationManager transformationManager;
    private final L10nManager l10nManager;
    private final UiManager uiManager;

    FormatContextImpl(BdfServer bdfServer, InstructionResolverProvider instructionResolverProvider, TransformationManager transformationManager, L10nManager l10nManager, UiManager uiManager) {
        this.bdfServer = bdfServer;
        this.instructionResolverProvider = instructionResolverProvider;
        this.transformationManager = transformationManager;
        this.l10nManager = l10nManager;
        this.uiManager = uiManager;
    }

    @Override
    public Fichotheque getFichotheque() {
        return bdfServer.getFichotheque();
    }

    @Override
    public InstructionResolverProvider getInstructionResolverProvider() {
        return instructionResolverProvider;
    }

    @Override
    public ExtractionFormatter getExtractionFormatter(String name, Map<String, Object> options) {
        return TransformationFormatterFactory.neExtractionFormatter(transformationManager, name, options);
    }

    @Override
    public FicheBlockFormatter getFicheBlockFormatter(String name, Map<String, Object> options) {
        return TransformationFormatterFactory.newFicheBlockFormatter(transformationManager, name, options);
    }

    @Override
    public LiageTest getLiageTest(Corpus corpus) {
        return UiUtils.checkLiageTest(uiManager.getMainUiComponents(corpus));
    }

    @Override
    public MessageLocalisationProvider getMessageLocalisationProvider() {
        return l10nManager;
    }

    @Override
    public Object getContextObject(String objectName) {
        return bdfServer.getContextObject(objectName);
    }

}
