/* BdfServer - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.managers.L10nManager;
import fr.exemole.bdfserver.tools.BdfMessageLocalisationFactory;
import java.util.Locale;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.localisation.CodeCatalog;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 *
 * @author Vincent Calame
 */
class L10nManagerImpl implements L10nManager {

    private final ResourceStorages resourceStorages;
    private BdfMessageLocalisationFactory messageLocalisationFactory;

    L10nManagerImpl(ResourceStorages resourceStorages) {
        this.resourceStorages = resourceStorages;
        innerUpdateMessageLocalisationFactory();
    }

    @Override
    public MessageLocalisation getMessageLocalisation(LangPreference langPreference, Locale formatLocale) {
        return messageLocalisationFactory.newInstance(langPreference, formatLocale);
    }

    @Override
    public MessageLocalisation getMessageLocalisation(Lang lang) {
        return messageLocalisationFactory.newInstance(lang);
    }

    @Override
    public void update() {
        innerUpdateMessageLocalisationFactory();
    }

    @Override
    public CodeCatalog getCodeCatalog() {
        return messageLocalisationFactory;
    }

    private void innerUpdateMessageLocalisationFactory() {
        this.messageLocalisationFactory = BdfMessageLocalisationFactory.buildFactory(resourceStorages);
    }

}
