/* BdfServer - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.impl;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.UiManager;
import fr.exemole.bdfserver.api.storage.StorageCheck;
import fr.exemole.bdfserver.api.storage.UiStorage;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.ui.components.FieldUiBuilder;
import java.util.HashMap;
import java.util.Map;
import net.fichotheque.Fichotheque;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.FichothequeListener;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.html.TrustedHtmlFactory;


/**
 *
 * @author Vincent Calame
 */
class UiManagerImpl implements UiManager, FichothequeListener {

    private final BdfServer bdfServer;
    private final UiStorage uiStorage;
    private final Map<SubsetKey, UiComponentsImpl> uiComponentsMap = new HashMap<SubsetKey, UiComponentsImpl>();

    private UiManagerImpl(BdfServer bdfServer, UiStorage uiStorage) {
        this.bdfServer = bdfServer;
        this.uiStorage = uiStorage;
    }

    static UiManagerImpl build(BdfServer bdfServer, UiStorage uiStorage, Map<SubsetKey, StorageCheck.UiCheck> uiCheckMap) {
        UiManagerImpl uiManager = new UiManagerImpl(bdfServer, uiStorage);
        Fichotheque fichotheque = bdfServer.getFichotheque();
        for (Corpus corpus : fichotheque.getCorpusList()) {
            UiComponentsImpl uiComponents = new UiComponentsImpl(corpus);
            StorageCheck.UiCheck uiCheck = uiCheckMap.get(corpus.getSubsetKey());
            if (uiCheck != null) {
                uiComponents.populate(fichotheque, corpus.getCorpusMetadata(), uiCheck.getMainUiComponents());
            }
            uiComponents.checkCorpus();
            uiManager.uiComponentsMap.put(corpus.getSubsetKey(), uiComponents);
        }
        return uiManager;
    }

    @Override
    public TrustedHtmlFactory getTrustedHtmlFactory() {
        return uiStorage.getTrustedHtmlFactory();
    }

    @Override
    public UiComponents getMainUiComponents(Corpus corpus) {
        SubsetKey subsetKey = corpus.getSubsetKey();
        UiComponentsImpl uiComponents = uiComponentsMap.get(subsetKey);
        if (uiComponents == null) {
            uiComponents = addCorpus(corpus);
        }
        return uiComponents;
    }

    void saveUiComponents(UiComponentsImpl uiComponents) {
        uiStorage.saveUiComponents(uiComponents.getSubsetKey(), uiComponents);
        bdfServer.getTransformationManager().clearDistTransformer();
    }

    @Override
    public void subsetCreated(FichothequeEditor fichothequeEditor, Subset subset) {
        if (subset instanceof Corpus) {
            Corpus corpus = (Corpus) subset;
            addCorpus(corpus);
            checkMasterSubset(corpus.getMasterSubset());
        }
    }

    @Override
    public void subsetRemoved(FichothequeEditor fichothequeEditor, SubsetKey subsetKey, Subset masterSubset) {
        if (subsetKey.isCorpusSubset()) {
            UiComponentsImpl uiComponents = uiComponentsMap.get(subsetKey);
            if (uiComponents != null) {
                uiComponentsMap.remove(subsetKey);
                uiStorage.removeUiComponents(subsetKey);
                bdfServer.getTransformationManager().clearDistTransformer();
            }
        }
        for (UiComponentsImpl uiComponents : uiComponentsMap.values()) {
            boolean done = uiComponents.removeAllSubsetInclude(subsetKey);
            if (done) {
                saveUiComponents(uiComponents);
            }
        }
    }

    @Override
    public void subsetItemRemoved(FichothequeEditor fichothequeEditor, Subset subset, int id) {
    }

    @Override
    public void corpusFieldCreated(FichothequeEditor fichothequeEditor, Corpus corpus, CorpusField corpusField) {
        FieldKey fieldKey = corpusField.getFieldKey();
        UiComponentsImpl uiComponents = uiComponentsMap.get(corpus.getSubsetKey());
        if (uiComponents != null) {
            uiComponents.add(FieldUiBuilder.init(fieldKey).toFieldUi());
            saveUiComponents(uiComponents);
        }
    }

    @Override
    public void corpusFieldRemoved(FichothequeEditor fichothequeEditor, Corpus corpus, FieldKey fieldKey) {
        UiComponentsImpl uiComponents = uiComponentsMap.get(corpus.getSubsetKey());
        if (uiComponents != null) {
            boolean done = uiComponents.removeComponentUi(fieldKey.getKeyString(), true);
            if (done) {
                saveUiComponents(uiComponents);
            }
        }
    }

    @Override
    public void ficheSaved(FichothequeEditor fichothequeEditor, FicheMeta ficheMeta, FicheAPI fiche) {
    }

    @Override
    public void subsetItemCreated(FichothequeEditor fichothequeEditor, SubsetItem subsetItem) {
    }

    private UiComponentsImpl addCorpus(Corpus corpus) {
        UiComponentsImpl uiComponents = new UiComponentsImpl(corpus);
        uiComponents.checkCorpus();
        uiComponents.checkParentage();
        uiComponents.checkSpecialIncludeUi(FichothequeConstants.LIAGE_NAME);
        uiComponentsMap.put(corpus.getSubsetKey(), uiComponents);
        return uiComponents;
    }

    private void checkMasterSubset(Subset masterSubset) {
        if (masterSubset == null) {
            return;
        }
        if (masterSubset instanceof Corpus) {
            UiComponentsImpl uiComponents = uiComponentsMap.get(masterSubset.getSubsetKey());
            if (uiComponents != null) {
                boolean done = uiComponents.checkParentage();
                if (done) {
                    saveUiComponents(uiComponents);
                }
            }
        }
    }

}
