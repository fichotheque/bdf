/* BdfServer - Copyright (c) 2015-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import java.io.IOException;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class ConfigurationJson {

    private ConfigurationJson() {

    }

    public static void properties(JSONWriter jw, LangConfiguration langConfiguration) throws IOException {
        jw.key("workingLangArray");
        CommonJson.array(jw, langConfiguration.getWorkingLangs());
        jw.key("supplementaryLangArray");
        CommonJson.array(jw, langConfiguration.getSupplementaryLangs());
    }

    public static void properties(JSONWriter jw, PathConfiguration pathConfiguration) throws IOException {
        jw.key("targetArray");
        jw.array();
        for (String targetName : pathConfiguration.getTargetNameSet()) {
            jw.value(targetName);
        }
        jw.endArray();
    }

}
