/* BdfServer - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.exportation.scrutari.CorpusScrutariDef;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.fichotheque.exportation.scrutari.ThesaurusScrutariDef;
import net.fichotheque.format.FormatSourceKey;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.Labels;
import net.scrutari.dataexport.api.BaseMetadataExport;


/**
 *
 * @author Vincent Calame
 */
public final class ScrutariExportJson {

    private ScrutariExportJson() {

    }

    public static void properties(JSONWriter jw, ScrutariExportDef scrutariExportDef) throws IOException {
        AccoladePattern ficheHrefPattern = scrutariExportDef.getFicheHrefPattern();
        String ficheHrefPatternString = "";
        if (ficheHrefPattern != null) {
            ficheHrefPatternString = ficheHrefPattern.toString();
        }
        AccoladePattern motcleHrefPattern = scrutariExportDef.getMotcleHrefPattern();
        String motcleHrefPatternString = "";
        if (motcleHrefPattern != null) {
            motcleHrefPatternString = motcleHrefPattern.toString();
        }
        jw.key("name")
                .value(scrutariExportDef.getName());
        jw.key("labelMap");
        CommonJson.object(jw, scrutariExportDef.getTitleLabels());
        jw.key("attrMap");
        CommonJson.object(jw, scrutariExportDef.getAttributes());
        jw.key("authority")
                .value(scrutariExportDef.getAuthority());
        jw.key("baseName")
                .value(scrutariExportDef.getBaseName());
        jw.key("baseIcon")
                .value(scrutariExportDef.getBaseIcon());
        jw.key("langUiArray");
        jw.array();
        for (Lang lang : scrutariExportDef.getUiLangs()) {
            jw.value(lang.toString());
        }
        jw.endArray();
        jw.key("ficheHrefPattern")
                .value(ficheHrefPatternString);
        jw.key("motcleHrefPattern")
                .value(motcleHrefPatternString);
        jw.key("baseIntituleMap");
        jw.object();
        {
            addLabels(jw, "short", scrutariExportDef.getCustomBaseIntitule(BaseMetadataExport.INTITULE_SHORT));
            addLabels(jw, "long", scrutariExportDef.getCustomBaseIntitule(BaseMetadataExport.INTITULE_LONG));
        }
        jw.endObject();
        jw.key("includeTokenArray");
        jw.array();
        for (String includeToken : scrutariExportDef.getIncludeTokenList()) {
            jw.value(includeToken);
        }
        jw.endArray();
        jw.key("targetName")
                .value(scrutariExportDef.getTargetName());
        jw.key("targetPath")
                .value(scrutariExportDef.getTargetPath().toString());
        SelectionOptionsJson.properties(jw, scrutariExportDef.getSelectionOptions());
        jw.key("corpusScrutariArray");
        jw.array();
        for (CorpusScrutariDef corpusScrutariDef : scrutariExportDef.getCorpusScrutariDefList()) {
            jw.object();
            {
                properties(jw, corpusScrutariDef);
            }
            jw.endObject();
        }
        jw.endArray();
        jw.key("thesaurusScrutariArray");
        jw.array();
        for (ThesaurusScrutariDef thesaurusScrutariDef : scrutariExportDef.getThesaurusScrutariDefList()) {
            jw.object();
            {
                properties(jw, thesaurusScrutariDef);
            }
            jw.endObject();
        }
        jw.endArray();
    }

    private static void addLabels(JSONWriter js, String key, Labels labels) throws IOException {
        if ((labels == null) || (labels.isEmpty())) {
            return;
        }
        js.key(key);
        CommonJson.object(js, labels);
    }

    public static void properties(JSONWriter jw, CorpusScrutariDef corpusScrutariDef) throws IOException {
        FormatSourceKey dateFormatSourceKey = corpusScrutariDef.getDateFormatSourceKey();
        String dateSource = "";
        if (dateFormatSourceKey != null) {
            dateSource = dateFormatSourceKey.getKeyString();
        }
        AccoladePattern hrefPattern = corpusScrutariDef.getHrefPattern();
        String hrefPatternString = "";
        if (hrefPattern != null) {
            hrefPatternString = hrefPattern.toString();
        }
        String multilangMode = corpusScrutariDef.getMultilangMode();
        if (multilangMode == null) {
            multilangMode = "";
        }
        String multilangParam = corpusScrutariDef.getMultilangParam();
        if (multilangParam == null) {
            multilangParam = "";
        }
        jw.key("subsetName")
                .value(corpusScrutariDef.getCorpusKey().getSubsetName());
        jw.key("dateSource")
                .value(dateSource);
        jw.key("fieldGenerationSource")
                .value(corpusScrutariDef.getFieldGenerationSource());
        jw.key("hrefPattern")
                .value(hrefPatternString);
        jw.key("multilangMode")
                .value(multilangMode);
        jw.key("multilangParam")
                .value(multilangParam);
        jw.key("includeTokenArray");
        jw.array();
        for (String includeToken : corpusScrutariDef.getIncludeTokenList()) {
            jw.value(includeToken);
        }
        jw.endArray();
    }

    public static void properties(JSONWriter jw, ThesaurusScrutariDef thesaurusScrutariDef) throws IOException {
        jw.key("subsetName")
                .value(thesaurusScrutariDef.getThesaurusKey().getSubsetName());
        jw.key("fieldGenerationSource")
                .value(thesaurusScrutariDef.getFieldGenerationSource());
        jw.key("wholeThesaurus")
                .value(thesaurusScrutariDef.isWholeThesaurus());
        jw.key("includeTokenArray");
        jw.array();
        for (String includeToken : thesaurusScrutariDef.getIncludeTokenList()) {
            jw.value(includeToken);
        }
        jw.endArray();
    }

}
