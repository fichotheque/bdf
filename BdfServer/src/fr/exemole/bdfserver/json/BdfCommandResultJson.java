/* BdfServer_JsonProducers - Copyright (c) 2016-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import java.io.IOException;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.MessageByLine;
import net.mapeadores.util.logging.MessageByLineLog;


/**
 *
 * @author Vincent Calame
 */
public final class BdfCommandResultJson {

    private BdfCommandResultJson() {

    }

    public static void properties(JSONWriter jw, BdfCommandResult bdfCommandResult, MessageLocalisation messageLocalisation) throws IOException {
        CommandMessage commandMessage = bdfCommandResult.getCommandMessage();
        if (commandMessage != null) {
            jw.key("commandMessage");
            CommonJson.object(jw, commandMessage, messageLocalisation);
        }
        Object messageByLineLogObj = bdfCommandResult.getResultObject(BdfInstructionConstants.MESSAGEBYLINELOG_OBJ);
        if ((messageByLineLogObj != null) && (messageByLineLogObj instanceof MessageByLineLog)) {
            MessageByLineLog messageByLineLog = (MessageByLineLog) messageByLineLogObj;
            jw.key("messageByLineLog");
            jw.object();
            for (MessageByLineLog.LogGroup logGroup : messageByLineLog.getLogGroupList()) {
                jw.key(logGroup.getURI());
                jw.array();
                for (MessageByLine messageByLine : logGroup.getMessageByLineList()) {
                    jw.object();
                    CommonJson.properties(jw, messageByLine, messageLocalisation);
                    jw.endObject();
                }
                jw.endArray();
            }
            jw.endObject();
        }
    }

}
