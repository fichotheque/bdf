/* BdfServer - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.exportation.table.TableExportContentDescription;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.exportation.table.TableExportDescription;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.MessageByLine;


/**
 *
 * @author Vincent Calame
 */
public final class TableExportJson {

    private TableExportJson() {

    }

    public static void properties(JSONWriter jw, TableExportDescription tableExportDescription, MessageLocalisation messageLocalisation) throws IOException {
        TableExportDef tableExportDef = tableExportDescription.getTableExportDef();
        String langMode = tableExportDef.getLangMode();
        if (langMode == null) {
            langMode = "";
        }
        jw.key("name")
                .value(tableExportDescription.getName());
        jw.key("state")
                .value(tableExportDescription.getState());
        jw.key("editable")
                .value(tableExportDescription.isEditable());
        jw.key("labelMap");
        CommonJson.object(jw, tableExportDef.getTitleLabels());
        jw.key("langMode")
                .value(langMode);
        jw.key("langArray");
        jw.array();
        for (Lang lang : tableExportDef.getLangs()) {
            jw.value(lang.toString());
        }
        jw.endArray();
        jw.key("attrMap");
        CommonJson.object(jw, tableExportDef.getAttributes());
        jw.key("contentArray");
        jw.array();
        for (TableExportContentDescription tecd : tableExportDescription.getTableExportContentDescriptionList()) {
            jw.object();
            {
                jw.key("path")
                        .value(tecd.getPath());
                stateAndMessagesProperties(jw, tecd, messageLocalisation);
            }
            jw.endObject();
        }
        jw.endArray();
    }

    public static void stateAndMessagesProperties(JSONWriter jw, TableExportContentDescription tableExportContentDescription, MessageLocalisation messageLocalisation) throws IOException {
        jw.key("editable")
                .value(tableExportContentDescription.isEditable());
        jw.key("state")
                .value(tableExportContentDescription.getState());
        jw.key("messageByLineArray");
        jw.array();
        for (MessageByLine messageByLine : tableExportContentDescription.getMessageByLineList()) {
            jw.object();
            CommonJson.properties(jw, messageByLine, messageLocalisation);
            jw.endObject();
        }
        jw.endArray();
    }


}
