/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import java.util.List;
import net.fichotheque.selection.CroisementCondition;
import net.fichotheque.selection.RangeCondition;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class CroisementJson {

    private CroisementJson() {

    }

    public static void properties(JSONWriter jw, CroisementCondition croisementCondition) throws IOException {
        List<String> modeList = croisementCondition.getLienModeList();
        if (!modeList.isEmpty()) {
            jw.key("modes");
            jw.array();
            for (String mode : modeList) {
                jw.value(mode);
            }
            jw.endArray();
        }
        RangeCondition weightRangeCondition = croisementCondition.getWeightRangeCondition();
        if (weightRangeCondition != null) {
            jw.key("weight");
            jw.object();
            ConditionJson.properties(jw, weightRangeCondition);
            jw.endObject();
        }
    }

}
