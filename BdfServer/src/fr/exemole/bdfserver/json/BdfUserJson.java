/* BdfServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import fr.exemole.bdfserver.api.users.BdfUser;
import java.io.IOException;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class BdfUserJson {

    private BdfUserJson() {

    }

    public static void properties(JSONWriter jw, BdfUser bdfUser) throws IOException {
        Redacteur redacteur = bdfUser.getRedacteur();
        jw.key("sphere")
                .value(redacteur.getSubsetName());
        jw.key("login")
                .value(redacteur.getLogin());
        jw.key("name")
                .value(redacteur.getCompleteName());
        jw.key("lang")
                .value(bdfUser.getWorkingLang());
        jw.key("locale")
                .value(bdfUser.getISOFormatLocaleString());
        jw.key("status")
                .value(redacteur.getStatus());
    }

    public static void populate(JsObject jsObject, BdfUser bdfUser) {
        Redacteur redacteur = bdfUser.getRedacteur();
        jsObject
                .put("sphere", redacteur.getSubsetName())
                .put("login", redacteur.getLogin())
                .put("name", redacteur.getCompleteName())
                .put("lang", bdfUser.getWorkingLang())
                .put("locale", bdfUser.getISOFormatLocaleString())
                .put("status", redacteur.getStatus());
    }

}
