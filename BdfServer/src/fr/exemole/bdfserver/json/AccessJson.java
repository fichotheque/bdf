/* BdfServer - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.exportation.access.AccessDef;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.json.CellJson;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class AccessJson {

    private AccessJson() {

    }

    public static void properties(JSONWriter jw, AccessDef accessDef) throws IOException {
        jw.key("name")
                .value(accessDef.getName());
        jw.key("tableExportName")
                .value(accessDef.getTableExportName());
        jw.key("public")
                .value(accessDef.isPublic());
        jw.key("labelMap");
        CommonJson.object(jw, accessDef.getTitleLabels());
        jw.key("attrMap");
        CommonJson.object(jw, accessDef.getAttributes());
        SelectionOptionsJson.properties(jw, accessDef.getSelectionOptions());
    }

    public static void properties(JSONWriter jw, FicheMeta ficheMeta) throws IOException {
        jw.key("corpus")
                .value(ficheMeta.getSubsetName());
        jw.key("id")
                .value(ficheMeta.getId());
        jw.key("title")
                .value(ficheMeta.getTitre());
        Lang lang = ficheMeta.getLang();
        if (lang != null) {
            jw.key("lang")
                    .value(lang.toString());
        }
    }

    public static void properties(JSONWriter jw, FicheMeta ficheMeta, PermissionSummary permissionSummary) throws IOException {
        properties(jw, ficheMeta);
        jw.key("editable")
                .value(permissionSummary.canWrite(ficheMeta));
    }

    public static void properties(JSONWriter jw, FicheMeta ficheMeta, PermissionSummary permissionSummary, CellConverter cellConverter) throws IOException {
        properties(jw, ficheMeta, permissionSummary);
        Cell[] cellArray = cellConverter.toCellArray(ficheMeta);
        CellJson.cellArrayMappingProperty(jw, cellArray);
    }

    public static void properties(JSONWriter jw, Motcle motcle, Lang lang) throws IOException {
        String idalpha = motcle.getIdalpha();
        jw.key("thesaurus")
                .value(motcle.getSubsetName());
        jw.key("id")
                .value(motcle.getId());
        if (idalpha != null) {
            jw.key("idalpha")
                    .value(idalpha);
        }
        jw.key("title")
                .value(motcle.getLabelString(lang, ""));
        jw.key("status")
                .value(motcle.getStatus());
    }

    public static void properties(JSONWriter jw, Motcle motcle, CellConverter cellConverter, Lang lang) throws IOException {
        properties(jw, motcle, lang);
        Cell[] cellArray = cellConverter.toCellArray(motcle);
        CellJson.cellArrayMappingProperty(jw, cellArray);
    }

    public static void properties(JSONWriter jw, Fiches fiches, PermissionSummary permissionSummary, CellConverter cellConverter, Lang lang) throws IOException {
        jw.key("count")
                .value(fiches.getFicheCount());
        jw.key("entries");
        jw.array();
        for (Fiches.Entry entry : fiches.getEntryList()) {
            Corpus corpus = entry.getCorpus();
            jw.object();
            {
                jw.key("name")
                        .value(corpus.getSubsetName());
                jw.key("title")
                        .value(FichothequeUtils.getTitle(corpus, lang));
                jw.key("array");
                jw.array();
                for (FicheMeta ficheMeta : entry.getFicheMetaList()) {
                    jw.object();
                    AccessJson.properties(jw, ficheMeta, permissionSummary, cellConverter);
                    jw.endObject();
                }
                jw.endArray();
            }
            jw.endObject();
        }
        jw.endArray();
    }


}
