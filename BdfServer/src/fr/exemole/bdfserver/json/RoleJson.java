/* BdfServer - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import fr.exemole.bdfserver.api.roles.FichePermission;
import fr.exemole.bdfserver.api.roles.Permission;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.api.roles.RoleDef;
import fr.exemole.bdfserver.tools.roles.RoleUtils;
import java.io.IOException;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class RoleJson {

    private RoleJson() {

    }

    public static void properties(JSONWriter jw, Role role) throws IOException {
        jw.key("name")
                .value(role.getName());
        jw.key("labelMap");
        CommonJson.object(jw, role.getTitleLabels());
        jw.key("attrMap");
        CommonJson.object(jw, role.getAttributes());
        jw.key("permissionMap");
        jw.object();
        for (RoleDef.SubsetEntry entry : role.getSubsetEntryList()) {
            Permission permission = entry.getPermission();
            short level = permission.getLevel();
            SubsetKey subsetKey = entry.getSubsetKey();
            jw.key(subsetKey.getKeyString());
            jw.object();
            {
                jw.key("level")
                        .value(RoleUtils.levelToString(level));
                if ((level == Permission.CUSTOM_LEVEL) && (subsetKey.isCorpusSubset())) {
                    FichePermission fichePermission = (FichePermission) permission.getCustomPermission();
                    jw.key("create")
                            .value(fichePermission.create());
                    jw.key("read")
                            .value(RoleUtils.fichePermissionTypeToString(fichePermission.read()));
                    jw.key("write")
                            .value(RoleUtils.fichePermissionTypeToString(fichePermission.write()));
                }
            }
            jw.endObject();
        }
        jw.endObject();
    }

    public static void roleArray(JSONWriter jw, List<Role> roleList, Lang lang) throws IOException {
        jw.key("roleArray");
        jw.array();
        for (Role role : roleList) {
            jw.object();
            {
                jw.key("name")
                        .value(role.getName());
                jw.key("title");
                jw.value(role.getTitleLabels().seekLabelString(lang, ""));
                jw.key("attrMap");
                CommonJson.object(jw, role.getAttributes());
            }
            jw.endObject();
        }
        jw.endArray();
    }

}
