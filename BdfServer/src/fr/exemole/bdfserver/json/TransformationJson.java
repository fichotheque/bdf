/* BdfServer - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationDescription;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.tools.exportation.transformation.TransformationCheck;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.MessageByLine;


/**
 *
 * @author Vincent Calame
 */
public final class TransformationJson {

    private TransformationJson() {

    }

    public static void properties(JSONWriter jw, TransformationDescription transformationDescription, MessageLocalisation messageLocalisation, Lang lang) throws IOException {
        TransformationCheck transformationCheck = TransformationCheck.check(transformationDescription, false);
        TransformationKey transformationKey = transformationDescription.getTransformationKey();
        jw.key("key")
                .value(transformationKey.getKeyString());
        jw.key("simpleArray");
        jw.array();
        if (transformationCheck.withSimpleTemplate()) {
            for (TemplateDescription templateDescription : transformationCheck.getSimpleTemplateDescriptionArray()) {
                jw.object();
                {
                    properties(jw, templateDescription, messageLocalisation);
                    CommonJson.title(jw, templateDescription.getTemplateDef().getTitleLabels(), lang);
                }
                jw.endObject();
            }
        }
        jw.endArray();
        jw.key("extensionArray");
        jw.array();
        for (String extension : transformationCheck.getExtensionArray()) {
            TemplateDescription[] array = transformationCheck.getStreamTemplateDescriptionArray(extension);
            jw.object();
            {
                jw.key("extension")
                        .value(extension);
                jw.key("templateArray");
                jw.array();
                for (TemplateDescription templateDescription : array) {
                    jw.object();
                    {
                        properties(jw, templateDescription, messageLocalisation);
                        CommonJson.title(jw, templateDescription.getTemplateDef().getTitleLabels(), lang);
                    }
                    jw.endObject();
                }
                jw.endArray();
            }
            jw.endObject();
        }
        jw.endArray();
    }

    public static void object(JSONWriter jw, TemplateDescription templateDescription, MessageLocalisation messageLocalisation) throws IOException {
        jw.object();
        properties(jw, templateDescription, messageLocalisation);
        jw.endObject();
    }

    public static void properties(JSONWriter jw, TemplateDescription templateDescription, MessageLocalisation messageLocalisation) throws IOException {
        TemplateKey templateKey = templateDescription.getTemplateKey();
        TemplateDef templateDef = templateDescription.getTemplateDef();
        jw.key("key")
                .value(templateKey.getKeyString());
        jw.key("transformationKey")
                .value(templateKey.getTransformationKey().getKeyString());
        if (templateKey.isStreamTemplate()) {
            jw.key("extension")
                    .value(templateKey.getExtension());
        }
        jw.key("name")
                .value(templateKey.getName());
        jw.key("type")
                .value(templateDescription.getType());
        jw.key("state")
                .value(templateDescription.getState());
        jw.key("labelMap");
        CommonJson.object(jw, templateDef.getTitleLabels());
        jw.key("errorArray");
        jw.array();
        for (Message errorMessage : templateDescription.getErrorMessageList()) {
            CommonJson.object(jw, errorMessage, messageLocalisation);
        }
        jw.endArray();
        jw.key("warningArray");
        jw.array();
        for (Message warningMessage : templateDescription.getWarningMessageList()) {
            CommonJson.object(jw, warningMessage, messageLocalisation);
        }
        jw.endArray();
        jw.key("contentArray");
        jw.array();
        for (TemplateContentDescription templateContentDescription : templateDescription.getTemplateContentDescriptionList()) {
            jw.object();
            {
                jw.key("path")
                        .value(templateContentDescription.getPath());
                jw.key("type")
                        .value(templateContentDescription.getType());
                jw.key("mandatory")
                        .value(templateContentDescription.isMandatory());
                stateAndMessagesProperties(jw, templateContentDescription, messageLocalisation);
            }
            jw.endObject();
        }
        jw.endArray();
        jw.key("attrMap");
        CommonJson.object(jw, templateDef.getAttributes());
    }

    public static void stateAndMessagesProperties(JSONWriter jw, TemplateContentDescription templateContentDescription, MessageLocalisation messageLocalisation) throws IOException {
        jw.key("state")
                .value(templateContentDescription.getState());
        jw.key("messageByLineArray");
        jw.array();
        for (MessageByLine messageByLine : templateContentDescription.getMessageByLineList()) {
            jw.object();
            CommonJson.properties(jw, messageByLine, messageLocalisation);
            jw.endObject();
        }
        jw.endArray();
    }

}
