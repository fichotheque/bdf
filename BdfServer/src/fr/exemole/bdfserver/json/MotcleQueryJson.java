/* BdfServer - Copyright (c) 2016-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import java.util.List;
import net.fichotheque.Fichotheque;
import net.fichotheque.selection.CroisementCondition;
import net.fichotheque.selection.FicheCondition;
import net.fichotheque.selection.MotcleCondition;
import net.fichotheque.selection.MotcleQuery;
import net.fichotheque.selection.RangeCondition;
import net.fichotheque.selection.StatusCondition;
import net.fichotheque.selection.SubsetCondition;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.conditions.ConditionsUtils;
import net.mapeadores.util.conditions.TextCondition;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class MotcleQueryJson {

    private MotcleQueryJson() {

    }

    public static void properties(JSONWriter jw, Fichotheque fichotheque, MotcleQuery motcleQuery) throws IOException {
        properties(jw, fichotheque, motcleQuery, null, false);
    }

    public static void properties(JSONWriter jw, Fichotheque fichotheque, MotcleCondition.Entry motcleConditionEntry) throws IOException {
        properties(jw, fichotheque, motcleConditionEntry.getMotcleQuery(), motcleConditionEntry.getCroisementCondition(), motcleConditionEntry.isWithMaster());
    }

    private static void properties(JSONWriter jw, Fichotheque fichotheque, MotcleQuery motcleQuery, @Nullable CroisementCondition croisementCondition, boolean withMaster) throws IOException {
        SubsetCondition thesaurusCondition = motcleQuery.getThesaurusCondition();
        jw.key("thesaurus");
        jw.object();
        ConditionJson.properties(jw, thesaurusCondition);
        jw.endObject();
        RangeCondition idRangeCondition = motcleQuery.getIdRangeCondition();
        if (idRangeCondition != null) {
            jw.key("range");
            jw.object();
            ConditionJson.properties(jw, idRangeCondition);
            jw.endObject();
        }
        RangeCondition levelRangeCondition = motcleQuery.getLevelRangeCondition();
        if (levelRangeCondition != null) {
            jw.key("level");
            jw.object();
            ConditionJson.properties(jw, levelRangeCondition);
            jw.endObject();
        }
        MotcleQuery.ContentCondition contentCondition = motcleQuery.getContentCondition();
        if (contentCondition != null) {
            TextCondition textCondition = contentCondition.getTextCondition();
            jw.key("content");
            jw.object();
            {
                jw.key("operator")
                        .value(textCondition.getLogicalOperator());
                jw.key("scope")
                        .value(contentCondition.getScope());
                jw.key("q")
                        .value(ConditionsUtils.conditionToString(textCondition));
            }
            jw.endObject();
        }
        FicheCondition ficheCondition = motcleQuery.getFicheCondition();
        if (ficheCondition != null) {
            jw.key("fiche");
            jw.object();
            ConditionJson.properties(jw, ficheCondition, fichotheque);
            jw.endObject();
        }
        StatusCondition statusCondition = motcleQuery.getStatusCondition();
        if (statusCondition != null) {
            jw.key("status");
            jw.array();
            for (String status : statusCondition.getStatusSet()) {
                jw.value(status);
            }
            jw.endArray();
        }
        if (withMaster) {
            jw.key("master")
                    .value(true);
        }
        if (croisementCondition != null) {
            jw.key("croisement");
            jw.object();
            CroisementJson.properties(jw, croisementCondition);
            jw.endObject();
        }
    }

    public static String toXmlString(List<MotcleQuery> motcleQueryList) {
        StringBuilder buf = new StringBuilder();
        if (!motcleQueryList.isEmpty()) {
            XMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            for (MotcleQuery motcleQuery : motcleQueryList) {
                try {
                    xmlWriter.openTag("motcle-query");
                    FichothequeXMLUtils.writeMotcleQuery(xmlWriter, motcleQuery);
                    xmlWriter.closeTag("motcle-query");
                } catch (IOException ioe) {
                }
            }
        }
        return buf.toString();
    }

}
