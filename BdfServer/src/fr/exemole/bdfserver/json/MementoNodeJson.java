/* BdfServer - Copyright (c) 2014-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import fr.exemole.bdfserver.api.memento.MementoNode;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class MementoNodeJson {

    private MementoNodeJson() {

    }

    public static void object(JSONWriter jw, MementoNode MementoNode) throws IOException {
        jw.object();
        properties(jw, MementoNode);
        jw.endObject();
    }

    public static void properties(JSONWriter jw, MementoNode mementoNode) throws IOException {
        boolean isLeaf = mementoNode.isLeaf();
        jw.key("name")
                .value(mementoNode.getName());
        jw.key("title")
                .value(mementoNode.getTitle());
        jw.key("leaf")
                .value(isLeaf);
        if (!isLeaf) {
            jw.key("subnodeArray");
            jw.array();
            for (MementoNode subnode : mementoNode.getSubnodeList()) {
                object(jw, subnode);
            }
            jw.endArray();
        }
        jw.key("text")
                .value(mementoNode.getText());
    }

}
