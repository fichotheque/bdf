/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusFieldJson {

    private CorpusFieldJson() {

    }

    public static void properties(JSONWriter jw, CorpusField corpusField, Lang lang) throws IOException {
        jw.key("key")
                .value(corpusField.getFieldString());
        jw.key("patternType")
                .value(getPatternType(corpusField));
        jw.key("title")
                .value(CorpusMetadataUtils.getFieldTitle(corpusField, lang));
    }

    private static String getPatternType(CorpusField corpusField) {
        switch (corpusField.getCategory()) {
            case FieldKey.SECTION_CATEGORY:
                return "section";
            case FieldKey.SPECIAL_CATEGORY:
                switch (corpusField.getFieldString()) {
                    case FieldKey.SPECIAL_TITRE:
                    case FieldKey.SPECIAL_ID:
                        return "value";
                }
            default:
                return corpusField.getFicheItemTypeString();
        }
    }

}
