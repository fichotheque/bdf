/* BdfServer - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class SqlExportJson {

    private SqlExportJson() {

    }

    public static void properties(JSONWriter jw, SqlExportDef sqlExportDef) throws IOException {
        jw.key("name")
                .value(sqlExportDef.getName());
        jw.key("labelMap");
        CommonJson.object(jw, sqlExportDef.getTitleLabels());
        jw.key("attrMap");
        CommonJson.object(jw, sqlExportDef.getAttributes());
        jw.key("sqlExportClassName")
                .value(sqlExportDef.getSqlExportClassName());
        jw.key("tableExportName")
                .value(sqlExportDef.getTableExportName());
        jw.key("targetName")
                .value(sqlExportDef.getTargetName());
        jw.key("targetPath")
                .value(sqlExportDef.getTargetPath().toString());
        jw.key("fileName")
                .value(sqlExportDef.getFileName());
        jw.key("postCommand")
                .value(sqlExportDef.getPostCommand());
        jw.key("paramArray");
        jw.array();
        for (SqlExportDef.Param param : sqlExportDef.getParamList()) {
            jw.object();
            {
                jw.key("name")
                        .value(param.getName());
                jw.key("value")
                        .value(param.getValue());
            }
            jw.endObject();
        }
        jw.endArray();
        SelectionOptionsJson.properties(jw, sqlExportDef.getSelectionOptions());
    }

}
