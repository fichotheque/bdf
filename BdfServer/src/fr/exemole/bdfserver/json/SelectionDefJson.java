/* BdfServer - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.selection.SelectionDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class SelectionDefJson {

    private SelectionDefJson() {

    }

    public static void properties(JSONWriter jw, SelectionDef selectionDef) throws IOException {
        FichothequeQueries fichothequeQueries = selectionDef.getFichothequeQueries();
        jw.key("name")
                .value(selectionDef.getName());
        jw.key("labelMap");
        CommonJson.object(jw, selectionDef.getTitleLabels());
        jw.key("attrMap");
        CommonJson.object(jw, selectionDef.getAttributes());
        jw.key("query");
        jw.object();
        {
            jw.key("ficheXml")
                    .value(FicheQueryJson.toXmlString(fichothequeQueries.getFicheQueryList()));
            jw.key("motcleXml")
                    .value(MotcleQueryJson.toXmlString(fichothequeQueries.getMotcleQueryList()));
        }
        jw.endObject();
    }

}
