/* BdfServer - Copyright (c) 2014-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.File;
import java.io.IOException;
import net.fichotheque.importation.ParseResult;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class ImportationJson {

    private ImportationJson() {

    }

    public static void properties(JSONWriter jw, ParseResult parseResult) throws IOException {
        jw.key("type")
                .value(parseResult.getType());
        File tmpFile = parseResult.getTmpFile();
        if (tmpFile != null) {
            jw.key("tmpFile")
                    .value(tmpFile.getName());
        }
        File tmpDir = parseResult.getTmpDir();
        if (tmpDir != null) {
            jw.key("tmpDir")
                    .value(tmpDir.getName());
        }
        int initErrorCount = parseResult.getInitErrorCount();
        if (initErrorCount > 0) {
            jw.key("initErrorArray");
            jw.array();
            for (int i = 0; i < initErrorCount; i++) {
                ParseResult.InitError initError = parseResult.getInitError(i);
                jw.object();
                {
                    jw.key("key")
                            .value(initError.getKey());
                    jw.key("text")
                            .value(initError.getText());
                }
                jw.endObject();
            }
            jw.endArray();
        }
        int initWarningCount = parseResult.getInitWarningCount();
        if (initWarningCount > 0) {
            jw.key("initWarningArray");
            jw.array();
            for (int i = 0; i < initWarningCount; i++) {
                ParseResult.InitWarning initWarning = parseResult.getInitWarning(i);
                jw.object();
                {
                    jw.key("key")
                            .value(initWarning.getKey());
                    jw.key("text")
                            .value(initWarning.getText());
                }
                jw.endObject();
            }
            jw.endArray();
        }
        int parseErrorCount = parseResult.getParseErrorCount();
        if (parseErrorCount > 0) {
            jw.key("parseErrorArray");
            jw.array();
            for (int i = 0; i < parseErrorCount; i++) {
                ParseResult.ParseError parseError = parseResult.getParseError(i);
                jw.object();
                {
                    jw.key("line")
                            .value(parseError.getLineNumber());
                    jw.key("rawtext")
                            .value(parseError.getRawText());
                }
                jw.endObject();
            }
            jw.endArray();
        }
        int bdfErrorCount = parseResult.getBdfErrorCount();
        if (bdfErrorCount > 0) {
            jw.key("bdfErrorArray");
            jw.array();
            for (int i = 0; i < bdfErrorCount; i++) {
                ParseResult.BdfError bdfError = parseResult.getBdfError(i);
                jw.object();
                {
                    jw.key("line")
                            .value(bdfError.getLineNumber());
                    jw.key("key")
                            .value(bdfError.getKey());
                    jw.key("text")
                            .value(bdfError.getText());
                }
                jw.endObject();
            }
            jw.endArray();
        }
    }

}
