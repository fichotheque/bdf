/* BdfServer - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import java.util.Set;
import net.fichotheque.EditOrigin;
import net.fichotheque.Fichotheque;
import net.fichotheque.history.HistoryUnit;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class HistoryJson {

    private HistoryJson() {

    }

    public static void properties(JSONWriter jw, HistoryUnit.Revision revision, @Nullable Set<String> globalIdSet) throws IOException {
        jw.key("name");
        jw.value(revision.getName());
        jw.key("editArray");
        jw.array();
        for (EditOrigin editOrigin : revision.getEditOriginList()) {
            jw.object();
            String globalId = editOrigin.getRedacteurGlobalId();
            if (globalId != null) {
                jw.key("user");
                jw.value(globalId);
                if (globalIdSet != null) {
                    globalIdSet.add(globalId);
                }
            }
            jw.key("time");
            jw.value(editOrigin.getISOTime());
            jw.key("source");
            jw.value(editOrigin.getSource());
            jw.endObject();
        }
        jw.endArray();
    }

    public static void userMapProperty(JSONWriter jw, Fichotheque fichotheque, Set<String> globalIdSet) throws IOException {
        jw.key("userMap");
        jw.object();
        for (String globalId : globalIdSet) {
            Redacteur redacteur = SphereUtils.getRedacteur(fichotheque, globalId);
            if (redacteur != null) {
                jw.key(globalId);
                jw.object();
                jw.key("fullname");
                jw.value(redacteur.getCompleteName());
                jw.key("login");
                jw.value(redacteur.getLogin());
                jw.key("sphere");
                jw.value(redacteur.getSubsetName());
                jw.endObject();
            }
        }
        jw.endObject();
    }

}
