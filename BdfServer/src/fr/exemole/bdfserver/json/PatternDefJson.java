/* BdfServer - Copyright (c) 2018-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.TransformationManager;
import java.io.IOException;
import java.util.List;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TransformationDescription;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.format.PatternDef;
import net.fichotheque.utils.TransformationUtils;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class PatternDefJson {

    private final static String[] IDALPHA_ARRAY = {"significant"};
    private final static String[] POSITION_ARRAY = {"global"};

    private PatternDefJson() {

    }

    public static void properties(JSONWriter jw, PatternDef patternDef, BdfServer bdfServer) throws IOException {
        jw.key("name")
                .value(patternDef.getName());
        String directValueType = patternDef.getDirectValueType();
        if (directValueType != null) {
            jw.key("directValueType")
                    .value(directValueType);
            switch (directValueType) {
                case PatternDef.IDALPHA_DIRECTVALUE:
                    directValueArray(jw, IDALPHA_ARRAY);
                    break;
                case PatternDef.TRANSFORMATION_FORMAT_DIRECTVALUE:
                    directValueArray(jw, TransformationKey.FORMAT_INSTANCE, bdfServer);
                    break;
                case PatternDef.TRANSFORMATION_SECTION_DIRECTVALUE:
                    directValueArray(jw, TransformationKey.SECTION_INSTANCE, bdfServer);
                    break;
                case PatternDef.POSITION_DIRECTVALUE:
                    directValueArray(jw, POSITION_ARRAY);
                    break;
            }
        }
        List<PatternDef.ParameterDef> parameterDefList = patternDef.getParameterDefList();
        jw.key("parameterArray");
        jw.array();
        for (PatternDef.ParameterDef parameterDef : parameterDefList) {
            jw.object();
            {
                jw.key("name")
                        .value(parameterDef.getName());
                List<String> availableList = parameterDef.getAvailableValueList();
                jw.key("availableValueArray");
                jw.array();
                for (String availableValue : availableList) {
                    jw.value(availableValue);
                }
                jw.endArray();
            }
            jw.endObject();
        }
        jw.endArray();
    }

    private static void directValueArray(JSONWriter jw, TransformationKey transformationKey, BdfServer bdfServer) throws IOException {
        TransformationManager transformationManager = bdfServer.getTransformationManager();
        TransformationDescription transformationDescription = transformationManager.getTransformationDescription(transformationKey);
        jw.key("directValueArray");
        jw.array();
        for (TemplateDescription templateDescription : transformationDescription.getSimpleTemplateDescriptionList()) {
            if (TransformationUtils.isUseableTemplateState(templateDescription.getState())) {
                jw.value(templateDescription.getTemplateKey().getName());
            }
        }
        jw.endArray();
    }

    private static void directValueArray(JSONWriter jw, String[] array) throws IOException {
        jw.key("directValueArray");
        jw.array();
        for (String value : array) {
            jw.value(value);
        }
        jw.endArray();
    }

}
