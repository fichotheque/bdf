/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.selection.FicheCondition;
import net.fichotheque.selection.MotcleCondition;
import net.fichotheque.selection.PeriodCondition;
import net.fichotheque.selection.RangeCondition;
import net.fichotheque.selection.SubsetCondition;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.primitives.Range;
import net.mapeadores.util.primitives.RangeUtils;


/**
 *
 * @author Vincent Calame
 */
public final class ConditionJson {

    private ConditionJson() {

    }

    public static void properties(JSONWriter jw, SubsetCondition subsetCondition) throws IOException {
        jw.key("all")
                .value(subsetCondition.isAll());
        jw.key("exclude")
                .value(subsetCondition.isExclude());
        jw.key("current")
                .value(subsetCondition.isWithCurrent());
        jw.key("names");
        jw.array();
        for (SubsetKey subsetKey : subsetCondition.getSubsetKeySet()) {
            jw.value(subsetKey.getSubsetName());
        }
        jw.endArray();
    }

    public static void properties(JSONWriter jw, PeriodCondition periodCondition) throws IOException {
        jw.key("start")
                .value(periodCondition.getStartString());
        jw.key("end")
                .value(periodCondition.getEndString());
        StringBuilder buf = new StringBuilder();
        if (periodCondition.isOnCreationDate()) {
            buf.append("creation");
        }
        if (periodCondition.isOnModificationDate()) {
            if (buf.length() > 0) {
                buf.append(" ");
            }
            buf.append("modification");
        }
        for (FieldKey fieldKey : periodCondition.getFieldKeyList()) {
            if (buf.length() > 0) {
                buf.append(" ");
            }
            buf.append(fieldKey.getKeyString());
        }
        if (buf.length() > 0) {
            jw.key("scope")
                    .value(buf.toString());
        }
    }

    public static void properties(JSONWriter jw, RangeCondition rangeCondition) throws IOException {
        jw.key("exclude")
                .value(rangeCondition.isExclude());
        jw.key("values");
        jw.array();
        for (Range range : rangeCondition.getRanges()) {
            jw.value(RangeUtils.positiveRangetoString(range));
        }
        jw.endArray();
    }

    public static void properties(JSONWriter jw, FicheCondition ficheCondition, Fichotheque fichotheque) throws IOException {
        jw.key("operator")
                .value(ficheCondition.getLogicalOperator());
        jw.key("queries");
        jw.array();
        for (FicheCondition.Entry entry : ficheCondition.getEntryList()) {
            jw.object();
            FicheQueryJson.properties(jw, fichotheque, entry);
            jw.endObject();
        }
        jw.endArray();
    }

    public static void properties(JSONWriter jw, MotcleCondition motcleCondition, Fichotheque fichotheque) throws IOException {
        jw.key("operator")
                .value(motcleCondition.getLogicalOperator());
        jw.key("queries");
        jw.array();
        for (MotcleCondition.Entry entry : motcleCondition.getEntryList()) {
            jw.object();
            MotcleQueryJson.properties(jw, fichotheque, entry);
            jw.endObject();
        }
        jw.endArray();
    }

}
