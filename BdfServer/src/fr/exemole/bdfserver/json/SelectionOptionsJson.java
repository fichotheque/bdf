/* BdfServer - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.json;

import java.io.IOException;
import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.selection.SelectionOptions;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class SelectionOptionsJson {

    private SelectionOptionsJson() {

    }

    public static void properties(JSONWriter jw, SelectionOptions selectionOptions) throws IOException {
        FichothequeQueries customFichothequeQueries = selectionOptions.getCustomFichothequeQueries();
        jw.key("selectionDefName")
                .value(selectionOptions.getSelectionDefName());
        jw.key("query");
        jw.object();
        {
            jw.key("ficheXml")
                    .value(FicheQueryJson.toXmlString(customFichothequeQueries.getFicheQueryList()));
            jw.key("motcleXml")
                    .value(MotcleQueryJson.toXmlString(customFichothequeQueries.getMotcleQueryList()));
        }
        jw.endObject();
    }

}
