/* BdfServer - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.sync;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.tools.importation.engines.ThesaurusImportEngine;
import java.util.HashSet;
import java.util.Set;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.ParentRecursivityException;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.thesaurus.sync.MotcleSync;
import net.fichotheque.thesaurus.sync.ThesaurusSync;
import net.fichotheque.tools.thesaurus.sync.SyncScan;
import net.fichotheque.tools.thesaurus.sync.SyncScanResult;
import net.fichotheque.tools.thesaurus.sync.ThesaurusSyncResult;


/**
 *
 * @author Vincent Calame
 */
public final class SyncEngine {

    private SyncEngine() {

    }

    public static ThesaurusSyncResult runThesaurusSync(EditSession editSession, BdfParameters bdfParameters, ThesaurusSync thesaurusSync, Thesaurus thesaurus) {
        ThesaurusSyncResult thesaurusSyncResult = ThesaurusSyncResult.init();
        SyncScanResult syncResult = SyncScan.scanThesaurusImport(thesaurusSync, thesaurus);
        ThesaurusImport changeImport = syncResult.getChangeImport();
        if (changeImport != null) {
            ThesaurusImportEngine.runThesaurusImport(editSession, bdfParameters, changeImport);
            thesaurusSyncResult.setChangeCount(changeImport.getMotcleImportList().size());
        }
        ThesaurusImport creationImport = syncResult.getCreationImport();
        if (creationImport != null) {
            ThesaurusImportEngine.runThesaurusImport(editSession, bdfParameters, creationImport);
            thesaurusSyncResult.setCreationCount(creationImport.getMotcleImportList().size());
        }
        OrderEdit orderEdit = new OrderEdit(thesaurusSync, editSession.getFichothequeEditor().getThesaurusEditor(thesaurus));
        int orderCount = orderEdit.run();
        thesaurusSyncResult.setOrderCount(orderCount);
        return thesaurusSyncResult;
    }


    private static class OrderEdit {

        private final ThesaurusSync originSync;
        private final ThesaurusEditor destinationEditor;
        private final Thesaurus destination;
        private final Set<Integer> changeSet = new HashSet<Integer>();

        private OrderEdit(ThesaurusSync originSync, ThesaurusEditor destinationEditor) {
            this.originSync = originSync;
            this.destinationEditor = destinationEditor;
            this.destination = destinationEditor.getThesaurus();;
        }

        private int run() {
            for (MotcleSync motcleSync : originSync.getFirstLevelList()) {
                Motcle destinationMotcle = destination.getMotcleByIdalpha(motcleSync.getIdalpha());
                try {
                    boolean done = destinationEditor.setParent(destinationMotcle, null);
                    if (done) {
                        changeSet.add(destinationMotcle.getId());
                    }
                    setParent(motcleSync, destinationMotcle);
                } catch (ParentRecursivityException pre) {

                }

            }
            int p = 0;
            for (MotcleSync motcleSync : originSync.getFirstLevelList()) {
                Motcle destinationMotcle = destination.getMotcleByIdalpha(motcleSync.getIdalpha());
                boolean done = destinationEditor.setChildIndex(destinationMotcle, p);
                if (done) {
                    changeSet.add(destinationMotcle.getId());
                }
                setOrder(motcleSync);
                p++;
            }
            return changeSet.size();
        }

        private void setParent(MotcleSync originParent, Motcle destinationParent) {
            for (MotcleSync orginChild : originParent.getChildList()) {
                Motcle destinationChild = destination.getMotcleByIdalpha(orginChild.getIdalpha());
                try {
                    boolean done = destinationEditor.setParent(destinationChild, destinationParent);
                    if (done) {
                        changeSet.add(destinationChild.getId());
                    }
                    setParent(orginChild, destinationChild);
                } catch (ParentRecursivityException pre) {

                }
            }

        }

        private void setOrder(MotcleSync originParent) {
            int p = 0;
            for (MotcleSync originChild : originParent.getChildList()) {
                Motcle destinationChild = destination.getMotcleByIdalpha(originChild.getIdalpha());
                boolean done = destinationEditor.setChildIndex(destinationChild, p);
                if (done) {
                    changeSet.add(destinationChild.getId());
                }
                setOrder(originChild);
                p++;
            }
        }

    }

}
