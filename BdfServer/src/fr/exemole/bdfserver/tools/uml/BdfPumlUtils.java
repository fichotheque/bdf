/* BdfServer - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.uml;

import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.uml.UmlClass;


/**
 *
 * @author Vincent Calame
 */
public class BdfPumlUtils {

    private BdfPumlUtils() {

    }

    public static UmlClass buildCorpusUmlClass(Corpus corpus, Lang lang, MessageLocalisation messageLocalisation) {
        UmlClass umlClass = buildUmlClass(corpus, lang);
        CorpusMetadata corpusMetadata = corpus.getCorpusMetadata();
        CorpusField soustitre = corpusMetadata.getCorpusField(FieldKey.SOUSTITRE);
        if (soustitre != null) {
            umlClass.addField(CorpusMetadataUtils.getFieldTitle(soustitre, lang));
        }
        addFieldList(umlClass, corpusMetadata.getProprieteList(), lang, messageLocalisation, "_ title.corpus.fields_propriete");
        addFieldList(umlClass, corpusMetadata.getInformationList(), lang, messageLocalisation, "_ title.corpus.fields_information");
        addFieldList(umlClass, corpusMetadata.getSectionList(), lang, messageLocalisation, "_ title.corpus.fields_section");
        return umlClass;
    }

    public static UmlClass buildUmlClass(Subset subset, Lang lang) {
        SubsetKey subsetKey = subset.getSubsetKey();
        short subsetCategory = subsetKey.getCategory();
        char letter;
        String color;
        if (subsetKey.isCorpusSubset()) {
            Subset masterSubset = ((Corpus) subset).getMasterSubset();
            if (masterSubset != null) {
                letter = 'S';
                color = "#e2e2e2";
            } else {
                letter = getLetter(SubsetKey.CATEGORY_CORPUS);
                color = getColor(SubsetKey.CATEGORY_CORPUS);
            }
        } else {
            letter = getLetter(subsetCategory);
            color = getColor(subsetCategory);
        }
        return UmlClass.init(subsetKey.getKeyString())
                .title(FichothequeUtils.getTitle(subset, lang))
                .circle(letter, color);
    }

    private static char getLetter(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return 'C';
            case SubsetKey.CATEGORY_THESAURUS:
                return 'T';
            case SubsetKey.CATEGORY_SPHERE:
                return 'U';
            case SubsetKey.CATEGORY_ADDENDA:
                return 'D';
            case SubsetKey.CATEGORY_ALBUM:
                return 'I';
            default:
                throw new IllegalArgumentException("wrong croisementCategory value");
        }
    }

    private static String getColor(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return "#ffbdbd";
            case SubsetKey.CATEGORY_THESAURUS:
                return "#acf2fe";
            case SubsetKey.CATEGORY_SPHERE:
                return "#b8ffb8";
            case SubsetKey.CATEGORY_ADDENDA:
                return "#ffdfb8";
            case SubsetKey.CATEGORY_ALBUM:
                return "#f5c4fd";
            default:
                throw new IllegalArgumentException("wrong croisementCategory value");
        }
    }

    public static String getRelation(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return " -- ";
            case SubsetKey.CATEGORY_THESAURUS:
                return " <-- ";
            case SubsetKey.CATEGORY_SPHERE:
                return " -- ";
            case SubsetKey.CATEGORY_ADDENDA:
                return " o-left- ";
            case SubsetKey.CATEGORY_ALBUM:
                return " o-right- ";
            default:
                throw new IllegalArgumentException("wrong croisementCategory value");
        }
    }

    private static void addFieldList(UmlClass umlClass, List<CorpusField> fieldList, Lang lang, MessageLocalisation messageLocalisation, String locKey) {
        if (fieldList.isEmpty()) {
            return;
        }
        umlClass.addSeparator(messageLocalisation.toString(locKey));
        for (CorpusField corpusField : fieldList) {
            umlClass.addField(CorpusMetadataUtils.getFieldTitle(corpusField, lang));
        }
    }

}
