/* BdfServer - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.memento;

import fr.exemole.bdfserver.api.memento.MementoNode;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public final class MementoUtils {

    public final static List<MementoNode> EMPTY_NODELIST = Collections.emptyList();

    private MementoUtils() {

    }

    public static List<MementoNode> wrap(MementoNode[] array) {
        return new MementoNodeList(array);
    }


    private static class MementoNodeList extends AbstractList<MementoNode> implements RandomAccess {

        private final MementoNode[] array;

        private MementoNodeList(MementoNode[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MementoNode get(int index) {
            return array[index];
        }

    }

}
