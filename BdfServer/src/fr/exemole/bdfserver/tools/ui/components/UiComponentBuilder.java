/* BdfServer - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ui.components;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.mapeadores.util.text.MultiStringable;


/**
 *
 * @author Vincent Calame
 */
public abstract class UiComponentBuilder {

    protected final Map<String, Object> optionMap = new HashMap<String, Object>();

    public UiComponentBuilder() {

    }

    public abstract UiComponentBuilder setStatus(String status);

    public UiComponentBuilder putOption(String name, Object obj) {
        if (obj == null) {
            optionMap.remove(name);
        } else if (obj instanceof String) {
            String value = (String) obj;
            if (value.isEmpty()) {
                optionMap.remove(name);
            } else {
                optionMap.put(name, value);
            }
        } else if (obj instanceof MultiStringable) {
            optionMap.put(name, obj);
        } else {
            throw new IllegalArgumentException("obj not instance of String or MultiStringable");
        }
        return this;
    }

    protected SortedMap<String, Object> sortOptions() {
        return new TreeMap<String, Object>(optionMap);
    }

}
