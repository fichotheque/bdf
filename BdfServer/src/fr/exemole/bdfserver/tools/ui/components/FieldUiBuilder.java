/* BdfServer - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ui.components;

import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.ui.FieldUi;
import java.util.Collections;
import java.util.Set;
import java.util.SortedMap;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class FieldUiBuilder extends UiComponentBuilder {

    private final FieldKey fieldKey;
    private final AttributeChangeBuilder attributeChangeBuilder = new AttributeChangeBuilder();
    private final Attributes previousAttributes;
    private String status = BdfServerConstants.DEFAULT_STATUS;

    public FieldUiBuilder(FieldKey fieldKey) {
        this(fieldKey, null);
    }

    public FieldUiBuilder(FieldKey fieldKey, @Nullable Attributes previousAttributes) {
        if (fieldKey == null) {
            throw new IllegalArgumentException("corpusField is null");
        }
        this.fieldKey = fieldKey;
        this.previousAttributes = previousAttributes;
    }

    public FieldUiBuilder(FieldUi fieldUi) {
        this.fieldKey = fieldUi.getFieldKey();
        this.status = fieldUi.getStatus();
        for (String optionName : fieldUi.getOptionNameSet()) {
            optionMap.put(optionName, fieldUi.getOption(optionName));
        }
        this.previousAttributes = fieldUi.getAttributes();
    }

    @Override
    public FieldUiBuilder setStatus(String status) {
        if (((status != null) && (FieldUi.isModifiableStatus(fieldKey)))) {
            this.status = BdfServerConstants.checkValidStatus(status);
        }
        return this;
    }

    @Override
    public FieldUiBuilder putOption(String name, Object obj) {
        return (FieldUiBuilder) super.putOption(name, obj);
    }

    public AttributeChangeBuilder getAttributeChangeBuilder() {
        return attributeChangeBuilder;
    }

    public FieldUi toFieldUi() {
        return new InternalFieldUi(fieldKey, status, sortOptions(), attributeChangeBuilder.toAttributes(previousAttributes));
    }

    public static FieldUiBuilder init(FieldKey fieldKey) {
        return new FieldUiBuilder(fieldKey, null);
    }

    public static FieldUiBuilder init(FieldKey fieldKey, @Nullable Attributes initAttributes) {
        return new FieldUiBuilder(fieldKey, initAttributes);
    }

    public static FieldUiBuilder derive(FieldUi fieldUi) {
        return new FieldUiBuilder(fieldUi);
    }


    private static class InternalFieldUi implements FieldUi {

        private final FieldKey fieldKey;
        private final String status;
        private final SortedMap<String, Object> optionMap;
        private final Attributes attributes;

        private InternalFieldUi(FieldKey fieldKey, String status, SortedMap<String, Object> optionMap, Attributes attributes) {
            this.fieldKey = fieldKey;
            this.status = status;
            this.optionMap = optionMap;
            this.attributes = attributes;
        }

        @Override
        public FieldKey getFieldKey() {
            return fieldKey;
        }

        @Override
        public String getStatus() {
            return status;
        }

        @Override
        public Object getOption(String optionName) {
            return optionMap.get(optionName);
        }

        @Override
        public Set<String> getOptionNameSet() {
            return Collections.unmodifiableSet(optionMap.keySet());
        }

        @Override
        public String getName() {
            return fieldKey.getKeyString();
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

    }

}
