/* BdfServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ui.components;


/**
 *
 * @author Vincent Calame
 */
public class UiSummary {

    private final boolean addendaInclude;
    private final boolean albumInclude;

    public UiSummary(boolean addendaInclude, boolean albumInclude) {
        this.addendaInclude = addendaInclude;
        this.albumInclude = albumInclude;
    }

    public boolean withAddendaInclude() {
        return addendaInclude;
    }

    public boolean withAlbumInclude() {
        return albumInclude;
    }

}
