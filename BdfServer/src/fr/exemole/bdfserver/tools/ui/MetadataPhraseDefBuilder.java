/* BdfServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ui;

import fr.exemole.bdfserver.api.ui.MetadataPhraseDef;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.localisation.Litteral;


/**
 *
 * @author Vincent Calame
 */
public class MetadataPhraseDefBuilder {

    private final String name;
    private Object l10nObject;
    private int size = 30;

    public MetadataPhraseDefBuilder(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name is null ");
        }
        if (!FichothequeUtils.isValidPhraseName(name)) {
            throw new IllegalArgumentException("name is invalid");
        }
        this.name = name;
    }

    public MetadataPhraseDefBuilder setL10nObject(Object l10nObject) {
        this.l10nObject = l10nObject;
        return this;
    }

    public MetadataPhraseDefBuilder setSize(int size) {
        if (size < 1) {
            size = 30;
        }
        this.size = size;
        return this;
    }

    public MetadataPhraseDef toMetadataPhraseDef() {
        Object finalL10nObject;
        if (l10nObject != null) {
            finalL10nObject = l10nObject;
        } else {
            finalL10nObject = new Litteral("[" + name + "]");
        }
        return new InternalMetadataPhraseDef(name, finalL10nObject, size);
    }

    public static MetadataPhraseDefBuilder init(String name) {
        return new MetadataPhraseDefBuilder(name);
    }


    private static class InternalMetadataPhraseDef implements MetadataPhraseDef {

        private final String name;
        private final Object l10nObject;
        private final int size;

        private InternalMetadataPhraseDef(String name, Object l10nObject, int size) {
            this.name = name;
            this.l10nObject = l10nObject;
            this.size = size;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Object getL10nObject() {
            return l10nObject;
        }

        @Override
        public int getSize() {
            return size;
        }

    }

}
