/* BdfServer - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ui;

import fr.exemole.bdfserver.api.ui.MetadataPhraseDef;
import java.text.ParseException;
import net.fichotheque.utils.FichothequeUtils;


/**
 *
 * @author Vincent Calame
 */
public final class MetadataPhraseDefParser {

    private MetadataPhraseDefParser() {

    }

    public static MetadataPhraseDef parse(String value) throws ParseException {
        String name = value;
        int idx = value.indexOf('|');
        if (idx == -1) {
            MetadataPhraseDef def = MetadataPhraseDefCatalog.getMetadataPhraseDef(name);
            if (def != null) {
                return def;
            } else {
                FichothequeUtils.checkPhraseName(name);
                return MetadataPhraseDefBuilder.init(name).toMetadataPhraseDef();
            }
        } else {
            name = value.substring(0, idx).trim();
            FichothequeUtils.checkPhraseName(name);
            String l10nKey = value.substring(idx + 1);
            int size = 30;
            int idx2 = l10nKey.indexOf('|');
            if (idx2 > 0) {
                try {
                    size = Integer.parseInt(l10nKey.substring(idx2 + 1).trim());
                } catch (NumberFormatException nfe) {

                }
                if (size < 1) {
                    size = 30;
                }
                l10nKey = l10nKey.substring(0, idx2);
            }
            l10nKey = l10nKey.trim();
            if (l10nKey.isEmpty()) {
                l10nKey = null;
            }
            return MetadataPhraseDefBuilder.init(name).setL10nObject(l10nKey).setSize(size).toMetadataPhraseDef();
        }
    }

}
