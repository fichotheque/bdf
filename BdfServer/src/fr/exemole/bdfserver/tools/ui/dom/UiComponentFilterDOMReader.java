/* BdfServer - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ui.dom;

import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.tools.ui.UiComponentFilterBuilder;
import fr.exemole.bdfserver.tools.ui.components.UiDOMUtils;
import net.mapeadores.util.html.TrustedHtmlFactory;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
public class UiComponentFilterDOMReader {

    private final UiComponentFilterBuilder builder;
    private final TrustedHtmlFactory trustedHtmlFactory;
    private final MessageHandler messageHandler;

    public UiComponentFilterDOMReader(UiComponentFilterBuilder builder, TrustedHtmlFactory trustedHtmlFactory, MessageHandler messageHandler) {
        this.builder = builder;
        this.messageHandler = messageHandler;
        this.trustedHtmlFactory = trustedHtmlFactory;
    }

    public void readComponentUiFilter(Element element_xml) {
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                String tagName = element.getTagName();
                if (tagName.equals("exclude")) {
                    readExclude(element);
                } else if (tagName.equals("replace")) {
                    readReplace(element);
                } else {
                    DomMessages.unknownTagWarning(messageHandler, tagName);
                }
            }
        }
    }

    private void readReplace(Element element_xml) {
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                UiComponent componentUi = readUiComponent(element);
                if (componentUi != null) {
                    builder.replaceUiComponent(componentUi);
                }
            }
        }
    }

    private void readExclude(Element element_xml) {
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                UiComponent componentUi = readUiComponent(element);
                if (componentUi != null) {
                    builder.excludeUiComponent(componentUi);
                }
            }
        }
    }

    private UiComponent readUiComponent(Element element) {
        String tagName = element.getTagName();
        switch (tagName) {
            case "field-ui":
                return UiDOMUtils.readFieldUi(element, messageHandler, "field-ui");
            case "subset-ui":
                return UiDOMUtils.readSubsetIncludeUi(element, messageHandler, "subset-ui");
            case "special-ui":
                return UiDOMUtils.readSpecialIncludeUi(element, messageHandler, "special-ui");
            case "comment-def":
                return UiDOMUtils.readCommentUi(element, trustedHtmlFactory, messageHandler, "comment-def");
            case "data-ui":
                return UiDOMUtils.readDataUi(element, messageHandler, "data-ui");
            default:
                DomMessages.unknownTagWarning(messageHandler, tagName);
                return null;
        }
    }

}
