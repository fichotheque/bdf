/* BdfServer - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ui;

import fr.exemole.bdfserver.api.ui.MetadataPhraseDef;
import net.fichotheque.FichothequeConstants;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public final class MetadataPhraseDefCatalog {

    public final static MetadataPhraseDef LONG_PHRASEDEF = MetadataPhraseDefBuilder.init(FichothequeConstants.LONG_PHRASE).setL10nObject("_ label.phrase.long").setSize(60).toMetadataPhraseDef();
    public final static MetadataPhraseDef FICHE_PHRASEDEF = MetadataPhraseDefBuilder.init(FichothequeConstants.FICHE_PHRASE).setL10nObject("_ label.phrase.fiche").toMetadataPhraseDef();
    public final static MetadataPhraseDef NEWFICHE_PHRASEDEF = MetadataPhraseDefBuilder.init(FichothequeConstants.NEWFICHE_PHRASE).setL10nObject("_ label.phrase.newfiche").toMetadataPhraseDef();
    public final static MetadataPhraseDef FICHESTYLE_PHRASEDEF = MetadataPhraseDefBuilder.init(FichothequeConstants.FICHESTYLE_PHRASE).setL10nObject("_ label.phrase.fichestyle").toMetadataPhraseDef();
    public final static MetadataPhraseDef SATELLITE_PHRASEDEF = MetadataPhraseDefBuilder.init(FichothequeConstants.SATELLITE_PHRASE).setL10nObject("_ label.phrase.satellite").toMetadataPhraseDef();
    public final static MetadataPhraseDef NAMING_PHRASEDEF = MetadataPhraseDefBuilder.init(FichothequeConstants.NAMING_PHRASE).setL10nObject("_ label.phrase.naming").toMetadataPhraseDef();

    private MetadataPhraseDefCatalog() {

    }

    @Nullable
    public static MetadataPhraseDef getMetadataPhraseDef(String name) {
        switch (name) {
            case FichothequeConstants.LONG_PHRASE:
                return LONG_PHRASEDEF;
            case FichothequeConstants.FICHE_PHRASE:
                return FICHE_PHRASEDEF;
            case FichothequeConstants.NEWFICHE_PHRASE:
                return NEWFICHE_PHRASEDEF;
            case FichothequeConstants.FICHESTYLE_PHRASE:
                return FICHESTYLE_PHRASEDEF;
            case FichothequeConstants.SATELLITE_PHRASE:
                return SATELLITE_PHRASEDEF;
            case FichothequeConstants.NAMING_PHRASE:
                return NAMING_PHRASEDEF;
            default:
                return null;
        }
    }

}
