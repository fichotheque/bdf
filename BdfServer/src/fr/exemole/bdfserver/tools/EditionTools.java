/* BdfServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import net.fichotheque.ExistingIdException;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.NoMasterIdException;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class EditionTools {

    private EditionTools() {
    }

    public static FicheMeta createFiche(EditSession editSession, BdfParameters bdfParameters, SubsetKey corpusKey, RequestMap requestMap) {
        CorpusEditor corpusEditor = editSession.getFichothequeEditor().getCorpusEditor(corpusKey);
        if (corpusEditor == null) {
            throw new IllegalArgumentException("Unknown corpus: " + corpusKey);
        }
        FicheMeta ficheMeta = null;
        try {
            ficheMeta = corpusEditor.createFiche(-1);
        } catch (ExistingIdException eii) {
            throw new ImplementationException(eii);
        } catch (NoMasterIdException nmie) {
            throw new IllegalArgumentException("Corpus cannot be a satellite corpus: " + corpusKey);
        }
        EditionEngine.replace(editSession, bdfParameters, requestMap, ficheMeta);
        return ficheMeta;
    }

}
