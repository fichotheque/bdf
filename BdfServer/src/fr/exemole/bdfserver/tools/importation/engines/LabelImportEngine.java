/* BdfServer - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.importation.engines;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerEditor;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.ui.IncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.ui.components.IncludeUiBuilder;
import java.util.List;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.MetadataEditor;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.corpus.metadata.CorpusMetadataEditor;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.importation.LabelImport;
import net.fichotheque.include.IncludeKey;
import net.fichotheque.tools.FichothequeTools;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.text.LabelUtils;


/**
 *
 * @author Vincent Calame
 */
public final class LabelImportEngine {

    private LabelImportEngine() {
    }

    public static void runLabelImport(EditSession editSession, LabelImport labelImport) {
        BdfServer bdfServer = editSession.getBdfServer();
        Lang lang = labelImport.getLang();
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        updateMetadata(fichothequeEditor.getFichothequeMetadataEditor(), labelImport.getFichothequePhraseImportList(), lang);
        for (LabelImport.MetadataImport metadataLabelImport : labelImport.getMetadataImportList()) {
            updateMetadata(FichothequeTools.getMetadataEditor(fichothequeEditor, metadataLabelImport.getSubset()), metadataLabelImport.getPhraseImportList(), lang);
        }
        BdfServerEditor bdfServerEditor = editSession.getBdfServerEditor();
        for (LabelImport.CorpusImport corpusLabelImport : labelImport.getCorpusImportList()) {
            Corpus corpus = corpusLabelImport.getCorpus();
            CorpusMetadataEditor cme = fichothequeEditor.getCorpusEditor(corpus.getSubsetKey()).getCorpusMetadataEditor();
            updateCorpusField(cme, corpusLabelImport, lang);
            updateInclude(bdfServerEditor, bdfServer.getUiManager().getMainUiComponents(corpus), corpusLabelImport, lang);
        }
    }

    private static void updateMetadata(MetadataEditor metadataEditor, List<LabelImport.PhraseImport> phraseImportList, Lang lang) {
        for (LabelImport.PhraseImport phraseImport : phraseImportList) {
            String name = phraseImport.getName();
            CleanedString value = phraseImport.getLabelString();
            if (value != null) {
                metadataEditor.putLabel(name, LabelUtils.toLabel(lang, value));
            } else {
                metadataEditor.removeLabel(name, lang);
            }
        }
    }

    private static void updateCorpusField(CorpusMetadataEditor corpusMetadataEditor, LabelImport.CorpusImport corpusImport, Lang lang) {
        CorpusMetadata corpusMetadata = (CorpusMetadata) corpusMetadataEditor.getMetadata();
        for (LabelImport.FieldKeyImport fieldKeyImport : corpusImport.getFieldKeyImportList()) {
            FieldKey fieldKey = fieldKeyImport.getFieldKey();
            CleanedString value = fieldKeyImport.getLabelString();
            CorpusField corpusField = corpusMetadata.getCorpusField(fieldKey);
            if (corpusField != null) {
                if (value != null) {
                    corpusMetadataEditor.putFieldLabel(corpusField, LabelUtils.toLabel(lang, value));
                } else {
                    corpusMetadataEditor.removeFieldLabel(corpusField, lang);
                }
            }
        }
    }

    private static void updateInclude(BdfServerEditor bdfServerEditor, UiComponents uiComponents, LabelImport.CorpusImport corpusImport, Lang lang) {
        for (LabelImport.IncludeKeyImport includeKeyImport : corpusImport.getIncludeKeyImportList()) {
            IncludeKey includeKey = includeKeyImport.getIncludeKey();
            CleanedString value = includeKeyImport.getLabelString();
            IncludeUi includeUi = (IncludeUi) uiComponents.getUiComponent(includeKey);
            if (includeUi != null) {
                IncludeUiBuilder includeUiBuilder = IncludeUiBuilder.init(includeUi);
                LabelChangeBuilder labelChangeBuilder = includeUiBuilder.getLabelChangeBuilder();
                if (value != null) {
                    labelChangeBuilder.putLabel(LabelUtils.toLabel(lang, value));
                } else {
                    labelChangeBuilder.putRemovedLang(lang);
                }
                bdfServerEditor.putComponentUi(uiComponents, includeUiBuilder.toIncludeUi());
            }
        }
    }

}
