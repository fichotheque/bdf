/* BdfServer - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.importation.engines;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheChange;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.NoMasterIdException;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.croisement.CroisementEditor;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.tools.FichothequeTools;
import net.fichotheque.tools.corpus.CorpusTools;
import net.fichotheque.tools.corpus.FieldGenerationEngine;
import net.fichotheque.utils.CorpusUtils;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusImportEngine {

    private CorpusImportEngine() {
    }

    public static void runCorpusImport(EditSession editSession, BdfParameters bdfParameters, CorpusImport corpusImport) {
        String type = corpusImport.getType();
        if (type.equals(CorpusImport.CREATION_TYPE)) {
            runCreation(editSession, bdfParameters, corpusImport);
        } else if (type.equals(CorpusImport.CHANGE_TYPE)) {
            runChange(editSession, bdfParameters, corpusImport);
        } else if (type.equals(CorpusImport.REMOVE_TYPE)) {
            runRemove(editSession, corpusImport);
        }
    }

    private static void runCreation(EditSession editSession, BdfParameters bdfParameters, CorpusImport corpusImport) {
        Corpus corpus = corpusImport.getCorpus();
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        CroisementEditor croisementEditor = fichothequeEditor.getCroisementEditor();
        CorpusEditor corpusEditor = fichothequeEditor.getCorpusEditor(corpus);
        FieldGenerationEngine engine = BdfCommandUtils.buildEngine(bdfParameters, corpus);
        for (CorpusImport.FicheImport ficheImport : corpusImport.getFicheImportList()) {
            CorpusImport.CreationFicheImport creationFicheImport = (CorpusImport.CreationFicheImport) ficheImport;
            FicheChange ficheChange = creationFicheImport.getFicheChange();
            FicheMeta ficheMeta;
            try {
                ficheMeta = corpusEditor.createFiche(creationFicheImport.getNewId());
                FuzzyDate creationDate = creationFicheImport.getCreationDate();
                if (creationDate != null) {
                    corpusEditor.setDate(ficheMeta, creationDate, false);
                    corpusEditor.setDate(ficheMeta, FuzzyDate.current(), true);
                } else {
                    corpusEditor.setDate(ficheMeta, FuzzyDate.current(), false);
                }
            } catch (ExistingIdException | NoMasterIdException e) {
                continue;
            }
            fichothequeEditor.changeAttributes(ficheMeta, creationFicheImport.getAttributeChange());
            LiensImportEngine.run(croisementEditor, ficheMeta, creationFicheImport.getLiensImport(), bdfParameters.getPermissionSummary());
            Fiche fiche = new Fiche();
            if (CorpusUtils.hasChanges(ficheChange)) {
                CorpusUtils.updateFiche(fiche, ficheChange);
            }
            CorpusTools.saveFiche(corpusEditor, ficheMeta, fiche, engine, false);
        }
    }

    private static void runChange(EditSession editSession, BdfParameters bdfParameters, CorpusImport corpusImport) {
        Corpus corpus = corpusImport.getCorpus();
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        CroisementEditor croisementEditor = fichothequeEditor.getCroisementEditor();
        CorpusEditor corpusEditor = fichothequeEditor.getCorpusEditor(corpus);
        FieldGenerationEngine engine = BdfCommandUtils.buildEngine(bdfParameters, corpus);
        for (CorpusImport.FicheImport ficheImport : corpusImport.getFicheImportList()) {
            CorpusImport.ChangeFicheImport changeFicheImport = (CorpusImport.ChangeFicheImport) ficheImport;
            FicheMeta ficheMeta = changeFicheImport.getFicheMeta();
            fichothequeEditor.changeAttributes(ficheMeta, changeFicheImport.getAttributeChange());
            LiensImportEngine.run(croisementEditor, ficheMeta, changeFicheImport.getLiensImport(), bdfParameters.getPermissionSummary());
            FicheChange ficheChange = changeFicheImport.getFicheChange();
            FuzzyDate creationDate = changeFicheImport.getCreationDate();
            if (creationDate != null) {
                corpusEditor.setDate(ficheMeta, creationDate, false);
            }
            if (CorpusUtils.hasChanges(ficheChange)) {
                Fiche fiche = corpus.getFiche(ficheMeta);
                CorpusUtils.updateFiche(fiche, ficheChange);
                CorpusTools.saveFiche(corpusEditor, ficheMeta, fiche, engine, true);
            }
        }
    }

    private static void runRemove(EditSession editSession, CorpusImport corpusImport) {
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        for (CorpusImport.FicheImport ficheImport : corpusImport.getFicheImportList()) {
            FicheMeta ficheMeta = ficheImport.getFicheMeta();
            FichothequeTools.remove(fichothequeEditor, ficheMeta);
        }
    }

}
