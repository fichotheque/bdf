/* BdfServer - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.importation.engines;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.croisement.CroisementEditor;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.ParentRecursivityException;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.tools.FichothequeTools;
import net.fichotheque.tools.thesaurus.ThesaurusTools;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.localisation.Langs;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusImportEngine {

    private ThesaurusImportEngine() {
    }

    public static void runThesaurusImport(EditSession editSession, BdfParameters bdfParameters, ThesaurusImport thesaurusImport) {
        String type = thesaurusImport.getType();
        if (type.equals(ThesaurusImport.CREATION_TYPE)) {
            runCreation(editSession, bdfParameters, thesaurusImport);
        } else if (type.equals(ThesaurusImport.CHANGE_TYPE)) {
            runChange(editSession, bdfParameters, thesaurusImport);
        } else if (type.equals(ThesaurusImport.REMOVE_TYPE)) {
            runRemove(editSession, thesaurusImport);
        } else if (type.equals(ThesaurusImport.MERGE_TYPE)) {
            runMerge(editSession, thesaurusImport);
        } else if (type.equals(ThesaurusImport.MOVE_TYPE)) {
            runMove(editSession, thesaurusImport);
        }
    }

    private static void runCreation(EditSession editSession, BdfParameters bdfParameters, ThesaurusImport thesaurusImport) {
        Thesaurus thesaurus = thesaurusImport.getThesaurus();
        boolean withIdalpha = thesaurus.isIdalphaType();
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        CroisementEditor croisementEditor = fichothequeEditor.getCroisementEditor();
        ThesaurusEditor thesaurusEditor = fichothequeEditor.getThesaurusEditor(thesaurus);
        for (ThesaurusImport.MotcleImport motcleImport : thesaurusImport.getMotcleImportList()) {
            ThesaurusImport.CreationMotcleImport creationMotcleImport = (ThesaurusImport.CreationMotcleImport) motcleImport;
            String newIdalpha = creationMotcleImport.getNewIdalpha();
            if (withIdalpha) {
                if (newIdalpha == null) {
                    continue;
                }
            } else {
                newIdalpha = null;
            }
            try {
                Motcle motcle = thesaurusEditor.createMotcle(creationMotcleImport.getNewId(), newIdalpha);
                ThesaurusUtils.changeMotcleLabels(thesaurusEditor, motcle, creationMotcleImport.getLabelChange());
                fichothequeEditor.changeAttributes(motcle, creationMotcleImport.getAttributeChange());
                LiensImportEngine.run(croisementEditor, motcle, creationMotcleImport.getLiensImport(), bdfParameters.getPermissionSummary());
                Motcle parent = null;
                int parentId = creationMotcleImport.getParentId();
                if (parentId > 0) {
                    parent = thesaurus.getMotcleById(parentId);
                } else if (withIdalpha) {
                    String parentIdalpha = creationMotcleImport.getParentIdalpha();
                    if (parentIdalpha != null) {
                        parent = thesaurus.getMotcleByIdalpha(parentIdalpha);
                    }
                }
                if (parent != null) {
                    try {
                        thesaurusEditor.setParent(motcle, parent);
                    } catch (ParentRecursivityException pre) {
                    }
                }
                String newStatus = creationMotcleImport.getNewStatus();
                if (newStatus != null) {
                    thesaurusEditor.setStatus(motcle, newStatus);
                }
            } catch (ExistingIdException | ParseException eie) {
            }
        }
    }

    private static void runChange(EditSession editSession, BdfParameters bdfParameters, ThesaurusImport thesaurusImport) {
        Thesaurus thesaurus = thesaurusImport.getThesaurus();
        boolean withIdalpha = thesaurus.isIdalphaType();
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        CroisementEditor croisementEditor = fichothequeEditor.getCroisementEditor();
        ThesaurusEditor thesaurusEditor = fichothequeEditor.getThesaurusEditor(thesaurus);
        for (ThesaurusImport.MotcleImport motcleImport : thesaurusImport.getMotcleImportList()) {
            ThesaurusImport.ChangeMotcleImport changeMotcleImport = (ThesaurusImport.ChangeMotcleImport) motcleImport;
            Motcle motcle = changeMotcleImport.getMotcle();
            String newStatus = changeMotcleImport.getNewStatus();
            if (newStatus != null) {
                thesaurusEditor.setStatus(motcle, newStatus);
            }
            ThesaurusUtils.changeMotcleLabels(thesaurusEditor, motcle, changeMotcleImport.getLabelChange());
            fichothequeEditor.changeAttributes(motcle, changeMotcleImport.getAttributeChange());
            LiensImportEngine.run(croisementEditor, motcle, changeMotcleImport.getLiensImport(), bdfParameters.getPermissionSummary());
            if (withIdalpha) {
                String newIdalpha = changeMotcleImport.getNewIdalpha();
                if (newIdalpha != null) {
                    try {
                        thesaurusEditor.setIdalpha(motcle, newIdalpha);
                    } catch (ExistingIdException | ParseException eie) {
                    }
                }
            }
            Object newParent = changeMotcleImport.getParent();
            if (newParent != null) {
                Motcle newMotcleParent = null;
                if (newParent instanceof Motcle) {
                    newMotcleParent = (Motcle) newParent;
                }
                try {
                    thesaurusEditor.setParent(motcle, newMotcleParent);
                } catch (ParentRecursivityException pre) {
                }
            }
        }
    }

    private static void runRemove(EditSession editSession, ThesaurusImport thesaurusImport) {
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        int resultCount = 0;
        for (ThesaurusImport.MotcleImport motcleImport : thesaurusImport.getMotcleImportList()) {
            Motcle motcle = motcleImport.getMotcle();
            boolean done = FichothequeTools.remove(fichothequeEditor, motcle);
            if (done) {
                resultCount++;
            }
        }
    }

    private static void runMerge(EditSession editSession, ThesaurusImport thesaurusImport) {
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        int resultCount = 0;
        for (ThesaurusImport.MotcleImport motcleImport : thesaurusImport.getMotcleImportList()) {
            ThesaurusImport.MergeMotcleImport mergeMotcleImport = (ThesaurusImport.MergeMotcleImport) motcleImport;
            try {
                Langs langs = editSession.getBdfServer().getThesaurusLangChecker().getAuthorizedLangs(mergeMotcleImport.getDestinationMotcle().getThesaurus());
                ThesaurusTools.merge(fichothequeEditor, mergeMotcleImport.getMotcle(), mergeMotcleImport.getDestinationMotcle(), langs);
                resultCount++;
            } catch (ParentRecursivityException pre) {
            }
        }
    }

    private static void runMove(EditSession editSession, ThesaurusImport thesaurusImport) {
        FichothequeEditor fichothequeEditor = editSession.getFichothequeEditor();
        Thesaurus destinationThesaurus = thesaurusImport.getDestinationThesaurus();
        Langs langs = editSession.getBdfServer().getThesaurusLangChecker().getAuthorizedLangs(destinationThesaurus);
        for (ThesaurusImport.MotcleImport motcleImport : thesaurusImport.getMotcleImportList()) {
            ThesaurusTools.move(fichothequeEditor, motcleImport.getMotcle(), destinationThesaurus, langs);
        }
    }

}
