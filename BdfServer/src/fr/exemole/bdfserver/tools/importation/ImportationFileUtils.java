/* BdfServer - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.importation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.importation.LabelImport;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.xml.importation.CorpusImportXMLPart;
import net.fichotheque.xml.importation.LabelImportXMLPart;
import net.fichotheque.xml.importation.ThesaurusImportXMLPart;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;


/**
 *
 * @author Vincent Calame
 */
public final class ImportationFileUtils {

    private final static DateFormat NAME_FORMAT = new SimpleDateFormat("yyyyMMdd'T'HHmmss");

    private ImportationFileUtils() {
    }

    public static File saveTmpXml(BdfServer bdfServer, LabelImport labelImport) throws IOException {
        File xmlTmFile = createXmlTmpFile(bdfServer, ImportationEngine.LABEL_IMPORT);
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xmlTmFile), "UTF-8"))) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(writer);
            xmlWriter.appendXMLDeclaration();
            LabelImportXMLPart labelImportXMLPart = new LabelImportXMLPart(xmlWriter);
            labelImportXMLPart.addLabelImport(labelImport);
        }
        return xmlTmFile;
    }

    public static File saveTmpXml(BdfServer bdfServer, ThesaurusImport thesaurusImport) throws IOException {
        File xmlTmFile = createXmlTmpFile(bdfServer, ImportationEngine.THESAURUS_IMPORT);
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xmlTmFile), "UTF-8"))) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(writer);
            xmlWriter.appendXMLDeclaration();
            ThesaurusImportXMLPart thesaurusImportXMLPart = new ThesaurusImportXMLPart(xmlWriter);
            thesaurusImportXMLPart.addThesaurusImport(thesaurusImport);
        }
        return xmlTmFile;
    }

    public static File saveTmpXml(BdfServer bdfServer, CorpusImport corpusImport) throws IOException {
        File xmlTmFile = createXmlTmpFile(bdfServer, ImportationEngine.CORPUS_IMPORT);
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xmlTmFile), "UTF-8"))) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(writer);
            xmlWriter.appendXMLDeclaration();
            CorpusImportXMLPart corpusImportXMLPart = new CorpusImportXMLPart(xmlWriter);
            corpusImportXMLPart.addCorpusImport(corpusImport);
        }
        return xmlTmFile;
    }

    public static File createXmlTmpFile(BdfServer bdfServer, String basename) {
        synchronized (bdfServer) {
            File tmpDirectory = ConfigurationUtils.getTmpDirectory(bdfServer, true);
            String number = Long.toHexString(System.currentTimeMillis());
            File f = new File(tmpDirectory, basename + "-" + number + ".xml");
            if (!f.exists()) {
                return f;
            } else {
                int p = 1;
                while (true) {
                    f = new File(tmpDirectory, basename + "-" + number + "_" + p + ".xml");
                    if (!f.exists()) {
                        return f;
                    } else {
                        p++;
                    }
                }
            }
        }
    }

    public static File createTmpDir(BdfServer bdfServer, String basename) throws IOException {
        synchronized (bdfServer) {
            File tmpDirectory = ConfigurationUtils.getTmpDirectory(bdfServer, true);
            Date current = new Date();
            String prefix = basename + "." + NAME_FORMAT.format(current);
            File dir = new File(tmpDirectory, prefix);
            if (!dir.exists()) {
                dir.mkdir();
                return dir;
            } else {
                int p = 1;
                while (true) {
                    dir = new File(tmpDirectory, prefix + "_" + p + ".xml");
                    if (!dir.exists()) {
                        dir.mkdir();
                        return dir;
                    } else {
                        p++;
                    }
                }
            }
        }
    }

    public static File getTmpFile(BdfServer bdfServer, String tmpFileName) {
        File tmpDirectory = ConfigurationUtils.getTmpDirectory(bdfServer);
        return new File(tmpDirectory, tmpFileName);
    }

    public static void confirm(BdfServer bdfServer, File tmpFile) {
    }

}
