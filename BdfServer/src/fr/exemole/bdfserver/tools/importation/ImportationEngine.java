/* BdfServer - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.importation;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.tools.importation.engines.CorpusImportEngine;
import fr.exemole.bdfserver.tools.importation.engines.LabelImportEngine;
import fr.exemole.bdfserver.tools.importation.engines.ThesaurusImportEngine;
import java.io.File;
import java.text.ParseException;
import net.fichotheque.Fichotheque;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.importation.LabelImport;
import net.fichotheque.tools.importation.LabelImportBuilder;
import net.fichotheque.tools.importation.thesaurus.ThesaurusImportBuilder;
import net.fichotheque.tools.importation.directory.DirectoryCorpusImport;
import net.fichotheque.tools.importation.dom.ImportationDomUtils;
import net.fichotheque.tools.importation.dom.LabelImportDOMReader;
import net.fichotheque.tools.importation.dom.ThesaurusImportDOMReader;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public final class ImportationEngine {

    public final static String LABEL_IMPORT = "labelimport";
    public final static String THESAURUS_IMPORT = "thesaurusimport";
    public final static String CORPUS_IMPORT = "corpusimport";

    private ImportationEngine() {
    }

    public static void run(EditSession editSession, BdfParameters bdfParameters, String engineType, File source) {
        Fichotheque fichotheque = bdfParameters.getFichotheque();
        switch (engineType) {
            case LABEL_IMPORT: {
                Element rootElement = DOMUtils.readDocument(source).getDocumentElement();
                String langString = rootElement.getAttribute("lang");
                try {
                    Lang lang = Lang.parse(langString);
                    LabelImportBuilder builder = new LabelImportBuilder(lang);
                    LabelImportDOMReader.init(fichotheque, builder)
                            .read(rootElement);
                    LabelImport labelImport = builder.toLabelImport();
                    LabelImportEngine.runLabelImport(editSession, labelImport);
                } catch (ParseException pe) {
                }
                break;
            }
            case THESAURUS_IMPORT: {
                Element rootElement = DOMUtils.readDocument(source).getDocumentElement();
                ThesaurusImportBuilder thesaurusImportBuilder = ImportationDomUtils.buildThesaurusImportBuilder(fichotheque, rootElement);
                if (thesaurusImportBuilder != null) {
                    ThesaurusImportDOMReader.init(thesaurusImportBuilder)
                            .read(rootElement);
                    ThesaurusImportEngine.runThesaurusImport(editSession, bdfParameters, thesaurusImportBuilder.toThesaurusImport());
                }
                break;
            }
            case CORPUS_IMPORT: {
                CorpusImport corpusImport = DirectoryCorpusImport.load(fichotheque, bdfParameters.getContentChecker(), source);
                if (corpusImport != null) {
                    CorpusImportEngine.runCorpusImport(editSession, bdfParameters, corpusImport);
                }
                break;
            }
        }
    }

    public static boolean isValidEngineType(String engineType) {
        switch (engineType) {
            case LABEL_IMPORT:
            case THESAURUS_IMPORT:
            case CORPUS_IMPORT:
                return true;
            default:
                return false;
        }
    }

}
