/* BdfServer - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.exportation.balayage.BalayageConstants;
import net.fichotheque.exportation.balayage.BalayageDef;
import net.fichotheque.exportation.balayage.SiteMapOption;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.tools.exportation.balayage.BalayageUnitBuilder;
import net.fichotheque.utils.SelectionUtils;
import net.mapeadores.util.localisation.LangContext;


/**
 *
 * @author Vincent Calame
 */
public class BalayageBuilder {

    private final List<FicheQuery> globalFicheQuery = new ArrayList<FicheQuery>();
    private final List<BalayageUnitBuilder> unitList = new ArrayList<BalayageUnitBuilder>();
    private final List<String> externalScriptNameList = new ArrayList<String>();
    private final List<String> scrutariExportNameList = new ArrayList<String>();
    private final List<String> sqlExportNameList = new ArrayList<String>();
    private final BalayageDef balayageDef;
    private String defaultLangOption = BalayageConstants.LANGOPTION_NONE;
    private LangContext langContext;
    private boolean withTransformation;
    private String rootName;
    private String path;
    private SiteMapOption siteMapOption;

    public BalayageBuilder(BalayageDef balayageDef) {
        this.balayageDef = balayageDef;
    }


    public BalayageBuilder addGlobalFicheQuery(FicheQuery ficheQuery) {
        globalFicheQuery.add(ficheQuery);
        return this;
    }

    public BalayageBuilder setRootName(String rootName) {
        this.rootName = rootName;
        return this;
    }

    public BalayageBuilder setPath(String path) {
        this.path = path;
        return this;
    }

    public BalayageBuilder setWithTransformation(boolean withTransformation) {
        this.withTransformation = withTransformation;
        return this;
    }

    public BalayageBuilder addPostscriptumExternalScript(String name) {
        externalScriptNameList.add(name);
        return this;
    }

    public BalayageBuilder addPostscriptumScrutariExport(String name) {
        scrutariExportNameList.add(name);
        return this;
    }

    public BalayageBuilder addPostscriptumSqlExport(String name) {
        sqlExportNameList.add(name);
        return this;
    }

    public BalayageBuilder setDefaultLangOption(String defaultLangOption) {
        defaultLangOption = BalayageConstants.checkLangOption(defaultLangOption);
        if (defaultLangOption.equals(BalayageConstants.LANGOPTION_UNKNOWN)) {
            defaultLangOption = BalayageConstants.LANGOPTION_NONE;
        }
        this.defaultLangOption = defaultLangOption;
        return this;
    }

    public BalayageBuilder setLangContext(LangContext langContext) {
        this.langContext = langContext;
        return this;
    }

    public BalayageBuilder setSiteMapOption(SiteMapOption siteMapOption) {
        this.siteMapOption = siteMapOption;
        return this;
    }

    public BalayageUnitBuilder addBalayageUnitBuilder(String type) {
        BalayageUnitBuilder unitBuilder = new BalayageUnitBuilder(type);
        unitList.add(unitBuilder);
        return unitBuilder;
    }

    public void toBalayage() {
        List<FicheQuery> globalFicheQueryList = SelectionUtils.wrap(globalFicheQuery.toArray(new FicheQuery[globalFicheQuery.size()]));
    }

    public static BalayageBuilder init(BalayageDef balayageDef) {
        return new BalayageBuilder(balayageDef);
    }

}
