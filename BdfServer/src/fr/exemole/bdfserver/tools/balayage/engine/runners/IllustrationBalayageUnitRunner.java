/* BdfServer - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage.engine.runners;

import fr.exemole.bdfserver.tools.balayage.engine.BalayageParameters;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.album.Album;
import net.fichotheque.exportation.balayage.BalayageUnit;
import net.fichotheque.selection.IllustrationQuery;
import net.fichotheque.utils.SelectionUtils;
import net.fichotheque.exportation.balayage.BalayageOutput;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationBalayageUnitRunner extends AbstractBalayageUnitRunner {

    private IllustrationOutputRunner[] outputRunnerArray;

    public IllustrationBalayageUnitRunner(BalayageParameters balayageParameters, int balayageIndex, BalayageUnit balayageUnit) {
        super(balayageParameters, balayageIndex, balayageUnit);
        List<IllustrationOutputRunner> outputList = new ArrayList<IllustrationOutputRunner>();
        int p = 0;
        for (BalayageOutput output : balayageUnit.getOutputList()) {
            IllustrationOutputRunner outputRunner = new IllustrationOutputRunner(balayageParameters, p, output, balayageIndex);
            if (outputRunner.isValid()) {
                outputList.add(outputRunner);
                p++;
            }
        }
        outputRunnerArray = outputList.toArray(new IllustrationOutputRunner[outputList.size()]);
    }

    @Override
    protected void run(String balayageUnitType) {
        IllustrationQuery illustrationQuery = balayageUnit.getIllustrationQuery();
        Album[] albumArray = SelectionUtils.toAlbumArray(fichotheque, illustrationQuery, extractionContext.getSubsetAccessPredicate());
        List<String> albumDimNameList = illustrationQuery.getAlbumDimNameList();
        for (Album album : albumArray) {
            for (IllustrationOutputRunner outputRunner : outputRunnerArray) {
                outputRunner.copy(album, albumDimNameList, System.currentTimeMillis());
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        int length = outputRunnerArray.length;
        for (int i = 0; i < length; i++) {
            outputRunnerArray[i].dispose();
            outputRunnerArray[i] = null;
        }
        outputRunnerArray = null;
    }

    @Override
    protected boolean isEmpty() {
        return (outputRunnerArray.length == 0);
    }

}
