/* BdfServer - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage.engine.runners;

import net.fichotheque.exportation.balayage.BalayageConstants;
import net.fichotheque.exportation.balayage.BalayageUnit;
import fr.exemole.bdfserver.api.balayage.BalayageUnitRunner;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageParameters;


/**
 *
 * @author Vincent Calame
 */
public class RunnerFactory {

    public static BalayageUnitRunner get(BalayageParameters balayageParameters, int index, BalayageUnit balayageUnit) {
        switch (balayageUnit.getType()) {
            case BalayageConstants.DOCUMENT_TYPE:
                return new DocumentBalayageUnitRunner(balayageParameters, index, balayageUnit);
            case BalayageConstants.ILLUSTRATION_TYPE:
                return new IllustrationBalayageUnitRunner(balayageParameters, index, balayageUnit);
            default:
                return new TransformBalayageUnitRunner(balayageParameters, index, balayageUnit);
        }
    }

}
