/* BdfServer - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage.engine;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.tools.BdfURI;
import java.io.File;
import java.util.Map;
import net.fichotheque.exportation.balayage.BalayageDef;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.fichotheque.exportation.balayage.BalayageMode;
import net.fichotheque.exportation.balayage.SiteMapOption;
import net.fichotheque.namespaces.BalayageSpace;
import net.fichotheque.tools.exportation.balayage.SiteMapOptionBuilder;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class BalayageInitializer {

    private final BdfServer bdfServer;
    private final BalayageDescription balayageDescription;
    private final BalayageDef balayageDef;
    private final PathConfiguration pathConfiguration;
    private File balayageRootDirectory;
    private String errorMessageKey;
    private Object errorMessageValue;
    private BalayageMode balayageMode;

    public BalayageInitializer(BdfServer bdfServer, BalayageDescription balayageDescription, PathConfiguration pathConfiguration) {
        this.bdfServer = bdfServer;
        this.balayageDescription = balayageDescription;
        this.balayageDef = balayageDescription.getBalayageDef();
        this.pathConfiguration = pathConfiguration;
        init();
    }

    private void init() {
        String targetName = balayageDef.getTargetName();
        if (targetName == null) {
            errorMessageKey = "_ error.empty.balayage.root";
            return;
        }
        File targetDirectory = pathConfiguration.getTargetDirectory(targetName);
        if (targetDirectory == null) {
            targetDirectory = bdfServer.getOutputStorage().getFile(RelativePath.build("balayages/" + balayageDef.getName()));
            targetDirectory.mkdirs();
        }
        if (!targetDirectory.exists()) {
            errorMessageKey = "_ error.unknown.balayage.root";
            String[] err = {targetName, targetDirectory.getAbsolutePath()};
            errorMessageValue = err;
            return;
        }
        if (!targetDirectory.isDirectory()) {
            errorMessageKey = "_ error.wrong.balayage.root_notdirectory";
            String[] err = {targetName, targetDirectory.getAbsolutePath()};
            errorMessageValue = err;
            return;
        }
        RelativePath targetPath = balayageDef.getTargetPath();
        if (!targetPath.isEmpty()) {
            balayageRootDirectory = new File(targetDirectory, targetPath.getPath());
            if (!balayageRootDirectory.exists()) {
                balayageRootDirectory.mkdirs();
            } else if (!balayageRootDirectory.isDirectory()) {
                errorMessageKey = "_ error.wrong.balayage.path_notdirectory";
                errorMessageValue = balayageRootDirectory.getAbsolutePath();
                return;
            }
        } else {
            balayageRootDirectory = targetDirectory;
        }
        if (balayageDef.getBalayageUnitList().isEmpty()) {
            errorMessageKey = "_ error.empty.balayage.unit";
        }
    }

    public boolean hasError() {
        return (errorMessageKey != null);
    }

    public String getErrorMessageKey() {
        return errorMessageKey;
    }

    public Object getErrorMessageValue() {
        return errorMessageValue;
    }

    public void setBalayageMode(BalayageMode balayageMode) {
        this.balayageMode = balayageMode;
    }

    public BalayageEngine getBalayageEngine(BalayageLog balayageLog) {
        if (hasError()) {
            return null;
        }
        BalayageParameters balayageParameters = BalayageParameters.init(balayageDescription, bdfServer, pathConfiguration)
                .setBalayageMode(balayageMode)
                .setBalayageLog(balayageLog)
                .setBalayageRootDirectory(balayageRootDirectory)
                .setCustomURIResolver(BdfURI.getURIResolver(bdfServer, pathConfiguration, true));
        SiteMapOption siteMapOption = initSiteMapOption();
        if (siteMapOption != null) {
            SiteMapBuilder siteMapBuilder = new SiteMapBuilder(siteMapOption);
            balayageParameters.setFileGenerationListener(siteMapBuilder);
        }
        BalayageEngine balayageEngine = new BalayageEngine(balayageParameters);
        return balayageEngine;
    }

    private SiteMapOption initSiteMapOption() {
        Attribute attribute = balayageDef.getAttributes().getAttribute(BalayageSpace.SITEMAP_KEY);
        if (attribute == null) {
            return null;
        }
        Map<String, String> map = attribute.toSubParamMap();
        return SiteMapOptionBuilder.init()
                .setPath(map.get("path"))
                .setBaseUrl(map.get("base-url"))
                .setFileName(map.get("file-name"))
                .toSiteMapOption();
    }

}
