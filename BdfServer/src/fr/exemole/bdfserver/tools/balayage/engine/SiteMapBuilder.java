/* BdfServer - Copyright (c) 2009-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage.engine;

import fr.exemole.bdfserver.api.balayage.FileGenerationListener;
import net.fichotheque.exportation.balayage.SiteMapOption;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.xml.DefaultXMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class SiteMapBuilder implements FileGenerationListener {

    private final List<String> urlList = new ArrayList<String>();
    private final SiteMapOption siteMapOption;

    public SiteMapBuilder(SiteMapOption siteMapOption) {
        this.siteMapOption = siteMapOption;
    }

    @Override
    public void generateFile(String langDir, String path, String fileName) {
        if (!fileName.endsWith(".html")) {
            return;
        }
        StringBuilder buf = new StringBuilder();
        if (langDir != null) {
            buf.append(langDir);
            buf.append('/');
        }
        if ((path != null) && (path.length() > 0)) {
            buf.append(path);
            buf.append('/');
        }
        buf.append(fileName);
        urlList.add(buf.toString());
    }

    public String writeSiteMapFile(File directory) throws IOException {
        String path = siteMapOption.getPath();
        if (path != null) {
            directory = new File(directory, path);
        }
        File destination = new File(directory, siteMapOption.getFileName());
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(destination), "UTF-8"))) {
            SiteMapWriter siteMapWriter = new SiteMapWriter(siteMapOption.getBaseUrl());
            siteMapWriter.setAppendable(writer);
            siteMapWriter.appendXMLDeclaration();
            siteMapWriter.setIndentLength(-999);
            siteMapWriter.start();
            for (String url : urlList) {
                siteMapWriter.addUrl(url);
            }
            siteMapWriter.end();
        }
        return destination.getPath();
    }


    private static class SiteMapWriter extends DefaultXMLWriter {

        private String baseUrl;

        SiteMapWriter(String baseUrl) {
            if (baseUrl == null) {
                this.baseUrl = "";
            } else {
                this.baseUrl = baseUrl;
            }
        }

        public void start() throws IOException {
            startOpenTag("urlset");
            addAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
            endOpenTag();
        }

        public void addUrl(String url) throws IOException {
            openTag("url");
            addSimpleElement("loc", baseUrl + url);
            closeTag("url");
        }

        public void end() throws IOException {
            closeTag("urlset");
        }

    }

}
