/* BdfServer - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage.engine.runners;

import fr.exemole.bdfserver.tools.balayage.engine.BalayageLog;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageParameters;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import net.mapeadores.util.io.FileUtils;
import net.fichotheque.exportation.balayage.BalayageOutput;


/**
 *
 * @author Vincent Calame
 */
public class AbstractOutputRunner {

    protected HashSet<String> pathDoneSet = new HashSet<String>();
    protected boolean cleanBefore = false;
    protected BalayageLog balayageLog;
    protected BalayageParameters balayageParameters;
    protected int balayageUnitIndex;
    protected int outputIndex;
    protected BalayageOutput output;
    protected boolean isValid = true;
    protected File rootDirectory;

    public AbstractOutputRunner(BalayageParameters balayageParameters, int outputIndex, BalayageOutput output, int balayageUnitIndex) {
        this.balayageParameters = balayageParameters;
        this.cleanBefore = output.isCleanBefore();
        this.balayageLog = balayageParameters.getBalayageLog();
        this.outputIndex = outputIndex;
        this.output = output;
        if (output.isIncludeOutput()) {
            rootDirectory = ConfigurationUtils.getBalayageIncludeDirectory(balayageParameters.getBdfServer());
        } else {
            rootDirectory = balayageParameters.getBalayageRootDirectory();
        }
    }

    public boolean isValid() {
        return isValid;
    }

    public void dispose() {
        pathDoneSet.clear();
        pathDoneSet = null;
        balayageLog = null;
        balayageParameters = null;
        output = null;
        rootDirectory = null;
    }

    protected void prepareDirectory(File directory) {
        String path = directory.getPath();
        if (pathDoneSet.contains(path)) {
            return;
        }
        pathDoneSet.add(path);
        try {
            directory.mkdirs();
        } catch (Exception e) {
            balayageLog.addOutputError(balayageUnitIndex, outputIndex, output, BalayageLog.OUTPUTPATH_EXCEPTION, path, e.getMessage());
            isValid = false;
        }
        if ((!cleanBefore) || (balayageParameters.getBalayageMode() != null)) {
            return;
        }
        File[] files = directory.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            if (!file.isDirectory()) {
                try {
                    FileUtils.forceDelete(file);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                    String msg = ioe.getMessage();
                    String message = ioe.getClass().getName();
                    if (msg != null) {
                        message = message + " : " + msg;
                    }
                    balayageLog.addOutputError(balayageUnitIndex, outputIndex, output, BalayageLog.IO_EXCEPTION, directory.getName(), message);
                }
            }
        }
    }

}
