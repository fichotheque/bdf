/* BdfServer - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage.engine.runners;

import net.fichotheque.exportation.balayage.BalayageUnit;
import fr.exemole.bdfserver.api.balayage.BalayageUnitRunner;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageLog;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageParameters;
import net.fichotheque.Fichotheque;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.extraction.ExtractParameters;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractBalayageUnitRunner implements BalayageUnitRunner {

    protected BalayageParameters balayageParameters;
    protected BalayageLog balayageLog;
    protected BalayageUnit balayageUnit;
    protected int balayageIndex;
    protected ExtractParameters extractParameters;
    protected Fichotheque fichotheque;
    protected ExtractionContext extractionContext;
    protected Lang defaultWorkingLang;

    public AbstractBalayageUnitRunner(BalayageParameters balayageParameters, int balayageIndex, BalayageUnit balayageUnit) {
        this.balayageParameters = balayageParameters;
        this.balayageLog = balayageParameters.getBalayageLog();
        this.balayageIndex = balayageIndex;
        this.balayageUnit = balayageUnit;
        this.extractParameters = balayageParameters.getExtractParameters();
        this.extractionContext = extractParameters.getExtractionContext();
        this.fichotheque = extractionContext.getFichotheque();
        this.defaultWorkingLang = extractionContext.getLangContext().getDefaultLang();
    }

    @Override
    public void dispose() {
        balayageUnit = null;
        balayageParameters = null;
        balayageLog = null;
        extractionContext = null;
    }

    @Override
    public void run() {
        if (isEmpty()) {
            balayageLog.addBalayageUnitError(balayageIndex, balayageUnit, BalayageLog.NO_OUTPUT, null, null);
        } else {
            balayageLog.addBalayageUnitLog(balayageIndex, balayageUnit);
            run(balayageUnit.getType());
        }
    }

    protected abstract boolean isEmpty();

    protected abstract void run(String balayageUnitType);

}
