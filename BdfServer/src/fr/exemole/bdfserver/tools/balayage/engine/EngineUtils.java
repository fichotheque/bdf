/* BdfServer - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.balayage.engine;

import net.fichotheque.addenda.Document;
import net.fichotheque.album.Album;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.valueresolvers.AlbumDimValueResolver;
import net.fichotheque.tools.valueresolvers.AlbumValueResolver;
import net.fichotheque.tools.valueresolvers.CorpusValueResolver;
import net.fichotheque.tools.valueresolvers.DocumentValueResolver;
import net.fichotheque.tools.valueresolvers.FicheValueResolver;
import net.fichotheque.tools.valueresolvers.MotcleValueResolver;
import net.fichotheque.tools.valueresolvers.ThesaurusValueResolver;
import net.fichotheque.utils.BalayageUtils;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.ValueResolver;
import net.fichotheque.exportation.balayage.BalayageOutput;


/**
 *
 * @author Vincent Calame
 */
public class EngineUtils {

    private EngineUtils() {
    }

    public static ValueResolver getValueResolver(Object obj) {
        if (obj == null) {
            return AccoladePattern.DEFAULT_VALUERESOLVER;
        }
        if (obj instanceof Corpus) {
            return new CorpusValueResolver((Corpus) obj);
        }
        if (obj instanceof FicheMeta) {
            return new FicheValueResolver((FicheMeta) obj);
        }
        if (obj instanceof Thesaurus) {
            return new ThesaurusValueResolver((Thesaurus) obj);
        }
        if (obj instanceof Motcle) {
            return new MotcleValueResolver((Motcle) obj);
        }
        if (obj instanceof Document) {
            return new DocumentValueResolver((Document) obj);
        }
        if (obj instanceof Album) {
            return new AlbumValueResolver((Album) obj);
        }
        if (obj instanceof AlbumDim) {
            return new AlbumDimValueResolver((AlbumDim) obj);
        }
        return null;
    }

    public static String getPath(BalayageOutput output, ValueResolver patternValueSet) {
        AccoladePattern outputPathPattern = output.getOutputPath();
        if (outputPathPattern != null) {
            String result = outputPathPattern.format(patternValueSet);
            if (result.length() == 0) {
                return null;
            } else {
                return result;
            }
        }
        return null;
    }

    public static String getFileName(BalayageOutput output, ValueResolver valueResolver, String type, boolean htmlPrefix) {
        AccoladePattern pattern = BalayageUtils.getPattern(output, type);
        String fileName = pattern.format(valueResolver);
        if (fileName.lastIndexOf('.') == -1) {
            if (htmlPrefix) {
                fileName = fileName + ".html";
            } else {
                fileName = fileName + ".xml";
            }
        }
        return fileName;
    }

}
