/* BdfServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.externalsource;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.externalsource.FichothequeSharing;
import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.externalsource.ExternalSource;
import net.fichotheque.externalsource.ExternalSourceDef;
import net.fichotheque.externalsource.ExternalSourceProvider;
import net.fichotheque.utils.FichothequeUtils;


/**
 *
 * @author Vincent Calame
 */
public class CoreExternalSourceProvider implements ExternalSourceProvider {

    private final BdfServer bdfServer;

    public CoreExternalSourceProvider(BdfServer bdfServer) {
        this.bdfServer = bdfServer;
    }

    @Override
    public ExternalSource getExternalSource(ExternalSourceDef externalSourceDef) {
        switch (externalSourceDef.getType()) {
            case CoreExternalSourceCatalog.FICHOTHEQUECORPUS_TYPENAME:
                FichothequeSharing fichothequeSharing = (FichothequeSharing) bdfServer.getContextObject(BdfServerConstants.FICHOTHEQUESHARING_CONTEXTOBJECT);
                if (fichothequeSharing != null) {
                    return getSource(fichothequeSharing, externalSourceDef);
                } else {
                    return null;
                }
            case CoreExternalSourceCatalog.WIKIDATA_TYPENAME:
                return new WikidataExternalSource();
            default:
                return null;
        }

    }

    private CorpusExternalSource getSource(FichothequeSharing fichothequeSharing, ExternalSourceDef externalSourceDef) {
        String fichothequeName = externalSourceDef.getParam("fichotheque");
        if (fichothequeName == null) {
            return null;
        }
        Fichotheque otherFichotheque = fichothequeSharing.getFichotheque(fichothequeName);
        if (otherFichotheque == null) {
            return null;
        }
        String corpusName = externalSourceDef.getParam("corpus");
        Corpus otherCorpus = FichothequeUtils.getCorpus(otherFichotheque, corpusName);
        if (otherCorpus == null) {
            return null;
        }
        return new CorpusExternalSource(bdfServer, fichothequeName, otherCorpus);
    }

}
