/* BdfServer - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.externalsource;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.externalsource.ExternalSourceType;
import fr.exemole.bdfserver.api.externalsource.FichothequeSharing;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;


/**
 *
 * @author Vincent Calame
 */
public class CoreExternalSourceCatalog {

    public final static String FICHOTHEQUECORPUS_TYPENAME = "fichothequecorpus";
    public final static String WIKIDATA_TYPENAME = "wikidata";
    public final static String DATAUI_FICHES_TYPENAME = "fiches";
    public final static ExternalSourceType FICHOTHEQUECORPUS = ExternalSourceTypeBuilder.init(FICHOTHEQUECORPUS_TYPENAME)
            .setL10nObject("_ title.external.fichothequecorpus")
            .addParam("fichotheque", "_ label.external.fichotheque")
            .addParam("corpus", "_ label.external.fichothequecorpus_corpus")
            .toExternalSourceType();
    public final static ExternalSourceType WIKIDATA = ExternalSourceTypeBuilder.init(WIKIDATA_TYPENAME)
            .setL10nObject("_ title.external.wikidata")
            .toExternalSourceType();
    public final static ExternalSourceType DATAUI_FICHES = ExternalSourceTypeBuilder.init(DATAUI_FICHES_TYPENAME)
            .setL10nObject("_ label.external.dataui_fiches")
            .addParam("fichotheque", "_ label.external.fichotheque")
            .addParam("matchingthesaurus", "_ label.external.matchingthesaurus")
            .addParam("corpus", "_ label.external.fiches_corpus")
            .toExternalSourceType();
    private final BdfServer bdfServer;
    private final boolean withFichothequeSharing;
    private final FichothequeSharing fichothequeSharing;

    public CoreExternalSourceCatalog(BdfServer bdfServer) {
        this.bdfServer = bdfServer;
        this.fichothequeSharing = (FichothequeSharing) bdfServer.getContextObject(BdfServerConstants.FICHOTHEQUESHARING_CONTEXTOBJECT);
        this.withFichothequeSharing = (fichothequeSharing != null);
    }

    public List<ExternalSourceType> getAvalaibleTypeList(Thesaurus thesaurus) {
        List<ExternalSourceType> result = new ArrayList<ExternalSourceType>();
        short thesaurusType = thesaurus.getThesaurusMetadata().getThesaurusType();
        if (thesaurusType == ThesaurusMetadata.BABELIEN_TYPE) {
            if (withFichothequeSharing) {
                result.add(FICHOTHEQUECORPUS);
            }
        } else if (thesaurusType == ThesaurusMetadata.IDALPHA_TYPE) {
            result.add(WIKIDATA);
        }
        return result;
    }

    public List<ExternalSourceType> getDataUiAvalaibleTypeList() {
        List<ExternalSourceType> result = new ArrayList<ExternalSourceType>();
        result.add(DATAUI_FICHES);
        return result;
    }

    public static ExternalSourceType getExternalSourceType(String typeName) {
        switch (typeName) {
            case FICHOTHEQUECORPUS_TYPENAME:
                return FICHOTHEQUECORPUS;
            case WIKIDATA_TYPENAME:
                return WIKIDATA;
            case DATAUI_FICHES_TYPENAME:
                return DATAUI_FICHES;
            default:
                return null;
        }
    }

}
