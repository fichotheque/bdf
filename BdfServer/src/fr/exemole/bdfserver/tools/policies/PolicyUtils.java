/* BdfServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.policies;

import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.fichotheque.thesaurus.policies.DynamicEditPolicy;


/**
 *
 * @author Vincent Calame
 */
public final class PolicyUtils {

    private PolicyUtils() {

    }

    public static boolean isValid(Thesaurus thesaurus, DynamicEditPolicy policy) {
        short thesaurusType = thesaurus.getThesaurusMetadata().getThesaurusType();
        if (policy instanceof DynamicEditPolicy.None) {
            return true;
        } else if (policy instanceof DynamicEditPolicy.Allow) {
            return (thesaurusType == ThesaurusMetadata.BABELIEN_TYPE);
        } else if (policy instanceof DynamicEditPolicy.Check) {
            return (thesaurusType == ThesaurusMetadata.BABELIEN_TYPE);
        } else if (policy instanceof DynamicEditPolicy.Transfer) {
            switch (thesaurus.getThesaurusMetadata().getThesaurusType()) {
                case ThesaurusMetadata.BABELIEN_TYPE:
                case ThesaurusMetadata.MULTI_TYPE:
                    return true;
                default:
                    return false;
            }
        } else if (policy instanceof DynamicEditPolicy.External) {
            return true;
        } else {
            return true;
        }
    }

}
