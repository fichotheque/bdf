/* BdfServer - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.policies.dom;

import fr.exemole.bdfserver.tools.policies.UserAllowBuilder;
import java.util.function.Consumer;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class UserAllowDOMReader {

    private final UserAllowBuilder userAllowBuilder;
    private final MessageHandler messageHandler;

    public UserAllowDOMReader(UserAllowBuilder userAllowBuilder, MessageHandler messageHandler) {
        this.userAllowBuilder = userAllowBuilder;
        this.messageHandler = messageHandler;
    }

    public void readUserAllow(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("data")) {
                userAllowBuilder.setDataChangeAllowed(true);
            } else if (tagName.equals("password")) {
                userAllowBuilder.setPasswordChangeAllowed(true);
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }

}
