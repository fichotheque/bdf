/* BdfServer - Copyright (c) 2014-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.policies;

import fr.exemole.bdfserver.api.policies.UserAllow;


/**
 *
 * @author Vincent Calame
 */
public class UserAllowBuilder {

    private boolean dataChangeAllowed;
    private boolean passwordChangeAllowed;

    public UserAllowBuilder() {
    }

    public UserAllowBuilder setDataChangeAllowed(boolean dataChangeAllowed) {
        this.dataChangeAllowed = dataChangeAllowed;
        return this;
    }

    public UserAllowBuilder setPasswordChangeAllowed(boolean passwordChangeAllowed) {
        this.passwordChangeAllowed = passwordChangeAllowed;
        return this;
    }

    public UserAllow toUserAllow() {
        return new InternalUserAllow(dataChangeAllowed, passwordChangeAllowed);
    }

    public static UserAllowBuilder init() {
        return new UserAllowBuilder();
    }


    private static class InternalUserAllow implements UserAllow {

        private final boolean dataChangeAllowed;
        private final boolean passwordChangeAllowed;

        public InternalUserAllow(boolean dataChangeAllowed, boolean passwordChangeAllowed) {
            this.dataChangeAllowed = dataChangeAllowed;
            this.passwordChangeAllowed = passwordChangeAllowed;
        }

        @Override
        public boolean isDataChangeAllowed() {
            return dataChangeAllowed;
        }

        @Override
        public boolean isPasswordChangeAllowed() {
            return passwordChangeAllowed;
        }

    }

}
