/* BdfServer - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.roles;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.instruction.PermissionException;
import fr.exemole.bdfserver.api.roles.SphereSupervisor;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.util.Collections;
import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Document;
import net.fichotheque.album.Illustration;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.permission.PermissionEntry;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.tools.permission.ListFilterEngine;


/**
 *
 * @author Vincent Calame
 */
public final class PermissionCheck {

    private PermissionCheck() {

    }

    public static void checkAdmin(PermissionSummary permissionSummary, String actionName) throws PermissionException {
        if (!permissionSummary.canDo(actionName)) {
            throw BdfErrors.permission("_ error.permission.admin");
        }
    }

    public static void checkSubsetAdmin(PermissionSummary permissionSummary, Subset subset) throws PermissionException {
        if (!permissionSummary.isSubsetAdmin(subset.getSubsetKey())) {
            throw BdfErrors.permission("_ error.permission.admin");
        }
    }

    public static void checkSubsetAccess(PermissionSummary permissionSummary, Subset subset) throws PermissionException {
        if (!permissionSummary.hasAccess(subset)) {
            throw BdfErrors.permission("_ error.permission.read", subset.getSubsetKeyString());
        }
    }

    public static void checkWrite(PermissionSummary permissionSummary, Illustration illustration) throws PermissionException {
        List<PermissionEntry> list = ListFilterEngine.filter(illustration.getAlbum(), Collections.singletonList((SubsetItem) illustration), permissionSummary);
        if ((list.isEmpty()) || (!list.get(0).isEditable())) {
            throw BdfErrors.permission("_ error.permission.write", illustration.getSubsetKey() + "/" + illustration.getId());
        }
    }

    public static void checkWrite(PermissionSummary permissionSummary, Document document) throws PermissionException {
        List<PermissionEntry> list = ListFilterEngine.filter(document.getAddenda(), Collections.singletonList((SubsetItem) document), permissionSummary);
        if ((list.isEmpty()) || (!list.get(0).isEditable())) {
            throw BdfErrors.permission("_ error.permission.write", document.getSubsetKey() + "/" + document.getId());
        }
    }

    public static void checkFicheCreate(PermissionSummary permissionSummary, Corpus corpus) throws PermissionException {
        SubsetKey subsetKey = corpus.getSubsetKey();
        if (!permissionSummary.canCreate(subsetKey)) {
            throw BdfErrors.permission("_ error.permission.createfiche", subsetKey.getKeyString());
        }
    }

    public static void checkWrite(PermissionSummary permissionSummary, FicheMeta ficheMeta) throws PermissionException {
        if (!permissionSummary.canWrite(ficheMeta)) {
            throw BdfErrors.permission("_ error.permission.write", ficheMeta.getGlobalId());
        }
    }

    public static void checkRead(PermissionSummary permissionSummary, FicheMeta ficheMeta) throws PermissionException {
        if (!permissionSummary.canRead(ficheMeta)) {
            throw BdfErrors.permission("_ error.permission.readfiche", ficheMeta.getGlobalId());
        }
    }

    public static boolean isAllowedBySphereSupervisor(BdfServer bdfServer, BdfUser bdfUser, String sphereName) {
        SphereSupervisor sphereSupervisor = (SphereSupervisor) bdfServer.getContextObject(BdfServerConstants.SPHERESUPERVISOR_CONTEXTOBJECT);
        if (sphereSupervisor != null) {
            if (sphereSupervisor.allowCoreChange(bdfUser, sphereName)) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static void checkSphereSupervisor(BdfServer bdfServer, BdfUser bdfUser, String sphereName) throws PermissionException {
        SphereSupervisor sphereSupervisor = (SphereSupervisor) bdfServer.getContextObject(BdfServerConstants.SPHERESUPERVISOR_CONTEXTOBJECT);
        if (sphereSupervisor != null) {
            if (!sphereSupervisor.allowCoreChange(bdfUser, sphereName)) {
                throw BdfErrors.permission("_ error.permission.spheresupervisor");
            }
        }
    }

}
