/* BdfServer - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.roles.dom;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurListDOMReader {

    private final MessageHandler messageHandler;
    private final Fichotheque fichotheque;

    public RedacteurListDOMReader(MessageHandler messageHandler, Fichotheque fichotheque) {
        this.messageHandler = messageHandler;
        this.fichotheque = fichotheque;
    }

    public List<Redacteur> readRedacteurList(Element element) {
        Set<Redacteur> set = new LinkedHashSet<Redacteur>();
        DOMUtils.readChildren(element, new RootConsumer(set));
        return new ArrayList<Redacteur>(set);
    }


    private class RootConsumer implements Consumer<Element> {

        private final Set<Redacteur> set;

        private RootConsumer(Set<Redacteur> set) {
            this.set = set;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "redacteur":
                    Redacteur redacteur = getRedacteur(element);
                    if (redacteur != null) {
                        set.add(redacteur);
                    }
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }

    private Redacteur getRedacteur(Element element) {
        String sphereString = element.getAttribute("sphere");
        try {
            SubsetKey sphereKey = SubsetKey.parse(SubsetKey.CATEGORY_SPHERE, sphereString);
            Sphere sphere = (Sphere) fichotheque.getSubset(sphereKey);
            if (sphere != null) {
                String idString = element.getAttribute("id");
                if (idString.length() == 0) {
                    idString = element.getAttribute("idsph");
                }
                if (idString.length() > 0) {
                    try {
                        int id = Integer.parseInt(idString);
                        return sphere.getRedacteurById(id);
                    } catch (NumberFormatException nfe) {
                    }
                } else {
                    String loginString = element.getAttribute("login");
                    if (loginString.length() > 0) {
                        return sphere.getRedacteurByLogin(loginString);
                    }
                }
            }
        } catch (ParseException pe) {
        }
        return null;
    }

}
