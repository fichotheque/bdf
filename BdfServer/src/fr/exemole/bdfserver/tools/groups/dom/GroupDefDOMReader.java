/* BdfServer - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.groups.dom;

import fr.exemole.bdfserver.tools.groups.GroupDefBuilder;
import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class GroupDefDOMReader {

    private final GroupDefBuilder groupDefBuilder;
    private final MessageHandler messageHandler;

    public GroupDefDOMReader(GroupDefBuilder groupDefBuilder, MessageHandler messageHandler) {
        this.groupDefBuilder = groupDefBuilder;
        this.messageHandler = messageHandler;
    }

    public GroupDefDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static GroupDefDOMReader init(GroupDefBuilder groupDefBuilder, MessageHandler messageHandler) {
        return new GroupDefDOMReader(groupDefBuilder, messageHandler);
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "label":
                case "lib": {
                    try {
                        LabelUtils.readLabel(element, groupDefBuilder);
                    } catch (ParseException ile) {
                        DomMessages.wrongLangAttribute(messageHandler, tagName, element.getAttribute("xml:lang"));
                    }
                    break;
                }
                case "attr":
                    AttributeUtils.readAttrElement(groupDefBuilder.getAttributesBuilder(), element);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }


}
