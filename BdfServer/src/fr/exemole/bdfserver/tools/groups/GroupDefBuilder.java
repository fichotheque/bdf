/* BdfServer - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.groups;

import fr.exemole.bdfserver.api.groups.GroupDef;
import java.text.ParseException;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public class GroupDefBuilder extends DefBuilder {

    private final String name;

    public GroupDefBuilder(String name) {
        try {
            GroupDef.checkGroupName(name);
        } catch (ParseException pe) {
            throw new IllegalArgumentException("Not a  valid group name: " + name);
        }
        this.name = name;
    }


    public GroupDef toGroupDef() {
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        return new InternalGroupDef(name, titleLabels, attributes);
    }

    public static GroupDefBuilder init(String name) {
        return new GroupDefBuilder(name);
    }


    private static class InternalGroupDef implements GroupDef {

        private final String name;
        private final Labels titleLabels;
        private final Attributes attributes;

        private InternalGroupDef(String name,
                Labels titleLabels, Attributes attributes) {
            this.name = name;
            this.titleLabels = titleLabels;
            this.attributes = attributes;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

    }

}
