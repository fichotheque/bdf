/* BdfServer - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.configuration;

import java.io.File;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class Target {

    private final File file;
    private final String url;

    public Target(File file, String url) {
        if (file == null) {
            throw new IllegalArgumentException("file is null");
        }
        this.file = file;
        url = StringUtils.nullTrim(url);
        if ((!url.isEmpty()) && (file.isDirectory()) && (!url.endsWith("/"))) {
            url = url + "/";
        }
        this.url = url;
    }

    public boolean isDirectoryTarget() {
        return file.isDirectory();
    }

    /**
     * Non nul
     *
     * @return
     */
    public File getFile() {
        return file;
    }

    /**
     * Non nul
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

}
