/* BdfServer - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.configuration;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import java.util.LinkedHashSet;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;


/**
 *
 * @author Vincent Calame
 */
public class LangConfigurationBuilder {

    private final Set<Lang> workingSet = new LinkedHashSet<Lang>();
    private final Set<Lang> supplementarySet = new LinkedHashSet<Lang>();
    private boolean allLanguages = false;
    private boolean withNonlatin = false;
    private boolean withoutSurnameFirst = false;

    public LangConfigurationBuilder() {
    }

    public LangConfigurationBuilder addWorkingLang(Lang workingLang) {
        workingSet.add(workingLang);
        supplementarySet.remove(workingLang);
        return this;
    }

    public LangConfigurationBuilder addSupplementaryLang(Lang supplementaryLang) {
        if (!workingSet.contains(supplementaryLang)) {
            supplementarySet.add(supplementaryLang);
        }
        return this;
    }

    public LangConfigurationBuilder setAllLanguages(boolean allLanguages) {
        this.allLanguages = allLanguages;
        return this;
    }

    public LangConfigurationBuilder setWithNonlatin(boolean withNonlatin) {
        this.withNonlatin = withNonlatin;
        return this;
    }

    public LangConfigurationBuilder setWithoutSurnameFirst(boolean withoutSurnameFirst) {
        this.withoutSurnameFirst = withoutSurnameFirst;
        return this;
    }

    public LangConfiguration toLangConfiguration() {
        Langs workingLangs;
        if (workingSet.isEmpty()) {
            workingLangs = LangsUtils.wrap(Lang.build("fr"));
        } else {
            workingLangs = LangsUtils.fromCollection(workingSet);
        }
        Langs supplementaryLangs = LangsUtils.fromCollection(supplementarySet);
        return new InternalLangConfiguration(workingLangs, supplementaryLangs, allLanguages, withNonlatin, withoutSurnameFirst);
    }

    public static LangConfigurationBuilder init() {
        return new LangConfigurationBuilder();
    }


    private static class InternalLangConfiguration implements LangConfiguration {

        private final Langs workingLangs;
        private final Langs supplementaryLangs;
        private final boolean allLanguages;
        private final boolean withNonlatin;
        private final boolean withoutSurnameFirst;

        private InternalLangConfiguration(Langs workingLangs, Langs supplementaryLangs, boolean allLanguages, boolean withNonlatin, boolean withoutSurnameFirst) {
            this.workingLangs = workingLangs;
            this.supplementaryLangs = supplementaryLangs;
            this.allLanguages = allLanguages;
            this.withNonlatin = withNonlatin;
            this.withoutSurnameFirst = withoutSurnameFirst;
        }

        @Override
        public Langs getWorkingLangs() {
            return workingLangs;
        }

        @Override
        public Langs getSupplementaryLangs() {
            return supplementaryLangs;
        }

        @Override
        public boolean isAllLanguages() {
            return allLanguages;
        }

        @Override
        public boolean isWithNonlatin() {
            return withNonlatin;
        }

        @Override
        public boolean isWithoutSurnameFirst() {
            return withoutSurnameFirst;
        }

    }

}
