/* BdfServer - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.configuration.dom;

import fr.exemole.bdfserver.tools.configuration.LangConfigurationBuilder;
import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class LangConfigurationDOMReader {

    private final LangConfigurationBuilder langConfigurationBuilder;
    private final MessageHandler messageHandler;

    public LangConfigurationDOMReader(LangConfigurationBuilder langConfigurationBuilder, MessageHandler messageHandler) {
        this.langConfigurationBuilder = langConfigurationBuilder;
        this.messageHandler = messageHandler;
    }

    public void readLangConfiguration(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("all-languages")) {
                langConfigurationBuilder.setAllLanguages(true);
            } else if ((tagName.equals("with-nonlatin")) || (tagName.equals("with-original-name"))) {
                langConfigurationBuilder.setWithNonlatin(true);
            } else if (tagName.equals("working-lang")) {
                try {
                    Lang lang = Lang.parse(element.getAttribute("lang"));
                    langConfigurationBuilder.addWorkingLang(lang);
                } catch (ParseException pe) {
                }
            } else if (tagName.equals("supplementary-lang")) {
                try {
                    Lang lang = Lang.parse(element.getAttribute("lang"));
                    langConfigurationBuilder.addSupplementaryLang(lang);
                } catch (ParseException pe) {
                }
            } else if ((tagName.equals("without-surname-first")) || (tagName.equals("without-nom-avant"))) {
                langConfigurationBuilder.setWithoutSurnameFirst(true);
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }

}
