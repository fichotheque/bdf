/* BdfServer - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.storage;

import fr.exemole.bdfserver.api.storage.BalayageStorage;
import fr.exemole.bdfserver.api.storage.StorageContent;
import java.util.List;
import net.fichotheque.exportation.balayage.BalayageDef;


/**
 *
 * @author Vincent Calame
 */
public class BalayageStorageUnitBuilder extends StorageUnitBuilder {

    private BalayageDef balayageDef;

    public BalayageStorageUnitBuilder() {
        super(true);
    }

    public BalayageStorageUnitBuilder setBalayageDef(BalayageDef balayageDef) {
        if (balayageDef == null) {
            throw new IllegalStateException("balayageDef is null");
        }
        this.balayageDef = balayageDef;
        return this;
    }

    public BalayageStorage.Unit toBalayageStorageUnit() {
        if (balayageDef == null) {
            throw new IllegalStateException("tableExportDef is null");
        }
        return new InternalBalayageStorageUnit(balayageDef, toStorageContentList());
    }


    private static class InternalBalayageStorageUnit implements BalayageStorage.Unit {

        private final BalayageDef balayageDef;
        private final List<StorageContent> storageContentList;

        private InternalBalayageStorageUnit(BalayageDef balayageDef, List<StorageContent> storageContentList) {
            this.balayageDef = balayageDef;
            this.storageContentList = storageContentList;
        }

        @Override
        public BalayageDef getBalayageDef() {
            return balayageDef;
        }

        @Override
        public List<StorageContent> getStorageContentList() {
            return storageContentList;
        }

    }

}
