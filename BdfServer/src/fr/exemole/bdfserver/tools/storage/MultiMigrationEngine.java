/* BdfServer - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.storage;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.storage.MigrationEngine;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class MultiMigrationEngine implements MigrationEngine {

    private List<MigrationEngine> migrationEngineList = new ArrayList<MigrationEngine>();

    public MultiMigrationEngine() {
    }

    @Override
    public void afterInitRun(BdfServer bdfServer) {
        for (MigrationEngine migrationEngine : migrationEngineList) {
            migrationEngine.afterInitRun(bdfServer);
        }
    }

    public void addMigrationEngine(MigrationEngine migrationEngine) {
        migrationEngineList.add(migrationEngine);
    }

}
