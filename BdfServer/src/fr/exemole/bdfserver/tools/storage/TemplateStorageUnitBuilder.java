/* BdfServer - Copyright (c) 2022-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.storage;

import fr.exemole.bdfserver.api.storage.StorageContent;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import java.util.List;
import net.fichotheque.exportation.transformation.TemplateDef;


/**
 *
 * @author Vincent Calame
 */
public class TemplateStorageUnitBuilder extends StorageUnitBuilder {

    private String type;
    private TemplateDef templateDef;

    public TemplateStorageUnitBuilder() {
        super(false);
    }

    public TemplateStorageUnitBuilder setType(String type) {
        if (type == null) {
            throw new IllegalArgumentException("type is null");
        }
        this.type = type;
        return this;
    }

    public TemplateStorageUnitBuilder setTemplateDef(TemplateDef templateDef) {
        if (templateDef == null) {
            throw new IllegalArgumentException("templateDef is null");
        }
        this.templateDef = templateDef;
        return this;
    }

    public TemplateStorage.Unit toTemplateStorageUnit() {
        if (type == null) {
            throw new IllegalStateException("init is null");
        }
        if (templateDef == null) {
            throw new IllegalStateException("templateDef is null");
        }
        return new InternalTemplateStorageUnit(type, templateDef, toStorageContentList());
    }

    public static TemplateStorageUnitBuilder init() {
        return new TemplateStorageUnitBuilder();
    }

    public static TemplateStorageUnitBuilder init(String type, TemplateDef templateDef) {
        return TemplateStorageUnitBuilder.init().setType(type).setTemplateDef(templateDef);
    }


    private static class InternalTemplateStorageUnit implements TemplateStorage.Unit {

        private final String type;
        private final TemplateDef templateDef;
        private final List<StorageContent> storageContentList;

        private InternalTemplateStorageUnit(String type, TemplateDef templateDef, List<StorageContent> storageContentList) {
            this.type = type;
            this.templateDef = templateDef;
            this.storageContentList = storageContentList;
        }

        @Override
        public TemplateDef getTemplateDef() {
            return templateDef;
        }

        @Override
        public String getType() {
            return type;
        }

        @Override
        public List<StorageContent> getStorageContentList() {
            return storageContentList;
        }

    }

}
