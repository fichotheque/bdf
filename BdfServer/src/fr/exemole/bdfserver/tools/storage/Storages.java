/* BdfServer - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.storage;

import fr.exemole.bdfserver.api.storage.AccessStorage;
import fr.exemole.bdfserver.api.storage.BalayageStorage;
import fr.exemole.bdfserver.api.storage.BdfExtensionStorage;
import fr.exemole.bdfserver.api.storage.BdfUserStorage;
import fr.exemole.bdfserver.api.storage.ConfigurationStorage;
import fr.exemole.bdfserver.api.storage.GroupStorage;
import fr.exemole.bdfserver.api.storage.MigrationEngine;
import fr.exemole.bdfserver.api.storage.PasswordStorage;
import fr.exemole.bdfserver.api.storage.PolicyStorage;
import fr.exemole.bdfserver.api.storage.RedacteurListStorage;
import fr.exemole.bdfserver.api.storage.RoleStorage;
import fr.exemole.bdfserver.api.storage.ScrutariExportStorage;
import fr.exemole.bdfserver.api.storage.SelectionStorage;
import fr.exemole.bdfserver.api.storage.SqlExportStorage;
import fr.exemole.bdfserver.api.storage.StorageCheck;
import fr.exemole.bdfserver.api.storage.StorageRoot;
import fr.exemole.bdfserver.api.storage.TableExportStorage;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import fr.exemole.bdfserver.api.storage.TreeStorage;
import fr.exemole.bdfserver.api.storage.UiStorage;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import net.fichotheque.FichothequeEditorProvider;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.logging.MessageLog;


/**
 *
 * @author Vincent Calame
 */
public class Storages {

    private final BdfServerDirs dirs;
    private StorageCheck bdfStorageCheck;
    private AccessStorage accessStorage;
    private BalayageStorage balayageStorage;
    private TableExportStorage tableExportStorage;
    private TemplateStorage templateStorage;
    private MessageLog initMessageLog;
    private ResourceStorages resourceStorages;
    private StorageRoot outputStorage;
    private StorageRoot cacheStorage;
    private BdfExtensionStorage bdfExtensionStorage;
    private BdfUserStorage bdfUserStorage;
    private PasswordStorage passwordStorage;
    private SelectionStorage selectionStorage;
    private SqlExportStorage sqlExportStorage;
    private ScrutariExportStorage scrutariExportStorage;
    private ConfigurationStorage configurationStorage;
    private RoleStorage roleStorage;
    private GroupStorage groupStorage;
    private TreeStorage treeStorage;
    private UiStorage uiStorage;
    private PolicyStorage policyStorage;
    private RedacteurListStorage redacteurListStorage;
    private FichothequeEditorProvider fichothequeEditorProvider;
    private final MultiMigrationEngine multiMigrationEngine = new MultiMigrationEngine();

    public Storages(BdfServerDirs dirs) {
        this.dirs = dirs;
    }

    public BdfServerDirs getBdfServerDirs() {
        return dirs;
    }

    public AccessStorage getAccessStorage() {
        return accessStorage;
    }

    public Storages setAccessStorage(AccessStorage accessStorage) {
        this.accessStorage = accessStorage;
        return this;
    }

    public BalayageStorage getBalayageStorage() {
        return balayageStorage;
    }

    public Storages setBalayageStorage(BalayageStorage balayageStorage) {
        this.balayageStorage = balayageStorage;
        return this;
    }

    public TableExportStorage getTableExportStorage() {
        return tableExportStorage;
    }

    public Storages setTableExportStorage(TableExportStorage tableExportStorage) {
        this.tableExportStorage = tableExportStorage;
        return this;
    }

    public TemplateStorage getTemplateStorage() {
        return templateStorage;
    }

    public Storages setTemplateStorage(TemplateStorage templateStorage) {
        this.templateStorage = templateStorage;
        return this;
    }

    public MessageLog getInitMessageLog() {
        return initMessageLog;
    }

    public Storages setInitMessageLog(MessageLog initMessageLog) {
        this.initMessageLog = initMessageLog;
        return this;
    }

    public ResourceStorages getResourceStorages() {
        return resourceStorages;
    }

    public Storages setResourceStorages(ResourceStorages resourceStorages) {
        this.resourceStorages = resourceStorages;
        return this;
    }

    public StorageRoot getOutputStorage() {
        return outputStorage;
    }

    public Storages setOutputStorage(StorageRoot outputStorage) {
        this.outputStorage = outputStorage;
        return this;
    }

    public StorageRoot getCacheStorage() {
        return cacheStorage;
    }

    public Storages setCacheStorage(StorageRoot cacheStorage) {
        this.cacheStorage = cacheStorage;
        return this;
    }

    public BdfExtensionStorage getBdfExtensionStorage() {
        return bdfExtensionStorage;
    }

    public Storages setBdfExtensionStorage(BdfExtensionStorage bdfExtensionStorage) {
        this.bdfExtensionStorage = bdfExtensionStorage;
        return this;
    }

    public StorageCheck getStorageCheck() {
        return bdfStorageCheck;
    }

    public Storages setStorageCheck(StorageCheck bdfStorageCheck) {
        this.bdfStorageCheck = bdfStorageCheck;
        return this;
    }

    public BdfUserStorage getBdfUserStorage() {
        return bdfUserStorage;
    }

    public Storages setBdfUserStorage(BdfUserStorage bdfUserStorage) {
        this.bdfUserStorage = bdfUserStorage;
        return this;
    }

    public PasswordStorage getPasswordStorage() {
        return passwordStorage;
    }

    public Storages setPasswordStorage(PasswordStorage passwordStorage) {
        this.passwordStorage = passwordStorage;
        return this;
    }

    public SelectionStorage getSelectionStorage() {
        return selectionStorage;
    }

    public Storages setSelectionStorage(SelectionStorage selectionStorage) {
        this.selectionStorage = selectionStorage;
        return this;
    }

    public SqlExportStorage getSqlExportStorage() {
        return sqlExportStorage;
    }

    public Storages setSqlExportStorage(SqlExportStorage sqlExportStorage) {
        this.sqlExportStorage = sqlExportStorage;
        return this;
    }

    public ScrutariExportStorage getScrutariExportStorage() {
        return scrutariExportStorage;
    }

    public Storages setScrutariExportStorage(ScrutariExportStorage scrutariExportStorage) {
        this.scrutariExportStorage = scrutariExportStorage;
        return this;
    }

    public RoleStorage getRoleStorage() {
        return roleStorage;
    }

    public Storages setRoleStorage(RoleStorage roleStorage) {
        this.roleStorage = roleStorage;
        return this;
    }

    public GroupStorage getGroupStorage() {
        return groupStorage;
    }

    public Storages setGroupStorage(GroupStorage groupStorage) {
        this.groupStorage = groupStorage;
        return this;
    }

    public TreeStorage getTreeStorage() {
        return treeStorage;
    }

    public Storages setTreeStorage(TreeStorage treeStorage) {
        this.treeStorage = treeStorage;
        return this;
    }

    public PolicyStorage getPolicyStorage() {
        return policyStorage;
    }

    public Storages setPolicyStorage(PolicyStorage policyStorage) {
        this.policyStorage = policyStorage;
        return this;
    }

    public RedacteurListStorage getRedacteurList() {
        return redacteurListStorage;
    }

    public Storages setRedacteurListStorage(RedacteurListStorage redacteurListStorage) {
        this.redacteurListStorage = redacteurListStorage;
        return this;
    }

    public FichothequeEditorProvider getFichothequeEditorProvider() {
        return fichothequeEditorProvider;
    }

    public Storages setFichothequeEditorProvider(FichothequeEditorProvider fichothequeEditorProvider) {
        this.fichothequeEditorProvider = fichothequeEditorProvider;
        return this;
    }

    public MigrationEngine getMigrationEngine() {
        return multiMigrationEngine;
    }

    public Storages addMigrationEngine(MigrationEngine migrationEngine) {
        multiMigrationEngine.addMigrationEngine(migrationEngine);
        return this;
    }

    public ConfigurationStorage getConfigurationStorage() {
        return configurationStorage;
    }

    public Storages setConfigurationStorage(ConfigurationStorage configurationStorage) {
        this.configurationStorage = configurationStorage;
        return this;
    }

    public UiStorage getUiStorage() {
        return uiStorage;
    }

    public Storages setUiStorage(UiStorage uiStorage) {
        this.uiStorage = uiStorage;
        return this;
    }

}
