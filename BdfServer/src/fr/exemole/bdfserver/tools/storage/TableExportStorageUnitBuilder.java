/* BdfServer - Copyright (c) 2016-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.storage;

import fr.exemole.bdfserver.api.storage.StorageContent;
import fr.exemole.bdfserver.api.storage.TableExportStorage;
import java.util.List;
import net.fichotheque.exportation.table.TableExportDef;


/**
 *
 * @author Vincent Calame
 */
public class TableExportStorageUnitBuilder extends StorageUnitBuilder {

    private TableExportDef tableExportDef;

    public TableExportStorageUnitBuilder() {
        super(true);
    }

    public TableExportStorageUnitBuilder setTableExportDef(TableExportDef tableExportDef) {
        this.tableExportDef = tableExportDef;
        return this;
    }

    public TableExportStorage.Unit toTableExportStorageUnit() {
        if (tableExportDef == null) {
            throw new IllegalStateException("tableExportDef is null");
        }
        return new InternalTableExportStorageUnit(tableExportDef, toStorageContentList());
    }

    public static TableExportStorageUnitBuilder init() {
        return new TableExportStorageUnitBuilder();
    }


    private static class InternalTableExportStorageUnit implements TableExportStorage.Unit {

        private final TableExportDef tableExportDef;
        private final List<StorageContent> storageContentList;

        public InternalTableExportStorageUnit(TableExportDef tableExportDef, List<StorageContent> storageContentList) {
            this.tableExportDef = tableExportDef;
            this.storageContentList = storageContentList;
        }

        @Override
        public TableExportDef getTableExportDef() {
            return tableExportDef;
        }

        @Override
        public List<StorageContent> getStorageContentList() {
            return storageContentList;
        }

    }

}
