/* BdfServer - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.docstream;

import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.album.Illustration;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.io.DocStream;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationDocStream implements DocStream {

    private final Illustration illustration;
    private final AlbumDim albumDim;
    private final short specialDim;

    public IllustrationDocStream(Illustration illustration, AlbumDim albumDim) {
        this.illustration = illustration;
        this.albumDim = albumDim;
        this.specialDim = 0;
    }

    public IllustrationDocStream(Illustration illustration, short specialDim) {
        this.illustration = illustration;
        this.albumDim = null;
        this.specialDim = specialDim;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        if (albumDim != null) {
            return illustration.getInputStream(albumDim);
        } else {
            return illustration.getInputStream(specialDim);
        }
    }

    @Override
    public String getMimeType() {
        return AlbumUtils.getMimeType(illustration.getFormatType());
    }

    @Override
    public String getCharset() {
        return null;
    }

    @Override
    public int getLength() {
        return -1;
    }

    @Override
    public long getLastModified() {
        return -1;
    }

}
