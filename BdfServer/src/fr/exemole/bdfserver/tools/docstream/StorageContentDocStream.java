/* BdfServer - Copyright (c) 2019-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.docstream;

import fr.exemole.bdfserver.api.storage.StorageContent;
import java.io.IOException;
import java.io.InputStream;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.mimetype.MimeTypeUtils;


/**
 *
 * @author Vincent Calame
 */
public class StorageContentDocStream implements DocStream {

    private final StorageContent storageContent;
    private final String mimeType;
    private final String charset;

    public StorageContentDocStream(StorageContent storageContent, String mimeType) {
        this.storageContent = storageContent;
        this.mimeType = mimeType;
        this.charset = MimeTypeUtils.getDefaultCharset(mimeType);
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return storageContent.getInputStream();
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    @Override
    public String getCharset() {
        return charset;
    }

    @Override
    public int getLength() {
        return -1;
    }

    @Override
    public long getLastModified() {
        return -1;
    }

}
