/* BdfServer - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.docstream;

import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.addenda.Version;
import net.mapeadores.util.io.DocStream;


/**
 *
 * @author Vincent Calame
 */
public class VersionDocStream implements DocStream {

    private final Version version;
    private final String mimeType;
    private final String charset;

    public VersionDocStream(Version version, String mimeType, String charset) {
        this.version = version;
        this.mimeType = mimeType;
        this.charset = charset;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return version.getInputStream();
    }

    @Override
    public int getLength() {
        return (int) version.getFileLength().getValue();
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    @Override
    public String getCharset() {
        return charset;
    }

    @Override
    public long getLastModified() {
        return -1;
    }

}
