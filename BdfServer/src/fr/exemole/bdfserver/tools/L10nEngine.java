/* BdfServer - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.ui.components.IncludeUiBuilder;
import net.fichotheque.MetadataEditor;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public final class L10nEngine {

    private final Lang[] langArray;
    private final MessageLocalisation[] messageLocalisationArray;

    private L10nEngine(Lang[] langArray, MessageLocalisation[] messageLocalisationArray) {
        this.langArray = langArray;
        this.messageLocalisationArray = messageLocalisationArray;
    }

    public void populate(IncludeUiBuilder builder, String messageKey) {
        populate(builder, LocalisationUtils.toMessage(messageKey));
    }

    public void populate(IncludeUiBuilder builder, Message message) {
        int length = langArray.length;
        for (int i = 0; i < length; i++) {
            CleanedString labelString = CleanedString.newInstance(messageLocalisationArray[i].toString(message));
            if (labelString != null) {
                builder.getLabelChangeBuilder().putLabel(LabelUtils.toLabel(langArray[i], labelString));
            }
        }
    }

    public void populate(IncludeUiBuilder builder, String messageKey, String defaultLabelString, Labels... valueLabels) {
        int length = langArray.length;
        for (int i = 0; i < length; i++) {
            Lang lang = langArray[i];
            Message message = toMessage(messageKey, lang, defaultLabelString, valueLabels);
            CleanedString labelString = CleanedString.newInstance(messageLocalisationArray[i].toString(message));
            if (labelString != null) {
                builder.getLabelChangeBuilder().putLabel(LabelUtils.toLabel(lang, labelString));
            }
        }
    }

    public void populate(MetadataEditor metadataEditor, @Nullable String phraseName, String messageKey) {
        populate(metadataEditor, phraseName, LocalisationUtils.toMessage(messageKey));
    }

    public void populate(MetadataEditor metadataEditor, @Nullable String phraseName, Message message) {
        int length = langArray.length;
        for (int i = 0; i < length; i++) {
            CleanedString labelString = CleanedString.newInstance(messageLocalisationArray[i].toString(message));
            if (labelString != null) {
                metadataEditor.putLabel(phraseName, LabelUtils.toLabel(langArray[i], labelString));
            }
        }
    }

    public void populate(MetadataEditor metadataEditor, @Nullable String phraseName, String messageKey, String defaultLabelString, Labels... valueLabels) {
        int length = langArray.length;
        for (int i = 0; i < length; i++) {
            Lang lang = langArray[i];
            Message message = toMessage(messageKey, lang, defaultLabelString, valueLabels);
            CleanedString labelString = CleanedString.newInstance(messageLocalisationArray[i].toString(message));
            if (labelString != null) {
                metadataEditor.putLabel(phraseName, LabelUtils.toLabel(lang, labelString));
            }
        }
    }

    private Message toMessage(String messageKey, Lang lang, String defaultString, Labels[] valueLabelsArray) {
        if (valueLabelsArray == null) {
            return LocalisationUtils.toMessage(messageKey);
        }
        int length = valueLabelsArray.length;
        Object[] valueArray = new Object[length];
        for (int i = 0; i < length; i++) {
            Label label = valueLabelsArray[i].getLabel(lang);
            if (label != null) {
                valueArray[i] = label.getLabelString();
            } else {
                valueArray[i] = defaultString;
            }
        }
        return LocalisationUtils.toMessage(messageKey, valueArray);
    }

    public static L10nEngine init(BdfServer bdfServer) {
        Lang[] langArray = LangsUtils.toArray(bdfServer.getLangConfiguration().getWorkingLangs());
        MessageLocalisation[] messageLocalisationArray = ConfigurationUtils.getMessageLocalisationArray(bdfServer, langArray);
        return new L10nEngine(langArray, messageLocalisationArray);
    }

}
