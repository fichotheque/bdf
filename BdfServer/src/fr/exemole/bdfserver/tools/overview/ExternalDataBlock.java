/* BdfServer - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.overview;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.ui.DataUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.ui.components.DataUiBuilder;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.LineMessageException;


/**
 *
 * @author Vincent Calame
 */
class ExternalDataBlock extends OverviewBlock {

    private final DataUiBuilder builder;

    ExternalDataBlock(int lineNumber, DataUi dataUi) {
        super(lineNumber, dataUi);
        this.builder = DataUiBuilder.init(dataUi);
    }

    ExternalDataBlock(int lineNumber, String dataName, String type) {
        super(lineNumber, dataName);
        this.builder = new DataUiBuilder(dataName, type);
    }

    @Override
    public boolean addText(String name, Lang lang, String text) {
        if (!isValidText(name)) {
            return false;
        }
        builder.getLabelChangeBuilder().putLabel(lang, text);
        return true;
    }

    @Override
    public void put(int lineNumber, String key, String value) throws LineMessageException {
        switch (key) {
            case "type":
                break;
            default:
                throw new LineMessageException(lineNumber, OverviewEngine.SEVERE_FICHOTHEQUE, "_ error.unknown.conf.key", key);
        }
    }

    @Override
    public void put(int lineNumber, String key, String name, String value) throws LineMessageException {
        switch (key) {
            case "param":
                builder.getExternalSourceDefBuilder().addParam(name, value);
                return;
            default:
                throw new LineMessageException(lineNumber, OverviewEngine.SEVERE_FICHOTHEQUE, "_ error.unknown.conf.key", key);
        }
    }

    private boolean isValidText(String labelName) {
        switch (labelName) {
            case "label":
                return true;
            default:
                return false;
        }
    }

    @Override
    public AttributeChangeBuilder getAttributeChangeBuilder() {
        return builder.getAttributeChangeBuilder();
    }

    @Override
    public void firstPass(EditSession editSession, UiComponents uiComponents) {
        editSession.getBdfServerEditor().putComponentUi(uiComponents, builder.toDataUi());
    }

    @Override
    public void secondPass(EditSession editSession, UiComponents uiComponents) {

    }

}
