/* BdfServer - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.overview;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.ui.CommentUi;
import fr.exemole.bdfserver.api.ui.DataUi;
import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.api.ui.SpecialIncludeUi;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import java.util.List;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.include.ExtendedIncludeKey;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.LineMessageException;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public abstract class OverviewBlock {

    private final int lineNumber;
    private final String name;

    OverviewBlock(int lineNumber, UiComponent uiComponent) {
        this.lineNumber = lineNumber;
        this.name = uiComponent.getName();
    }

    OverviewBlock(int lineNumber, String name) {
        this.lineNumber = lineNumber;
        this.name = name;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String getName() {
        return name;
    }

    public void addAttribute(AttributeKey attributeKey, List<CleanedString> list) {
        AttributeChangeBuilder attributeChangeBuilder = getAttributeChangeBuilder();
        if (list.isEmpty()) {
            attributeChangeBuilder.putRemovedAttributeKey(attributeKey);
        } else {
            attributeChangeBuilder.appendValues(attributeKey, list);
        }
    }

    public abstract boolean addText(String name, Lang lang, String text);

    public abstract AttributeChangeBuilder getAttributeChangeBuilder();

    public abstract void firstPass(EditSession editSession, UiComponents uiComponents);

    public abstract void secondPass(EditSession editSession, UiComponents uiComponents);

    public abstract void put(int lineNumber, String key, String value) throws LineMessageException;

    public abstract void put(int lineNumber, String key, String name, String value) throws LineMessageException;

    public String checkStatus(int lineNumber, String value) throws LineMessageException {
        try {
            return BdfServerConstants.checkValidStatus(value);
        } catch (IllegalArgumentException iae) {
            throw new LineMessageException(lineNumber, OverviewEngine.SEVERE_FICHOTHEQUE, "_ error.unknown.parametervalue", "status", value);
        }
    }

    public Object parseOptionValue(String optionName, String optionValue) {
        return optionValue;
    }

    public static OverviewBlock fromUiComponent(int lineNumber, BdfServer bdfServer, UiComponent uiComponent, CorpusMetadata corpusMetadata) {
        if (uiComponent instanceof FieldUi) {
            return new FieldBlock(lineNumber, (FieldUi) uiComponent, corpusMetadata);
        }
        if (uiComponent instanceof SubsetIncludeUi) {
            return new SubsetIncludeBlock(lineNumber, (SubsetIncludeUi) uiComponent);
        }
        if (uiComponent instanceof SpecialIncludeUi) {
            return new SpecialIncludeBlock(lineNumber, (SpecialIncludeUi) uiComponent);
        }
        if (uiComponent instanceof DataUi) {
            return new ExternalDataBlock(lineNumber, (DataUi) uiComponent);
        }
        if (uiComponent instanceof CommentUi) {
            return new CommentBlock(lineNumber, bdfServer, (CommentUi) uiComponent);
        }
        throw new ClassCastException("Unknown implementation: " + uiComponent.getClass().getCanonicalName());
    }

    public static OverviewBlock metadata(int lineNumber, CorpusMetadata corpusMetadata) {
        return new MetadataBlock(lineNumber, corpusMetadata);
    }

    public static OverviewBlock fieldKey(int lineNumber, FieldKey fieldKey, short ficheItemType, CorpusMetadata corpusMetadata) {
        return new FieldBlock(lineNumber, fieldKey, ficheItemType, corpusMetadata);
    }

    public static OverviewBlock includeKey(int lineNumber, ExtendedIncludeKey includeKey) {
        return new SubsetIncludeBlock(lineNumber, includeKey);
    }

    public static OverviewBlock specialInclude(int lineNumber, String specialIncludeName) {
        return new SpecialIncludeBlock(lineNumber, specialIncludeName);
    }

    public static OverviewBlock externalData(int lineNumber, String dataName, String type) {
        return new ExternalDataBlock(lineNumber, dataName, type);
    }

    public static OverviewBlock comment(int lineNumber, BdfServer bdfServer, String commentName) {
        return new CommentBlock(lineNumber, bdfServer, commentName);
    }

}
