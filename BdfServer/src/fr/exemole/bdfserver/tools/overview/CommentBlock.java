/* BdfServer - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.overview;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.ui.CommentUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import fr.exemole.bdfserver.tools.ui.components.CommentUiBuilder;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.LineMessageException;


/**
 *
 * @author Vincent Calame
 */
class CommentBlock extends OverviewBlock {

    private final CommentUiBuilder builder;

    CommentBlock(int lineNumber, BdfServer bdfServer, CommentUi commentUi) {
        super(lineNumber, commentUi);
        this.builder = new CommentUiBuilder(bdfServer.getUiManager().getTrustedHtmlFactory(), commentUi);
    }

    CommentBlock(int lineNumber, BdfServer bdfServer, String commentName) {
        super(lineNumber, commentName);
        this.builder = new CommentUiBuilder(bdfServer.getUiManager().getTrustedHtmlFactory(), commentName);
    }

    @Override
    public boolean addText(String name, Lang lang, String text) {
        if (!isValidText(name)) {
            return false;
        }
        builder.putHtml(lang, text);
        return true;
    }

    private boolean isValidText(String labelName) {
        switch (labelName) {
            case "html":
                return true;
            default:
                return false;
        }
    }

    @Override
    public void put(int lineNumber, String key, String value) throws LineMessageException {
        switch (key) {
            case "location":
                builder.setLocation(UiUtils.maskStringToInt(value));
                break;
            default:
                throw new LineMessageException(lineNumber, OverviewEngine.SEVERE_FICHOTHEQUE, "_ error.unknown.conf.key", key);
        }
    }

    @Override
    public void put(int lineNumber, String key, String name, String value) throws LineMessageException {
        throw new LineMessageException(lineNumber, OverviewEngine.SEVERE_FICHOTHEQUE, "_ error.unknown.conf.key", key);
    }

    @Override
    public AttributeChangeBuilder getAttributeChangeBuilder() {
        return builder.getAttributeChangeBuilder();
    }

    @Override
    public void firstPass(EditSession editSession, UiComponents uiComponents) {
        editSession.getBdfServerEditor().putComponentUi(uiComponents, builder.toCommentUi());
    }

    @Override
    public void secondPass(EditSession editSession, UiComponents uiComponents) {

    }

}
