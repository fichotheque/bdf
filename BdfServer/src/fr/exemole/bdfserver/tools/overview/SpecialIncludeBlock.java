/* BdfServer - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.overview;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.ui.SpecialIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.ui.components.IncludeUiBuilder;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.LineMessageException;


/**
 *
 * @author Vincent Calame
 */
class SpecialIncludeBlock extends OverviewBlock {

    private final IncludeUiBuilder builder;

    SpecialIncludeBlock(int lineNumber, SpecialIncludeUi specialIncludeUi) {
        super(lineNumber, specialIncludeUi);
        this.builder = new IncludeUiBuilder(specialIncludeUi);
    }

    SpecialIncludeBlock(int lineNumber, String specialName) {
        super(lineNumber, specialName);
        this.builder = new IncludeUiBuilder(specialName);
    }

    @Override
    public boolean addText(String name, Lang lang, String text) {
        if (!isValidText(name)) {
            return false;
        }
        builder.getLabelChangeBuilder().putLabel(lang, text);
        return true;
    }

    private boolean isValidText(String labelName) {
        switch (labelName) {
            case "custom":
                return true;
            default:
                return false;
        }
    }

    @Override
    public void put(int lineNumber, String key, String value) throws LineMessageException {
        switch (key) {
            case "status":
                String status = checkStatus(lineNumber, value);
                builder.setStatus(status);
                break;
            default:
                throw new LineMessageException(lineNumber, OverviewEngine.SEVERE_FICHOTHEQUE, "_ error.unknown.conf.key", key);
        }
    }

    @Override
    public void put(int lineNumber, String key, String name, String value) throws LineMessageException {
        switch (key) {
            case "ui": {
                Object uiOptionObject = parseOptionValue(name, value);
                if (uiOptionObject != null) {
                    builder.putOption(name, uiOptionObject);
                }
                break;
            }
            default:
                throw new LineMessageException(lineNumber, OverviewEngine.SEVERE_FICHOTHEQUE, "_ error.unknown.conf.key", key);
        }
    }

    @Override
    public AttributeChangeBuilder getAttributeChangeBuilder() {
        return builder.getAttributeChangeBuilder();
    }

    @Override
    public void firstPass(EditSession editSession, UiComponents uiComponents) {
        editSession.getBdfServerEditor().putComponentUi(uiComponents, builder.toIncludeUi());
    }

    @Override
    public void secondPass(EditSession editSession, UiComponents uiComponents) {

    }

}
