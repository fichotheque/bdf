/* BdfServer - Copyright (c) 2014-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.subsettree;

import fr.exemole.bdfserver.api.subsettree.SubsetNode;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public class SubsetNodeBuilder {

    private final SubsetKey subsetKey;

    public SubsetNodeBuilder(SubsetKey subsetKey) {
        if (subsetKey == null) {
            throw new IllegalArgumentException("subsetKey is null");
        }
        this.subsetKey = subsetKey;
    }

    public SubsetNode toSubsetNode() {
        return new InternalSubsetNode(subsetKey);
    }

    public static SubsetNodeBuilder init(SubsetKey subsetKey) {
        return new SubsetNodeBuilder(subsetKey);
    }

    public static SubsetNode build(SubsetKey subsetKey) {
        if (subsetKey == null) {
            throw new IllegalArgumentException("subsetKey is null");
        }
        return new InternalSubsetNode(subsetKey);
    }


    private static class InternalSubsetNode implements SubsetNode {

        private final SubsetKey subsetKey;

        private InternalSubsetNode(SubsetKey subsetKey) {
            this.subsetKey = subsetKey;
        }

        @Override
        public SubsetKey getSubsetKey() {
            return subsetKey;
        }

    }

}
