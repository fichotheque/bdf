/* BdfServer - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.subsettree;

import fr.exemole.bdfserver.api.subsettree.CroisementSubsetNode;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.croisement.Croisements;


/**
 *
 * @author Vincent Calame
 */
public class CroisementSubsetNodeBuilder {

    private final Subset subset;
    private final Croisements croisements;

    public CroisementSubsetNodeBuilder(Subset subset, Croisements croisements) {
        if (subset == null) {
            throw new IllegalArgumentException("subset is null");
        }
        if (croisements == null) {
            throw new IllegalArgumentException("croisements is null");
        }
        this.subset = subset;
        this.croisements = croisements;
    }

    public CroisementSubsetNode toCroisementSubsetNode() {
        return new InternalCroisementSubsetNode(subset, croisements);
    }

    public CroisementSubsetNodeBuilder init(Subset subset, Croisements croisements) {
        return new CroisementSubsetNodeBuilder(subset, croisements);
    }

    public static CroisementSubsetNode build(Subset subset, Croisements croisements) {
        if (subset == null) {
            throw new IllegalArgumentException("subset is null");
        }
        if (croisements == null) {
            throw new IllegalArgumentException("croisements is null");
        }
        return new InternalCroisementSubsetNode(subset, croisements);
    }


    private static class InternalCroisementSubsetNode implements CroisementSubsetNode {

        private final Subset subset;
        private final Croisements croisements;

        private InternalCroisementSubsetNode(Subset subset, Croisements croisements) {
            this.subset = subset;
            this.croisements = croisements;
        }

        @Override
        public SubsetKey getSubsetKey() {
            return subset.getSubsetKey();
        }

        @Override
        public Subset getSubset() {
            return subset;
        }

        @Override
        public Croisements getCroisements() {
            return croisements;
        }

    }

}
