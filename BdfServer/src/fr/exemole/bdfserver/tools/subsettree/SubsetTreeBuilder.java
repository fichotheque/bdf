/* BdfServer - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.subsettree;

import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import java.util.Collection;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class SubsetTreeBuilder extends NodeListBuilder {

    private final boolean ignoreEmptyGroupNode;

    public SubsetTreeBuilder(boolean ignoreEmptyGroupNode) {
        this.ignoreEmptyGroupNode = ignoreEmptyGroupNode;
    }

    public boolean ignoreEmptyGroupNode() {
        return ignoreEmptyGroupNode;
    }

    public SubsetTree toSubsetTree() {
        return new InternalSubsetTree(toNodeList(ignoreEmptyGroupNode));
    }

    public static SubsetTreeBuilder init(boolean ignoreEmptyGroupNode) {
        return new SubsetTreeBuilder(ignoreEmptyGroupNode);
    }

    public static SubsetTree build(Collection<SubsetTree.Node> nodes) {
        int size = nodes.size();
        SubsetTree.Node[] array = nodes.toArray(new SubsetTree.Node[size]);
        return new InternalSubsetTree(TreeUtils.wrap(array));
    }


    private static class InternalSubsetTree implements SubsetTree {

        private final List<SubsetTree.Node> list;

        private InternalSubsetTree(List<SubsetTree.Node> list) {
            this.list = list;
        }

        @Override
        public List<SubsetTree.Node> getNodeList() {
            return list;
        }

    }

}
