/* BdfServer - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.subsettree;

import fr.exemole.bdfserver.api.subsettree.GroupNode;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class GroupNodeBuilder extends NodeListBuilder {

    private final String name;
    private boolean ignoreEmptyGroupNode;

    public GroupNodeBuilder(String name, boolean ignoreEmptyGroupNode) {
        this.name = name;
    }

    public boolean ignoreEmptyGroupNode() {
        return ignoreEmptyGroupNode;
    }

    public GroupNode toGroupNode() {
        return new InternalGroupNode(name, toNodeList(ignoreEmptyGroupNode));
    }


    private static class InternalGroupNode implements GroupNode {

        private final String name;
        private final List<SubsetTree.Node> list;

        private InternalGroupNode(String name, List<SubsetTree.Node> list) {
            this.name = name;
            this.list = list;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<SubsetTree.Node> getSubnodeList() {
            return list;
        }

    }

}
