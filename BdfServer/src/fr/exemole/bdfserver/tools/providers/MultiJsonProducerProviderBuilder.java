/* BdfServer - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.providers;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.providers.JsonProducerProvider;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public class MultiJsonProducerProviderBuilder {

    private List<JsonProducerProvider> providerList = new ArrayList<JsonProducerProvider>();

    public MultiJsonProducerProviderBuilder() {
    }

    public void addJsonProducerProvider(JsonProducerProvider jsonProducerProvider) {
        if (jsonProducerProvider instanceof MultiJsonProducerProvider) {
            JsonProducerProvider[] array = ((MultiJsonProducerProvider) jsonProducerProvider).array;
            int length = array.length;
            for (int i = 0; i < length; i++) {
                providerList.add(array[i]);
            }
        } else {
            providerList.add(jsonProducerProvider);
        }
    }

    public JsonProducerProvider toJsonProducerProvider() {
        int size = providerList.size();
        return new MultiJsonProducerProvider(providerList.toArray(new JsonProducerProvider[size]));
    }


    private static class MultiJsonProducerProvider implements JsonProducerProvider {

        private JsonProducerProvider[] array;

        private MultiJsonProducerProvider(JsonProducerProvider[] array) {
            this.array = array;
        }

        @Override
        public JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
            int length = array.length;
            for (int i = 0; i < length; i++) {
                JsonProducer jsonProducer = array[i].getJsonProducer(parameters);
                if (jsonProducer != null) {
                    return jsonProducer;
                }
            }
            return null;
        }

    }

}
