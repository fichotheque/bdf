/* BdfServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools;

import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.request.RequestConstants;


/**
 *
 * @author Vincent Calame
 */
public class BdfHref implements CharSequence {

    private final StringBuilder buf = new StringBuilder();
    private boolean first = true;

    public BdfHref(String href, boolean first) {
        buf.append(href);
        this.first = first;
    }

    public BdfHref page(String pageName) {
        return param(RequestConstants.PAGE_PARAMETER, pageName);
    }

    public BdfHref pageErr(String pageName) {
        return param(InteractionConstants.PAGE_ERROR_PARAMNAME, pageName);
    }

    public BdfHref stream(String streamName) {
        return param(RequestConstants.STREAM_PARAMETER, streamName);
    }

    public BdfHref command(String commandName) {
        return param(RequestConstants.COMMAND_PARAMETER, commandName);
    }

    public BdfHref param(String paramName, String paramValue) {
        if (paramValue == null) {
            return this;
        }
        testFirst();
        buf.append(paramName);
        buf.append("=");
        buf.append(paramValue);
        return this;
    }

    public BdfHref sqlExport(String sqlExportName) {
        return param(InteractionConstants.SQLEXPORT_PARAMNAME, sqlExportName);
    }

    public BdfHref scrutariExport(String scrutariExportName) {
        return param(InteractionConstants.SCRUTARIEXPORT_PARAMNAME, scrutariExportName);
    }

    public BdfHref tableExport(String tableExportName) {
        return param(InteractionConstants.TABLEEXPORT_PARAMNAME, tableExportName);
    }

    public BdfHref balayage(String balayageName) {
        return param(InteractionConstants.BALAYAGE_PARAMNAME, balayageName);
    }

    public BdfHref refresh() {
        return param(InteractionConstants.REFRESH_PARAMNAME, String.valueOf(System.currentTimeMillis()));
    }

    public BdfHref subset(Subset subset) {
        return subset(subset.getSubsetKey());
    }

    public BdfHref subset(SubsetKey subsetKey) {
        return param(subsetKey.getCategoryString(), subsetKey.getSubsetName());
    }

    public BdfHref subsetItem(SubsetItem subsetItem) {
        SubsetKey subsetKey = subsetItem.getSubsetKey();
        subset(subsetKey);
        buf.append('&');
        buf.append(InteractionConstants.ID_PARAMNAME);
        buf.append("=");
        buf.append(subsetItem.getId());
        return this;
    }

    public BdfHref derive(String paramName, String paramValue) {
        BdfHref newInstance = new BdfHref(buf.toString(), first);
        return newInstance.param(paramName, paramValue);
    }

    @Override
    public String toString() {
        return buf.toString();
    }

    @Override
    public int length() {
        return buf.length();
    }

    @Override
    public char charAt(int index) {
        return buf.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return buf.subSequence(start, end);
    }

    private void testFirst() {
        if (first) {
            buf.append('?');
            first = false;
        } else {
            buf.append('&');
        }
    }

}
