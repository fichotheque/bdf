/* BdfServer - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.runners;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.fichotheque.exportation.table.TableExportContext;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.tools.exportation.scrutari.ScrutariExportEngine;
import net.fichotheque.tools.permission.PermissionUtils;
import net.mapeadores.util.hook.HookHandler;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.scrutari.dataexport.ScrutariInfoUtils;


/**
 *
 * @author Vincent Calame
 */
public final class ScrutariExportRunner {

    public final static String INFO_SUFFIX = ".scrutari-info";
    public final static String DATA_SUFFIX = ".scrutari-data";
    private final BdfServer bdfServer;
    private final ScrutariExportDef scrutariExportDef;
    private final PathConfiguration pathConfiguration;
    private final File destinationDirectory;

    private ScrutariExportRunner(ScrutariExportDef scrutariExportDef, BdfServer bdfServer, PathConfiguration pathConfiguration) {
        this.scrutariExportDef = scrutariExportDef;
        this.bdfServer = bdfServer;
        this.pathConfiguration = pathConfiguration;
        this.destinationDirectory = ConfigurationUtils.getTarget(pathConfiguration, scrutariExportDef).getFile();
    }

    public static void run(ScrutariExportDef scrutariExportDef, BdfServer bdfServer, PathConfiguration pathConfiguration) throws IOException {
        ScrutariExportRunner runner = new ScrutariExportRunner(scrutariExportDef, bdfServer, pathConfiguration);
        runner.run();
    }

    private void run() throws IOException {
        writeInfo();
        writeData();
    }

    private void writeInfo() throws IOException {
        File file = new File(destinationDirectory, scrutariExportDef.getName() + INFO_SUFFIX + ".xml");
        try (BufferedWriter bufwriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            String[] urlArray = new String[1];
            urlArray[0] = scrutariExportDef.getName() + DATA_SUFFIX + ".xml";
            ScrutariInfoUtils.writeScrutariInfo(bufwriter, 0, true, new Date(), urlArray);
        }
    }

    private void writeData() throws IOException {
        File file = new File(destinationDirectory, scrutariExportDef.getName() + DATA_SUFFIX + ".xml");
        ExtractionContext extractionContext = BdfServerUtils.initExtractionContextBuilder(bdfServer, LocalisationUtils.toUserLangContext(bdfServer.getLangConfiguration().getDefaultWorkingLang()), PermissionUtils.FICHOTHEQUEADMIN_PERMISSIONSUMMARY).toExtractionContext();
        try (BufferedWriter bufwriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            TableExportContext tableExportContext = bdfServer.getTableExportContext();
            HookHandler hookHandler = BdfServerUtils.getHookHandler(bdfServer, null, "ScrutariExport");
            ScrutariExportEngine scrutariExportEngine = ScrutariExportEngine.build(scrutariExportDef, LangsUtils.toArray(bdfServer.getLangConfiguration().getWorkingLangs()), extractionContext, tableExportContext, bdfServer.getThesaurusLangChecker(), hookHandler, BdfServerUtils.resolveSelectionOptions(bdfServer, scrutariExportDef.getSelectionOptions()));
            scrutariExportEngine.run(bufwriter, 0, true);
        }
    }

}
