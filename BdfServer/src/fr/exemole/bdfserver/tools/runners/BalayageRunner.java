/* BdfServer - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.runners;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.balayage.BalayageSession;
import fr.exemole.bdfserver.api.balayage.LockedBalayageException;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageEngine;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageInitializer;
import fr.exemole.bdfserver.tools.balayage.engine.BalayageLog;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import java.io.File;
import net.fichotheque.exportation.balayage.BalayageDef;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.fichotheque.exportation.balayage.BalayageMode;
import net.fichotheque.utils.BalayageUtils;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LogUtils;


/**
 *
 * @author Vincent Calame
 */
public final class BalayageRunner {

    public final static String INIT_ERROR = "init_error";
    public final static String LOCK_ERROR = "lock_error";

    private BalayageRunner() {
    }

    /**
     * Retourne soit une instance de CommandMessage en cas d'erreur soit
     * l'instance de BalayageLog en argument.
     */
    public static Result run(BalayageDescription balayageDescription, String mode, BdfServer bdfServer, PathConfiguration pathConfiguration) {
        BalayageDef balayageDef = balayageDescription.getBalayageDef();
        String balayageName = balayageDef.getName();
        Result result;
        try (BalayageSession session = bdfServer.getBalayageManager().openBalayageSession(balayageName)) {
            BalayageInitializer balayageInitializer = new BalayageInitializer(bdfServer, balayageDescription, pathConfiguration);
            if (balayageInitializer.hasError()) {
                return new Result(LogUtils.error(balayageInitializer.getErrorMessageKey(), balayageInitializer.getErrorMessageValue()));
            }
            if (mode != null) {
                BalayageMode balayageMode = BalayageUtils.getMode(balayageDef, mode);
                if (balayageMode != null) {
                    balayageInitializer.setBalayageMode(balayageMode);
                }
            }
            try (BalayageLog balayageLog = new BalayageLog(balayageName, getBalayageLogFile(bdfServer, balayageName, mode))) {
                BalayageEngine balayageEngine = balayageInitializer.getBalayageEngine(balayageLog);
                balayageEngine.run();
                result = new Result(balayageLog);
            }

        } catch (LockedBalayageException e) {
            result = new Result(LogUtils.error("_ error.existing.balayagesession", balayageName));
        }
        return result;
    }

    private static File getBalayageLogFile(BdfServer bdfServer, String balayageName, String mode) {
        File logDirectory = ConfigurationUtils.getLogDirectory(bdfServer);
        if (mode == null) {
            mode = "";
        } else {
            mode = "_" + mode;
        }
        return new File(logDirectory, "balayage_" + balayageName + mode + ".txt");
    }


    public static class Result {

        private final CommandMessage commandMessage;
        private final BalayageLog balayageLog;

        private Result(CommandMessage commandMessage) {
            this.commandMessage = commandMessage;
            this.balayageLog = null;
        }

        private Result(BalayageLog balayageLog) {
            this.balayageLog = balayageLog;
            this.commandMessage = null;
        }

        public boolean hasError() {
            return (commandMessage != null);
        }

        public CommandMessage getError() {
            return commandMessage;
        }

        public BalayageLog getBalayageLog() {
            return balayageLog;
        }

    }

}
