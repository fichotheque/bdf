/* BdfServer - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.dataresolvers;

import java.io.IOException;
import net.fichotheque.extraction.DataResolver;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ErrorDataResolver implements DataResolver {

    private final String originalType;
    private final String errorMessage;

    public ErrorDataResolver(String originalType, String errorMessage) {
        this.originalType = originalType;
        this.errorMessage = errorMessage;
    }

    @Override
    public String getType() {
        return "error";
    }

    @Override
    public Writer getWriter(SubsetItemPointeur pointeur, LangContext langContext) {
        return new InternalWriter();
    }


    private class InternalWriter implements DataResolver.Writer {

        private InternalWriter() {

        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public void write(XMLWriter xmlWriter) throws IOException {
            xmlWriter.addSimpleElement("type", originalType);
            xmlWriter.addSimpleElement("error", errorMessage);
        }

    }

}
