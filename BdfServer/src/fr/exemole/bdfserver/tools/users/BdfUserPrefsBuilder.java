/* BdfServer - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.users;

import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import java.util.Locale;
import net.fichotheque.selection.FicheQuery;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;


/**
 *
 * @author Vincent Calame
 */
public class BdfUserPrefsBuilder {

    private final AttributesBuilder attributesBuilder;
    private Lang workingLang;
    private Locale customFormatLocale;
    private LangPreference customLangPreference;
    private FicheQuery defaultFicheQuery;


    public BdfUserPrefsBuilder() {
        this.attributesBuilder = new AttributesBuilder();
    }

    public AttributesBuilder getAttributesBuilder() {
        return attributesBuilder;
    }

    public BdfUserPrefsBuilder setWorkingLang(Lang workingLang) {
        this.workingLang = workingLang;
        return this;
    }

    public BdfUserPrefsBuilder setCustomFormatLocale(Locale customFormatLocale) {
        this.customFormatLocale = customFormatLocale;
        return this;
    }

    public BdfUserPrefsBuilder setCustomLangPreference(LangPreference customLangPreference) {
        this.customLangPreference = customLangPreference;
        return this;
    }

    public BdfUserPrefsBuilder setDefaultFicheQuery(FicheQuery defaultFicheQuery) {
        this.defaultFicheQuery = defaultFicheQuery;
        return this;
    }

    public BdfUserPrefs toBdfUserPrefs() {
        return new InternalBdfUserPrefs(workingLang, customFormatLocale, customLangPreference, attributesBuilder.toAttributes(), defaultFicheQuery);
    }

    public static BdfUserPrefsBuilder init() {
        return new BdfUserPrefsBuilder();
    }


    private static class InternalBdfUserPrefs implements BdfUserPrefs {

        private final Lang workingLang;
        private final Locale customFormatLocale;
        private final LangPreference customLangPreference;
        private final Attributes attributes;
        private final FicheQuery defaultFicheQuery;


        private InternalBdfUserPrefs(Lang workingLang, Locale customFormatLocale, LangPreference customLangPreference,
                Attributes attributes, FicheQuery defaultFicheQuery) {
            this.workingLang = workingLang;
            this.customFormatLocale = customFormatLocale;
            this.customLangPreference = customLangPreference;
            this.attributes = attributes;
            this.defaultFicheQuery = defaultFicheQuery;
        }

        @Override
        public Lang getWorkingLang() {
            return workingLang;
        }

        @Override
        public Locale getCustomFormatLocale() {
            return customFormatLocale;
        }

        @Override
        public LangPreference getCustomLangPreference() {
            return customLangPreference;
        }

        @Override
        public FicheQuery getDefaultFicheQuery() {
            return defaultFicheQuery;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

    }

}
