/* BdfServer - Copyright (c) 2016-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.users;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.utils.FichothequeMetadataUtils;
import net.mapeadores.opendocument.io.SheetWriter;
import net.mapeadores.opendocument.io.odtable.OdColumnDef;
import net.mapeadores.opendocument.io.odtable.OdTableDef;
import net.mapeadores.opendocument.io.odtable.OdTableDefBuilder;
import net.mapeadores.opendocument.io.odtable.OdsXMLPart;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class StatusSheetWriter implements SheetWriter {

    private final BdfParameters bdfParameters;
    private final MessageLocalisation messageLocalisation;
    private final String originalTableName;
    private final OdTableDef odTableDef;

    public StatusSheetWriter(BdfParameters bdfParameters) {
        this.bdfParameters = bdfParameters;
        this.messageLocalisation = bdfParameters.getMessageLocalisation();
        this.originalTableName = messageLocalisation.toNotNullString("_ title.exportation.statussheet");
        this.odTableDef = OdTableDefBuilder.buildStandard(originalTableName, 2);
    }

    @Override
    public List<OdTableDef> getTableDefList() {
        return Collections.singletonList(odTableDef);
    }

    @Override
    public void writeTable(OdsXMLPart odsXMLPart, String originalName, String checkedName) throws IOException {
        StringBuilder buf = new StringBuilder();
        Redacteur redacteur = bdfParameters.getBdfUser().getRedacteur();
        buf.append(redacteur.getCompleteName());
        buf.append(" – ");
        buf.append(redacteur.getBracketStyle());
        String cellStyleName = odsXMLPart.getCellStyleName(checkedName, 2, OdColumnDef.DATE_STYLE_FAMILY, null);
        odsXMLPart
                .tableStart(checkedName)
                .rowStart()
                .stringCell(messageLocalisation.toString("_ label.exportation.status_origin"))
                .stringCell(FichothequeMetadataUtils.getLongTitle(bdfParameters.getFichotheque(), bdfParameters.getWorkingLang()))
                .rowEnd()
                .rowStart()
                .stringCell(messageLocalisation.toString("_ label.exportation.status_user"))
                .stringCell(buf.toString())
                .rowEnd()
                .rowStart()
                .stringCell(messageLocalisation.toString("_ label.exportation.status_date"))
                .dateCell(FuzzyDate.current(), cellStyleName)
                .rowEnd()
                .tableEnd();
    }

}
