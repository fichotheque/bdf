/* BdfServer - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.session;

import fr.exemole.bdfserver.api.session.LoginException;
import fr.exemole.bdfserver.api.session.LoginParameters;
import java.text.ParseException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class LoginParametersBuilder {

    private String formAction = "";
    private String reversePath = "";
    private LoginException loginException;
    private RequestMap requestMap;
    private CommandMessage commandMessage;
    private final Set<SubsetKey> loginSphereKeySet = new LinkedHashSet<SubsetKey>();
    private SubsetKey defaultSphereKey;
    private String titlePhraseName;


    public LoginParametersBuilder() {
    }

    public LoginParametersBuilder setFormAction(String formAction) {
        this.formAction = formAction;
        return this;
    }

    public LoginParametersBuilder setReversePath(String reversePath) {
        this.reversePath = reversePath;
        return this;
    }

    public LoginParametersBuilder setLoginException(LoginException loginException) {
        this.loginException = loginException;
        return this;
    }

    public LoginParametersBuilder setRequestMap(RequestMap requestMap) {
        this.requestMap = requestMap;
        return this;
    }

    public LoginParametersBuilder setCommandMessage(CommandMessage commandMessage) {
        this.commandMessage = commandMessage;
        return this;
    }

    public LoginParametersBuilder addLoginSphereKey(String sphereName) throws ParseException {
        SubsetKey sphereKey = SubsetKey.parse(SubsetKey.CATEGORY_SPHERE, sphereName);
        loginSphereKeySet.add(sphereKey);
        return this;
    }

    public LoginParametersBuilder addLoginSphereKey(SubsetKey loginSphereKey) {
        if (!loginSphereKey.isSphereSubset()) {
            throw new IllegalArgumentException("!loginSphereKey.isSphereSubset()");
        }
        loginSphereKeySet.add(loginSphereKey);
        return this;
    }

    public LoginParametersBuilder setDefaultSphereKey(SubsetKey defaultSphereKey) {
        if ((defaultSphereKey != null) && (!defaultSphereKey.isSphereSubset())) {
            throw new IllegalArgumentException("!loginSphereKey.isSphereSubset()");
        }
        this.defaultSphereKey = defaultSphereKey;
        return this;
    }

    public LoginParametersBuilder setTitlePhraseName(String titlePhraseName) {
        this.titlePhraseName = titlePhraseName;
        return this;
    }

    public LoginParameters toLoginParameters() {
        List<SubsetKey> finalList = FichothequeUtils.toList(loginSphereKeySet);
        return new InternalLoginParameters(formAction, reversePath, loginException, requestMap, commandMessage, finalList, defaultSphereKey, titlePhraseName);
    }

    public static LoginParametersBuilder init() {
        return new LoginParametersBuilder();
    }


    private static class InternalLoginParameters implements LoginParameters {

        private final String formAction;
        private final String reversePath;
        private final LoginException loginException;
        private final RequestMap requestMap;
        private final CommandMessage commandMessage;
        private final List<SubsetKey> loginSphereKeyList;
        private final SubsetKey defaultSphereKey;
        private final String titlePhraseName;


        private InternalLoginParameters(String formAction, String reversePath, LoginException loginException, RequestMap requestMap, CommandMessage commandMessage, List<SubsetKey> loginSphereKeyList, SubsetKey defaultSphereKey, String titlePhraseName) {
            this.formAction = formAction;
            this.reversePath = reversePath;
            this.loginException = loginException;
            this.requestMap = requestMap;
            this.commandMessage = commandMessage;
            this.loginSphereKeyList = loginSphereKeyList;
            this.defaultSphereKey = defaultSphereKey;
            this.titlePhraseName = titlePhraseName;
        }

        @Override
        public String getFormAction() {
            return formAction;
        }

        @Override
        public String getReversePath() {
            return reversePath;
        }

        @Override
        public LoginException getLoginException() {
            return loginException;
        }

        @Override
        public RequestMap getRequestMap() {
            return requestMap;
        }

        @Override
        public CommandMessage getCommandMessage() {
            return commandMessage;
        }

        @Override
        public List<SubsetKey> getLoginSphrereKeyList() {
            return loginSphereKeyList;
        }

        @Override
        public SubsetKey getDefaultSphereKey() {
            return defaultSphereKey;
        }

        @Override
        public String getTitlePhraseName() {
            return titlePhraseName;
        }

    }

}
