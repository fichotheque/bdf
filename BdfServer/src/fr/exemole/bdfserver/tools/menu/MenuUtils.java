/* BdfServer - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.menu;

import fr.exemole.bdfserver.api.menu.ActionEntry;
import fr.exemole.bdfserver.api.menu.MenuNode;
import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public final class MenuUtils {

    private MenuUtils() {

    }

    public static List<MenuNode> wrap(MenuNode[] array) {
        return new MenuNodeList(array);
    }

    public static List<ActionEntry> wrap(ActionEntry[] array) {
        return new ActionEntryList(array);
    }


    private static class MenuNodeList extends AbstractList<MenuNode> implements RandomAccess {

        private final MenuNode[] array;

        private MenuNodeList(MenuNode[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MenuNode get(int index) {
            return array[index];
        }

    }


    private static class ActionEntryList extends AbstractList<ActionEntry> implements RandomAccess {

        private final ActionEntry[] array;

        private ActionEntryList(ActionEntry[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ActionEntry get(int index) {
            return array[index];
        }

    }

}
