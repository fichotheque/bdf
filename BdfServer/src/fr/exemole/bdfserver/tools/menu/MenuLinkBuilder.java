/* BdfServer - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.menu;

import fr.exemole.bdfserver.api.menu.MenuLink;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class MenuLinkBuilder {

    private final String url;
    private String title;
    private Message titleMessage;
    private String iconPath;
    private String target;
    private String pageName;

    public MenuLinkBuilder(String url) {
        if (url == null) {
            throw new IllegalArgumentException("url is null");
        }
        this.url = url;
    }

    public MenuLinkBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public MenuLinkBuilder setTitleMessage(Message titleMessage) {
        this.titleMessage = titleMessage;
        return this;
    }

    public MenuLinkBuilder setTitleMessage(String messageKey, Object... messageValues) {
        this.titleMessage = LocalisationUtils.toMessage(messageKey, messageValues);
        return this;
    }

    public MenuLinkBuilder setIconPath(String iconPath) {
        this.iconPath = iconPath;
        return this;
    }

    public MenuLinkBuilder setTarget(String target) {
        this.target = target;
        return this;
    }

    public MenuLinkBuilder setPageName(String pageName) {
        this.pageName = pageName;
        return this;
    }

    public MenuLink toMenuLink() {
        String finalTitle = title;
        if (finalTitle == null) {
            if (titleMessage != null) {
                finalTitle = titleMessage.getMessageKey();
            } else {
                finalTitle = "";
            }
        }
        return new InternalMenuLink(finalTitle, titleMessage, iconPath, target, url, pageName);
    }


    private static class InternalMenuLink implements MenuLink {

        private final String title;
        private final Message titleMessage;
        private final String iconPath;
        private final String target;
        private final String url;
        private final String pageName;

        private InternalMenuLink(String title, Message titleMessage, String iconPath, String target, String url, String pageName) {
            this.title = title;
            this.titleMessage = titleMessage;
            this.iconPath = iconPath;
            this.target = target;
            this.url = url;
            this.pageName = pageName;
        }

        @Override
        public Message getTitleMessage() {
            return titleMessage;
        }

        @Override
        public String getTitle() {
            return title;
        }

        @Override
        public String getIconPath() {
            return iconPath;
        }

        @Override
        public String getTarget() {
            return target;
        }

        @Override
        public String getUrl() {
            return url;
        }

        @Override
        public String getPageName() {
            return pageName;
        }

    }

}
