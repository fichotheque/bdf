/* BdfServer - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.menu;

import fr.exemole.bdfserver.api.menu.ActionEntry;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class ActionEntryBuilder {

    private final String url;
    private Object titleObject;
    private String target;
    private String pageName;
    private String cssName;

    public ActionEntryBuilder(String url) {
        if (url == null) {
            throw new IllegalArgumentException("url is null");
        }
        this.url = url;
    }

    public ActionEntryBuilder setTitle(String title) {
        this.titleObject = title;
        return this;
    }

    public ActionEntryBuilder setTitleMessage(Message titleMessage) {
        this.titleObject = titleMessage;
        return this;
    }

    public ActionEntryBuilder setTitleMessage(String messageKey, Object... messageValues) {
        this.titleObject = LocalisationUtils.toMessage(messageKey, messageValues);
        return this;
    }

    public ActionEntryBuilder setCssName(String cssName) {
        this.cssName = cssName;
        return this;
    }

    public ActionEntry toActionEntry() {
        Object finalTitleObject = titleObject;
        if (finalTitleObject == null) {
            finalTitleObject = "??";
        }
        return new InternalActionEntry(cssName, finalTitleObject, target, url, pageName);
    }

    public static ActionEntryBuilder init(String url) {
        return new ActionEntryBuilder(url);
    }


    private static class InternalActionEntry implements ActionEntry {

        private final String cssName;
        private final Object title;
        private final String target;
        private final String url;
        private final String pageName;

        private InternalActionEntry(String cssName, Object title, String target, String url, String pageName) {
            this.cssName = cssName;
            this.title = title;
            this.target = target;
            this.url = url;
            this.pageName = pageName;
        }

        @Override
        public String getCssName() {
            return cssName;
        }

        @Override
        public Object getTitle() {
            return title;
        }

        @Override
        public String getTarget() {
            return target;
        }

        @Override
        public String getUrl() {
            return url;
        }

        @Override
        public String getPageName() {
            return pageName;
        }

    }

}
