/* BdfServer - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.interaction;

import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class PropertiesParam {

    public final static String TABLEEXPORT_TYPE = "tableexport";
    public final static String SPECIAL_TYPE = "special";
    public final static String EMPTY_SPECIAL = "empty";
    public final static String TABLE_SPECIAL = "table";
    public final static String FORM_SPECIAL = "form";
    public final static String PHRASES_SPECIAL = "phrases";
    public final static PropertiesParam EMPTY;
    public final static PropertiesParam TABLE;
    public final static PropertiesParam FORM;
    public final static PropertiesParam PHRASES;
    private final String type;
    private final String name;

    static {
        EMPTY = new PropertiesParam(SPECIAL_TYPE, EMPTY_SPECIAL);
        TABLE = new PropertiesParam(SPECIAL_TYPE, TABLE_SPECIAL);
        FORM = new PropertiesParam(SPECIAL_TYPE, FORM_SPECIAL);
        PHRASES = new PropertiesParam(SPECIAL_TYPE, PHRASES_SPECIAL);
    }

    private PropertiesParam(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public boolean isTableExport() {
        return (type.equals(TABLEEXPORT_TYPE));
    }

    public boolean isSpecial() {
        return (type.equals(SPECIAL_TYPE));
    }

    public static PropertiesParam fromRequest(RequestMap requestMap) {
        return fromRequest(requestMap, InteractionConstants.PROPERTIES_PARAMNAME, InteractionConstants.TABLEEXPORT_PARAMNAME);
    }

    public static PropertiesParam fromRequest(RequestMap requestMap, String propertiesParamName, String tableExportParamName) {
        if (propertiesParamName != null) {
            PropertiesParam propertiesParam = fromProperties(requestMap.getTrimedParameter(propertiesParamName));
            if (propertiesParam != null) {
                return propertiesParam;
            }
        }
        if (tableExportParamName != null) {
            PropertiesParam propertiesParam = fromTableExport(requestMap.getTrimedParameter(tableExportParamName));
            if (propertiesParam != null) {
                return propertiesParam;
            }
        }
        return EMPTY;
    }

    private static PropertiesParam fromProperties(String properties) {
        int idx = properties.indexOf(":");
        if (idx > 1) {
            String type = properties.substring(0, idx);
            String name = properties.substring(idx + 1);
            switch (type) {
                case SPECIAL_TYPE: {
                    switch (properties) {
                        case EMPTY_SPECIAL:
                            return EMPTY;
                        case TABLE_SPECIAL:
                            return TABLE;
                        case FORM_SPECIAL:
                            return FORM;
                        case PHRASES_SPECIAL:
                            return PHRASES;
                        default:
                            return new PropertiesParam(SPECIAL_TYPE, name);
                    }
                }
                case TABLEEXPORT_TYPE: {
                    return fromTableExport(name);
                }
            }
        }
        return null;
    }

    private static PropertiesParam fromTableExport(String tableExportName) {
        if (!tableExportName.isEmpty()) {
            switch (tableExportName) {
                case "_table":
                    return TABLE;
                case "_form":
                    return FORM;
                default:
                    return new PropertiesParam(TABLEEXPORT_TYPE, tableExportName);
            }
        }
        return null;
    }

}
