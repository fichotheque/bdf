/* BdfServer - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.apps;

import java.util.Map;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class AppConf {

    public final static String ARGS_PREFIX = "args_";
    public final static String ACTIVE = "active";
    public final static String MULTI_ACTIVE = "multi_active";
    public final static String MULTI_NOAUTHENTIFICATION = "multi_noauthentication";
    public final static String CORE_JSLIBS = "core_jslibs";
    public final static String CORE_JQUERYEXTENSIONS = "core_jqueryextensions";
    public final static String CORE_CODEMIRRORMODES = "core_codemirrormodes";
    public final static String CORE_LEAFLETEXTENSIONS = "core_leafletextensions";
    public final static String CORE_THIRDLIBS = "core_thirdlibs";
    public final static String CORE_THEMECSSFILES = "core_themecssfiles";
    public final static String CORE_INCLUDEFICHECSS = "core_includefichecss";
    public final static String CORE_JSORDER = "core_jsorder";
    public final static String CORE_BODYCSSCLASS = "core_bodycssclass";
    public final static String CORE_BDFUSERNEED = "core_bdfuserneed";
    public final static String CORE_TITLEPHRASENAME = "core_titlephrasename";
    public final static String LOGIN_SESSIONMESSAGE = "login_sessionmessage";
    public final static String LOGIN_AVAILABLESPHERES = "login_availablespheres";
    public final static String LOGIN_DEFAULTSPHERE = "login_defaultsphere";
    public final static String LOGIN_TITLEPHRASENAME = "login_titlephrasename";
    private final static String[] EMPTY_ARRAY = new String[0];
    private final String appName;
    private final Map<String, String> iniMap;

    public AppConf(String appName, Map<String, String> iniMap) {
        this.appName = appName;
        this.iniMap = iniMap;
    }

    public String getAppName() {
        return appName;
    }

    public JsObject getArgsJsObject() {
        boolean done = false;
        JsObject jsObject = JsObject.init();
        for (Map.Entry<String, String> entry : iniMap.entrySet()) {
            String key = entry.getKey();
            if (key.startsWith(ARGS_PREFIX)) {
                String name = key.substring(ARGS_PREFIX.length());
                if (name.length() > 0) {
                    jsObject.put(name, entry.getValue());
                    done = true;
                }
            }
        }
        if (done) {
            return jsObject;
        } else {
            return null;
        }
    }

    public String[] getArray(String paramName) {
        String value = iniMap.get(paramName);
        if (value == null) {
            return EMPTY_ARRAY;
        }
        return StringUtils.getTechnicalTokens(value, true);
    }

    public boolean getBoolean(String paramName) {
        boolean defaultValue = false;
        String value = iniMap.get(paramName);
        if (value == null) {
            return defaultValue;
        }
        value = value.toLowerCase().trim();
        switch (value) {
            case "":
                return defaultValue;
            case "0":
            case "false":
                return false;
            case "1":
            case "true":
                return true;
            default:
                return defaultValue;
        }
    }

    public String getString(String paramName) {
        return iniMap.get(paramName);
    }

    public String getString(String paramName, String defaultValue) {
        String value = iniMap.get(paramName);
        if (value != null) {
            return value;
        } else {
            return defaultValue;
        }
    }

}
