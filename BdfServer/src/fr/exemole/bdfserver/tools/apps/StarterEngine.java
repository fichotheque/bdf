/* BdfServer - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.apps;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import net.fichotheque.EditOrigin;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public final class StarterEngine {

    private final BdfServer bdfServer;
    private final EditOrigin editOrigin;
    private final String appName;
    private final String originRoot;
    private final RelativePath destinationRoot;

    private StarterEngine(BdfServer bdfServer, EditOrigin editOrigin, String appName, String originRoot, RelativePath destinationRoot) {
        this.bdfServer = bdfServer;
        this.editOrigin = editOrigin;
        this.appName = appName;
        this.originRoot = originRoot;
        this.destinationRoot = destinationRoot;
    }

    public static void copy(BdfServer bdfServer, EditOrigin editOrigin, String appName, String starterName) throws IOException, ParseException {
        RelativePath destinationRoot = StorageUtils.parseAppResourcePath(appName, "");
        StarterEngine engine = new StarterEngine(bdfServer, editOrigin, appName, "defaultfiles/" + starterName + "/", destinationRoot);
        engine.copyFiles();
    }

    private void copyFiles() throws IOException, ParseException {
        copyFile("app.ini");
        copyFile("app.html");
        createDirectory("css");
        createDirectory("js");
        createDirectory("l10n");
        createDirectory("templates");
        copyCustomFile();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(StarterEngine.class.getResourceAsStream(originRoot + "list.txt"), "UTF-8"))) {
            String ligne;
            while ((ligne = reader.readLine()) != null) {
                ligne = ligne.trim();
                if (ligne.length() > 0) {
                    copyFile(ligne);
                }
            }
        } catch (IOException ioe) {
        }
        for (Lang lang : bdfServer.getLangConfiguration().getWorkingLangs()) {
            try (InputStream is = IOUtils.toInputStream("#_ l10n.key=", "UTF-8")) {
                StorageUtils.saveResource(bdfServer, destinationRoot.buildChild("l10n/" + lang.toString() + ".ini"), is, editOrigin);
            }
        }
    }

    private void copyFile(String filePath) throws IOException {
        try (InputStream is = StarterEngine.class.getResourceAsStream(originRoot + filePath)) {
            StorageUtils.saveResource(bdfServer, destinationRoot.buildChild(filePath), is, editOrigin);
        }
    }

    private void copyCustomFile() throws IOException, ParseException {
        RelativePath customFilePath = AppConfUtils.getCustomIniPath(appName);
        try (InputStream is = StarterEngine.class.getResourceAsStream(originRoot + "custom.ini")) {
            StorageUtils.saveResource(bdfServer, customFilePath, is, editOrigin);
        }
    }

    private void createDirectory(String name) throws ParseException {
        StorageUtils.createDirectory(bdfServer, StorageUtils.parseAppResourcePath(appName, name));
    }

}
