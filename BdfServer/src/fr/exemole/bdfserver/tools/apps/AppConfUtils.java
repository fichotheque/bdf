/* BdfServer_Html - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.apps;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.mapeadores.util.ini.IniParser;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.ResourceFolder;
import net.mapeadores.util.io.ResourceStorage;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public final class AppConfUtils {

    private AppConfUtils() {

    }

    public static boolean isAvailableStarter(String starterName) {
        switch (starterName) {
            case "minimal":
            case "dashboard":
                return true;
            default:
                return false;
        }
    }

    public static RelativePath getAppIniPath(String appName) throws ParseException {
        return StorageUtils.parseAppResourcePath(appName, "app.ini");
    }

    public static RelativePath getCustomIniPath(String appName) throws ParseException {
        return RelativePath.build("custom/app-" + appName + ".ini");
    }

    public static AppConf getAppConf(ResourceStorages resourceStorages, String appName) {
        RelativePath appIniPath;
        try {
            appIniPath = getAppIniPath(appName);
        } catch (ParseException pe) {
            return null;
        }
        if (!resourceStorages.containsResource(appIniPath)) {
            return null;
        }
        Map<String, String> iniMap = new HashMap<String, String>();
        parseIni(resourceStorages, appIniPath, iniMap);
        try {
            RelativePath customIniPath = getCustomIniPath(appName);
            parseIni(resourceStorages, customIniPath, iniMap);
        } catch (ParseException pe) {

        }
        return new AppConf(appName, iniMap);
    }

    private static void parseIni(ResourceStorages resourceStorages, RelativePath relativePath, Map<String, String> iniMap) {
        DocStream docStream = resourceStorages.getResourceDocStream(relativePath);
        if (docStream != null) {
            try (InputStream is = docStream.getInputStream()) {
                IniParser.parseIni(is, iniMap);
            } catch (IOException ioe) {
                throw new BdfStorageException(ioe);
            }
        }
    }

    public static SortedMap<String, AppConf> getAppConfList(ResourceStorages resourceStorages) {
        SortedMap< String, AppConf> result = new TreeMap<String, AppConf>();
        for (ResourceStorage resourceStorage : resourceStorages) {
            ResourceFolder appsFolder = resourceStorage.getResourceFolder(StorageUtils.APPS_ROOT);
            if (appsFolder != null) {
                for (ResourceFolder subfolder : appsFolder.getSubfolderList()) {
                    String name = subfolder.getName();
                    if (!result.containsKey(name)) {
                        AppConf appConf = getAppConf(resourceStorages, name);
                        if (appConf != null) {
                            result.put(name, appConf);
                        }
                    }
                }
            }
        }
        return result;
    }

}
