/* global Fql,Fapi,Bdf,$$,Dashboard */
var ARGS = {
};

const CURRENT_CONFIG = new Fapi("");

const API_CACHE = new Fapi.Cache();

const DEFAULT_NAVIGATION = new Navigation("");

$(function () {
    Bdf.initTemplates();
    Dashboard.init({
        mainHtml: "",
        navHtml: "",
        toolbarHtml: ""
    });
});
