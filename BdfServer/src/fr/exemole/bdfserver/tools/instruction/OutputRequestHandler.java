/* BdfServer - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.Domain;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.tools.configuration.PathConfigurationBuilder;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.access.AccessDef;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TransformationDescription;
import net.fichotheque.selection.SelectionDef;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class OutputRequestHandler extends RequestHandler implements OutputParameters {

    private final OutputParameters outputParameters;

    public OutputRequestHandler(OutputParameters outputParameters) {
        super(outputParameters, outputParameters.getRequestMap());
        this.outputParameters = outputParameters;
    }

    @Override
    public Domain getDomain() {
        return outputParameters.getDomain();
    }

    @Override
    public String getOutput() {
        return outputParameters.getOutput();
    }

    @Override
    public BdfCommandResult getBdfCommandResult() {
        return outputParameters.getBdfCommandResult();
    }

    public Corpus getCorpus() throws ErrorMessageException {
        Corpus corpus = (Corpus) getResultObject(BdfInstructionConstants.CORPUS_OBJ);
        if (corpus == null) {
            FicheMeta ficheMeta = (FicheMeta) getResultObject(BdfInstructionConstants.FICHEMETA_OBJ);
            if (ficheMeta != null) {
                corpus = ficheMeta.getCorpus();
            }
        }
        if (corpus == null) {
            corpus = getMandatoryCorpus();
        }
        checkSubsetAccess(corpus);
        return corpus;
    }

    public Thesaurus getThesaurus() throws ErrorMessageException {
        Thesaurus thesaurus = (Thesaurus) getResultObject(BdfInstructionConstants.THESAURUS_OBJ);
        if (thesaurus == null) {
            Motcle motcle = (Motcle) getResultObject(BdfInstructionConstants.MOTCLE_OBJ);
            if (motcle != null) {
                thesaurus = motcle.getThesaurus();
            }
        }
        if (thesaurus == null) {
            thesaurus = getMandatoryThesaurus();
        }
        checkSubsetAccess(thesaurus);
        return thesaurus;
    }

    public Addenda getAddenda() throws ErrorMessageException {
        Addenda addenda = (Addenda) getResultObject(BdfInstructionConstants.ADDENDA_OBJ);
        if (addenda == null) {
            Document document = (Document) getResultObject(BdfInstructionConstants.DOCUMENT_OBJ);
            if (document != null) {
                addenda = document.getAddenda();
            }
        }
        if (addenda == null) {
            addenda = getMandatoryAddenda();
        }
        checkSubsetAccess(addenda);
        return addenda;
    }

    public Album getAlbum() throws ErrorMessageException {
        Album album = (Album) getResultObject(BdfInstructionConstants.ALBUM_OBJ);
        if (album == null) {
            Illustration illustration = (Illustration) getResultObject(BdfInstructionConstants.ILLUSTRATION_OBJ);
            if (illustration != null) {
                album = illustration.getAlbum();
            }
        }
        if (album == null) {
            album = getMandatoryAlbum();
        }
        checkSubsetAccess(album);
        return album;
    }

    public Sphere getSphere() throws ErrorMessageException {
        Sphere sphere = (Sphere) getResultObject(BdfInstructionConstants.SPHERE_OBJ);
        if (sphere == null) {
            Redacteur redacteur = (Redacteur) getResultObject(BdfInstructionConstants.REDACTEUR_OBJ);
            if (redacteur != null) {
                sphere = redacteur.getSphere();
            }
        }
        if (sphere == null) {
            sphere = getMandatorySphere();
        }
        checkSubsetAccess(sphere);
        return sphere;
    }

    public FicheMeta getFicheMeta() throws ErrorMessageException {
        FicheMeta ficheMeta = (FicheMeta) getResultObject(BdfInstructionConstants.FICHEMETA_OBJ);
        if (ficheMeta == null) {
            ficheMeta = getMandatoryFicheMeta(true);
        }
        return ficheMeta;
    }

    public Illustration getIllustration() throws ErrorMessageException {
        Illustration illustration = (Illustration) getResultObject(BdfInstructionConstants.ILLUSTRATION_OBJ);
        if (illustration == null) {
            illustration = getMandatoryIllustration();
        }
        return illustration;
    }

    public Document getDocument() throws ErrorMessageException {
        Document document = (Document) getResultObject(BdfInstructionConstants.DOCUMENT_OBJ);
        if (document == null) {
            document = getMandatoryDocument();
        }
        return document;
    }

    public Redacteur getRedacteur() throws ErrorMessageException {
        Redacteur redacteur = (Redacteur) getResultObject(BdfInstructionConstants.REDACTEUR_OBJ);
        if (redacteur == null) {
            redacteur = getMandatoryRedacteur();
        }
        return redacteur;
    }

    public Motcle getMotcle() throws ErrorMessageException {
        Motcle motcle = (Motcle) getResultObject(BdfInstructionConstants.MOTCLE_OBJ);
        if (motcle == null) {
            motcle = getMandatoryMotcle();
        }
        return motcle;
    }

    public Role getRole() throws ErrorMessageException {
        Role role = (Role) getResultObject(BdfInstructionConstants.ROLE_OBJ);
        if (role == null) {
            role = getMandatoryRole();
        }
        return role;
    }

    public SelectionDef getSelectionDef() throws ErrorMessageException {
        SelectionDef selectionDefDef = (SelectionDef) getResultObject(BdfInstructionConstants.SELECTIONDEF_OBJ);
        if (selectionDefDef == null) {
            selectionDefDef = getMandatorySelectionDef();
        }
        return selectionDefDef;
    }

    public ScrutariExportDef getScrutariExportDef() throws ErrorMessageException {
        ScrutariExportDef scrutariExportDef = (ScrutariExportDef) getResultObject(BdfInstructionConstants.SCRUTARIEXPORTDEF_OBJ);
        if (scrutariExportDef == null) {
            scrutariExportDef = getMandatoryScrutariExportDef();
        }
        return scrutariExportDef;
    }

    public SqlExportDef getSqlExportDef() throws ErrorMessageException {
        SqlExportDef sqlExportDef = (SqlExportDef) getResultObject(BdfInstructionConstants.SQLEXPORTDEF_OBJ);
        if (sqlExportDef == null) {
            sqlExportDef = getMandatorySqlExportDef();
        }
        return sqlExportDef;
    }

    public AccessDef getAccessDef() throws ErrorMessageException {
        AccessDef accessDef = (AccessDef) getResultObject(BdfInstructionConstants.ACCESSDEF_OBJ);;
        if (accessDef == null) {
            accessDef = getMandatoryAccessDef();
        }
        return accessDef;
    }

    public TableExportDescription getTableExportDescription() throws ErrorMessageException {
        TableExportDescription tableExportDescription = (TableExportDescription) getResultObject(BdfInstructionConstants.TABLEEXPORTDESCRIPTION_OBJ);
        if (tableExportDescription == null) {
            tableExportDescription = getMandatoryTableExportDescription();
        }
        return tableExportDescription;
    }

    public TransformationDescription getTransformationDescription() throws ErrorMessageException {
        TransformationDescription transformationDescription = (TransformationDescription) getResultObject(BdfInstructionConstants.TRANSFORMATIONDESCRIPTION_OBJ);
        if (transformationDescription == null) {
            transformationDescription = getMandatoryTransformationDescription();
        }
        return transformationDescription;
    }

    public TemplateDescription getTemplateDescription() throws ErrorMessageException {
        TemplateDescription templateDescription = (TemplateDescription) getResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ);
        if (templateDescription == null) {
            templateDescription = getMandatoryTemplateDescription();
        }
        return templateDescription;
    }

    public PathConfiguration getPathConfiguration() {
        PathConfiguration pathConfiguration = (PathConfiguration) getResultObject(BdfInstructionConstants.PATHCONFIGURATION_OBJ);
        if (pathConfiguration == null) {
            pathConfiguration = PathConfigurationBuilder.build(getBdfServer());
        }
        return pathConfiguration;
    }

    public BalayageDescription getBalayageDescription() throws ErrorMessageException {
        BalayageDescription balayageDescription = (BalayageDescription) getResultObject(BdfInstructionConstants.BALAYAGEDESCRIPTION_OBJ);
        if (balayageDescription == null) {
            balayageDescription = getMandatoryBalayageDescription();
        }
        return balayageDescription;
    }

    public TemplateContentDescription getTemplateContentDescription() throws ErrorMessageException {
        TemplateContentDescription templateContentDescription = (TemplateContentDescription) getResultObject(BdfInstructionConstants.TEMPLATECONTENTDESCRIPTION_OBJ);
        if (templateContentDescription == null) {
            TemplateDescription templateDescription = (TemplateDescription) getResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ);
            if (templateDescription == null) {
                templateDescription = getMandatoryTemplateDescription();
            }
            templateContentDescription = getMandatoryTemplateContentDescription(templateDescription);
        }
        return templateContentDescription;
    }

    public BalayageDescription getBalayage() throws ErrorMessageException {
        BalayageDescription balayageDescription = (BalayageDescription) getResultObject(BdfInstructionConstants.BALAYAGEDESCRIPTION_OBJ);
        if (balayageDescription == null) {
            balayageDescription = getMandatoryBalayageDescription();
        }
        return balayageDescription;
    }

    public RelativePath getRelativePath() throws ErrorMessageException {
        RelativePath path = (RelativePath) getResultObject(BdfInstructionConstants.RELATIVEPATH_OBJ);
        if (path == null) {
            path = getMandatoryRelativePath();
        }
        return path;
    }

    public static OutputRequestHandler init(OutputParameters outputParameters) {
        return new OutputRequestHandler(outputParameters);
    }

}
