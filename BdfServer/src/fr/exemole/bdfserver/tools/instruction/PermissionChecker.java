/* BdfServer - Copyright (c) 2020-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.instruction.PermissionException;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.tools.roles.PermissionCheck;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.addenda.Document;
import net.fichotheque.album.Illustration;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;


/**
 *
 * @author Vincent Calame
 */
public class PermissionChecker {

    private final PermissionSummary permissionSummary;

    public PermissionChecker(BdfParameters bdfParameters) {
        this.permissionSummary = bdfParameters.getPermissionSummary();
    }

    public PermissionChecker checkAdmin(String... actionNames) throws PermissionException {
        if (actionNames != null) {
            for (String actionName : actionNames) {
                PermissionCheck.checkAdmin(permissionSummary, actionName);
            }
        }
        return this;
    }

    public PermissionChecker checkHistory(Subset subset, int id) throws PermissionException {
        SubsetItem subsetItem = subset.getSubsetItemById(id);
        if ((subsetItem != null) && (subsetItem instanceof FicheMeta)) {
            checkRead((FicheMeta) subsetItem);
        } else {
            checkSubsetAdmin(subset);
        }
        return this;
    }

    public PermissionChecker checkSubsetAdmin(Subset subset) throws PermissionException {
        PermissionCheck.checkSubsetAdmin(permissionSummary, subset);
        return this;
    }

    public PermissionChecker checkSubsetAccess(Subset subset) throws PermissionException {
        PermissionCheck.checkSubsetAccess(permissionSummary, subset);
        return this;
    }

    public PermissionChecker checkWrite(Illustration illustration) throws PermissionException {
        PermissionCheck.checkWrite(permissionSummary, illustration);
        return this;
    }

    public PermissionChecker checkWrite(Document document) throws PermissionException {
        PermissionCheck.checkWrite(permissionSummary, document);
        return this;
    }

    public PermissionChecker checkWrite(FicheMeta ficheMeta) throws PermissionException {
        PermissionCheck.checkWrite(permissionSummary, ficheMeta);
        return this;
    }

    public PermissionChecker checkRead(FicheMeta ficheMeta) throws PermissionException {
        PermissionCheck.checkRead(permissionSummary, ficheMeta);
        return this;
    }

    public PermissionChecker checkFicheCreate(Corpus corpus) throws PermissionException {
        PermissionCheck.checkFicheCreate(permissionSummary, corpus);
        return this;
    }

    public static PermissionChecker init(BdfParameters bdfParameters) {
        return new PermissionChecker(bdfParameters);
    }

}
