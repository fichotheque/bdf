/* BdfServer - Copyright (c) 2012-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommandParameters;
import fr.exemole.bdfserver.api.interaction.Domain;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class BdfCommandParametersBuilder {

    private BdfCommandParametersBuilder() {
    }

    public static BdfCommandParameters build(String commandName, Domain defaultDomain, BdfServer bdfServer, RequestMap requestMap) {
        int idx = commandName.indexOf(':');
        if (idx == -1) {
            idx = commandName.indexOf('/');
        }
        Domain domain;
        if (idx == -1) {
            domain = defaultDomain;
        } else {
            domain = Domain.parse(commandName.substring(0, idx));
            commandName = commandName.substring(idx + 1);
        }
        return new InternalBdfCommandParameters(domain, commandName, bdfServer, requestMap);
    }


    private static class InternalBdfCommandParameters implements BdfCommandParameters {

        private final Domain domain;
        private final String commandName;
        private final BdfServer bdfServer;
        private final RequestMap requestMap;

        private InternalBdfCommandParameters(Domain domain, String commandName, BdfServer bdfServer, RequestMap requestMap) {
            this.domain = domain;
            this.commandName = commandName;
            this.bdfServer = bdfServer;
            this.requestMap = requestMap;
        }

        @Override
        public Domain getDomain() {
            return domain;
        }

        @Override
        public String getCommandName() {
            return commandName;
        }

        @Override
        public BdfServer getBdfServer() {
            return bdfServer;
        }

        @Override
        public RequestMap getRequestMap() {
            return requestMap;
        }

    }

}
