/* BdfServer_JsonProducers - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.json.BdfCommandResultJson;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 *
 * @author Vincent Calame
 */
public class ResultJsonProducer implements JsonProducer {

    private final MessageLocalisation messageLocalisation;
    private final List<Object> partList = new ArrayList<Object>();
    private BdfCommandResult bdfCommandResult;

    public ResultJsonProducer(OutputParameters parameters) {
        this(parameters.getBdfCommandResult(), parameters.getMessageLocalisation());
    }

    public ResultJsonProducer(BdfCommandResult bdfCommandResult, MessageLocalisation messageLocalisation) {
        if (messageLocalisation == null) {
            throw new IllegalArgumentException("messageLocalisation is null");
        }
        this.bdfCommandResult = bdfCommandResult;
        this.messageLocalisation = messageLocalisation;
    }

    public void setBdfCommandResult(BdfCommandResult bdfCommandResult) {
        this.bdfCommandResult = bdfCommandResult;
    }

    public void add(JsonProperty jsonProperty) {
        partList.add(jsonProperty);
    }

    public boolean isEmpty() {
        return partList.isEmpty();
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        for (Object obj : partList) {
            if (obj instanceof JsonProperty) {
                JsonProperty jsonProperty = (JsonProperty) obj;
                jw.key(jsonProperty.getName());
                jsonProperty.writeValue(jw);
            }
        }
        if (bdfCommandResult != null) {
            BdfCommandResultJson.properties(jw, bdfCommandResult, messageLocalisation);
        }
        jw.endObject();
    }

}
