/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.instruction.PermissionException;
import java.io.IOException;
import net.mapeadores.util.logging.CommandMessageBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.LogUtils;


/**
 *
 * @author Vincent Calame
 */
public final class BdfErrors {

    private BdfErrors() {

    }

    public static ErrorMessageException error(String messageKey) {
        return new ErrorMessageException(messageKey);
    }

    public static ErrorMessageException error(String messageKey, Object... values) {
        return new ErrorMessageException(messageKey, values);
    }

    public static ErrorMessageException error(CommandMessageBuilder builder, String errorTitleKey) {
        return new ErrorMessageException(builder.setError(errorTitleKey).toCommandMessage());
    }

    public static ErrorMessageException emptyMandatoryParameter(String paramName) {
        return new ErrorMessageException("_ error.empty.mandatoryparameter", paramName);
    }

    public static ErrorMessageException wrongParameterValue(String paramName, String paramValue) {
        return new ErrorMessageException("_ error.wrong.parametervalue", paramName, paramValue);
    }

    public static ErrorMessageException unknownParameterValue(String paramName, String paramValue) {
        return new ErrorMessageException("_ error.unknown.parametervalue", paramName, paramValue);
    }

    public static ErrorMessageException unsupportedParameterValue(String paramName, String paramValue) {
        return new ErrorMessageException("_ error.unsupported.parametervalue", paramName, paramValue);
    }

    public static ErrorMessageException unsupportedNotEditableParameterValue(String paramName, String paramValue) {
        return new ErrorMessageException("_ error.unsupported.parametervalue_noteditable", paramName, paramValue);
    }

    public static ErrorMessageException unknownPage(String pageValue) {
        return new ErrorMessageException("_ error.unknown.page", pageValue);
    }

    public static ErrorMessageException unknownJson(String jsonValue) {
        return new ErrorMessageException("_ error.unknown.json", jsonValue);
    }

    public static ErrorMessageException unknownStream(String streamValue) {
        return new ErrorMessageException("_ error.unknown.stream", streamValue);
    }

    public static ErrorMessageException ioException(IOException ioe) {
        return new ErrorMessageException("_ error.exception.io", ioe.getMessage());
    }

    public static ErrorMessageException internalError(String text) {
        return new ErrorMessageException("_ error.exception.bdfinstruction", text);
    }

    public static ErrorMessageException missingCommandResultPage(String page) {
        return new ErrorMessageException("_ error.unsupported.missingcommandresult_page", page);
    }

    public static ErrorMessageException missingCommandResultJson(String json) {
        return new ErrorMessageException("_ error.unsupported.missingcommandresult_json", json);
    }

    public static PermissionException permission(String messageKey, Object... values) {
        return new PermissionException(LogUtils.error(messageKey, values));
    }

}
