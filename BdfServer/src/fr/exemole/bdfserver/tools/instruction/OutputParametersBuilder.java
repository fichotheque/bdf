/* BdfServer - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.Domain;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.roles.PermissionSummaryBuilder;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class OutputParametersBuilder {

    private OutputParametersBuilder() {
    }

    public static OutputParameters build(String output, Domain defaultDomain, BdfServer bdfServer, RequestMap requestMap, BdfUser bdfUser, BdfCommandResult bdfCommandResult) {
        Domain domain;
        int idx = output.indexOf(':');
        if (idx == -1) {
            idx = output.indexOf('/');
        }
        if (idx == -1) {
            domain = defaultDomain;
        } else {
            domain = Domain.parse(output.substring(0, idx));
            output = output.substring(idx + 1);
        }
        return new InternalOutputParameters(output, bdfServer, requestMap, bdfUser, bdfCommandResult, domain);
    }


    private static class InternalOutputParameters implements OutputParameters {

        private final String output;
        private final BdfServer bdfServer;
        private final RequestMap requestMap;
        private final BdfUser bdfUser;
        private final BdfCommandResult bdfCommandResult;
        private final Domain domain;
        private PermissionSummary permissionSummary = null;

        private InternalOutputParameters(String output, BdfServer bdfServer, RequestMap requestMap, BdfUser bdfUser, BdfCommandResult bdfCommandResult, Domain domain) {
            this.output = output;
            this.bdfServer = bdfServer;
            this.requestMap = requestMap;
            this.bdfUser = bdfUser;
            this.bdfCommandResult = bdfCommandResult;
            this.domain = domain;
        }

        @Override
        public String getOutput() {
            return output;
        }

        @Override
        public BdfServer getBdfServer() {
            return bdfServer;
        }

        @Override
        public RequestMap getRequestMap() {
            return requestMap;
        }

        @Override
        public BdfUser getBdfUser() {
            return bdfUser;
        }

        @Override
        public BdfCommandResult getBdfCommandResult() {
            return bdfCommandResult;
        }

        @Override
        public Domain getDomain() {
            return domain;
        }

        @Override
        public PermissionSummary getPermissionSummary() {
            if (permissionSummary == null) {
                permissionSummary = PermissionSummaryBuilder.build(bdfServer, bdfUser);
            }
            return permissionSummary;
        }

    }

}
