/* BdfServer - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.BdfServer;
import java.util.Map;
import java.util.Set;
import net.fichotheque.Fichotheque;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractRequestHandler {

    protected final BdfServer bdfServer;
    protected final RequestMap requestMap;
    private Map<String, String> defaultMap = null;

    public AbstractRequestHandler(BdfServer bdfServer, RequestMap requestMap) {
        this.bdfServer = bdfServer;
        this.requestMap = requestMap;
    }

    public void setDefaultMap(Map<String, String> defaultMap) {
        this.defaultMap = defaultMap;
    }

    protected abstract void store(String paramName, String paramValue);

    public BdfServer getBdfServer() {
        return bdfServer;
    }

    public Fichotheque getFichotheque() {
        return bdfServer.getFichotheque();
    }

    public RequestMap getRequestMap() {
        return requestMap;
    }

    public Set<String> getParameterNameSet() {
        return requestMap.getParameterNameSet();
    }

    public boolean hasParameter(String paramName) {
        return (requestMap.getParameter(paramName) != null);
    }

    public boolean isTrue(String paramName) {
        String paramValue = requestMap.getTrimedParameter(paramName);
        if (paramValue == null) {
            return false;
        }
        store(paramName, paramValue);
        return StringUtils.isTrue(paramValue);
    }

    public String getParameter(String paramName, boolean mandatory) throws ErrorMessageException {
        if (mandatory) {
            return getMandatoryParameter(paramName);
        } else {
            return getTrimedParameter(paramName);
        }
    }

    public String getMandatoryParameter(String paramName) throws ErrorMessageException {
        String paramValue = requestMap.getParameter(paramName);
        if (paramValue == null) {
            throw BdfErrors.emptyMandatoryParameter(paramName);
        }
        store(paramName, paramValue);
        return paramValue;
    }

    public String getTrimedParameter(String paramName) {
        return getTrimedParameter(paramName, "");
    }

    public String getTrimedParameter(String paramName, boolean allowsNull) {
        if (allowsNull) {
            String paramValue = requestMap.getParameter(paramName);
            if (paramValue == null) {
                return null;
            } else {
                paramValue = paramValue.trim();
                if (paramValue.isEmpty()) {
                    if (defaultMap != null) {
                        return defaultMap.get(paramName);
                    }
                }
                return paramValue;
            }
        } else {
            return getTrimedParameter(paramName, "");
        }
    }

    public String getTrimedParameter(String paramName, String defaultValue) {
        String paramValue = requestMap.getTrimedParameter(paramName);
        if (paramValue.isEmpty()) {
            if (defaultMap != null) {
                String value = defaultMap.get(paramName);
                if ((value != null) && (!value.isEmpty())) {
                    return value;
                }
            }
            return defaultValue;
        } else {
            store(paramName, paramValue);
            return paramValue;
        }
    }

    public String[] getTokens(String paramName) {
        String[] tokens = requestMap.getParameterValues(paramName);
        if (tokens != null) {
            String compilation = StringUtils.implode(tokens, ';');
            store(paramName, compilation);
            return tokens;
        }
        if (defaultMap != null) {
            String value = defaultMap.get(paramName);
            if ((value != null) && (!value.isEmpty())) {
                return StringUtils.getTokens(value, ';', StringUtils.EMPTY_EXCLUDE);
            }
        }
        return StringUtils.EMPTY_STRINGARRAY;
    }

}
