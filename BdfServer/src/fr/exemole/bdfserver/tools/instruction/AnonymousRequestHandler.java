/* BdfServer - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.BdfServer;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AnonymousRequestHandler extends AbstractUtilRequestHandler {

    public AnonymousRequestHandler(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    protected void store(String paramName, String paramValue) {

    }

    public static AnonymousRequestHandler init(BdfServer bdfServer, RequestMap requestMap) {
        return new AnonymousRequestHandler(bdfServer, requestMap);
    }

}
