/* BdfServer - Copyright (c) 2009-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.users.BdfUser;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.handlers.RedirectResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public class RedirectBdfInstruction implements BdfInstruction  {

    private final String path;

    public RedirectBdfInstruction(String path) {
        this.path = path;
    }

    @Override
    public short getBdfUserNeed() {
        return BdfInstructionConstants.NONE_BDFUSER;
    }

    @Override
    public ResponseHandler runInstruction(BdfUser bdfUser) {
        return new RedirectResponseHandler(path);
    }

}
