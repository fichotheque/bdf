/* BdfServer - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.logging.CommandMessage;


/**
 *
 * @author Vincent Calame
 */
public class BdfCommandResultBuilder {

    private final Map<String, Object> resultObjectMap = new HashMap<String, Object>();
    private CommandMessage commandMessage;

    public BdfCommandResultBuilder() {
    }

    public BdfCommandResultBuilder putResultObject(String name, Object obj) {
        resultObjectMap.put(name, obj);
        return this;
    }

    public BdfCommandResultBuilder setCommandMessage(CommandMessage commandMessage) {
        this.commandMessage = commandMessage;
        return this;
    }

    public BdfCommandResult toBdfCommandResult() {
        return new InternalBdfCommandResult(resultObjectMap, commandMessage);
    }

    public static BdfCommandResultBuilder init() {
        return new BdfCommandResultBuilder();
    }


    private static class InternalBdfCommandResult implements BdfCommandResult {

        private final Map<String, Object> resultObjectMap;
        private final CommandMessage commandMessage;
        private final Set<String> resultObjectNameSet;

        private InternalBdfCommandResult(Map<String, Object> resultObjectMap, CommandMessage commandMessage) {
            this.resultObjectMap = resultObjectMap;
            this.resultObjectNameSet = Collections.unmodifiableSet(resultObjectMap.keySet());
            this.commandMessage = commandMessage;
        }

        @Override
        public Object getResultObject(String name) {
            return resultObjectMap.get(name);
        }

        @Override
        public CommandMessage getCommandMessage() {
            return commandMessage;
        }

        @Override
        public Set<String> getResultObjectNameSet() {
            return resultObjectNameSet;
        }

    }

}
