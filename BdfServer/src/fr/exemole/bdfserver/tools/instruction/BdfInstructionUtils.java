/* BdfServer - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommandParameters;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.instruction.TypedBdfInstruction;
import fr.exemole.bdfserver.api.interaction.Domain;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.exportation.table.CellEngineUtils;
import fr.exemole.bdfserver.tools.interaction.PropertiesParam;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.utils.TableExportUtils;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.LangPreferenceBuilder;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.request.RequestUtils;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class BdfInstructionUtils {

    public final static BdfInstruction NOTFOUND_BDFINSTRUCTION = new NotFoundBdfInstruction();


    private BdfInstructionUtils() {
    }

    public static MessageLocalisation getMessageLocalisation(RequestMap requestMap, BdfServer bdfServer, @Nullable UserLangContext userLangContext) {
        if (userLangContext != null) {
            return bdfServer.getL10nManager().getMessageLocalisation(userLangContext);
        } else {
            return bdfServer.getL10nManager().getMessageLocalisation(getLangPreference(requestMap, bdfServer));
        }
    }

    public static LangPreference getLangPreference(RequestMap requestMap, BdfServer bdfServer) {
        LangPreferenceBuilder langPreferenceBuilder = new LangPreferenceBuilder();
        RequestUtils.checkLangPreference(requestMap, langPreferenceBuilder);
        ConfigurationUtils.checkLangPreference(bdfServer.getLangConfiguration(), langPreferenceBuilder);
        langPreferenceBuilder.addLang(Lang.build("fr"));
        return langPreferenceBuilder.toLangPreference();
    }

    public static SubsetItem checkMasterSubsetItem(Corpus corpus, int id, BdfUser bdfUser) throws ErrorMessageException {
        Subset masterSubset = corpus.getMasterSubset();
        if (masterSubset != null) {
            SubsetItem masterSubsetItem = masterSubset.getSubsetItemById(id);
            if (masterSubsetItem == null) {
                throw BdfErrors.error("_ error.unknown.mastersubsetitem", id, masterSubset.getSubsetKeyString());
            }
            return masterSubsetItem;
        } else {
            return null;
        }
    }

    public static boolean checkConfirmation(RequestMap requestMap, BdfServer bdfServer) {
        String checkConfirmation = requestMap.getParameter(InteractionConstants.CONFIRMATIONCHECK_PARAMNAME);
        if (checkConfirmation == null) {
            return false;
        }
        return (checkConfirmation.equals(InteractionConstants.OK_CONFIRMATIONCHECK_PARAMVALUE));
    }

    public static String getFicheGetLink(FicheMeta ficheMeta, String extension) {
        return "fiches/" + ficheMeta.getSubsetName() + "-" + String.valueOf(ficheMeta.getId()) + "." + extension;
    }

    public static String getFicheGetLink(FicheMeta ficheMeta, String extension, String name) {
        return "fiches/" + ficheMeta.getSubsetName() + "-" + String.valueOf(ficheMeta.getId()) + "-" + name + "." + extension;
    }

    public static String getCompilationGetLink(String extension) {
        return "fiches/compilation." + extension;
    }

    public static String getCompilationGetLink(String extension, String name) {
        return "fiches/_compilation-" + name + "." + extension;
    }

    public static String getExtensionDomain(String registrationName) {
        return Domain.getCompleteDomain(Domains.EXTENSION, registrationName);
    }

    public static boolean ownsToExtension(BdfCommandParameters bdfCommandParameters, String registrationName) {
        return isExtensionDomain(bdfCommandParameters.getDomain(), registrationName);
    }

    public static boolean ownsToExtension(OutputParameters parameters, String registrationName) {
        return isExtensionDomain(parameters.getDomain(), registrationName);
    }

    public static boolean isExtensionDomain(Domain domain, String registrationName) {
        return (domain.getFirstPart().equals(Domains.EXTENSION)) && (domain.getSecondPart().equals(registrationName));
    }

    public static String getAbsolutePageName(String domain, String page) {
        return domain + ":" + page;
    }

    public static String getAbsoluteCommandName(String domain, String commandName) {
        return domain + ":" + commandName;
    }

    public static String getPageOutput(String pageName) {
        return "page|" + pageName;
    }

    public static String getStreamOutput(String streamName) {
        return "stream|" + streamName;
    }

    public static String getJsonOutput(String jsonName) {
        return "json|" + jsonName;
    }

    public static boolean isJsonOutput(BdfInstruction bdfInstruction) {
        if (bdfInstruction instanceof TypedBdfInstruction) {
            return (((TypedBdfInstruction) bdfInstruction).getOutputType() == TypedBdfInstruction.JSON_TYPE);
        }
        return false;
    }

    public static String getPhraseParamPrefix(String phraseName) {
        return "phrase_" + phraseName + "/";
    }

    public static CellConverter getCellConverter(OutputParameters outputParameters) {
        return (CellConverter) getCellEngine(outputParameters, outputParameters.getRequestMap(), false);
    }

    public static CellConverter getCellConverter(OutputParameters outputParameters, boolean noDefault) {
        return (CellConverter) getCellEngine(outputParameters, outputParameters.getRequestMap(), noDefault);
    }

    public static CellEngine getCellEngine(BdfParameters bdfParameters, RequestMap requestMap, boolean noDefault) {
        BdfServer bdfServer = bdfParameters.getBdfServer();
        PropertiesParam propertiesParam = PropertiesParam.fromRequest(requestMap);
        CellEngine cellEngine = null;
        if (propertiesParam.isTableExport()) {
            ExtractionContext extractionContext = bdfParameters.getDefaultExtractionContext();
            cellEngine = CellEngineUtils.newCellEngine(bdfServer, extractionContext, propertiesParam.getName());
        }
        if (cellEngine == null) {
            if (!noDefault) {
                cellEngine = TableExportUtils.EMPTY_CELLENGINE;
            }
        }
        return cellEngine;
    }


    private static class NotFoundBdfInstruction implements BdfInstruction {

        private NotFoundBdfInstruction() {

        }

        @Override
        public short getBdfUserNeed() {
            return BdfInstructionConstants.NONE_BDFUSER;
        }

        @Override
        public ResponseHandler runInstruction(BdfUser bdfUser) {
            return null;
        }

    }

}
