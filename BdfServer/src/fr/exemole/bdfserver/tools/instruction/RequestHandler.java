/* BdfServer - Copyright (c) 2024-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.users.BdfUser;
import java.util.LinkedHashMap;
import java.util.Map;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RequestHandler extends AbstractUtilRequestHandler implements BdfParameters {

    protected final BdfParameters bdfParameters;
    private Map<String, String> storeMap = null;
    private boolean store = false;

    public RequestHandler(BdfParameters bdfParameters, RequestMap requestMap) {
        super(bdfParameters.getBdfServer(), requestMap);
        this.bdfParameters = bdfParameters;
    }

    public RequestHandler disableStore() {
        this.store = false;
        return this;
    }

    public RequestHandler enableStore() {
        this.store = true;
        if (storeMap == null) {
            this.storeMap = new LinkedHashMap<String, String>();
        }
        return this;
    }

    @Override
    public BdfUser getBdfUser() {
        return bdfParameters.getBdfUser();
    }

    @Override
    public PermissionSummary getPermissionSummary() {
        return bdfParameters.getPermissionSummary();
    }

    @Override
    protected void store(String paramName, String paramValue) {
        if (store) {
            storeMap.put(paramName, paramValue);
        }
    }

    public void store(String storeName) {
        if (storeMap != null) {
            bdfServer.store(getBdfUser(), storeName, storeMap);
            storeMap.clear();
        }
    }

    public FicheMeta getMandatoryFicheMeta(boolean alternativeError) throws ErrorMessageException {
        if (!alternativeError) {
            return getMandatoryFicheMeta();
        }
        Corpus corpus = getMandatoryCorpus();
        int id = getMandatoryId();
        FicheMeta ficheMeta = corpus.getFicheMetaById(id);
        if (ficheMeta == null) {
            String corpusTitle = FichothequeUtils.getTitle(corpus, getWorkingLang());
            throw BdfErrors.error("_ error.unknown.fiche", corpusTitle, id);
        }
        return ficheMeta;
    }


    public static RequestHandler init(BdfParameters bdfParameters, RequestMap requestMap) {
        return new RequestHandler(bdfParameters, requestMap);
    }

    public static RequestHandler cloneHandler(RequestHandler requestHandler) {
        return new RequestHandler(requestHandler.bdfParameters, requestHandler.requestMap);
    }

}
