/* BdfServer - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.instruction;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.roles.PermissionSummaryBuilder;


/**
 *
 * @author Vincent Calame
 */
public class DefaultBdfParameters implements BdfParameters {

    private final BdfServer bdfServer;
    private final BdfUser bdfUser;
    private PermissionSummary permissionSummary = null;

    public DefaultBdfParameters(BdfServer bdfServer, BdfUser bdfUser) {
        this.bdfServer = bdfServer;
        this.bdfUser = bdfUser;
    }

    @Override
    public BdfServer getBdfServer() {
        return bdfServer;
    }

    @Override
    public BdfUser getBdfUser() {
        return bdfUser;
    }

    @Override
    public PermissionSummary getPermissionSummary() {
        if (permissionSummary == null) {
            permissionSummary = PermissionSummaryBuilder.build(bdfServer, bdfUser);
        }
        return permissionSummary;
    }

}
