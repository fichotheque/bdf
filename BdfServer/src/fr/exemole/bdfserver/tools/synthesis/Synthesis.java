/* BdfServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.synthesis;

import fr.exemole.bdfserver.api.BdfServer;
import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Sphere;


/**
 *
 * @author Vincent Calame
 */
public class Synthesis {

    private final SortedMap<String, BdfServer> fichothequeMap;
    private final SortedMap<String, SphereSynthesis> synthesisMap;


    private Synthesis(SortedMap<String, BdfServer> fichothequeMap, SortedMap<String, SphereSynthesis> synthesisMap) {
        this.fichothequeMap = fichothequeMap;
        this.synthesisMap = synthesisMap;
    }

    public SphereSynthesis[] getSphereSynthesisArray() {
        return synthesisMap.values().toArray(new SphereSynthesis[synthesisMap.size()]);
    }

    public BdfServer[] getBdfServerArray() {
        return fichothequeMap.values().toArray(new BdfServer[fichothequeMap.size()]);
    }

    public BdfServer getBdfServer(String name) {
        return fichothequeMap.get(name);
    }


    public static class Builder {

        private final SortedMap<String, BdfServer> fichothequeMap = new TreeMap<String, BdfServer>();
        private final SortedMap<String, SphereSynthesis> synthesisMap = new TreeMap<String, SphereSynthesis>();

        public Builder() {

        }

        public void addFichotheque(BdfServer bdfServer) {
            String name = bdfServer.getName();
            Fichotheque fichotheque = bdfServer.getFichotheque();
            for (Sphere sphere : fichotheque.getSphereList()) {
                SubsetKey sphereKey = sphere.getSubsetKey();
                String sphereName = sphereKey.getSubsetName();
                SphereSynthesis current = synthesisMap.get(sphereName);
                if (current == null) {
                    current = new SphereSynthesis(sphereKey);
                    synthesisMap.put(sphereName, current);
                }
                current.addSphere(bdfServer, sphere);
                fichothequeMap.put(name, bdfServer);
            }
        }

        public Synthesis toSynthesis() {
            return new Synthesis(new TreeMap<String, BdfServer>(fichothequeMap), new TreeMap<String, SphereSynthesis>(synthesisMap));
        }

    }

}
