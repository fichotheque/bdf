/* BdfServer - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ficheform.ThesaurusIncludeElement;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.tools.ficheform.FicheFormUtils;
import java.util.Collection;
import java.util.List;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class HiddenThesaurusIncludeElementBuilder {

    private final String name;
    private final Thesaurus thesaurus;
    private final MotcleEntryListBuilder motcleEntryListBuilder = new MotcleEntryListBuilder();
    private String label = "";
    private boolean mandatory = false;
    private String value = "";
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;

    public HiddenThesaurusIncludeElementBuilder(String name, Thesaurus thesaurus) {
        this.name = name;
        this.thesaurus = thesaurus;
    }

    public HiddenThesaurusIncludeElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public HiddenThesaurusIncludeElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public HiddenThesaurusIncludeElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public HiddenThesaurusIncludeElementBuilder setValue(String value) {
        if (value == null) {
            throw new IllegalArgumentException("value is null");
        }
        this.value = value;
        return this;
    }

    public ThesaurusIncludeElement.Hidden toHiddenIncludeElement() {
        return new InternalHiddenThesaurusIncludeElement(name, label, mandatory, attributes, thesaurus, value, motcleEntryListBuilder.toList());
    }

    @Nullable
    public static HiddenThesaurusIncludeElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, SubsetIncludeUi includeUi) {
        Thesaurus thesaurus = (Thesaurus) ficheFormParameters.getBdfServer().getFichotheque().getSubset(includeUi.getSubsetKey());
        if (thesaurus == null) {
            return null;
        }
        HiddenThesaurusIncludeElementBuilder builder = init(includeUi.getName(), thesaurus)
                .setAttributes(includeUi.getAttributes());
        SubsetItemPointeur pointeur = FicheFormUtils.checkMasterPointeur(fichePointeur, ficheFormParameters, includeUi);
        String value;
        if (pointeur.isEmpty()) {
            builder.motcleEntryListBuilder.populateDefault(ficheFormParameters, includeUi, thesaurus);
            value = FicheFormUtils.getDefVal(ficheFormParameters, includeUi);
        } else {
            Collection<Liaison> liaisons = builder.motcleEntryListBuilder.populate(pointeur.getCroisements(thesaurus), includeUi, true);
            value = FicheFormUtils.getStringValue(thesaurus, liaisons, includeUi, ficheFormParameters);
        }
        if ((builder.motcleEntryListBuilder.isEmpty()) && (includeUi.isObsolete())) {
            return null;
        }
        return builder
                .setValue(value);
    }

    public static HiddenThesaurusIncludeElementBuilder init(String name, Thesaurus thesaurus) {
        return new HiddenThesaurusIncludeElementBuilder(name, thesaurus);
    }


    private static class InternalHiddenThesaurusIncludeElement implements ThesaurusIncludeElement.Hidden {

        private final String name;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final Thesaurus thesaurus;
        private final String value;
        private final List<ThesaurusIncludeElement.Entry> list;

        private InternalHiddenThesaurusIncludeElement(String name, String label, boolean mandatory, Attributes attributes, Thesaurus thesaurus, String value, List<ThesaurusIncludeElement.Entry> list) {
            this.name = name;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.thesaurus = thesaurus;
            this.value = value;
            this.list = list;
        }

        @Override
        public String getIncludeName() {
            return name;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public Thesaurus getThesaurus() {
            return thesaurus;
        }

        @Override
        public String getValue() {
            return value;
        }

        @Override
        public Attribute getIdalphaStyle() {
            return null;
        }

        @Override
        public List<ThesaurusIncludeElement.Entry> getEntryList() {
            return list;
        }

    }

}
