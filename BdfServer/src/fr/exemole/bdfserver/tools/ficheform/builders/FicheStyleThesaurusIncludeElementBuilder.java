/* BdfServer - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ficheform.ThesaurusIncludeElement;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.tools.L10nUtils;
import fr.exemole.bdfserver.tools.externalsource.ExternalSourceUtils;
import fr.exemole.bdfserver.tools.ficheform.FicheFormUtils;
import java.util.List;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class FicheStyleThesaurusIncludeElementBuilder {

    private final String name;
    private final Thesaurus thesaurus;
    private final MotcleEntryListBuilder motcleEntryListBuilder = new MotcleEntryListBuilder();
    private String label = "";
    private boolean mandatory = false;
    private boolean hasPoidsFilter;
    private Attribute idalphaStyle = null;
    private boolean withExternalSource;
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;

    public FicheStyleThesaurusIncludeElementBuilder(String name, Thesaurus thesaurus) {
        this.name = name;
        this.thesaurus = thesaurus;
    }

    public FicheStyleThesaurusIncludeElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public FicheStyleThesaurusIncludeElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public FicheStyleThesaurusIncludeElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public FicheStyleThesaurusIncludeElementBuilder setIdalphaStyle(Attribute idalphaStyle) {
        this.idalphaStyle = idalphaStyle;
        return this;
    }

    public FicheStyleThesaurusIncludeElementBuilder setHasPoidsFilter(boolean hasPoidsFilter) {
        this.hasPoidsFilter = hasPoidsFilter;
        return this;
    }

    public FicheStyleThesaurusIncludeElementBuilder addMotcle(Motcle motcle, int poids) {
        motcleEntryListBuilder.add(motcle, poids);
        return this;
    }

    public FicheStyleThesaurusIncludeElementBuilder setWithExternalSource(boolean withExternalSource) {
        this.withExternalSource = withExternalSource;
        return this;
    }

    public ThesaurusIncludeElement.FicheStyle toFicheStyleThesaurusIncludeElement() {
        return new InternalFicheStyleThesaurusIncludeElement(name, label, mandatory, attributes, thesaurus, idalphaStyle, hasPoidsFilter, motcleEntryListBuilder.toList(), withExternalSource);
    }

    @Nullable
    public static FicheStyleThesaurusIncludeElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, SubsetIncludeUi includeUi) {
        Thesaurus thesaurus = (Thesaurus) ficheFormParameters.getBdfServer().getFichotheque().getSubset(includeUi.getSubsetKey());
        if (thesaurus == null) {
            return null;
        }
        boolean isMandatory = includeUi.isMandatory();
        boolean withExternalSource = (ExternalSourceUtils.getExternalSource(ficheFormParameters.getBdfServer(), thesaurus) != null);
        FicheStyleThesaurusIncludeElementBuilder builder = init(includeUi.getName(), thesaurus)
                .setMandatory(isMandatory)
                .setAttributes(includeUi.getAttributes())
                .setIdalphaStyle(FicheFormUtils.getIdalphaStyle(includeUi, thesaurus))
                .setLabel(L10nUtils.toLabelString(ficheFormParameters, includeUi))
                .setHasPoidsFilter(includeUi.getExtendedIncludeKey().hasPoidsFilter())
                .setWithExternalSource(withExternalSource);
        SubsetItemPointeur pointeur = FicheFormUtils.checkMasterPointeur(fichePointeur, ficheFormParameters, includeUi);
        if (!pointeur.isEmpty()) {
            builder.motcleEntryListBuilder.populate(pointeur.getCroisements(thesaurus), includeUi, true);
        }
        if ((builder.motcleEntryListBuilder.isEmpty()) && (includeUi.isObsolete())) {
            return null;
        }
        return builder;
    }

    public static FicheStyleThesaurusIncludeElementBuilder init(String name, Thesaurus thesaurus) {
        return new FicheStyleThesaurusIncludeElementBuilder(name, thesaurus);
    }


    private static class InternalFicheStyleThesaurusIncludeElement implements ThesaurusIncludeElement.FicheStyle {

        private final String name;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final Thesaurus thesaurus;
        private final Attribute idalphaStyle;
        private final boolean withPoidsFilter;
        private final List<ThesaurusIncludeElement.Entry> list;
        private final boolean withExternalSource;

        private InternalFicheStyleThesaurusIncludeElement(String name, String label, boolean mandatory, Attributes attributes, Thesaurus thesaurus, Attribute idalphaStyle, boolean withPoidsFilter, List<ThesaurusIncludeElement.Entry> list, boolean withExternalSource) {
            this.name = name;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.thesaurus = thesaurus;
            this.idalphaStyle = idalphaStyle;
            this.withPoidsFilter = withPoidsFilter;
            this.list = list;
            this.withExternalSource = withExternalSource;
        }

        @Override
        public String getIncludeName() {
            return name;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public Thesaurus getThesaurus() {
            return thesaurus;
        }

        @Override
        public Attribute getIdalphaStyle() {
            return idalphaStyle;
        }

        @Override
        public boolean hasPoidsFilter() {
            return withPoidsFilter;
        }

        @Override
        public List<Entry> getEntryList() {
            return list;
        }

        @Override
        public boolean isWithExternalSource() {
            return withExternalSource;
        }

    }

}
