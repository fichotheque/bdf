/* BdfServer - Copyright (c) 2009-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.exportation.table.FicheTableParameters;
import fr.exemole.bdfserver.api.ficheform.CorpusIncludeElement;
import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ui.IncludeUi;
import fr.exemole.bdfserver.api.ui.SpecialIncludeUi;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.tools.L10nUtils;
import fr.exemole.bdfserver.tools.exportation.table.CellEngineUtils;
import fr.exemole.bdfserver.tools.exportation.table.DefaultTableDefFactory;
import fr.exemole.bdfserver.tools.ficheform.FicheFormUtils;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableDef;
import net.fichotheque.include.ExtendedIncludeKey;
import net.fichotheque.include.LiageTest;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.tools.exportation.table.SubsetTableBuilder;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class CorpusIncludeElementBuilder {

    private final static String CHECK_TYPE = "check";
    private final static String TABLE_TYPE = "table";
    private final String name;
    private final Corpus corpus;
    private final List<InternalEntry> entryList = new ArrayList<InternalEntry>();
    private String type = CHECK_TYPE;
    private String label = "";
    private boolean mandatory = false;
    private boolean hasPoidsFilter = false;
    private int rows = 1;
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;
    private CellEngine cellEngine;

    public CorpusIncludeElementBuilder(String name, Corpus corpus) {
        this.name = name;
        this.corpus = corpus;
    }

    public CorpusIncludeElementBuilder setHasPoidsFilter(boolean hasPoidsFilter) {
        this.hasPoidsFilter = hasPoidsFilter;
        return this;
    }

    public CorpusIncludeElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public CorpusIncludeElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public CorpusIncludeElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public CorpusIncludeElementBuilder setRows(int rows) {
        this.rows = rows;
        return this;
    }

    public CorpusIncludeElementBuilder setCellEngine(CellEngine cellEngine) {
        this.cellEngine = cellEngine;
        if (cellEngine == null) {
            type = CHECK_TYPE;
        } else {
            type = TABLE_TYPE;
        }
        return this;
    }

    public CorpusIncludeElementBuilder addFicheMeta(FicheMeta ficheMeta, int poids) {
        boolean globalLiage = (name.equals(FichothequeConstants.LIAGE_NAME));
        String value;
        if (globalLiage) {
            value = ficheMeta.getGlobalId();
        } else {
            value = String.valueOf(ficheMeta.getId());
        }
        if (poids > 1) {
            value = value + "<" + poids + ">";
        }
        entryList.add(new InternalEntry(ficheMeta, poids, value));
        return this;
    }

    public CorpusIncludeElementBuilder populateOptions(FicheFormParameters ficheFormParameters, IncludeUi includeUi) {
        return this
                .setLabel(L10nUtils.toLabelString(ficheFormParameters, includeUi))
                .setMandatory(includeUi.isMandatory())
                .setAttributes(includeUi.getAttributes());
    }

    public CorpusIncludeElement toCorpusIncludeElement() {
        List<CorpusIncludeElement.Entry> finalList = wrap(entryList.toArray(new InternalEntry[entryList.size()]));
        if (type.equals(TABLE_TYPE)) {
            return new InternalTableCorpusIncludeElement(name, corpus, label, mandatory, attributes, finalList, cellEngine);
        } else {
            return new InternalCheckCorpusIncludeElement(name, corpus, hasPoidsFilter, label, mandatory, attributes, finalList, rows);
        }
    }

    @Nullable
    public static CorpusIncludeElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, SpecialIncludeUi includeUi) {
        if (!includeUi.getName().equals(FichothequeConstants.LIAGE_NAME)) {
            return null;
        }
        CorpusIncludeElementBuilder builder = init(FichothequeConstants.LIAGE_NAME, null)
                .setHasPoidsFilter(false)
                .setRows(4);
        if (!fichePointeur.isEmpty()) {
            Collection<Liaison> liaisons = CroisementUtils.getNoList(fichePointeur, "", getLiageTest(fichePointeur, ficheFormParameters), ficheFormParameters.getPermissionSummary().getSubsetAccessPredicate(), null);
            for (Liaison liaison : liaisons) {
                builder.addFicheMeta((FicheMeta) liaison.getSubsetItem(), liaison.getLien().getPoids());
            }
        }
        if (includeUi.isObsolete()) {
            if (fichePointeur.isEmpty()) {
                return null;
            } else {
                if (builder.entryList.isEmpty()) {
                    return null;
                }
            }
        }
        return builder
                .populateOptions(ficheFormParameters, includeUi);
    }

    @Nullable
    public static CorpusIncludeElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, SubsetIncludeUi includeUi) {
        Corpus corpus = (Corpus) ficheFormParameters.getBdfServer().getFichotheque().getSubset(includeUi.getSubsetKey());
        if (corpus == null) {
            return null;
        }
        ExtendedIncludeKey includeKey = includeUi.getExtendedIncludeKey();
        SubsetItemPointeur pointeur = FicheFormUtils.checkMasterPointeur(fichePointeur, ficheFormParameters, includeUi);
        CorpusIncludeElementBuilder builder = CorpusIncludeElementBuilder.init(includeUi.getName(), corpus)
                .setHasPoidsFilter(includeKey.hasPoidsFilter());
        populate(builder, corpus, pointeur, includeKey);
        if ((builder.entryList.isEmpty()) && (includeUi.isObsolete())) {
            return null;
        }
        builder
                .populateOptions(ficheFormParameters, includeUi);
        if (includeUi.isFicheTable()) {
            BdfServer bdfServer = ficheFormParameters.getBdfServer();
            List<UiComponent> uiComponentList = UiUtils.filterFicheTableUiComponents(bdfServer.getUiManager().getMainUiComponents(corpus), fichePointeur.getSubsetKey());
            TableDef tableDef = DefaultTableDefFactory.fromComponentList(bdfServer, corpus, uiComponentList, FicheTableParameters.LABEL_PATTERNMODE, ficheFormParameters.getPermissionSummary());
            SubsetTable subsetTable = SubsetTableBuilder.init(corpus).populate(tableDef, bdfServer.getTableExportContext()).toSubsetTable();
            builder.setCellEngine(CellEngineUtils.newCellEngine(bdfServer, ficheFormParameters.getDefaultExtractionContext(), null, subsetTable));
        }
        return builder;
    }

    private static void populate(CorpusIncludeElementBuilder builder, Corpus corpus, SubsetItemPointeur pointeur, ExtendedIncludeKey includeKey) {
        Collection<Liaison> liaisons = pointeur.getLiaisons(corpus, includeKey);
        if (includeKey.hasPoidsFilter()) {
            for (Liaison liaison : liaisons) {
                builder.addFicheMeta((FicheMeta) liaison.getSubsetItem(), -1);
            }
        } else {
            for (Liaison liaison : liaisons) {
                builder.addFicheMeta((FicheMeta) liaison.getSubsetItem(), liaison.getLien().getPoids());
            }
        }
    }

    public static CorpusIncludeElementBuilder init(String name, Corpus corpus) {
        return new CorpusIncludeElementBuilder(name, corpus);
    }

    private static List<CorpusIncludeElement.Entry> wrap(CorpusIncludeElement.Entry[] array) {
        return new EntryList(array);
    }

    private static LiageTest getLiageTest(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters) {
        String liageTestName = LiageTest.class.getName();
        Object obj = fichePointeur.getPointeurObject(liageTestName);
        if ((obj != null) && (obj instanceof LiageTest)) {
            return (LiageTest) obj;
        }
        LiageTest liageTest = UiUtils.checkLiageTest(ficheFormParameters.getBdfServer().getUiManager().getMainUiComponents(fichePointeur.getCorpus()));
        fichePointeur.putPointeurObject(liageTestName, liageTest);
        return liageTest;
    }


    private static class InternalCheckCorpusIncludeElement implements CorpusIncludeElement.Check {

        private final String name;
        private final Corpus corpus;
        private final boolean withPoidsFilter;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final List<CorpusIncludeElement.Entry> entryList;
        private final int rows;


        private InternalCheckCorpusIncludeElement(String name, Corpus corpus, boolean withPoidsFilter, String label, boolean mandatory, Attributes attributes, List<CorpusIncludeElement.Entry> entryList, int rows) {
            this.name = name;
            this.withPoidsFilter = withPoidsFilter;
            this.corpus = corpus;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.rows = rows;
            this.entryList = entryList;
        }

        @Override
        public String getIncludeName() {
            return name;
        }

        @Override
        public Corpus getCorpus() {
            return corpus;
        }

        @Override
        public boolean hasPoidsFilter() {
            return withPoidsFilter;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public List<Entry> getEntryList() {
            return entryList;
        }

        @Override
        public int getRows() {
            return rows;
        }


    }


    private static class InternalTableCorpusIncludeElement implements CorpusIncludeElement.Table {

        private final String name;
        private final Corpus corpus;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final List<CorpusIncludeElement.Entry> entryList;
        private final CellEngine cellEngine;


        private InternalTableCorpusIncludeElement(String name, Corpus corpus, String label, boolean mandatory, Attributes attributes, List<CorpusIncludeElement.Entry> entryList, CellEngine cellEngine) {
            this.name = name;
            this.corpus = corpus;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.entryList = entryList;
            this.cellEngine = cellEngine;
        }

        @Override
        public String getIncludeName() {
            return name;
        }

        @Override
        public Corpus getCorpus() {
            return corpus;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public List<Entry> getEntryList() {
            return entryList;
        }

        @Override
        public CellEngine getCellEngine() {
            return cellEngine;
        }


    }


    private static class InternalEntry implements CorpusIncludeElement.Entry {

        private final FicheMeta ficheMeta;
        private final int poids;
        private final String value;

        private InternalEntry(FicheMeta ficheMeta, int poids, String value) {
            this.ficheMeta = ficheMeta;
            this.poids = poids;
            this.value = value;
        }

        @Override
        public FicheMeta getFicheMeta() {
            return ficheMeta;
        }

        @Override
        public int getPoids() {
            return poids;
        }

        @Override
        public String getValue() {
            return value;
        }

    }


    private static class EntryList extends AbstractList<CorpusIncludeElement.Entry> implements RandomAccess {

        private final CorpusIncludeElement.Entry[] array;

        private EntryList(CorpusIncludeElement.Entry[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public CorpusIncludeElement.Entry get(int index) {
            return array[index];
        }

    }

}
