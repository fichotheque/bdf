/* BdfServer - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.ficheform.AlbumIncludeElement;
import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.tools.L10nUtils;
import java.util.Collection;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.include.ExtendedIncludeKey;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.utils.AlbumUtils;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class AlbumIncludeElementBuilder {

    private final String name;
    private final Album album;
    private String label = "";
    private boolean mandatory = false;
    private Collection<Liaison> liaisons = CroisementUtils.EMPTY_LIAISONLIST;
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;


    public AlbumIncludeElementBuilder(String name, Album album) {
        this.name = name;
        this.album = album;
    }

    public AlbumIncludeElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public AlbumIncludeElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public AlbumIncludeElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public AlbumIncludeElementBuilder setLiaisons(Collection<Liaison> liaisons) {
        if (liaisons == null) {
            this.liaisons = CroisementUtils.EMPTY_LIAISONLIST;
        } else {
            this.liaisons = liaisons;
        }
        return this;
    }

    public AlbumIncludeElement toAlbumIncludeElement() {
        int size = liaisons.size();
        Illustration[] illustrationArray = new Illustration[size];
        int p = 0;
        for (Liaison liaison : liaisons) {
            illustrationArray[p] = (Illustration) liaison.getSubsetItem();
            p++;
        }
        return new InternalAlbumIncludeElement(name, album, label, mandatory, attributes, AlbumUtils.wrap(illustrationArray));
    }

    @Nullable
    public static AlbumIncludeElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, SubsetIncludeUi includeUi) {
        ExtendedIncludeKey includeKey = includeUi.getExtendedIncludeKey();
        SubsetKey albumKey = includeKey.getSubsetKey();
        Album album = (Album) ficheFormParameters.getBdfServer().getFichotheque().getSubset(albumKey);
        if (album == null) {
            return null;
        }
        Collection<Liaison> liaisons = fichePointeur.getLiaisons(album, includeKey);
        if ((liaisons.isEmpty()) && (includeUi.isObsolete())) {
            return null;
        }
        return init(includeUi.getName(), album)
                .setLiaisons(liaisons)
                .setLabel(L10nUtils.toLabelString(ficheFormParameters, includeUi))
                .setAttributes(includeUi.getAttributes());
    }

    public static AlbumIncludeElementBuilder init(String name, Album album) {
        return new AlbumIncludeElementBuilder(name, album);
    }


    private static class InternalAlbumIncludeElement implements AlbumIncludeElement {

        private final String name;
        private final Album album;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final List<Illustration> illustrationList;

        private InternalAlbumIncludeElement(String name, Album album, String label, boolean mandatory, Attributes attributes, List<Illustration> illustrationList) {
            this.name = name;
            this.album = album;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.illustrationList = illustrationList;
        }

        @Override
        public String getIncludeName() {
            return name;
        }

        @Override
        public Album getAlbum() {
            return album;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public List<Illustration> getIllustrationList() {
            return illustrationList;
        }

    }

}
