/* BdfServer - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ficheform.MontantInformationSubfieldsElement;
import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.tools.L10nUtils;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.syntax.FormSyntax;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.money.Currencies;
import net.mapeadores.util.money.ExtendedCurrency;
import net.mapeadores.util.text.SeparatorOptions;


/**
 *
 * @author Vincent Calame
 */
public class MontantInformationSubfieldsElementBuilder {

    private final CorpusField corpusField;
    private final Map<ExtendedCurrency, String> currencyMap = new LinkedHashMap<ExtendedCurrency, String>();
    private String label = "";
    private boolean mandatory = false;
    private String othersValue = "";
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;

    public MontantInformationSubfieldsElementBuilder(CorpusField corpusField) {
        this.corpusField = corpusField;
        Currencies currencies = corpusField.getCurrencies();
        if (currencies == null) {
            throw new IllegalArgumentException("corpusField.getFieldOptions().getCurrencies() is null");
        }
        for (ExtendedCurrency currency : currencies) {
            currencyMap.put(currency, "");
        }
    }

    public MontantInformationSubfieldsElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public MontantInformationSubfieldsElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public MontantInformationSubfieldsElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public MontantInformationSubfieldsElementBuilder putCurrency(ExtendedCurrency currency, String montantValue) {
        if (montantValue == null) {
            throw new IllegalArgumentException("montantValue is null");
        }
        if (currencyMap.containsKey(currency)) {
            currencyMap.put(currency, montantValue);
        }
        return this;
    }

    public MontantInformationSubfieldsElementBuilder setOthersValue(String othersValue) {
        if (othersValue == null) {
            throw new IllegalArgumentException("othersValue is null");
        }
        this.othersValue = othersValue;
        return this;
    }

    private MontantInformationSubfieldsElementBuilder setCurrencyFromFicheItems(FicheItems ficheItems, FicheFormParameters ficheFormParameters) {
        if (ficheItems == null) {
            return this;
        }
        FormSyntax.Parameters syntaxParameters = ficheFormParameters.getFicheItemFormSyntaxParameters();
        List<FicheItem> othersList = new ArrayList<FicheItem>();
        char decimalChar = syntaxParameters.decimalChar();
        for (FicheItem ficheItem : ficheItems) {
            if (ficheItem instanceof Montant) {
                Montant montant = (Montant) ficheItem;
                if (!currencyMap.containsKey(montant.getCurrency())) {
                    othersList.add(montant);
                } else {
                    currencyMap.put(montant.getCurrency(), montant.getDecimal().toStringWithBlank(decimalChar));
                }
            } else {
                othersList.add(ficheItem);
            }
        }
        if (!othersList.isEmpty()) {
            boolean isBlock = corpusField.isBlockDisplayInformationField();
            SeparatorOptions separatorOptions = (isBlock) ? ficheFormParameters.getBlockSeparatorOptions() : ficheFormParameters.getInlineSeparatorOptions();
            setOthersValue(FormSyntax.toString(FicheUtils.toFicheItems(othersList), ficheFormParameters.getBdfServer().getFichotheque(), separatorOptions, syntaxParameters));
        }
        return this;
    }

    public MontantInformationSubfieldsElement toMontantInformationSubfieldsElement() {
        int size = currencyMap.size();
        InternalEntry[] entryArray = new InternalEntry[size];
        int p = 0;
        for (Map.Entry<ExtendedCurrency, String> entry : currencyMap.entrySet()) {
            entryArray[p] = new InternalEntry(entry.getKey(), entry.getValue());
            p++;
        }
        return new InternalMontantInformationSubfieldsElement(corpusField, label, mandatory, attributes, wrap(entryArray), othersValue);
    }

    public static MontantInformationSubfieldsElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, CorpusField corpusField, FieldUi fieldUi) {
        FicheItems ficheItems = (FicheItems) fichePointeur.getValue(corpusField);
        if ((ficheItems == null) && (fieldUi.isObsolete())) {
            return null;
        }
        return init(corpusField)
                .setCurrencyFromFicheItems(ficheItems, ficheFormParameters)
                .setAttributes(fieldUi.getAttributes())
                .setLabel(L10nUtils.toLabelString(ficheFormParameters, corpusField));
    }


    public static MontantInformationSubfieldsElementBuilder init(CorpusField corpusField) {
        return new MontantInformationSubfieldsElementBuilder(corpusField);
    }

    private static List<MontantInformationSubfieldsElement.Entry> wrap(MontantInformationSubfieldsElement.Entry[] array) {
        return new EntryList(array);
    }


    private static class InternalMontantInformationSubfieldsElement implements MontantInformationSubfieldsElement {

        private final CorpusField corpusField;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final List<MontantInformationSubfieldsElement.Entry> entryList;
        private final String othersValue;

        private InternalMontantInformationSubfieldsElement(CorpusField corpusField, String label, boolean mandatory, Attributes attributes, List<MontantInformationSubfieldsElement.Entry> entryList, String othersValue) {
            this.corpusField = corpusField;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.entryList = entryList;
            this.othersValue = othersValue;
        }

        @Override
        public CorpusField getCorpusField() {
            return corpusField;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public List<MontantInformationSubfieldsElement.Entry> getEntryList() {
            return entryList;
        }

        @Override
        public String getOthersValue() {
            return othersValue;
        }

    }


    private static class InternalEntry implements MontantInformationSubfieldsElement.Entry {

        private final ExtendedCurrency currency;
        private final String value;

        private InternalEntry(ExtendedCurrency currency, String value) {
            this.currency = currency;
            this.value = value;
        }

        @Override
        public ExtendedCurrency getCurrency() {
            return currency;
        }

        @Override
        public String getMontantValue() {
            return value;
        }

    }


    private static class EntryList extends AbstractList<MontantInformationSubfieldsElement.Entry> implements RandomAccess {

        private final MontantInformationSubfieldsElement.Entry[] array;

        private EntryList(MontantInformationSubfieldsElement.Entry[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MontantInformationSubfieldsElement.Entry get(int index) {
            return array[index];
        }

    }

}
