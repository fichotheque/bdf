/* BdfServer - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ficheform.HiddenFieldElement;
import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.tools.L10nUtils;
import fr.exemole.bdfserver.tools.ficheform.FicheFormUtils;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.pointeurs.FichePointeur;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class HiddenFieldElementBuilder {

    private final CorpusField corpusField;
    private String label = "";
    private boolean mandatory = false;
    private String value = "";
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;

    public HiddenFieldElementBuilder(CorpusField corpusField) {
        this.corpusField = corpusField;
    }

    public HiddenFieldElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public HiddenFieldElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public HiddenFieldElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public HiddenFieldElementBuilder setValue(String value) {
        if (value == null) {
            throw new IllegalArgumentException("value is null");
        }
        this.value = value;
        return this;
    }

    public HiddenFieldElement toHiddenFieldElement() {
        return new InternalHiddenFieldElement(corpusField, label, mandatory, attributes, value);
    }

    public static HiddenFieldElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, CorpusField corpusField, FieldUi fieldUi, String defaultValue) {
        String value;
        if (fichePointeur.isEmpty()) {
            if (defaultValue != null) {
                value = defaultValue;
            } else {
                value = FicheFormUtils.getDefVal(fichePointeur, ficheFormParameters, fieldUi);
            }
        } else {
            value = FicheFormUtils.getStringValue(fichePointeur, corpusField, ficheFormParameters);
        }
        if ((value.isEmpty()) && (fieldUi.isObsolete())) {
            return null;
        }
        return init(corpusField)
                .setValue(value)
                .setLabel(L10nUtils.toLabelString(ficheFormParameters, corpusField))
                .setAttributes(fieldUi.getAttributes());
    }

    public static HiddenFieldElementBuilder init(CorpusField corpusField) {
        return new HiddenFieldElementBuilder(corpusField);
    }


    private static class InternalHiddenFieldElement implements HiddenFieldElement {

        private final CorpusField corpusField;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final String value;

        private InternalHiddenFieldElement(CorpusField corpusField, String label, boolean mandatory, Attributes attributes, String value) {
            this.corpusField = corpusField;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.value = value;
        }

        @Override
        public CorpusField getCorpusField() {
            return corpusField;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public String getValue() {
            return value;
        }

    }

}
