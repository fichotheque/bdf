/* BdfServer - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ficheform.ImageProprieteSubfieldsElement;
import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.tools.L10nUtils;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.syntax.FormSyntax;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class ImageProprieteSubfieldsElementBuilder {

    private final CorpusField corpusField;
    private String label = "";
    private boolean mandatory = false;
    private String src = "";
    private String alt = "";
    private String title = "";
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;

    public ImageProprieteSubfieldsElementBuilder(CorpusField corpusField) {
        this.corpusField = corpusField;
    }

    public ImageProprieteSubfieldsElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public ImageProprieteSubfieldsElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public ImageProprieteSubfieldsElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public ImageProprieteSubfieldsElementBuilder setSrc(String src) {
        if (src == null) {
            throw new IllegalArgumentException("src is null");
        }
        this.src = src;
        return this;
    }

    public ImageProprieteSubfieldsElementBuilder setAlt(String alt) {
        if (alt == null) {
            throw new IllegalArgumentException("alt is null");
        }
        this.alt = alt;
        return this;
    }

    public ImageProprieteSubfieldsElementBuilder setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("title is null");
        }
        this.title = title;
        return this;
    }

    public ImageProprieteSubfieldsElement toImageProprieteSubfieldsElement() {
        return new InternalImageProprieteSubfieldsElement(corpusField, label, mandatory, attributes, src, alt, title);
    }

    public static ImageProprieteSubfieldsElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, CorpusField corpusField, FieldUi fieldUi) {
        ImageProprieteSubfieldsElementBuilder builder = init(corpusField);
        FicheItem ficheItem = (FicheItem) fichePointeur.getValue(corpusField);
        if (ficheItem != null) {
            if (ficheItem instanceof Image) {
                Image image = (Image) ficheItem;
                builder.setSrc(image.getSrc())
                        .setAlt(image.getAlt())
                        .setTitle(image.getTitle());
            } else {
                builder.setSrc(FormSyntax.toString(ficheItem, fichePointeur.getFichotheque(), null));
            }
        } else if (fieldUi.isObsolete()) {
            return null;
        }
        return builder
                .setMandatory(fieldUi.isMandatory())
                .setAttributes(fieldUi.getAttributes())
                .setLabel(L10nUtils.toLabelString(ficheFormParameters, corpusField));
    }

    public static ImageProprieteSubfieldsElementBuilder init(CorpusField corpusField) {
        return new ImageProprieteSubfieldsElementBuilder(corpusField);
    }


    private static class InternalImageProprieteSubfieldsElement implements ImageProprieteSubfieldsElement {

        private final CorpusField corpusField;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final String src;
        private final String alt;
        private final String title;

        private InternalImageProprieteSubfieldsElement(CorpusField corpusField, String label, boolean mandatory, Attributes attributes, String src, String alt, String title) {
            this.corpusField = corpusField;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.src = src;
            this.alt = alt;
            this.title = title;
        }

        @Override
        public CorpusField getCorpusField() {
            return corpusField;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public String getSrc() {
            return src;
        }

        @Override
        public String getAlt() {
            return alt;
        }

        @Override
        public String getTitle() {
            return title;
        }

    }

}
