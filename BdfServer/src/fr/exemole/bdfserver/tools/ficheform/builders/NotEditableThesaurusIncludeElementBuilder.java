/* BdfServer - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.ficheform.builders;

import fr.exemole.bdfserver.api.ficheform.FicheFormParameters;
import fr.exemole.bdfserver.api.ficheform.ThesaurusIncludeElement;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.tools.L10nUtils;
import fr.exemole.bdfserver.tools.ficheform.FicheFormUtils;
import java.util.List;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class NotEditableThesaurusIncludeElementBuilder {

    private final String name;
    private final Thesaurus thesaurus;
    private final MotcleEntryListBuilder motcleEntryListBuilder = new MotcleEntryListBuilder();
    private String label = "";
    private boolean mandatory = false;
    private Attribute idalphaStyle = null;
    private Attributes attributes = AttributeUtils.EMPTY_ATTRIBUTES;

    public NotEditableThesaurusIncludeElementBuilder(String name, Thesaurus thesaurus) {
        this.name = name;
        this.thesaurus = thesaurus;
    }

    public NotEditableThesaurusIncludeElementBuilder setLabel(String label) {
        if (label == null) {
            throw new IllegalArgumentException("label is null");
        }
        this.label = label;
        return this;
    }

    public NotEditableThesaurusIncludeElementBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public NotEditableThesaurusIncludeElementBuilder setAttributes(Attributes attributes) {
        if (attributes == null) {
            this.attributes = AttributeUtils.EMPTY_ATTRIBUTES;
        } else {
            this.attributes = attributes;
        }
        return this;
    }

    public NotEditableThesaurusIncludeElementBuilder addMotcle(Motcle motcle) {
        motcleEntryListBuilder.add(motcle, -1);
        return this;
    }

    public NotEditableThesaurusIncludeElementBuilder setIdalphaStyle(Attribute idalphaStyle) {
        this.idalphaStyle = idalphaStyle;
        return this;
    }

    public ThesaurusIncludeElement.NotEditable toNotEditableIndexationElement() {
        return new InternalNotEditableIndexationElement(name, label, mandatory, attributes, thesaurus, idalphaStyle, motcleEntryListBuilder.toList());
    }

    public static ThesaurusIncludeElement.NotEditable build(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, SubsetIncludeUi includeUi) {
        NotEditableThesaurusIncludeElementBuilder builder = check(fichePointeur, ficheFormParameters, includeUi);
        if (builder == null) {
            return null;
        } else {
            return builder.toNotEditableIndexationElement();
        }
    }

    @Nullable
    public static NotEditableThesaurusIncludeElementBuilder check(FichePointeur fichePointeur, FicheFormParameters ficheFormParameters, SubsetIncludeUi includeUi) {
        Thesaurus thesaurus = (Thesaurus) ficheFormParameters.getBdfServer().getFichotheque().getSubset(includeUi.getSubsetKey());
        if (thesaurus == null) {
            return null;
        }
        NotEditableThesaurusIncludeElementBuilder builder = init(includeUi.getName(), thesaurus)
                .setIdalphaStyle(FicheFormUtils.getIdalphaStyle(includeUi, thesaurus))
                .setAttributes(includeUi.getAttributes())
                .setLabel(L10nUtils.toLabelString(ficheFormParameters, includeUi));
        SubsetItemPointeur pointeur = FicheFormUtils.checkMasterPointeur(fichePointeur, ficheFormParameters, includeUi);
        if (pointeur.isEmpty()) {
            builder.motcleEntryListBuilder.populateDefault(ficheFormParameters, includeUi, thesaurus);
        } else {
            builder.motcleEntryListBuilder.populate(pointeur.getCroisements(thesaurus), includeUi, false);
        }
        if ((builder.motcleEntryListBuilder.isEmpty()) && (includeUi.isObsolete())) {
            return null;
        }
        return builder;
    }

    public static NotEditableThesaurusIncludeElementBuilder init(String name, Thesaurus thesaurus) {
        return new NotEditableThesaurusIncludeElementBuilder(name, thesaurus);
    }


    private static class InternalNotEditableIndexationElement implements ThesaurusIncludeElement.NotEditable {

        private final String name;
        private final String label;
        private final boolean mandatory;
        private final Attributes attributes;
        private final Thesaurus thesaurus;
        private final Attribute idalphaStyle;
        private final List<ThesaurusIncludeElement.Entry> list;


        private InternalNotEditableIndexationElement(String name, String label, boolean mandatory, Attributes attributes, Thesaurus thesaurus, Attribute idalphaStyle, List<ThesaurusIncludeElement.Entry> list) {
            this.name = name;
            this.label = label;
            this.mandatory = mandatory;
            this.attributes = attributes;
            this.thesaurus = thesaurus;
            this.idalphaStyle = idalphaStyle;
            this.list = list;
        }

        @Override
        public String getIncludeName() {
            return name;
        }

        @Override
        public String getLabel() {
            return label;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public Thesaurus getThesaurus() {
            return thesaurus;
        }

        @Override
        public Attribute getIdalphaStyle() {
            return idalphaStyle;
        }

        @Override
        public List<Entry> getEntryList() {
            return list;
        }

    }

}
