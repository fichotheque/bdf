/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.diagnostic.urlscan;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.metadata.FieldKey;


/**
 *
 * @author Vincent Calame
 */
public class UrlInfo {

    private final static Map<String, FicheInfo> EMPTY_MAP = Collections.emptyMap();
    private final String url;
    private final UrlReport urlReport;
    private final Map<String, FicheInfo> ficheInfoMap;

    private UrlInfo(String url, UrlReport urlReport, Map<String, FicheInfo> ficheInfoMap) {
        this.url = url;
        this.urlReport = urlReport;
        this.ficheInfoMap = ficheInfoMap;
    }

    public String getUrl() {
        return url;
    }

    public UrlReport getUrlReport() {
        return urlReport;
    }

    public Map<String, FicheInfo> getFicheInfoMap() {
        return ficheInfoMap;
    }

    void add(FicheMeta ficheMeta, FieldKey fieldKey) {
        String globalId = ficheMeta.getGlobalId();
        FicheInfo ficheInfo = ficheInfoMap.get(globalId);
        if (ficheInfo == null) {
            ficheInfo = new FicheInfo(ficheMeta);
            ficheInfoMap.put(globalId, ficheInfo);
        }
        ficheInfo.add(fieldKey);
    }

    public static UrlInfo init(String url, UrlReport urlReport) {
        return new UrlInfo(url, urlReport, EMPTY_MAP);
    }

    static UrlInfo initWithFicheInfo(String url, UrlReport urlReport) {
        return new UrlInfo(url, urlReport, new TreeMap<String, FicheInfo>());
    }


    public static class FicheInfo {

        private final FicheMeta ficheMeta;
        private final Set<FieldKey> fieldKeySet = new LinkedHashSet<FieldKey>();

        private FicheInfo(FicheMeta ficheMeta) {
            this.ficheMeta = ficheMeta;
        }

        private void add(FieldKey fieldKey) {
            fieldKeySet.add(fieldKey);
        }

        public FicheMeta getFicheMeta() {
            return ficheMeta;
        }

        public Set<FieldKey> getFieldKeySet() {
            return fieldKeySet;
        }

    }

}
