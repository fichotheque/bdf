/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.diagnostic.urlscan;

import net.mapeadores.util.http.UrlStatus;


/**
 *
 * @author Vincent Calame
 */
public class UrlReport {

    private final String url;
    private final UrlStatus urlStatus;
    private final long time;

    public UrlReport(String url, UrlStatus urlStatus, long time) {
        this.url = url;
        this.urlStatus = urlStatus;
        this.time = time;
    }

    public String getUrl() {
        return url;
    }

    public UrlStatus getUrlStatus() {
        return urlStatus;
    }

    public long getTime() {
        return time;
    }

}
