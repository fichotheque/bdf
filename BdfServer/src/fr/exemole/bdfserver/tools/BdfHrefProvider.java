/* BdfServer - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools;


/**
 *
 * @author Vincent Calame
 */
public abstract class BdfHrefProvider {

    protected BdfHrefProvider() {

    }

    public abstract BdfHref getSrc(String name);

}
