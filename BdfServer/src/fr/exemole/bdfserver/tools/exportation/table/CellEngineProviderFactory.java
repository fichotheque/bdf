/* BdfServer - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.table;

import fr.exemole.bdfserver.api.BdfServer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.CellEngineProvider;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.tools.exportation.table.CellEngineFactory;


/**
 *
 * @author Vincent Calame
 */
public final class CellEngineProviderFactory {

    private CellEngineProviderFactory() {

    }

    public static CellEngineProvider newInstance(BdfServer bdfServer, ExtractionContext extractionContext, boolean ignoreLangModeCheck) {
        return new InternalCellEngineProvider(bdfServer, extractionContext, ignoreLangModeCheck);
    }


    private static class InternalCellEngineProvider implements CellEngineProvider {

        private final BdfServer bdfServer;
        private final Map<String, CellEngine> engineMap = new HashMap<String, CellEngine>();
        private final Set<String> unknownSet = new HashSet<String>();
        private final ExtractionContext extractionContext;
        private final boolean ignoreLangModeCheck;

        private InternalCellEngineProvider(BdfServer bdfServer, ExtractionContext extractionContext, boolean ignoreLangModeCheck) {
            this.bdfServer = bdfServer;
            this.extractionContext = extractionContext;
            this.ignoreLangModeCheck = ignoreLangModeCheck;
        }

        @Override
        public CellEngine getCellEngine(String name) {
            CellEngine cellEngine = engineMap.get(name);
            if (cellEngine != null) {
                return cellEngine;
            }
            if (unknownSet.contains(name)) {
                return null;
            }
            TableExport tableExport = bdfServer.getTableExportManager().getTableExport(name);
            if (tableExport == null) {
                unknownSet.add(name);
                return null;
            }
            ExtractionContext customExtractionContext;
            if (!ignoreLangModeCheck) {
                customExtractionContext = CellEngineUtils.checkLangMode(extractionContext, bdfServer, tableExport);
            } else {
                customExtractionContext = extractionContext;
            }
            cellEngine = CellEngineFactory.newInstance(tableExport.getSubsetTableList(), bdfServer.getTableExportContext(), customExtractionContext, this);
            engineMap.put(name, cellEngine);
            return cellEngine;
        }

    }

}
