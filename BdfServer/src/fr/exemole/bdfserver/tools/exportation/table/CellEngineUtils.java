/* BdfServer - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.table;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.CellEngineProvider;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.tools.exportation.table.CellEngineFactory;
import net.fichotheque.utils.ExtractionUtils;
import net.fichotheque.utils.TableExportUtils;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.LangContext;


/**
 *
 * @author Vincent Calame
 */
public final class CellEngineUtils {

    private CellEngineUtils() {

    }

    public static CellEngine newCellEngine(BdfServer bdfServer, ExtractionContext extractionContext, @Nullable TableExportDef tableExportDef, SubsetTable subsetTable) {
        return newCellEngine(bdfServer, extractionContext, TableExportUtils.toTableExport(tableExportDef, subsetTable), false);
    }

    public static CellEngine newCellEngine(BdfServer bdfServer, ExtractionContext extractionContext, TableExport tableExport, boolean ignoreLangModeCheck) {
        ExtractionContext customExtractionContext;
        if (!ignoreLangModeCheck) {
            customExtractionContext = checkLangMode(extractionContext, bdfServer, tableExport);
        } else {
            customExtractionContext = extractionContext;
        }
        CellEngineProvider cellEngineProvider = CellEngineProviderFactory.newInstance(bdfServer, extractionContext, ignoreLangModeCheck);
        CellEngine cellEngine = CellEngineFactory.newInstance(tableExport.getSubsetTableList(), bdfServer.getTableExportContext(), customExtractionContext, cellEngineProvider);
        return cellEngine;
    }

    public static CellEngine newCellEngine(BdfServer bdfServer, ExtractionContext extractionContext, String tableExportName) {
        CellEngineProvider cellEngineProvider = CellEngineProviderFactory.newInstance(bdfServer, extractionContext, false);
        return cellEngineProvider.getCellEngine(tableExportName);
    }

    public static ExtractionContext checkLangMode(ExtractionContext extractionContext, BdfServer bdfServer, TableExport tableExport) {
        TableExportDef tableExportDef = tableExport.getTableExportDef();
        if (tableExportDef != null) {
            LangContext checkedLangContext = BdfServerUtils.checkLangMode(bdfServer, tableExport.getTableExportDef());
            if (checkedLangContext != null) {
                return ExtractionUtils.derive(extractionContext, checkedLangContext);
            }
        }
        return extractionContext;
    }

}
