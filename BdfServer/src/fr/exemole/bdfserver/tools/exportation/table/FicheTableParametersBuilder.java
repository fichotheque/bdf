/* BdfServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.table;

import fr.exemole.bdfserver.api.exportation.table.FicheTableParameters;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class FicheTableParametersBuilder {

    private final Set<String> withSet = new HashSet<String>();
    private String patternMode = FicheTableParameters.STANDARD_PATTERNMODE;

    public FicheTableParametersBuilder() {
    }

    public FicheTableParametersBuilder putWith(String key) {
        withSet.add(key);
        return this;
    }

    public FicheTableParametersBuilder setPatternMode(String patternMode) {
        this.patternMode = FicheTableParameters.checkPatternMode(patternMode);
        return this;
    }

    public FicheTableParameters toFicheTableParameters() {
        return new InternalFicheTableParameters(new HashSet<String>(withSet), patternMode);
    }

    public static FicheTableParametersBuilder init() {
        return new FicheTableParametersBuilder();
    }


    private static class InternalFicheTableParameters implements FicheTableParameters {

        private final Set<String> withSet;
        private final List<String> withList;
        private final String patternMode;

        private InternalFicheTableParameters(Set<String> withSet, String patternMode) {
            this.withSet = withSet;
            this.patternMode = patternMode;
            this.withList = StringUtils.toList(withSet);
        }

        @Override
        public boolean isWith(String key) {
            return withSet.contains(key);
        }

        @Override
        public String getPatternMode() {
            return patternMode;
        }

        @Override
        public List<String> getWithList() {
            return withList;
        }

    }

}
