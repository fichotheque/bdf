/* BdfServer - Copyright (c) 2007-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.table;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.exportation.table.FicheTableParameters;
import fr.exemole.bdfserver.api.managers.TableExportManager;
import fr.exemole.bdfserver.api.namespaces.UiSpace;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.exportation.table.CroisementTable;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableDef;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.exportation.table.TableExportContentDescription;
import net.fichotheque.exportation.table.TableExportContext;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.exportation.table.SubsetTableBuilder;
import net.fichotheque.utils.TableExportUtils;


/**
 *
 * @author Vincent Calame
 */
public final class BdfTableExportUtils {

    public final static FicheTableParameters NONE_FICHETABLEPARAMETERS = FicheTableParametersBuilder.init()
            .toFicheTableParameters();
    public final static FicheTableParameters ALL_FICHETABLEPARAMETERS = FicheTableParametersBuilder.init()
            .putWith(FicheTableParameters.WITH_SECTION)
            .putWith(FicheTableParameters.WITH_CHRONO)
            .putWith(FicheTableParameters.WITH_ADDENDAINCLUDE)
            .putWith(FicheTableParameters.WITH_ALBUMINCLUDE)
            .putWith(FicheTableParameters.WITH_CORPUSINCLUDE)
            .putWith(FicheTableParameters.WITH_THESAURUSINCLUDE)
            .toFicheTableParameters();


    private BdfTableExportUtils() {
    }

    public static TableExport toDefaultTableExport(BdfServer bdfServer, FicheTableParameters ficheTableParameters, PermissionSummary permissionSummary) {
        SubsetTable[] subsetTableArray = initSubsetTableArray(bdfServer, ficheTableParameters, permissionSummary);
        return TableExportUtils.toTableExport(null, subsetTableArray, new CroisementTable[0]);
    }

    public static SubsetTable toDefaultSubsetTable(BdfServer bdfServer, Subset subset, FicheTableParameters ficheTableParameters, PermissionSummary permissionSummary) {
        TableDef tableDef;
        if (subset instanceof Corpus) {
            Corpus corpus = (Corpus) subset;
            tableDef = DefaultTableDefFactory.fromUi(bdfServer, corpus, ficheTableParameters, permissionSummary);

        } else if (subset instanceof Thesaurus) {
            Thesaurus thesaurus = (Thesaurus) subset;
            tableDef = DefaultTableDefFactory.fromThesaurusMetadata(thesaurus, bdfServer.getThesaurusLangChecker());
        } else {
            throw new UnsupportedOperationException("Unable to deal with : " + subset.getSubsetKeyString());
        }
        return SubsetTableBuilder.init(subset).populate(tableDef, bdfServer.getTableExportContext()).toSubsetTable();
    }

    public static SubsetTable[] initSubsetTableArray(BdfServer bdfServer, FicheTableParameters ficheTableParameters, PermissionSummary permissionSummary) {
        Fichotheque fichotheque = bdfServer.getFichotheque();
        TableExportContext tableExportContext = bdfServer.getTableExportContext();
        List<SubsetTable> list = new ArrayList<SubsetTable>();
        for (Corpus corpus : fichotheque.getCorpusList()) {
            if (!permissionSummary.hasAccess(corpus)) {
                continue;
            }
            TableDef tableDef = DefaultTableDefFactory.fromUi(bdfServer, corpus, ficheTableParameters, permissionSummary);
            list.add(SubsetTableBuilder.init(corpus).populate(tableDef, tableExportContext).toSubsetTable());
        }
        return list.toArray(new SubsetTable[list.size()]);
    }

    public static List<TableExportDescription> getAvailableTableExportDescriptionList(TableExportManager manager, SubsetKey subsetKey, String hideTest) {
        String requestedPath = subsetKey + ".txt";
        List<TableExportDescription> list = new ArrayList<TableExportDescription>();
        for (TableExportDescription tableExportDescription : manager.getValidTableExportDescriptionList()) {
            if (UiSpace.testHide(tableExportDescription.getTableExportDef().getAttributes(), hideTest)) {
                continue;
            }
            boolean here = false;
            for (TableExportContentDescription tableExportContentDescription : tableExportDescription.getTableExportContentDescriptionList()) {
                String contentPath = tableExportContentDescription.getPath();
                if (contentPath.equals(requestedPath)) {
                    here = true;
                    break;
                }
            }
            if (here) {
                list.add(tableExportDescription);
            }
        }
        return list;
    }

    public static TableExportDescription getValidTableExportDescription(TableExportManager manager, String tableExportName) {
        for (TableExportDescription tableExportDescription : manager.getValidTableExportDescriptionList()) {
            if (tableExportDescription.getName().equals(tableExportName)) {
                return tableExportDescription;
            }
        }
        return null;
    }

}
