/* BdfServer - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.table;

import fr.exemole.bdfserver.api.exportation.table.FicheTableParameters;
import fr.exemole.bdfserver.api.exportation.table.TableExportParameters;
import net.fichotheque.exportation.table.TableExportConstants;


/**
 *
 * @author Vincent Calame
 */
public class TableExportParametersBuilder {

    private String tableExportName = null;
    private String headerType = TableExportConstants.COLUMNTITLE_HEADER;
    private FicheTableParameters ficheTableParameters = BdfTableExportUtils.NONE_FICHETABLEPARAMETERS;

    public TableExportParametersBuilder() {
    }

    public TableExportParametersBuilder setTableExportName(String tableExportName) {
        this.tableExportName = tableExportName;
        return this;
    }

    public TableExportParametersBuilder setHeaderType(String headerType) {
        TableExportConstants.checkHeaderType(headerType);
        this.headerType = headerType;
        return this;
    }

    public TableExportParametersBuilder setFicheTableParameters(FicheTableParameters ficheTableParameters) {
        if (ficheTableParameters == null) {
            throw new IllegalArgumentException("ficheTableParameters is null");
        }
        this.ficheTableParameters = ficheTableParameters;
        return this;
    }

    public TableExportParameters toTableExportParameters() {
        return new InternalTableExportParameters(tableExportName, headerType, ficheTableParameters);
    }

    public static TableExportParametersBuilder init() {
        return new TableExportParametersBuilder();
    }


    private static class InternalTableExportParameters implements TableExportParameters {

        private final String tableExportName;
        private final String headerType;
        private final FicheTableParameters ficheTableParameters;

        private InternalTableExportParameters(String tableExportName, String headerType, FicheTableParameters ficheTableParameters) {
            this.tableExportName = tableExportName;
            this.headerType = headerType;
            this.ficheTableParameters = ficheTableParameters;
        }

        @Override
        public String getTableExportName() {
            return tableExportName;
        }

        @Override
        public FicheTableParameters getDefaulFicheTableParameters() {
            return ficheTableParameters;
        }

        @Override
        public String getHeaderType() {
            return headerType;
        }

    }

}
