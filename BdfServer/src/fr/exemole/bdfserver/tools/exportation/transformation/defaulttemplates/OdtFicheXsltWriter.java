/* BdfServer - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.ui.CommentUi;
import fr.exemole.bdfserver.api.ui.DataUi;
import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.api.ui.SpecialIncludeUi;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import java.io.IOException;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.corpus.metadata.FieldKey;


/**
 *
 * @author Vincent Calame
 */
public class OdtFicheXsltWriter extends AbstractFicheXsltWriter {

    public OdtFicheXsltWriter(BdfServer bdfServer, int extractVersion) {
        super(bdfServer, extractVersion);
    }

    public void add(Corpus corpus, boolean withCorpusCondition, boolean withLogo) throws IOException {
        if (corpus == null) {
            openMatchXslTemplate("fiche", null);
            closeXslTemplate();
            return;
        }
        if (withCorpusCondition) {
            openMatchXslTemplate("fiche[@corpus='" + corpus.getSubsetName() + "']", null);
        } else {
            openMatchXslTemplate("fiche", null);
        }
        appendUiComponents(corpus, uiManager.getMainUiComponents(corpus), withLogo);
        closeXslTemplate();
        addSeparator();
    }

    private void appendUiComponents(Corpus corpus, UiComponents uiComponents, boolean withLogo) throws IOException {
        CorpusMetadata corpusMetadata = corpus.getCorpusMetadata();
        String subsetName = corpus.getSubsetName();
        addXslVariable("label", "$LABELS[@corpus='" + subsetName + "']/label", false);
        if (withLogo) {
            addSeparator();
            apply("/", "resource-Logo");
        }
        addSeparator();
        apply(".", "component-Header");
        addSeparator();
        if (uiComponents.contains(FieldKey.SOUSTITRE)) {
            apply("soustitre", "component-Header");
            addSeparator();
        }
        for (UiComponent uiComponent : uiComponents.getUiComponentList()) {
            if (uiComponent instanceof FieldUi) {
                appendFieldUi((FieldUi) uiComponent, corpusMetadata);
            } else if (uiComponent instanceof SpecialIncludeUi) {
                appendSpecialIncludeUi((SpecialIncludeUi) uiComponent);
            } else if (uiComponent instanceof SubsetIncludeUi) {
                appendSubsetIncludeUi((SubsetIncludeUi) uiComponent);
            } else if (uiComponent instanceof CommentUi) {
            } else if (uiComponent instanceof DataUi) {
                appendDataUi((DataUi) uiComponent);
            }
        }
        appendChrono();
    }

    private void appendSpecialIncludeUi(SpecialIncludeUi includeUi) throws IOException {
        switch (includeUi.getName()) {
            case FichothequeConstants.LIAGE_NAME:
                apply(getTagName(SubsetKey.CATEGORY_CORPUS) + "[@name='" + FichothequeConstants.LIAGE_NAME + "']", "component-Listitem", includeUi.getName());
                break;
        }
    }

}
