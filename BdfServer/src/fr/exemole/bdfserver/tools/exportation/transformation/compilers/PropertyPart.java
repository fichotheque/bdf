/* BdfServer - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.compilers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
class PropertyPart {

    private final static Map<String, String> EMPTY_MAP = Collections.emptyMap();
    private final String name;
    private final String mode;
    private final Map<String, String> optionMap;

    private PropertyPart(String name, String mode, Map<String, String> optionMap) {
        this.name = name;
        this.mode = mode;
        this.optionMap = optionMap;
    }

    public String getName() {
        return name;
    }

    public String getMode() {
        return mode;
    }

    public boolean hasOption(String optionName) {
        return optionMap.containsKey(optionName);
    }

    public String getOptionValue(String optionName) {
        return optionMap.get(optionName);
    }

    static PropertyPart parse(String text) {
        String[] tokens = StringUtils.getTechnicalTokens(text, false);
        String name = "";
        String mode = "";
        Map<String, String> optionMap = EMPTY_MAP;
        int length = tokens.length;
        if (length > 0) {
            name = tokens[0];
        }
        if (length > 1) {
            mode = tokens[1];
        }
        if (length > 2) {
            optionMap = new HashMap<String, String>();
            for (int i = 2; i < length; i++) {
                String token = tokens[i];
                int idx = token.indexOf('=');
                if (idx == -1) {
                    optionMap.put(token, "");
                } else {
                    String optionName = token.substring(0, idx).trim();
                    String optionValue = token.substring(idx + 1).trim();
                    if (optionName.length() > 0) {
                        optionMap.put(optionName, optionValue);
                    }
                }
            }
        }
        return new PropertyPart(name, mode, optionMap);
    }

}
