/* BdfServer - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation;

import net.fichotheque.exportation.transformation.TemplateDescription;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class ExtensionInfo {

    private final String extension;
    private final String action;
    private final Message linkMessage;
    private final boolean defaultTemplateAvailable;
    private final TemplateDescription[] templateDescriptionArray;

    public ExtensionInfo(String extension, String action, Message linkMessage, boolean defaultTemplateAvailable, TemplateDescription[] templateDescriptionArray) {
        this.extension = extension;
        this.action = action;
        this.linkMessage = linkMessage;
        this.defaultTemplateAvailable = defaultTemplateAvailable;
        this.templateDescriptionArray = templateDescriptionArray;
    }

    public String getExtension() {
        return extension;
    }

    public String getAction() {
        return action;
    }

    public Message getLinkMessage() {
        return linkMessage;
    }

    public int getTemplateCount() {
        int count = templateDescriptionArray.length;
        if (defaultTemplateAvailable) {
            count++;
        }
        return count;
    }

    public boolean isDefaultTemplateAvailable() {
        return defaultTemplateAvailable;
    }

    public TemplateDescription[] getTemplateDescriptionArray() {
        return templateDescriptionArray;
    }

}
