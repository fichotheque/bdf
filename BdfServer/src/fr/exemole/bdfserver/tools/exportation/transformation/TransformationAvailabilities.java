/* BdfServer - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation;

import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationKey;


/**
 *
 * @author Vincent Calame
 */
public class TransformationAvailabilities {

    private TransformationAvailabilities() {

    }

    public static boolean isValidTemplateType(String templateType, TemplateKey templateKey) {
        TransformationKey transformationKey = templateKey.getTransformationKey();
        if (templateKey.isSimpleTemplate()) {
            switch (templateType) {
                case "xslt":
                    return true;
                default:
                    return false;
            }
        } else {
            String extension = templateKey.getExtension();
            if (extension.equals("odt")) {
                switch (templateType) {
                    case "xslt":
                        if (acceptXsltOdt(transformationKey)) {
                            return true;
                        } else {
                            return false;
                        }
                    case "properties":
                        if (acceptPropertiesOdt(transformationKey)) {
                            return true;
                        } else {
                            return false;
                        }
                    default:
                        return false;
                }
            } else {
                return false;
            }
        }

    }

    public static boolean acceptXsltOdt(TransformationKey transformationKey) {
        if (transformationKey.isCorpusTransformationKey()) {
            return true;
        }
        switch (transformationKey.getKeyString()) {
            case TransformationKey.COMPILATION:
                return true;
            default:
                return false;
        }
    }

    public static boolean acceptPropertiesOdt(TransformationKey transformationKey) {
        if (transformationKey.isCorpusTransformationKey()) {
            return true;
        }
        return false;
    }

    public static boolean hasDefaultFragment(TransformationKey transformationKey) {
        switch (transformationKey.getKeyString()) {
            case TransformationKey.SECTION:
            case TransformationKey.FORMAT:
                return true;
            default:
                return false;
        }
    }

    public static boolean hasDefaultExtractionDef(TransformationKey transformationKey) {
        if (transformationKey.isCorpusTransformationKey()) {
            return true;
        }
        switch (transformationKey.getKeyString()) {
            case TransformationKey.COMPILATION:
            case TransformationKey.STATTHESAURUS:
                return true;
            default:
                return false;
        }
    }

    public static boolean useCorpusListAttribute(TransformationKey transformationKey) {
        switch (transformationKey.getKeyString()) {
            case TransformationKey.COMPILATION:
                return true;
            default:
                return false;
        }
    }

}
