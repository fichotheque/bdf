/* @@@@@APPLICATION@@@@@ - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.compilers;

import net.fichotheque.album.Illustration;


/**
 *
 * @author Vincent Calame
 */
class FrameDim {

    private String width;
    private String height;

    private FrameDim(float w, float h) {
        this.width = String.valueOf(w) + "cm";
        this.height = String.valueOf(h) + "cm";
    }

    public String getWidth() {
        return width;
    }

    public String getHeight() {
        return height;
    }

    static FrameDim build(Illustration illustration, PropertyPart propertyPart) {
        float width = get(propertyPart, "width");
        float height = get(propertyPart, "height");
        float ratio = getRatio(illustration);
        if (width <= 0) {
            if (height <= 0) {
                height = 1;
                width = ratio;
            } else {
                width = height * ratio;
            }
        } else if (height <= 0) {
            height = width / ratio;
        }
        return new FrameDim(width, height);
    }

    static float get(PropertyPart propertyPart, String optionName) {
        String optionValue = propertyPart.getOptionValue(optionName);
        if (optionValue == null) {
            return - 1;
        }
        try {
            float value = Float.valueOf(optionValue);
            return value;
        } catch (NumberFormatException nfe) {
            return - 1;
        }
    }

    static float getRatio(Illustration illustration) {
        return ((float) illustration.getOriginalWidth()) / illustration.getOriginalHeight();
    }

}
