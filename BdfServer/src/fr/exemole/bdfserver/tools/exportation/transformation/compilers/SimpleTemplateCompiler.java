/* BdfServer - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.compilers;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.api.exportation.transformation.SimpleTemplateFactory;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public abstract class SimpleTemplateCompiler extends TemplateCompiler {

    public SimpleTemplateCompiler(BdfServer bdfServer, PathConfiguration pathConfiguration, TemplateKey templateKey, MessageHandler messageHandler) {
        super(bdfServer, pathConfiguration, templateKey, messageHandler);
    }

    public abstract SimpleTemplateFactory compile(TemplateStorage.Unit storageUnit) throws CompilationException;

}
