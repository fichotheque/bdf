/* BdfServer - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public final class TemplateStorageUnitFactory {

    private TemplateStorageUnitFactory() {

    }

    public static TemplateStorage.Unit newXsltHtml(BdfServer bdfServer, TemplateKey templateKey, DefaultOptions options, Attributes initAttributes) {
        return XsltHtmlUnit.build(bdfServer, templateKey, options, initAttributes);
    }

    public static TemplateStorage.Unit newXsltOdt(BdfServer bdfServer, TemplateKey templateKey, DefaultOptions options, Attributes initAttributes) {
        return XsltOdtUnit.build(bdfServer, templateKey, options, initAttributes);
    }

    public static TemplateStorage.Unit newPropertiesOdt(BdfServer bdfServer, PathConfiguration pathConfiguration, TemplateKey templateKey, String tableExportName, Attributes initAttributes) {
        return PropertiesOdtUnit.build(bdfServer, pathConfiguration, templateKey, tableExportName, initAttributes);
    }

    public static TemplateStorage.Unit newXsltFragment(BdfServer bdfServer, TemplateKey templateKey, Attributes initAttributes) {
        return XsltFragmentUnit.build(bdfServer, templateKey, initAttributes);
    }


    public static class Options {

        private boolean withIf;
        private boolean withExtractionDef;

        public Options() {

        }

        public boolean withIf() {
            return withIf;
        }

        public boolean withExtractionDef() {
            return withExtractionDef;
        }

        public Options withIf(boolean withIf) {
            this.withIf = withIf;
            return this;
        }

        public Options withExtractionDef(boolean withExtractionDef) {
            this.withExtractionDef = withExtractionDef;
            return this;
        }

    }

}
