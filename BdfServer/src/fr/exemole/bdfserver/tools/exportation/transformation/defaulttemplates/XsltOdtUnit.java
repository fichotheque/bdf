/* BdfServer - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import fr.exemole.bdfserver.tools.exportation.transformation.BdfTransformationUtils;
import fr.exemole.bdfserver.tools.exportation.transformation.DefaultExtractionDefFactory;
import fr.exemole.bdfserver.tools.exportation.transformation.TransformationAvailabilities;
import fr.exemole.bdfserver.tools.storage.TemplateStorageUnitBuilder;
import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.extraction.def.ExtractionDef;
import net.fichotheque.tools.exportation.transformation.TemplateDefBuilder;
import net.fichotheque.utils.TransformationUtils;
import net.mapeadores.opendocument.io.OdXML;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
class XsltOdtUnit {

    private XsltOdtUnit() {
    }

    static TemplateStorage.Unit build(BdfServer bdfServer, TemplateKey templateKey, DefaultOptions options, Attributes initAttributes) {
        if (templateKey.isSimpleTemplate()) {
            throw new IllegalArgumentException("templateKey is simple template key");
        } else if (!templateKey.getExtension().equals("odt")) {
            throw new IllegalArgumentException("templateKey.getExtension != odt");
        }
        if (!TransformationAvailabilities.acceptXsltOdt(templateKey.getTransformationKey())) {
            throw new IllegalArgumentException("unsupported transformation key: " + templateKey.getTransformationKey());
        }
        TemplateDef templateDef = TemplateDefBuilder.init(templateKey, initAttributes).toTemplateDef();
        String xsltString = getXslt(bdfServer, templateKey.getTransformationKey(), initAttributes);
        TemplateStorageUnitBuilder builder = (TemplateStorageUnitBuilder) TemplateStorageUnitBuilder.init("xslt", templateDef)
                .addStorageContent("content.xsl", xsltString)
                .addStorageContent("styles.css", DefaultTemplateUtils.getDefaultStylesCss())
                .addStorageContent("styles.xml", DefaultTemplateUtils.getDefaultStylesXML(bdfServer));
        if (options.withExtractionDef()) {
            ExtractionDef extractionDef = DefaultExtractionDefFactory.newInstance(bdfServer, templateKey.getTransformationKey(), initAttributes);
            if (extractionDef != null) {
                String extractionContent = DefaultTemplateUtils.getExtractionDefXML(extractionDef, options.compactStyle());
                builder.addStorageContent("extraction.xml", extractionContent);
            }
        }
        return builder.toTemplateStorageUnit();
    }


    private static String getXslt(BdfServer bdfServer, TransformationKey transformationKey, Attributes attributes) {
        int extractVersion = TransformationUtils.getExtractVersion(attributes);
        StringBuilder buf = new StringBuilder();
        OdtFicheXsltWriter ficheXsltWriter = new OdtFicheXsltWriter(bdfServer, extractVersion);
        ficheXsltWriter.setAppendable(buf);
        ficheXsltWriter.setIndentLength(0);
        try {
            ficheXsltWriter.appendXMLDeclaration();
            ficheXsltWriter.startXslStyleSheetOpenTag();
            OdXML.addDocumentNameSpaceAttributes(ficheXsltWriter);
            ficheXsltWriter.endOpenTag();
            if (transformationKey.isCorpusTransformationKey()) {
                ficheXsltWriter.addXslInclude("bdf://this/xslt/v2/transformations/odt/fiche.xsl");
                SubsetKey corpusKey = transformationKey.toCorpusKey();
                Corpus corpus = (Corpus) bdfServer.getFichotheque().getSubset(corpusKey);
                ficheXsltWriter.add(corpus, false, true);
            } else {
                ficheXsltWriter.addXslInclude("bdf://this/xslt/v2/transformations/odt/compilation.xsl");
                for (Corpus corpus : BdfTransformationUtils.getCorpusList(bdfServer.getFichotheque(), attributes)) {
                    ficheXsltWriter.add(corpus, true, false);
                }
            }
            ficheXsltWriter.closeXslStyleSheet();
        } catch (IOException ioe) {
        }
        return buf.toString();
    }


}
