/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates;


/**
 *
 * @author Vincent Calame
 */
public class DefaultOptions {

    private boolean withExtractionDef;
    private boolean compactStyle;

    public DefaultOptions() {

    }

    public boolean withExtractionDef() {
        return withExtractionDef;
    }

    public boolean compactStyle() {
        return compactStyle;
    }

    public DefaultOptions withExtractionDef(boolean withExtractionDef) {
        this.withExtractionDef = withExtractionDef;
        return this;
    }

    public DefaultOptions compactStyle(boolean compactStyle) {
        this.compactStyle = compactStyle;
        return this;
    }

    public static DefaultOptions init() {
        return new DefaultOptions();
    }

}
