/* BdfServer - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.def.ExtractionDef;
import net.fichotheque.xml.defs.ExtractionDefXMLPart;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;


/**
 *
 * @author Vincent Calame
 */
public final class DefaultTemplateUtils {

    private final static RelativePath DEFAULT_STYLES_PATH = RelativePath.build("xml/odtstyles.xml");

    private DefaultTemplateUtils() {

    }

    public static String getDefaultStylesCss() {
        return "@import url(\"bdf://this/css/fiche_odt.css\");";
    }

    public static String getDefaultStylesXML(BdfServer bdfServer) {
        return bdfServer.getResourceStorages().getResourceDocStream(DEFAULT_STYLES_PATH).getContent();
    }

    public static String getExtractionDefXML(ExtractionDef extractionDef, boolean compactStyle) {
        StringBuilder buf = new StringBuilder();
        AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf, 0);
        try {
            xmlWriter.appendXMLDeclaration();
            ExtractionDefXMLPart.init(xmlWriter, compactStyle)
                    .addExtractionDef(extractionDef);
        } catch (IOException ioe) {
            throw new ShouldNotOccurException(ioe);
        }
        return buf.toString();
    }

    public static String getTagName(short subsetCategory, int extractVersion) {
        switch (extractVersion) {
            case ExtractionConstants.INITIAL_VERSION:
                switch (subsetCategory) {
                    case SubsetKey.CATEGORY_CORPUS:
                        return "extraitcorpus";
                    case SubsetKey.CATEGORY_ALBUM:
                        return "extraitalbum";
                    case SubsetKey.CATEGORY_ADDENDA:
                        return "extraitaddenda";
                    case SubsetKey.CATEGORY_THESAURUS:
                        return "extraitthesaurus";
                    default:
                        throw new SwitchException("Unknown type: " + subsetCategory);
                }
            default:
                switch (subsetCategory) {
                    case SubsetKey.CATEGORY_CORPUS:
                        return "fiches";
                    case SubsetKey.CATEGORY_ALBUM:
                        return "illustrations";
                    case SubsetKey.CATEGORY_ADDENDA:
                        return "documents";
                    case SubsetKey.CATEGORY_THESAURUS:
                        return "motcles";
                    default:
                        throw new SwitchException("Unknown category: " + subsetCategory);
                }
        }
    }

    public static String getMode(SubsetIncludeUi includeUi) {
        switch (includeUi.getCategory()) {
            case SubsetKey.CATEGORY_CORPUS:
                if (includeUi.isFicheTable()) {
                    return "component-Table";
                } else {
                    return "component-Listitem";
                }
            case SubsetKey.CATEGORY_THESAURUS:
                if (includeUi.matchInputType(BdfServerConstants.INPUT_FICHESTYLE)) {
                    return "component-Listitem";
                } else {
                    return "component-Inline";
                }
            case SubsetKey.CATEGORY_ALBUM:
                return "component-Inline";
            case SubsetKey.CATEGORY_ADDENDA:
                return "component-Listitem";
            default:
                throw new SwitchException("Unknown category: " + includeUi.getCategory());
        }
    }

    public static String getMode(CorpusField corpusField) {
        if ((corpusField != null) && (corpusField.isBlockDisplayInformationField())) {
            switch (corpusField.getFicheItemType()) {
                case CorpusField.ITEM_FIELD:
                case CorpusField.PARA_FIELD:
                case CorpusField.IMAGE_FIELD:
                    return "component-Block";
                default:
                    return "component-Listitem";
            }
        } else {
            return "component-Inline";
        }
    }

}
