/* BdfServer - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import fr.exemole.bdfserver.tools.exportation.transformation.BdfTransformationUtils;
import fr.exemole.bdfserver.tools.exportation.transformation.DefaultExtractionDefFactory;
import fr.exemole.bdfserver.tools.storage.TemplateStorageUnitBuilder;
import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.extraction.def.ExtractionDef;
import net.fichotheque.tools.exportation.transformation.TemplateDefBuilder;
import net.fichotheque.utils.TransformationUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.xml.XsltWriter;


/**
 *
 * @author Vincent Calame
 */
class XsltHtmlUnit {

    private XsltHtmlUnit() {
    }

    static TemplateStorage.Unit build(BdfServer bdfServer, TemplateKey templateKey, DefaultOptions options, Attributes initAttributes) {
        if (!templateKey.isSimpleTemplate()) {
            throw new IllegalArgumentException("templateKey is not a simple template key");
        }
        TransformationKey transformationKey = templateKey.getTransformationKey();
        TemplateDef templateDef = TemplateDefBuilder.init(templateKey, initAttributes).toTemplateDef();
        String xslt;
        if (transformationKey.isCorpusTransformationKey()) {
            xslt = getFicheXslt(bdfServer, transformationKey, initAttributes);
        } else {
            switch (transformationKey.getKeyString()) {
                case TransformationKey.COMPILATION:
                    xslt = getFicheXslt(bdfServer, transformationKey, initAttributes);
                    break;
                case TransformationKey.SECTION:
                case TransformationKey.FORMAT:
                    xslt = getResourceTransformationXslt(bdfServer, transformationKey);
                    break;
                default:
                    xslt = getIncludeTransformationXslt(transformationKey);
            }
        }
        TemplateStorageUnitBuilder builder = TemplateStorageUnitBuilder.init("xslt", templateDef);
        builder.addStorageContent("transformer.xsl", xslt);
        if (options.withExtractionDef()) {
            ExtractionDef extractionDef = DefaultExtractionDefFactory.newInstance(bdfServer, transformationKey, initAttributes);
            if (extractionDef != null) {
                String extractionContent = DefaultTemplateUtils.getExtractionDefXML(extractionDef, options.compactStyle());
                builder.addStorageContent("extraction.xml", extractionContent);
            }
        }
        return builder.toTemplateStorageUnit();
    }

    private static String getResourceTransformationXslt(BdfServer bdfServer, TransformationKey transformationKey) {
        RelativePath path = RelativePath.build("xslt/v2/transformations/html/" + transformationKey + ".xsl");
        return bdfServer.getResourceStorages().getResourceDocStream(path).getContent();
    }

    private static String getIncludeTransformationXslt(TransformationKey transformationKey) {
        StringBuilder buf = new StringBuilder();
        XsltWriter xsltWriter = new XsltWriter();
        xsltWriter.setAppendable(buf);
        xsltWriter.setIndentLength(0);
        try {
            xsltWriter.appendXMLDeclaration();
            xsltWriter.openXslStyleSheet();
            xsltWriter.addXslInclude("bdf://this/xslt/v2/transformations/html/" + transformationKey + ".xsl");
            xsltWriter.closeXslStyleSheet();
        } catch (IOException ioe) {
        }
        return buf.toString();
    }

    private static String getFicheXslt(BdfServer bdfServer, TransformationKey transformationKey, Attributes attributes) {
        int extractVersion = TransformationUtils.getExtractVersion(attributes);
        StringBuilder buf = new StringBuilder();
        SimpleFicheXsltWriter ficheXsltWriter = new SimpleFicheXsltWriter(bdfServer, extractVersion);
        ficheXsltWriter.setAppendable(buf);
        try {
            ficheXsltWriter.appendXMLDeclaration();
            ficheXsltWriter.openXslStyleSheet();
            if (transformationKey.isCorpusTransformationKey()) {
                ficheXsltWriter.addXslInclude("bdf://this/xslt/v2/transformations/html/fiche.xsl");
                SubsetKey corpusKey = transformationKey.toCorpusKey();
                Corpus corpus = (Corpus) bdfServer.getFichotheque().getSubset(corpusKey);
                ficheXsltWriter.add(corpus, false);
            } else {
                ficheXsltWriter.addXslInclude("bdf://this/xslt/v2/transformations/html/compilation.xsl");
                for (Corpus corpus : BdfTransformationUtils.getCorpusList(bdfServer.getFichotheque(), attributes)) {
                    ficheXsltWriter.add(corpus, true);
                }
            }
            ficheXsltWriter.closeXslStyleSheet();
        } catch (IOException ioe) {
        }
        return buf.toString();
    }


}
