/* BdfServer - Copyright (c) 2022-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import fr.exemole.bdfserver.tools.exportation.transformation.TransformationAvailabilities;
import fr.exemole.bdfserver.tools.storage.TemplateStorageUnitBuilder;
import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.tools.exportation.transformation.TemplateDefBuilder;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
class XsltFragmentUnit {

    private XsltFragmentUnit() {

    }

    static TemplateStorage.Unit build(BdfServer bdfServer, TemplateKey templateKey, Attributes initAttributes) {
        if (!templateKey.isSimpleTemplate()) {
            throw new IllegalArgumentException("templateKey is not a simple template key");
        }
        TransformationKey transformationKey = templateKey.getTransformationKey();
        if (!TransformationAvailabilities.hasDefaultFragment(transformationKey)) {
            throw new IllegalArgumentException("No fragment available");
        }
        TemplateDef templateDef = TemplateDefBuilder.init(templateKey, initAttributes).toTemplateDef();
        String xslt = getTransformationXslt(bdfServer, transformationKey);
        return ((TemplateStorageUnitBuilder) TemplateStorageUnitBuilder.init("xslt", templateDef)
                .addStorageContent("transformer.xsl", xslt))
                .toTemplateStorageUnit();
    }

    private static String getTransformationXslt(BdfServer bdfServer, TransformationKey transformationKey) {
        RelativePath path = RelativePath.build("xslt/v2/transformations/fragment/" + transformationKey + ".xsl");
        return bdfServer.getResourceStorages().getResourceDocStream(path).getContent();
    }

}
