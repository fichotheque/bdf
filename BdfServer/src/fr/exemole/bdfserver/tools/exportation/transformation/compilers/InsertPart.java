/* BdfServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation.compilers;


/**
 *
 * @author Vincent Calame
 */
class InsertPart {

    private final String name;

    InsertPart(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
