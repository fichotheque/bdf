/* BdfServer - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.tools.exportation.transformation;

import fr.exemole.bdfserver.api.exportation.transformation.StreamTemplateFactory;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Map;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import net.fichotheque.exportation.transformation.StreamTemplate;
import net.fichotheque.extraction.def.ExtractionDef;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.exceptions.NestedTransformerException;
import net.mapeadores.util.io.StreamTransformer;


/**
 *
 * @author Vincent Calame
 */
public class XsltStreamTemplateFactory implements StreamTemplateFactory {

    private final Attributes attributes;
    private final ExtractionDef customExtractionDef;
    private final StreamTransformer streamTransformer;


    public XsltStreamTemplateFactory(Attributes attributes, ExtractionDef customExtractionDef, StreamTransformer streamTransformer) {
        this.attributes = attributes;
        this.streamTransformer = streamTransformer;
        this.customExtractionDef = customExtractionDef;
    }

    @Override
    public StreamTemplate.Xslt newInstance() {
        return new XsltStreamTemplate(attributes, customExtractionDef, streamTransformer);
    }


    private static class XsltStreamTemplate implements StreamTemplate.Xslt {

        private final Attributes attributes;
        private final ExtractionDef customExtractionDef;
        private final StreamTransformer streamTransformer;

        public XsltStreamTemplate(Attributes attributes, ExtractionDef customExtractionDef, StreamTransformer streamTransformer) {
            this.attributes = attributes;
            this.customExtractionDef = customExtractionDef;
            this.streamTransformer = streamTransformer;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public ExtractionDef getCustomExtractionDef() {
            return customExtractionDef;
        }

        @Override
        public String getMimeType() {
            return streamTransformer.getMimeType();
        }

        @Override
        public void transform(String extractionString, OutputStream outputStream, Map<String, Object> transformerParameters, Map<String, String> outputProperties) throws IOException {
            Source xmlSource = new StreamSource(new StringReader(extractionString));
            try {
                streamTransformer.transform(xmlSource, outputStream, transformerParameters, outputProperties);
            } catch (TransformerException te) {
                throw new NestedTransformerException(te);
            }
        }

    }

}
