/* BdfServer - Copyright (c) 2016-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.transformation;

import net.fichotheque.exportation.transformation.TemplateDef;
import java.io.IOException;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class TemplateDefXMLPart extends XMLPart {

    public TemplateDefXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addTemplateDef(TemplateDef templateDef) throws IOException {
        openTag("template");
        LabelUtils.addLabels(this, templateDef.getTitleLabels());
        AttributeUtils.addAttributes(this, templateDef.getAttributes());
        closeTag("template");
    }

}
