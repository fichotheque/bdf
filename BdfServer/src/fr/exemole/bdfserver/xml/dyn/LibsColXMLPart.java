/* BdfServer - Copyright (c) 2007-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.dyn;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.libscol.LibsColKey;
import fr.exemole.bdfserver.api.ui.CommentUi;
import fr.exemole.bdfserver.api.ui.IncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.L10nUtils;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import java.io.IOException;
import java.util.List;
import net.fichotheque.Fichotheque;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class LibsColXMLPart extends XMLPart {

    private final LibsXMLPart libsXMLWriter;
    private final Lang lang;
    private final BdfServer bdfServer;
    private final Fichotheque fichotheque;

    public LibsColXMLPart(XMLWriter xmlWriter, BdfServer bdfServer, Lang lang) {
        super(xmlWriter);
        this.lang = lang;
        libsXMLWriter = new LibsXMLPart(xmlWriter, lang);
        this.bdfServer = bdfServer;
        this.fichotheque = bdfServer.getFichotheque();
    }

    public void appendLibsCol(LibsColKey libsColKey) throws IOException {
        openTag("libscol");
        if (libsColKey.isFromSubset()) {
            SubsetKey subsetKey = libsColKey.getSubsetKey();
            Subset subset = bdfServer.getFichotheque().getSubset(subsetKey);
            if (subset instanceof Corpus) {
                appendCorpus((Corpus) subset);
            }
        } else if (libsColKey.equals(LibsColKey.ALLCORPUS)) {
            appendAllCorpus();
        } else {
            throw new SwitchException("Unknown libsColKey = " + libsColKey.toString());
        }
        closeTag("libscol");
    }

    private void appendAllCorpus() throws IOException {
        for (Corpus corpus : fichotheque.getCorpusList()) {
            appendCorpus(corpus);
        }
    }

    private void appendCorpus(Corpus corpus) throws IOException {
        libsXMLWriter.appendCorpusLibs(corpus.getCorpusMetadata());
        UiComponents uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        appendUiComponents(corpus.getSubsetName(), uiComponents);
    }

    private void appendUiComponents(String corpusName, UiComponents uiComponents) throws IOException {
        startOpenTag("libs");
        addAttribute("category", "corpusui");
        addAttribute("corpus", corpusName);
        endOpenTag();
        for (UiComponent uiComponent : uiComponents.getUiComponentList()) {
            if (uiComponent instanceof IncludeUi) {
                IncludeUi includeUi = (IncludeUi) uiComponent;
                String labelString = L10nUtils.getIncludeTitle(bdfServer, includeUi, lang);
                if (labelString == null) {
                    labelString = includeUi.getName();
                }
                appendIncludeUiLib(includeUi.getName(), labelString);
                if (includeUi.getName().equals(FichothequeConstants.PARENTAGE_NAME)) {
                    appendIncludeUiLib("rattachement", labelString);
                }
            }
        }
        appendSpecialIncludeUiLib(FichothequeConstants.DATECREATION_NAME);
        appendSpecialIncludeUiLib(FichothequeConstants.DATEMODIFICATION_NAME);
        List<CommentUi> commentList = UiUtils.getCommentUiList(uiComponents);
        for (CommentUi commentUi : commentList) {
            appendCommentUiLib(commentUi.getCommentName(), commentUi.getHtmlByLang(lang));
        }
        closeTag("libs");
    }

    private void appendSpecialIncludeUiLib(String includeKeyString) throws IOException {
        String labelString = L10nUtils.getSpecialIncludeTitle(bdfServer, includeKeyString, lang);
        if (labelString == null) {
            labelString = includeKeyString;
        }
        appendIncludeUiLib(includeKeyString, labelString);
    }

    private void appendIncludeUiLib(String includeKeyString, String labelString) throws IOException {
        startOpenTag("lib");
        addAttribute("include-key", includeKeyString);
        endOpenTag();
        addText(labelString);
        closeTag("lib", false);
    }

    private void appendCommentUiLib(String commentName, CharSequence labelString) throws IOException {
        startOpenTag("lib");
        addAttribute("comment-name", commentName);
        endOpenTag();
        addText(labelString);
        closeTag("lib", false);
    }

    public static String getLibsColXML(Lang lang, BdfServer bdfServer, LibsColKey libsColKey, boolean withTabs) {
        StringBuilder buf = new StringBuilder(1024);
        AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf, (withTabs) ? 0 : -999);
        try {
            xmlWriter.appendXMLDeclaration();
            LibsColXMLPart libsColXMLPart = new LibsColXMLPart(xmlWriter, bdfServer, lang);
            libsColXMLPart.appendLibsCol(libsColKey);
        } catch (IOException ioe) {
        }
        return buf.toString();
    }

}
