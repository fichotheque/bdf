/* BdfServer - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.dyn;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.namespaces.RoleSpace;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.api.ui.SubsetIncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.api.users.BdfUser;
import java.io.IOException;
import net.fichotheque.Subset;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.include.ExtendedIncludeKey;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class UserXMLPart extends XMLPart {

    private final BdfServer bdfServer;
    private final BdfUser bdfUser;
    private final PermissionSummary permissionSummary;

    public UserXMLPart(XMLWriter xmlWriter, BdfServer bdfServer, BdfUser bdfUser, PermissionSummary permissionSummary) {
        super(xmlWriter);
        this.bdfServer = bdfServer;
        this.bdfUser = bdfUser;
        this.permissionSummary = permissionSummary;
    }

    public void start() throws IOException {
        Redacteur redacteur = bdfUser.getRedacteur();
        startOpenTag("user");
        addAttribute("sphere", redacteur.getSubsetName());
        addAttribute("id", redacteur.getId());
        addAttribute("login", redacteur.getLogin());
        endOpenTag();
    }

    public void addCorpusUi(Corpus corpus) throws IOException {
        boolean hideMaster = hideMaster(corpus);
        startOpenTag("ui");
        addAttribute("corpus", corpus.getSubsetName());
        endOpenTag();
        if (!permissionSummary.hasAccess(corpus)) {
            addEmptyElement("deny");
            closeTag("ui");
            return;
        }
        UiComponents mainUiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        for (UiComponent uiComponent : mainUiComponents.getUiComponentList()) {
            if (uiComponent instanceof SubsetIncludeUi) {
                SubsetIncludeUi includeUi = ((SubsetIncludeUi) uiComponent);
                if (testHide(includeUi.getExtendedIncludeKey(), hideMaster, includeUi.getAttributes())) {
                    startOpenTag("hide");
                    addAttribute("name", uiComponent.getName());
                    closeEmptyTag();
                }
            }
        }
        closeTag("ui");
    }

    public void end() throws IOException {
        closeTag("user");
    }

    private boolean hideMaster(Corpus corpus) {
        Subset subset = corpus.getMasterSubset();
        if (subset == null) {
            return false;
        }
        return !permissionSummary.hasAccess(subset.getSubsetKey());
    }

    private boolean testHide(ExtendedIncludeKey includeKey, boolean hideMaster, Attributes attributes) {
        if (!permissionSummary.hasAccess(includeKey.getSubsetKey())) {
            return true;
        }
        if ((includeKey.isMaster()) && (hideMaster)) {
            return true;
        }
        if (!RoleSpace.canRead(permissionSummary, attributes)) {
            return true;
        }
        return false;
    }

    public static void writeDefaultUser(XMLWriter xmlWriter) throws IOException {
        xmlWriter.startOpenTag("user");
        xmlWriter.endOpenTag();
        xmlWriter.closeTag("user");
    }

}
