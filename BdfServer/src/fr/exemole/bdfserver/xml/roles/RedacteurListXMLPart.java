/* BdfServer - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.roles;

import java.io.IOException;
import java.util.List;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurListXMLPart extends XMLPart {

    public RedacteurListXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addRedacteurList(List<Redacteur> redacteurList) throws IOException {
        openTag("redacteur-list");
        for (Redacteur redacteur : redacteurList) {
            startOpenTag("redacteur");
            addAttribute("sphere", redacteur.getSubsetName());
            addAttribute("id", redacteur.getId());
            closeEmptyTag();
        }
        closeTag("redacteur-list");
    }

}
