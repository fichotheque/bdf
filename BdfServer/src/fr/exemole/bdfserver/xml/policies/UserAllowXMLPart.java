/* BdfServer - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.policies;

import fr.exemole.bdfserver.api.policies.UserAllow;
import java.io.IOException;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class UserAllowXMLPart extends XMLPart {

    public UserAllowXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addUserAllow(UserAllow userAllow) throws IOException {
        openTag("user-allow");
        if (userAllow.isDataChangeAllowed()) {
            addEmptyElement("data");
        }
        if (userAllow.isPasswordChangeAllowed()) {
            addEmptyElement("password");
        }
        closeTag("user-allow");
    }

}
