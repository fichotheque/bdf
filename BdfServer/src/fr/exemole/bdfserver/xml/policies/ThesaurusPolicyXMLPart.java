/* BdfServer - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.policies;

import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.externalsource.ExternalSourceDef;
import net.fichotheque.thesaurus.policies.DynamicEditPolicy;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusPolicyXMLPart extends XMLPart {

    public ThesaurusPolicyXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addThesaurusPolicy(SubsetKey subsetKey, DynamicEditPolicy policy) throws IOException {
        startOpenTag("thesaurus-policy");
        if (policy instanceof DynamicEditPolicy.Check) {
            DynamicEditPolicy.Check checkPolicy = (DynamicEditPolicy.Check) policy;
            addAttribute("type", "check");
            endOpenTag();
            for (SubsetKey otherSubsetKey : checkPolicy.getCheckSubseKeyList()) {
                addSimpleElement("thesaurus", otherSubsetKey.getSubsetName());
            }
        } else if (policy instanceof DynamicEditPolicy.Transfer) {
            DynamicEditPolicy.Transfer transferPolicy = (DynamicEditPolicy.Transfer) policy;
            addAttribute("type", "transfer");
            endOpenTag();
            addSimpleElement("thesaurus", transferPolicy.getTransferThesaurusKey().getSubsetName());
        } else if (policy instanceof DynamicEditPolicy.Allow) {
            addAttribute("type", "allow");
            endOpenTag();
        } else if (policy instanceof DynamicEditPolicy.External) {
            ExternalSourceDef externalSourceDef = ((DynamicEditPolicy.External) policy).getExternalSourceDef();
            addAttribute("type", "external");
            addAttribute("source-type", externalSourceDef.getType());
            endOpenTag();
            for (String paramName : externalSourceDef.getParamNameSet()) {
                startOpenTag("param");
                addAttribute("name", paramName);
                endOpenTag();
                addText(externalSourceDef.getParam(paramName));
                closeTag("param", false);
            }
        } else {
            endOpenTag();
        }
        closeTag("thesaurus-policy");
    }

}
