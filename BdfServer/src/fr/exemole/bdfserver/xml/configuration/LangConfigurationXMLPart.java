/* BdfServer - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.configuration;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import java.io.IOException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class LangConfigurationXMLPart extends XMLPart {

    public LangConfigurationXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addLangConfiguration(LangConfiguration langConfiguration) throws IOException {
        openTag("lang-configuration");
        for (Lang lang : langConfiguration.getWorkingLangs()) {
            startOpenTag("working-lang");
            addAttribute("lang", lang.toString());
            closeEmptyTag();
        }
        for (Lang lang : langConfiguration.getSupplementaryLangs()) {
            startOpenTag("supplementary-lang");
            addAttribute("lang", lang.toString());
            closeEmptyTag();
        }
        if (langConfiguration.isAllLanguages()) {
            addEmptyElement("all-languages");
        }
        if (langConfiguration.isWithNonlatin()) {
            addEmptyElement("with-nonlatin");
        }
        if (langConfiguration.isWithoutSurnameFirst()) {
            addEmptyElement("without-surname-first");
        }
        closeTag("lang-configuration");
    }

}
