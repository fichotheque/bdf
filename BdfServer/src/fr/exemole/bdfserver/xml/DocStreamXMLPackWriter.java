/* BdfServer - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml;

import java.io.IOException;
import java.io.InputStream;
import net.mapeadores.util.base64.Base64;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.xml.DefaultXMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class DocStreamXMLPackWriter extends DefaultXMLWriter {

    public DocStreamXMLPackWriter() {
    }

    public void appendDocStream(DocStream docStream, String path) throws IOException {
        String charset = docStream.getCharset();
        startOpenTag("xml-pack");
        if (charset == null) {
            addAttribute("type", "base64");
        } else {
            addAttribute("type", "cdata");
        }
        addAttribute("path", path);
        addAttribute("mime-type", docStream.getMimeType());
        endOpenTag();
        if (charset == null) {
            try (InputStream is = docStream.getInputStream()) {
                byte[] byteArray = IOUtils.toByteArray(is);
                append(Base64.encodeBase64String(byteArray));
            }
        } else {
            try (InputStream is = docStream.getInputStream()) {
                String text = IOUtils.toString(is, charset);
                addCData(text);
            }
        }
        closeTag("xml-pack", false);
    }

}
