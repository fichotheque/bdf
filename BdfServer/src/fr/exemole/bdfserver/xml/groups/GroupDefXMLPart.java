/* BdfServer - Copyright (c) 2014-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.groups;

import fr.exemole.bdfserver.api.groups.GroupDef;
import java.io.IOException;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class GroupDefXMLPart extends XMLPart {

    public GroupDefXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addGroupDef(GroupDef roleDef) throws IOException {
        openTag("group");
        LabelUtils.addLabels(this, roleDef.getTitleLabels());
        AttributeUtils.addAttributes(this, roleDef.getAttributes());
        closeTag("group");
    }

}
