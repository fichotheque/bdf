/* BdfServer - Copyright (c) 2008-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.xml.users;

import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import java.io.IOException;
import java.util.Locale;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class BdfUserPrefsXMLPart extends XMLPart {

    public BdfUserPrefsXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addPrefs(BdfUserPrefs bdfUserPrefs) throws IOException {
        openTag("user-prefs");
        appendLocalisationInfo(bdfUserPrefs);
        appendSelection(bdfUserPrefs);
        AttributeUtils.addAttributes(this, bdfUserPrefs.getAttributes());
        closeTag("user-prefs");
    }

    private void appendLocalisationInfo(BdfUserPrefs bdfUserPrefs) throws IOException {
        Lang workingLang = bdfUserPrefs.getWorkingLang();
        if (workingLang != null) {
            addSimpleElement("lang", workingLang.toString());
        }
        Locale customFormatLocale = bdfUserPrefs.getCustomFormatLocale();
        if (customFormatLocale != null) {
            addSimpleElement("format-locale", Lang.toISOString(customFormatLocale));
        }
        LangPreference customLangPreference = bdfUserPrefs.getCustomLangPreference();
        if (customLangPreference != null) {
            openTag("lang-preference");
            for (Lang lang : customLangPreference) {
                addSimpleElement("lang", lang.toString());
            }
            closeTag("lang-preference");
        }
    }

    private void appendSelection(BdfUserPrefs bdfUserPrefs) throws IOException {
        FicheQuery ficheQuery = bdfUserPrefs.getDefaultFicheQuery();
        if (ficheQuery != null) {
            openTag("selection");
            FichothequeXMLUtils.writeFicheQuery(this, ficheQuery);
            closeTag("selection");
        }
    }

}
