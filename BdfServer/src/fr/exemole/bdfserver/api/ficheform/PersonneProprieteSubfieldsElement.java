/* BdfServer_API - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
public interface PersonneProprieteSubfieldsElement extends FormElement.Field {

    public PersonCore getPersonCore();

    public boolean isWithNonlatin();

    public boolean isWithoutSurnameFirst();

}
