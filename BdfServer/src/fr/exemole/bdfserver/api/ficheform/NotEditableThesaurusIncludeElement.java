/* BdfServer_API - Copyright (c) 2009-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import java.util.Set;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface NotEditableThesaurusIncludeElement extends FormElement.Include {

    public Lang getWorkingLang();

    /**
     * Non nul mais peut être de longueur nulle.
     */
    public String getIdalphaSeparator();

    public Set<Motcle> getSelectedMotcleSet();

}
