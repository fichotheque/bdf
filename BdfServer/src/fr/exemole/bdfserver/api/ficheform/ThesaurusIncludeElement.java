/* BdfServer_API - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetKey;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attribute;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusIncludeElement extends FormElement.Include {

    public Thesaurus getThesaurus();

    @Nullable
    public Attribute getIdalphaStyle();

    public List<Entry> getEntryList();

    public default boolean isEmpty() {
        return getEntryList().isEmpty();
    }

    public default boolean contains(Motcle motcle) {
        for (Entry entry : getEntryList()) {
            if (entry.getMotcle().equals(motcle)) {
                return true;
            }
        }
        return false;
    }


    public static interface NotEditable extends ThesaurusIncludeElement {

    }


    public static interface Hidden extends ThesaurusIncludeElement {

        /**
         * Non nul mais peut être de longueur nulle.
         */
        public String getValue();

    }


    public interface Text extends ThesaurusIncludeElement {


        public SubsetKey getDestinationSubsetKey();

        /**
         * Non nul mais peut être de longueur nulle.
         */
        public String getValue();

        public boolean isRedimAllowed();

        public int getRows();

        /**
         * Doit avoir les valeurs possibles de InputUiOption
         */
        public String getWidthType();

        public boolean isWithExternalSource();

    }


    public interface Choice extends ThesaurusIncludeElement {

        public String geChoiceType();

        public boolean isNoneAllowed();

        public SubsetKey getDestinationSubsetKey();

        public Predicate<Motcle> getFilterPredicate();

        public boolean isNewIndexation();

    }


    public interface FicheStyle extends ThesaurusIncludeElement {

        public boolean hasPoidsFilter();

        public boolean isWithExternalSource();

    }


    public static interface Entry {

        public Motcle getMotcle();

        public int getPoids();

        public String getValue();

    }

}
