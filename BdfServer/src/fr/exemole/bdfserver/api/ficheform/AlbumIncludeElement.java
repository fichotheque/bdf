/* BdfServer_API - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import java.util.List;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumIncludeElement extends FormElement.Include {

    public Album getAlbum();

    public List<Illustration> getIllustrationList();

}
