/* BdfServer_API - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import java.util.List;
import net.mapeadores.util.money.ExtendedCurrency;


/**
 *
 * @author Vincent Calame
 */
public interface MontantInformationSubfieldsElement extends FormElement.Field {

    public List<Entry> getEntryList();

    /**
     * Non nul mais peut être de longueur nulle.
     */
    public String getOthersValue();


    public static interface Entry {

        public ExtendedCurrency getCurrency();

        /**
         * Non nul mais peut être de longueur nulle.
         */
        public String getMontantValue();

    }

}
