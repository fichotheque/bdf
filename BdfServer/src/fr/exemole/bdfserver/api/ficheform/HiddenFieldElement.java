/* BdfServer_API - Copyright (c) 2009-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;


/**
 *
 * @author Vincent Calame
 */
public interface HiddenFieldElement extends FormElement.Field {

    /**
     * Non nul mais peut être de longueur nulle.
     */
    public String getValue();


}
