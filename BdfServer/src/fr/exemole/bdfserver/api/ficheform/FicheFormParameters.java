/* BdfServer - Copyright (c) 2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import net.fichotheque.SubsetItem;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.syntax.FicheblockSyntax;
import net.fichotheque.syntax.FormSyntax;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.text.SeparatorOptions;


/**
 *
 * @author Vincent Calame
 */
public interface FicheFormParameters {

    public BdfServer getBdfServer();

    public PermissionSummary getPermissionSummary();

    public UserLangContext getUserLangContext();

    public FormSyntax.Parameters getFicheItemFormSyntaxParameters();

    public FicheblockSyntax.Parameters getFicheBlockFormSyntaxParameters();

    /**
     * Peut être nul.
     */
    public Redacteur getDefaultRedacteur();

    public SeparatorOptions getInlineSeparatorOptions();

    public SeparatorOptions getBlockSeparatorOptions();

    public SubsetItem getMasterSubsetItem();

    public String getSupplementaryParameter(String name);

    public String getCustomDefaultValue(String componentName);

    public default Lang getWorkingLang() {
        return getUserLangContext().getWorkingLang();
    }

    public default ExtractionContext getDefaultExtractionContext() {
        return BdfServerUtils.initExtractionContextBuilder(getBdfServer(), getUserLangContext(), getPermissionSummary()).toExtractionContext();
    }

}
