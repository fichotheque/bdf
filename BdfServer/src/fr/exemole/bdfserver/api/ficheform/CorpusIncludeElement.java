/* BdfServer_API - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.table.CellEngine;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusIncludeElement extends FormElement.Include {

    @Nullable
    public Corpus getCorpus();

    public List<Entry> getEntryList();


    public static interface Entry {

        public FicheMeta getFicheMeta();

        public int getPoids();

        public String getValue();

    }


    public interface Check extends CorpusIncludeElement {

        public int getRows();

        public boolean hasPoidsFilter();

    }


    public interface Table extends CorpusIncludeElement {

        public CellEngine getCellEngine();

    }

}
