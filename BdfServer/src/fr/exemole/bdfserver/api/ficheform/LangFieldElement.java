/* BdfServer_API - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;


/**
 * Interface des paramètres pour la construction d'un élément de formulaire
 * consacré à la langue d'une fiche.
 *
 * @author Vincent Calame
 */
public interface LangFieldElement extends FormElement.Field {

    /**
     * Retourne la valeur initiale de la langue. Cette valeur peut être nulle
     * pour indiquer l'absence de valeur intiale.
     *
     * @return un objet Lang correspondant à la valeur initiale de la langue ou
     * null
     */
    @Nullable
    public Lang getLang();

    /**
     * Retourne le tableau des langues disponibles. Le tableau peut être nul,
     * indiquant ainsi qu'il n'y a pas de limitation concernant les langues. En
     * revanche, il ne doit pas être de longueur nulle, ne pas contenir de
     * valeurs nulle et aucune langue n'est censée apparaitre deux fois.
     *
     * @return un tableau d'objets Lang indiquant les langues disponibles ou
     * null
     */
    @Nullable
    public Lang[] getAvailableLangArray();

}
