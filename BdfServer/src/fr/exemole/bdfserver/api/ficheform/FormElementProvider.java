/* BdfServer_API - Copyright (c) 2009-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.api.ui.IncludeUi;
import net.fichotheque.pointeurs.FichePointeur;


/**
 *
 * @author Vincent Calame
 */
public interface FormElementProvider {

    public FormElement.Field getFormElement(FichePointeur fichePointeur, FieldUi fieldUi);

    public FormElement.Include getFormElement(FichePointeur fichePointeur, IncludeUi includeUi);

}
