/* BdfServer_API - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import net.mapeadores.util.text.MultiStringable;


/**
 *
 * @author Vincent Calame
 */
public interface GeopointProprieteSubfieldsElement extends FormElement.Field {

    /**
     * Non nul mais peut être de longueur nulle.
     */
    public String getLatitude();

    /**
     * Non nul mais peut être de longueur nulle.
     */
    public String getLongitude();

    /**
     * Peut être nul
     */
    public MultiStringable getAddressFieldNames();

}
