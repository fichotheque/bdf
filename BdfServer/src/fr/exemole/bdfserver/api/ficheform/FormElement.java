/* BdfServer_API - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ficheform;

import net.fichotheque.corpus.metadata.CorpusField;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public interface FormElement {

    /**
     * Non nul mais peut être de longueur nulle.
     */
    public String getLabel();

    public boolean isMandatory();

    public Attributes getAttributes();


    public static interface Field extends FormElement {

        public CorpusField getCorpusField();

    }


    public static interface Include extends FormElement {

        public String getIncludeName();

    }

}
