/* BdfServer - Copyright (c) 2010-2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.externalscript;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public interface ExternalScript {

    public String getName();

    public Process exec() throws IOException;

}
