/* BdfServer_API - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction;


/**
 *
 * @author Vincent Calame
 */
public interface Domains {

    public final static String ALBUM = "album";
    public final static String EDITION = "edition";
    public final static String SPHERE = "sphere";
    public final static String CONFIGURATION = "configuration";
    public final static String SELECTION = "selection";
    public final static String MAILING = "mailing";
    public final static String CORPUS = "corpus";
    public final static String ADMINISTRATION = "administration";
    public final static String EXPORTATION = "exportation";
    public final static String RUN = "run";
    public final static String MAIN = "main";
    public final static String MISC = "misc";
    public final static String PIOCHE = "pioche";
    public final static String SESSION = "session";
    public final static String THESAURUS = "thesaurus";
    public final static String ADDENDA = "addenda";
    public final static String IMPORTATION = "importation";
    public final static String EXTENSION = "ext";
    public final static String APP = "app";
}
