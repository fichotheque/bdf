/* BdfServer_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction;


/**
 *
 * @author Vincent Calame
 */
public interface GetConstants {

    public final static String ADMIN_ROOT = "admin";
    public final static String API_ROOT = "api";
    public final static String BALAYAGECONTENTS_ROOT = "balayagecontents";
    public final static String BALAYAGES_ROOT = "balayages";
    public final static String DIAGRAMS_ROOT = "diagrams";
    public final static String DOCUMENTS_ROOT = "documents";
    public final static String EXTENSION_ROOT = "ext";
    public final static String FICHES_ROOT = "fiches";
    public final static String ILLUSTRATIONS_ROOT = "illustrations";
    public final static String MOTCLES_ROOT = "motcles";
    public final static String TABLEEXPORT_ROOT = "tableexport";
    public final static String TABLES_ROOT = "tables";
    public final static String TRANSFORMATIONS_ROOT = "transformations";
    public final static String USERS_ROOT = "users";
    public final static String DEFAULTEXPORT_PARAMNAME = "defaultexport";
    public final static String EXTRA_XML_PARAMNAME = "extra-xml";
    public final static String LIMIT_PARAMNAME = "limit";
    public final static String MERGE_PARAMNAME = "merge";
    public final static String QUERY_XML_PARAMNAME = "query-xml";
    public final static String SORTTYPE_PARAMNAME = "sorttype";
    public final static String TEMPLATE_PARAMNAME = "template";
    public final static String THESAURUSTABLE_PARAMNAME = "thesaurustable";
    public final static String GET_MULTI = "get-multi";
    public final static String GET_HTMLFRAGMENT = "get-htmlfragment";

}
