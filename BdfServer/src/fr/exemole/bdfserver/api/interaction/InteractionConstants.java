/* BdfServer_API - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction;


/**
 *
 * @author Vincent Calame
 */
public interface InteractionConstants {

    public final static String ACCESS_PARAMNAME = "access";
    public final static String ALBUMDIM_PARAMNAME = "albumdim";
    public final static String BACKGROUND_PARAMNAME = "background";
    public final static String BALAYAGE_PARAMNAME = "balayage";
    public final static String CONFIRMATIONCHECK_PARAMNAME = "confirmationcheck";
    public final static String BDF_DEFAULT_LOGIN_PARAMNAME = "bdf-default-login";
    public final static String BDF_DEFAULT_SPHERE_PARAMNAME = "bdf-default-sphere";
    public final static String BDF_EXIT_PARAMNAME = "bdf-exit";
    public final static String BDF_FRAMESTYLE_PARAMNAME = "bdf-framestyle";
    public final static String BDF_LOGIN_PARAMNAME = "bdf-login";
    public final static String BDF_NOSCRIPT_PARAMNAME = "bdf-noscript";
    public final static String BDF_PASSWORD_PARAMNAME = "bdf-password";
    public final static String BDF_SPHERE_PARAMNAME = "bdf-sphere";
    public final static String CHARSET_PARAMNAME = "charset";
    public final static String CSV_DELIMITER_PARAMNAME = "csv_delimiter";
    public final static String CSV_QUOTE_PARAMNAME = "csv_quote";
    public final static String CSV_ESCAPED_PARAMNAME = "csv_escaped";
    public final static String EXTENSION_PARAMNAME = "extension";
    public final static String GOTO_PARAMNAME = "goto";
    public final static String HEADERTYPE_PARAMNAME = "headertype";
    public final static String ID_PARAMNAME = "id";
    public final static String IDALPHA_PARAMNAME = "idalpha";
    public final static String IDMTCL_PARAMNAME = "idmtcl";
    public final static String MODE_PARAMNAME = "mode";
    public final static String PATH_PARAMNAME = "path";
    public final static String NEWPHRASE_PARAMNAME = "newphrase";
    public final static String PAGE_ERROR_PARAMNAME = "page-err";
    public final static String PAGE_OPTIONS_PARAMNAME = "page-options";
    public final static String PAGE_RESULT_PARAMNAME = "page-result";
    public final static String PAGE_RESULT_OPTIONS_PARAMNAME = "page-result-options";
    public final static String PARSETYPE_PARAMNAME = "parsetype";
    public final static String PREFIXLIST_PARAMNAME = "prefixlist";
    public final static String PREFIXTYPE_PARAMNAME = "prefixtype";
    public final static String REFRESH_PARAMNAME = "refresh";
    public final static String ROLE_PARAMNAME = "role";
    public final static String SCRUTARIEXPORT_PARAMNAME = "scrutariexport";
    public final static String SELECTION_PARAMNAME = "selection";
    public final static String SORT_PARAMNAME = "sort";
    public final static String SQLEXPORT_PARAMNAME = "sqlexport";
    public final static String SUBSET_PARAMNAME = "subset";
    public final static String TABLEEXPORT_PARAMNAME = "tableexport";
    public final static String TEMPLATE_PARAMNAME = "template";
    public final static String TRANSFORMATION_PARAMNAME = "transformation";
    public final static String XML_PARAMNAME = "xml";
    public final static String PROPERTIES_PARAMNAME = "properties";
    public final static String WITH_PARAMNAME = "with";
    public final static String PATTERNMODE_PARAMNAME = "patternmode";
    public final static String OK_CONFIRMATIONCHECK_PARAMVALUE = "ok";
    public final static String SATELLITES_PREFIXTYPE_PARAMVALUE = "satellites";
    public final static String NEWPHRASE_PARAMPREFIX = "newphrase/";
    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String MESSAGE_PAGE = "_message";
    public final static String DEFAULT_JSON = "_default";
    public final static String SUBUNIT_MODE = "subunit";
}
