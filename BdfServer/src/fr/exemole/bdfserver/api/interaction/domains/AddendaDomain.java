/* BdfServer_API - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface AddendaDomain {

    public final static String ADDENDA_PAGE = "addenda";
    public final static String ADDENDA_ADVANCEDCOMMANDS_PAGE = "addenda-advancedcommands";
    public final static String ADDENDA_CREATIONFORM_PAGE = "addenda-creationform";
    public final static String ADDENDA_METADATAFORM_PAGE = "addenda-metadataform";
    public final static String DOCUMENT_ADMINFORM_PAGE = "document-adminform";
    public final static String DOCUMENT_ADVANCEDCOMMANDS_PAGE = "document-advancedcommands";
    public final static String DOCUMENT_CHANGE_PAGE = "document-change";
    public final static String DOCUMENT_CREATE_PAGE = "document-create";
    public final static String DOCUMENT_UPLOAD_CONFIRM_PAGE = "document-upload-confirm";
    public final static String DOCUMENT_UPLOAD_NEW_PAGE = "document-upload-new";
    public final static String DOCUMENT_UPLOAD_VERSION_PAGE = "document-upload-version";
    public final static String CREATION_CHANGETYPE = "creation";
    public final static String DOCUMENT_CHANGETYPE = "document";
    public final static String DOCUMENT_ARRAY_JSON = "document-array";

}
