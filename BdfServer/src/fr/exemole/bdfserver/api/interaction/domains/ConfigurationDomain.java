/* BdfServer_API - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface ConfigurationDomain {

    public final static String CONFIGADVANCEDCOMMANDS_PAGE = "config-advancedcommands";
    public final static String COREFORM_PAGE = "config-core";
    public final static String ETCCONFIG_PAGE = "config-etc";
    public final static String EXTENSIONSFORM_PAGE = "config-extensions";
    public final static String GROUPFORM_PAGE = "config-groups";
    public final static String LOGOSFORM_PAGE = "config-logos";
    public final static String PHRASESFORM_PAGE = "config-phrases";
    public final static String SUBSETTREEFORM_PAGE = "config-subsettree";
    public final static String EXTERNALSCRIPT_ARRAY_JSON = "externalscript-array";
    public final static String LANGCONFIGURATION_JSON = "langconfiguration";
    public final static String PATHCONFIGURATION_JSON = "pathconfiguration";
    public final static String SUBSETTREES_JSON = "subsettrees";
    public final static String WITHDETAILS_PARAMNAME = "withdetails";

}
