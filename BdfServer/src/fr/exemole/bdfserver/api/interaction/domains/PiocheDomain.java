/* BdfServer_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface PiocheDomain {

    public final static String DOCUMENT_PAGE = "document";
    public final static String EXTERNALSOURCE_PAGE = "externalsource";
    public final static String FIELD_PAGE = "field";
    public final static String FICHE_PAGE = "fiche";
    public final static String GEOCODAGE_PAGE = "geocodage";
    public final static String LANGUE_PAGE = "langue";
    public final static String MOTCLE_PAGE = "motcle";
    public final static String PAYS_PAGE = "pays";
    public final static String REDACTEUR_PAGE = "redacteur";
    public final static String CORPUS_PARAMNAME = "corpus";
    public final static String FIELD_PARAMNAME = "field";
    public final static String APPELANT_PARAMNAME = "appelant";
    public final static String LAT_PARAMNAME = "lat";
    public final static String LON_PARAMNAME = "lon";
    public final static String NOCURRENT_PARAMNAME = "nocurrent";
    public final static String CATEGORY_PARAMNAME = "category";
    public final static String ACTION_PARAMNAME = "action";
    public final static String DEFAULSPHERE_PARAMNAME = "defaultsphere";
    public final static String IMAGEFIELD_PARAMNAME = "imagefield";
    public final static String FILENUMBER_PARAMNAME = "filenumber";
    public final static String FIELDTYPE_PARAMNAME = "fieldtype";
    public final static String Q_PARAMNAME = "q";
    public final static String ADDRESSFIELDS_PARAMNAME = "addressfields";
    public final static String BUTTONS_PARAMNAME = "buttons";
    public final static String SUBSETS_PARAMNAME = "subsets";
    public final static String CROISEMENT_PARAMNAME = "croisement";
    public final static String LIMIT_PARAMNAME = "limit";
    public final static String SORTPRECEDENCE_PARAMNAME = "sortprecedence";
    public final static String TEMPFILENAME_PARAMNAME = "tempfilename";
    public final static String WANTED_PARAMNAME = "wanted";
    public final static String ZOOM_PARAMNAME = "zoom";
    public final static String ID_PARAMVALUE = "Id";
    public final static String ID_WANTED = "id";
    public final static String CODE_ID_WANTED = "code-id";
    public final static String CODE_TITLE_WANTED = "code-title";
    public final static String EXTERNALITEM_JSON = "externalitem";
    public final static String DOCUMENT_JSON = "document";
    public final static String FICHE_JSON = "fiche";
    public final static String MOTCLE_JSON = "motcle";
    public final static String REDACTEUR_JSON = "redacteur";
    public final static String PAYS_JSON = "pays";
    public final static String LANGUE_JSON = "langue";
    public final static String GEOCODAGE_JSON = "geocodage";
    public final static String ILLUSTRATION_JSON = "illustration";
    public final static String FIELD_JSON = "field";
    public final static String DATATION_FIELDTYPE = "datation";
    public final static String ALL_FIELDTYPE = "all";
}
