/* BdfServer_API - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface MiscDomain {

    public final static String BLANK_PAGE = "blank";
    public final static String DIAGRAMS_PAGE = "diagrams";
    public final static String FICHESGEO_PAGE = "fichesgeo";
    public final static String GLOBALSTATS_PAGE = "globalstats";
    public final static String HISTORY_PAGE = "history";
    public final static String ISO_LANGUAGES_PAGE = "iso-languages";
    public final static String ISO_COUNTRIES_PAGE = "iso-countries";
    public final static String MEMENTO_PAGE = "memento";
    public final static String TABLEEXPORTFORM_PAGE = "tableexportform";
    public final static String FICHESGEO_JSON = "fichesgeo";
    public final static String MEMENTO_UNITINFO_ARRAY_JSON = "memento-unitinfo-array";
    public final static String MEMENTO_UNIT_JSON = "memento-unit";
    public final static String HISTORY_JSON = "history";
    public final static String REVISIONS_FICHE_JSON = "revisions-fiche";
}
