/* BdfServer_API - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface MailingDomain {

    public final static String SEND_FORM_PAGE = "send-form";
    public final static String SEND_REPORT_PAGE = "send-report";
    public final static String COMPILATION_SENDTYPE = "compilation";
    public final static String FICHE_SENDTYPE = "fiche";
    public final static String SELECTION_SENDTYPE = "selection";
    public final static String TABLEEXPORT_SENDTYPE = "tableexport";
}
