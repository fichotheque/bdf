/* BdfServer_API - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusDomain {

    public final static String MOTCLE_ADVANCEDCOMMANDS_PAGE = "motcle-advancedcommands";
    public final static String MOTCLE_CHANGEFORM_PAGE = "motcle-changeform";
    public final static String MOTCLE_CREATIONFORM_PAGE = "motcle-creationform";
    public final static String MOTCLE_INDEXATIONFORM_PAGE = "motcle-indexationform";
    public final static String MOTCLE_PONDERATIONFORM_PAGE = "motcle-ponderationform";
    public final static String MOTCLE_SELECTIONINDEXATIONFORM_PAGE = "motcle-selectionindexationform";
    public static final String SELECTIONINDEXATION_FORM_PAGE = "selectionindexation-form";
    public static final String SELECTIONINDEXATION_CHOICE_PAGE = "selectionindexation-choice";
    public final static String THESAURUS_PAGE = "thesaurus";
    public final static String THESAURUS_ADVANCEDCOMMANDS_PAGE = "thesaurus-advancedcommands";
    public final static String THESAURUS_CREATIONFORM_PAGE = "thesaurus-creationform";
    public final static String THESAURUS_METADATAFORM_PAGE = "thesaurus-metadataform";
    public final static String ASC_TRI_PARAMVALUE = "asc";
    public final static String CORPUSASC_TRI_PARAMVALUE = "corpusasc";
    public final static String CORPUSDESC_TRI_PARAMVALUE = "corpusdesc";
    public final static String DESC_TRI_PARAMVALUE = "desc";
    public final static String LABELFILTER_PARAMNAME = "labelfilter";
    public final static String LABELLANG_PARAMNAME = "labellang";
    public final static String SATELLITECORPUS_PARAMNAME = "satellitecorpus";
    public final static String SATELLITEFILTER_PARAMNAME = "satellitefilter";
    public final static String SORT_PARAMNAME = "sort";
    public final static String WITH_FILTERVALUE = "with";
    public final static String WITHOUT_FILTERVALUE = "withoutlabel";
    public final static String MOTCLE_JSON = "motcle";
    public final static String THESAURUS_JSON = "thesaurus";
    public final static String THESAURUS_METADATA_JSON = "thesaurus-metadata";
}
