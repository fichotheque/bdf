/* BdfServer_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface SelectionDomain {

    public final static String DEFS_PAGE = "defs";
    public final static String FQLEDITOR_PAGE = "fqleditor";
    public final static String FQLXML_PAGE = "fqlxml";
    public final static String SELECTFORM_PAGE = "selectform";
    public final static String SELECTIONNAME_PARAMNAME = "selectionname";
    public final static String SELECTIONDEF_JSON = "selectiondef";
    public final static String SELECTIONDEF_ARRAY_JSON = "selectiondef-array";
    public final static String FQL_JSON = "fql";

}
