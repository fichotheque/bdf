/* BdfServer_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface EditionDomain {

    public final static String FICHE_CHANGE_PAGE = "fiche-change";
    public final static String FICHE_CREATION_PAGE = "fiche-creation";
    public final static String FICHE_INDEXATION_PAGE = "fiche-indexation";
    public final static String FICHE_RESULT_PAGE = "fiche-result";
    public final static String SECTIONPREVIEW_PAGE = "sectionpreview";
    public final static String ZOOMEDIT_PAGE = "zoomedit";
    public final static String PREFILL_PARAMNAME = "prefill";
    public final static String FORCE_PARAMNAME = "force";
    public final static String SECTION_JSON = "section";
}
