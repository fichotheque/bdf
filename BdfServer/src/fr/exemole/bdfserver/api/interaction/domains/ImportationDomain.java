/* BdfServer_API - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface ImportationDomain {

    public final static String CORPUSIMPORT_PAGE = "corpusimport";
    public final static String IMPORTATION_CONFIRM_PAGE = "importation-confirm";
    public final static String LABELIMPORT_PAGE = "labelimport";
    public static final String PARSERESULT_JSON = "parseresult";
    public final static String THESAURUSIMPORT_PAGE = "thesaurusimport";
    public final static String FICHEBLOCKSYNTAX_JSON = "ficheblocksyntax";
    public static final String METAREPORT_JSON = "metareport";
}
