/* BdfServer_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusDomain {

    public final static String CORPUS_PAGE = "corpus";
    public final static String CORPUS_ADVANCEDCOMMANDS_PAGE = "corpus-advancedcommands";
    public final static String CORPUS_CONF_PAGE = "corpus-conf";
    public final static String CORPUS_DIAGRAM_PAGE = "corpus-diagram";
    public final static String CORPUS_CREATIONFORM_PAGE = "corpus-creationform";
    public final static String CORPUS_PHRASESFORM_PAGE = "corpus-phrasesform";
    public final static String DUPLICATIONLOG_PAGE = "duplicationlog";
    public final static String FICHE_JSON = "fiche";
    public final static String FICHE_ADDENDA_PAGE = "fiche-addenda";
    public final static String FICHE_ADVANCEDCOMMANDS_PAGE = "fiche-advancedcommands";
    public final static String FICHE_ALBUM_PAGE = "fiche-album";
    public final static String FICHE_DISCARDFORM_PAGE = "fiche-discardform";
    public final static String FIELD_CREATIONFORM_PAGE = "field-creationform";
    public final static String FICHE_REMOVEFORM_PAGE = "fiche-removeform";
    public final static String FICHE_RETRIEVEFORM_PAGE = "fiche-retrieveform";
    public final static String FICHES_JSON = "fiches";
    public final static String FIELD_OPTIONSFORM_PAGE = "field-optionsform";
    public final static String FIELD_REMOVEFORM_PAGE = "field-removeform";
    public final static String INCLUDE_ADDENDA_PAGE = "include-addenda";
    public final static String INCLUDE_ALBUM_PAGE = "include-album";
    public final static String INCLUDE_COMMENT_PAGE = "include-comment";
    public final static String INCLUDE_CORPUS_PAGE = "include-corpus";
    public final static String INCLUDE_EXTERNALSOURCE_PAGE = "include-externalsource";
    public final static String INCLUDE_SPECIAL_PAGE = "include-special";
    public final static String INCLUDE_THESAURUS_PAGE = "include-thesaurus";
    public final static String REPONDERATIONLOG_PAGE = "reponderationlog";
    public final static String UI_COMPONENTATTRIBUTESFORM_PAGE = "ui-componentattributesform";
    public final static String UI_COMPONENTOPTIONSFORM_PAGE = "ui-componentoptionsform";
    public final static String UI_COMPONENTPOSITIONFORM_PAGE = "ui-componentpositionform";
    public final static String UI_JSON = "ui";
    public final static String REMOVEDLIST_PAGE = "removedlist";
    public final static String REMOVEDLIST_JSON = "removedlist";
    public final static String CROISEMENTHISTORYLIST_JSON = "croisementhistorylist";

}
