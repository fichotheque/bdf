/* BdfServer_API - Copyright (c) 2010-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumDomain {

    public final static String ALBUM_PAGE = "album";
    public final static String ALBUM_CREATIONFORM_PAGE = "album-creationform";
    public final static String ALBUM_METADATAFORM_PAGE = "album-metadataform";
    public final static String ALBUM_ADVANCEDCOMMANDS_PAGE = "album-advancedcommands";
    public final static String ILLUSTRATION_UPLOAD_NEW_PAGE = "illustration-upload-new";
    public final static String ILLUSTRATION_UPLOAD_REPLACE_PAGE = "illustration-upload-replace";
    public final static String ILLUSTRATION_UPLOAD_CONFIRM_PAGE = "illustration-upload-confirm";
    public final static String ILLUSTRATION_CHANGE_PAGE = "illustration-change";
    public final static String CREATION_CHANGETYPE = "creation";
    public final static String TMPFILE_CHANGETYPE = "tmpfile";
    public final static String ILLUSTRATION_CHANGETYPE = "illustration";
    public final static String ILLUSTRATION_ADMINFORM_PAGE = "illustration-adminform";
    public final static String ILLUSTRATION_ADVANCEDCOMMANDS_PAGE = "illustration-advancedcommands";
    public final static String ILLUSTRATION_TMPFILE_JSON = "illustration-tmpfile";
}
