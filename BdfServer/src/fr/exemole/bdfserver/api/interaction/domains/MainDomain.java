/* BdfServer_API - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface MainDomain {

    public final static String COLLECTIONS_PAGE = "collections";
    public final static String FICHES_PAGE = "fiches";
    public final static String FRAMESET_PAGE = "frameset";
    public final static String IFRAMES_PAGE = "iframes";
    public final static String MENU_PAGE = "menu";
    public final static String START_PAGE = "start";
    public final static String TABLEDISPLAY_PAGE = "tabledisplay";
    public final static String FICHES_RELOAD_PARAMNAME = "reload";
    public final static String REMEMBER_ACTION_PARAMNAME = "remember_action";
    public final static String AVAILABLEPATTERNS_JSON = "availablepatterns";
    public final static String TABLE_JSON = "table";
}
