/* BdfServer_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface ExportationDomain {

    public final static String ACCESSES_PAGE = "accesses";
    public final static String BALAYAGES_PAGE = "balayages";
    public final static String BALAYAGE_ERROR_PAGE = "balayage-error";
    public final static String BALAYAGE_INDEX_PAGE = "balayage-index";
    public final static String BALAYAGE_RESULT_PAGE = "balayage-result";
    public final static String SCRUTARIEXPORTS_PAGE = "scrutariexports";
    public final static String SQLEXPORTS_PAGE = "sqlexports";
    public final static String TABLEEXPORTS_PAGE = "tableexports";
    public final static String TRANSFORMATIONS_PAGE = "transformations";
    public final static String TRANSFORMATION_BINARYUPDATE_PAGE = "transformation-binaryupdate";
    public final static String ACCESS_JSON = "access";
    public final static String ACCESS_ARRAY_JSON = "access-array";
    public final static String BALAYAGE_JSON = "balayage";
    public final static String BALAYAGE_ARRAY_JSON = "balayage-array";
    public final static String BALAYAGE_CONTENT_JSON = "balayage-content";
    public final static String BALAYAGE_LOG_JSON = "balayage-log";
    public final static String TABLEEXPORT_JSON = "tableexport";
    public final static String TABLEEXPORT_ARRAY_JSON = "tableexport-array";
    public final static String TABLEEXPORT_CONTENT_JSON = "tableexport-content";
    public final static String SCRUTARIEXPORT_JSON = "scrutariexport";
    public final static String SCRUTARIEXPORT_ARRAY_JSON = "scrutariexport-array";
    public final static String SCRUTARIEXPORT_PATHS_JSON = "scrutariexport-paths";
    public final static String TRANSFORMATION_DESCRIPTION_JSON = "transformation-description";
    public final static String TRANSFORMATION_DESCRIPTIONS_JSON = "transformation-descriptions";
    public final static String TRANSFORMATION_TEMPLATEDESCRIPTION_JSON = "transformation-templatedescription";
    public final static String TRANSFORMATION_TEMPLATECONTENT_JSON = "transformation-templatecontent";
    public final static String SQLEXPORT_JSON = "sqlexport";
    public final static String SQLEXPORT_ARRAY_JSON = "sqlexport-array";
    public final static String SQLEXPORT_PATHS_JSON = "sqlexport-paths";
}
