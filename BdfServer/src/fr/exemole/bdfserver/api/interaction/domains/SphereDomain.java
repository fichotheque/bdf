/* BdfServer_API - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface SphereDomain {

    public final static String REDACTEUR_ADMINFORM_PAGE = "redacteur-adminform";
    public final static String REDACTEUR_ADVANCEDCOMMANDS_PAGE = "redacteur-advancedcommands";
    public final static String REDACTEUR_CREATIONFORM_PAGE = "redacteur-creationform";
    public final static String REDACTEUR_PREFERENCES_PAGE = "redacteur-preferences";
    public final static String REDACTEUR_STATS_PAGE = "redacteur-stats";
    public final static String REDACTEUR_USERFORM_PAGE = "redacteur-userform";
    public final static String SPHERE_PAGE = "sphere";
    public final static String SPHERE_ADVANCEDCOMMANDS_PAGE = "sphere-advancedcommands";
    public final static String SPHERE_CREATIONFORM_PAGE = "sphere-creationform";
    public final static String SPHERE_METADATAFORM_PAGE = "sphere-metadataform";

}
