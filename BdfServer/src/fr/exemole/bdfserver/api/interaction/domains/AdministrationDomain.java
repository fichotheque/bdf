/* BdfServer_API - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.interaction.domains;


/**
 *
 * @author Vincent Calame
 */
public interface AdministrationDomain {

    public final static String BACKUP_PAGE = "backup";
    public final static String BACKUPRESULT_PAGE = "backupresult";
    public final static String DIAGNOSTIC_PAGE = "diagnostic";
    public final static String INDEX_PAGE = "index";
    public final static String INITLOG_PAGE = "initlog";
    public final static String RESOURCES_PAGE = "resources";
    public final static String RESOURCE_UPLOAD_PAGE = "resource-upload";
    public final static String ROLES_PAGE = "roles";
    public final static String SESSIONLIST_PAGE = "sessionlist";
    public final static String RESOURCE_TREE_JSON = "resource-tree";
    public final static String ROLE_JSON = "role";
    public final static String ROLE_ARRAY_JSON = "role-array";
    public final static String STREAMTEXT_JSON = "streamtext";
    public final static String URLINFOS_JSON = "urlinfos";
}
