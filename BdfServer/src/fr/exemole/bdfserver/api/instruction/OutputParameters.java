/* BdfServer_API - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.instruction;

import fr.exemole.bdfserver.api.interaction.Domain;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public interface OutputParameters extends BdfParameters {

    public Domain getDomain();

    public String getOutput();

    public RequestMap getRequestMap();

    public BdfCommandResult getBdfCommandResult();

    public default Object getResultObject(String name) {
        BdfCommandResult commandResult = getBdfCommandResult();
        if (commandResult == null) {
            return null;
        }
        return commandResult.getResultObject(name);
    }

}
