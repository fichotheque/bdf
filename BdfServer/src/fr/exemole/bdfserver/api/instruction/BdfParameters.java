/* BdfServer_API - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.instruction;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.roles.PermissionCheck;
import java.util.Locale;
import java.util.Map;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.addenda.Document;
import net.fichotheque.album.Illustration;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.tools.extraction.builders.ExtractionContextBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 *
 * @author Vincent Calame
 */
public interface BdfParameters {

    public BdfServer getBdfServer();

    public BdfUser getBdfUser();

    public PermissionSummary getPermissionSummary();

    public default Fichotheque getFichotheque() {
        return getBdfServer().getFichotheque();
    }

    public default Lang getWorkingLang() {
        return getBdfUser().getWorkingLang();
    }

    public default Locale getFormatLocale() {
        return getBdfUser().getFormatLocale();
    }

    public default ContentChecker getContentChecker() {
        return getBdfServer().getContentChecker();
    }

    public default MessageLocalisation getMessageLocalisation() {
        return getBdfServer().getL10nManager().getMessageLocalisation(getBdfUser());
    }

    public default boolean isFichothequeAdmin() {
        return getBdfServer().getPermissionManager().isAdmin(getBdfUser().getRedacteur());
    }

    public default void checkFichothequeAdmin() throws PermissionException {
        if (!isFichothequeAdmin()) {
            throw BdfErrors.permission("_ error.permission.admin");
        }
    }

    public default void checkSubsetAdmin(Subset subset) throws PermissionException {
        PermissionCheck.checkSubsetAdmin(getPermissionSummary(), subset);
    }

    public default void checkSubsetAccess(Subset subset) throws PermissionException {
        PermissionCheck.checkSubsetAccess(getPermissionSummary(), subset);
    }

    public default void checkWrite(SubsetItem subsetItem) throws PermissionException {
        PermissionSummary permissionSummary = getPermissionSummary();
        if (subsetItem instanceof FicheMeta) {
            PermissionCheck.checkWrite(permissionSummary, (FicheMeta) subsetItem);
        } else if (subsetItem instanceof Illustration) {
            PermissionCheck.checkWrite(permissionSummary, (Illustration) subsetItem);
        } else if (subsetItem instanceof Document) {
            PermissionCheck.checkWrite(permissionSummary, (Document) subsetItem);
        }
    }

    public default void checkRead(SubsetItem subsetItem) throws PermissionException {
        PermissionSummary permissionSummary = getPermissionSummary();
        if (subsetItem instanceof FicheMeta) {
            PermissionCheck.checkRead(permissionSummary, (FicheMeta) subsetItem);
        }
    }

    public default void checkSphereSupervisor(String sphereName) throws PermissionException {
        PermissionCheck.checkSphereSupervisor(getBdfServer(), getBdfUser(), sphereName);
    }

    public default ExtractionContextBuilder getExtractionContextBuilder() {
        return BdfServerUtils.initExtractionContextBuilder(getBdfServer(), getBdfUser(), getPermissionSummary());
    }

    public default ExtractionContext getDefaultExtractionContext() {
        return getExtractionContextBuilder().toExtractionContext();
    }

    public default Map<String, String> getStoredValues(String storeName) {
        return getBdfServer().getStoredValues(getBdfUser(), storeName);
    }

}
