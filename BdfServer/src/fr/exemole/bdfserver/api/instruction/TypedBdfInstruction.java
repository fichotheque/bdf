/* BdfServer_API - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.instruction;


/**
 *
 * @author Vincent Calame
 */
public interface TypedBdfInstruction extends BdfInstruction {

    public final static short UNKNOWN_TYPE = 0;
    public final static short STREAM_TYPE = 1;
    public final static short JSON_TYPE = 2;
    public final static short PAGE_TYPE = 3;

    public short getOutputType();

}
