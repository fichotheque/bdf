/* BdfServer_API - Copyright (c) 2010-2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.instruction;

import fr.exemole.bdfserver.api.users.BdfUser;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public interface BdfInstruction {

    public final static String HTMLPRODUCER_OPTION = "htmlproducer";
    public final static String LOGINPARAMETERS_OPTION = "loginparameters";
    public final static String SESSIONMESSAGE_OPTION = "sessionmessage";
    public final static String AVAILABLESPHERES_OPTION = "availablespheres";
    public final static String DEFAULTSPHERE_OPTION = "defaultsphere";
    public final static String TITLEPHRASENAME_OPTION = "titlephrasename";

    public short getBdfUserNeed();

    public ResponseHandler runInstruction(BdfUser bdfUser);

    public default Object getLoginOption(String optionName) {
        return null;
    }

}
