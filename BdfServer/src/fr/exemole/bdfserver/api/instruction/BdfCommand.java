/* BdfServer_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.instruction;

import fr.exemole.bdfserver.api.users.BdfUser;
import net.mapeadores.util.logging.CommandMessage;


/**
 *
 * @author Vincent Calame
 */
public interface BdfCommand {

    public boolean needSynchronisation();

    public CommandMessage testCommand(BdfUser bdfUser);

    public BdfCommandResult doCommand(BdfUser bdfUser);

}
