/* BdfServer_API - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.instruction;


/**
 *
 * @author Vincent Calame
 */
public interface BdfInstructionConstants {

    public final static short NONE_BDFUSER = 1;
    public final static short IF_ANY_BDFUSER = 2;
    public final static short MANDATORY_BDFUSER = 3;
    public final static short EXIT_BDFUSER = 4;
    public final static String SUBSET_OBJ = "obj.subset";
    public final static String ACCESSDEF_OBJ = "obj.accessdef";
    public final static String ADDENDA_OBJ = "obj.addenda";
    public final static String ALBUM_OBJ = "obj.album";
    public final static String ALBUMIMAGE_OBJ = "obj.albumimage";
    public final static String ALBUMDIM_OBJ = "obj.albumdim";
    public final static String DOCUMENT_OBJ = "obj.document";
    public final static String ILLUSTRATION_OBJ = "obj.illustration";
    public final static String SPHERE_OBJ = "obj.sphere";
    public final static String THESAURUS_OBJ = "obj.thesaurus";
    public final static String MOTCLE_OBJ = "obj.motcle";
    public final static String CORPUS_OBJ = "obj.corpus";
    public final static String FICHEMETA_OBJ = "obj.fichemeta";
    public final static String PIOCHEPARAMETERS_OBJ = "obj.piocheparameters";
    public final static String STRING_OBJ = "obj.string";
    public final static String OBJECT_OBJ = "obj.object";
    public final static String OBJECTARRAY_OBJ = "obj.object_array";
    public final static String INTARRAY_OBJ = "obj.int_array";
    public final static String SUBSETKEY_OBJ = "obj.subsetkey";
    public final static String REDACTEUR_OBJ = "obj.redacteur";
    public final static String EMAILBUFFER_OBJ = "obj.emailbuffer";
    public final static String SENDREPORT_OBJ = "obj.sendreport";
    public final static String TMPFILE_OBJ = "obj.tmpfile";
    public final static String ROLE_OBJ = "obj.role";
    public final static String SCRUTARIEXPORTDEF_OBJ = "obj.scrutariexportdef";
    public final static String SELECTIONDEF_OBJ = "obj.selectiondef";
    public final static String SQLEXPORTDEF_OBJ = "obj.sqlexportdef";
    public final static String BALAYAGELOG_OBJ = "obj.balayagelog";
    public final static String MODE_OBJ = "obj.mode";
    public final static String BACKUPFILENAME_OBJ = "obj.backupfilename";
    public final static String TABLEEXPORTDESCRIPTION_OBJ = "obj.tableexportdescription";
    public final static String REPONDERATIONLOG_OBJ = "obj.duplicationlog";
    public final static String VERSIONNAME_OBJ = "obj.versionname";
    public final static String DOCUMENTCHANGEINFO_OBJ = "obj.documentchangeinfo";
    public final static String FILELENGTH_OBJ = "obj.filelength";
    public final static String PARSERESULT_OBJ = "obj.parseresult";
    public final static String TYPE_OBJ = "obj.type";
    public final static String TRANSFORMATIONDESCRIPTION_OBJ = "obj.transformationdescription";
    public final static String TEMPLATEDESCRIPTION_OBJ = "obj.templatedescription";
    public final static String TEMPLATECONTENTDESCRIPTION_OBJ = "obj.templatecontentdescription";
    public final static String RELATIVEPATH_OBJ = "obj.relativepath";
    public final static String MESSAGEBYLINELOG_OBJ = "obj.messagebylinelog";
    public final static String BALAYAGEDESCRIPTION_OBJ = "obj.templatedescription";
    public final static String PATHCONFIGURATION_OBJ = "obj.pathconfiguration";
    public final static String FICHEBLOCKS_OBJ = "obj.ficheblocks";
    public final static String DESTINATION_OBJ = "obj.destination";
    public final static String SUBSETMATCH_OBJ = "obj.subsetmatch";
    public final static String URLINFOCOLLECTION_OBJ = "obj.urlinfocollection";
    public final static String FICHOTHEQUEQUERIES_OBJ = "obj.fichothequequeries";
    public final static String METAREPORT_OBJ = "obj.metareport";
}
