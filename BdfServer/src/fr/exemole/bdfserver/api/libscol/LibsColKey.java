/* BdfServer - Copyright (c) 2007-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.libscol;

import java.text.ParseException;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public final class LibsColKey {

    public static final LibsColKey ALLCORPUS = new LibsColKey("allcorpus");
    private String libsColKeyString;
    private SubsetKey subsetKey;

    public LibsColKey() {
    }

    private LibsColKey(String libsColKeyString) {
        this.libsColKeyString = libsColKeyString;
    }

    private LibsColKey(String libsColKeyString, SubsetKey subsetKey) {
        this.libsColKeyString = libsColKeyString;
        this.subsetKey = subsetKey;
    }

    public static LibsColKey parse(String s) throws ParseException {
        if (s.equals("allcorpus")) {
            return ALLCORPUS;
        }
        SubsetKey subsetKey = SubsetKey.parse(s);
        return new LibsColKey(s, subsetKey);
    }

    public boolean isFromSubset() {
        return (subsetKey != null);
    }

    public SubsetKey getSubsetKey() {
        return subsetKey;
    }

    @Override
    public String toString() {
        return libsColKeyString;
    }

    @Override
    public int hashCode() {
        return libsColKeyString.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof LibsColKey)) {
            return false;
        }
        LibsColKey other = (LibsColKey) obj;
        return (other.libsColKeyString).equals(this.libsColKeyString);
    }

}
