/* BdfServer_API - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.exportation.transformation;


/**
 *
 * @author Vincent Calame
 */
public interface TransformationConstants {

    public final static String BDF_BALAYAGEROOT_PARAMETER = "BDF_BALAYAGEROOT";
    public final static String BDF_FICHOTHEQUEPATH_PARAMETER = "BDF_FICHOTHEQUEPATH";
    public final static String BDF_FILEBASENAME_PARAMETER = "BDF_FILEBASENAME";
    public final static String BDF_FILEEXTENSION_PARAMETER = "BDF_FILEEXTENSION";
    public final static String BDF_FILENAME_PARAMETER = "BDF_FILENAME";
    public final static String BDF_LANG_PARAMETER = "BDF_LANG";
    public final static String BDF_PATH_PARAMETER = "BDF_PATH";
    public final static String BDF_REVERSEPATH_PARAMETER = "BDF_REVERSEPATH";
    public final static String BDF_VERSION_PARAMETER = "BDF_VERSION";
    public final static String EXTERNALTARGET_PARAMETER = "EXTERNALTARGET";
    public final static String INCLUDESCRIPTS_PARAMETER = "INCLUDESCRIPTS";
    public final static String SHOWEMPTY_PARAMETER = "SHOWEMPTY";
    public final static String STARTLEVEL_PARAMETER = "STARTLEVEL";
    public final static String TEXT_COLON_PARAMETER = "TEXT_COLON";
    public final static String TEXT_FIELDBULLET_PARAMETER = "TEXT_FIELDBULLET";
    public final static String TEXT_SECTIONEND_PARAMETER = "TEXT_SECTIONEND";
    public final static String WITH_FICHENUMBER_PARAMETER = "WITH_FICHENUMBER";
    public final static String WITH_RESOURCELOGO_PARAMETER = "WITH_RESOURCELOGO";
    public final static String USER_SPHERE_PARAMETER = "USER_SPHERE";
    public final static String USER_LOGIN_PARAMETER = "USER_LOGIN";
    public final static String WORKINGLANG_PARAMETER = "WORKINGLANG";
    public final static String ERROR_CATEGORY = "error";
    public final static String WARNING_CATEGORY = "warning";
}
