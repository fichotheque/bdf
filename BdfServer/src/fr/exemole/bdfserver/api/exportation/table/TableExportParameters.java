/* BdfServer_API - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.exportation.table;


/**
 *
 * @author Vincent Calame
 */
public interface TableExportParameters {

    /**
     * Peut être nul.
     */
    public String getTableExportName();

    public FicheTableParameters getDefaulFicheTableParameters();

    public String getHeaderType();

}
