/* BdfServer_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.exportation.table;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface FicheTableParameters {

    public final static String WITH_SECTION = "section";
    public final static String WITH_CHRONO = "chrono";
    public final static String WITH_ADDENDAINCLUDE = "addendainclude";
    public final static String WITH_ALBUMINCLUDE = "albuminclude";
    public final static String WITH_CORPUSINCLUDE = "corpusinclude";
    public final static String WITH_THESAURUSINCLUDE = "thesaurusinclude";
    public final static String STANDARD_PATTERNMODE = "standard";
    public final static String FORMSYNTAX_PATTERNMODE = "formsyntax";
    public final static String CODE_PATTERNMODE = "code";
    public final static String LABEL_PATTERNMODE = "label";

    public boolean isWith(String key);

    public String getPatternMode();

    public List<String> getWithList();

    public static String checkPatternMode(String patternMode) {
        switch (patternMode) {
            case STANDARD_PATTERNMODE:
                return STANDARD_PATTERNMODE;
            case FORMSYNTAX_PATTERNMODE:
                return FORMSYNTAX_PATTERNMODE;
            case CODE_PATTERNMODE:
                return CODE_PATTERNMODE;
            case LABEL_PATTERNMODE:
                return LABEL_PATTERNMODE;
            default:
                throw new IllegalArgumentException("wrong value :" + patternMode);
        }
    }

}
