/* BdfServer - Copyright (c) 2009-2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.exportation.sql;

import fr.exemole.bdfserver.api.BdfServer;
import net.fichotheque.exportation.sql.SqlExport;


/**
 *
 * @author Vincent Calame
 */
public interface BdfServerSqlExport extends SqlExport {

    public void setBdfServer(BdfServer bdfServer);

}
