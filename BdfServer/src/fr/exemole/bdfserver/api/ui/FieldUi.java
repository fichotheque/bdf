/* BdfServer_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ui;

import fr.exemole.bdfserver.api.BdfServerConstants;
import net.fichotheque.corpus.metadata.FieldKey;


/**
 *
 * @author Vincent Calame
 */
public interface FieldUi extends UiComponent {

    public FieldKey getFieldKey();

    public default String getFieldString() {
        return getFieldKey().getKeyString();
    }

    @Override
    public default boolean isModifiableStatus() {
        return isModifiableStatus(getFieldKey());
    }

    @Override
    public default boolean isRelevantOption(String optionName) {
        switch (optionName) {
            case BdfServerConstants.DEFAULTVALUE_OPTION: {
                switch (getFieldString()) {
                    case FieldKey.SPECIAL_LANG:
                    case FieldKey.SPECIAL_REDACTEURS:
                    case FieldKey.SPECIAL_ID:
                        return false;
                    default:
                        return true;
                }
            }
            case BdfServerConstants.INPUTROWS_OPTION: {
                switch (getFieldString()) {
                    case FieldKey.SPECIAL_ID:
                        return false;
                    default:
                        return true;
                }
            }
            case BdfServerConstants.INPUTWIDTH_OPTION: {
                switch (getFieldString()) {
                    case FieldKey.SPECIAL_ID:
                    case FieldKey.SPECIAL_LANG:
                        return false;
                    default:
                        return true;
                }
            }

        }
        return false;
    }

    public static boolean isModifiableStatus(FieldKey fieldKey) {
        return !fieldKey.isMandatoryField();
    }

    public static boolean hasValueUiOption(FieldKey fieldKey) {
        switch (fieldKey.getKeyString()) {
            case FieldKey.SPECIAL_LANG:
            case FieldKey.SPECIAL_REDACTEURS:
            case FieldKey.SPECIAL_ID:
                return false;
            default:
                return true;
        }
    }

    public static boolean hasInputUiOption(FieldKey fieldKey) {
        switch (fieldKey.getKeyString()) {
            case FieldKey.SPECIAL_ID:
                return false;
            default:
                return true;
        }
    }

}
