/* BdfServer - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ui;

import fr.exemole.bdfserver.api.BdfServerConstants;
import net.fichotheque.SubsetKey;
import net.fichotheque.include.ExtendedIncludeKey;


/**
 *
 * @author Vincent Calame
 */
public interface SubsetIncludeUi extends IncludeUi {

    public ExtendedIncludeKey getExtendedIncludeKey();

    @Override
    public default boolean isModifiableStatus() {
        return true;
    }

    public default SubsetKey getSubsetKey() {
        return getExtendedIncludeKey().getSubsetKey();
    }

    public default boolean isFicheTable() {
        if (getCategory() != SubsetKey.CATEGORY_CORPUS) {
            return false;
        }
        String variant = getOptionValue(BdfServerConstants.INCLUDEVARIANT_OPTION);
        return variant.equals("table");
    }

    public default short getCategory() {
        return getSubsetKey().getCategory();
    }

    public default boolean matchInputType(String inputType) {
        String value = getOptionValue(BdfServerConstants.INPUTTYPE_OPTION);
        return value.equals(inputType);
    }

    public static boolean hasInputUiOption(SubsetKey subsetKey) {
        return subsetKey.isThesaurusSubset();
    }

    @Override
    public default boolean isRelevantOption(String optionName) {
        switch (optionName) {
            case BdfServerConstants.DEFAULTVALUE_OPTION:
            case BdfServerConstants.INPUTROWS_OPTION:
            case BdfServerConstants.INPUTWIDTH_OPTION:
            case BdfServerConstants.INPUTTYPE_OPTION:
                return getSubsetKey().isThesaurusSubset();
            case BdfServerConstants.INCLUDEVARIANT_OPTION:
                return getSubsetKey().isCorpusSubset();
        }
        return false;
    }

}
