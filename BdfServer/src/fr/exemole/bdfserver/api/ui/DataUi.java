/* BdfServer - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ui;

import fr.exemole.bdfserver.api.BdfServerConstants;
import java.text.ParseException;
import java.util.Collections;
import java.util.Set;
import net.fichotheque.externalsource.ExternalSourceDef;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface DataUi extends UiComponent {

    public String getDataName();

    public Labels getLabels();

    public ExternalSourceDef getExternalSourceDef();

    @Override
    public default String getStatus() {
        return BdfServerConstants.DEFAULT_STATUS;
    }

    @Override
    public default Set<String> getOptionNameSet() {
        return Collections.emptySet();
    }

    @Override
    public default Object getOption(String optionName) {
        return null;
    }

    @Override
    public default boolean isModifiableStatus() {
        return false;
    }

    @Override
    public default boolean isRelevantOption(String optionName) {
        return false;
    }

    public static String toComponentName(String dataName) {
        return "data_" + dataName;
    }

    public static void checkDataName(String dataName) throws ParseException {
        StringUtils.checkTechnicalName(dataName, false);
    }

}
