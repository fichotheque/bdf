/* BdfServer_API - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ui;

import fr.exemole.bdfserver.api.BdfServerConstants;
import java.text.ParseException;
import java.util.Collections;
import java.util.Set;
import net.mapeadores.util.html.TrustedHtml;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface CommentUi extends UiComponent {

    public final static int FORM_BITVALUE = 1;
    public final static int TEMPLATE_BITVALUE = 2;
    public final static int ALL_MASK = 3;

    public int getLocation();

    public String getCommentName();

    public Langs getLangs();

    public TrustedHtml getHtmlByLang(Lang lang);

    @Override
    public default String getStatus() {
        return BdfServerConstants.DEFAULT_STATUS;
    }

    @Override
    public default Set<String> getOptionNameSet() {
        return Collections.emptySet();
    }

    @Override
    public default Object getOption(String optionName) {
        return null;
    }

    @Override
    public default boolean isModifiableStatus() {
        return false;
    }

    @Override
    public default boolean isRelevantOption(String optionName) {
        return false;
    }

    public default boolean isClone() {
        return false;
    }

    public default String getCloneName() {
        return getName();
    }

    public default boolean isForm() {
        return ((getLocation() & CommentUi.FORM_BITVALUE) != 0);
    }

    public default boolean isTemplate() {
        return ((getLocation() & CommentUi.TEMPLATE_BITVALUE) != 0);
    }

    public static String toComponentName(String commentName) {
        return "comment_" + commentName;
    }

    public static void checkCommentName(String commentName) throws ParseException {
        StringUtils.checkTechnicalName(commentName, false);
    }

}
