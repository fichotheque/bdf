/* BdfServer - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ui;

import fr.exemole.bdfserver.api.BdfServerConstants;
import java.util.Set;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.text.MultiStringable;


/**
 *
 * @author Vincent Calame
 */
public interface UiComponent {

    public String getName();

    public String getStatus();

    public Set<String> getOptionNameSet();

    /**
     * Instance de String ou MultiStringable
     */
    public Object getOption(String optionName);

    public Attributes getAttributes();

    public default boolean isMandatory() {
        return getStatus().equals(BdfServerConstants.MANDATORY_STATUS);
    }

    public default boolean isObsolete() {
        return getStatus().equals(BdfServerConstants.OBSOLETE_STATUS);
    }

    public boolean isModifiableStatus();

    public boolean isRelevantOption(String optionName);

    public default String getOptionValue(String optionName) {
        return getOptionValue(optionName, "");
    }

    public default String getOptionValue(String optionName, String defaultValue) {
        Object obj = getOption(optionName);
        if (obj == null) {
            return defaultValue;
        }
        if (obj instanceof MultiStringable) {
            return ((MultiStringable) obj).toString(", ");
        } else {
            return (String) obj;
        }
    }

}
