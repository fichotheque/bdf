/* BdfServer - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ui;

import net.fichotheque.FichothequeConstants;


/**
 *
 * @author Vincent Calame
 */
public interface SpecialIncludeUi extends IncludeUi {

    @Override
    public default boolean isModifiableStatus() {
        if (getName().equals(FichothequeConstants.LIAGE_NAME)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public default boolean isRelevantOption(String optionName) {
        return false;
    }

    public static String checkSpecialIncludeName(String name) {
        switch (name) {
            case FichothequeConstants.LIAGE_NAME:
                return FichothequeConstants.LIAGE_NAME;
            case FichothequeConstants.PARENTAGE_NAME:
            case "rattachement":
                return FichothequeConstants.PARENTAGE_NAME;
            default:
                throw new IllegalArgumentException("Unknown special include name: " + name);
        }
    }


}
