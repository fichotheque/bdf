/* BdfServer - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.ui;

import java.util.List;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.extraction.DataKey;


/**
 *
 * @author Vincent Calame
 */
public interface UiComponents {

    public List<UiComponent> getUiComponentList();

    public UiComponent getUiComponent(String name);

    public default boolean contains(String name) {
        return (getUiComponent(name) != null);
    }

    public default boolean contains(FieldKey fieldKey) {
        return (getUiComponent(fieldKey) != null);
    }

    public default UiComponent getUiComponent(Object obj) {
        if (obj instanceof DataKey) {
            return getUiComponent(DataUi.toComponentName(((DataKey) obj).getName()));
        } else {
            return getUiComponent(obj.toString());
        }
    }

}
