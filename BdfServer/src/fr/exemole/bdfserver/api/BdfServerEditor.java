/* BdfServer_API - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import fr.exemole.bdfserver.api.groups.GroupDef;
import fr.exemole.bdfserver.api.policies.UserAllow;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.api.roles.RoleEditor;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import fr.exemole.bdfserver.api.users.BdfUserPrefsEditor;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public interface BdfServerEditor {

    public BdfServer getBdfServer();

    public RoleEditor getRoleEditor();

    public BdfUserPrefsEditor getBdfUserPrefsEditor(BdfUserPrefs bdfUserPrefs);

    public boolean removeAttribute(Object attributesHolder, AttributeKey attributeKey);

    public boolean putAttribute(Object attributesHolder, Attribute attribute);

    public boolean putLabel(Object labelHolder, Label label);

    public boolean removeLabel(Object labelHolder, Lang lang);

    public void removeGroupDef(GroupDef groupDef);

    public GroupDef createGroupDef(String name);

    public void setSubsetTree(short subsetCategory, SubsetTree subsetTree);

    public void setLangConfiguration(LangConfiguration langConfiguration);

    public void setActiveExtensionList(List<String> activeExtensionList);

    public void setSubsetPolicy(SubsetKey subsetKey, Object policyObject);

    public boolean setUserAllow(UserAllow userAllow);

    public boolean setAdmin(String redacteurGlobalId, boolean isAdmin);

    public boolean link(Redacteur redacteur, Role role);

    public boolean unlink(String redacteurGlobalId, Role role);

    public boolean removeComponentUi(UiComponents uiComponents, String name);

    public boolean putComponentUi(UiComponents uiComponents, UiComponent componentUi);

    public boolean setPositionArray(UiComponents uiComponents, String[] nameArray);

}
