/* BdfServer_API - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.roles;

import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;


/**
 *
 * @author Vincent Calame
 */
public interface SatelliteOpportunities {

    public final static short CREATE = 1;
    public final static short WRITE = 2;
    public final static short READ = 3;

    public List<Entry> getEntryList();

    public boolean onlyReadAllowed();


    public interface Entry {

        public Corpus getSatelliteCorpus();

        public short getAvailableActionCategory();

        public FicheMeta getFicheMeta();

    }

}
