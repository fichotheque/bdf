/* BdfServer_API - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.roles;


/**
 *
 * @author Vincent Calame
 */
public interface Permission {

    public final static short NONE_LEVEL = 0;
    public final static short STANDARD_LEVEL = 1;
    public final static short CUSTOM_LEVEL = 2;
    public final static short ADMIN_LEVEL = 3;

    public short getLevel();

    public CustomPermission getCustomPermission();


    public interface CustomPermission {

    }

}
