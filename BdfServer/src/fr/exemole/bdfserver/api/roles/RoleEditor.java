/* BdfServer_API - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.roles;

import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public interface RoleEditor {

    public Role createRole(String name) throws ParseException, ExistingIdException;

    public void removeRole(Role role);

    public void putSubsetPermission(Role role, SubsetKey subsetKey, Permission permission);

    public void removeSubsetPermission(Role role, SubsetKey subsetKey);

}
