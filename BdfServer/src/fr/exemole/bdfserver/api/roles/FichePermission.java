/* BdfServer_API - Copyright (c) 2008-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.roles;


/**
 *
 * @author Vincent Calame
 */
public interface FichePermission extends Permission.CustomPermission {

    public final static short NONE = 0;
    public final static short OWN = 1;
    public final static short SPHERE = 2;
    public final static short ALL = 3;

    public boolean create();

    public short read();

    public short write();

}
