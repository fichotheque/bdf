/* BdfServer_API - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.roles;

import java.text.ParseException;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface RoleDef {

    public static String DEFAULT_ROLE = "_default";

    public String getName();

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public List<SubsetEntry> getSubsetEntryList();


    public static interface SubsetEntry {

        public SubsetKey getSubsetKey();

        public Permission getPermission();

    }

    public static void checkRoleName(String roleName) throws ParseException {
        if (!roleName.equals(DEFAULT_ROLE)) {
            StringUtils.checkTechnicalName(roleName, true);
        }
    }

}
