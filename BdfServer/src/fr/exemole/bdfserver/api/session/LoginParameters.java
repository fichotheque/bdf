/* BdfServer_API - Copyright (c) 2012-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.session;

import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public interface LoginParameters {

    public String getFormAction();

    public String getReversePath();

    public LoginException getLoginException();

    public RequestMap getRequestMap();

    public CommandMessage getCommandMessage();

    public default List<SubsetKey> getLoginSphrereKeyList() {
        return null;
    }

    public default SubsetKey getDefaultSphereKey() {
        return null;
    }

    public default String getTitlePhraseName() {
        return FichothequeConstants.LONG_PHRASE;
    }

}
