/* BdfServer_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.session;


/**
 * Exception renvoyée en cas d'échec de l'authentification.
 *
 * @author Vincent Calame
 */
public class LoginException extends Exception {


    /**
     * Erreur sur le mot de passe
     */
    public static String PASSWORD_ERROR = "_ error.wrong.login.password";
    /**
     * Erreur sur l'identifiant
     */
    public static String LOGIN_ERROR = "_ error.unknown.login.login";
    /**
     * Erreur sur la sphère
     */
    public static String SPHERE_ERROR = "_ error.wrong.login.sphere";
    /**
     * Rédacteur inactif
     */
    public static String INACTIVE_ERROR = "_ error.unsupported.login.inactive";
    /**
     * Erreur sur un paramètre manquant
     */
    public static String UNDEFINED_ERROR = "_ error.empty.login.undefined";
    /**
     * Erreur sur l'expiration de la session en cours
     */
    public static String EXIT_ERROR = "_ error.unsupported.login.exit";
    private final String errorMessageKey;

    public LoginException(String errorMessageKey) {
        super(errorMessageKey);
        this.errorMessageKey = errorMessageKey;
    }

    public String getErrorMessageKey() {
        return errorMessageKey;
    }

}
