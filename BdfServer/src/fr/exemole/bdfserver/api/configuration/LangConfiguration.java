/* BdfServer_API - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.configuration;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;


/**
 *
 * @author Vincent Calame
 */
public interface LangConfiguration {

    /**
     * Doit comprendre au moins un élément.
     */
    public Langs getWorkingLangs();

    public Langs getSupplementaryLangs();

    public boolean isAllLanguages();

    public boolean isWithNonlatin();

    public boolean isWithoutSurnameFirst();

    public default Lang getDefaultWorkingLang() {
        return getWorkingLangs().get(0);
    }

}
