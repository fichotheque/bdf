/* BdfServer_API - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.configuration;

import java.io.File;
import java.util.Set;


/**
 *
 * @author Vincent Calame
 */
public interface PathConfiguration {

    public final static String PUBLIC_TARGET = "_public";

    public default File getPublicDirectory() {
        return getTargetDirectory(PUBLIC_TARGET);
    }

    public String getPublicUrl(String relativePath);

    public File getTargetDirectory(String name);

    public Set<String> getTargetNameSet();

}
