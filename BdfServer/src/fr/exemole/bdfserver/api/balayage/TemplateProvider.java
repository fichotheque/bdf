/* BdfServer_API - Copyright (c) 2009-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.balayage;


/**
 *
 * @author Vincent Calame
 */
public interface TemplateProvider {

    public void setBalayageRoot(String balayageRoot);

    public void setCurrentFileName(String fileName);

    public void setCurrentPath(String path);

    public void setCurrentLang(String lang);

    public BalayageTemplate getBalayageTemplate(Object extractionObject);

}
