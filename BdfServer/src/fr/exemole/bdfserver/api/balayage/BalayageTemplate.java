/* BdfServer_API - Copyright (c) 2013-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.balayage;

import java.io.OutputStream;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageTemplate {

    public String getMimeType();

    public String getCharset();

    public void transform(String extractionString, OutputStream outputStream);

}
