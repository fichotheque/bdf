/* BdfServer_API - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.menu;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public interface MenuNode {

    @Nullable
    public Message getTitleMessage();

    public String getTitle();

}
