/* BdfServer_API - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.menu;

import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface ActionEntry {

    public String getCssName();

    /**
     * Instance de Message ou String, non nul
     *
     * @return
     */
    public Object getTitle();

    /**
     * Si nul, utilise la zone principale par défaut
     */
    @Nullable
    public String getTarget();

    /**
     * Ne doit pas être nul
     */
    public String getUrl();

    /**
     * Peut être nul.
     */
    @Nullable
    public String getPageName();

}
