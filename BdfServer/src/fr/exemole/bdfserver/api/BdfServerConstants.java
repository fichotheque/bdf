/* BdfServer_API - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api;


/**
 *
 * @author Vincent Calame
 */
public interface BdfServerConstants {

    public final static String SESSIONOBSERVER_CONTEXTOBJECT = "sessionObserver";
    public final static String SMTPMANAGER_CONTEXTOBJECT = "smtpManager";
    public final static String FICHOTHEQUESHARING_CONTEXTOBJECT = "fichothequeSharing";
    public final static String BDFSERVER_CONTEXTOBJECT = "bdfServer";
    public final static String MIMETYPERESOLVER_CONTEXTOBJECT = "mimeTypeResolver";
    public final static String SPHERESUPERVISOR_CONTEXTOBJECT = "sphereSupervisor";
    public final static String VAR_STORAGE = "var";
    public final static String ETC_STORAGE = "etc";
    public final static String DIST_STORAGE = "dist";
    public final static String DEFAULT_STATUS = "default";
    public final static String MANDATORY_STATUS = "mandatory";
    public final static String OPTIONAL_STATUS = "optional";
    public final static String OBSOLETE_STATUS = "obsolete";
    public final static String DEFAULTVALUE_OPTION = "defaultValue";
    public final static String INCLUDEVARIANT_OPTION = "includeVariant";
    public final static String INPUTROWS_OPTION = "inputRows";
    public final static String INPUTWIDTH_OPTION = "inputWidth";
    public final static String INPUTTYPE_OPTION = "inputType";
    public final static String WIDTH_SMALL = "small";
    public final static String WIDTH_MEDIUM = "medium";
    public final static String WIDTH_LARGE = "large";
    public final static String INPUT_TEXT = "text";
    public final static String INPUT_SELECT = "select";
    public final static String INPUT_CHECK = "check";
    public final static String INPUT_RADIO = "radio";
    public final static String INPUT_NOTEDITABLE = "noteditable";
    public final static String INPUT_HIDDEN = "hidden";
    public final static String INPUT_FICHESTYLE = "fichestyle";

    public static String checkValidStatus(String status) {
        if ((status == null) || (status.isEmpty())) {
            return DEFAULT_STATUS;
        }
        switch (status) {
            case DEFAULT_STATUS:
                return DEFAULT_STATUS;
            case MANDATORY_STATUS:
                return MANDATORY_STATUS;
            case OPTIONAL_STATUS:
                return OPTIONAL_STATUS;
            case OBSOLETE_STATUS:
                return OBSOLETE_STATUS;
            default:
                throw new IllegalArgumentException("Unknown status: " + status);
        }
    }

    public static String checkInputType(String inputType) {
        switch (inputType) {
            case INPUT_TEXT:
            case "suggestion":
                return INPUT_TEXT;
            case INPUT_SELECT:
                return INPUT_SELECT;
            case INPUT_CHECK:
                return INPUT_CHECK;
            case INPUT_RADIO:
                return INPUT_RADIO;
            case INPUT_NOTEDITABLE:
                return INPUT_NOTEDITABLE;
            case INPUT_HIDDEN:
                return INPUT_HIDDEN;
            case INPUT_FICHESTYLE:
                return INPUT_FICHESTYLE;
            default:
                throw new IllegalArgumentException("wrong value :" + inputType);
        }
    }

    public static String checkWidth(String widthType) {
        switch (widthType) {
            case WIDTH_SMALL:
                return WIDTH_SMALL;
            case WIDTH_MEDIUM:
                return WIDTH_MEDIUM;
            case WIDTH_LARGE:
                return WIDTH_LARGE;
            default: {
                try {
                    int size = Integer.parseInt(widthType);
                    if (size < 10) {
                        return WIDTH_SMALL;
                    }
                    if (size < 50) {
                        return WIDTH_MEDIUM;
                    }
                    return WIDTH_LARGE;
                } catch (NumberFormatException nfe) {
                    throw new IllegalArgumentException("wrong value : " + widthType);
                }
            }
        }

    }

}
