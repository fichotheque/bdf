/* BdfServer_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class CellSpace {

    public final static CheckedNameSpace CELL_NAMESPACE = CheckedNameSpace.build("cell");
    public final static AttributeKey ORDER_KEY = AttributeKey.build(CELL_NAMESPACE, "order");
    public final static AttributeKey FORMAT_KEY = AttributeKey.build(CELL_NAMESPACE, "format");

    private CellSpace() {

    }

}
