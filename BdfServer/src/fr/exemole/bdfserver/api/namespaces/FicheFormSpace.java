/* BdfServer_API - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class FicheFormSpace {

    public final static CheckedNameSpace FICHEFORM_NAMESPACE = CheckedNameSpace.build("ficheform");
    public final static AttributeKey CLASSES_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "classes");
    public final static AttributeKey SATELLITES_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "satellites");
    public final static AttributeKey ENTRYTYPE_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "entrytype");
    public final static AttributeKey MAXLENGTH_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "maxlength");
    public final static AttributeKey LANG_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "lang");
    public final static AttributeKey SPELLCHECK_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "spellcheck");
    public final static AttributeKey GROUPS_ATTRIBUTEKEY = AttributeKey.build(FICHEFORM_NAMESPACE, "groups");
    public final static AttributeKey CONDITION_ATTRIBUTEKEY = AttributeKey.build(FICHEFORM_NAMESPACE, "condition");
    public final static AttributeKey GEOSTART_LAT_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "geostart:lat");
    public final static AttributeKey GEOSTART_LON_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "geostart:lon");
    public final static AttributeKey GEOSTART_ZOOM_KEY = AttributeKey.build(FICHEFORM_NAMESPACE, "geostart:zoom");

    private FicheFormSpace() {

    }

    public final static AttributeKey getPlaceholderAttributeKey(Lang lang) {
        return AttributeKey.build(FICHEFORM_NAMESPACE, "placeholder:" + lang.toString());
    }

    public final static AttributeKey getTooltipAttributeKey(Lang lang) {
        return AttributeKey.build(FICHEFORM_NAMESPACE, "tooltip:" + lang.toString());
    }

}
