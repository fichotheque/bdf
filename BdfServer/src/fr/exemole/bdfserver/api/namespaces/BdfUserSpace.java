/* BdfServer_API - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.namespaces;

import net.fichotheque.Subset;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class BdfUserSpace {

    public final static CheckedNameSpace BDFUSER_NAMESPACE = CheckedNameSpace.build("bdfuser");
    public final static CheckedNameSpace DEFAULT_NAMESPACE = CheckedNameSpace.build("bdfuser.default");
    public final static CheckedNameSpace TABLE_NAMESPACE = CheckedNameSpace.build("bdfuser.table");
    public final static CheckedNameSpace THESAURUS_FILTER_NAMESPACE = CheckedNameSpace.build("bdfuser.thesaurus.filter");
    public final static CheckedNameSpace TEMPLATE_SIMPLE_NAMESPACE = CheckedNameSpace.build("bdfuser.template.simple");
    public final static CheckedNameSpace TEMPLATE_STREAM_NAMESPACE = CheckedNameSpace.build("bdfuser.template.stream");
    public final static CheckedNameSpace SPELLCHECK_NAMESPACE = CheckedNameSpace.build("bdfuser.spellcheck");
    public final static CheckedNameSpace SYNTAX_NAMESPACE = CheckedNameSpace.build("bdfuser.syntax");
    public final static AttributeKey TABLEEXPORT_WITH_KEY = AttributeKey.build(BdfUserSpace.DEFAULT_NAMESPACE, "tableexport.with");
    public final static AttributeKey TABLEEXPORT_HEADERTYPE_KEY = AttributeKey.build(BdfUserSpace.DEFAULT_NAMESPACE, "tableexport.headertype");
    public final static AttributeKey TABLEEXPORT_PATTERNMODE_KEY = AttributeKey.build(BdfUserSpace.DEFAULT_NAMESPACE, "tableexport.patternmode");
    public final static AttributeKey TABLEEXPORT_STATUSSHEET_KEY = AttributeKey.build(BdfUserSpace.DEFAULT_NAMESPACE, "tableexport.statussheet");
    public final static AttributeKey FICHEFORM_KEY = AttributeKey.build(BdfUserSpace.BDFUSER_NAMESPACE, "ficheform");
    public final static AttributeKey INCLUDESCRIPTS_KEY = AttributeKey.build(BdfUserSpace.BDFUSER_NAMESPACE, "includescripts");
    public final static AttributeKey NOTAB_KEY = AttributeKey.build(BdfUserSpace.BDFUSER_NAMESPACE, "notab");
    public final static AttributeKey HIDEEMPTY_KEY = AttributeKey.build(BdfUserSpace.BDFUSER_NAMESPACE, "hideempty");
    public final static AttributeKey SYNTAXACTIVE_KEY = AttributeKey.build(BdfUserSpace.BDFUSER_NAMESPACE, "syntaxactive");
    public final static AttributeKey ADMIN_KEY = AttributeKey.build(BdfUserSpace.BDFUSER_NAMESPACE, "admin");

    private BdfUserSpace() {
    }

    public static AttributeKey toSimpleTemplateAttributeKey(TransformationKey transformationKey) {
        return AttributeKey.build(TEMPLATE_SIMPLE_NAMESPACE, transformationKey.getKeyString());
    }

    public static AttributeKey toStreamTemplateAttributeKey(TransformationKey transformationKey, String extension) {
        if (extension == null) {
            throw new IllegalArgumentException("extension is null");
        }
        return AttributeKey.build(TEMPLATE_STREAM_NAMESPACE, transformationKey.getKeyString() + '.' + extension);
    }

    public static AttributeKey toTableAttributeKey(Subset subset) {
        return AttributeKey.build(TABLE_NAMESPACE, subset.getSubsetKeyString());
    }

    public static AttributeKey toFilterParametersKey(Thesaurus thesaurus) {
        return AttributeKey.build(THESAURUS_FILTER_NAMESPACE, thesaurus.getSubsetKeyString());
    }

    public static AttributeKey toSpellcheckAttributeKey(Subset subset) {
        return AttributeKey.build(SPELLCHECK_NAMESPACE, subset.getSubsetKeyString());
    }

    public static AttributeKey toSyntaxAttributeKey(Subset subset) {
        return AttributeKey.build(SYNTAX_NAMESPACE, subset.getSubsetKeyString());
    }

}
