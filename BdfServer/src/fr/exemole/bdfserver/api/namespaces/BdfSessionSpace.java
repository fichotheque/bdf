/* BdfServer_API - Copyright (c) 2019-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.namespaces;

import net.fichotheque.Fichotheque;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class BdfSessionSpace {

    public final static CheckedNameSpace BDFSESSION_NAMESPACE = CheckedNameSpace.build("bdfsession");
    public final static AttributeKey LOGIN_KEY = AttributeKey.build(BDFSESSION_NAMESPACE, "login");
    public final static AttributeKey SPHEREROW_KEY = AttributeKey.build(BDFSESSION_NAMESPACE, "sphererow");

    private BdfSessionSpace() {

    }

    public static boolean isHiddenAtLogin(Sphere sphere) {
        Attribute attribute = sphere.getSphereMetadata().getAttributes().getAttribute(BdfSessionSpace.LOGIN_KEY);
        if (attribute == null) {
            return false;
        } else {
            for (String value : attribute) {
                if (value.equals("hidden")) {
                    return true;
                }
            }
            return false;
        }
    }

    public static boolean showUniqueRow(Fichotheque fichotheque) {
        Attribute attribute = fichotheque.getFichothequeMetadata().getAttributes().getAttribute(BdfSessionSpace.SPHEREROW_KEY);
        if (attribute == null) {
            return false;
        } else {
            for (String value : attribute) {
                if (value.equals("show_unique")) {
                    return true;
                }
            }
            return false;
        }
    }

}
