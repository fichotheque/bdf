/* BdfServer_API - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.namespaces;

import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class UiSpace {

    public final static CheckedNameSpace UI_NAMESPACE = CheckedNameSpace.build("ui");
    public final static AttributeKey HIDE_KEY = AttributeKey.build(UI_NAMESPACE, "hide");
    public final static AttributeKey FONTAWESOME_KEY = AttributeKey.build(UI_NAMESPACE, "fontawesome");

    private UiSpace() {

    }

    public static boolean testHide(Attributes attributes, String hideTest) {
        Attribute attribute = attributes.getAttribute(UiSpace.HIDE_KEY);
        if (attribute == null) {
            return false;
        }
        for (String value : attribute) {
            if (value.equals(hideTest)) {
                return true;
            }
        }
        return false;
    }

}
