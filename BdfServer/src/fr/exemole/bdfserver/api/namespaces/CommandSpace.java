/* BdfServer_API - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public class CommandSpace {

    public final static CheckedNameSpace COMMAND_NAMESPACE = CheckedNameSpace.build("command");
    public final static AttributeKey DUPLICATION_EXCLUDE_KEY = AttributeKey.build(COMMAND_NAMESPACE, "duplication:exclude");

    private CommandSpace() {

    }

}
