/* BdfServer_API - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.groups;

import java.text.ParseException;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface GroupDef {

    public String getName();

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public default String getTitle(Lang preferredLang) {
        return getTitleLabels().seekLabelString(preferredLang, getName());
    }

    public static void checkGroupName(String groupName) throws ParseException {
        StringUtils.checkTechnicalName(groupName, true);
    }

}
