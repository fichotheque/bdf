/* BdfServer - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.users;


/**
 *
 * @author Vincent Calame
 */
public interface BdfUserConstants {

    public final static String FORM_TYPOOPTIONS = "bdf.form.typooptions";
    public final static String HTML_WITHJAVASCRIPT = "bdf.html.withjavascript";
    public final static String SELECTION_DEFNAME = "bdf.selection.defname";
    public final static String SELECTION_TYPE = "bdf.selection.type";
    public final static String SESSION_CREATIONTIME = "bdf.session.creationtime";
    public final static String SESSION_LASTACCESSEDTIME = "bdf.session.lastaccessedtime";
    public final static String SESSION_ROOTURL = "bdf.session.rooturl";
    public final static String THESAURUSMERGELAST_SUBSETKEY_OBJ = "bdf.thesaurus.merge_last";
    public final static String THESAURUSMOVELAST_SUBSETKEY_OBJ = "bdf.thesaurus.move_last";

}
