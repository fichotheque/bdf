/* BdfServer - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.users;

import fr.exemole.bdfserver.api.exportation.table.FicheTableParameters;
import fr.exemole.bdfserver.api.exportation.table.TableExportParameters;
import fr.exemole.bdfserver.api.namespaces.BdfUserSpace;
import fr.exemole.bdfserver.tools.exportation.table.FicheTableParametersBuilder;
import fr.exemole.bdfserver.tools.exportation.table.TableExportParametersBuilder;
import java.util.Locale;
import net.fichotheque.exportation.table.TableExportConstants;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.selection.FicheQuery;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;


/**
 *
 * @author Vincent Calame
 */
public interface BdfUserPrefs {

    /**
     * Peut être nul.
     */
    @Nullable
    public Lang getWorkingLang();

    @Nullable
    public Locale getCustomFormatLocale();

    @Nullable
    public LangPreference getCustomLangPreference();

    public Attributes getAttributes();

    @Nullable
    public FicheQuery getDefaultFicheQuery();

    public default String getSimpleTemplateName(TransformationKey transformationKey) {
        AttributeKey attributeKey = BdfUserSpace.toSimpleTemplateAttributeKey(transformationKey);
        return getAttributes().getFirstValue(attributeKey);
    }

    public default String getStreamTemplateName(TransformationKey transformationKey, String extension) {
        AttributeKey attributeKey = BdfUserSpace.toStreamTemplateAttributeKey(transformationKey, extension);
        return getAttributes().getFirstValue(attributeKey);
    }

    public default boolean getBoolean(AttributeKey attributeKey) {
        return getAttributes().isTrue(attributeKey);
    }

    public default String getDefaultHeaderType() {
        Attribute attribute = getAttributes().getAttribute(BdfUserSpace.TABLEEXPORT_HEADERTYPE_KEY);
        if (attribute != null) {
            try {
                return TableExportConstants.checkHeaderType(attribute.getFirstValue());
            } catch (IllegalArgumentException iae) {

            }
        }
        return TableExportConstants.COLUMNTITLE_HEADER;
    }

    public default FicheTableParameters getDefaultFicheTableParameters() {
        FicheTableParametersBuilder builder = new FicheTableParametersBuilder();
        String patternMode = getAttributes().getFirstValue(BdfUserSpace.TABLEEXPORT_PATTERNMODE_KEY);
        if (patternMode != null) {
            try {
                builder.setPatternMode(patternMode);
            } catch (IllegalArgumentException iae) {

            }
        }
        Attribute withAttribute = getAttributes().getAttribute(BdfUserSpace.TABLEEXPORT_WITH_KEY);
        if (withAttribute != null) {
            int valueLength = withAttribute.size();
            for (int i = 0; i < valueLength; i++) {
                builder.putWith(withAttribute.get(i));
            }
        }
        return builder.toFicheTableParameters();
    }

    public default TableExportParameters getDefaultTableExportParameters() {
        return TableExportParametersBuilder.init()
                .setHeaderType(getDefaultHeaderType())
                .setFicheTableParameters(getDefaultFicheTableParameters())
                .toTableExportParameters();
    }

}
