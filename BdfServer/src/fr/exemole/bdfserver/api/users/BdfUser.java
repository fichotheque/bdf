/* BdfServer - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.users;

import net.fichotheque.EditOrigin;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.sphere.LoginKey;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface BdfUser extends UserLangContext {

    public LoginKey getLoginKey();

    public String getBdfServerName();

    public Redacteur getRedacteur();

    public BdfUserPrefs getPrefs();

    public FicheQuery getFicheQuery();

    public Fiches getSelectedFiches();

    /**
     * Retourne la valeur d'un paramètre stocké le temps de la session.
     * L'instance de Bdfuser se contente d'être un lieu de stockage, il ne fait
     * rien avec les valeurs en question. C'est aux applications utilisant ces
     * paramètres d'utiliser une convention de nommage évitant les carambolages.
     */
    public Object getParameterValue(String parameterName);

    public void putParameter(String parameterName, Object parameterValue);

    public EditOrigin newEditOrigin(String source);

    public default String format(int integer) {
        return StringUtils.toString(integer, getFormatLocale());
    }

    public default EditOrigin newEditOrigin(String domain, String commandName) {
        return newEditOrigin(domain + "/" + commandName);
    }

}
