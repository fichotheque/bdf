/* BdfServer_API - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.users;

import fr.exemole.bdfserver.api.namespaces.BdfUserSpace;
import java.util.Locale;
import net.fichotheque.exportation.table.TableExportConstants;
import net.fichotheque.selection.FicheQuery;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;


/**
 *
 * @author Vincent Calame
 */
public interface BdfUserPrefsEditor {

    public BdfUserPrefsEditor setWorkingLang(Lang workingLang);

    public BdfUserPrefsEditor setCustomFormatLocale(Locale customFormatLocale);

    public BdfUserPrefsEditor setCustomLangPreference(LangPreference customLangPreference);

    public BdfUserPrefsEditor setDefaultFicheQuery(FicheQuery defaultFicheQuery);

    public BdfUserPrefsEditor removeAttribute(AttributeKey attributeKey);

    public BdfUserPrefsEditor putAttribute(Attribute attribute);

    public default BdfUserPrefsEditor setBoolean(AttributeKey attributeKey, boolean value) {
        if (value) {
            Attribute attribute = AttributeBuilder.toAttribute(attributeKey, "1");
            putAttribute(attribute);
        } else {
            removeAttribute(attributeKey);
        }
        return this;
    }

    public default BdfUserPrefsEditor setDefaultHeaderType(String headerType) {
        putAttribute(AttributeBuilder.toAttribute(BdfUserSpace.TABLEEXPORT_HEADERTYPE_KEY, TableExportConstants.checkHeaderType(headerType)));
        return this;
    }

}
