/* BdfServer_API- Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api;

import fr.exemole.bdfserver.api.storage.BdfExtensionStorage;
import net.fichotheque.Fichotheque;


/**
 *
 * @author Vincent Calame
 */
public interface BdfExtensionInitializer {

    public String getRegistrationName();

    public Factory init(BdfExtensionStorage bdfExtensionStorage, Fichotheque fichotheque);


    public static interface Factory {

        public Object getImplementation(BdfServer bdfServer, Class interfaceClass);

    }

}
