/* BdfServer - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import java.io.File;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface StorageRoot {

    /**
     * Si filePath est vide, c'est le répertoire racine qui est retourné.
     */
    public File getFile(RelativePath filePath);

    public Lock getLock(RelativePath filePath);


    public interface Lock {

    }

}
