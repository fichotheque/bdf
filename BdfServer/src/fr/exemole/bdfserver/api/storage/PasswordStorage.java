/* BdfServer - Copyright (c) 2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;


/**
 *
 * @author Vincent Calame
 */
public interface PasswordStorage {

    public String[] getPasswordArray();

    /**
     * le tableau doit comprendre la chaine du mot de passe puis la chaine de la
     * valeur
     */
    public void savePasswordArray(String[] passwordArray);

}
