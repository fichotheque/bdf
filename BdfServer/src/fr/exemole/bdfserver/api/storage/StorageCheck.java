/* BdfServer_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import fr.exemole.bdfserver.api.groups.GroupDef;
import fr.exemole.bdfserver.api.policies.UserAllow;
import fr.exemole.bdfserver.api.roles.RoleDef;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.api.ui.UiComponents;
import java.util.List;
import java.util.Map;
import net.fichotheque.SubsetKey;
import net.fichotheque.exportation.access.AccessDef;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.fichotheque.selection.SelectionDef;
import net.fichotheque.sphere.Redacteur;


/**
 *
 * @author Vincent Calame
 */
public interface StorageCheck {

    public LangConfiguration getLangConfiguration();

    public List<SelectionDef> getSelectionDefList();

    public List<AccessDef> getAccessDefList();

    public List<ScrutariExportDef> getScrutariExportDefList();

    public List<SqlExportDef> getSqlExportDefList();

    public List<String> getActiveExtensionList();

    public List<RoleDef> getRoleDefList();

    public List<GroupDef> getGroupDefList();

    public Map<Short, SubsetTree> getSubsetTreeMap();

    public UserAllow getUserAllow();

    public Map<SubsetKey, Object> getSubsetPolicyMap();

    public Map<String, List<Redacteur>> getRedacteurListMap();

    public Map<SubsetKey, UiCheck> getUiCheckMap();


    public interface UiCheck {

        public UiComponents getMainUiComponents();

    }

}
