/* BdfServer - Copyright (c) 2007-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.table.TableExportDef;


/**
 * Les différentes méthodes peuvent renvoyer des exceptions BdfStorageException.
 * Elles renverront également des exceptions si les chaines ne répondent pas à
 * StringUtils.isTechnicalName(name, true)
 *
 * @author Vincent Calame
 */
public interface TableExportStorage {

    /**
     * Classé par ordre alphabétique du nom
     */
    public Unit[] checkStorage();

    public Unit getTableExportStorageUnit(String tableExportName);

    public boolean saveTableExportDef(TableExportDef tableExportDef, EditOrigin editOrigin);

    public boolean removeTableExport(String tableExportName, EditOrigin editOrigin);

    /**
     * @throw IllegalArgumentException si newTableExportName n'est pas un nom
     * technique
     */
    public boolean copyTableExport(String tableExportName, String newTableExportName, EditOrigin editOrigin);

    public StorageContent getStorageContent(String tableExportName, String contentPath);

    /**
     * @throw IllegalArgumentException si tableExportName ou contentPath ne sont
     * pas des noms corrects
     */
    public void saveStorageContent(String tableExportName, String contentPath, InputStream inputStream, EditOrigin editOrigin) throws IOException;

    public boolean removeStorageContent(String tableExportName, String contentPath, EditOrigin editOrigin);


    public interface Unit {

        public TableExportDef getTableExportDef();

        public List<StorageContent> getStorageContentList();

    }

}
