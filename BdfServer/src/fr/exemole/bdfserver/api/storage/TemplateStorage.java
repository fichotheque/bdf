/* BdfServer - Copyright (c) 2007-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateKey;


/**
 *
 * @author Vincent Calame
 */
public interface TemplateStorage {

    public Unit[] checkStorage();

    public Unit getTemplateStorageUnit(TemplateKey templateKey);

    public void createTemplate(Unit templateStorageUnit, EditOrigin editOrigin) throws IOException;

    public boolean removeTemplate(TemplateKey templateKey, EditOrigin editOrigin);

    public void saveTemplateDef(TemplateDef templateDef, EditOrigin editOrigin);

    public StorageContent getStorageContent(TemplateKey templateKey, String contentPath);

    public void saveStorageContent(TemplateKey templateKey, String contentPath, InputStream inputStream, EditOrigin editOrigin) throws IOException;

    public boolean removeStorageContent(TemplateKey templateKey, String contentPath, EditOrigin editOrigin);


    public interface Unit {

        public default TemplateKey getTemplateKey() {
            return getTemplateDef().getTemplateKey();
        }

        public TemplateDef getTemplateDef();

        public String getType();

        public List<StorageContent> getStorageContentList();

    }

}
