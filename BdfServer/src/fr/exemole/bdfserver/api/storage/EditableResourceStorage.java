/* BdfServer_API - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import net.mapeadores.util.io.ResourceStorage;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface EditableResourceStorage extends ResourceStorage {

    public void saveResource(RelativePath path, InputStream inputStream, EditOrigin editOrigin) throws IOException;

    public boolean createFolder(RelativePath path);

    public boolean removeResource(RelativePath path, EditOrigin editOrigin);

}
