/* BdfServer - Copyright (c) 2012-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import java.util.Map;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;


/**
 *
 * @author Vincent Calame
 */
public interface BdfUserStorage {

    public void saveBdfUserPrefs(Redacteur redacteur, BdfUserPrefs bdfUserPrefs);

    public void removeBdfUser(Sphere sphere, int id);

    public void removeSphere(SubsetKey sphereKey);

    /**
     * Nul si non défini
     */
    public BdfUserPrefs getBdfUserPrefs(Redacteur redacteur);

    public Map<String, String> getStoreMap(Redacteur redacteur, String name);

    public void putStoreMap(Redacteur redacteur, String name, Map<String, String> map);

}
