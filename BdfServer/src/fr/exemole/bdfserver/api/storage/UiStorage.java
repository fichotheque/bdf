/* BdfServer_API - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import fr.exemole.bdfserver.api.ui.UiComponents;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.html.TrustedHtmlFactory;


/**
 *
 * @author Vincent Calame
 */
public interface UiStorage {

    public TrustedHtmlFactory getTrustedHtmlFactory();

    public void saveUiComponents(SubsetKey subsetKey, UiComponents uiComponents);

    public void removeUiComponents(SubsetKey subsetKey);

}
