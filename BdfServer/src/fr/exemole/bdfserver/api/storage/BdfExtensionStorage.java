/* BdfServer_API - Copyright (c) 2009-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import java.io.File;


/**
 *
 * @author Vincent Calame
 */
public interface BdfExtensionStorage {

    /**
     * Retourne e chemin d'un répertoire qui peut ne pas exister (mais qui peut
     * être créé)
     */
    public File getDataDirectory(String extensionName);

    /**
     * Peut retourner null si aucune sauvegarde n'est prévue
     */
    public File getBackupDirectory(String extensionName);

}
