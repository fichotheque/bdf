/* BdfServer_API - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import fr.exemole.bdfserver.api.roles.RoleDef;

/**
 *
 * @author Vincent Calame
 */
public interface RoleStorage {

    public void saveRoleDef(RoleDef roleDef);
    
    public void removeRoleDef(String roleName);
}
