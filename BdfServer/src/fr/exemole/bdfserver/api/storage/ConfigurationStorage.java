/* BdfServer_API - Copyright (c) 2014-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface ConfigurationStorage {

    public void saveLangConfiguration(LangConfiguration langConfiguration);

    public void saveActiveExtensionList(List<String> activeExtensionList);

}
