/* BdfServer - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import java.io.File;


/**
 *
 * @author Vincent Calame
 */
public class BdfStorageException extends RuntimeException {

    public BdfStorageException() {
    }

    public BdfStorageException(String message) {
        super(message);
    }

    public BdfStorageException(Throwable throwable) {
        super(throwable);
    }

    public BdfStorageException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public BdfStorageException(File f, Throwable throwable) {
        super("file:" + f.getAbsolutePath(), throwable);
    }

}
