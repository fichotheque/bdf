/* BdfServer_API - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.balayage.BalayageDef;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageStorage {

    public Unit[] checkStorage();

    public Unit getBalayageStorageUnit(String balayageName);

    public boolean removeBalayage(String balayageName, EditOrigin editOrigin);

    public void saveBalayageDef(BalayageDef balayageDef, EditOrigin editOrigin);

    public StorageContent getStorageContent(String balayageName, String contentPath);

    public void saveStorageContent(String balayageName, String contentPath, InputStream inputStream, EditOrigin editOrigin) throws IOException;

    public boolean removeStorageContent(String balayageName, String contentPath, EditOrigin editOrigin);


    public interface Unit {

        public BalayageDef getBalayageDef();

        public List<StorageContent> getStorageContentList();

    }


}
