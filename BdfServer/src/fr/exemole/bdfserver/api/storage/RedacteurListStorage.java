/* BdfServer_API - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.storage;

import java.util.List;
import net.fichotheque.sphere.Redacteur;


/**
 *
 * @author Vincent Calame
 */
public interface RedacteurListStorage {

    public void saveRedacteurList(String name, List<Redacteur> redacteurList);

    public void removeRedacteurList(String name);

}
