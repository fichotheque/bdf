/* BdfServer_API - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.memento;

import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface MementoUnit extends MementoNode {

    public RelativePath getPath();


    public interface Info {

        public String getName();

        public RelativePath getPath();

        public String getTitle();

    }

}
