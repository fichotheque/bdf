/*  BdfServer_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api;

import net.fichotheque.FichothequeEditor;


/**
 *
 * @author Vincent Calame
 */
public interface EditSession extends AutoCloseable {

    public BdfServer getBdfServer();

    public FichothequeEditor getFichothequeEditor();

    public BdfServerEditor getBdfServerEditor();

    @Override
    public void close();

}
