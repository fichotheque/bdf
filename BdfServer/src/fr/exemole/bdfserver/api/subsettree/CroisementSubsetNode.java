/* BdfServer_API - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.subsettree;

import net.fichotheque.Subset;
import net.fichotheque.croisement.Croisements;


/**
 *
 * @author Vincent Calame
 */
public interface CroisementSubsetNode extends SubsetNode {

    public Subset getSubset();

    public Croisements getCroisements();

}
