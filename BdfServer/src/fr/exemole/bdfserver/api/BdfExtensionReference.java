/* BdfServer_API - Copyright (c) 2012-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api;

import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public interface BdfExtensionReference {

    public String getRegistrationName();

    public boolean isActive();

    public Message getTitleMessage();

    public Object getImplementation(Class interfaceClass);

}
