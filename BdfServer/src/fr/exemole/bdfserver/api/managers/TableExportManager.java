/* BdfServer_API - Copyright (c) 2007-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import net.fichotheque.EditOrigin;
import net.fichotheque.SubsetKey;
import net.fichotheque.exportation.table.TableDef;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.exportation.table.TableExportDescription;


/**
 *
 * @author Vincent Calame
 */
public interface TableExportManager {

    public void update();

    public List<TableExportDescription> getTableExportDescriptionList();

    public List<TableExportDescription> getValidTableExportDescriptionList();

    public String getTableExportContent(String tableExportName, String contentPath);

    /**
     * Déclenche une mise à jour (Pas besoin de lancer update() suite à cet
     * appel)
     */
    public void removeTableExport(String tableExportName, EditOrigin editOrigin);

    /**
     * Déclenche une mise à jour (Pas besoin de lancer update() suite à cet
     * appel)
     *
     * @throw IllegalArgumentException si newTableExportName n'est pas un nom
     * technique
     */
    public TableExportDescription copyTableExport(String tableExportName, String newTableExportName, EditOrigin editOrigin);

    public TableExportDescription putTableExportDef(TableExportDef tableExportDef, EditOrigin editOrigin);

    public TableExportDescription putTableExportContent(String tableExportName, String contentPath, InputStream inputStream, EditOrigin editOrigin) throws IOException;

    public TableExportDescription removeTableExportContent(String tableExportName, String contentPath, EditOrigin editOrigin);

    public TableExportDescription putTableDef(String tableExportName, SubsetKey corpusKey, TableDef tableDef, EditOrigin editOrigin);

    public TableExport getTableExport(String tableExportName);

    public boolean containsTableExport(String tableExportName);

}
