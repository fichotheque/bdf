/* BdfServer - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import fr.exemole.bdfserver.api.storage.StorageContent;
import fr.exemole.bdfserver.api.storage.TemplateStorage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.NoDefaultTemplateException;
import net.fichotheque.exportation.transformation.SimpleTemplate;
import net.fichotheque.exportation.transformation.StreamTemplate;
import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationDescription;
import net.fichotheque.exportation.transformation.TransformationKey;


/**
 * Interface de gestion des gabarits de transformation.
 *
 * @author Vincent Calame
 */
public interface TransformationManager {

    public void clearDistTransformer();

    public void update();

    public String getTemplateContent(TemplateKey templateKey, String contentPath);

    public StorageContent getTemplateStorageContent(TemplateKey templateKey, String contentPath);

    public TemplateDescription updateTemplateDescription(TemplateKey templateKey);

    public TemplateDescription createTemplate(TemplateStorage.Unit templateStorageUnit, EditOrigin editOrigin) throws IOException;

    public void putTemplateDef(TemplateDef templateDef, EditOrigin editOrigin);

    public TemplateDescription putTemplateContent(TemplateKey templateKey, String path, InputStream inputStream, EditOrigin editOrigin) throws IOException;

    public TemplateDescription removeTemplateContent(TemplateKey templateKey, String path, EditOrigin editOrigin);

    public TransformationDescription getTransformationDescription(TransformationKey transformationKey);

    public boolean containsTemplate(TemplateKey templateKey);

    public SimpleTemplate getSimpleTemplate(TemplateKey templateKey, boolean useDefault);

    public StreamTemplate getStreamTemplate(TemplateKey templateKey, boolean useDefault) throws NoDefaultTemplateException;

    public boolean removeTemplate(TemplateKey templateKey, EditOrigin editOrigin);

    public Set<String> getDefaultStreamTemplateAvailableExtensionSet(TransformationKey transformationKey);

    public default boolean isDefaultStreamTemplateAvailable(TransformationKey transformationKey, String extension) {
        return getDefaultStreamTemplateAvailableExtensionSet(transformationKey).contains(extension);
    }

}
