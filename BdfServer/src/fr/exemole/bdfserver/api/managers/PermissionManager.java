/* BdfServer_API - Copyright (c) 2006-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import fr.exemole.bdfserver.api.roles.Role;
import java.util.List;
import net.fichotheque.sphere.Redacteur;


/**
 *
 * @author Vincent Calame
 */
public interface PermissionManager {

    public boolean isAdmin(Redacteur redacteur);

    /**
     * Copie non synchronisée
     *
     * @return la liste des administrateurs
     */
    public List<Redacteur> getAdminRedacteurList();

    /**
     * Le rôle par défaut est toujours placé en premier. Non synchronisé.
     *
     * @return la liste des rôles
     */
    public List<Role> getRoleList();

    /**
     * N'est jamais nul ni de longueur nulle. Un rédacteur a toujours au moins
     * un rôle (en général celui par défaut). Non synchronisé.
     *
     * @return la liste des rôles d'un rédacteur
     */
    public List<Role> getRoleList(Redacteur redacteur);

    /**
     * Copie non synchronisée
     *
     * @return la liste des rédacteurs d'un rôle
     */
    public List<Redacteur> getRedacteurList(Role role);

    public Role getRole(String roleName);

}
