/* BdfServer_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import fr.exemole.bdfserver.api.users.BdfUser;
import java.util.List;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.selection.SelectionDef;


/**
 *
 * @author Vincent Calame
 */
public interface SelectionManager {

    public List<SelectionDef> getSelectionDefList();

    public SelectionDef getSelectionDef(String name);

    public void putSelectionDef(SelectionDef selectionDef);

    public void removeSelectionDef(String name);

    public void updateFicheSelection(BdfUser bdfUser);

    public void setFicheSelection(BdfUser bdfUser, FicheQuery ficheQuery, String sortType);

    public void setFicheSelection(BdfUser bdfUser, FichothequeQueries fichothequeQueries, String sortType);

    public void setFicheSelection(BdfUser bdfUser, String selectionDefName, String sortType);

    public void setCustomSelectedFiches(BdfUser bdfUser, Fiches fiches);

}
