/* BdfServer - Copyright (c) 2006-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;


/**
 *
 * @author Vincent Calame
 */
public interface PasswordManager {

    public boolean checkPassword(String passwordKey, String passwordValue);

    public void setPassword(String passwordKey, String passwordValue);

    public String getEncryptedPassword(String passwordKey);

    public void setEncryptedPassword(String passwordKey, String encryptedPasswordValue);

    /**
     * Succession avec une chaine correspondant à la clé puis une chaine
     * correspondant à la valeur
     */
    public String[] toPasswordArray();

}
