/* BdfServer_API - Copyright (c) 2012-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import fr.exemole.bdfserver.api.BdfExtensionReference;
import fr.exemole.bdfserver.api.storage.BdfExtensionStorage;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface ExtensionManager {

    public List<BdfExtensionReference> getBdfExtensionReferenceList();

    public BdfExtensionReference getBdfExtensionReference(String registrationName);

    public BdfExtensionStorage getBdfExtensionStorage();

}
