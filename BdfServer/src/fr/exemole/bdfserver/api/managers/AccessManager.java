/* BdfServer_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import java.util.List;
import net.fichotheque.exportation.access.AccessDef;


/**
 *
 * @author Vincent Calame
 */
public interface AccessManager {

    public List<AccessDef> getAccessDefList();

    public AccessDef getAccessDef(String name);

    public void putAccessDef(AccessDef accessDef);

    public void removeAccessDef(String name);

}
