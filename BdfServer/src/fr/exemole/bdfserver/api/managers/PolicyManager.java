/* BdfServer_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import fr.exemole.bdfserver.api.policies.UserAllow;
import net.fichotheque.thesaurus.policies.PolicyProvider;


/**
 *
 * @author Vincent Calame
 */
public interface PolicyManager {

    public UserAllow getUserAllow();

    public PolicyProvider getPolicyProvider();

}
