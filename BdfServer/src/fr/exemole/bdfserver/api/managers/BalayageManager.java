/* BdfServer_API - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import fr.exemole.bdfserver.api.balayage.BalayageSession;
import fr.exemole.bdfserver.api.balayage.LockedBalayageException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.balayage.BalayageDef;
import net.fichotheque.exportation.balayage.BalayageDescription;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageManager {

    public void update();

    public List<BalayageDescription> getBalayageDescriptionList();

    public BalayageDescription getBalayage(String name);

    public BalayageSession openBalayageSession(String name) throws LockedBalayageException;

    public String getBalayageContent(String balayageName, String contentPath);

    public BalayageDescription putBalayageDef(BalayageDef balayageDef, EditOrigin editOrigin);

    public BalayageDescription putBalayageContent(String balayageName, String contentPath, InputStream inputStream, EditOrigin editOrigin) throws IOException;

    public BalayageDescription removeBalayageContent(String balayageName, String contentPath, EditOrigin editOrigin);

    public void removeBalayage(String balayageName, EditOrigin editOrigin);

}
