/* BdfServer - Copyright (c) 2007-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.managers;

import java.util.List;
import net.fichotheque.exportation.sql.SqlExportDef;


/**
 *
 * @author Vincent Calame
 */
public interface SqlExportManager {

    public List<SqlExportDef> getSqlExportDefList();

    public SqlExportDef getSqlExportDef(String name);

    public void putSqlExportDef(SqlExportDef sqlExportDef);

    public void removeSqlExportDef(String name);

}
