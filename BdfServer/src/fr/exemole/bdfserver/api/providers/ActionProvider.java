/* BdfServer_API - Copyright (c) 2019-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.providers;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.menu.ActionGroup;
import fr.exemole.bdfserver.api.users.BdfUser;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface ActionProvider {

    public final static String ADMIN_KEY = "admin";

    @Nullable
    public String getCssThemeFile(String actionKey);

    public ActionGroup getActionGroup(String actionKey, BdfServer bdfServer, BdfUser bdfUser);

}
