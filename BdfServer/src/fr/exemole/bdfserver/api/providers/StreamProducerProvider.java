/* BdfServer_API - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.providers;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import net.mapeadores.util.io.StreamProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public interface StreamProducerProvider {

    public StreamProducer getStreamProducer(OutputParameters parameters) throws ErrorMessageException;

}
