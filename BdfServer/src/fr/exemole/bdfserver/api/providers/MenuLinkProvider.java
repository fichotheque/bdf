/* BdfServer_API - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.providers;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.menu.MenuGroup;
import fr.exemole.bdfserver.api.users.BdfUser;


/**
 *
 * @author Vincent Calame
 */
public interface MenuLinkProvider {

    public final static String MAINMENU_KEY = "mainMenu";

    /**
     * Peut retourner nul s'il n'y a pas de groupes de menu pour l'objet en
     * question.
     */
    public MenuGroup[] getMenuGroupArray(BdfServer bdfServer, BdfUser bdfUser, Object menuObject);

}
