/* BdfServer_API - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.providers;

import fr.exemole.bdfserver.api.ui.MetadataPhraseDef;
import java.util.List;
import net.fichotheque.Metadata;


/**
 *
 * @author Vincent Calame
 */
public interface MetadataPhraseDefProvider {

    public List<MetadataPhraseDef> getMetadataPhraseDefList(Metadata metadata);

}
