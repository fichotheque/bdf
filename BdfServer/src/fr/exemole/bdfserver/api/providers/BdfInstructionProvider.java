/* BdfServer_API - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.api.providers;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public interface BdfInstructionProvider {

    public BdfInstruction getBdfInstruction(BdfServer bdfServer, String extensionPath, RequestMap requestMap);

}
