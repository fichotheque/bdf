Pour ouvrir les sources avec Eclipse, vous pouvez utiliser comme modèle le contenu du répertoire BDF/.

Pour cela, ouvrez BDF/project.xml, remplacez /home/user/bdf par le chemin du dépôt sur votre ordinateur (une seule occurrence à la fin du fichier à la définition de BDF_PATH) et renommez le fichier BDF/.project 

Faites de même avec BDF/classpath.xml : remplacez toutes les occurrences de /home/user/bdf par le chemin de votre dépôt et renommez le fichier BDF/.classpath

Dans Eclipse, ouvrez File -> Import... et choisissez l'option General -> Existing Projects into Workspace. À l'étape suivante, indiquez le chemin de votre dépôt dans le premier champ « Select Root directory » et cochez l'option « Copy projects into workspace » puis validez. (Testé avec Eclipse 2024-12).  
