/* FichothequeLib_Xml - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.defs;

import java.io.IOException;
import net.fichotheque.exportation.scrutari.CorpusScrutariDef;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.fichotheque.exportation.scrutari.ThesaurusScrutariDef;
import net.fichotheque.format.FormatSourceKey;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariExportDefXMLPart extends XMLPart {

    public ScrutariExportDefXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public ScrutariExportDefXMLPart addScrutariExportDef(ScrutariExportDef scrutariExportDef) throws IOException {
        openTag("scrutariexport");
        LabelUtils.addLabels(this, scrutariExportDef.getTitleLabels());
        addSimpleElement("authority", scrutariExportDef.getAuthority());
        addSimpleElement("base-name", scrutariExportDef.getBaseName());
        addSimpleElement("base-icon", scrutariExportDef.getBaseIcon());
        Langs uiLangs = scrutariExportDef.getUiLangs();
        if (!uiLangs.isEmpty()) {
            openTag("langs-ui");
            for (Lang lang : uiLangs) {
                addSimpleElement("lang", lang.toString());
            }
            closeTag("langs-ui");
        }
        addHrefPattern(scrutariExportDef.getFicheHrefPattern(), "fiche-pattern");
        addHrefPattern(scrutariExportDef.getMotcleHrefPattern(), "motcle-pattern");
        Labels shortIntitule = scrutariExportDef.getCustomBaseIntitule(1);
        if (shortIntitule != null) {
            openTag("intitule-short");
            LabelUtils.addLabels(this, shortIntitule);
            closeTag("intitule-short");
        }
        Labels longIntitule = scrutariExportDef.getCustomBaseIntitule(2);
        if (longIntitule != null) {
            openTag("intitule-long");
            LabelUtils.addLabels(this, longIntitule);
            closeTag("intitule-long");
        }
        addSimpleElement("target-name", scrutariExportDef.getTargetName());
        addSimpleElement("target-path", scrutariExportDef.getTargetPath().toString());
        for (String includeToken : scrutariExportDef.getIncludeTokenList()) {
            addSimpleElement("include-token", includeToken);
        }
        for (CorpusScrutariDef corpusScrutariDef : scrutariExportDef.getCorpusScrutariDefList()) {
            addCorpusScrutariDef(corpusScrutariDef);
        }
        for (ThesaurusScrutariDef thesaurusScrutariDef : scrutariExportDef.getThesaurusScrutariDefList()) {
            addThesaurusScrutariDef(thesaurusScrutariDef);
        }
        FichothequeXMLUtils.writeSelectionOptions(this, scrutariExportDef.getSelectionOptions());
        AttributeUtils.addAttributes(this, scrutariExportDef.getAttributes());
        closeTag("scrutariexport");
        return this;
    }

    private void addHrefPattern(AccoladePattern hrefPattern, String tagName) throws IOException {
        if (hrefPattern == null) {
            return;
        }
        addSimpleElement(tagName, hrefPattern.toString());
    }

    private void addCorpusScrutariDef(CorpusScrutariDef corpusScrutariDef) throws IOException {
        startOpenTag("corpus-def");
        addAttribute("corpus", corpusScrutariDef.getCorpusKey().getSubsetName());
        endOpenTag();
        FormatSourceKey formatSourceKey = corpusScrutariDef.getDateFormatSourceKey();
        if (formatSourceKey != null) {
            addSimpleElement("date", formatSourceKey.getKeyString());
        }
        openTag("field-generation");
        addCData(corpusScrutariDef.getFieldGenerationSource());
        closeTag("field-generation", false);
        addHrefPattern(corpusScrutariDef.getHrefPattern(), "href-pattern");
        String multilangMode = corpusScrutariDef.getMultilangMode();
        if (multilangMode != null) {
            startOpenTag("multilang");
            addAttribute("mode", multilangMode);
            String multilangParam = corpusScrutariDef.getMultilangParam();
            if (multilangParam != null) {
                addAttribute("param", multilangParam);
            }
            closeEmptyTag();
        }
        for (String includeToken : corpusScrutariDef.getIncludeTokenList()) {
            addSimpleElement("include-token", includeToken);
        }
        closeTag("corpus-def");
    }

    private void addThesaurusScrutariDef(ThesaurusScrutariDef thesaurusScrutariDef) throws IOException {
        startOpenTag("thesaurus-def");
        addAttribute("thesaurus", thesaurusScrutariDef.getThesaurusKey().getSubsetName());
        endOpenTag();
        openTag("field-generation");
        addCData(thesaurusScrutariDef.getFieldGenerationSource());
        closeTag("field-generation", false);
        if (thesaurusScrutariDef.isWholeThesaurus()) {
            addEmptyElement("whole-thesaurus");
        }
        for (String includeToken : thesaurusScrutariDef.getIncludeTokenList()) {
            addSimpleElement("include-token", includeToken);
        }
        closeTag("thesaurus-def");
    }

    public static ScrutariExportDefXMLPart init(XMLWriter xmlWriter) {
        return new ScrutariExportDefXMLPart(xmlWriter);
    }

}
