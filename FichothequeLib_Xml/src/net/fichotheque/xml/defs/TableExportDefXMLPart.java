/* FichothequeLib_Xml - Copyright (c) 2016-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.defs;

import java.io.IOException;
import net.fichotheque.exportation.table.TableExportDef;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class TableExportDefXMLPart extends XMLPart {

    public TableExportDefXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public TableExportDefXMLPart addTableExportDef(TableExportDef tableExportDef) throws IOException {
        openTag("tableexport");
        LabelUtils.addLabels(this, tableExportDef.getTitleLabels());
        String langMode = tableExportDef.getLangMode();
        if (langMode != null) {
            startOpenTag("langs");
            addAttribute("mode", langMode);
            Langs langs = tableExportDef.getLangs();
            if (!langs.isEmpty()) {
                endOpenTag();
                for (Lang lang : langs) {
                    addSimpleElement("lang", lang.toString());
                }
                closeTag("langs");
            } else {
                closeEmptyTag();
            }
        }
        AttributeUtils.addAttributes(this, tableExportDef.getAttributes());
        closeTag("tableexport");
        return this;
    }

    public static TableExportDefXMLPart init(XMLWriter xmlWriter) {
        return new TableExportDefXMLPart(xmlWriter);
    }

}
