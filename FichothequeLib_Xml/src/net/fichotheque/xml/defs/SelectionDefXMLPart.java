/* FichothequeLib_Xml - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.defs;

import java.io.IOException;
import net.fichotheque.selection.SelectionDef;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class SelectionDefXMLPart extends XMLPart {

    public SelectionDefXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public SelectionDefXMLPart addSelectionDef(SelectionDef selectionDef) throws IOException {
        openTag("selection");
        LabelUtils.addLabels(this, selectionDef.getTitleLabels());
        FichothequeXMLUtils.writeFichothequeQueries(this, selectionDef.getFichothequeQueries());
        AttributeUtils.addAttributes(this, selectionDef.getAttributes());
        closeTag("selection");
        return this;
    }

    public static SelectionDefXMLPart init(XMLWriter xmlWriter) {
        return new SelectionDefXMLPart(xmlWriter);
    }

}
