/* FichothequeLib_Xml - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.defs;

import java.io.IOException;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class SqlExportDefXMLPart extends XMLPart {

    public SqlExportDefXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public SqlExportDefXMLPart addSqlExportDef(SqlExportDef sqlExportDef) throws IOException {
        openTag("sqlexport");
        LabelUtils.addLabels(this, sqlExportDef.getTitleLabels());
        addSimpleElement("tableexport-name", sqlExportDef.getTableExportName());
        addSimpleElement("sqlexport-classname", sqlExportDef.getSqlExportClassName());
        addSimpleElement("target-name", sqlExportDef.getTargetName());
        addSimpleElement("target-path", sqlExportDef.getTargetPath().toString());
        addSimpleElement("file-name", sqlExportDef.getFileName());
        addSimpleElement("post-command", sqlExportDef.getPostCommand());
        for (SqlExportDef.Param param : sqlExportDef.getParamList()) {
            startOpenTag("param");
            addAttribute("name", param.getName());
            endOpenTag();
            addText(param.getValue());
            closeTag("param", false);
        }
        FichothequeXMLUtils.writeSelectionOptions(this, sqlExportDef.getSelectionOptions());
        AttributeUtils.addAttributes(this, sqlExportDef.getAttributes());
        closeTag("sqlexport");
        return this;
    }

    public static SqlExportDefXMLPart init(XMLWriter xmlWriter) {
        return new SqlExportDefXMLPart(xmlWriter);
    }

}
