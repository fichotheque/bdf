/* FichothequeLib_Xml - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.defs;

import java.io.IOException;
import net.fichotheque.exportation.access.AccessDef;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class AccessDefXMLPart extends XMLPart {

    public AccessDefXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public AccessDefXMLPart addAccessDef(AccessDef accessDef) throws IOException {
        openTag("access");
        if (accessDef.isPublic()) {
            addEmptyElement("public");
        }
        LabelUtils.addLabels(this, accessDef.getTitleLabels());
        addSimpleElement("tableexport-name", accessDef.getTableExportName());
        FichothequeXMLUtils.writeSelectionOptions(this, accessDef.getSelectionOptions());
        AttributeUtils.addAttributes(this, accessDef.getAttributes());
        closeTag("access");
        return this;
    }

    public static AccessDefXMLPart init(XMLWriter xmlWriter) {
        return new AccessDefXMLPart(xmlWriter);
    }

}
