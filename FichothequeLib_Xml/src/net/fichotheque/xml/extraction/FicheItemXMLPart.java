/* FichothequeLib_Xml - Copyright (c) 2009-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.extraction;

import java.io.IOException;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.fiche.Link;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.fiche.Nombre;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Pays;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.extraction.ExtractParameters;
import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.extraction.IrefConverter;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.ListLangContext;
import net.mapeadores.util.localisation.SpecialCodes;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.money.ExtendedCurrency;
import net.mapeadores.util.money.MoneyUtils;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.primitives.DegreSexagesimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.DateFormatBundle;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class FicheItemXMLPart extends XMLPart {

    private final ExtractParameters extractParameters;
    private final FicheBlockXMLPart ficheBlockXMLPart;
    private final ExtractionContext extractionContext;
    private final int extractVersion;

    public FicheItemXMLPart(XMLWriter xmlWriter, ExtractParameters extractParameters) {
        super(xmlWriter);
        this.extractParameters = extractParameters;
        this.extractionContext = extractParameters.getExtractionContext();
        this.extractVersion = extractParameters.getExtractVersion();
        ficheBlockXMLPart = new FicheBlockXMLPart(xmlWriter, extractParameters);
    }

    public void addFicheItems(FicheItems ficheItems, Object fieldOptionObject, IrefConverter irefConverter) throws IOException {
        int size = ficheItems.size();
        for (int i = 0; i < size; i++) {
            addFicheItem(ficheItems.get(i), fieldOptionObject, irefConverter);
        }
    }

    public void addFicheItem(FicheItem ficheItem, Object fieldOptionObject, IrefConverter irefConverter) throws IOException {
        if (ficheItem instanceof Item) {
            addItem((Item) ficheItem);
        } else if (ficheItem instanceof Personne) {
            addPersonne((Personne) ficheItem);
        } else if (ficheItem instanceof Datation) {
            addDatation((Datation) ficheItem, fieldOptionObject);
        } else if (ficheItem instanceof Langue) {
            addLangue((Langue) ficheItem);
        } else if (ficheItem instanceof Pays) {
            addPays((Pays) ficheItem);
        } else if (ficheItem instanceof Link) {
            addLink((Link) ficheItem, fieldOptionObject);
        } else if (ficheItem instanceof Courriel) {
            addCourriel((Courriel) ficheItem);
        } else if (ficheItem instanceof Montant) {
            addMontant((Montant) ficheItem, fieldOptionObject);
        } else if (ficheItem instanceof Nombre) {
            addNombre((Nombre) ficheItem);
        } else if (ficheItem instanceof Geopoint) {
            addGeopoint((Geopoint) ficheItem);
        } else if (ficheItem instanceof Para) {
            addPara((Para) ficheItem, irefConverter);
        } else if (ficheItem instanceof Image) {
            addImage((Image) ficheItem, fieldOptionObject);
        } else {
            throw new ClassCastException("unknown instance of FicheItem = " + ficheItem.getClass().getName());
        }
    }

    public void addItem(Item item) throws IOException {
        addSimpleElement("item", item.getValue());
    }

    public void addCourriel(Courriel courriel) throws IOException {
        EmailCore emailCore = courriel.getEmailCore();
        String addrSpec = emailCore.getAddrSpec();
        startOpenTag("courriel");
        addAttribute("addr-spec", addrSpec);
        String realName = emailCore.getRealName();
        if (realName.length() > 0) {
            addAttribute("real-name", realName);
        }
        int idx = addrSpec.indexOf('@');
        addAttribute("user", addrSpec.substring(0, idx));
        addAttribute("domain", addrSpec.substring(idx + 1));
        endOpenTag();
        addText(courriel.toString());
        closeTag("courriel", false);
    }

    public void addDatation(Datation datation, Object fieldOptionObject) throws IOException {
        FuzzyDate date = datation.getDate();
        startOpenTag("datation");
        if (extractVersion == ExtractionConstants.INITIAL_VERSION) {
            addAttribute("a", String.valueOf(date.getYear()));
            switch (date.getType()) {
                case FuzzyDate.HALFYEAR_TYPE:
                    addAttribute("type", "s");
                    addAttribute("s", String.valueOf(date.getHalfYear()));
                    break;
                case FuzzyDate.QUARTER_TYPE:
                    addAttribute("type", "t");
                    addAttribute("t", String.valueOf(date.getQuarter()));
                    break;
                case FuzzyDate.MONTH_TYPE:
                    addAttribute("type", "m");
                    addAttribute("m", String.valueOf(date.getMonth()));
                    break;
                case FuzzyDate.DAY_TYPE:
                    addAttribute("type", "j");
                    addAttribute("m", String.valueOf(date.getMonth()));
                    addAttribute("j", String.valueOf(date.getDay()));
                    break;
                default:
                    addAttribute("type", "a");
            }
        } else {
            addAttribute("y", String.valueOf(date.getYear()));
            switch (date.getType()) {
                case FuzzyDate.HALFYEAR_TYPE:
                    addAttribute("type", "s");
                    addAttribute("s", String.valueOf(date.getHalfYear()));
                    break;
                case FuzzyDate.QUARTER_TYPE:
                    addAttribute("type", "q");
                    addAttribute("q", String.valueOf(date.getQuarter()));
                    break;
                case FuzzyDate.MONTH_TYPE:
                    addAttribute("type", "m");
                    addAttribute("m", String.valueOf(date.getMonth()));
                    break;
                case FuzzyDate.DAY_TYPE:
                    addAttribute("type", "d");
                    addAttribute("m", String.valueOf(date.getMonth()));
                    addAttribute("d", String.valueOf(date.getDay()));
                    break;
                default:
                    addAttribute("type", "y");
            }
        }
        addAttribute("iso", date.toISOString());
        addAttribute("sort", date.toSortString());
        endOpenTag();
        addDateLabels(datation.getDate(), fieldOptionObject);
        closeTag("datation");
    }

    private void addDateLabels(FuzzyDate date, Object fieldOptionObject) throws IOException {
        if ((fieldOptionObject != null) && (fieldOptionObject instanceof Short)) {
            short labelType = ((Short) fieldOptionObject);
            date = date.truncate(labelType);
        }
        LangContext langContext = extractionContext.getLangContext();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                Locale formatLocale = unit.getFormatLocale();
                DateFormatBundle dateFormatBundle = DateFormatBundle.getDateFormatBundle(formatLocale);
                LabelUtils.addLabel(this, lang, date.getDateLitteral(dateFormatBundle));
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            Locale formatLocale = userLangContext.getFormatLocale();
            DateFormatBundle dateFormatBundle = DateFormatBundle.getDateFormatBundle(formatLocale);
            LabelUtils.addLabel(this, null, date.getDateLitteral(dateFormatBundle));
        }
    }

    public void addLangue(Langue langue) throws IOException {
        String code = langue.getLang().toString();
        startOpenTag("langue");
        addAttribute("lang", code);
        endOpenTag();
        addLangLabels(code);
        closeTag("langue");
    }

    private void addLangLabels(String code) throws IOException {
        LangContext langContext = extractionContext.getLangContext();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                String message = extractionContext.getMessageLocalisationProvider().getMessageLocalisation(lang).toString(code);
                if (message != null) {
                    message = StringUtils.getFirstPart(message);
                    LabelUtils.addLabel(this, lang, message);
                }
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            String message = extractionContext.getMessageLocalisationProvider().getMessageLocalisation(userLangContext).toString(code);
            if (message != null) {
                message = StringUtils.getFirstPart(message);
                LabelUtils.addLabel(this, null, message);
            }
        }
    }

    public void addNombre(Nombre nombre) throws IOException {
        startOpenTag("nombre");
        Decimal decimal = nombre.getDecimal();
        addDecimalAttributes(decimal);
        endOpenTag();
        addNombreLabels(decimal);
        closeTag("nombre");
    }

    public void addGeopoint(Geopoint geopoint) throws IOException {
        startOpenTag("geopoint");
        DegreDecimal latitude = geopoint.getLatitude();
        DegreDecimal longitude = geopoint.getLongitude();
        addAttribute("lat", latitude.toString());
        addAttribute("lon", longitude.toString());
        endOpenTag();
        DegreSexagesimal latitudeSexa = DegreSexagesimal.fromDegreDecimal(latitude);
        DegreSexagesimal longitudeSexa = DegreSexagesimal.fromDegreDecimal(longitude);
        startOpenTag("latitude");
        addSexagesimalAttributes(latitudeSexa);
        endOpenTag();
        addSexagesimalLabels(latitudeSexa, true);
        closeTag("latitude");
        startOpenTag("longitude");
        addSexagesimalAttributes(longitudeSexa);
        endOpenTag();
        addSexagesimalLabels(longitudeSexa, false);
        closeTag("longitude");
        closeTag("geopoint");
    }

    private void addSexagesimalAttributes(DegreSexagesimal sexa) throws IOException {
        addAttribute("deg", sexa.getDegre());
        addAttribute("mn", sexa.getMinute());
        addAttribute("sec", sexa.getSeconde());
    }

    private void addSexagesimalLabels(DegreSexagesimal sexa, boolean latitude) throws IOException {
        String code;
        if (latitude) {
            code = SpecialCodes.getLatitudeSpecialCode(sexa);
        } else {
            code = SpecialCodes.getLongitudeSpecialCode(sexa);
        }
        LangContext langContext = extractionContext.getLangContext();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                StringBuilder buf = new StringBuilder();
                buf.append(sexa.toString(false, " "));
                buf.append(" ");
                String message = extractionContext.getMessageLocalisationProvider().getMessageLocalisation(lang).toString(code);
                if (message != null) {
                    buf.append(message);
                } else {
                    buf.append(code);
                }
                LabelUtils.addLabel(this, lang, buf.toString());
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            StringBuilder buf = new StringBuilder();
            buf.append(sexa.toString(false, " "));
            buf.append(" ");
            String message = extractionContext.getMessageLocalisationProvider().getMessageLocalisation(userLangContext).toString(code);
            if (message != null) {
                buf.append(message);
            } else {
                buf.append(code);
            }
            LabelUtils.addLabel(this, null, buf.toString());
        }
    }

    private void addNombreLabels(Decimal decimal) throws IOException {
        LangContext langContext = extractionContext.getLangContext();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                Locale formatLocale = unit.getFormatLocale();
                DecimalFormatSymbols symbols = new DecimalFormatSymbols(formatLocale);
                String strg = decimal.toString(symbols);
                LabelUtils.addLabel(this, lang, strg);
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            Locale formatLocale = userLangContext.getFormatLocale();
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(formatLocale);
            String strg = decimal.toString(symbols);
            LabelUtils.addLabel(this, null, strg);
        }
    }

    public void addMontant(Montant montant, Object fieldOptionObject) throws IOException {
        startOpenTag("montant");
        Decimal decimal = montant.getDecimal();
        addDecimalAttributes(decimal);
        addAttribute("cur", montant.getCurrency().getCurrencyCode());
        endOpenTag();
        addMontantLabels(decimal, montant.getCurrency(), montant.toMoneyLong(), fieldOptionObject);
        closeTag("montant");
    }

    private void addMontantLabels(Decimal decimal, ExtendedCurrency currency, long moneyLong, Object fieldOptionObject) throws IOException {
        LangContext langContext = extractionContext.getLangContext();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                Locale formatLocale = unit.getFormatLocale();
                DecimalFormatSymbols symbols = new DecimalFormatSymbols(formatLocale);
                String strg;
                if ((fieldOptionObject != null) && (fieldOptionObject.equals(ExtractionConstants.FORCE_SUBUNIT))) {
                    strg = MoneyUtils.toLitteralString(moneyLong, currency, symbols, false);
                } else {
                    strg = MoneyUtils.toLitteralString(decimal, currency, symbols);
                }
                LabelUtils.addLabel(this, lang, strg);
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            Locale formatLocale = userLangContext.getFormatLocale();
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(formatLocale);
            String strg;
            if ((fieldOptionObject != null) && (fieldOptionObject.equals(ExtractionConstants.FORCE_SUBUNIT))) {
                strg = MoneyUtils.toLitteralString(moneyLong, currency, symbols, false);
            } else {
                strg = MoneyUtils.toLitteralString(decimal, currency, symbols);
            }
            LabelUtils.addLabel(this, null, strg);
        }
    }

    private void addDecimalAttributes(Decimal decimal) throws IOException {
        String partieEntiere = decimal.getPartieEntiereString();
        String partieDecimale = decimal.getPartieDecimaleString();
        addAttribute("int", partieEntiere);
        addAttribute("dec", partieDecimale);
        String val = (partieDecimale.length() != 0) ? partieEntiere + "." + partieDecimale : partieEntiere;
        addAttribute("val", val);
    }

    public void addLink(Link link, Object fieldOptionObject) throws IOException {
        startOpenTag("link");
        addAttribute("href", link.getHref());
        if ((fieldOptionObject != null) && (fieldOptionObject instanceof String)) {
            if (!StringUtils.isAbsoluteUrlString(link.getHref())) {
                addAttribute("base-url", (String) fieldOptionObject);
            }
        }
        endOpenTag();
        String title = link.getTitle();
        if (title.length() == 0) {
            title = link.getHref();
            if ((title.startsWith("http://")) && (title.length() > 8)) {
                title = title.substring(7);
            } else if ((title.startsWith("https://")) && (title.length() > 9)) {
                title = title.substring(8);
            }
        }
        addSimpleElement("title", title);
        addSimpleElement("comment", link.getComment());
        closeTag("link");
    }

    public void addImage(Image image, Object fieldOptionObject) throws IOException {
        startOpenTag("image");
        addAttribute("src", image.getSrc());
        if ((fieldOptionObject != null) && (fieldOptionObject instanceof String)) {
            if (!StringUtils.isAbsoluteUrlString(image.getSrc())) {
                addAttribute("base-url", (String) fieldOptionObject);
            }
        }
        endOpenTag();
        addSimpleElement("alt", image.getAlt());
        addSimpleElement("title", image.getTitle());
        closeTag("image");
    }

    public void addPays(Pays pays) throws IOException {
        startOpenTag("pays");
        addAttribute("country", pays.getCountry().toString());
        endOpenTag();
        addPaysLabels(pays.getCountry().toString());
        closeTag("pays");
    }

    private void addPaysLabels(String countryCode) throws IOException {
        LangContext langContext = extractionContext.getLangContext();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                String message = extractionContext.getMessageLocalisationProvider().getMessageLocalisation(lang).toString(countryCode);
                if (message != null) {
                    message = StringUtils.getFirstPart(message);
                    LabelUtils.addLabel(this, lang, message);
                }
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            String message = extractionContext.getMessageLocalisationProvider().getMessageLocalisation(userLangContext).toString(countryCode);
            if (message != null) {
                message = StringUtils.getFirstPart(message);
                LabelUtils.addLabel(this, null, message);
            }
        }
    }

    public void addPersonne(Personne personne) throws IOException {
        startOpenTag("personne");
        String redacteurGlobalId = personne.getRedacteurGlobalId();
        if (redacteurGlobalId != null) {
            try {
                SubsetKey sphereKey = SphereUtils.getSubsetKey(redacteurGlobalId);
                int id = SphereUtils.getId(redacteurGlobalId);
                addAttribute("sphere", sphereKey.getSubsetName());
                addAttribute("id", String.valueOf(id));
                Redacteur redacteur = null;
                Sphere sphere = (Sphere) extractionContext.getFichotheque().getSubset(sphereKey);
                if (sphere != null) {
                    redacteur = sphere.getRedacteurById(id);
                }
                if (redacteur != null) {
                    addAttribute("login", redacteur.getLogin());
                    endOpenTag();
                    addPersonCore(redacteur.getPersonCore());
                    closeTag("personne");
                } else {
                    closeEmptyTag();
                }
            } catch (java.text.ParseException pe) {
                closeEmptyTag();
            }
        } else {
            endOpenTag();
            addPersonCore(personne.getPersonCore());
            addSimpleElement("organism", personne.getOrganism());
            if (extractVersion == ExtractionConstants.INITIAL_VERSION) {
                addSimpleElement("organisme", personne.getOrganism());
            }
            closeTag("personne");
        }
    }

    private void addPersonCore(PersonCore personCore) throws IOException {
        String surname = personCore.getSurname();
        boolean surnameFirst = personCore.isSurnameFirst();
        if (surname.length() > 0) {
            startOpenTag("surname");
            if (surnameFirst) {
                addAttribute("surname-first", "true");
            }
            endOpenTag();
            addText(surname);
            closeTag("surname", false);
        }
        addSimpleElement("forename", personCore.getForename());
        addSimpleElement("nonlatin", personCore.getNonlatin());
        if (extractVersion == ExtractionConstants.INITIAL_VERSION) {
            if (surname.length() > 0) {
                startOpenTag("nom");
                if (surnameFirst) {
                    addAttribute("avant", "true");
                }
                endOpenTag();
                addText(surname);
                closeTag("nom", false);
            }
            addSimpleElement("prenom", personCore.getForename());
            addSimpleElement("original", personCore.getNonlatin());
        }
    }

    public void addPara(Para para, IrefConverter irefConverter) throws IOException {
        startOpenTag("para");
        endOpenTag();
        ficheBlockXMLPart.addTextContent(para, irefConverter);
        closeTag("para", false);
    }

}
