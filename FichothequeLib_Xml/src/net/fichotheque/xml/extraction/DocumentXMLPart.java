/* FichothequeLib_Xml - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.extraction;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import net.fichotheque.addenda.Document;
import net.fichotheque.addenda.Version;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.ExtractParameters;
import net.fichotheque.extraction.def.DocumentFilter;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.extraction.run.DocumentExtractInfo;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.ListLangContext;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.primitives.FileLength;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class DocumentXMLPart extends XMLPart {

    private final ExtractParameters extractParameters;

    public DocumentXMLPart(XMLWriter xmlWriter, ExtractParameters extractParameters) {
        super(xmlWriter);
        this.extractParameters = extractParameters;
    }

    public void addDocument(DocumentExtractInfo documentExtractInfo) throws IOException {
        DocumentFilter documentFilter = documentExtractInfo.getDocumentFilter();
        Document document = documentExtractInfo.getDocument();
        Croisement croisement = documentExtractInfo.getCroisement();
        String tagName = getTagName(documentFilter.getTagNameInfo());
        if (tagName != null) {
            startOpenTag(tagName);
            addAttribute("addenda", document.getSubsetName());
            addAttribute("id", document.getId());
            addAttribute("basename", document.getBasename());
            int poids = ExtractionXMLUtils.getMainPoids(croisement);
            if (poids > 0) {
                addAttribute("poids", String.valueOf(poids));
            }
            endOpenTag();
            ExtractionXMLUtils.writeCroisement(this, croisement);
        }
        for (Version version : document.getVersionList()) {
            addVersion(version);
        }
        if (tagName != null) {
            closeTag(tagName);
        }
    }

    private void addVersion(Version version) throws IOException {
        FileLength fileLength = version.getFileLength();
        float value = fileLength.getRoundedValue();
        short roundType = fileLength.getRoundType();
        boolean isKibiOctet = (fileLength.getRoundType() == FileLength.KB_ROUND);
        startOpenTag("version");
        addAttribute("extension", version.getExtension());
        endOpenTag();
        startOpenTag("size");
        addAttribute("value", String.valueOf(value));
        String unit = (isKibiOctet) ? "Ki" : "Mi";
        addAttribute("unit", unit);
        endOpenTag();
        addSizeLib(value, isKibiOctet);
        closeTag("size");
        closeTag("version");
    }

    private void addSizeLib(float value, boolean isKibiOctet) throws IOException {
        LangContext langContext = extractParameters.getExtractionContext().getLangContext();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                Locale locale = unit.getFormatLocale();
                NumberFormat format = NumberFormat.getInstance(locale);
                StringBuilder buf = new StringBuilder();
                buf.append(format.format(value));
                buf.append(' ');
                if (isKibiOctet) {
                    buf.append(LocalisationUtils.getKibiOctet(lang));
                } else {
                    buf.append(LocalisationUtils.getMebiOctet(lang));
                }
                LabelUtils.addLabel(this, lang, buf.toString());
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            Lang lang = userLangContext.getWorkingLang();
            Locale locale = userLangContext.getFormatLocale();
            NumberFormat format = NumberFormat.getInstance(locale);
            StringBuilder buf = new StringBuilder();
            buf.append(format.format(value));
            buf.append(' ');
            if (isKibiOctet) {
                buf.append(LocalisationUtils.getKibiOctet(lang));
            } else {
                buf.append(LocalisationUtils.getMebiOctet(lang));
            }
            LabelUtils.addLabel(this, null, buf.toString());
        }
    }

    private String getTagName(TagNameInfo tagNameInfo) {
        switch (tagNameInfo.getType()) {
            case TagNameInfo.CUSTOM_TYPE:
                return tagNameInfo.getCustomTagName();
            case TagNameInfo.NULL_TYPE:
                return null;
            default:
                return "document";
        }
    }

}
