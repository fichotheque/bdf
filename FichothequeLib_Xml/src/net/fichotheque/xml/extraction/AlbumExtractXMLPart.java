/* FichothequeLib_Xml - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.extraction;

import java.io.IOException;
import java.util.List;
import net.fichotheque.album.Album;
import net.fichotheque.extraction.ExtractParameters;
import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.def.AlbumExtractDef;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.extraction.filterunit.FilterUnit;
import net.fichotheque.extraction.run.AlbumExtractResult;
import net.fichotheque.extraction.run.IllustrationExtractInfo;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class AlbumExtractXMLPart extends XMLPart {

    private final ExtractParameters extractParameters;
    private final int extractVersion;
    private final IllustrationXMLPart illustrationXMLPart;

    public AlbumExtractXMLPart(XMLWriter xmlWriter, ExtractParameters extractParameters) {
        super(xmlWriter);
        this.extractParameters = extractParameters;
        this.extractVersion = extractParameters.getExtractVersion();
        illustrationXMLPart = new IllustrationXMLPart(xmlWriter);
    }

    public void addAlbumExtract(AlbumExtractResult albumExtractResult) throws IOException {
        addAlbumExtract(albumExtractResult, null);
    }

    public void addAlbumExtract(AlbumExtractResult albumExtractResult, FilterUnit filterUnit) throws IOException {
        AlbumExtractDef albumExtractDef = albumExtractResult.getAlbumExtractDef();
        List<AlbumExtractResult.Entry> entryList = albumExtractResult.getEntryList();
        if (entryList.isEmpty()) {
            if (extractParameters.hideIfEmpty(filterUnit)) {
                return;
            }
        }
        String tagName = getTagName(albumExtractDef.getTagNameInfo());
        boolean withTag = (tagName != null);
        String name = albumExtractDef.getName();
        if (name != null) {
            if (withTag) {
                startOpenTag(tagName);
                addAttribute("name", name);
                if (extractVersion == ExtractionConstants.INITIAL_VERSION) {
                    addAttribute("extrait-name", name);
                }
                ExtractionXMLUtils.addFilterParameters(this, filterUnit);
                endOpenTag();
            }
            for (AlbumExtractResult.Entry entry : entryList) {
                for (IllustrationExtractInfo illustrationExtractInfo : entry.getIllustrationExtractInfoList()) {
                    illustrationXMLPart.addIllustration(illustrationExtractInfo);
                }
            }
            if (withTag) {
                closeTag(tagName);
            }
        } else {
            for (AlbumExtractResult.Entry entry : entryList) {
                Album album = entry.getAlbum();
                if (withTag) {
                    startOpenTag(tagName);
                    addAttribute("album", album.getSubsetName());
                    ExtractionXMLUtils.addFilterParameters(this, filterUnit);
                    endOpenTag();
                }
                for (IllustrationExtractInfo illustrationExtractInfo : entry.getIllustrationExtractInfoList()) {
                    illustrationXMLPart.addIllustration(illustrationExtractInfo);
                }
                if (withTag) {
                    closeTag(tagName);
                }
            }
        }
    }

    private String getTagName(TagNameInfo tagNameInfo) {
        switch (tagNameInfo.getType()) {
            case TagNameInfo.CUSTOM_TYPE:
                return tagNameInfo.getCustomTagName();
            case TagNameInfo.NULL_TYPE:
                return null;
            default:
                switch (extractVersion) {
                    case ExtractionConstants.INITIAL_VERSION:
                        return "extraitalbum";
                    default:
                        return "illustrations";
                }
        }
    }

}
