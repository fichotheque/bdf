/* FichothequeLib_Xml - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.extraction;

import java.io.IOException;
import java.util.List;
import net.fichotheque.extraction.ExtractParameters;
import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.extraction.def.ThesaurusExtractDef;
import net.fichotheque.extraction.filterunit.FilterUnit;
import net.fichotheque.extraction.run.MotcleExtractInfo;
import net.fichotheque.extraction.run.ThesaurusExtractResult;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.ListLangContext;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusExtractXMLPart extends XMLPart {

    private final ExtractParameters extractParameters;
    private final int extractVersion;
    private final MotcleXMLPart motcleXMLPart;

    public ThesaurusExtractXMLPart(XMLWriter xmlWriter, ExtractParameters extractParameters) {
        super(xmlWriter);
        this.extractParameters = extractParameters;
        this.extractVersion = extractParameters.getExtractVersion();
        motcleXMLPart = new MotcleXMLPart(xmlWriter, extractParameters);
    }

    public void addThesaurusExtract(ThesaurusExtractResult thesaurusExtractResult) throws IOException {
        addThesaurusExtract(thesaurusExtractResult, null);
    }

    public void addThesaurusExtract(ThesaurusExtractResult thesaurusExtractResult, FilterUnit filterUnit) throws IOException {
        ThesaurusExtractDef thesaurusExtractDef = thesaurusExtractResult.getThesaurusExtractDef();
        List<ThesaurusExtractResult.Entry> entryList = thesaurusExtractResult.getEntryList();
        if (entryList.isEmpty()) {
            if (extractParameters.hideIfEmpty(filterUnit)) {
                return;
            }
        }
        String tagName = getTagName(thesaurusExtractDef.getTagNameInfo());
        boolean withTag = (tagName != null);
        String name = thesaurusExtractDef.getName();
        if (name != null) {
            if (withTag) {
                startOpenTag(tagName);
                addAttribute("name", name);
                if (extractVersion == ExtractionConstants.INITIAL_VERSION) {
                    addAttribute("extrait-name", name);
                }
                ExtractionXMLUtils.addFilterParameters(this, filterUnit);
                endOpenTag();
            }
            for (ThesaurusExtractResult.Entry entry : entryList) {
                for (MotcleExtractInfo motcleExtractInfo : entry.getMotcleExtractInfoList()) {
                    motcleXMLPart.addMotcle(motcleExtractInfo);
                }
            }
            if (withTag) {
                closeTag(tagName);
            }
        } else {
            boolean withThesaurusTitle = thesaurusExtractDef.isWithThesaurusTitle();
            LangContext langContext = extractParameters.getExtractionContext().getLangContext();
            for (ThesaurusExtractResult.Entry entry : entryList) {
                Thesaurus thesaurus = entry.getThesaurus();
                if (withTag) {
                    startOpenTag(tagName);
                    addAttribute("thesaurus", thesaurus.getSubsetName());
                    ExtractionXMLUtils.addFilterParameters(this, filterUnit);
                    endOpenTag();
                }
                for (MotcleExtractInfo motcleExtractInfo : entry.getMotcleExtractInfoList()) {
                    motcleXMLPart.addMotcle(motcleExtractInfo);
                }
                if (withThesaurusTitle) {
                    String titleTagName = getTitleTagName();
                    openTag(titleTagName);
                    if (langContext instanceof ListLangContext) {
                        for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                            Lang lang = unit.getLang();
                            String title = FichothequeUtils.getTitle(thesaurus, lang);
                            LabelUtils.addLabel(this, lang, title);
                        }
                    } else if (langContext instanceof UserLangContext) {
                        UserLangContext userLangContext = (UserLangContext) langContext;
                        Lang workingLang = userLangContext.getWorkingLang();
                        String title = FichothequeUtils.getTitle(thesaurus, workingLang);
                        LabelUtils.addLabel(this, null, title);
                    }
                    closeTag(titleTagName);
                }
                if (withTag) {
                    closeTag(tagName);
                }
            }
        }
    }

    private String getTagName(TagNameInfo tagNameInfo) {
        switch (tagNameInfo.getType()) {
            case TagNameInfo.CUSTOM_TYPE:
                return tagNameInfo.getCustomTagName();
            case TagNameInfo.NULL_TYPE:
                return null;
            default:
                switch (extractVersion) {
                    case ExtractionConstants.INITIAL_VERSION:
                        return "extraitthesaurus";
                    default:
                        return "motcles";
                }
        }
    }

    private String getTitleTagName() {
        switch (extractVersion) {
            case ExtractionConstants.INITIAL_VERSION:
                return "intitule";
            default:
                return "title";
        }
    }

}
