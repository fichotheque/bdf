/* FichothequeLib_Xml - Copyright (c) 2009-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.extraction;

import net.fichotheque.corpus.metadata.FieldKey;


/**
 *
 * @author Vincent Calame
 */
public class FieldNameInfo {

    public final static FieldNameInfo NO_PART = new FieldNameInfo();
    private String part1 = null;
    private String part2 = null;

    private FieldNameInfo() {
    }

    public FieldNameInfo(String part1, String part2) {
        this.part1 = part1;
        this.part2 = part2;
    }

    public String getPart1() {
        return part1;
    }

    public String getPart2() {
        return part2;
    }

    public static FieldNameInfo testPrefix(FieldKey fieldKey, String prefix) {
        String fieldName = fieldKey.getFieldName();
        if (fieldName.startsWith(prefix)) {
            return new FieldNameInfo(prefix, fieldName.substring(prefix.length()));
        } else {
            return null;
        }
    }

}
