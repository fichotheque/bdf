/* FichothequeLib_Xml - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.extraction;

import java.io.IOException;
import java.util.List;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.extraction.ExtractParameters;
import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.def.AddendaExtractDef;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.extraction.filterunit.FilterUnit;
import net.fichotheque.extraction.run.AddendaExtractResult;
import net.fichotheque.extraction.run.DocumentExtractInfo;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class AddendaExtractXMLPart extends XMLPart {

    private final ExtractParameters extractParameters;
    private final int extractVersion;
    private final DocumentXMLPart documentXMLPart;

    public AddendaExtractXMLPart(XMLWriter xmlWriter, ExtractParameters extractParameters) {
        super(xmlWriter);
        this.extractParameters = extractParameters;
        this.extractVersion = extractParameters.getExtractVersion();
        documentXMLPart = new DocumentXMLPart(xmlWriter, extractParameters);
    }

    public void addAddendaExtract(AddendaExtractResult addendaExtractResult) throws IOException {
        addAddendaExtract(addendaExtractResult, null);
    }

    public void addAddendaExtract(AddendaExtractResult addendaExtractResult, FilterUnit filterUnit) throws IOException {
        AddendaExtractDef addendaExtractDef = addendaExtractResult.getAddendaExtractDef();
        List<AddendaExtractResult.Entry> entryList = addendaExtractResult.getEntryList();
        if (entryList.isEmpty()) {
            if (extractParameters.hideIfEmpty(filterUnit)) {
                return;
            }
        }
        String tagName = getTagName(addendaExtractDef.getTagNameInfo());
        boolean withTag = (tagName != null);
        String name = addendaExtractDef.getName();
        if (name != null) {
            if (withTag) {
                startOpenTag(tagName);
                addAttribute("name", name);
                if (extractVersion == ExtractionConstants.INITIAL_VERSION) {
                    addAttribute("extrait-name", name);
                }
                ExtractionXMLUtils.addFilterParameters(this, filterUnit);
                endOpenTag();
            }
            for (AddendaExtractResult.Entry entry : entryList) {
                for (DocumentExtractInfo documentExtractInfo : entry.getDocumentExtractInfoList()) {
                    documentXMLPart.addDocument(documentExtractInfo);
                }
            }
            if (withTag) {
                closeTag(tagName);
            }
        } else {
            for (AddendaExtractResult.Entry entry : entryList) {
                Addenda addenda = entry.getAddenda();
                if (withTag) {
                    startOpenTag(tagName);
                    addAttribute("addenda", addenda.getSubsetName());
                    ExtractionXMLUtils.addFilterParameters(this, filterUnit);
                    endOpenTag();
                }
                for (DocumentExtractInfo documentExtractInfo : entry.getDocumentExtractInfoList()) {
                    documentXMLPart.addDocument(documentExtractInfo);
                }
                if (withTag) {
                    closeTag(tagName);
                }
            }
        }
    }

    private String getTagName(TagNameInfo tagNameInfo) {
        switch (tagNameInfo.getType()) {
            case TagNameInfo.CUSTOM_TYPE:
                return tagNameInfo.getCustomTagName();
            case TagNameInfo.NULL_TYPE:
                return null;
            default:
                switch (extractVersion) {
                    case ExtractionConstants.INITIAL_VERSION:
                        return "extraitaddenda";
                    default:
                        return "documents";
                }
        }
    }

}
