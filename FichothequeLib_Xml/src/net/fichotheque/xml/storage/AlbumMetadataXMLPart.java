/* FichothequeLib_Xml - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.album.metadata.AlbumMetadata;
import net.fichotheque.utils.AlbumUtils;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.awt.ResizeInfo;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class AlbumMetadataXMLPart extends XMLPart {

    public AlbumMetadataXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendAlbumMetadata(AlbumMetadata albumMetadata) throws IOException {
        openTag("album-metadata");
        FichothequeXMLUtils.write(albumMetadata, this);
        for (AlbumDim albumDim : albumMetadata.getAlbumDimList()) {
            appendAlbumDim(albumDim);
        }
        closeTag("album-metadata");
    }

    private void appendAlbumDim(AlbumDim albumDim) throws IOException {
        String type = albumDim.getDimType();
        startOpenTag("album-dim");
        addAttribute("name", albumDim.getName());
        addAttribute("type", type);
        ResizeInfo resizeInfo = albumDim.getResizeInfo();
        if (AlbumUtils.needWidth(type)) {
            addAttribute("width", resizeInfo.getWidth());
        }
        if (AlbumUtils.needHeight(type)) {
            addAttribute("height", resizeInfo.getHeight());
        }
        endOpenTag();
        closeTag("album-dim");
    }

}
