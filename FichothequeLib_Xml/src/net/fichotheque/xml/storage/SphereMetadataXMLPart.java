/* FichothequeLib_Xml - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.sphere.metadata.SphereMetadata;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class SphereMetadataXMLPart extends XMLPart {

    public SphereMetadataXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendSphereMetadata(SphereMetadata sphereMetadata) throws IOException {
        openTag("sphere-metadata");
        FichothequeXMLUtils.write(sphereMetadata, this);
        closeTag("sphere-metadata");
    }

}
