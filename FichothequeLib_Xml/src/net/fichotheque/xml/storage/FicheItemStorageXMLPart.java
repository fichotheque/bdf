/* FichothequeLib_Xml - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.fiche.Link;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.fiche.Nombre;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Pays;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class FicheItemStorageXMLPart extends XMLPart {

    private final FicheBlockStorageXMLPart ficheBlockStorageXMLPart;

    public FicheItemStorageXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
        ficheBlockStorageXMLPart = new FicheBlockStorageXMLPart(xmlWriter);
    }

    public void addFicheItems(FicheItems ficheItems) throws IOException {
        int size = ficheItems.size();
        for (int i = 0; i < size; i++) {
            addFicheItem(ficheItems.get(i), null);
        }
    }

    public void addFicheItems(FicheItems ficheItems, Object fieldOptionObject) throws IOException {
        int size = ficheItems.size();
        for (int i = 0; i < size; i++) {
            addFicheItem(ficheItems.get(i), fieldOptionObject);
        }
    }

    public void addFicheItem(FicheItem ficheItem) throws IOException {
        addFicheItem(ficheItem, null);
    }

    public void addFicheItem(FicheItem ficheItem, Object fieldOptionObject) throws IOException {
        if (ficheItem instanceof Item) {
            addItem((Item) ficheItem);
        } else if (ficheItem instanceof Personne) {
            addPersonne((Personne) ficheItem);
        } else if (ficheItem instanceof Datation) {
            addDatation((Datation) ficheItem, fieldOptionObject);
        } else if (ficheItem instanceof Langue) {
            addLangue((Langue) ficheItem);
        } else if (ficheItem instanceof Pays) {
            addPays((Pays) ficheItem);
        } else if (ficheItem instanceof Link) {
            addLink((Link) ficheItem, fieldOptionObject);
        } else if (ficheItem instanceof Courriel) {
            addCourriel((Courriel) ficheItem);
        } else if (ficheItem instanceof Montant) {
            addMontant((Montant) ficheItem, fieldOptionObject);
        } else if (ficheItem instanceof Nombre) {
            addNombre((Nombre) ficheItem);
        } else if (ficheItem instanceof Geopoint) {
            addGeopoint((Geopoint) ficheItem);
        } else if (ficheItem instanceof Para) {
            addPara((Para) ficheItem);
        } else if (ficheItem instanceof Image) {
            addImage((Image) ficheItem, fieldOptionObject);
        } else {
            throw new ClassCastException("unknown instance of FicheItem = " + ficheItem.getClass().getName());
        }
    }

    public void addItem(Item item) throws IOException {
        addSimpleElement("item", item.getValue());
    }

    public void addCourriel(Courriel courriel) throws IOException {
        EmailCore emailCore = courriel.getEmailCore();
        String addrSpec = emailCore.getAddrSpec();
        startOpenTag("courriel");
        addAttribute("addr-spec", addrSpec);
        String realName = emailCore.getRealName();
        if (realName.length() > 0) {
            addAttribute("real-name", realName);
        }
        closeEmptyTag();
    }

    public void addDatation(Datation datation, Object fieldOptionObject) throws IOException {
        FuzzyDate date = datation.getDate();
        startOpenTag("datation");
        addAttribute("a", String.valueOf(date.getYear()));
        switch (date.getType()) {
            case FuzzyDate.HALFYEAR_TYPE:
                addAttribute("type", "s");
                addAttribute("s", String.valueOf(date.getHalfYear()));
                break;
            case FuzzyDate.QUARTER_TYPE:
                addAttribute("type", "t");
                addAttribute("t", String.valueOf(date.getQuarter()));
                break;
            case FuzzyDate.MONTH_TYPE:
                addAttribute("type", "m");
                addAttribute("m", String.valueOf(date.getMonth()));
                break;
            case FuzzyDate.DAY_TYPE:
                addAttribute("type", "j");
                addAttribute("m", String.valueOf(date.getMonth()));
                addAttribute("j", String.valueOf(date.getDay()));
                break;
            default:
                addAttribute("type", "a");
        }
        closeEmptyTag();
    }

    public void addLangue(Langue langue) throws IOException {
        String code = langue.getLang().toString();
        startOpenTag("langue");
        addAttribute("lang", code);
        closeEmptyTag();
    }

    public void addNombre(Nombre nombre) throws IOException {
        startOpenTag("nombre");
        Decimal decimal = nombre.getDecimal();
        addDecimalAttributes(decimal);
        closeEmptyTag();
    }

    public void addGeopoint(Geopoint geopoint) throws IOException {
        startOpenTag("geopoint");
        DegreDecimal latitude = geopoint.getLatitude();
        DegreDecimal longitude = geopoint.getLongitude();
        addAttribute("lat", latitude.toString());
        addAttribute("lon", longitude.toString());
        closeEmptyTag();
    }

    public void addMontant(Montant montant, Object fieldOptionObject) throws IOException {
        startOpenTag("montant");
        Decimal decimal = montant.getDecimal();
        addDecimalAttributes(decimal);
        addAttribute("cur", montant.getCurrency().getCurrencyCode());
        closeEmptyTag();
    }


    private void addDecimalAttributes(Decimal decimal) throws IOException {
        String partieEntiere = decimal.getPartieEntiereString();
        String partieDecimale = decimal.getPartieDecimaleString();
        String val = (partieDecimale.length() != 0) ? partieEntiere + "." + partieDecimale : partieEntiere;
        addAttribute("val", val);
    }

    public void addLink(Link link, Object fieldOptionObject) throws IOException {
        startOpenTag("link");
        addAttribute("href", link.getHref());
        if (link.hasHrefOnly()) {
            closeEmptyTag();
        } else {
            endOpenTag();
            addSimpleElement("title", link.getTitle());
            addSimpleElement("comment", link.getComment());
            closeTag("link");
        }
    }

    public void addImage(Image image, Object fieldOptionObject) throws IOException {
        startOpenTag("image");
        addAttribute("src", image.getSrc());
        if (image.hasSrcOnly()) {
            closeEmptyTag();
        } else {
            endOpenTag();
            addSimpleElement("alt", image.getAlt());
            addSimpleElement("title", image.getTitle());
            closeTag("image");
        }
    }

    public void addPays(Pays pays) throws IOException {
        startOpenTag("pays");
        addAttribute("country", pays.getCountry().toString());
        closeEmptyTag();
    }


    public void addPersonne(Personne personne) throws IOException {
        startOpenTag("personne");
        String redacteurGlobalId = personne.getRedacteurGlobalId();
        if (redacteurGlobalId != null) {
            try {
                SubsetKey sphereKey = SphereUtils.getSubsetKey(redacteurGlobalId);
                int id = SphereUtils.getId(redacteurGlobalId);
                addAttribute("sphere", sphereKey.getSubsetName());
                addAttribute("id", String.valueOf(id));
                closeEmptyTag();
            } catch (java.text.ParseException pe) {
                closeEmptyTag();
            }
        } else {
            endOpenTag();
            addPersonCore(personne.getPersonCore());
            addSimpleElement("organism", personne.getOrganism());
            closeTag("personne");
        }
    }

    private void addPersonCore(PersonCore personCore) throws IOException {
        String surname = personCore.getSurname();
        boolean surnameFirst = personCore.isSurnameFirst();
        if (surname.length() > 0) {
            startOpenTag("surname");
            if (surnameFirst) {
                addAttribute("surname-first", "true");
            }
            endOpenTag();
            addText(surname);
            closeTag("surname", false);
        }
        addSimpleElement("forename", personCore.getForename());
        addSimpleElement("nonlatin", personCore.getNonlatin());
    }

    public void addPara(Para para) throws IOException {
        startOpenTag("para");
        endOpenTag();
        ficheBlockStorageXMLPart.addTextContent(para);
        closeTag("para", false);
    }

}
