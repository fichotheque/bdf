/* FichothequeLib_Xml - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.croisement.Lien;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class CroisementStorageXMLPart extends XMLPart {

    public CroisementStorageXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendCroisement(Croisement croisement) throws IOException {
        openTag("croisement");
        for (Lien lien : croisement.getLienList()) {
            startOpenTag("lien");
            addAttribute("mode", lien.getMode());
            addAttribute("poids", lien.getPoids());
            addAttribute("position1", lien.getPosition1());
            addAttribute("position2", lien.getPosition2());
            closeEmptyTag();
        }
        AttributeUtils.addAttributes(this, croisement.getAttributes());
        closeTag("croisement");
    }

}
