/* FichothequeLib_Xml - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurStorageXMLPart extends XMLPart {

    public RedacteurStorageXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendRedacteur(Redacteur redacteur) throws IOException {
        openTag("redacteur");
        appendPersonCore(redacteur.getPersonCore());
        EmailCore emailCore = redacteur.getEmailCore();
        if (emailCore != null) {
            appendEmailCore(emailCore);
        }
        AttributeUtils.addAttributes(this, redacteur.getAttributes());
        closeTag("redacteur");
    }

    private void appendPersonCore(PersonCore personCore) throws IOException {
        startOpenTag("person-core");
        if (personCore.isSurnameFirst()) {
            addAttribute("surname-first", "true");
        }
        addAttribute("surname", personCore.getSurname());
        addAttribute("forename", personCore.getForename());
        addAttribute("nonlatin", personCore.getNonlatin());
        closeEmptyTag();
    }

    private void appendEmailCore(EmailCore emailCore) throws IOException {
        startOpenTag("email-core");
        addAttribute("addr-spec", emailCore.getAddrSpec());
        addAttribute("real-name", emailCore.getRealName());
        closeEmptyTag();
    }

}
