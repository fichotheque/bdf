/* FichothequeLib_Xml - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusMetadataXMLPart extends XMLPart {

    public ThesaurusMetadataXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendThesaurusMetadata(ThesaurusMetadata thesaurusMetadata) throws IOException {
        startOpenTag("thesaurus-metadata");
        addAttribute("thesaurus-type", ThesaurusUtils.thesaurusTypeToString(thesaurusMetadata.getThesaurusType()));
        endOpenTag();
        Langs langs = thesaurusMetadata.getAuthorizedLangs();
        if (langs != null) {
            startOpenTag("langs");
            endOpenTag();
            for (Lang lang : langs) {
                addSimpleElement("lang", lang.toString());
            }
            closeTag("langs");
        }
        FichothequeXMLUtils.write(thesaurusMetadata, this);
        closeTag("thesaurus-metadata");
    }

}
