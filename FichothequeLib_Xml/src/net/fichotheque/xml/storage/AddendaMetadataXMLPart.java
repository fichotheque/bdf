/* FichothequeLib_Xml - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.addenda.metadata.AddendaMetadata;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class AddendaMetadataXMLPart extends XMLPart {

    public AddendaMetadataXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendAddendaMetadata(AddendaMetadata addendaMetadata) throws IOException {
        openTag("addenda-metadata");
        FichothequeXMLUtils.write(addendaMetadata, this);
        closeTag("addenda-metadata");
    }

}
