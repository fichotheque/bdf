/* FichothequeLib_Xml - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class SphereListXMLPart extends XMLPart {

    public SphereListXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendSphereList(Sphere sphere, Predicate<SubsetItem> predicate) throws IOException {
        openTag("sphere-list");
        for (Redacteur redacteur : sphere.getRedacteurList()) {
            appendRedacteur(redacteur, predicate);
        }
        closeTag("sphere-list");
    }

    private void appendRedacteur(Redacteur redacteur, Predicate<SubsetItem> predicate) throws IOException {
        if ((predicate != null) && (!predicate.test(redacteur))) {
            return;
        }
        String status = redacteur.getStatus();
        startOpenTag("redacteur");
        addAttribute("id", redacteur.getId());
        addAttribute("login", redacteur.getLogin());
        addAttribute("status", status);
        closeEmptyTag();
    }

}
