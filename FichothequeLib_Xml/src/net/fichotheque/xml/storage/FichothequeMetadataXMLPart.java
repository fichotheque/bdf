/* FichothequeLib_Xml - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.metadata.FichothequeMetadata;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeMetadataXMLPart extends XMLPart {

    public FichothequeMetadataXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendFichothequeMetadata(FichothequeMetadata fichothequeMetadata) throws IOException {
        openTag("fichotheque-metadata");
        addSimpleElement("authority", fichothequeMetadata.getAuthority());
        addSimpleElement("base-name", fichothequeMetadata.getBaseName());
        FichothequeXMLUtils.write(fichothequeMetadata, this);
        closeTag("fichotheque-metadata");
    }

}
