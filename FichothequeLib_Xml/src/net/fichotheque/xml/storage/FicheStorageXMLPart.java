/* FichothequeLib_Xml - Copyright (c) 2009-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import java.util.List;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Information;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Propriete;
import net.fichotheque.corpus.fiche.Section;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class FicheStorageXMLPart extends XMLPart {

    private final FicheItemStorageXMLPart ficheItemXMLPart;
    private final FicheBlockStorageXMLPart ficheBlockStorageXMLPart;

    public FicheStorageXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
        ficheItemXMLPart = new FicheItemStorageXMLPart(this);
        ficheBlockStorageXMLPart = new FicheBlockStorageXMLPart(this);
    }

    public void appendFiche(FicheAPI fiche) throws IOException {
        startOpenTag("fiche");
        XMLUtils.addXmlLangAttribute(this, fiche.getLang());
        endOpenTag();
        addEntete(fiche);
        addCorpsdefiche(fiche);
        closeTag("fiche");
    }

    private void addEntete(FicheAPI fiche) throws IOException {
        openTag("entete");
        addSimpleElement("titre", fiche.getTitre());
        Para soustitrePara = fiche.getSoustitre();
        if (soustitrePara != null) {
            startOpenTag("soustitre");
            endOpenTag();
            ficheBlockStorageXMLPart.addTextContent(soustitrePara);
            closeTag("soustitre", false);
        }
        for (Propriete propriete : fiche.getProprieteList()) {
            addPropriete(propriete);
        }
        for (Information information : fiche.getInformationList()) {
            addInformation(information);
        }
        FicheItems redacteursItemList = fiche.getRedacteurs();
        if (redacteursItemList != null) {
            openTag("redacteurs");
            ficheItemXMLPart.addFicheItems(redacteursItemList);
            closeTag("redacteurs");
        }
        closeTag("entete");
    }

    private void addCorpsdefiche(FicheAPI fiche) throws IOException {
        List<Section> sectionList = fiche.getSectionList();
        if (sectionList.isEmpty()) {
            return;
        }
        openTag("corpsdefiche");
        for (Section section : sectionList) {
            addSection(section);
        }
        closeTag("corpsdefiche");

    }

    private void addPropriete(Propriete propriete) throws IOException {
        FicheItem ficheItem = propriete.getFicheItem();
        startOpenTag("propriete");
        addAttribute("name", propriete.getFieldKey().getFieldName());
        endOpenTag();
        ficheItemXMLPart.addFicheItem(ficheItem);
        closeTag("propriete");
    }

    private void addInformation(Information information) throws IOException {
        startOpenTag("information");
        addAttribute("name", information.getFieldKey().getFieldName());
        endOpenTag();
        ficheItemXMLPart.addFicheItems(information);
        closeTag("information");
    }

    private void addSection(Section section) throws IOException {
        startOpenTag("section");
        addAttribute("name", section.getFieldKey().getFieldName());
        endOpenTag();
        ficheBlockStorageXMLPart.addFicheBlocks(section);
        closeTag("section");
    }

}
