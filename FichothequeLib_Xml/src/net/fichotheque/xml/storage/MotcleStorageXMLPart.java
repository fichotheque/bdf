/* FichothequeLib_Xml - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class MotcleStorageXMLPart extends XMLPart {

    public MotcleStorageXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendMotcle(Motcle motcle) throws IOException {
        openTag("motcle");
        LabelUtils.addLabels(this, motcle.getLabels());
        AttributeUtils.addAttributes(this, motcle.getAttributes());
        closeTag("motcle");
    }

}
