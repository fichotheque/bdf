/* FichothequeLib_Xml - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetItem;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusTreeXMLPart extends XMLPart {

    public ThesaurusTreeXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendThesaurus(Thesaurus thesaurus, Predicate<SubsetItem> predicate) throws IOException {
        openTag("thesaurus-tree");
        for (Motcle motcle : thesaurus.getFirstLevelList()) {
            appendMotcle(motcle, predicate);
        }
        closeTag("thesaurus-tree");
    }

    private void appendMotcle(Motcle motcle, Predicate<SubsetItem> predicate) throws IOException {
        if ((predicate != null) && (!predicate.test(motcle))) {
            return;
        }
        startOpenTag("motcle");
        addAttribute("id", motcle.getId());
        String idalpha = motcle.getIdalpha();
        if (idalpha != null) {
            addAttribute("idalpha", idalpha);
        }
        String status = motcle.getStatus();
        if (!status.equals(FichothequeConstants.ACTIVE_STATUS)) {
            addAttribute("status", status);
        }
        List<Motcle> children = motcle.getChildList();
        if (children.isEmpty()) {
            closeEmptyTag();
        } else {
            endOpenTag();
            for (Motcle child : children) {
                appendMotcle(child, predicate);
            }
            closeTag("motcle");
        }
    }

}
