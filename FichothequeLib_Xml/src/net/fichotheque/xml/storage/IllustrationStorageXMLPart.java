/* FichothequeLib_Xml - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.storage;

import java.io.IOException;
import net.fichotheque.album.Illustration;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationStorageXMLPart extends XMLPart {

    public IllustrationStorageXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendIllustration(Illustration illustration) throws IOException {
        openTag("illustration");
        AttributeUtils.addAttributes(this, illustration.getAttributes());
        closeTag("illustration");
    }

}
