/* FichothequeLib_Xml - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.importation;

import java.io.IOException;
import java.util.List;
import net.fichotheque.corpus.FicheChange;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.xml.storage.FicheStorageXMLPart;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class CorpusImportXMLPart extends XMLPart {

    private final LiensImportXMLPart liensImportXMLPart;
    private final FicheStorageXMLPart ficheStorageXMLPart;

    public CorpusImportXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
        this.liensImportXMLPart = new LiensImportXMLPart(xmlWriter);
        this.ficheStorageXMLPart = new FicheStorageXMLPart(xmlWriter);
    }

    public void addCorpusImport(CorpusImport corpusImport) throws IOException {
        String type = corpusImport.getType();
        startOpenTag("corpusimport");
        addAttribute("corpus", corpusImport.getCorpus().getSubsetName());
        addAttribute("type", type);
        endOpenTag();
        for (CorpusImport.FicheImport ficheImport : corpusImport.getFicheImportList()) {
            if (ficheImport instanceof CorpusImport.ChangeFicheImport) {
                addChangeFicheImport((CorpusImport.ChangeFicheImport) ficheImport);
            } else if (ficheImport instanceof CorpusImport.CreationFicheImport) {
                addCreationFicheImport((CorpusImport.CreationFicheImport) ficheImport);
            } else {
                addFicheImport(ficheImport);
            }
        }
        closeTag("corpusimport");
    }

    private void addFicheImport(CorpusImport.FicheImport ficheImport) throws IOException {
        startOpenTag("ficheimport");
        addAttribute("id", ficheImport.getFicheMeta().getId());
        closeEmptyTag();
    }

    public void addChangeFicheImport(CorpusImport.ChangeFicheImport ficheImport) throws IOException {
        startOpenTag("ficheimport");
        addAttribute("id", ficheImport.getFicheMeta().getId());
        endOpenTag();
        addFicheChange(ficheImport.getFicheChange());
        addAttributeChange(ficheImport.getAttributeChange());
        liensImportXMLPart.addLiensImport(ficheImport.getLiensImport());
        addCreationDate(ficheImport.getCreationDate());
        closeTag("ficheimport");
    }

    public void addCreationFicheImport(CorpusImport.CreationFicheImport ficheImport) throws IOException {
        startOpenTag("ficheimport");
        int id = ficheImport.getNewId();
        if (id > 0) {
            addAttribute("id", id);
        }
        endOpenTag();
        addFicheChange(ficheImport.getFicheChange());
        addAttributeChange(ficheImport.getAttributeChange());
        liensImportXMLPart.addLiensImport(ficheImport.getLiensImport());
        addCreationDate(ficheImport.getCreationDate());
        closeTag("ficheimport");
    }

    private void addFicheChange(FicheChange ficheChange) throws IOException {
        FicheAPI ficheAPI = ficheChange.getFicheAPI();
        if (ficheAPI != null) {
            ficheStorageXMLPart.appendFiche(ficheAPI);
        }
        List<FieldKey> fieldKeyList = ficheChange.getRemovedList();
        if (!fieldKeyList.isEmpty()) {
            openTag("removed");
            for (FieldKey fieldKey : fieldKeyList) {
                addSimpleElement("key", fieldKey.getKeyString());
            }
            closeTag("removed");
        }
    }

    private void addAttributeChange(AttributeChange attributeChange) throws IOException {
        AttributeUtils.addAttributes(this, attributeChange.getChangedAttributes());
        for (AttributeKey attributeKey : attributeChange.getRemovedAttributeKeyList()) {
            startOpenTag("attr");
            addAttribute("ns", attributeKey.getNameSpace());
            addAttribute("key", attributeKey.getLocalKey());
            closeEmptyTag();
        }
    }

    private void addCreationDate(FuzzyDate creationDate) throws IOException {
        if (creationDate != null) {
            startOpenTag("chrono");
            addAttribute("creation", creationDate.toISOString());
            closeEmptyTag();
        }
    }

}
