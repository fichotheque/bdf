/* FichothequeLib_Xml - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.importation;

import java.io.IOException;
import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetItem;
import net.fichotheque.importation.LiensImport;
import net.fichotheque.include.IncludeKey;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class LiensImportXMLPart extends XMLPart {

    public LiensImportXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addLiensImport(LiensImport liensImport) throws IOException {
        boolean withLiage = liensImport.isLiageRemoved();
        List<IncludeKey> removedIncludeKeyList = liensImport.getRemovedIncludeKeyList();
        if ((!removedIncludeKeyList.isEmpty()) || (withLiage)) {
            startOpenTag("liens");
            addAttribute("type", "remove");
            endOpenTag();
            for (IncludeKey includeKey : removedIncludeKeyList) {
                addSimpleElement("include-key", includeKey.getKeyString());
            }
            if (withLiage) {
                addSimpleElement("include-key", FichothequeConstants.LIAGE_NAME);
            }
            closeTag("liens");
        }
        List<LiensImport.LienImport> replaceList = liensImport.getReplaceLienImportList();
        if (!replaceList.isEmpty()) {
            startOpenTag("liens");
            addAttribute("type", "replace");
            endOpenTag();
            for (LiensImport.LienImport lienImport : replaceList) {
                addLienImport(lienImport);
            }
            closeTag("liens");
        }
        List<LiensImport.LienImport> appendList = liensImport.getAppendLienImportList();
        if (!appendList.isEmpty()) {
            startOpenTag("liens");
            addAttribute("type", "append");
            endOpenTag();
            for (LiensImport.LienImport lienImport : appendList) {
                addLienImport(lienImport);
            }
            closeTag("liens");
        }
    }

    private void addLienImport(LiensImport.LienImport lienImport) throws IOException {
        startOpenTag("lien");
        if (lienImport.isLiageOrigin()) {
            addAttribute("origin", FichothequeConstants.LIAGE_NAME);
        } else {
            addAttribute("origin", lienImport.getOriginIncludeKey().getKeyString());
        }
        addAttribute("mode", lienImport.getMode());
        addAttribute("subset", lienImport.getOtherSubset().getSubsetKeyString());
        addAttribute("poids", lienImport.getPoids());
        SubsetItem subsetItem = lienImport.getOtherSubsetItem();
        if (subsetItem != null) {
            addAttribute("id", subsetItem.getId());
        } else {
            Label label = lienImport.getLabel();
            addAttribute("lang", label.getLang().toString());
            addAttribute("lanel", label.getLabelString());
        }
        closeEmptyTag();
    }

}
