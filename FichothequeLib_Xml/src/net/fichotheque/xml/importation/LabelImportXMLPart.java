/* FichothequeLib_Xml - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.importation;

import java.io.IOException;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.importation.LabelImport;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class LabelImportXMLPart extends XMLPart {

    public LabelImportXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addLabelImport(LabelImport labelImport) throws IOException {
        startOpenTag("labelimport");
        addAttribute("lang", labelImport.getLang().toString());
        endOpenTag();
        List<LabelImport.PhraseImport> fichothequePhaseImportList = labelImport.getFichothequePhraseImportList();
        if (!fichothequePhaseImportList.isEmpty()) {
            openTag("fichotheque");
            addPhraseImportList(fichothequePhaseImportList);
            closeTag("fichotheque");
        }
        for (LabelImport.MetadataImport metadataImport : labelImport.getMetadataImportList()) {
            SubsetKey subsetKey = metadataImport.getSubset().getSubsetKey();
            String tagName = subsetKey.getCategoryString();
            startOpenTag(tagName);
            addAttribute("name", subsetKey.getSubsetName());
            endOpenTag();
            addPhraseImportList(metadataImport.getPhraseImportList());
            closeTag(tagName);
        }
        for (LabelImport.CorpusImport corpusImport : labelImport.getCorpusImportList()) {
            startOpenTag("corpus");
            addAttribute("name", corpusImport.getCorpus().getSubsetName());
            endOpenTag();
            addCorpusLabelImport(corpusImport);
            closeTag("corpus");
        }
        closeTag("labelimport");
    }

    private void addPhraseImportList(List<LabelImport.PhraseImport> phaseImportList) throws IOException {
        for (LabelImport.PhraseImport phraseImport : phaseImportList) {
            CleanedString value = phraseImport.getLabelString();
            String name = phraseImport.getName();
            if (name == null) {
                startOpenTag("title");
            } else {
                startOpenTag("phrase");
                addAttribute("name", name);
            }
            if (value != null) {
                endOpenTag();
                addText(value.toString());
                if (name == null) {
                    closeTag("title", false);
                } else {
                    closeTag("phrase", false);
                }
            } else {
                closeEmptyTag();
            }
        }
    }

    private void addCorpusLabelImport(LabelImport.CorpusImport corpusImport) throws IOException {
        for (LabelImport.FieldKeyImport fieldKeyImport : corpusImport.getFieldKeyImportList()) {
            CleanedString value = fieldKeyImport.getLabelString();
            startOpenTag("field");
            addAttribute("field-key", fieldKeyImport.getFieldKey().getKeyString());
            if (value != null) {
                endOpenTag();
                addText(value.toString());
                closeTag("field", false);
            } else {
                closeEmptyTag();
            }
        }
        for (LabelImport.IncludeKeyImport includeKeyImport : corpusImport.getIncludeKeyImportList()) {
            CleanedString value = includeKeyImport.getLabelString();
            startOpenTag("include");
            addAttribute("include-key", includeKeyImport.getIncludeKey().getKeyString());
            if (value != null) {
                endOpenTag();
                addText(value.toString());
                closeTag("include", false);
            } else {
                closeEmptyTag();
            }
        }
    }

}
