/* FichothequeLib_Xml - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.xml.importation;

import java.io.IOException;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusImportXMLPart extends XMLPart {

    private final LiensImportXMLPart liensImportXMLPart;

    public ThesaurusImportXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
        this.liensImportXMLPart = new LiensImportXMLPart(xmlWriter);
    }

    public void addThesaurusImport(ThesaurusImport thesaurusImport) throws IOException {
        String type = thesaurusImport.getType();
        startOpenTag("thesaurusimport");
        addAttribute("thesaurus", thesaurusImport.getThesaurus().getSubsetName());
        addAttribute("type", type);
        Thesaurus destinationThesaurus = thesaurusImport.getDestinationThesaurus();
        if (destinationThesaurus != null) {
            addAttribute("destination", destinationThesaurus.getSubsetName());
        }
        endOpenTag();
        for (ThesaurusImport.MotcleImport motcleImport : thesaurusImport.getMotcleImportList()) {
            if (motcleImport instanceof ThesaurusImport.ChangeMotcleImport) {
                addChangeMotcleImport((ThesaurusImport.ChangeMotcleImport) motcleImport);
            } else if (motcleImport instanceof ThesaurusImport.CreationMotcleImport) {
                addCreationMotcleImport((ThesaurusImport.CreationMotcleImport) motcleImport);
            } else if (motcleImport instanceof ThesaurusImport.MergeMotcleImport) {
                addMergeMotcleImport((ThesaurusImport.MergeMotcleImport) motcleImport);
            } else {
                addMotcleImport(motcleImport);
            }
        }
        closeTag("thesaurusimport");
    }

    private void addMotcleImport(ThesaurusImport.MotcleImport motcleImport) throws IOException {
        startOpenTag("motcleimport");
        addAttribute("id", motcleImport.getMotcle().getId());
        closeEmptyTag();
    }

    private void addChangeMotcleImport(ThesaurusImport.ChangeMotcleImport motcleImport) throws IOException {
        startOpenTag("motcleimport");
        addAttribute("id", motcleImport.getMotcle().getId());
        endOpenTag();
        String newIdalpha = motcleImport.getNewIdalpha();
        if (newIdalpha != null) {
            addSimpleElement("idalpha", newIdalpha);
        }
        String newStatus = motcleImport.getNewStatus();
        if (newStatus != null) {
            addSimpleElement("status", newStatus);
        }
        Object parent = motcleImport.getParent();
        if (parent != null) {
            startOpenTag("parent");
            if (parent instanceof Motcle) {
                addAttribute("id", ((Motcle) parent).getId());
            }
            closeEmptyTag();
        }
        addLabelChange(motcleImport.getLabelChange());
        addAttributeChange(motcleImport.getAttributeChange());
        liensImportXMLPart.addLiensImport(motcleImport.getLiensImport());
        closeTag("motcleimport");
    }

    private void addCreationMotcleImport(ThesaurusImport.CreationMotcleImport motcleImport) throws IOException {
        startOpenTag("motcleimport");
        int id = motcleImport.getNewId();
        if (id > 0) {
            addAttribute("id", id);
        }
        endOpenTag();
        String newIdalpha = motcleImport.getNewIdalpha();
        if (newIdalpha != null) {
            addSimpleElement("idalpha", newIdalpha);
        }
        String newStatus = motcleImport.getNewStatus();
        if (newStatus != null) {
            addSimpleElement("status", newStatus);
        }
        int parentId = motcleImport.getParentId();
        if (parentId > 0) {
            startOpenTag("parent");
            addAttribute("id", parentId);
            closeEmptyTag();
        } else {
            String parentIdalpha = motcleImport.getParentIdalpha();
            if (parentIdalpha != null) {
                startOpenTag("parent");
                addAttribute("idalpha", parentIdalpha);
                closeEmptyTag();
            }
        }
        addLabelChange(motcleImport.getLabelChange());
        addAttributeChange(motcleImport.getAttributeChange());
        liensImportXMLPart.addLiensImport(motcleImport.getLiensImport());
        closeTag("motcleimport");
    }

    private void addMergeMotcleImport(ThesaurusImport.MergeMotcleImport motcleImport) throws IOException {
        startOpenTag("motcleimport");
        addAttribute("id", motcleImport.getMotcle().getId());
        addAttribute("destination", motcleImport.getDestinationMotcle().getId());
        closeEmptyTag();
    }

    private void addLabelChange(LabelChange labelChange) throws IOException {
        LabelUtils.addLabels(this, labelChange.getChangedLabels());
        for (Lang lang : labelChange.getRemovedLangList()) {
            startOpenTag("label");
            addAttribute("xml:lang", lang.toString());
            closeEmptyTag();
        }
    }

    private void addAttributeChange(AttributeChange attributeChange) throws IOException {
        AttributeUtils.addAttributes(this, attributeChange.getChangedAttributes());
        for (AttributeKey attributeKey : attributeChange.getRemovedAttributeKeyList()) {
            startOpenTag("attr");
            addAttribute("ns", attributeKey.getNameSpace());
            addAttribute("key", attributeKey.getLocalKey());
            closeEmptyTag();
        }
    }

}
