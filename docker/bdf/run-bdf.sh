#!/bin/bash

imageName=bdf
containerName=bdf-1
hostPort=8181
dataPath="$(pwd)"/data
etcPath=$dataPath/etc
varPath=$dataPath/var
extensionPath=$dataPath/extensions

case $1 in
    central)
    imageName=bdf-central
    containerName=bdf-central-1
    ;;
esac

docker run \
--name $containerName \
-p $hostPort:8080 \
--mount type=bind,source=$etcPath,destination=/etc/bdf \
--mount type=bind,source=$varPath,destination=/var/local/bdf \
--mount type=bind,source=$extensionPath,destination=/usr/local/bdf/extensions \
$imageName

#To start, open http://localhost:$hostPort/bdf/multi-admin
