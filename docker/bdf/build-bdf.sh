#!/bin/bash

imageName=bdf
confPath=conf

case $1 in
    central)
    imageName=bdf-central
    confPath=conf-central
    ;;
esac

docker build --build-arg confPath=$confPath -t $imageName .
