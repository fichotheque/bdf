/* BdfServer_Get - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Collection;
import net.fichotheque.SubsetItem;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableExportConstants;
import net.fichotheque.exportation.table.TableExportContext;
import net.fichotheque.tools.exportation.table.CsvTableWriter;
import net.fichotheque.tools.exportation.table.TableExportEngine;
import net.mapeadores.util.io.StreamProducer;
import net.mapeadores.util.mimetype.MimeTypeConstants;


/**
 *
 * @author Vincent Calame
 */
public class TableExportCsvProducer implements StreamProducer {

    private final String fileName;
    private final BdfParameters bdfParameters;
    private final SubsetTable subsetTable;
    private final Collection<SubsetItem> subsetItems;
    private final String headerType;
    private final TableExportContext tableExportContext;
    private final CellEngine cellEngine;

    public TableExportCsvProducer(String fileName, BdfParameters bdfParameters, SubsetTable subsetTable, Collection<SubsetItem> subsetItems, String headerType, TableExportContext tableExportContext, CellEngine cellEngine) {
        this.fileName = fileName;
        this.bdfParameters = bdfParameters;
        this.subsetTable = subsetTable;
        this.subsetItems = subsetItems;
        this.headerType = headerType;
        this.tableExportContext = tableExportContext;
        this.cellEngine = cellEngine;
    }

    @Override
    public String getMimeType() {
        return MimeTypeConstants.CSV;
    }

    @Override
    public String getCharset() {
        return "UTF-8";
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        try (OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8")) {
            CsvTableWriter csvTableWriter = new CsvTableWriter(subsetTable.getColDefList(), writer, bdfParameters.getFormatLocale());
            switch (headerType) {
                case TableExportConstants.COLUMNTITLE_HEADER:
                    csvTableWriter.appendColumnTitleHeader(bdfParameters.getWorkingLang(), tableExportContext.getSourceLabelProvider(), subsetTable.getSubset());
                    break;
                case TableExportConstants.COLUMNKEY_HEADER:
                    csvTableWriter.appendColumnKeyHeader();
                    break;
            }
            TableExportEngine.exportSubset(subsetTable, csvTableWriter, cellEngine, subsetItems);
        }
    }

}
