/* BdfServer_Get - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers.transformation;

import java.io.IOException;
import java.io.OutputStream;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.exportation.transformation.StreamTemplate;
import net.mapeadores.util.io.StreamProducer;


/**
 *
 * @author Vincent Calame
 */
public class PropertiesStreamTemplateProducer implements StreamProducer {

    private final String fileName;
    private final Cell[] cellArray;
    private final StreamTemplate.Properties streamTemplate;

    public PropertiesStreamTemplateProducer(String fileName, Cell[] cellArray, StreamTemplate.Properties streamTemplate) {
        this.fileName = fileName;
        this.cellArray = cellArray;
        this.streamTemplate = streamTemplate;
    }

    @Override
    public String getMimeType() {
        return streamTemplate.getMimeType();
    }

    @Override
    public String getCharset() {
        return null;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        streamTemplate.fill(cellArray, outputStream);
    }

}
