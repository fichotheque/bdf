/* BdfServer_Get - Copyright (c) 2022-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers.transformation;

import fr.exemole.bdfserver.tools.exportation.transformation.TransformerParameters;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import net.fichotheque.exportation.transformation.StreamTemplate;
import net.mapeadores.util.io.StreamProducer;


/**
 *
 * @author Vincent Calame
 */
public class XsltStreamTemplateProducer implements StreamProducer {

    private final String fileName;
    private final String extractionString;
    private final StreamTemplate.Xslt streamTemplate;
    private final TransformerParameters transformerParameters;
    private final Map<String, String> outputProperties;

    public XsltStreamTemplateProducer(String fileName, String extractionString, StreamTemplate.Xslt streamTemplate, TransformerParameters transformerParameters, Map<String, String> outputProperties) {
        this.fileName = fileName;
        this.extractionString = extractionString;
        this.streamTemplate = streamTemplate;
        this.transformerParameters = transformerParameters;
        this.outputProperties = outputProperties;
    }

    @Override
    public String getMimeType() {
        return streamTemplate.getMimeType();
    }

    @Override
    public String getCharset() {
        return null;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        streamTemplate.transform(extractionString, outputStream, transformerParameters.getMap(), outputProperties);
    }

}
