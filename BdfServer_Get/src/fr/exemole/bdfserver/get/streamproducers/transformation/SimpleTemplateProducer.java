/* BdfServer_Get - Copyright (c) 2022-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers.transformation;

import fr.exemole.bdfserver.tools.exportation.transformation.TransformerParameters;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import net.fichotheque.exportation.transformation.SimpleTemplate;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.io.StreamProducer;


/**
 *
 * @author Vincent Calame
 */
public class SimpleTemplateProducer implements StreamProducer {

    private final String fileName;
    private final String extractionString;
    private final SimpleTemplate simpleTemplate;
    private final TransformerParameters transformerParameters;

    public SimpleTemplateProducer(String fileName, String extractionString, SimpleTemplate simpleTemplate, TransformerParameters transformerParameters) {
        this.fileName = fileName;
        this.extractionString = extractionString;
        this.simpleTemplate = simpleTemplate;
        this.transformerParameters = transformerParameters;
    }

    @Override
    public String getMimeType() {
        return simpleTemplate.getMimeType();
    }

    @Override
    public String getCharset() {
        return simpleTemplate.getCharset();
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        String result = simpleTemplate.transform(extractionString, transformerParameters.getMap());
        IOUtils.copy(new StringReader(result), outputStream, simpleTemplate.getCharset());
    }

}
