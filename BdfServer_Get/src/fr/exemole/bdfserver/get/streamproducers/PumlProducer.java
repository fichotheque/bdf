/* BdfServer_Get - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.uml.CorpusPumlWriter;
import fr.exemole.bdfserver.tools.uml.FichothequePumlWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.io.StreamProducer;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.mimetype.MimeTypeConstants;


/**
 *
 * @author Vincent Calame
 */
public class PumlProducer implements StreamProducer {


    private final BdfServer bdfServer;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;
    private final Corpus corpus;

    public PumlProducer(BdfServer bdfServer, BdfUser bdfUser, @Nullable Corpus corpus) {
        this.bdfServer = bdfServer;
        this.lang = bdfUser.getWorkingLang();
        this.messageLocalisation = bdfServer.getL10nManager().getMessageLocalisation(bdfUser);
        this.corpus = corpus;
    }

    @Override
    public String getMimeType() {
        return MimeTypeConstants.PLAIN;
    }

    @Override
    public String getCharset() {
        return "UTF-8";
    }

    @Override
    public String getFileName() {
        return null;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        try (OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8")) {
            if (corpus == null) {
                FichothequePumlWriter pumlWriter = new FichothequePumlWriter(bdfServer, lang, messageLocalisation);
                pumlWriter.write(writer);
            } else {
                CorpusPumlWriter pumlWriter = new CorpusPumlWriter(bdfServer, lang, messageLocalisation, corpus);
                pumlWriter.write(writer);
            }
        }
    }

}
