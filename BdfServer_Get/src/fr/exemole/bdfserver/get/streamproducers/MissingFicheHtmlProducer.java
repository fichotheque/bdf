/* BdfServer_Get - Copyright (c) 2019-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.utils.FichothequeUtils;


/**
 *
 * @author Vincent Calame
 */
public class MissingFicheHtmlProducer extends BdfServerHtmlProducer {

    private final Corpus corpus;
    private final int ficheId;


    public MissingFicheHtmlProducer(BdfParameters bdfParameters, Corpus corpus, int ficheId) {
        super(bdfParameters);
        this.corpus = corpus;
        this.ficheId = ficheId;
        setReversePath("../");
        setBodyCssClass("global-body-White");
    }

    @Override
    public void printHtml() {
        String corpusTitle = FichothequeUtils.getTitle(corpus, workingLang);
        start();
        this
                .P()
                .SPAN("global-ErrorMessage")
                .__localize("_ error.unknown.fiche", corpusTitle, ficheId)
                ._SPAN()
                ._P();
        end();
    }

}
