/* BdfServer_Get - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.exportation.table.TableExportConstants;
import net.fichotheque.tools.exportation.table.TableExportOds;
import net.fichotheque.tools.exportation.table.TableExportOdsFactory;
import net.fichotheque.tools.exportation.table.TableExportOdsParameters;
import net.mapeadores.opendocument.io.odtable.OdsOptions;
import net.mapeadores.util.io.StreamProducer;
import net.mapeadores.util.mimetype.MimeTypeConstants;


/**
 *
 * @author Vincent Calame
 */
public class TableExportOdsProducer implements StreamProducer {

    private static final short MULTI_TYPE = 1;
    private static final short UNIQUE_TYPE = 2;
    private final String fileName;
    private final BdfParameters bdfParameters;
    private final short type;
    private final TableExport tableExport;
    private final Fiches fiches;
    private final SubsetTable uniqueSubsetTable;
    private final Collection<SubsetItem> uniqueSubsetItems;
    private final boolean merge;
    private final TableExportOdsParameters tableExportOdsParameters;

    private TableExportOdsProducer(String fileName, BdfParameters bdfParameters, TableExport tableExport, Fiches fiches, boolean merge, TableExportOdsParameters tableExportOdsParameters) {
        this.fileName = fileName;
        this.type = MULTI_TYPE;
        this.bdfParameters = bdfParameters;
        this.tableExport = tableExport;
        this.fiches = fiches;
        this.uniqueSubsetTable = null;
        this.uniqueSubsetItems = null;
        this.tableExportOdsParameters = tableExportOdsParameters;
        this.merge = merge;
    }

    private TableExportOdsProducer(String fileName, BdfParameters bdfParameters, SubsetTable subsetTable, Collection<SubsetItem> subsetItems, TableExportOdsParameters tableExportOdsParameters) {
        this.fileName = fileName;
        this.type = UNIQUE_TYPE;
        this.bdfParameters = bdfParameters;
        this.tableExport = null;
        this.fiches = null;
        this.uniqueSubsetTable = subsetTable;
        this.uniqueSubsetItems = subsetItems;
        this.tableExportOdsParameters = tableExportOdsParameters;
        this.merge = false;
    }

    @Override
    public String getMimeType() {
        return MimeTypeConstants.ODS;
    }

    @Override
    public String getCharset() {
        return null;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        OdsOptions odsOptions = BdfServerUtils.buildOdsOptions(bdfParameters.getBdfServer(), "bdf://this/css/ods/tableexport.css")
                .supplementarySheetWriter(BdfUserUtils.getSupplementarySheetWriter(bdfParameters));
        TableExportOds tableExportOds = initTableExportOds(odsOptions);
        tableExportOds.write(outputStream, isFixFirstRow());
    }

    private TableExportOds initTableExportOds(OdsOptions odsOptions) {
        if (type == MULTI_TYPE) {
            if (merge) {
                String tableName = bdfParameters.getMessageLocalisation().toNotNullString("_ title.exportation.mergesheet");
                return TableExportOdsFactory.merge(tableName, tableExport, fiches, tableExportOdsParameters, odsOptions);
            } else {
                return TableExportOdsFactory.multi(tableExport, fiches, tableExportOdsParameters, odsOptions);
            }
        } else {
            return TableExportOdsFactory.unique(uniqueSubsetTable, uniqueSubsetItems, tableExportOdsParameters, odsOptions);
        }
    }

    private boolean isFixFirstRow() {
        if (tableExportOdsParameters.getHeaderType().equals(TableExportConstants.NONE_HEADER)) {
            return false;
        }
        if ((type == MULTI_TYPE) && (merge)) {
            return false;
        }
        return true;
    }

    public static TableExportOdsProducer newMultiInstance(String fileName, BdfParameters bdfParameters, TableExport tableExport, Fiches fiches, boolean merge, TableExportOdsParameters tableExportOdsParameters) {
        return new TableExportOdsProducer(fileName, bdfParameters, tableExport, fiches, merge, tableExportOdsParameters);
    }

    public static TableExportOdsProducer newUniqueInstance(String fileName, BdfParameters bdfParameters, SubsetTable subsetTable, Collection<SubsetItem> subsetItemList, TableExportOdsParameters tableExportOdsParameters) {
        return new TableExportOdsProducer(fileName, bdfParameters, subsetTable, subsetItemList, tableExportOdsParameters);
    }

}
