/* BdfServer_Get - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import java.text.DecimalFormatSymbols;
import java.util.Collection;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.ColDef;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableExportConstants;
import net.fichotheque.exportation.table.TableExportContext;
import net.fichotheque.format.SourceLabelProvider;
import net.fichotheque.tools.exportation.table.TableExportEngine;
import net.fichotheque.utils.TableDefUtils;
import net.mapeadores.util.html.HtmlTableWriter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class TableExportHtmlProducer extends BdfServerHtmlProducer {

    private final SubsetTable subsetTable;
    private final Collection<SubsetItem> subsetItems;
    private final String headerType;
    private final boolean fragment;
    private final TableExportContext tableExportContext;
    private final CellEngine cellEngine;

    public TableExportHtmlProducer(BdfParameters bdfParameters, SubsetTable subsetTable, Collection<SubsetItem> subsetItems, String headerType, boolean fragment, TableExportContext tableExportContext, CellEngine cellEngine) {
        super(bdfParameters);
        this.subsetTable = subsetTable;
        this.subsetItems = subsetItems;
        this.headerType = headerType;
        this.fragment = fragment;
        this.tableExportContext = tableExportContext;
        this.cellEngine = cellEngine;
        setReversePath("../");
    }

    @Override
    public void printHtml() {
        if (!fragment) {
            start();
        }
        this
                .TABLE();
        Subset subset = subsetTable.getSubset();
        switch (headerType) {
            case TableExportConstants.COLUMNTITLE_HEADER:
                printHeaderTitle(subsetTable, workingLang, subset);
                break;
            case TableExportConstants.COLUMNKEY_HEADER:
                printHeaderKey(subsetTable);
                break;
        }
        HtmlTableWriter htmlTableWriter = new HtmlTableWriter(this, new DecimalFormatSymbols(bdfUser.getFormatLocale()));
        TableExportEngine.exportSubset(subsetTable, htmlTableWriter, cellEngine, subsetItems);
        this
                ._TABLE();
        if (!fragment) {
            end();
        }
    }

    private void printHeaderTitle(SubsetTable table, Lang workingLang, Subset subset) {
        SourceLabelProvider sourceLabelProvider = tableExportContext.getSourceLabelProvider();
        this
                .TR();
        for (ColDef colDef : table.getColDefList()) {
            this
                    .TH()
                    .__escape(TableDefUtils.getColTitle(colDef, workingLang, sourceLabelProvider, subset))
                    ._TH();
        }
        this
                ._TR();
    }

    private void printHeaderKey(SubsetTable table) {
        this
                .TR();
        for (ColDef colDef : table.getColDefList()) {
            this
                    .TH()
                    .__escape(colDef.getColName())
                    ._TH();
        }
        this
                ._TR();
    }


}
