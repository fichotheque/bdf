/* BdfServer_Get - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers;

import java.io.IOException;
import net.fichotheque.extraction.ExtractionSource;
import net.fichotheque.extraction.def.ExtractionDef;
import net.fichotheque.tools.extraction.ExtractionEngine;
import net.fichotheque.extraction.ExtractParameters;
import net.mapeadores.util.xml.XmlProducer;


/**
 *
 * @author Vincent Calame
 */
public class ExtractionXmlProducer implements XmlProducer {

    private final ExtractParameters extractParameters;
    private final ExtractionDef extractionDef;
    private final ExtractionSource extractionSource;
    private final String extraXml;

    public ExtractionXmlProducer(ExtractParameters extractParameters, ExtractionDef extractionDef, ExtractionSource extractionSource, String extraXml) {
        this.extractParameters = extractParameters;
        this.extractionDef = extractionDef;
        this.extractionSource = extractionSource;
        this.extraXml = extraXml;
    }

    @Override
    public void writeXml(Appendable appendable) throws IOException {
        ExtractionEngine.init(extractParameters, extractionDef)
                .prettyXml(true)
                .extraXml(extraXml)
                .append(extractionSource, appendable);
    }

}
