/* BdfServer_Get - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.streamproducers;

import java.io.IOException;
import net.mapeadores.util.text.lexie.LexieDistribution;
import net.mapeadores.util.text.lexie.xml.LexieDistributionXMLPart;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.UtilXmlProducer;


/**
 *
 * @author Vincent Calame
 */
public class LexieDistributionXmlProducer extends UtilXmlProducer {

    private final LexieDistribution lexieDistribution;

    public LexieDistributionXmlProducer(LexieDistribution lexieDistribution) {
        this.lexieDistribution = lexieDistribution;
    }

    @Override
    public void write(AppendableXMLWriter xmlWriter) throws IOException {
        LexieDistributionXMLPart lexieDistributionXMLPart = new LexieDistributionXMLPart(xmlWriter);
        lexieDistributionXMLPart.appendLexieDistribution(lexieDistribution);
    }

}
