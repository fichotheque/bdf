/* BdfServer_Get - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.instructions;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.get.streamproducers.SphereOdsProducer;
import fr.exemole.bdfserver.tools.roles.PermissionSummaryBuilder;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;
import net.mapeadores.util.servlets.exceptions.ForbiddenException;
import net.mapeadores.util.text.FileName;


/**
 *
 * @author Vincent Calame
 */
public class UsersInstruction implements BdfInstruction {

    private final BdfServer bdfServer;
    private final String filePath;

    public UsersInstruction(BdfServer bdfServer, String filePath) {
        this.bdfServer = bdfServer;
        this.filePath = filePath;
    }

    @Override
    public short getBdfUserNeed() {
        return BdfInstructionConstants.MANDATORY_BDFUSER;
    }

    @Override
    public ResponseHandler runInstruction(BdfUser bdfUser) {
        FileName fileName;
        try {
            fileName = FileName.parse(filePath);
        } catch (ParseException pe) {
            return null;
        }
        if (!fileName.getExtension().equals("ods")) {
            return null;
        }
        String baseName = fileName.getBasename();
        Sphere activeSphere;
        boolean isFichothequeAdmin = BdfUserUtils.isAdmin(bdfServer, bdfUser);
        if (baseName.equals("_all")) {
            if (!isFichothequeAdmin) {
                throw new ForbiddenException();
            }
            activeSphere = null;
        } else {
            try {
                SubsetKey sphereKey = SubsetKey.parse(SubsetKey.CATEGORY_SPHERE, baseName);
                activeSphere = (Sphere) bdfServer.getFichotheque().getSubset(sphereKey);
            } catch (ParseException pe) {
                return null;
            }
            if (activeSphere == null) {
                return null;
            }
            if (!isFichothequeAdmin) {
                PermissionSummary permissionSummary = PermissionSummaryBuilder.build(bdfServer, bdfUser);
                if (!permissionSummary.isSubsetAdmin(activeSphere.getSubsetKey())) {
                    throw new ForbiddenException();
                }
            }
        }
        return ServletUtils.wrap(new SphereOdsProducer(bdfServer, bdfUser, activeSphere));
    }

}
