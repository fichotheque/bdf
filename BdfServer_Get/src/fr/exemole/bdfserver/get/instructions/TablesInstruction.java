/* BdfServer_Get - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.instructions;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.GetConstants;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.get.instructions.tables.MultiTableExportEngine;
import fr.exemole.bdfserver.get.instructions.tables.UniqueTableExportEngine;
import fr.exemole.bdfserver.tools.instruction.DefaultBdfParameters;
import java.text.ParseException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.handlers.ResponseHandlerFactory;
import net.mapeadores.util.text.FileName;


/**
 *
 * @author Vincent Calame
 */
public class TablesInstruction implements BdfInstruction {

    private final BdfServer bdfServer;
    private final String filePath;
    private final RequestMap requestMap;

    public TablesInstruction(BdfServer bdfServer, String filePath, RequestMap requestMap) {
        this.bdfServer = bdfServer;
        this.filePath = filePath;
        this.requestMap = requestMap;
    }

    @Override
    public short getBdfUserNeed() {
        return BdfInstructionConstants.MANDATORY_BDFUSER;
    }

    @Override
    public ResponseHandler runInstruction(BdfUser bdfUser) {
        BdfParameters bdfParameters = new DefaultBdfParameters(bdfServer, bdfUser);
        try {
            if (filePath.equals(GetConstants.GET_MULTI)) {
                MultiTableExportEngine multiTableExportEngine = MultiTableExportEngine.buildFromRequest(bdfParameters, requestMap);
                return multiTableExportEngine.run();
            } else if (filePath.equals(GetConstants.GET_HTMLFRAGMENT)) {
                UniqueTableExportEngine uniqueTableExportEngine = UniqueTableExportEngine.buildFromRequest(bdfParameters, requestMap);
                return uniqueTableExportEngine.run();
            } else {
                FileName fileName;
                try {
                    fileName = FileName.parse(filePath);
                } catch (ParseException pe) {
                    return null;
                }
                UniqueTableExportEngine uniqueTableExportEngine = UniqueTableExportEngine.buildFromPath(bdfParameters, fileName, requestMap);
                return uniqueTableExportEngine.run();
            }
        } catch (ErrorMessageException eme) {
            return ResponseHandlerFactory.getHtmlInstance(bdfParameters.getMessageLocalisation(), eme.getErrorMessage());
        }
    }

}
