/* BdfServer_Get - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.instructions;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.get.streamproducers.LabelsOdsProducer;
import fr.exemole.bdfserver.get.streamproducers.RoleOdsProducer;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.text.ParseException;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;
import net.mapeadores.util.servlets.exceptions.ForbiddenException;
import net.mapeadores.util.text.FileName;


/**
 *
 * @author Vincent Calame
 */
public class AdminInstruction implements BdfInstruction {

    private final static String ROLE_PREFIX = "role-";
    private final BdfServer bdfServer;
    private final String filePath;

    public AdminInstruction(BdfServer bdfServer, String filePath) {
        this.bdfServer = bdfServer;
        this.filePath = filePath;
    }

    @Override
    public short getBdfUserNeed() {
        return BdfInstructionConstants.MANDATORY_BDFUSER;
    }

    @Override
    public ResponseHandler runInstruction(BdfUser bdfUser) {
        FileName fileName;
        try {
            fileName = FileName.parse(filePath);
        } catch (ParseException pe) {
            return null;
        }
        String baseName = fileName.getBasename();
        String extension = fileName.getExtension();
        if (baseName.equals("labels")) {
            if (extension.equals("ods")) {
                testAdmin(bdfUser);
                return ServletUtils.wrap(new LabelsOdsProducer(bdfServer, null));
            } else {
                return null;
            }
        } else if (baseName.equals("roles")) {
            if (extension.equals("ods")) {
                testAdmin(bdfUser);
                return ServletUtils.wrap(new RoleOdsProducer(bdfServer, bdfUser, null, null));
            } else {
                return null;
            }
        } else if (baseName.startsWith(ROLE_PREFIX)) {
            String roleName = baseName.substring(ROLE_PREFIX.length());
            Role role = bdfServer.getPermissionManager().getRole(roleName);
            if (role == null) {
                return null;
            } else {
                if (extension.equals("ods")) {
                    testAdmin(bdfUser);
                    return ServletUtils.wrap(new RoleOdsProducer(bdfServer, bdfUser, role, null));
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

    private void testAdmin(BdfUser bdfUser) {
        if (!BdfUserUtils.isAdmin(bdfServer, bdfUser)) {
            throw new ForbiddenException();
        }
    }

}
