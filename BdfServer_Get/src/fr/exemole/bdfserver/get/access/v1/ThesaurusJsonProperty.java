/* BdfServer_Get - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access.v1;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import java.util.List;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusJsonProperty implements JsonProperty {

    private final Thesaurus thesaurus;
    private final CellEngine cellEngine;
    private final Lang uiLang;
    private final Lang motcleLang;

    public ThesaurusJsonProperty(Thesaurus thesaurus, CellEngine cellEngine, Lang uiLang, Lang motcleLang) {
        this.thesaurus = thesaurus;
        this.cellEngine = cellEngine;
        this.uiLang = uiLang;
        this.motcleLang = motcleLang;
    }

    @Override
    public String getName() {
        return "thesaurus";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        {
            jw.key("name")
                    .value(thesaurus.getSubsetKey().getSubsetName());
            jw.key("title")
                    .value(FichothequeUtils.getTitle(thesaurus, uiLang));
            jw.key("array");
            writeMotcleList(jw, thesaurus.getFirstLevelList());
        }
        jw.endObject();
    }

    private void writeMotcleList(JSONWriter jw, List<Motcle> motcleList) throws IOException {
        jw.array();
        for (Motcle motcle : motcleList) {
            jw.object();
            {
                AccessJson.properties(jw, motcle, cellEngine, motcleLang);
                jw.key("children");
                writeMotcleList(jw, motcle.getChildList());
            }
            jw.endObject();
        }
        jw.endArray();
    }

}
