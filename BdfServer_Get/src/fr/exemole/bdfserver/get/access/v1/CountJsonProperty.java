/* BdfServer_Get - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access.v1;

import java.io.IOException;
import java.util.Collection;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class CountJsonProperty implements JsonProperty {

    private final Collection collection;

    public CountJsonProperty(Collection collection) {
        this.collection = collection;
    }

    @Override
    public String getName() {
        return "count";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.value(collection.size());
    }

}
