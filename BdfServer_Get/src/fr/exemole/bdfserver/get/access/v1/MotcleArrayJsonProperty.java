/* BdfServer_Get - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access.v1;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import java.util.Collection;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class MotcleArrayJsonProperty implements JsonProperty {

    private final Collection<Motcle> collection;
    private final CellConverter cellConverter;
    private final Lang lang;

    public MotcleArrayJsonProperty(Collection<Motcle> collection, CellConverter cellConverter, Lang lang) {
        this.collection = collection;
        this.cellConverter = cellConverter;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "array";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (Motcle motcle : collection) {
            jw.object();
            AccessJson.properties(jw, motcle, cellConverter, lang);
            jw.endObject();
        }
        jw.endArray();
    }

}
