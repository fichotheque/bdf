/* BdfServer_Get - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access.v1;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class MotcleJsonProperty implements JsonProperty {

    private final Motcle motcle;
    private final CellConverter cellConverter;
    private final Lang lang;

    public MotcleJsonProperty(Motcle motcle, CellConverter cellConverter, Lang lang) {
        this.motcle = motcle;
        this.cellConverter = cellConverter;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "motcle";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        AccessJson.properties(jw, motcle, cellConverter, lang);
        jw.endObject();
    }

}
