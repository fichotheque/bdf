/* BdfServer_Get - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access.v1;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import java.util.Collection;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class FicheArrayJsonProperty implements JsonProperty {

    private final PermissionSummary permissionSummary;
    private final CellConverter cellConverter;
    private final Collection<FicheMeta> collection;

    public FicheArrayJsonProperty(Collection<FicheMeta> collection, PermissionSummary permissionSummary, CellConverter cellConverter) {
        this.collection = collection;
        this.permissionSummary = permissionSummary;
        this.cellConverter = cellConverter;
    }

    @Override
    public String getName() {
        return "array";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (FicheMeta ficheMeta : collection) {
            jw.object();
            AccessJson.properties(jw, ficheMeta, permissionSummary, cellConverter);
            jw.endObject();
        }
        jw.endArray();
    }

}
