/* BdfServer_Get - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access.v1;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.get.access.AccessGate;
import fr.exemole.bdfserver.get.access.AccessJsonProducer;
import fr.exemole.bdfserver.tools.roles.PermissionSummaryBuilder;
import java.util.Collection;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.exportation.access.AccessDef;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class V1EndPointFactory {

    private V1EndPointFactory() {

    }

    public static JsonProducer getJsonProducer(BdfServer bdfServer, RequestMap requestMap, AccessDef accessDef, Lang lang, String endPoint, @Nullable BdfUser bdfUser) throws ErrorMessageException {
        PermissionSummary permissionSummary = null;
        if (bdfUser != null) {
            permissionSummary = PermissionSummaryBuilder.build(bdfServer, bdfUser);
        }
        AccessGate accessGate = AccessGate.build(bdfServer, accessDef, permissionSummary, lang);
        AccessJsonProducer accessJsonProducer = new AccessJsonProducer();
        AccessGateRequestHandler requestHandler = AccessGateRequestHandler.init(accessGate, requestMap);
        switch (endPoint) {
            case "fiche": {
                FicheMeta ficheMeta = requestHandler.getFiche();
                accessJsonProducer.add(new FicheJsonProperty(accessGate.getPermissionSummary(), ficheMeta, accessGate.getCellEngine()));
                break;
            }
            case "fiches": {
                Fiches fiches = requestHandler.getFiches();
                accessJsonProducer.add(new FichesJsonProperty(accessGate.getPermissionSummary(), fiches, accessGate.getCellEngine(), lang));
                break;
            }
            case "thesaurus": {
                Thesaurus thesaurus = requestHandler.getThesaurus();
                accessJsonProducer.add(new ThesaurusJsonProperty(thesaurus, accessGate.getCellEngine(), lang, ThesaurusUtils.checkDisponibility(accessGate.getBdfServer().getThesaurusLangChecker(), thesaurus, lang)));
                break;
            }
            case "fiche-array": {
                Collection<FicheMeta> collection = requestHandler.getFicheArray();
                accessJsonProducer.add(new CountJsonProperty(collection));
                accessJsonProducer.add(new FicheArrayJsonProperty(collection, accessGate.getPermissionSummary(), accessGate.getCellEngine()));
                break;
            }
            case "motcle": {
                Motcle motcle = requestHandler.getMotcle();
                accessJsonProducer.add(new MotcleJsonProperty(motcle, accessGate.getCellEngine(), lang));
                break;
            }
            case "motcle-array": {
                Collection<Motcle> collection = requestHandler.getMotcleArray();
                accessJsonProducer.add(new CountJsonProperty(collection));
                accessJsonProducer.add(new MotcleArrayJsonProperty(collection, accessGate.getCellEngine(), lang));
                break;
            }
        }
        if (accessJsonProducer.isEmpty()) {
            return null;
        } else {
            return accessJsonProducer;
        }
    }


}
