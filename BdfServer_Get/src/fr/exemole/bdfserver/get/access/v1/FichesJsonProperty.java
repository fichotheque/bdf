/* BdfServer_Get - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access.v1;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class FichesJsonProperty implements JsonProperty {

    private final PermissionSummary permissionSummary;
    private final Fiches fiches;
    private final CellEngine cellEngine;
    private final Lang lang;

    public FichesJsonProperty(PermissionSummary permissionSummary, Fiches fiches, CellEngine cellEngine, Lang lang) {
        this.fiches = fiches;
        this.lang = lang;
        this.cellEngine = cellEngine;
        this.permissionSummary = permissionSummary;
    }

    @Override
    public String getName() {
        return "fiches";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        AccessJson.properties(jw, fiches, permissionSummary, cellEngine, lang);
        jw.endObject();
    }

}
