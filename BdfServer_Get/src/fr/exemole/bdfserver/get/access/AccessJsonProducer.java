/* BdfServer_Get - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.get.access;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class AccessJsonProducer implements JsonProducer {

    private final List<JsonProperty> partList = new ArrayList<JsonProperty>();

    public AccessJsonProducer() {

    }

    public boolean isEmpty() {
        return partList.isEmpty();
    }

    public void add(JsonProperty jsonProperty) {
        partList.add(jsonProperty);
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        for (JsonProperty jsonProperty : partList) {
            jw.key(jsonProperty.getName());
            jsonProperty.writeValue(jw);
        }
        jw.endObject();
    }

}
