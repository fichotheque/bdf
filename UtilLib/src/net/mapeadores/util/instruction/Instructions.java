/* UtilLib - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.instruction;

import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.instruction.Instructions.Part;


/**
 *
 * @author Vincent Calame
 */
public interface Instructions extends List<Part>, RandomAccess {


    public static interface Part {

    }


    public static interface LiteralPart extends Part {

        public String getText();

    }


    public static interface InstructionPart extends Part {

        public Instruction getInstruction();

    }

}
