/* UtilLib - Copyright (c) 2007-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.instruction;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;


/**
 * La liste ne doit pas être vide
 *
 * @author Vincent Calame
 */
public interface Instruction extends List<Argument>, RandomAccess {

    public default Map<String, Object> toOptionMap(int offset) {
        int size = size();
        if (offset >= size) {
            return Collections.emptyMap();
        }
        Map<String, Object> result = new LinkedHashMap<String, Object>();
        for (int i = offset; i < size; i++) {
            Argument argument = get(i);
            String optionName = argument.getKey();
            String value = argument.getValue();
            Object optionValue;
            if (value == null) {
                if (optionName.startsWith("!")) {
                    optionName = optionName.substring(1);
                    optionValue = Boolean.FALSE;
                } else {
                    optionValue = Boolean.TRUE;
                }
            } else {
                try {
                    optionValue = Integer.parseInt(value);
                } catch (NumberFormatException nfe) {
                    optionValue = value;
                }
            }
            result.put(optionName, optionValue);
        }
        return result;
    }

}
