/* UtilLib - Copyright (c) 2012-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.instruction;

import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public interface InstructionResolver {

    /**
     * Retourne null si l'instance de InstructionResolver ne gère pas
     * l'instruction en argument. Envoie une exception si l'instruction n'est
     * sont pas correctes
     */
    public Object resolve(Instruction instruction) throws ErrorMessageException;

}
