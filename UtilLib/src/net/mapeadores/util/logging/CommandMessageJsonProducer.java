/* UtilLib - Copyright (c) 2010-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.io.IOException;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 *
 * @author Vincent Calame
 */
public class CommandMessageJsonProducer implements JsonProducer {

    private final CommandMessage[] commandMessages;
    private final MessageLocalisation messageLocalisation;

    public CommandMessageJsonProducer(MessageLocalisation messageLocalisation, CommandMessage... commandMessages) {
        if (commandMessages.length == 0) {
            throw new IllegalArgumentException("commandMessages is empty");
        }
        this.commandMessages = commandMessages;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        int length = commandMessages.length;
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("commandMessage");
            CommonJson.object(jw, commandMessages[length - 1], messageLocalisation);
            if (length > 1) {
                jw.key("otherCommandMessageArray");
                jw.array();
                for (int i = 0; i < (length - 1); i++) {
                    CommonJson.object(jw, commandMessages[i], messageLocalisation);
                }
                jw.endArray();
            }
        }
        jw.endObject();
    }

}
