/* UtilLib - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.io.IOException;
import java.io.StringReader;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.TransformerException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.instruction.InstructionErrorHandler;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 *
 * @author Vincent Calame
 */
public final class LogUtils {

    public final static MessageLog EMPTY_MESSAGELOG = new EmptyMessageLog();
    public final static List<MessageSource> EMPTY_MESSAGESOURCELIST = Collections.emptyList();
    public final static MultiMessageHandler NULL_MULTIMESSAGEHANDLER = new NullMultiMessageHandler();
    public final static LineMessageHandler NULL_LINEMESSAGEHANDLER = new NullLineMessageHandler();
    public final static List<MessageByLine> EMPTY_MESSAGEBYLINELIST = Collections.emptyList();
    public final static List<Message> EMPTY_MESSAGELIST = Collections.emptyList();
    private final static Object[] EMPTY = new Object[0];

    private LogUtils() {
    }

    public static LineMessage toLineMessage(int lineNumber, String messageKey, Object... messageValues) {
        if (messageKey == null) {
            throw new IllegalArgumentException("messageKey is null");
        }
        if (messageValues == null) {
            messageValues = EMPTY;
        }
        return new InternalLineMessage(lineNumber, messageKey, messageValues);
    }

    public static LineMessage toLineMessage(int lineNumber, Message message) {
        return new InternalLineMessage(lineNumber, message.getMessageKey(), message.getMessageValues());
    }

    public static String toString(MessageSource messageSource) {
        StringBuilder buf = new StringBuilder();
        try {
            append(buf, messageSource);
        } catch (IOException ioe) {
        }
        return buf.toString();
    }

    public static String toString(MessageLog messageLog) {
        StringBuilder buf = new StringBuilder();
        try {
            for (MessageSource messageSource : messageLog.getMessageSourceList()) {
                append(buf, messageSource);
            }
        } catch (IOException ioe) {
        }
        return buf.toString();
    }

    public static String toXmlString(MessageLog messageLog) {
        StringBuilder buf = new StringBuilder();
        try {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            XMLUtils.appendXmlDeclaration(xmlWriter);
            MessageXMLPart messageXMLPart = new MessageXMLPart(xmlWriter);
            messageXMLPart.addLogs(messageLog);
        } catch (IOException ioe) {

        }
        return buf.toString();
    }

    public static String toXmlString(MessageSource messageSource) {
        StringBuilder buf = new StringBuilder();
        try {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            XMLUtils.appendXmlDeclaration(xmlWriter);
            MessageXMLPart messageXMLPart = new MessageXMLPart(xmlWriter);
            messageXMLPart.addMessageSource(messageSource);
        } catch (IOException ioe) {

        }
        return buf.toString();
    }


    public static MessageSource readMessageSource(String name, String xmlString) {
        MessageSourceBuilder messageSourceBuilder = new MessageSourceBuilder(name);
        DocumentBuilder docBuilder = DOMUtils.newDocumentBuilder();
        try {
            Document document = docBuilder.parse(new InputSource(new StringReader(xmlString)));
            MessageDOMReader reader = new MessageDOMReader(messageSourceBuilder);
            reader.readMessageSource(messageSourceBuilder, document.getDocumentElement(), "");
            return messageSourceBuilder.toMessageSource();
        } catch (SAXException saxe) {
            DomMessages.saxException(messageSourceBuilder, saxe);
            return messageSourceBuilder.toMessageSource();
        } catch (IOException ioe) {
            throw new ShouldNotOccurException(ioe);
        }
    }

    public static MessageLog readMessageLog(String xmlString) {
        SimpleMessageHandler simpleHandler = new SimpleMessageHandler();
        MessageLogBuilder logBuilder = new MessageLogBuilder();
        DocumentBuilder docBuilder = DOMUtils.newDocumentBuilder();
        try {
            Document document = docBuilder.parse(new InputSource(new StringReader(xmlString)));
            MessageDOMReader reader = new MessageDOMReader(simpleHandler);
            reader.readMessageLog(logBuilder, document.getDocumentElement(), "");
        } catch (SAXException saxe) {
            logBuilder.setCurrentSource("logfile");
            DomMessages.saxException(logBuilder, saxe);
        } catch (IOException ioe) {
            throw new ShouldNotOccurException(ioe);
        }
        return logBuilder.toMessageLog();
    }

    public static boolean printHtml(HtmlPrinter hp, MessageLog messageLog, String listCssClasses) {
        if (messageLog.isEmpty()) {
            return false;
        }
        hp
                .UL(listCssClasses);
        for (MessageSource messageSource : messageLog.getMessageSourceList()) {
            List<MessageSource.Entry> messageList = messageSource.getEntryList();
            if (!messageList.isEmpty()) {
                hp
                        .LI();
                hp
                        .P()
                        .CODE("cm-link")
                        .__escape(messageSource.getName())
                        ._CODE()
                        ._P();
                hp
                        .UL();
                for (MessageSource.Entry entry : messageList) {
                    Message message = entry.getMessage();
                    String category = entry.getCategory();
                    hp
                            .LI()
                            .P()
                            .CODE("cm-qualifier")
                            .__escape('[')
                            .__escape(category)
                            .__escape(']')
                            ._CODE()
                            .__space()
                            .SPAN("cm-quote")
                            .__localize(message)
                            ._SPAN()
                            ._P()
                            ._LI();
                }
                hp
                        ._UL();
                hp
                        ._LI();
            }
        }
        hp
                ._UL();
        return true;
    }

    public static void writeJsonValue(JSONWriter jw, MessageLog messageLog) throws IOException {
        jw.array();
        for (MessageSource messageSource : messageLog.getMessageSourceList()) {
            List<MessageSource.Entry> entryList = messageSource.getEntryList();
            if (!entryList.isEmpty()) {
                jw.object();
                {
                    jw.key("name")
                            .value(messageSource.getName());
                    jw.key("uri")
                            .value(messageSource.getName());
                    jw.key("messageArray");
                    jw.array();
                    for (MessageSource.Entry entry : entryList) {
                        Message message = entry.getMessage();
                        String category = entry.getCategory();
                        jw.object();
                        {
                            jw.key("category")
                                    .value(category);
                            jw.key("key")
                                    .value(message.getMessageKey());
                            Object[] values = message.getMessageValues();
                            if (values.length > 0) {
                                jw.key("valueArray");
                                jw.array();
                                for (Object obj : values) {
                                    jw.value(obj.toString());
                                }
                                jw.endArray();
                            }
                        }
                        jw.endObject();
                    }
                    jw.endArray();
                }
                jw.endObject();
            }
        }
        jw.endArray();
    }

    public static void append(Appendable buf, MessageSource messageSource) throws IOException {
        String sourceName = messageSource.getName();
        List<MessageSource.Entry> messageList = messageSource.getEntryList();
        if (messageList.isEmpty()) {
            return;
        }
        boolean withTabs = false;
        if (sourceName != null) {
            buf.append("[[");
            buf.append(sourceName);
            buf.append("]]");
            buf.append("\n");
            withTabs = true;
        }
        for (MessageSource.Entry entry : messageList) {
            Message message = entry.getMessage();
            String category = entry.getCategory();
            if (withTabs) {
                buf.append("      ");
            }
            buf.append("[");
            buf.append(category);
            buf.append("] ");
            buf.append(message.getMessageKey());
            Object[] values = message.getMessageValues();
            int length = values.length;
            if (length > 0) {
                buf.append(" = ");
                for (int i = 0; i < length; i++) {
                    if (i > 0) {
                        buf.append(" | ");
                    }
                    buf.append(values[i].toString());
                }
            }
            buf.append("\n");
        }
    }

    public static InstructionErrorHandler encapsulate(LineMessageHandler lineMessageHandler, String category, int lineNumber) {
        return new LineInstructionErrorHandler(lineMessageHandler, category, lineNumber);
    }

    public static List<MessageSource> wrap(MessageSource[] array) {
        return new MessageSourceList(array);
    }

    public static List<MessageSource.Entry> wrap(MessageSource.Entry[] array) {
        return new MessageSourceEntryList(array);
    }

    public static List<MessageByLine> wrap(MessageByLine[] array) {
        return new MessageByLineList(array);
    }

    public static void handleSAXException(String category, SAXException saxException, LineMessageHandler lineMessageHandler) {
        if (saxException instanceof SAXParseException) {
            SAXParseException spe = (SAXParseException) saxException;
            lineMessageHandler.addMessage(spe.getLineNumber(), category, "_ error.exception.xml.sax_parse", saxException.getMessage(), String.valueOf(spe.getLineNumber()), String.valueOf(spe.getColumnNumber()));
        } else {
            lineMessageHandler.addMessage(0, category, "_ error.exception.xml.sax", saxException.getMessage());
        }
    }

    /**
     * Traitement particulier car sourceLocator n'est pas toujours défini par
     * Xalan
     *
     * @param category
     * @param transformerException
     * @param systemId
     * @param lineMessageHandler
     */
    public static void handleTransformerException(String category, TransformerException transformerException, String systemId, LineMessageHandler lineMessageHandler) {
        SourceLocator sourceLocator = transformerException.getLocator();
        boolean done = false;
        if (sourceLocator != null) {
            String sourceSystemId = sourceLocator.getSystemId();
            int lineNumber = sourceLocator.getLineNumber();
            if ((lineNumber != -1) && ((sourceSystemId == null) || (sourceSystemId.equals(systemId)))) {
                lineMessageHandler.addMessage(lineNumber, category, "_ error.exception.xml.xslt", transformerException.getMessage());
                done = true;
            }
        }
        if (done) {
            return;
        }
        String message = transformerException.getMessage().trim();
        int idx = message.indexOf("line ");
        if (idx > - 1) {
            String end = message.substring(idx + 5).trim();
            String number = end;
            int spaceIdx = end.indexOf(":");
            if (spaceIdx > -1) {
                number = end.substring(0, spaceIdx);
                end = end.substring(spaceIdx + 1).trim();
            }
            try {
                int lineNumber = Integer.parseInt(number);
                String currentSystemId = message.substring(0, idx).trim();
                if (currentSystemId.endsWith(":")) {
                    currentSystemId = currentSystemId.substring(0, currentSystemId.length() - 1).trim();
                }
                if ((currentSystemId.isEmpty()) || (currentSystemId.equals(systemId))) {
                    lineMessageHandler.addMessage(lineNumber, category, "_ error.exception.xml.xslt", end);
                    done = true;
                }
            } catch (NumberFormatException nfe) {

            }
        }
        if (!done) {
            lineMessageHandler.addMessage(0, category, "_ error.exception.xml.xslt", transformerException.getMessageAndLocation());
        }
    }

    public static CommandMessage error(String messageKey, Object... messageValues) {
        if (messageKey == null) {
            throw new IllegalArgumentException("messageKey is null");
        }
        if (messageValues == null) {
            messageValues = EMPTY;
        }
        return new SimpleCommandMessage(messageKey, messageValues, true);
    }

    public static CommandMessage error(Message message) {
        Object[] messageValues = message.getMessageValues();
        if (messageValues == null) {
            messageValues = EMPTY;
        }
        return new SimpleCommandMessage(message.getMessageKey(), messageValues, true);
    }

    /*public static CommandMessage error(Message message, List<Message> multiErrorList) {
        CommandMessage commandMessage = new CommandMessage(message);
        commandMessage.initIsErrorMessage(true);
        commandMessage.initMultiErrorList(multiErrorList);
        return commandMessage;
    }*/

    public static CommandMessage done(String messageKey, Object... messageValues) {
        if (messageKey == null) {
            throw new IllegalArgumentException("messageKey is null");
        }
        if (messageValues == null) {
            messageValues = EMPTY;
        }
        return new SimpleCommandMessage(messageKey, messageValues, false);
    }

    public static CommandMessage done(Message message) {
        Object[] messageValues = message.getMessageValues();
        if (messageValues == null) {
            messageValues = EMPTY;
        }
        return new SimpleCommandMessage(message.getMessageKey(), messageValues, false);
    }


    public static class ShouldNotOccurMessageHandler implements MessageHandler {

        public ShouldNotOccurMessageHandler() {
        }

        @Override
        public void addMessage(String category, Message message) {
            StringBuilder buf = new StringBuilder();
            buf.append(category);
            buf.append(" / ");
            buf.append(message.getMessageKey());
            Object[] values = message.getMessageValues();
            for (Object value : values) {
                buf.append(" / ");
                buf.append(value);
            }
            throw new ShouldNotOccurException(buf.toString());
        }

    }


    /*public static CommandMessage done(Message message, MessageLog processMessageLog) {
        CommandMessage commandMessage = new CommandMessage(message);
        commandMessage.initMessageLog(processMessageLog);
        return commandMessage;
    }*/
    private static class MessageSourceList extends AbstractList<MessageSource> implements RandomAccess {

        private final MessageSource[] array;

        private MessageSourceList(MessageSource[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MessageSource get(int index) {
            return array[index];
        }

    }


    private static class InternalLineMessage implements LineMessage {

        private final String messageKey;
        private final Object[] messageValues;
        private final int lineNumber;

        private InternalLineMessage(int lineNumber, String messageKey, Object[] messageValues) {
            this.lineNumber = lineNumber;
            this.messageKey = messageKey;
            this.messageValues = messageValues;
        }

        @Override
        public int getLineNumber() {
            return lineNumber;
        }

        @Override
        public String getMessageKey() {
            return messageKey;
        }

        @Override
        public Object[] getMessageValues() {
            return messageValues;
        }

    }


    private static class EmptyMessageLog implements MessageLog {

        private EmptyMessageLog() {
        }

        @Override
        public List<MessageSource> getMessageSourceList() {
            return EMPTY_MESSAGESOURCELIST;
        }

    }


    private static class NullMultiMessageHandler implements MultiMessageHandler {

        private NullMultiMessageHandler() {
        }

        @Override
        public void addMessage(String category, Message message) {
        }

        @Override
        public void addMessage(String category, String messageKey, Object... messageValues) {

        }

        @Override
        public void setCurrentSource(String uri) {
        }

    }


    private static class NullLineMessageHandler implements LineMessageHandler {

        private NullLineMessageHandler() {
        }

        @Override
        public void addMessage(String category, LineMessage lineMessage) {
        }

        @Override
        public void addMessage(int lineNumber, String category, String messageKey, Object... messageValues) {

        }

    }


    private static class LineInstructionErrorHandler implements InstructionErrorHandler {

        private final String category;
        private final LineMessageHandler lineMessageHandler;
        private final int lineNumber;

        private LineInstructionErrorHandler(LineMessageHandler lineMessageHandler, String category, int lineNumber) {
            this.lineMessageHandler = lineMessageHandler;
            this.category = category;
            this.lineNumber = lineNumber;
        }

        @Override
        public void invalidAsciiCharacterError(String part, int row, int col) {
            addMessage("_ error.wrong.character_notascii", part, row, col);
        }

        @Override
        public void invalidEndCharacterError(String part, int row, int col) {
            addMessage("_ error.wrong.character_end", part, row, col);
        }

        @Override
        public void invalidSeparatorCharacterError(String part, int row, int col) {
            addMessage("_ error.wrong.character_separator", part, row, col);
        }

        private void addMessage(String messageKey, String part, int row, int col) {
            lineMessageHandler.addMessage(lineNumber, category, messageKey, part, row, col);
        }

    }


    private static class MessageSourceEntryList extends AbstractList<MessageSource.Entry> implements RandomAccess {

        private final MessageSource.Entry[] array;

        private MessageSourceEntryList(MessageSource.Entry[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MessageSource.Entry get(int index) {
            return array[index];
        }

    }


    private static class MessageByLineList extends AbstractList<MessageByLine> implements RandomAccess {

        private final MessageByLine[] array;

        private MessageByLineList(MessageByLine[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MessageByLine get(int index) {
            return array[index];
        }

    }


    private static class SimpleCommandMessage implements CommandMessage {

        private final String messageKey;
        private final Object[] messageValues;
        private final boolean isError;

        private SimpleCommandMessage(String messageKey, Object[] messageValues, boolean isError) {
            this.messageKey = messageKey;
            this.messageValues = messageValues;
            this.isError = isError;
        }

        @Override
        public String getMessageKey() {
            return messageKey;
        }

        @Override
        public Object[] getMessageValues() {
            return messageValues;
        }

        @Override
        public boolean isErrorMessage() {
            return isError;
        }

        @Override
        public List<Message> getMultiErrorList() {
            return EMPTY_MESSAGELIST;
        }


        @Override
        public MessageLog getMessageLog() {
            return null;
        }

    }

}
