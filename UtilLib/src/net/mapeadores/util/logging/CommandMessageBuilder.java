/* UtilLib - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class CommandMessageBuilder {

    private List<Message> multiErrorList;
    private MessageLog messageLog;
    private Message mainMessage;
    private boolean isError;

    public CommandMessageBuilder() {

    }

    public CommandMessageBuilder setError(String messageKey, Object... messageValues) {
        this.mainMessage = LocalisationUtils.toMessage(messageKey, messageValues);
        isError = true;
        return this;
    }

    public CommandMessageBuilder setError(Message message) {
        this.mainMessage = message;
        isError = true;
        return this;
    }

    public CommandMessageBuilder setDone(String messageKey, Object... messageValues) {
        this.mainMessage = LocalisationUtils.toMessage(messageKey, messageValues);
        isError = false;
        return this;
    }

    public CommandMessageBuilder setDone(Message message) {
        this.mainMessage = message;
        isError = false;
        return this;
    }

    public CommandMessageBuilder addMultiError(Message message) {
        if (multiErrorList == null) {
            multiErrorList = new ArrayList<Message>();
        }
        multiErrorList.add(message);
        return this;
    }

    public CommandMessageBuilder addMultiError(String messageKey, Object... messageValues) {
        if (multiErrorList == null) {
            multiErrorList = new ArrayList<Message>();
        }
        multiErrorList.add(LocalisationUtils.toMessage(messageKey, messageValues));
        return this;
    }

    public CommandMessageBuilder addLog(MessageLog messageLog) {
        this.messageLog = messageLog;
        return this;
    }

    public boolean hasMultiError() {
        return (multiErrorList != null);
    }


    public CommandMessage toCommandMessage() {
        Message finalMainMessage;
        if (mainMessage != null) {
            finalMainMessage = mainMessage;
        } else if (multiErrorList != null) {
            finalMainMessage = multiErrorList.get(0);
            isError = true;
        } else {
            throw new IllegalStateException("mainMessage is not defined");
        }
        List<Message> finalList;
        if (multiErrorList == null) {
            finalList = LogUtils.EMPTY_MESSAGELIST;
        } else {
            finalList = multiErrorList;
        }
        return new InternalCommandMessage(finalMainMessage, isError, finalList, messageLog);
    }

    public static CommandMessageBuilder init() {
        return new CommandMessageBuilder();
    }


    private static class InternalCommandMessage implements CommandMessage {

        private final Message mainMessage;
        private final boolean isError;
        private final List<Message> errorList;
        private final MessageLog messageLog;

        private InternalCommandMessage(Message mainMessage, boolean isError, List<Message> errorList, MessageLog messageLog) {
            this.mainMessage = mainMessage;
            this.isError = isError;
            this.errorList = errorList;
            this.messageLog = messageLog;
        }

        @Override
        public String getMessageKey() {
            return mainMessage.getMessageKey();
        }

        @Override
        public Object[] getMessageValues() {
            return mainMessage.getMessageValues();
        }

        @Override
        public boolean isErrorMessage() {
            return isError;
        }

        @Override
        public List<Message> getMultiErrorList() {
            return errorList;
        }


        @Override
        public MessageLog getMessageLog() {
            return messageLog;
        }

    }

}
