/* UtilLib - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.List;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class SimpleMessageHandler implements MessageHandler {

    private final MessageSourceBuilder builder = new MessageSourceBuilder("");

    public SimpleMessageHandler() {
    }

    @Override
    public void addMessage(String category, Message message) {
        builder.addMessage(category, message);
    }

    public boolean hasMessage() {
        return (!builder.isEmpty());
    }

    public List<MessageSource.Entry> toList() {
        return builder.toMessageSource().getEntryList();
    }

    public List<Message> filter(String category) {
        return builder.toMessageList(category);
    }

    public List<Message> filterWarning() {
        return builder.toMessageList("warning.");
    }

    public List<Message> filterSevere() {
        return builder.toMessageList("severe.");
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        List<MessageSource.Entry> list = builder.toMessageSource().getEntryList();
        for (MessageSource.Entry entry : list) {
            Message message = entry.getMessage();
            if (buf.length() > 0) {
                buf.append("\n");
            }
            buf.append(message.getMessageKey());
            Object[] values = message.getMessageValues();
            int length = values.length;
            if (length > 0) {
                for (int i = 0; i < length; i++) {
                    if (i > 0) {
                        buf.append(" / ");
                    } else {
                        buf.append(": ");
                    }
                    buf.append(values[i].toString());
                }
            }
        }
        return buf.toString();
    }

}
