/* UtilLib - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.io.IOException;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class MessageXMLPart extends XMLPart {

    public MessageXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addLogs(MessageLog messageLog) throws IOException {
        openTag("logs");
        for (MessageSource messageSource : messageLog.getMessageSourceList()) {
            addMessageSource(messageSource);
        }
        closeTag("logs");
    }

    public void addMessageSource(MessageSource messageSource) throws IOException {
        String name = messageSource.getName();
        startOpenTag("log");
        addAttribute("name", name);
        addAttribute("uri", name);
        endOpenTag();
        for (MessageSource.Entry entry : messageSource.getEntryList()) {
            addMessage(entry.getMessage(), entry.getCategory());
        }
        closeTag("log");
    }

    public void addMessage(Message message) throws IOException {
        addMessage(message, null);
    }

    private void addMessage(Message message, String category) throws IOException {
        startOpenTag("message");
        addAttribute("category", category);
        addAttribute("key", message.getMessageKey());
        Object[] values = message.getMessageValues();
        if (values.length == 0) {
            closeEmptyTag();
        } else {
            endOpenTag();
            for (Object val : values) {
                addSimpleElement("value", val.toString());
            }
            closeTag("message");
        }
    }

}
