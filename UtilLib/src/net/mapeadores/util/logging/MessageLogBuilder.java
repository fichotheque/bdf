/* UtilLib - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class MessageLogBuilder implements MultiMessageHandler {

    private final Map<String, MessageSourceBuilder> builderMap = new LinkedHashMap<String, MessageSourceBuilder>();
    private MessageSourceBuilder currentBuffer = null;

    public MessageLogBuilder() {
    }

    @Override
    public void setCurrentSource(String sourceName) {
        MessageSourceBuilder builder = builderMap.get(sourceName);
        if (builder == null) {
            builder = new MessageSourceBuilder(sourceName);
            builderMap.put(sourceName, builder);
        }
        currentBuffer = builder;
    }

    @Override
    public void addMessage(String category, Message message) {
        if (currentBuffer == null) {
            setCurrentSource("");
        }
        currentBuffer.addMessage(category, message);
    }

    public MessageLog toMessageLog() {
        List<MessageSource> tempList = new ArrayList<MessageSource>();
        for (MessageSourceBuilder builder : builderMap.values()) {
            MessageSource messageSource = builder.toMessageSource();
            if (!messageSource.isEmpty()) {
                tempList.add(messageSource);
            }
        }
        List<MessageSource> finalList = LogUtils.wrap(tempList.toArray(new MessageSource[tempList.size()]));
        return new InternalMessageLog(finalList);
    }


    private static class InternalMessageLog implements MessageLog {

        private final List<MessageSource> list;

        private InternalMessageLog(List<MessageSource> list) {
            this.list = list;
        }

        @Override
        public List<MessageSource> getMessageSourceList() {
            return list;
        }

    }

}
