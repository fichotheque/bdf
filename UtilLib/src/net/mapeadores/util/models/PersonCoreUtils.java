/* FichothequeLib_API - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.models;


/**
 *
 * @author Vincent Calame
 */
public class PersonCoreUtils {

    public final static PersonCore EMPTY_PERSONCORE = new InternalPersonCore("", "", "", false);

    private PersonCoreUtils() {
    }

    public static PersonCore toPersonCore(String surname, String forename, String nonlatin, boolean surnameFirst) {
        if (surname == null) {
            surname = "";
        }
        if (forename == null) {
            forename = "";
        }
        if (nonlatin == null) {
            nonlatin = "";
        }
        return new InternalPersonCore(surname, forename, nonlatin, surnameFirst);
    }

    public static PersonCore clonePersonCore(PersonCore personCore) {
        return new InternalPersonCore(personCore);
    }

    public static boolean areEqual(PersonCore pc1, PersonCore pc2) {
        if (pc2.isSurnameFirst() != pc1.isSurnameFirst()) {
            return false;
        }
        if (!pc2.getSurname().equals(pc1.getSurname())) {
            return false;
        }
        if (!pc2.getForename().equals(pc1.getForename())) {
            return false;
        }
        if (!pc2.getNonlatin().equals(pc1.getNonlatin())) {
            return false;
        }
        return true;
    }


    private static class InternalPersonCore implements PersonCore {

        private final String surname;
        private final String forename;
        private final String nonlatin;
        private final boolean surnameFirst;

        private InternalPersonCore(String surname, String forename, String nonlatin, boolean surnameFirst) {
            this.surname = surname;
            this.forename = forename;
            this.nonlatin = nonlatin;
            this.surnameFirst = surnameFirst;
        }

        private InternalPersonCore(PersonCore personCore) {
            this.surname = personCore.getSurname();
            this.forename = personCore.getForename();
            this.nonlatin = personCore.getNonlatin();
            this.surnameFirst = personCore.isSurnameFirst();
        }

        @Override
        public String getSurname() {
            return surname;
        }

        @Override
        public String getForename() {
            return forename;
        }

        @Override
        public String getNonlatin() {
            return nonlatin;
        }

        @Override
        public boolean isSurnameFirst() {
            return surnameFirst;
        }

    }

}
