/* UtilLib - Copyright (c) 2010-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io.docstream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.mimetype.MimeTypeConstants;


/**
 *
 * @author Vincent Calame
 */
public class StringDocStream implements DocStream {

    private String mimeType = MimeTypeConstants.PLAIN;
    private byte[] byteArray;

    public StringDocStream(String s) {
        this(s, MimeTypeConstants.PLAIN);
    }

    public StringDocStream(String s, String mimeType) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
        try {
            byteArray = s.getBytes("UTF-8");
        } catch (UnsupportedEncodingException uee) {
            throw new ShouldNotOccurException(uee);
        }
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(byteArray);
    }

    @Override
    public int getLength() {
        return byteArray.length;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
    }

    @Override
    public String getCharset() {
        return "UTF-8";
    }

    @Override
    public long getLastModified() {
        return -1;
    }

}
