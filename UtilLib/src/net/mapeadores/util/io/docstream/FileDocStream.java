/* UtilLib - Copyright (c) 2010-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io.docstream;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.mimetype.MimeTypeUtils;


/**
 *
 * @author Vincent Calame
 */
public class FileDocStream implements DocStream {

    private final File f;
    private String mimeType = MimeTypeConstants.OCTETSTREAM;
    private String charset = null;

    public FileDocStream(File f) {
        this.f = f;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new FileInputStream(f);
    }

    @Override
    public int getLength() {
        return (int) f.length();
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
        this.charset = MimeTypeUtils.getDefaultCharset(mimeType);
    }

    public void setMimeType(String mimeType, String charset) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
        this.charset = charset;
    }

    @Override
    public String getCharset() {
        return charset;
    }

    @Override
    public long getLastModified() {
        long date = f.lastModified();
        if (date == 0) {
            return -1;
        }
        return date;
    }

}
