/* UtilLib - Copyright (c) 2006-2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.io.IOException;
import java.io.InputStream;
import net.mapeadores.util.exceptions.NestedIOException;


/**
 *
 * @author Vincent Calame
 */
public interface DocStream {

    public InputStream getInputStream() throws IOException;

    public String getMimeType();

    /**
     * Obligatoire s'il s'agit de texte. Si la valeur est nulle, on considère
     * que le format du fichier est du binaire
     */
    public String getCharset();

    /**
     * Retourne -1 si la taille est inconnue
     */
    public int getLength();

    /**
     * Retourne -1 si la date de dernière modification est inconnue
     */
    public long getLastModified();

    public default String getContent() {
        String charset = getCharset();
        if (charset == null) {
            return "";
        }
        try (InputStream is = getInputStream()) {
            return IOUtils.toString(is, charset);
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
    }

}
