/* UtilLib - Copyright (c) 2008-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.transform.ErrorListener;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.TransformerException;
import net.mapeadores.util.exceptions.ExceptionsUtils;
import org.xml.sax.SAXParseException;


/**
 *
 * @author Vincent Calame
 */
public class BufferErrorListener implements ErrorListener {

    private final List<TransformerException> errorList = new ArrayList<TransformerException>();
    private final List<TransformerException> warningList = new ArrayList<TransformerException>();
    private final Set<String> messageSet = new HashSet<String>();

    public BufferErrorListener() {
    }

    public boolean hasError() {
        return !errorList.isEmpty();
    }

    public boolean hasWarning() {
        return !warningList.isEmpty();
    }

    public String getErrorMessage() {
        StringBuilder buf = new StringBuilder();
        for (TransformerException te : errorList) {
            if (buf.length() > 0) {
                buf.append(" / ");
            }
            buf.append(getTransformerMessage(te));
        }

        return buf.toString();
    }

    public String getWarningMessage() {
        StringBuilder buf = new StringBuilder();
        for (TransformerException te : warningList) {
            if (buf.length() > 0) {
                buf.append(" / ");
            }
            buf.append(getTransformerMessage(te));
        }

        return buf.toString();
    }

    public List<TransformerException> getErrorList() {
        return errorList;
    }

    public List<TransformerException> getWarningList() {
        return warningList;
    }

    @Override
    public void error(TransformerException transformerException) throws TransformerException {
        addError(transformerException);
    }

    @Override
    public void fatalError(TransformerException transformerException) throws TransformerException {
        addError(transformerException);
        throw transformerException;
    }

    @Override
    public void warning(TransformerException transformerException) throws TransformerException {
        addWarning(transformerException);
    }

    public void addError(TransformerException transformerException) {
        String message = transformerException.getMessageAndLocation();
        if (!messageSet.contains(message)) {
            messageSet.add(message);
            errorList.add(transformerException);
        }
    }

    public void addWarning(TransformerException transformerException) {
        String message = transformerException.getMessageAndLocation();
        if (!messageSet.contains(message)) {
            messageSet.add(message);
            warningList.add(transformerException);
        }
    }

    private String getTransformerMessage(TransformerException te) {
        Throwable e = te.getCause();
        if ((e != null) && (e instanceof TransformerException)) {
            return getTransformerMessage((TransformerException) e);
        }
        StringBuilder buf = new StringBuilder();
        SourceLocator sourceLocator = te.getLocator();
        if (sourceLocator != null) {
            ExceptionsUtils.append(buf, sourceLocator.getPublicId(), sourceLocator.getSystemId(), sourceLocator.getLineNumber(), sourceLocator.getColumnNumber());
        } else if ((e != null) && (e instanceof SAXParseException)) {
            SAXParseException spe = (SAXParseException) e;
            ExceptionsUtils.append(buf, spe);
        }
        if (e != null) {
            buf.append(e.getLocalizedMessage());
        } else {
            buf.append(te.getMessageAndLocation());
        }
        return buf.toString();
    }

}
