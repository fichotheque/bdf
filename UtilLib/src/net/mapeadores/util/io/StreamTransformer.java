/* UtilLib - Copyright (c) 2006-202 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.io.OutputStream;
import java.util.Map;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;


/**
 *
 * @author Vincent Calame
 */
public interface StreamTransformer {

    public String getMimeType();

    public String getExtension();

    public void transform(Source source, OutputStream outputStream, Map<String, Object> transformerParameters, Map<String, String> outputProperties) throws TransformerException;

}
