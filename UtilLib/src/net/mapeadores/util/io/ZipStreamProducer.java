/* UtilLib - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.ZipOutputStream;


/**
 *
 * @author Vincent Calame
 */
public abstract class ZipStreamProducer implements StreamProducer {

    private final String fileName;

    public ZipStreamProducer() {
        this.fileName = null;
    }

    public ZipStreamProducer(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String getMimeType() {
        return "application/zip";
    }

    @Override
    public String getCharset() {
        return null;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        try (ZipOutputStream os = new ZipOutputStream(outputStream)) {
            writeZipStream(os);
        }
    }

    public abstract void writeZipStream(ZipOutputStream zipOutputStream) throws IOException;

}
