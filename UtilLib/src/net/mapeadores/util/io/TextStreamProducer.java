/* UtilLib - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import net.mapeadores.util.mimetype.MimeTypeConstants;


/**
 *
 * @author Vincent Calame
 */
public class TextStreamProducer implements StreamProducer {

    private final String text;
    private final String charset;
    private final String fileName;

    public TextStreamProducer(String text) {
        this(text, null, "UTF-8");
    }

    public TextStreamProducer(String text, String fileName) {
        this(text, fileName, "UTF-8");
    }

    public TextStreamProducer(String text, String fileName, String charset) {
        if (text == null) {
            throw new IllegalArgumentException("text is null");
        }
        if (charset == null) {
            throw new IllegalArgumentException("charset is null");
        }
        this.text = text;
        this.fileName = fileName;
        this.charset = charset;
    }

    @Override
    public String getMimeType() {
        return MimeTypeConstants.PLAIN;
    }

    @Override
    public String getCharset() {
        return charset;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        IOUtils.copy(new StringReader(text), outputStream, charset);
    }

}
