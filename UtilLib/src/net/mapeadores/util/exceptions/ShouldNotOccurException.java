/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;


/**
 *
 * @author Vincent Calame
 */
public class ShouldNotOccurException extends RuntimeException {

    public ShouldNotOccurException() {
    }

    public ShouldNotOccurException(String message) {
        super(message);
    }

    public ShouldNotOccurException(String message, Exception e) {
        super(e.getClass().getName() + " cannot occur : " + message, e);
    }

    public ShouldNotOccurException(Exception e) {
        super(e.getClass().getName() + " cannot occur", e);
    }

}
