/* UtilLib - Copyright (c) 2007-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;

import javax.xml.transform.TransformerException;
import javax.xml.transform.SourceLocator;
import org.xml.sax.SAXParseException;


/**
 *
 * @author Vincent Calame
 */
public class NestedTransformerException extends RuntimeException {

    private TransformerException transformerException;

    public NestedTransformerException(TransformerException te) {
        super(te);
        this.transformerException = te;
    }

    @Override
    public String getMessage() {
        return getTransformerMessage(transformerException);
    }

    public TransformerException geTransformerException() {
        return transformerException;
    }

    public TransformerException getTransformerException() {
        return transformerException;
    }

    private String getTransformerMessage(TransformerException te) {
        Throwable e = te.getCause();
        if ((e != null) && (e instanceof TransformerException)) {
            return getTransformerMessage((TransformerException) e);
        }
        StringBuilder buf = new StringBuilder();
        SourceLocator sourceLocator = te.getLocator();
        if (sourceLocator != null) {
            ExceptionsUtils.append(buf, sourceLocator.getPublicId(), sourceLocator.getSystemId(), sourceLocator.getLineNumber(), sourceLocator.getColumnNumber());
        } else if ((e != null) && (e instanceof SAXParseException)) {
            SAXParseException spe = (SAXParseException) e;
            ExceptionsUtils.append(buf, spe);
        }
        if (e != null) {
            buf.append(e.getLocalizedMessage());
        } else {
            buf.append(te.getMessageAndLocation());
        }
        return buf.toString();
    }

}
