/* Util - Copyright (c) 2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.request;


/**
 *
 * @author Vincent Calame
 */
public interface RequestConstants {

    public final static String LOCALE_PARAMETER = "locale";
    public final static String COMMAND_PARAMETER = "cmd";
    public final static String OUTPUT_PARAMETER = "output";
    public final static String PAGE_PARAMETER = "page";
    public final static String JSON_PARAMETER = "json";
    public final static String STREAM_PARAMETER = "stream";
    public final static String REDIRECT_PARAMETER = "redirect";
    public final static String TEST_PARAMETER = "test";
    public final static String DATATYPE_PARAMETER = "datatype";
    public final static String CALLBACK_PARAMETER = "callback";
    public final static String JSONTYPE_PARAMETER = "jsontype";
}
