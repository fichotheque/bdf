/* UtilLib - Copyright (c) 2009-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.geoloc;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.exceptions.InternalResourceException;
import net.mapeadores.util.ini.IniParser;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.primitives.DegrePoint;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class GeolocUtils {

    private final static Map<String, DegrePoint> countryGeolocMap = new HashMap<String, DegrePoint>();

    static {
        try (InputStream is = GeolocUtils.class.getResourceAsStream("geoloc_country.ini")) {
            Map<String, String> iniMap = new HashMap<String, String>();
            IniParser.parseIni(is, iniMap);
            for (Map.Entry<String, String> entry : iniMap.entrySet()) {
                String value = entry.getValue();
                int idx = value.indexOf(',');
                DegreDecimal latitude = DegreDecimal.newInstance(StringUtils.parseDecimal(value.substring(0, idx).trim()));
                DegreDecimal longitude = DegreDecimal.newInstance(StringUtils.parseDecimal(value.substring(idx + 1).trim()));
                countryGeolocMap.put(entry.getKey(), new DegrePoint(latitude, longitude));
            }
            iniMap.clear();
        } catch (IOException ioe) {
            throw new InternalResourceException(ioe);

        } catch (NumberFormatException nfe) {
            throw new InternalResourceException(nfe);
        }
    }

    private GeolocUtils() {
    }

    public static DegrePoint getCountryGeoloc(String countryCode) {
        return countryGeolocMap.get(countryCode);
    }

}
