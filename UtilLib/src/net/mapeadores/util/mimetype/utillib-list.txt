application/x-desmodo        dsmd crtxml
application/x-simplegrille   dsms

text/xml             xml
text/plain           txt
text/html            html
text/css             css
text/csv             csv
text/x-ini           ini
text/x-properties    properties
text/x-clojure       clojure

application/octet-stream   bin
application/json           json
application/xslt+xml       xsl
application/xml-dtd        dtd
application/javascript     js

image/jpeg   jpg
image/png    png