/* UtilLib - Copyright (c) 2012-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.mimetype;


/**
 *
 * @author Vincent Calame
 */
public interface MimeTypeConstants {

    public final static String XML = "text/xml";
    public final static String PLAIN = "text/plain";
    public final static String HTML = "text/html";
    public final static String CSS = "text/css";
    /**
     * Format CSV défini par le RFC 4180 http://tools.ietf.org/html/rfc4180
     */
    public final static String CSV = "text/csv";
    public final static String OCTETSTREAM = "application/octet-stream";
    public final static String JSON = "application/json";
    public final static String XSLT = "application/xslt+xml";
    public final static String DTD = "application/xml-dtd";
    public final static String JAVASCRIPT = "application/javascript";
    public final static String JPEG = "image/jpeg";
    public final static String PNG = "image/png";
    public final static String ODS = "application/vnd.oasis.opendocument.spreadsheet";
    public final static String ODT = "application/vnd.oasis.opendocument.text";
    public final static String SVG = "image/svg+xml";
}
