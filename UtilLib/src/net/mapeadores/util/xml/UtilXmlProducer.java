/* UtilLib - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public abstract class UtilXmlProducer implements XmlProducer {

    public UtilXmlProducer() {

    }

    @Override
    public void writeXml(Appendable appendable) throws IOException {
        AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(appendable, 0);
        xmlWriter.appendXMLDeclaration();
        write(xmlWriter);
    }

    public abstract void write(AppendableXMLWriter xmlWriter) throws IOException;

}
