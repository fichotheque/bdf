/* UtilLib - Copyright (c) 2006-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public interface XMLWriter {

    public XMLWriter startOpenTag(String name) throws IOException;

    public XMLWriter startOpenTag(String name, boolean indentBefore) throws IOException;

    public XMLWriter endOpenTag() throws IOException;

    public XMLWriter closeEmptyTag() throws IOException;

    public XMLWriter openTag(String name) throws IOException;

    public XMLWriter openTag(String name, boolean indentBefore) throws IOException;

    public XMLWriter closeTag(String name) throws IOException;

    public XMLWriter closeTag(String name, boolean indentBefore) throws IOException;

    public XMLWriter addText(CharSequence charSequence) throws IOException;

    public XMLWriter addCData(CharSequence charSequence) throws IOException;

    public XMLWriter addAttribute(String attributeName, String value) throws IOException;

    public XMLWriter addAttribute(String attributeName, int i) throws IOException;

    /**
     * N'insère rien si value est nul ou de longueur nulle
     */
    public default XMLWriter addSimpleElement(String name, String value) throws IOException {
        if ((value == null) || (value.length() == 0)) {
            return this;
        }
        startOpenTag(name);
        endOpenTag();
        addText(value);
        closeTag(name, false);
        return this;
    }

    public default XMLWriter addSimpleElement(String name, String value, String lang) throws IOException {
        if ((value == null) || (value.length() == 0)) {
            return this;
        }
        startOpenTag(name);
        addAttribute("xml:lang", lang);
        endOpenTag();
        addText(value);
        closeTag(name, false);
        return this;
    }

    public default XMLWriter addEmptyElement(String name) throws IOException {
        startOpenTag(name);
        closeEmptyTag();
        return this;
    }

}
