/* UtilLib - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.util.LinkedHashMap;
import java.util.Map;
import org.w3c.dom.DocumentFragment;


/**
 *
 * @author Vincent Calame
 */
public class DocumentFragmentHolderBuilder {

    private final Map<String, DocumentFragment> fragmentMap = new LinkedHashMap<String, DocumentFragment>();

    public DocumentFragmentHolderBuilder() {

    }

    public DocumentFragmentHolderBuilder put(String key, DocumentFragment documentFragment) {
        fragmentMap.put(key, documentFragment);
        return this;
    }

    public DocumentFragmentHolder toDocumentFragmentHolder() {
        Map<String, DocumentFragment> finalFragmentMap = new LinkedHashMap<String, DocumentFragment>(fragmentMap);
        return new InternalDocumentFragmentHolder(finalFragmentMap);
    }

    public static DocumentFragmentHolderBuilder init() {
        return new DocumentFragmentHolderBuilder();
    }


    private static class InternalDocumentFragmentHolder implements DocumentFragmentHolder {

        private final Map<String, DocumentFragment> fragmentMap;

        private InternalDocumentFragmentHolder(Map<String, DocumentFragment> fragmentMap) {
            this.fragmentMap = fragmentMap;
        }

        @Override
        public DocumentFragment getFragment(String fragmentKey) {
            return fragmentMap.get(fragmentKey);
        }

    }

}
