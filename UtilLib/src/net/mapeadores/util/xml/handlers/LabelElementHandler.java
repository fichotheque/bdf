/* UtilLib - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml.handlers;

import java.text.ParseException;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class LabelElementHandler implements ElementHandler {

    private final static Lang UNDETERMINED_LANG = Lang.build("und");
    private Lang lang;
    private final StringBuilder buf = new StringBuilder();
    private int subCount = 0;

    public LabelElementHandler(Attributes attributes, String xpath) throws ErrorMessageException {
        String langString = attributes.getValue("xml:lang");
        if ((langString != null) && (langString.length() > 0)) {
            try {
                lang = Lang.parse(langString);
            } catch (ParseException pe) {
                throw new ErrorMessageException("_ error.wrong.xml.langattributevalue", xpath, langString);
            }
        }
        if (lang == null) {
            throw new ErrorMessageException("_ error.empty.xml.attribute", xpath, "xml:lang");
        }
    }

    @Override
    public void processStartElement(String tagname, Attributes attributes) {
        subCount++;
        buf.append("<");
        buf.append(tagname);
        buf.append(">");
    }

    @Override
    public boolean processEndElement(String tagname) {
        if (subCount == 0) {
            return true;
        }
        subCount--;
        buf.append("</");
        buf.append(tagname);
        buf.append(">");
        return false;
    }

    @Override
    public void processText(char[] ch, int start, int length) {
        buf.append(ch, start, length);
    }

    public ElementHandler initSubElementHandler(String tagname, Map attributeMap) {
        return null;
    }

    public void flushSubElementHandler(String tagname, ElementHandler elementHandler) {
    }

    /**
     * Peut être nul
     */
    public Label toLabel() {
        CleanedString cleanedString = CleanedString.newInstance(buf);
        if (cleanedString == null) {
            return null;
        }
        if (lang == null) {
            lang = UNDETERMINED_LANG;
        }
        return LabelUtils.toLabel(lang, cleanedString);
    }

}
