/* UtilLib - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml.handlers;

import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
public abstract class StackElementHandler implements ElementHandler {

    private ElementHandler subElementHandler;

    public StackElementHandler() {
    }

    @Override
    public void processStartElement(String tagname, Attributes attributes) {
        if (subElementHandler != null) {
            subElementHandler.processStartElement(tagname, attributes);
        } else {
            subElementHandler = newSubHandler(tagname, attributes);
            if (subElementHandler == null) {
                subElementHandler = new NullHandler();
            }
        }
    }

    @Override
    public boolean processEndElement(String tagname) {
        if (subElementHandler != null) {
            boolean endSubElement = subElementHandler.processEndElement(tagname);
            if (endSubElement) {
                closeSubHandler(subElementHandler);
                subElementHandler = null;
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void processText(char[] ch, int start, int length) {
        if (subElementHandler != null) {
            subElementHandler.processText(ch, start, length);
        }
    }

    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        return null;
    }

    public void closeSubHandler(ElementHandler subElementHandler) {

    }

    public static String getAttributeString(Attributes attributes, String name) {
        String result = attributes.getValue(name);
        if (result == null) {
            return null;
        }
        if (result.length() == 0) {
            return null;
        }
        return result;
    }

}
