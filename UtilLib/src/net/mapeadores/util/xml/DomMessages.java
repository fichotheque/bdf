/* FichothequeLib_Tools - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.logging.MessageHandler;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public class DomMessages {

    public final static String SEVERE_MALFORMED = "severe.xml.malformed";
    public final static String SEVERE_INVALID = "severe.xml.invalid";
    public final static String WARNING_INVALID = "warning.xml.invalid";

    private DomMessages() {
    }

    public static void saxException(MessageHandler handler, SAXException saxe) {
        handler.addMessage(SEVERE_MALFORMED, "_ error.exception.xml.sax", saxe.getMessage());
    }

    public static void missingChildTag(MessageHandler handler, String tagName, String childTagName) {
        handler.addMessage(SEVERE_INVALID, "_ error.unsupported.xml.missingtag", tagName, childTagName);
    }

    public static void unknownTagWarning(MessageHandler handler, String tagName) {
        handler.addMessage(WARNING_INVALID, "_ error.unknown.xml.tag", tagName);
    }

    public static void emptyAttribute(MessageHandler handler, String tagName, String attributeName) {
        handler.addMessage(SEVERE_INVALID, "_ error.empty.xml.attribute", attributeName, tagName);
    }

    public static void emptyElement(MessageHandler handler, String tagName) {
        handler.addMessage(SEVERE_INVALID, "_ error.empty.xml.tag", tagName);
    }

    public static void wrongElementValue(MessageHandler handler, String tagName, String value) {
        handler.addMessage(SEVERE_INVALID, "_ error.wrong.xml.tagvalue", tagName, value);
    }

    public static void wrongAttributeValue(MessageHandler handler, String tagName, String attribute, String value) {
        handler.addMessage(SEVERE_INVALID, "_ error.wrong.xml.attributevalue", tagName, attribute, value);
    }

    public static void wrongLangAttribute(MessageHandler handler, String tagName, String value) {
        handler.addMessage(SEVERE_INVALID, "_ error.wrong.xml.langattributevalue", tagName, value);
    }

    public static void wrongIntegerAttributeValue(MessageHandler handler, String tagName, String attributeName, String intString) {
        handler.addMessage(SEVERE_INVALID, "_ error.wrong.xml.attributevalue_integer", tagName, attributeName, intString);
    }

    public static void malformed(MessageHandler messageHandler, String key, Object... values) {
        messageHandler.addMessage(SEVERE_MALFORMED, LocalisationUtils.toMessage(key, values));
    }

    public static void invalid(MessageHandler messageHandler, String key, Object... values) {
        messageHandler.addMessage(SEVERE_INVALID, LocalisationUtils.toMessage(key, values));
    }

    public static void invalidWarning(MessageHandler messageHandler, String key, Object... values) {
        messageHandler.addMessage(WARNING_INVALID, LocalisationUtils.toMessage(key, values));
    }

}
