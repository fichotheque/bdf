/* UtilLib - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 *
 * @author Vincent Calame
 */
public class DOMXMLWriter implements XMLWriter {

    private Document document;
    private Node currentNode;
    private Element currentElement;

    public DOMXMLWriter(Document document) {
        this.document = document;
        this.currentNode = document;
    }

    @Override
    public DOMXMLWriter startOpenTag(String name) throws IOException {
        newCurrentElement(name);
        return this;
    }

    @Override
    public DOMXMLWriter startOpenTag(String name, boolean indentBefore) throws IOException {
        newCurrentElement(name);
        return this;
    }

    @Override
    public DOMXMLWriter endOpenTag() throws IOException {
        return this;
    }

    @Override
    public DOMXMLWriter closeEmptyTag() throws IOException {
        closeCurrentElement();
        return this;
    }

    @Override
    public DOMXMLWriter openTag(String name) throws IOException {
        newCurrentElement(name);
        return this;
    }

    @Override
    public DOMXMLWriter openTag(String name, boolean indentBefore) throws IOException {
        newCurrentElement(name);
        return this;
    }

    @Override
    public DOMXMLWriter closeTag(String name) throws IOException {
        closeCurrentElement();
        return this;
    }

    @Override
    public DOMXMLWriter closeTag(String name, boolean indentBefore) throws IOException {
        closeCurrentElement();
        return this;
    }

    @Override
    public DOMXMLWriter addText(CharSequence charSequence) throws IOException {
        if (charSequence.length() > 0) {
            appendToCurrent(document.createTextNode(charSequence.toString()));
        }
        return this;
    }

    @Override
    public DOMXMLWriter addCData(CharSequence charSequence) throws IOException {
        if (charSequence.length() > 0) {
            currentElement.appendChild(document.createCDATASection(charSequence.toString()));
        }
        return this;
    }

    @Override
    public DOMXMLWriter addAttribute(String attributeName, String value) throws IOException {
        if ((value != null) && (value.length() > 0)) {
            currentElement.setAttribute(attributeName, value);
        }
        return this;
    }

    @Override
    public DOMXMLWriter addAttribute(String attributeName, int i) throws IOException {
        currentElement.setAttribute(attributeName, Integer.toString(i));
        return this;
    }

    @Override
    public DOMXMLWriter addSimpleElement(String tagname, String value) throws IOException {
        if ((value == null) || (value.length() == 0)) {
            return this;
        }
        Element element = document.createElement(tagname);
        element.appendChild(document.createTextNode(value));
        appendToCurrent(element);
        return this;
    }

    @Override
    public DOMXMLWriter addEmptyElement(String tagname) throws IOException {
        Element element = document.createElement(tagname);
        appendToCurrent(element);
        return this;
    }

    private void newCurrentElement(String name) {
        Element element = document.createElement(name);
        appendToCurrent(element);
        this.currentElement = element;
        this.currentNode = element;
    }

    private void closeCurrentElement() {
        this.currentNode = currentElement.getParentNode();
        if (currentNode instanceof Element) {
            this.currentElement = (Element) currentNode;
        } else {
            this.currentElement = null;
        }
    }

    private void appendToCurrent(Node node) {
        currentNode.appendChild(node);
    }

}
