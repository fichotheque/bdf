/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;
import java.io.InputStream;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.io.IOUtils;


/**
 *
 * @author Vincent Calame
 */
public class StringXMLWriter extends DefaultXMLWriter {

    private StringBuilder buf = new StringBuilder();

    public StringXMLWriter() {
        setAppendable(buf);
    }

    public String toStringAndClear() {
        String result = buf.toString();
        buf = new StringBuilder();
        setAppendable(buf);
        setIndentLength(0);
        return result;
    }

    public InputStream toInputStreamAndClear() {
        try {
            return IOUtils.toInputStream(toStringAndClear(), "UTF-8");
        } catch (IOException ioe) {
            throw new ShouldNotOccurException(ioe);
        }
    }

}
