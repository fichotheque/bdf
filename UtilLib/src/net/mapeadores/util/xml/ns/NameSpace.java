/* UtilLib - Copyright (c) 2008-2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml.ns;


/**
 *
 * @author Vincent Calame
 */
public class NameSpace {

    public static final NameSpace XHTML_NAMESPACE = new NameSpace("", "http://www.w3.org/1999/xhtml");
    public static final NameSpace ATOM_NAMESPACE = new NameSpace("", "http://www.w3.org/2005/Atom");
    public static final NameSpace XSL_NAMESPACE = new NameSpace("xsl", "http://www.w3.org/1999/XSL/Transform");
    public static final NameSpace SVG_NAMESPACE = new NameSpace("svg", "http://www.w3.org/2000/svg");
    public static final NameSpace XLINK_NAMESPACE = new NameSpace("xlink", "http://www.w3.org/1999/xlink");
    private String prefix = "";
    private String name;

    public NameSpace() {
    }

    public NameSpace(String prefix, String name) {
        setPrefix(prefix);
        this.name = name;
    }

    /**
     * N'est jamais null, la longueur nulle indique l'espace par défaut.
     */
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        if (prefix == null) {
            this.prefix = "";
        } else {
            this.prefix = prefix;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
