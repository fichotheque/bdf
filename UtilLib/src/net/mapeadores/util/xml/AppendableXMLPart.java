/* UtilLib - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class AppendableXMLPart extends XMLPart implements AppendableXMLWriter {

    private final AppendableXMLWriter appendableXmlWriter;

    public AppendableXMLPart(AppendableXMLWriter xmlWriter) {
        super(xmlWriter);
        this.appendableXmlWriter = xmlWriter;
    }

    public AppendableXMLWriter getAppendableXMLWriter() {
        return appendableXmlWriter;
    }

    @Override
    public Appendable append(char c) throws IOException {
        appendableXmlWriter.append(c);
        return this;
    }

    @Override
    public Appendable append(CharSequence charSequence) throws IOException {
        appendableXmlWriter.append(charSequence);
        return this;
    }

    @Override
    public Appendable append(CharSequence charSequence, int start, int end) throws IOException {
        appendableXmlWriter.append(charSequence, start, end);
        return this;
    }

    @Override
    public AppendableXMLPart appendIndent() throws IOException {
        appendableXmlWriter.appendIndent();
        return this;
    }

    @Override
    public AppendableXMLPart appendXMLDeclaration() throws IOException {
        appendableXmlWriter.appendXMLDeclaration();
        return this;
    }

    @Override
    public int getCurrentIndentValue() {
        return appendableXmlWriter.getCurrentIndentValue();
    }

}
