/* UtilLib - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;


/**
 *
 * @author Vincent Calame
 */
public class CommandBox {

    private String name;
    private String lockey;
    private Map<String, String> hiddenMap;
    private String page;
    private String errorPage;
    private String action;
    private String family;
    private String target;
    private String mode = "";
    private String actionCssClass;
    private boolean multipart = false;
    private String helpUrl;
    private String submitLocKey;
    private String submitProcess = "test";
    private boolean veil = false;

    public CommandBox() {
    }

    private CommandBox(CommandBox commandBox, String name, String lockey) {
        this.name = name;
        this.lockey = lockey;
        if (commandBox.hiddenMap != null) {
            this.hiddenMap = new HashMap<String, String>(commandBox.hiddenMap);
        }
        this.page = commandBox.page;
        this.errorPage = commandBox.errorPage;
        this.action = commandBox.action;
        this.family = commandBox.family;
        this.target = commandBox.target;
        this.multipart = commandBox.multipart;
        this.mode = commandBox.mode;
        this.actionCssClass = commandBox.actionCssClass;
        this.helpUrl = commandBox.helpUrl;
        this.submitLocKey = commandBox.submitLocKey;
        this.submitProcess = commandBox.submitProcess;
        this.veil = commandBox.veil;
    }

    public CommandBox derive(String name, String lockey) {
        return new CommandBox(this, name, lockey);
    }

    public String name() {
        return name;
    }

    public CommandBox name(String name) {
        this.name = name;
        return this;
    }

    public String lockey() {
        return lockey;
    }

    public CommandBox lockey(String lockey) {
        this.lockey = lockey;
        return this;
    }

    public Map<String, String> hiddenMap() {
        return hiddenMap;
    }

    public CommandBox hidden(String name, String value) {
        if (hiddenMap == null) {
            hiddenMap = new HashMap<String, String>();
        }
        hiddenMap.put(name, value);
        return this;
    }

    public String mode() {
        return mode;
    }

    public CommandBox mode(String mode) {
        if (mode == null) {
            this.mode = "";
        } else {
            this.mode = mode;
        }
        return this;
    }

    public String page() {
        return page;
    }

    public CommandBox page(String page) {
        this.page = page;
        return this;
    }

    public String errorPage() {
        return errorPage;
    }

    public CommandBox errorPage(String errorPage) {
        this.errorPage = errorPage;
        return this;
    }

    public String action() {
        return action;
    }

    public CommandBox action(String action) {
        this.action = action;
        return this;
    }

    public String actionCssClass() {
        return actionCssClass;
    }

    public CommandBox actionCssClass(String actionCssClass) {
        this.actionCssClass = actionCssClass;
        return this;
    }

    public String family() {
        return family;
    }

    public CommandBox family(String family) {
        this.family = family;
        return this;
    }

    public String target() {
        return target;
    }

    public CommandBox target(String target) {
        this.target = target;
        return this;
    }

    public boolean multipart() {
        return multipart;
    }

    public CommandBox multipart(boolean multipart) {
        this.multipart = multipart;
        return this;
    }

    public String helpUrl() {
        return helpUrl;
    }

    public CommandBox helpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
        return this;
    }

    public String submitLocKey() {
        return submitLocKey;
    }

    public CommandBox submitLocKey(String submitLocKey) {
        this.submitLocKey = submitLocKey;
        return this;
    }

    public String submitProcess() {
        return submitProcess;
    }

    public CommandBox submitProcess(String submitProcess) {
        this.submitProcess = submitProcess;
        return this;
    }

    public boolean veil() {
        return veil;
    }

    public CommandBox veil(boolean veil) {
        this.veil = veil;
        return this;
    }

    public CommandBox __(boolean bool) {
        return this;
    }

    public CommandBox __(Consumer<CommandBox> consumer) {
        consumer.accept(this);
        return this;
    }

    public static CommandBox init() {
        return new CommandBox();
    }

}
