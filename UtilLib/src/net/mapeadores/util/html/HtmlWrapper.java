/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.function.BiConsumer;
import java.util.function.Consumer;


/**
 *
 * @author Vincent Calame
 */
public interface HtmlWrapper {

    public void wrap(HtmlPrinter hp, Consumer<HtmlPrinter> consumer);

    public void wrap(HtmlPrinter hp, Runnable runnable);

    public void wrap(HtmlPrinter hp, BiConsumer<HtmlPrinter, Object> consumer, Object argument);

}
