/* UtilLib - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.function.Consumer;


/**
 *
 * @author Vincent Calame
 */
public final class ConsumerFactory {

    private ConsumerFactory() {

    }

    public static Consumer<HtmlPrinter> p(String cssClasses, String locKey) {
        return new LocP(cssClasses, locKey);
    }

    public static Consumer<HtmlPrinter> emptySpan(String cssClasses) {
        return new EmptySpan(cssClasses);
    }

    public static Consumer<HtmlPrinter> startDiv(String cssClasses) {
        return new StartDiv(cssClasses);
    }

    public static Consumer<HtmlPrinter> endDiv() {
        return new EndDiv();
    }

    public static Consumer<HtmlPrinter> startP(String cssClasses) {
        return new StartP(cssClasses);
    }

    public static Consumer<HtmlPrinter> endP() {
        return new EndP();
    }

    public static Consumer<HtmlPrinter> startSpan(String cssClasses) {
        return new StartSpan(cssClasses);
    }

    public static Consumer<HtmlPrinter> endSpan() {
        return new EndSpan();
    }


    private static class LocP implements Consumer<HtmlPrinter> {

        private final String cssClasses;
        private final String locKey;

        private LocP(String cssClasses, String locKey) {
            this.cssClasses = cssClasses;
            this.locKey = locKey;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .P(cssClasses)
                    .__localize(locKey)
                    ._P();
        }

    }


    private static class EmptySpan implements Consumer<HtmlPrinter> {

        private final String cssClasses;

        private EmptySpan(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .SPAN(cssClasses)
                    ._SPAN();
        }

    }


    private static class StartDiv implements Consumer<HtmlPrinter> {

        private final String cssClasses;

        private StartDiv(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .DIV(cssClasses);
        }

    }


    private static class EndDiv implements Consumer<HtmlPrinter> {

        private EndDiv() {
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    ._DIV();
        }

    }


    private static class StartP implements Consumer<HtmlPrinter> {

        private final String cssClasses;

        private StartP(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .P(cssClasses);
        }

    }


    private static class EndP implements Consumer<HtmlPrinter> {

        private EndP() {
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    ._P();
        }

    }


    private static class StartSpan implements Consumer<HtmlPrinter> {

        private final String cssClasses;

        private StartSpan(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .SPAN(cssClasses);
        }

    }


    private static class EndSpan implements Consumer<HtmlPrinter> {

        private EndSpan() {
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    ._SPAN();
        }

    }

}
