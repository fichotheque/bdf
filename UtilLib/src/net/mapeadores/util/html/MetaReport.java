/* UtilLib_Html - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.Collection;
import java.util.Set;
import net.mapeadores.util.text.MultiStringable;


/**
 *
 * @author Vincent Calame
 */
public interface MetaReport {

    public final static String HTML = "Html";
    public final static String DUBLIN_CORE = "DublinCore";
    public final static String OPENGRAPH = "OpenGraph";
    public final static String SCHEMA = "Schema";

    public Set<String> getAvalaibleProtocolSet();

    public ProtocolValues getProtocolValues(String protocolName);

    public default boolean containsProtocol(String protocol) {
        return getAvalaibleProtocolSet().contains(protocol);
    }

    public default MultiStringable getValues(Collection<MetaParam> metaParams) {
        for (MetaParam metaParam : metaParams) {
            ProtocolValues protocolValues = getProtocolValues(metaParam.getProtocol());
            if (protocolValues != null) {
                MultiStringable values = protocolValues.getValues(metaParam.getName());
                if (values != null) {
                    return values;
                }
            }
        }
        return null;
    }

    public default MultiStringable getValues(MetaParam[] metaParams) {
        for (MetaParam metaParam : metaParams) {
            ProtocolValues protocolValues = getProtocolValues(metaParam.getProtocol());
            if (protocolValues != null) {
                MultiStringable values = protocolValues.getValues(metaParam.getName());
                if (values != null) {
                    return values;
                }
            }
        }
        return null;
    }


    public interface ProtocolValues {

        public String getProtocolName();

        public Set<String> getParamNameSet();

        public MultiStringable getValues(String paramName);

    }

}
