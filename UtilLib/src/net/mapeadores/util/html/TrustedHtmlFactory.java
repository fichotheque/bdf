/* UtilLib - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;


/**
 *
 * @author Vincent Calame
 */
public abstract class TrustedHtmlFactory {

    public final static TrustedHtmlFactory UNCHECK = new UncheckTrustedHtmlFactory();

    public TrustedHtmlFactory() {

    }

    public abstract String check(String html);

    public TrustedHtml build(String html) {
        return new TrustedHtml(check(html));
    }


    private static class UncheckTrustedHtmlFactory extends TrustedHtmlFactory {

        public UncheckTrustedHtmlFactory() {

        }

        @Override
        public String check(String html) {
            return html;
        }

    }

}
