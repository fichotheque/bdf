/* UtilLib - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;

import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public final class AttributeKey implements Serializable, Comparable<AttributeKey> {

    private static final Map<String, AttributeKey> internMap = new HashMap<String, AttributeKey>();
    public final static String TRANSIENT_NAMESPACE = "_transient";
    private final String attributeKeyString;
    private final String nameSpace;
    private final String localKey;

    private AttributeKey(String attributeKeyString, String nameSpace, String localKey) {
        this.nameSpace = nameSpace;
        this.localKey = localKey;
        this.attributeKeyString = attributeKeyString;
        intern(this);
    }

    /**
     * N'est jamais nul et correspond à [_a-zA-Z][-_.a-zA-Z0-9]*
     */
    public String getNameSpace() {
        return nameSpace;
    }

    /**
     * N'est jamais nul et correspond à [_a-zA-Z][-_.a-zA-Z0-9]*
     */
    public String getLocalKey() {
        return localKey;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        AttributeKey otherAttributeKey = (AttributeKey) other;
        if (!otherAttributeKey.nameSpace.equals(this.nameSpace)) {
            return false;
        }
        return otherAttributeKey.localKey.equals(this.localKey);
    }

    @Override
    public int compareTo(AttributeKey other) {
        int comp = this.nameSpace.compareTo(other.nameSpace);
        if (comp == 0) {
            comp = this.localKey.compareTo(other.localKey);
        }
        return comp;
    }

    @Override
    public int hashCode() {
        return attributeKeyString.hashCode();
    }

    @Override
    public String toString() {
        return attributeKeyString;
    }

    public static AttributeKey build(CheckedNameSpace nameSpace, CheckedLocalKey localKey) {
        if (nameSpace == null) {
            throw new IllegalArgumentException("nameSpace is null");
        }
        if (localKey == null) {
            throw new IllegalArgumentException("localKey is null");
        }
        String attributeKeyString = nameSpace.toString() + ":" + localKey.toString();
        AttributeKey current = internMap.get(attributeKeyString);
        if (current != null) {
            return current;
        }
        return new AttributeKey(attributeKeyString, nameSpace.toString(), localKey.toString());
    }

    public static AttributeKey build(CheckedNameSpace nameSpace, CharSequence localKey) {
        if (nameSpace == null) {
            throw new IllegalArgumentException("nameSpace is null");
        }
        if (localKey == null) {
            throw new IllegalArgumentException("localKey is null");
        }
        return AttributeKey.build(nameSpace, CheckedLocalKey.build(localKey));
    }

    public static AttributeKey parse(String s) throws ParseException {
        AttributeKey current = internMap.get(s);
        if (current != null) {
            return current;
        }
        int idx = s.indexOf(':');
        if (idx == -1) {
            throw new ParseException("Missing :", 0);
        } else if (idx == 0) {
            throw new ParseException("Starting with :", 0);
        } else if (idx == (s.length() - 1)) {
            throw new ParseException("Ending with :", 0);
        }
        CheckedNameSpace nameSpace = CheckedNameSpace.parse(s.substring(0, idx));
        CheckedLocalKey localKey = CheckedLocalKey.parse(s.substring(idx + 1));
        return AttributeKey.build(nameSpace, localKey);
    }

    private synchronized static void intern(AttributeKey attributeKey) {
        internMap.put(attributeKey.toString(), attributeKey);
    }

}
