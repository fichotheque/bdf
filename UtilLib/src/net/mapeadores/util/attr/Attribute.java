/* UtilLib - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import net.mapeadores.util.text.StringUtils;


/**
 * Liste de chaines répondant aux conditions suivantes : - au moins une valeur -
 * les valeurs ne sont jamais nulles, de longueur nulles et ne commencent ni
 * finissent par des espaces blancs (value.trim() = value)
 *
 * @author Vincent Calame
 */
public interface Attribute extends List<String>, RandomAccess {

    /**
     * N'est jamais nul
     */
    public AttributeKey getAttributeKey();

    /**
     * Indentique à get(0). Rappel qu'il doit toujours y avoir au moins une
     * valeur.
     *
     */
    public default String getFirstValue() {
        return get(0);
    }

    public default boolean isTrue() {
        return StringUtils.isTrue(getFirstValue());
    }

    public default String getSubParamValue(String subParamName) {
        String prefix = subParamName + ":";
        for (String value : this) {
            if (value.startsWith(prefix)) {
                String subValue = value.substring(prefix.length()).trim();
                return subValue;
            }
        }
        return null;
    }

    public default Map<String, String> toSubParamMap() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        StringBuilder noNameBuf = new StringBuilder();
        for (String value : this) {
            int idx = value.indexOf(':');
            if (idx > 0) {
                String name = value.substring(0, idx);
                String subValue = value.substring(idx + 1).trim();
                map.put(name, subValue);
            } else {
                if (noNameBuf.length() > 0) {
                    noNameBuf.append(';');
                }
                noNameBuf.append(value);
            }
        }
        if (noNameBuf.length() > 0) {
            map.put("", noNameBuf.toString());
        }
        return map;
    }

}
