/* UtilLib - Copyright (c) 2011-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;

import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;


/**
 *
 * @author Vincent Calame
 */
public class AttributeDefBuilder extends DefBuilder {

    private final AttributeKey attributeKey;
    private final Map<String, Object> defObjectMap = new HashMap<String, Object>();

    public AttributeDefBuilder(AttributeKey attributeKey) {
        this(attributeKey, null);
    }

    public AttributeDefBuilder(AttributeKey attributeKey, Attributes initAttributes) {
        super(initAttributes);
        if (attributeKey == null) {
            throw new IllegalArgumentException("attributeKey is null");
        }
        this.attributeKey = attributeKey;
    }

    public AttributeKey getAttributeKey() {
        return attributeKey;
    }

    public void putDefObject(String objectName, Object object) {
        defObjectMap.put(objectName, object);
    }

    public AttributeDef toAttributeDef() {
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        Map<String, Object> finalDefObjectMap = new HashMap<String, Object>(defObjectMap);
        Phrases phrases = toPhrases();
        return new InternalAttributeDef(attributeKey, finalDefObjectMap, phrases, titleLabels, attributes);
    }


    private static class InternalAttributeDef implements AttributeDef {

        private final AttributeKey attributeKey;
        private final Map<String, Object> defObjectMap;
        private final Labels titleLabels;
        private final Phrases phrases;
        private final Attributes attributes;

        private InternalAttributeDef(AttributeKey attributeKey, Map<String, Object> defObjectMap, Phrases phrases,
                Labels titleLabels, Attributes attributes) {
            this.attributeKey = attributeKey;
            this.defObjectMap = defObjectMap;
            this.titleLabels = titleLabels;
            this.phrases = phrases;
            this.attributes = attributes;
        }

        @Override
        public AttributeKey getAttributeKey() {
            return attributeKey;
        }

        @Override
        public Object getDefObject(String objectName) {
            return defObjectMap.get(objectName);
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public Phrases getPhrases() {
            return phrases;
        }

    }

}
