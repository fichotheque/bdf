/* UtilLib - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.exceptions.InternalResourceException;
import net.mapeadores.util.ini.IniParser;


/**
 *
 * @author Vincent Calame
 */
public final class Country implements Serializable, Comparable<Country> {

    private static final long serialVersionUID = 1L;
    private final static Map<String, Country> countryMap = new HashMap<String, Country>();
    private final static Map<String, String> conversionMap = new HashMap<String, String>();

    static {
        try (InputStream is = Country.class.getResourceAsStream("conversion_country.ini")) {
            IniParser.parseIni(is, conversionMap);
        } catch (IOException ioe) {
            throw new InternalResourceException(ioe);
        }
    }

    private final String code;

    private Country(String code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        return code.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        Country otherCountry = (Country) other;
        return otherCountry.code.equals(this.code);
    }

    @Override
    public String toString() {
        return code;
    }

    @Override
    public int compareTo(Country otherCountry) {
        return this.code.compareTo(otherCountry.code);
    }

    public static Country build(String countryCode) {
        try {
            return parse(countryCode);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe.getMessage());
        }
    }

    public static Country parse(String countryCode) throws ParseException {
        if (countryCode == null) {
            throw new ParseException("null", 0);
        }
        Country country = countryMap.get(countryCode);
        if (country != null) {
            return country;
        }
        synchronized (countryMap) {
            int length = countryCode.length();
            if (length == 2) {
                countryCode = countryCode.toUpperCase();
                country = countryMap.get(countryCode);
                if (country != null) {
                    return country;
                }
                if (!testChar(countryCode.charAt(0))) {
                    throw new ParseException("wrong char: " + countryCode.charAt(0), 0);
                }
                if (!testChar(countryCode.charAt(1))) {
                    throw new ParseException("wrong char: " + countryCode.charAt(1), 1);
                }
                String conversionValue = conversionMap.get(countryCode);
                if (conversionValue != null) {
                    country = countryMap.get(conversionValue);
                    if (country == null) {
                        country = new Country(conversionValue);
                        countryMap.put(conversionValue, country);
                    }
                } else {
                    country = new Country(countryCode);
                }
                countryMap.put(countryCode, country);
                return country;
            } else if (length == 0) {
                throw new ParseException("empty", 0);
            } else if (length == 1) {
                throw new ParseException("length = 1", 0);
            } else {
                throw new ParseException("length > 2", 2);
            }
        }
    }

    private static boolean testChar(char carac) {
        return ((carac >= 'A') && (carac <= 'Z'));
    }

}
