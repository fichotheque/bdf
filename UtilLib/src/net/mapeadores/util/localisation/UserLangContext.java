/* UtilLib - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import java.text.ParseException;
import java.util.Locale;


/**
 *
 * @author Vincent Calame
 */
public interface UserLangContext extends LangContext {

    /**
     * N'est jamais nul.
     *
     * @return instance de la langue de travai
     */
    public Lang getWorkingLang();

    /**
     * N'est jamais nul.
     *
     * @return liste de la préférence des langues
     */
    public LangPreference getLangPreference();

    /**
     * N'est jamais nul.
     *
     * @return localisation à utiliser pour le formatage (nombres, dates)
     */
    public Locale getFormatLocale();

    @Override
    public default Lang getDefaultLang() {
        return getWorkingLang();
    }

    @Override
    public default Locale getDefaultFormatLocale() {
        return getFormatLocale();
    }


    public default boolean isDefaultLocale() {
        try {
            return ((Lang.fromLocale(getFormatLocale())).equals(getWorkingLang()));
        } catch (ParseException pe) {
            return false;
        }
    }

    public default boolean isDefaultLangPreference() {
        LangPreference langPreference = getLangPreference();
        return ((langPreference.size() == 1) && (langPreference.get(0).equals(getWorkingLang())));
    }

    public default String getISOFormatLocaleString() {
        return Lang.toISOString(getFormatLocale());
    }

}
