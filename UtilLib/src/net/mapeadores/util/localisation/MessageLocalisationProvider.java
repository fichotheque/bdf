/* UtilLib - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import java.util.Locale;


/**
 *
 * @author Vincent Calame
 */
public interface MessageLocalisationProvider {

    /**
     * Retourne l'instance de MessageLocalisation à utiliser pour la traduction
     * des clés de localisation en fonction des préférences de langues
     * indiquées.
     */
    public MessageLocalisation getMessageLocalisation(LangPreference langPreference, Locale formatLocale);

    /**
     * Retourne l'instance de MessageLocalisation à utiliser pour la traduction
     * des clés de localisation en fonction de la langue indiquée.
     */
    public MessageLocalisation getMessageLocalisation(Lang lang);

    public default MessageLocalisation getMessageLocalisation(LangPreference langPreference) {
        return getMessageLocalisation(langPreference, langPreference.getFirstLang().toLocale());
    }

    public default MessageLocalisation getMessageLocalisation(UserLangContext userLangContext) {
        return getMessageLocalisation(userLangContext.getLangPreference(), userLangContext.getFormatLocale());
    }

}
