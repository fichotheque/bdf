/* UtilLib - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;


/**
 *
 * @author Vincent Calame
 */
public class MessageLocalisationPack {

    private final InternalMessageLocalisation messageLocalisation = new InternalMessageLocalisation();
    private MessageLocalisation[] messageLocalisationArray = null;
    private int[] priorityArray = null;

    public MessageLocalisationPack() {
    }

    public void addMessageLocalisation(MessageLocalisation messageLocalisation, int priority) {
        if (messageLocalisation == null) {
            throw new NullPointerException("messageLocalisation is null");
        }
        if (messageLocalisationArray == null) {
            messageLocalisationArray = new MessageLocalisation[1];
            priorityArray = new int[1];
            messageLocalisationArray[0] = messageLocalisation;
            priorityArray[0] = priority;
        } else {
            int ln = messageLocalisationArray.length;
            int insertionIndex = ln;
            for (int i = 0; i < ln; i++) {
                if (priority >= priorityArray[i]) {
                    insertionIndex = i;
                    break;
                }
            }
            MessageLocalisation[] tempLoc = new MessageLocalisation[ln + 1];
            int[] tempPrior = new int[ln + 1];
            if (insertionIndex > 0) {
                System.arraycopy(messageLocalisationArray, 0, tempLoc, 0, insertionIndex);
                System.arraycopy(priorityArray, 0, tempPrior, 0, insertionIndex);
            }
            tempLoc[insertionIndex] = messageLocalisation;
            tempPrior[insertionIndex] = priority;
            if (insertionIndex < ln) {
                System.arraycopy(messageLocalisationArray, insertionIndex, tempLoc, insertionIndex + 1, ln - insertionIndex);
                System.arraycopy(priorityArray, insertionIndex, tempPrior, insertionIndex + 1, ln - insertionIndex);
            }
            messageLocalisationArray = tempLoc;
            priorityArray = tempPrior;
        }
    }

    public MessageLocalisation getMessageLocalisationInterface() {
        return messageLocalisation;
    }


    private class InternalMessageLocalisation implements MessageLocalisation {

        private InternalMessageLocalisation() {
        }

        @Override
        public String toString(String messageKey) {
            if (messageKey.isEmpty()) {
                return "";
            }
            if (messageLocalisationArray == null) {
                return null;
            }
            int length = messageLocalisationArray.length;
            for (int i = 0; i < length; i++) {
                MessageLocalisation messageLocalisation = messageLocalisationArray[i];
                String result = messageLocalisation.toString(messageKey);
                if (result != null) {
                    return result;
                }
            }
            return null;
        }

        @Override
        public String toString(Message message) {
            String messageKey = message.getMessageKey();
            if (messageKey.isEmpty()) {
                return LocalisationUtils.joinValues(message);
            }
            if (messageLocalisationArray == null) {
                return null;
            }
            int length = messageLocalisationArray.length;
            for (int i = 0; i < length; i++) {
                MessageLocalisation messageLocalisation = messageLocalisationArray[i];
                String result = messageLocalisation.toString(message);
                if (result != null) {
                    return result;
                }
            }
            return null;
        }

        @Override
        public boolean containsKey(String messageKey) {
            if (messageLocalisationArray == null) {
                return false;
            }
            int length = messageLocalisationArray.length;
            for (int i = 0; i < length; i++) {
                if (messageLocalisationArray[i].containsKey(messageKey)) {
                    return true;
                }
            }
            return false;
        }

    }

}
