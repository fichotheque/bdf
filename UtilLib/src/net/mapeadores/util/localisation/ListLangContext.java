/* UtilLib - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import java.util.List;
import java.util.Locale;
import java.util.RandomAccess;


/**
 * Une même langue est garantie n'apparaitre qu'une seule fois. Doit contenir au
 * moins un élément.
 *
 * @author Vincent Calame
 */
public interface ListLangContext extends LangContext, List<ListLangContext.Unit>, RandomAccess {

    @Override
    public default Lang getDefaultLang() {
        return get(0).getLang();
    }

    @Override
    public default Locale getDefaultFormatLocale() {
        return get(0).getFormatLocale();
    }


    public static interface Unit {

        public Lang getLang();

        public Locale getFormatLocale();

    }

}
