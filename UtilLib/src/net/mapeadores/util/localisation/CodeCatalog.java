/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import java.util.Set;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
public interface CodeCatalog {

    public Set<String> getLangCodeSet();

    public Set<String> getCountryCodeSet();

    public Set<String> getLangCodeSet(TextCondition condition, LangPreference langPreference);

    public Set<String> getCountryCodeSet(TextCondition condition, LangPreference langPreference);

}
