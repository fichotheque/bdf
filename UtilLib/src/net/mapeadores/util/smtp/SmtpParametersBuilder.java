/* UtilLib - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.smtp;

import net.mapeadores.util.models.EmailCore;


/**
 *
 * @author Vincent Calame
 */
public class SmtpParametersBuilder {

    private String authentificationType = SmtpParameters.AUTHENTIFICATION_NONE;
    private String host;
    private String username;
    private String password;
    private EmailCore defaultFrom;
    private int port = 25;

    public SmtpParametersBuilder() {
    }

    public SmtpParametersBuilder setAuthentificationType(String authentificationType) {
        switch (authentificationType) {
            case SmtpParameters.AUTHENTIFICATION_NONE:
            case SmtpParameters.AUTHENTIFICATION_BASIC:
            case SmtpParameters.AUTHENTIFICATION_SSL:
            case SmtpParameters.AUTHENTIFICATION_STARTTLS:
                this.authentificationType = authentificationType;
                break;
            default:
                throw new IllegalArgumentException("Unknown authentification type : " + authentificationType);
        }
        return this;
    }

    public SmtpParametersBuilder setHost(String host) {
        this.host = host;
        return this;
    }

    public SmtpParametersBuilder setPort(int port) {
        if (port < 1) {
            this.port = 25;
        } else {
            this.port = port;
        }
        return this;
    }

    public SmtpParametersBuilder setUsername(String username) {
        this.username = username;
        return this;
    }

    public SmtpParametersBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public SmtpParametersBuilder setDefaultFrom(EmailCore defaultFrom) {
        this.defaultFrom = defaultFrom;
        return this;
    }

    public SmtpParameters toSmtpParameters() {
        return new InternalSmtpParameters(authentificationType, host, username, password, defaultFrom, port);
    }

    public static SmtpParametersBuilder init() {
        return new SmtpParametersBuilder();
    }


    private static class InternalSmtpParameters implements SmtpParameters {

        private final String authentificationType;
        private final String host;
        private final String username;
        private final String password;
        private final EmailCore defaultFrom;
        private final int port;

        private InternalSmtpParameters(String authentificationType, String host, String username, String password,
                EmailCore defaultFrom, int port) {
            this.authentificationType = authentificationType;
            this.host = host;
            this.username = username;
            this.password = password;
            this.defaultFrom = defaultFrom;
            this.port = port;
        }

        @Override
        public String getAuthentificationType() {
            return authentificationType;
        }

        @Override
        public String getHost() {
            return host;
        }

        @Override
        public int getPort() {
            return port;
        }

        @Override
        public String getUsername() {
            return username;
        }

        @Override
        public String getPassword() {
            return password;
        }

        @Override
        public EmailCore getDefaultFrom() {
            return defaultFrom;
        }

    }

}
