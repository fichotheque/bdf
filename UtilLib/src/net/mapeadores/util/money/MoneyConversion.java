/* UtilLib - Copyright (c) 2012-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.money;

import net.mapeadores.util.primitives.Decimal;


/**
 * Conversion entre deux monnaies, une étant la monnaie pivot et la seconde
 * étant la monnaie autre.
 *
 * @author Vincent Calame
 */
public interface MoneyConversion {

    /**
     * Retourne le taux de conversion entre la monnaie autre et la monnaie
     * pivot. Une unité dans la monnaie autre est égal à la valeur de
     * getConversionRate() dans la monnaie pivot. 1 * otherCurrency =
     * conversionRate * pivotCurrency
     *
     * @return le taux de conversion
     */
    public Decimal getConversionRate();

    /**
     * Convertit un montant dans la monnaie pivot vers la monnaie autre.
     * pivotMoneyLong peut être négatif.
     *
     * @param pivotMoneyLong montant dans la monnaie pivot à convertir
     * @return le montant convertit
     */
    public long convertFromPivot(long pivotMoneyLong);

    /**
     * Convertit un montant dans la monnaie autre vers la monnaie pivot.
     * otherMoneyLong peut être négatif.
     *
     * @param otherMoneyLong montant dans la monnaie autre à convertir
     * @return le montant convertit
     */
    public long convertToPivot(long otherMoneyLong);

}
