/* UtilLib - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface Labels extends List<Label>, RandomAccess {

    public Label getLabel(Lang lang);

    public default Label getFirstLabel() {
        if (isEmpty()) {
            return null;
        } else {
            return get(0);
        }
    }

    public default Label getLangPartCheckedLabel(Lang lang) {
        Label label = getLabel(lang);
        if ((label == null) && (!lang.isRootLang())) {
            label = getLabel(lang.getRootLang());
        }
        return label;
    }

    public default String seekLabelString(Lang preferredLang, String defaultString) {
        Label label = getLangPartCheckedLabel(preferredLang);
        if (label == null) {
            label = getFirstLabel();
        }
        if (label != null) {
            return label.getLabelString();
        } else {
            return defaultString;
        }
    }

}
