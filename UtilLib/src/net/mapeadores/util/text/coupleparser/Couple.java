/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.coupleparser;


/**
 *
 * @author Vincent Calame
 */
public abstract class Couple {

    private int lineNumber;
    private String value;

    public Couple(int lineNumber, String value) {
        this.lineNumber = lineNumber;
        this.value = value;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String getValue() {
        return value;
    }

}
