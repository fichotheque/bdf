/* UtilLib - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.io.UnsupportedEncodingException;


/**
 * Exception renvoyée par UTF8Brige.toUTF8() si la table de caractères n'est pas
 * connue. Cette exception étend la classe RuntimeException afin de ne pas avoir
 * besoin d'être traitée. En effet, cette exception a très peu de probabilité
 * d'arriver et si elle arrive c'est dû au client.
 *
 * @author Vincent Calame
 */
public class UTF8BridgeException extends RuntimeException {

    public UTF8BridgeException(String charsetName, UnsupportedEncodingException uue) {
        super("charset = " + charsetName + " is not supported", uue);
    }

}
