/* UtilLib - Copyright (c) 2017-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.text.ParseException;


/**
 *
 * @author Vincent Calame
 */
public final class ValidExtension implements CharSequence {

    public final static ValidExtension ODT = new ValidExtension("odt");

    private final String string;

    private ValidExtension(String string) {
        this.string = string;
    }

    @Override
    public char charAt(int index) {
        return string.charAt(index);
    }

    @Override
    public int length() {
        return string.length();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return string.subSequence(start, end);
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public int hashCode() {
        return string.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        ValidExtension otherValidExtension = (ValidExtension) other;
        return otherValidExtension.string.equals(this.string);
    }

    public static ValidExtension parse(String extension) throws ParseException {
        StringUtils.checkTechnicalName(extension, false);
        return new ValidExtension(extension);
    }

}
