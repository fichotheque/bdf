/* UtilLib - Copyright (c) 2012-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.alphabet;


/**
 *
 * @author Vincent Calame
 */
public interface AlphabeticEntry {

    public String getSourceString();

    public boolean isWithArticle();

    public String getArticleString();

    public boolean isWithSeparationSpace();

    public String getEntryString();

    public String getInitialChar();

    public String getAlphabeticSort();

}
