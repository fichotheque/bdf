/* UtilLib - Copyright (c) 2006-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;


/**
 *
 * @author Vincent Calame
 */
public class DateFormatBundle {

    private final static Map<Locale, DateFormatBundle> cacheMap = new HashMap<Locale, DateFormatBundle>();
    public final static String S1_KEy = "s1";
    public final static String S2_KEy = "s2";
    public final static String T1_KEy = "t1";
    public final static String T2_KEy = "t2";
    public final static String T3_KEy = "t3";
    public final static String T4_KEy = "t4";
    private final DateFormat datejour;
    private final DateFormat datemois;
    private final DateFormat dateannee;
    private final DateFormat parserdate;
    private final ResourceBundle datationBundle;

    private DateFormatBundle(Locale locale) {
        parserdate = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        datejour = DateFormat.getDateInstance(DateFormat.LONG, locale);
        datemois = new SimpleDateFormat("MMMM yyyy", locale);
        dateannee = new SimpleDateFormat("yyyy", locale);
        datationBundle = ResourceBundle.getBundle("net.mapeadores.util.text.resources.DateResourceBundle", locale);
    }

    public static DateFormatBundle getDateFormatBundle(Locale locale) {
        DateFormatBundle dateFormatBundle = cacheMap.get(locale);
        if (dateFormatBundle == null) {
            synchronized (cacheMap) {
                dateFormatBundle = new DateFormatBundle(locale);
                cacheMap.put(locale, dateFormatBundle);
            }
        }
        return dateFormatBundle;
    }

    public DateFormat getFormatAnnee() {
        return dateannee;
    }

    public DateFormat getFormatJour() {
        return datejour;
    }

    public DateFormat getFormatMois() {
        return datemois;
    }

    public DateFormat getParseFormat() {
        return parserdate;
    }

    public String getLocalizedString(String key) {
        return datationBundle.getString(key);
    }

}
