/* UtilLib - Copyright (c) 2013-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.search;

import net.mapeadores.util.text.TextConstants;


/**
 *
 * @author Vincent Calame
 */
public class SearchUtils {

    private SearchUtils() {
    }

    public static SimpleSearchToken toSimpleSearchToken(String tokenString, int searchType) {
        return new DefaultSimpleSearchToken(tokenString, searchType);
    }

    public static MultiSearchToken toMultiSearchToken(SimpleSearchToken[] array) {
        return new ArrayMultiSearchToken(array);
    }

    public static String toCompleteString(SimpleSearchToken token) {
        String tokenString = token.getTokenString();
        int searchType = token.getSearchType();
        if (searchType == TextConstants.MATCHES) {
            return tokenString;
        } else {
            StringBuilder buf = new StringBuilder(tokenString.length() + 2);
            if ((searchType & TextConstants.ENDSWITH) != 0) {
                buf.append('*');
            }
            buf.append(tokenString);
            if ((searchType & TextConstants.STARTSWITH) != 0) {
                buf.append('*');
            }
            return buf.toString();
        }
    }


    private static class DefaultSimpleSearchToken implements SimpleSearchToken {

        private final int searchType;
        private final String tokenString;

        private DefaultSimpleSearchToken(String tokenString, int searchType) {
            this.tokenString = tokenString;
            this.searchType = searchType;
        }

        @Override
        public int getSearchType() {
            return searchType;
        }

        @Override
        public String getTokenString() {
            return tokenString;
        }

    }


    private static class ArrayMultiSearchToken implements MultiSearchToken {

        private final SimpleSearchToken[] subtokenArray;

        public ArrayMultiSearchToken(SimpleSearchToken[] subtokenArray) {
            this.subtokenArray = subtokenArray;
        }

        @Override
        public int getSubtokenCount() {
            return subtokenArray.length;
        }

        @Override
        public SimpleSearchToken getSubtoken(int i) {
            return subtokenArray[i];
        }

    }

}
