/* UtilLib - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.resources;

import java.util.ListResourceBundle;


/**
 *
 * @author Vincent Calame
 */
public class DateResourceBundle_fr extends ListResourceBundle {

    public Object[][] getContents() {
        return contents;
    }

    static final Object[][] contents = {
        // LOCALIZE THIS
        {"s1", "1er semestre"},
        {"s2", "2nd semestre"},
        {"t1", "1er trimestre"},
        {"t2", "2ème trimestre"},
        {"t3", "3ème trimestre"},
        {"t4", "4ème trimestre"}
    // END OF MATERIAL TO LOCALIZE
    };
}
