/* UtilLib - Copyright (c) 2007-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.tableparser;

import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class CsvParameters implements TableParser.Parameters {

    private char delimiter = ',';
    private char quote = '"';
    private boolean escapedCSV = false;

    public CsvParameters() {
    }

    public char getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(char delimiter) {
        switch (delimiter) {
            case ',':
            case ';':
            case '\t':
            case '|':
                this.delimiter = delimiter;
                break;
            default:
                throw new IllegalArgumentException("Wrong delimiter value = " + delimiter);
        }
    }

    public char getQuote() {
        return quote;
    }

    public void setQuote(char quote) {
        switch (quote) {
            case '\'':
            case '"':
                this.quote = quote;
                break;
            default:
                throw new IllegalArgumentException("Wrong quote value = " + quote);
        }
    }

    public boolean isEscapedCSV() {
        return escapedCSV;
    }

    public void setEscapedCSV(boolean escapedCSV) {
        this.escapedCSV = escapedCSV;
    }

    public static char getDefaultDelimiter() {
        return ',';
    }

    public static char getDefaultQuote() {
        return '"';
    }

    public static char[] getAvailableDelimiterArray() {
        char[] caracs = {',', ';', '\t', '|'};
        return caracs;
    }

    public static char[] getAvailableQuoteArray() {
        char[] caracs = {'\'', '\"'};
        return caracs;
    }

    public static String[] getAvalaibleDelimiterArrayToString() {
        return StringUtils.toStringArray(getAvailableDelimiterArray());
    }

    public static String[] getAvalaibleQuoteArrayToString() {
        return StringUtils.toStringArray(getAvailableQuoteArray());
    }

}
