/* UtilLib - Copyright (c) 2017-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public class PhrasesBuilder {

    private final Map<String, LabelChangeBuilder> builderMap = new LinkedHashMap<String, LabelChangeBuilder>();

    public PhrasesBuilder() {

    }

    public LabelChangeBuilder getPhraseBuilder(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name is null");
        }
        LabelChangeBuilder builder = builderMap.get(name);
        if (builder == null) {
            builder = new LabelChangeBuilder();
            builderMap.put(name, builder);
        }
        return builder;
    }

    public Phrases toPhrases() {
        Map<String, Phrase> phraseMap = new HashMap<String, Phrase>();
        int size = builderMap.size();
        Phrase[] phraseArray = new Phrase[size];
        int p = 0;
        for (Map.Entry<String, LabelChangeBuilder> entry : builderMap.entrySet()) {
            String name = entry.getKey();
            Phrase phrase = entry.getValue().toPhrase(name);
            phraseMap.put(name, phrase);
            phraseArray[p] = phrase;
            p++;
        }
        return new InternalPhrases(phraseArray, phraseMap);
    }

    public static Phrases toPhrases(Map<String, PhraseCache> cacheMap) {
        List<Phrase> tempList = new ArrayList<Phrase>();
        Map<String, Phrase> phraseMap = new HashMap<String, Phrase>();
        for (Map.Entry<String, PhraseCache> entry : cacheMap.entrySet()) {
            String name = entry.getKey();
            if (name != null) {
                Phrase phrase = entry.getValue().getPhrase();
                tempList.add(phrase);
                phraseMap.put(name, phrase);
            }
        }
        Phrase[] phraseArray = tempList.toArray(new Phrase[tempList.size()]);
        return new InternalPhrases(phraseArray, phraseMap);
    }


    private static class InternalPhrases extends AbstractList<Phrase> implements Phrases {

        private final Phrase[] phraseArray;
        private final Map<String, Phrase> phraseMap;

        private InternalPhrases(Phrase[] phraseArray, Map<String, Phrase> phraseMap) {
            this.phraseArray = phraseArray;
            this.phraseMap = phraseMap;
        }

        @Override
        public int size() {
            return phraseArray.length;
        }

        @Override
        public Phrase get(int index) {
            return phraseArray[index];
        }

        @Override
        public Phrase getPhrase(String name) {
            return phraseMap.get(name);
        }

    }

}
