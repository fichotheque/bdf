/* UtilLib - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public final class RelativePath implements CharSequence {

    public final static RelativePath EMPTY = new RelativePath("");
    private final String path;

    private RelativePath(String path) {
        this.path = path;
    }

    @Override
    public char charAt(int index) {
        return path.charAt(index);
    }

    @Override
    public int length() {
        return path.length();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return path.subSequence(start, end);
    }

    @Override
    public String toString() {
        return path;
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        RelativePath otherPath = (RelativePath) other;
        return otherPath.path.equals(this.path);
    }

    public String getLastName() {
        int idx = path.lastIndexOf('/');
        if (idx == -1) {
            return path;
        } else {
            return path.substring(idx + 1);
        }
    }

    public String getParentPath() {
        int idx = path.lastIndexOf('/');
        if (idx == -1) {
            return "";
        } else {
            return path.substring(0, idx + 1);
        }
    }

    public boolean isEmpty() {
        return path.isEmpty();
    }

    public boolean hasParent() {
        return (path.indexOf('/') != -1);
    }

    public String getPath() {
        return path;
    }


    public String[] toArray() {
        if (path.length() == 0) {
            return new String[0];
        }
        int idx = path.indexOf("/");
        if (idx == -1) {
            String[] result = {path};
            return result;
        }
        List<String> partList = new ArrayList<String>();
        partList.add(path.substring(0, idx));
        while (true) {
            int idx2 = path.indexOf("/", idx + 1);
            if (idx2 == -1) {
                String part = path.substring(idx + 1);
                if (part.length() > 0) {
                    partList.add(part);
                }
                break;
            } else {
                partList.add(path.substring(idx + 1, idx2));
                idx = idx2;
            }
        }
        return partList.toArray(new String[partList.size()]);
    }

    public RelativePath buildChild(String childPath) {
        if (path.isEmpty()) {
            return build(childPath);
        } else {
            return build(path + "/" + childPath);
        }
    }

    public RelativePath parseChild(String childPath) throws ParseException {
        if (path.isEmpty()) {
            return parse(childPath);
        } else {
            return parse(path + "/" + childPath);
        }
    }

    public static RelativePath parse(String path) throws ParseException {
        if (path == null) {
            return EMPTY;
        }
        path = path.trim();
        if (path.isEmpty()) {
            return EMPTY;
        }
        String normalizedPath = StringUtils.normalizeRelativePath(path);
        if (normalizedPath == null) {
            throw new ParseException("Malformed: " + path, 0);
        }
        return new RelativePath(normalizedPath);
    }

    public static RelativePath build(String path) {
        try {
            return parse(path);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe.getMessage());
        }
    }

}
