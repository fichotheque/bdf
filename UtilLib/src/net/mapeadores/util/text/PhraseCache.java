/* UtilLib - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.util.LinkedHashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class PhraseCache {

    private final Map<Lang, Label> labelMap = new LinkedHashMap<Lang, Label>();
    private final String name;
    private Phrase cachedPhrase;

    public PhraseCache(String name) {
        this.name = name;
    }

    public synchronized void clear() {
        labelMap.clear();
        cachedPhrase = null;
    }

    public synchronized void putLabels(Labels labels) {
        for (Label label : labels) {
            labelMap.put(label.getLang(), label);
        }
        cachedPhrase = null;
    }

    public synchronized boolean putLabel(Label label) {
        Lang lang = label.getLang();
        Label current = labelMap.get(lang);
        if (current != null) {
            if (LabelUtils.areEquals(current, label)) {
                return false;
            }
        }
        labelMap.put(lang, label);
        cachedPhrase = null;
        return true;
    }

    public synchronized boolean removeLabel(Lang lang) {
        Label label = labelMap.remove(lang);
        if (label != null) {
            cachedPhrase = null;
            return true;
        } else {
            return false;
        }
    }

    public Phrase getPhrase() {
        Phrase result = cachedPhrase;
        if (result == null) {
            result = cache();
        }
        return result;
    }

    public boolean isEmpty() {
        return labelMap.isEmpty();
    }

    private synchronized Phrase cache() {
        Phrase cache = LabelUtils.toPhrase(name, labelMap);
        cachedPhrase = cache;
        return cache;
    }

}
