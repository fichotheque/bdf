/* UtilLib - Copyright (c) 2005-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;


/**
 *
 *
 * @author Vincent Calame
 */
public interface TextConstants {

    public final static int CONTAINS = 3;
    public final static int MATCHES = 0;
    public final static int ENDSWITH = 2;
    public final static int STARTSWITH = 1;

}
