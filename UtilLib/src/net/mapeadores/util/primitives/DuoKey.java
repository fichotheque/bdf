/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;


/**
 *
 * @author Vincent Calame
 */
public final class DuoKey {

    private final int int1;
    private final int int2;

    public DuoKey(int int1, int int2) {
        this.int1 = int1;
        this.int2 = int2;
    }

    public int getInt1() {
        return int1;
    }

    public int getInt2() {
        return int2;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof DuoKey)) {
            return false;
        }
        DuoKey other = (DuoKey) o;
        return ((other.int1 == this.int1) && (other.int2 == this.int2));
    }

    /**
     * Même hashCode que Long
     */
    @Override
    public int hashCode() {
        return int1 ^ int2;
    }

}
