/* UtilLib - Copyright (c) 2009-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives.io;

import java.io.DataInput;
import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class DataInputPrimitivesReader implements PrimitivesReader {

    private final DataInput dataInput;

    public DataInputPrimitivesReader(DataInput dataInput) {
        this.dataInput = dataInput;
    }

    @Override
    public int readInt() throws IOException {
        return dataInput.readInt();
    }

    @Override
    public short readShort() throws IOException {
        return dataInput.readShort();
    }

    @Override
    public boolean readBoolean() throws IOException {
        return dataInput.readBoolean();
    }

    @Override
    public String readString() throws IOException {
        String s = dataInput.readUTF();
        if (s.equals("\u0000")) {
            return null;
        } else {
            return s;
        }
    }

    @Override
    public char readChar() throws IOException {
        return dataInput.readChar();
    }

    @Override
    public double readDouble() throws IOException {
        return dataInput.readDouble();
    }

    @Override
    public long readLong() throws IOException {
        return dataInput.readLong();
    }

    @Override
    public byte readByte() throws IOException {
        return dataInput.readByte();
    }

    @Override
    public void read(byte[] buffer, int offset, int length) throws IOException {
        dataInput.readFully(buffer, offset, length);
    }

    @Override
    public float readFloat() throws IOException {
        return dataInput.readFloat();
    }

    @Override
    public int skipBytes(int length) throws IOException {
        return dataInput.skipBytes(length);
    }

}
