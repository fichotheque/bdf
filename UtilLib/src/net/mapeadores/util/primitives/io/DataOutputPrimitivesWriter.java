/* UtilLib - Copyright (c) 2006-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives.io;

import java.io.DataOutput;
import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class DataOutputPrimitivesWriter implements PrimitivesWriter {

    private final DataOutput dataOutput;

    public DataOutputPrimitivesWriter(DataOutput dataOutput) {
        this.dataOutput = dataOutput;
    }

    @Override
    public void writeInt(int i) throws IOException {
        dataOutput.writeInt(i);
    }

    @Override
    public void writeShort(short s) throws IOException {
        dataOutput.writeShort(s);
    }

    @Override
    public void writeBoolean(boolean b) throws IOException {
        dataOutput.writeBoolean(b);
    }

    @Override
    public void writeString(String s) throws IOException {
        if (s == null) {
            dataOutput.writeUTF("\u0000");
        } else {
            dataOutput.writeUTF(s);
        }
    }

    @Override
    public void writeChar(char carac) throws IOException {
        dataOutput.writeChar(carac);
    }

    @Override
    public void writeDouble(double d) throws IOException {
        dataOutput.writeDouble(d);
    }

    @Override
    public void writeLong(long l) throws IOException {
        dataOutput.writeLong(l);
    }

    @Override
    public void writeByte(byte b) throws IOException {
        dataOutput.writeByte(b);
    }

    @Override
    public void writeFloat(float f) throws IOException {
        dataOutput.writeFloat(f);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        dataOutput.write(buffer, offset, length);
    }

}
