/* UtilLib - Copyright (c) 2006-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;


/**
 *
 * @author Vincent Calame
 */
public final class FileLength {

    public final static short KB_ROUND = 1;
    public final static short MB_ROUND = 2;
    public final static FileLength EMPTY_FILELENGTH = new FileLength(0);
    float roundedValue = 0;
    short roundType = KB_ROUND;
    long filelength;

    public FileLength(long filelength) {
        if (filelength < 0) {
            throw new IllegalArgumentException("fileLength < 0");
        }
        this.filelength = filelength;
        initRound(filelength);
    }

    public long getValue() {
        return filelength;
    }

    public float getRoundedValue() {
        return roundedValue;
    }

    public short getRoundType() {
        return roundType;
    }

    private void initRound(long l) {
        if (l == 0) {
            return;
        }
        float f = ((float) l) / 1024f;
        float f2 = arrondit(f);
        if (f2 != -1) {
            roundedValue = f2;
        } else {
            roundType = MB_ROUND;
            f = f / 1024f;
            float f3 = arrondit(f);
            if (f3 != -1) {
                roundedValue = f3;
            } else {
                roundedValue = Math.round(f3);
            }
        }
    }

    float arrondit(float f) {
        if (f < 10) {
            f = ((float) Math.round(f * 10)) / 10;
            if (f == 0) {
                f = 0.1f;
            }
            return f;
        } else if (f < 100) {
            f = Math.round(f);
            return f;
        } else if (f < 1000) {
            f = ((float) Math.round(f / 10)) * 10;
            return f;
        }
        return -1;
    }

}
