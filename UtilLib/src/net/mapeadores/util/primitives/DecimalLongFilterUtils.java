/* UtilLib - Copyright (c) 2012-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;

import java.text.ParseException;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class DecimalLongFilterUtils {

    private DecimalLongFilterUtils() {
    }

    public static DecimalLongFilter parseDecimalLongFilter(String s) throws ParseException {
        String[] tokens = StringUtils.getTechnicalTokens(s, ';', false);
        int length = tokens.length;
        if (length == 0) {
            return null;
        }
        if (length > 2) {
            throw new ParseException("Too many tokens", s.length() - 1);
        }
        if (length == 1) {
            try {
                Decimal decimal = StringUtils.parseDecimal(tokens[0]);
                return DecimalLongFilterFactory.newInstance(DecimalLong.toDecimalLong(decimal));
            } catch (NumberFormatException nfe) {
                throw new ParseException("Wrong decimal: " + tokens[0], 0);
            }
        } else {
            long long1;
            long long2;
            try {
                Decimal decimal1 = StringUtils.parseDecimal(tokens[0]);
                long1 = DecimalLong.toDecimalLong(decimal1);
            } catch (NumberFormatException nfe) {
                throw new ParseException("Wrong decimal: " + tokens[0], 0);
            }
            try {
                Decimal decimal2 = StringUtils.parseDecimal(tokens[1]);
                long2 = DecimalLong.toDecimalLong(decimal2);
            } catch (NumberFormatException nfe) {
                throw new ParseException("Wrong decimal: " + tokens[1], 0);
            }
            return DecimalLongFilterFactory.newInstance(long1, long2);
        }
    }

}
