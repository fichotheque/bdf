/* UtilLib - Copyright (c) 2005-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;

import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
public final class Range implements Comparable<Range>, Serializable {

    private static final long serialVersionUID = 1L;
    public static final int MAXIMUM = 0x7FFFFFFF;
    public static final int MINIMUM = 0xFFFFFFFF;
    private final int min, max;

    /**
     */
    public Range(int min, int max) {
        if (max < min) {
            this.max = min;
            this.min = max;
        } else {
            this.min = min;
            this.max = max;
        }
    }

    public int min() {
        return min;
    }

    public int max() {
        return max;
    }

    public int length() {
        return max - min + 1;
    }

    public Range headRange(int newMax) {
        if (newMax < min) {
            return null;
        }
        if (newMax >= max) {
            return this;
        }
        return new Range(min, newMax);

    }

    public Range headRange(Range Range) {
        return headRange(Range.max - 1);
    }

    public Range tailRange(int newMin) {
        if (newMin > max) {
            return null;
        }
        if (newMin <= min) {
            return this;
        }
        return new Range(newMin, max);
    }

    public Range tailRange(Range Range) {
        return tailRange(Range.min + 1);
    }

    public boolean intersects(Range Range) {
        if (this.max < Range.min) {
            return false;
        }
        if (this.min > Range.max) {
            return false;
        }
        return true;
    }

    public boolean isNeighbour(Range range) {
        if (this.max == (range.min - 1)) {
            return true;
        }
        if (this.min == (range.max + 1)) {
            return true;
        }
        return false;
    }

    public Range intersection(Range range) {
        if (!intersects(range)) {
            return null;
        }
        return new Range(Math.max(this.min, range.min), Math.min(this.max, range.max));
    }

    public Range union(Range range) {
        return new Range(Math.min(min, range.min), Math.max(max, range.max));
    }

    public boolean contains(Range Range) {
        return ((this.min <= Range.min) && (this.max >= Range.max));
    }

    public boolean contains(int value) {
        return ((value >= min) && (value <= max));
    }

    @Override
    public int hashCode() {
        return max;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if ((obj == null) || (!(obj instanceof Range))) {
            return false;
        }
        Range Range = (Range) obj;
        return (Range.max == this.max) && (Range.min == this.min);
    }

    @Override
    public String toString() {
        if (min == max) {
            return String.valueOf(min);
        }
        return min + "-" + max;
    }

    @Override
    public int compareTo(Range other) {
        if (this.min < other.min) {
            return -1;
        }
        if (this.min > other.min) {
            return 1;
        }
        if (this.max < other.max) {
            return -1;
        }
        if (this.max > other.max) {
            return 1;
        }
        return 0;
    }

}
