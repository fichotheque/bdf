/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;

import java.util.List;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public interface Longs extends List<Long>, RandomAccess {

    public long getLong(int i);

}
