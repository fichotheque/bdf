/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;

/**
 *
 * @author Vincent Calame
 */
public class DegrePoint {

    private final DegreDecimal latitude;
    private final DegreDecimal longitude;

    public DegrePoint(DegreDecimal latitude, DegreDecimal longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public DegreDecimal getLatitude() {
        return latitude;
    }

    public DegreDecimal getLongitude() {
        return longitude;
    }
}
