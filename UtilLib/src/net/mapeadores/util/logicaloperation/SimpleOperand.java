/* UtilLib - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logicaloperation;


/**
 *
 * @author Vincent Calame
 */
public interface SimpleOperand extends Operand {

    public boolean isAcceptMode();

    public String getOperandString();

    /**
     * Partie composée de caractères valides ([-_.,!a-zA-Z0-9]) jusqu'au dernier
     * deux-points. Jamais nul mais de longueur nulle si scope n'est pas défini.
     */
    public String getScope();

    /**
     * Partie après le dernier deux-points. Égal à getOperandString si
     * getScope().length() == 0.
     */
    public String getBody();

}
