/* UtilLib - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logicaloperation;


/**
 *
 * @author Vincent Calame
 */
public class LogicalOperationParser {

    private LogicalOperationParser() {
    }

    /**
     * Les opérandes résultantes sont des instances de SubOperand ou de SimpleOperand
     */
    public static Operand parse(String operationString) throws OperandException {
        InformatiqueParser parser = new InformatiqueParser(operationString);
        try {
            parser.parse();
            return parser.toOperand();
        } catch (RuntimeOperandException oe) {
            throw new OperandException(oe);
        }
    }

}
