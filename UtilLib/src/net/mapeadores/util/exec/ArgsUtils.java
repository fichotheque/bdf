/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exec;

import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public final class ArgsUtils {

    private ArgsUtils() {

    }

    public static List<CommandDef> wrap(CommandDef[] array) {
        return new CommandDefList(array);
    }

    public static List<ParameterDef> wrap(ParameterDef[] array) {
        return new ParameterDefList(array);
    }

    public static List<CommandDef.UsedParameter> wrap(CommandDef.UsedParameter[] array) {
        return new UsedParameterList(array);
    }


    private static class CommandDefList extends AbstractList<CommandDef> implements RandomAccess {

        private final CommandDef[] array;

        private CommandDefList(CommandDef[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public CommandDef get(int index) {
            return array[index];
        }

    }


    private static class ParameterDefList extends AbstractList<ParameterDef> implements RandomAccess {

        private final ParameterDef[] array;

        private ParameterDefList(ParameterDef[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ParameterDef get(int index) {
            return array[index];
        }

    }


    private static class UsedParameterList extends AbstractList<CommandDef.UsedParameter> implements RandomAccess {

        private final CommandDef.UsedParameter[] array;

        private UsedParameterList(CommandDef.UsedParameter[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public CommandDef.UsedParameter get(int index) {
            return array[index];
        }

    }

}
