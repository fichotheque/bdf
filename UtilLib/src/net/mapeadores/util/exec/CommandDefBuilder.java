/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exec;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public class CommandDefBuilder {

    private final String name;
    private final Map<Character, InternalUsedParameter> usedParameterMap = new LinkedHashMap<Character, InternalUsedParameter>();
    private String titleLocKey;

    public CommandDefBuilder(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name is null");
        }
        this.name = name;
    }

    public CommandDefBuilder addUsedParameter(char letter, boolean mandatory) {
        usedParameterMap.put(letter, new InternalUsedParameter(letter, mandatory));
        return this;
    }

    public CommandDefBuilder setTitleLocKey(String titleLocKey) {
        this.titleLocKey = titleLocKey;
        return this;
    }

    public CommandDef toCommandDef() {
        String finalTitleLocKey = titleLocKey;
        if ((finalTitleLocKey == null) || (finalTitleLocKey.isEmpty())) {
            finalTitleLocKey = name;
        }
        List<CommandDef.UsedParameter> usedParameterList = ArgsUtils.wrap(usedParameterMap.values().toArray(new CommandDef.UsedParameter[usedParameterMap.size()]));
        return new InternalCommandDef(name, finalTitleLocKey, usedParameterList);
    }

    public static CommandDefBuilder init(String name) {
        return new CommandDefBuilder(name);
    }


    private static class InternalCommandDef implements CommandDef {

        private final String name;
        private final String titleLocKey;
        private final List<UsedParameter> usedParameterList;

        public InternalCommandDef(String name, String titleLocKey, List<UsedParameter> usedParameterList) {
            this.name = name;
            this.titleLocKey = titleLocKey;
            this.usedParameterList = usedParameterList;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getTitleLocKey() {
            return titleLocKey;
        }

        @Override
        public List<UsedParameter> getUsedParameterList() {
            return usedParameterList;
        }

    }


    private static class InternalUsedParameter implements CommandDef.UsedParameter {

        private final char letter;
        private final boolean mandatory;

        private InternalUsedParameter(char letter, boolean mandatory) {
            this.letter = letter;
            this.mandatory = mandatory;
        }

        @Override
        public char getLetter() {
            return letter;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

    }

}
