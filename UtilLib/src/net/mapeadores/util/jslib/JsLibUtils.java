/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.jslib;

import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public final class JsLibUtils {

    public final static List<JsLib> EMPTY_JSLIBLIST = Collections.emptyList();
    public final static List<ThirdLib> EMPTY_THIRDLIBLIST = Collections.emptyList();
    public final static List<TemplateFamily> EMPTY_TEMPLATEFAMILYLIST = Collections.emptyList();

    private JsLibUtils() {

    }

    public static List<JsLib> wrap(JsLib[] array) {
        return new JsLibList(array);
    }

    public static List<ThirdLib> wrap(ThirdLib[] array) {
        return new ThirdLibList(array);
    }

    public static List<TemplateFamily> wrap(TemplateFamily[] array) {
        return new TemplateFamilyList(array);
    }


    private static class JsLibList extends AbstractList<JsLib> implements RandomAccess {

        private final JsLib[] array;

        private JsLibList(JsLib[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public JsLib get(int index) {
            return array[index];
        }

    }


    private static class ThirdLibList extends AbstractList<ThirdLib> implements RandomAccess {

        private final ThirdLib[] array;

        private ThirdLibList(ThirdLib[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ThirdLib get(int index) {
            return array[index];
        }

    }


    private static class TemplateFamilyList extends AbstractList<TemplateFamily> implements RandomAccess {

        private final TemplateFamily[] array;

        private TemplateFamilyList(TemplateFamily[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public TemplateFamily get(int index) {
            return array[index];
        }

    }

}
