/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.jslib;


/**
 *
 * @author Vincent Calame
 */
public class TemplateFamilyBuilder {

    private final String name;
    private Object originObject = null;

    public TemplateFamilyBuilder(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name is null");
        }
        this.name = name;
    }

    public TemplateFamilyBuilder setOriginObject(Object originObject) {
        this.originObject = originObject;
        return this;
    }

    public TemplateFamily toTemplateFamily() {
        return new InternalTemplateFamily(name, originObject);
    }

    public static TemplateFamilyBuilder init(String name) {
        return new TemplateFamilyBuilder(name);
    }


    private static class InternalTemplateFamily implements TemplateFamily {

        private final String name;
        private final Object originObject;

        private InternalTemplateFamily(String name, Object originObject) {
            this.name = name;
            this.originObject = originObject;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Object getOriginObject() {
            return originObject;
        }

    }

    /*
    public final static short CORE_TYPE = 1;
    public final static short EXTENSION_TYPE = 2;
    public final static short APP_TYPE = 3;
    private final String familyName;
    private final short originType;
    private final String originName;


    public TemplateFamily(String familyName) {
        this.familyName = familyName;
        this.originType = CORE_TYPE;
        this.originName = null;
    }

    public TemplateFamily(String familyName, short originType, String originName) {
        this.familyName = familyName;
        this.originType = originType;
        this.originName = originName;
    }

    public String getName() {
        return familyName;
    }

    public short getOriginType() {
        return originType;
    }

    @Nullable
    public String getOriginName() {
        return originName;
    }
     */

}
