/* UtilLib - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conditions;


/**
 *
 * @author Vincent Calame
 */
public interface TextTest {

    public short getTestType();

    public String getText();

    public default String getCompleteText() {
        String text = getText();
        switch (getTestType()) {
            case ConditionsConstants.STARTSWITH_TEST:
                return text + "*";
            case ConditionsConstants.ENDSWITH_TEST:
                return "*" + text;
            case ConditionsConstants.CONTAINS_TEST:
                return "*" + text + "*";
            case ConditionsConstants.MATCHES_TEST:
                return text;
            case ConditionsConstants.EMPTY_TEST:
                return "!*";
            case ConditionsConstants.NOT_STARTSWITH_TEST:
                return "!" + text + "*";
            case ConditionsConstants.NOT_ENDSWITH_TEST:
                return "!*" + text;
            case ConditionsConstants.NOT_CONTAINS_TEST:
                return "!*" + text + "*";
            case ConditionsConstants.NOT_MATCHES_TEST:
                return "!" + text;
            case ConditionsConstants.NOT_EMPTY_TEST:
                return "*";
            default:
                return "";
        }
    }

}
