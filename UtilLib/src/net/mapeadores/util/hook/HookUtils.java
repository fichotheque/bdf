/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.hook;


/**
 *
 * @author Vincent Calame
 */
public final class HookUtils {

    public final static HookHandler NONE_HANDLER = new NoneHookHandler();

    private HookUtils() {

    }


    private static class NoneHookHandler implements HookHandler {

        private NoneHookHandler() {
        }

        @Override
        public void handle(String hookName, Object... arguments) {
        }

    }

}
