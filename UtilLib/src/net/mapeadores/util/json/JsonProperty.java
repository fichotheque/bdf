/* UtilLib - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.json;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public interface JsonProperty {


    public String getName();

    public void writeValue(JSONWriter jw) throws IOException;

    public default void write(JSONWriter jw) throws IOException {
        jw.key(getName());
        writeValue(jw);
    }

}
