/* FichothequeLib_Tools - Copyright (c) 2016-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.misc;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.PhrasesBuilder;


/**
 *
 * @author Vincent Calame
 */
public class DefBuilder {

    private final AttributesBuilder attributesBuilder;
    private final LabelChangeBuilder labelChangeBuilder = new LabelChangeBuilder();
    private final PhrasesBuilder phrasesBuilder = new PhrasesBuilder();

    public DefBuilder() {
        this.attributesBuilder = new AttributesBuilder();
    }

    public DefBuilder(@Nullable Attributes initAttributes) {
        this.attributesBuilder = new AttributesBuilder(initAttributes);
    }

    public AttributesBuilder getAttributesBuilder() {
        return attributesBuilder;
    }

    public PhrasesBuilder getPhrasesBuilder() {
        return phrasesBuilder;
    }

    public void putLabel(Label label) {
        labelChangeBuilder.putLabel(label);
    }

    protected Labels toLabels() {
        return labelChangeBuilder.toLabels();
    }

    protected Attributes toAttributes() {
        return attributesBuilder.toAttributes();
    }

    protected Phrases toPhrases() {
        return phrasesBuilder.toPhrases();
    }

}
