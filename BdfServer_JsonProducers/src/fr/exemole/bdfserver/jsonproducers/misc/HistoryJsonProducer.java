/* BdfServer_JsonProducers - Copyright (c) 2017-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.misc;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.json.HistoryJson;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.Fichotheque;
import net.fichotheque.history.HistoryUnit;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public class HistoryJsonProducer implements JsonProducer {

    private final BdfServer bdfServer;
    private final HistoryUnit history;

    public HistoryJsonProducer(BdfParameters bdfParameters, HistoryUnit history) {
        this.bdfServer = bdfParameters.getBdfServer();
        this.history = history;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        Fichotheque fichotheque = bdfServer.getFichotheque();
        Set<String> globalIdSet = new LinkedHashSet<String>();
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        jw.key("history");
        {
            jw.object();
            {
                jw.key("deleted")
                        .value(history.isDeleted());
                jw.key("revisionArray");
                jw.array();
                for (HistoryUnit.Revision revision : history.getRevisionList()) {
                    jw.object();
                    HistoryJson.properties(jw, revision, globalIdSet);
                    jw.endObject();

                }
                jw.endArray();
                HistoryJson.userMapProperty(jw, fichotheque, globalIdSet);
            }
            jw.endObject();
        }
        jw.endObject();
    }

}
