/* BdfServer_JsonProducers - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.misc;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.MiscDomain;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.PermissionChecker;
import fr.exemole.bdfserver.tools.instruction.RequestHandler;
import java.text.ParseException;
import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.Subset;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.history.FicheHistory;
import net.fichotheque.history.HistoryUnit;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class MiscJsonProducerFactory {

    private MiscJsonProducerFactory() {
    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String json = parameters.getOutput();
        switch (json) {
            case MiscDomain.FICHESGEO_JSON: {
                return new FichesGeoJsonProducer(parameters);
            }
            case MiscDomain.HISTORY_JSON: {
                Subset subset = requestHandler.getMandatorySubset();
                int id = requestHandler.getMandatoryId();
                PermissionChecker.init(parameters).checkHistory(subset, id);
                HistoryUnit history = null;
                if (subset instanceof Corpus) {
                    FicheHistory ficheHistory = ((Corpus) subset).getFicheHistory(id);
                    if (ficheHistory != null) {
                        history = ficheHistory.getFicheUnit();
                    }
                }
                return new HistoryJsonProducer(parameters, history);
            }
            case MiscDomain.MEMENTO_UNIT_JSON: {
                RelativePath mementoPath = getPath(requestHandler);
                return new MementoUnitJsonProducer(parameters, mementoPath);
            }
            case MiscDomain.MEMENTO_UNITINFO_ARRAY_JSON: {
                return new MementoUnitInfoArrayJsonProducer(parameters);
            }
            case MiscDomain.REVISIONS_FICHE_JSON: {
                Corpus corpus = requestHandler.getMandatoryCorpus();
                int id = requestHandler.getMandatoryId();
                PermissionChecker.init(parameters).checkHistory(corpus, id);
                Set<String> revisionNameSet = getRevisionNameArray(requestHandler);
                return new FicheRevisionsJsonProducer(corpus, id, revisionNameSet);
            }
            default:
                return null;
        }
    }

    private static Set<String> getRevisionNameArray(RequestHandler requestHandler) throws ErrorMessageException {
        String revisionsParam = requestHandler.getMandatoryParameter("revisions");
        String[] revisions = StringUtils.getTechnicalTokens(revisionsParam, true);
        Set<String> set = new LinkedHashSet<String>();
        for (String revision : revisions) {
            set.add(revision);
        }
        return set;
    }

    private static RelativePath getPath(RequestHandler requestHandler) throws ErrorMessageException {
        String path = requestHandler.getMandatoryParameter("path");
        try {
            return RelativePath.parse(path);
        } catch (ParseException pe) {
            throw BdfErrors.wrongParameterValue("path", path);
        }
    }

}
