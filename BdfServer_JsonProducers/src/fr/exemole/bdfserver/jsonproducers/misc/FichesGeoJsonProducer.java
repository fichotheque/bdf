/* BdfServer_JsonProducers - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.misc;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import net.fichotheque.permission.PermissionSummary;
import fr.exemole.bdfserver.api.users.BdfUser;
import java.io.IOException;
import java.util.Locale;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.utils.FicheUtils;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class FichesGeoJsonProducer implements JsonProducer {

    private final BdfParameters bdfParameters;

    public FichesGeoJsonProducer(BdfParameters bdfParameters) {
        this.bdfParameters = bdfParameters;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        PermissionSummary permissionSummary = bdfParameters.getPermissionSummary();
        BdfUser bdfUser = bdfParameters.getBdfUser();
        Lang workingLang = bdfUser.getWorkingLang();
        Locale formatLocale = bdfUser.getFormatLocale();
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("type")
                    .value("FeatureCollection");
            jw.key("features");
            jw.array();
            Fiches fiches = bdfUser.getSelectedFiches();
            for (Fiches.Entry entry : fiches.getEntryList()) {
                Corpus corpus = entry.getCorpus();
                CorpusField geolocalisationField = corpus.getCorpusMetadata().getGeolocalisationField();
                if (geolocalisationField != null) {
                    String corpusName = corpus.getSubsetName();
                    for (FicheMeta ficheMeta : entry.getFicheMetaList()) {
                        FicheItem ficheItem = (FicheItem) FicheUtils.getValue(ficheMeta, geolocalisationField.getFieldKey());
                        if ((ficheItem != null) && (ficheItem instanceof Geopoint)) {
                            Geopoint geopoint = (Geopoint) ficheItem;
                            jw.object();
                            {
                                jw.key("type")
                                        .value("Feature");
                                jw.key("geometry");
                                jw.object();
                                {
                                    jw.key("type")
                                            .value("Point");
                                    jw.key("coordinates");
                                    jw.array();
                                    {
                                        jw.value(geopoint.getLongitude().toDecimal().toFloat());
                                        jw.value(geopoint.getLatitude().toDecimal().toFloat());
                                    }
                                    jw.endArray();
                                }
                                jw.endObject();
                                jw.key("properties");
                                jw.object();
                                {
                                    jw.key("corpus")
                                            .value(corpusName);
                                    jw.key("id")
                                            .value(ficheMeta.getId());
                                    jw.key("titre")
                                            .value(ficheMeta.getTitre());
                                    jw.key("intitule")
                                            .value(FichothequeUtils.getNumberPhrase(ficheMeta, FichothequeConstants.FICHE_PHRASE, workingLang, formatLocale));
                                    jw.key("editable")
                                            .value(permissionSummary.canWrite(ficheMeta));
                                }
                                jw.endObject();
                            }
                            jw.endObject();
                        }
                    }
                }
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
