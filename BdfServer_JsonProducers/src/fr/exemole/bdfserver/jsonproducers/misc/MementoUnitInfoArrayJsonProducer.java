/* BdfServer_JsonProducers - Copyright (c) 2014-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.misc;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.memento.MementoUnit;
import fr.exemole.bdfserver.tools.memento.MementoEngine;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public class MementoUnitInfoArrayJsonProducer implements JsonProducer {

    private final List<MementoUnit.Info> mementoUnitInfoList;

    public MementoUnitInfoArrayJsonProducer(BdfParameters bdfParameters) {
        this.mementoUnitInfoList = MementoEngine.getMementoUnitInfoList(bdfParameters);
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("mementoUnitInfoArray");
            jw.array();
            for (MementoUnit.Info info : mementoUnitInfoList) {
                jw.object();
                {
                    jw.key("name")
                            .value(info.getName());
                    jw.key("path")
                            .value(info.getPath().toString());
                    jw.key("title")
                            .value(info.getTitle());
                }
                jw.endObject();
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
