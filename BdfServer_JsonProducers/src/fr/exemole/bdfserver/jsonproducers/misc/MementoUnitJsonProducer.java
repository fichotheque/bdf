/* BdfServer_JsonProducers - Copyright (c) 2020-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.misc;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.memento.MementoNode;
import fr.exemole.bdfserver.api.memento.MementoUnit;
import fr.exemole.bdfserver.json.MementoNodeJson;
import fr.exemole.bdfserver.tools.memento.MementoEngine;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class MementoUnitJsonProducer implements JsonProducer {

    private final MementoUnit mementoUnit;

    public MementoUnitJsonProducer(BdfParameters bdfParameters, RelativePath path) {
        this.mementoUnit = MementoEngine.getMementoUnit(bdfParameters, path);
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("mementoUnit");
            jw.object();
            {
                jw.key("name")
                        .value(mementoUnit.getName());
                jw.key("path")
                        .value(mementoUnit.getPath().toString());
                jw.key("title")
                        .value(mementoUnit.getTitle());
                jw.key("subnodeArray");
                jw.array();
                for (MementoNode mementoNode : mementoUnit.getSubnodeList()) {
                    MementoNodeJson.object(jw, mementoNode);
                }
                jw.endArray();
            }
            jw.endObject();
        }
        jw.endObject();
    }

}
