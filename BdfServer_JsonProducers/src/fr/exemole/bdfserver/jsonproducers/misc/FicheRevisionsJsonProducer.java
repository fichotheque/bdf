/* BdfServer_JsonProducers - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.misc;

import java.io.IOException;
import java.util.Set;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.json.FicheJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public class FicheRevisionsJsonProducer implements JsonProducer {

    private final Corpus corpus;
    private final Set<String> revisionNameSet;
    private final int id;

    public FicheRevisionsJsonProducer(Corpus corpus, int id, Set<String> revisionNameSet) {
        this.corpus = corpus;
        this.id = id;
        this.revisionNameSet = revisionNameSet;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("ficheRevisions");
            jw.object();
            for (String revisionName : revisionNameSet) {
                Fiche fiche = corpus.getFicheRevision(id, revisionName);
                jw.key(revisionName);
                jw.object();
                if (fiche != null) {
                    FicheJson.properties(jw, corpus, fiche);
                }
                jw.endObject();
            }
            jw.endObject();
        }
        jw.endObject();
    }

}
