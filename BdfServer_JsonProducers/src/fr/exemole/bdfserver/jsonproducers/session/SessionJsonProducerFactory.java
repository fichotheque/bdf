/* BdfServer_JsonProducers - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.session;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommandResult;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.SessionDomain;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class SessionJsonProducerFactory {

    private SessionJsonProducerFactory() {

    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        String json = parameters.getOutput();
        switch (json) {
            case SessionDomain.PING_JSON: {
                BdfServer bdfServer = parameters.getBdfServer();
                BdfUser bdfUser = parameters.getBdfUser();
                MessageLocalisation messageLocalisation = BdfInstructionUtils.getMessageLocalisation(parameters.getRequestMap(), bdfServer, bdfUser);
                BdfCommandResult bdfCommandResult = parameters.getBdfCommandResult();
                CommandMessage commandMessage = null;
                if (bdfCommandResult != null) {
                    commandMessage = bdfCommandResult.getCommandMessage();
                }
                if (bdfUser == null) {
                    return PingJsonProducer.unauthentified(messageLocalisation, commandMessage);
                } else {
                    return PingJsonProducer.authentified(bdfServer, bdfUser, messageLocalisation, commandMessage);
                }
            }
            default:
                return null;
        }
    }

}
