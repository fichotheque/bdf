/* BdfServer_JsonProducers - Copyright (c) 2017-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.session;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.json.BdfUserJson;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.io.IOException;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;


/**
 *
 * @author Vincent Calame
 */
public class PingJsonProducer implements JsonProducer {

    private final boolean authentified;
    private final MessageLocalisation messageLocalisation;
    private final CommandMessage commandMessage;
    private final BdfServer bdfServer;
    private final BdfUser bdfUser;

    public PingJsonProducer(boolean authentified, MessageLocalisation messageLocalisation, CommandMessage commandMessage, BdfServer bdfServer, BdfUser bdfUser) {
        this.authentified = authentified;
        this.messageLocalisation = messageLocalisation;
        this.commandMessage = commandMessage;
        this.bdfServer = bdfServer;
        this.bdfUser = bdfUser;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("authentified")
                    .value(authentified);
            if (bdfUser != null) {
                BdfUserJson.properties(jw, bdfUser);
                jw.key("admin")
                        .value(BdfUserUtils.isAdmin(bdfServer, bdfUser));
                jw.key("roleArray");
                jw.array();
                for (Role role : bdfServer.getPermissionManager().getRoleList(bdfUser.getRedacteur())) {
                    jw.object();
                    {
                        jw.key("name")
                                .value(role.getName());
                        jw.key("attrMap");
                        CommonJson.object(jw, role.getAttributes());
                        CommonJson.title(jw, role.getTitleLabels(), bdfUser.getWorkingLang());
                    }
                    jw.endObject();
                }
                jw.endArray();
            }
            if (commandMessage != null) {
                jw.key("commandMessage");
                CommonJson.object(jw, commandMessage, messageLocalisation);
            }
        }
        jw.endObject();
    }

    public static PingJsonProducer unauthentified(MessageLocalisation messageLocalisation, CommandMessage commandMessage) {
        return new PingJsonProducer(false, messageLocalisation, commandMessage, null, null);
    }

    public static PingJsonProducer authentified(BdfServer bdfServer, BdfUser bdfUser) {
        if (bdfServer == null) {
            throw new IllegalArgumentException("bdfServer is null");
        }
        if (bdfUser == null) {
            throw new IllegalArgumentException("bdfUser is null");
        }
        return new PingJsonProducer(true, null, null, bdfServer, bdfUser);
    }

    public static PingJsonProducer authentified(BdfServer bdfServer, BdfUser bdfUser, MessageLocalisation messageLocalisation, CommandMessage commandMessage) {
        if (bdfServer == null) {
            throw new IllegalArgumentException("bdfServer is null");
        }
        if (bdfUser == null) {
            throw new IllegalArgumentException("bdfUser is null");
        }
        return new PingJsonProducer(true, messageLocalisation, commandMessage, bdfServer, bdfUser);
    }

}
