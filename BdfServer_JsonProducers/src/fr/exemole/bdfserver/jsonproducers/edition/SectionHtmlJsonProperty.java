/* BdfServer_JsonProducers - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.edition;

import fr.exemole.bdfserver.api.instruction.BdfParameters;
import java.io.IOException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.format.FormatContext;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.formatters.FicheBlockFormatter;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.utils.FormatterUtils;
import net.fichotheque.utils.pointeurs.PointeurFactory;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class SectionHtmlJsonProperty implements JsonProperty {

    private final BdfParameters bdfParameters;
    private final Corpus corpus;
    private final FicheBlocks ficheBlocks;

    public SectionHtmlJsonProperty(BdfParameters bdfParameters, Corpus corpus, FicheBlocks ficheBlocks) {
        this.bdfParameters = bdfParameters;
        this.corpus = corpus;
        this.ficheBlocks = ficheBlocks;
    }

    @Override
    public String getName() {
        return "html";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        ExtractionContext extractionContext = bdfParameters.getDefaultExtractionContext();
        FormatContext formatContext = bdfParameters.getBdfServer().getFormatContext();
        SubsetItemPointeur pointeur = PointeurFactory.newFichePointeur(corpus);
        FicheBlockFormatter ficheBlockFormatter = formatContext.getFicheBlockFormatter(null);
        FormatSource formatSource = FormatterUtils.toFormatSource(pointeur, extractionContext, null, formatContext);
        String html = ficheBlockFormatter.formatFicheBlocks(ficheBlocks, formatSource, corpus.getSubsetKey());
        jw.value(html);
    }

}
