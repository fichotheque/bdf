/* BdfServer_JsonProducers - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.edition;

import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import fr.exemole.bdfserver.tools.instruction.ResultJsonProducer;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class EditionJsonProducerFactory {

    private EditionJsonProducerFactory() {

    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        String json = parameters.getOutput();
        ResultJsonProducer jsonProducer = new ResultJsonProducer(parameters);
        JsonProperty jsonProperty = getJsonProperty(parameters, json);
        if (jsonProperty == null) {
            return null;
        } else {
            jsonProducer.add(jsonProperty);
            return jsonProducer;
        }
    }

    public static JsonProperty getJsonProperty(OutputParameters parameters, String name) throws ErrorMessageException {
        switch (name) {
            case EditionDomain.SECTION_JSON: {
                FicheBlocks ficheBlocks = (FicheBlocks) parameters.getResultObject(BdfInstructionConstants.FICHEBLOCKS_OBJ);
                Corpus corpus = (Corpus) parameters.getResultObject(BdfInstructionConstants.CORPUS_OBJ);
                if ((ficheBlocks != null) && (corpus != null)) {
                    return new SectionHtmlJsonProperty(parameters, corpus, ficheBlocks);
                } else {
                    throw BdfErrors.missingCommandResultJson(EditionDomain.SECTION_JSON);
                }
            }
            default:
                return null;
        }
    }

}
