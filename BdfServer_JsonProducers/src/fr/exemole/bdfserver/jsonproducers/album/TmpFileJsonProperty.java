/* BdfServer_HtmlProducers - Copyright (c) 2016-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.album;

import java.io.File;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TmpFileJsonProperty implements JsonProperty {

    private final File tmpFile;

    public TmpFileJsonProperty(File tmpFile) {
        this.tmpFile = tmpFile;
    }

    @Override
    public String getName() {
        return "tmpFile";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        {
            jw.key("name")
                    .value(tmpFile.getName());
        }
        jw.endObject();
    }

}
