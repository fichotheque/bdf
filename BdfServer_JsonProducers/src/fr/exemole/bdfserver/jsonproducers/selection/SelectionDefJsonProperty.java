/* BdfServer_JsonProducers - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.selection;

import fr.exemole.bdfserver.json.SelectionDefJson;
import java.io.IOException;
import net.fichotheque.selection.SelectionDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class SelectionDefJsonProperty implements JsonProperty {

    private final SelectionDef selectionDef;
    private final Lang lang;

    public SelectionDefJsonProperty(SelectionDef selectionDef, Lang lang) {
        this.selectionDef = selectionDef;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "selectionDef";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        writeValue(jw, selectionDef, lang);
    }

    static void writeValue(JSONWriter jw, SelectionDef selectionDef, Lang lang) throws IOException {
        jw.object();
        SelectionDefJson.properties(jw, selectionDef);
        CommonJson.title(jw, selectionDef.getTitleLabels(), lang);
        jw.endObject();
    }

}
