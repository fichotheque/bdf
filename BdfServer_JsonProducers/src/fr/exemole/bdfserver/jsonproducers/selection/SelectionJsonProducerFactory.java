/* BdfServer_JsonProducers - Copyright (c) 2022-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.SelectionDomain;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.ResultJsonProducer;
import net.fichotheque.selection.FichothequeQueries;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class SelectionJsonProducerFactory {

    private SelectionJsonProducerFactory() {

    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String json = parameters.getOutput();
        ResultJsonProducer jsonProducer = new ResultJsonProducer(parameters);
        JsonProperty jsonProperty = getJsonProperty(requestHandler, json);
        if (jsonProperty == null) {
            return null;
        } else {
            jsonProducer.add(jsonProperty);
            return jsonProducer;
        }
    }

    public static JsonProperty getJsonProperty(OutputRequestHandler requestHandler, String name) throws ErrorMessageException {
        BdfServer bdfServer = requestHandler.getBdfServer();
        Lang lang = requestHandler.getWorkingLang();
        switch (name) {
            case SelectionDomain.FQL_JSON: {
                FichothequeQueries fichothequeQueries = (FichothequeQueries) requestHandler.getResultObject(BdfInstructionConstants.FICHOTHEQUEQUERIES_OBJ);
                if (fichothequeQueries != null) {
                    return new FqlJsonProperty(requestHandler.getFichotheque(), fichothequeQueries);
                } else {
                    throw BdfErrors.missingCommandResultJson(SelectionDomain.FQL_JSON);
                }
            }
            case SelectionDomain.SELECTIONDEF_JSON: {
                return new SelectionDefJsonProperty(requestHandler.getSelectionDef(), lang);
            }
            case SelectionDomain.SELECTIONDEF_ARRAY_JSON: {
                return new SelectionDefArrayJsonProperty(bdfServer, lang);
            }
            default:
                return null;
        }
    }

}
