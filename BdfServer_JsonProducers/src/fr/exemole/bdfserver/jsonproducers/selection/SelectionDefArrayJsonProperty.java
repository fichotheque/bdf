/* BdfServer_JsonProducers - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.SelectionManager;
import java.io.IOException;
import net.fichotheque.selection.SelectionDef;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class SelectionDefArrayJsonProperty implements JsonProperty {

    private final SelectionManager selectionManager;
    private final Lang lang;

    public SelectionDefArrayJsonProperty(BdfServer bdfServer, Lang lang) {
        this.selectionManager = bdfServer.getSelectionManager();
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "selectionDefArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (SelectionDef selectionDef : selectionManager.getSelectionDefList()) {
            SelectionDefJsonProperty.writeValue(jw, selectionDef, lang);
        }
        jw.endArray();
    }

}
