/* BdfServer_JsonProducers - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.selection;

import fr.exemole.bdfserver.json.FicheQueryJson;
import fr.exemole.bdfserver.json.MotcleQueryJson;
import java.io.IOException;
import net.fichotheque.Fichotheque;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.selection.MotcleQuery;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class FqlJsonProperty implements JsonProperty {

    private final Fichotheque fichotheque;
    private final FichothequeQueries fichothequeQueries;

    public FqlJsonProperty(Fichotheque fichotheque, FichothequeQueries fichothequeQueries) {
        this.fichotheque = fichotheque;
        this.fichothequeQueries = fichothequeQueries;
    }

    @Override
    public String getName() {
        return "fql";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        jw.key("fiche");
        jw.array();
        for (FicheQuery ficheQuery : fichothequeQueries.getFicheQueryList()) {
            jw.object();
            FicheQueryJson.properties(jw, fichotheque, ficheQuery);
            jw.endObject();
        }
        jw.endArray();
        jw.key("motcle");
        jw.array();
        for (MotcleQuery motcleQuery : fichothequeQueries.getMotcleQueryList()) {
            jw.object();
            MotcleQueryJson.properties(jw, fichotheque, motcleQuery);
            jw.endObject();
        }
        jw.endArray();
        jw.endObject();
    }

}
