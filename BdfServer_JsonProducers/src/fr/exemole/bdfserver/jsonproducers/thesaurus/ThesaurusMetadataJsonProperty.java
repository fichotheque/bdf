/* BdfServer_JsonProducers - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.thesaurus;

import java.io.IOException;
import net.fichotheque.json.MetadataJson;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusMetadataJsonProperty implements JsonProperty {

    private final ThesaurusMetadata thesaurusMetadata;

    public ThesaurusMetadataJsonProperty(ThesaurusMetadata thesaurusMetadata) {
        this.thesaurusMetadata = thesaurusMetadata;
    }

    @Override
    public String getName() {
        return "thesaurusMetadata";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        {
            jw.key("name")
                    .value(thesaurusMetadata.getThesaurus().getSubsetName());
            MetadataJson.properties(jw, thesaurusMetadata);
        }
        jw.endObject();
    }

}
