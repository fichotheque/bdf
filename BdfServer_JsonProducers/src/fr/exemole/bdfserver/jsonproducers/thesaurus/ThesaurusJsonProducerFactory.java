/* BdfServer_JsonProducers - Copyright (c) 2017-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.ThesaurusDomain;
import fr.exemole.bdfserver.tools.exportation.table.CellEngineUtils;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.ResultJsonProducer;
import fr.exemole.bdfserver.tools.interaction.PropertiesParam;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.TableExportUtils;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusJsonProducerFactory {

    private ThesaurusJsonProducerFactory() {

    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String json = parameters.getOutput();
        ResultJsonProducer jsonProducer = new ResultJsonProducer(parameters);
        JsonProperty jsonProperty = getJsonProperty(requestHandler, json);
        if (jsonProperty == null) {
            return null;
        } else {
            jsonProducer.add(jsonProperty);
            return jsonProducer;
        }
    }

    public static JsonProperty getJsonProperty(OutputRequestHandler requestHandler, String name) throws ErrorMessageException {
        BdfServer bdfServer = requestHandler.getBdfServer();
        Lang lang = requestHandler.getWorkingLang();
        switch (name) {
            case ThesaurusDomain.MOTCLE_JSON: {
                Motcle motcle = requestHandler.getMotcle();
                lang = ThesaurusUtils.checkDisponibility(bdfServer.getThesaurusLangChecker(), motcle.getThesaurus(), lang);
                return new MotcleJsonProperty(motcle, lang, getCellConverter(requestHandler));
            }
            case ThesaurusDomain.THESAURUS_JSON: {
                Thesaurus thesaurus = requestHandler.getThesaurus();
                lang = ThesaurusUtils.checkDisponibility(bdfServer.getThesaurusLangChecker(), thesaurus, lang);
                return new ThesaurusJsonProperty(thesaurus, lang, BdfInstructionUtils.getCellConverter(requestHandler));
            }
            case ThesaurusDomain.THESAURUS_METADATA_JSON: {
                Thesaurus thesaurus = requestHandler.getThesaurus();
                return new ThesaurusMetadataJsonProperty(thesaurus.getThesaurusMetadata());
            }
            default:
                return null;
        }
    }

    private static CellConverter getCellConverter(OutputParameters parameters) {
        BdfServer bdfServer = parameters.getBdfServer();
        PropertiesParam propertiesParam = PropertiesParam.fromRequest(parameters.getRequestMap());
        CellConverter cellConverter = null;
        if (propertiesParam.isSpecial()) {
            if (propertiesParam.getName().equals(PropertiesParam.PHRASES_SPECIAL)) {
                cellConverter = TableExportUtils.getPhraseConverter(parameters.getWorkingLang(), parameters.getFormatLocale());
            }
        } else if (propertiesParam.isTableExport()) {
            ExtractionContext extractionContext = parameters.getDefaultExtractionContext();
            cellConverter = CellEngineUtils.newCellEngine(bdfServer, extractionContext, propertiesParam.getName());
        }
        if (cellConverter == null) {
            cellConverter = TableExportUtils.EMPTY_CELLENGINE;
        }
        return cellConverter;
    }

}
