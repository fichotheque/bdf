/* BdfServer_JsonProducers - Copyright (c) 2017-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.thesaurus;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import java.util.List;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusJsonProperty implements JsonProperty {

    private final Thesaurus thesaurus;
    private final Lang lang;
    private final CellConverter cellConverter;

    public ThesaurusJsonProperty(Thesaurus thesaurus, Lang lang, CellConverter cellConverter) {
        this.thesaurus = thesaurus;
        this.lang = lang;
        this.cellConverter = cellConverter;
    }

    @Override
    public String getName() {
        return "thesaurus";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        {
            jw.key("name")
                    .value(thesaurus.getSubsetKey().getSubsetName());
            jw.key("array");
            writeMotcleList(jw, thesaurus.getFirstLevelList());
        }
        jw.endObject();
    }

    private void writeMotcleList(JSONWriter jw, List<Motcle> motcleList) throws IOException {
        jw.array();
        for (Motcle motcle : motcleList) {
            jw.object();
            {
                AccessJson.properties(jw, motcle, cellConverter, lang);
                jw.key("children");
                writeMotcleList(jw, motcle.getChildList());
            }
            jw.endObject();
        }
        jw.endArray();
    }

}
