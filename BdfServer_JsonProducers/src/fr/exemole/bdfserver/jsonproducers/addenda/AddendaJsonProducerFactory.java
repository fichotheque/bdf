/* BdfServer_JsonProducers - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.addenda;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.AddendaDomain;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import net.fichotheque.addenda.Addenda;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class AddendaJsonProducerFactory {

    private AddendaJsonProducerFactory() {

    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String json = parameters.getOutput();
        switch (json) {
            case AddendaDomain.DOCUMENT_ARRAY_JSON: {
                Addenda addenda = requestHandler.getAddenda();
                return new DocumentArrayJsonProducer(parameters, addenda);
            }
            default:
                return null;
        }
    }

}
