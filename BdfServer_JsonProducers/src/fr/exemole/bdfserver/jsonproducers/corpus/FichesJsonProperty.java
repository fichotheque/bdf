/* BdfServer_JsonProducers - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.corpus;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import net.fichotheque.corpus.Fiches;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class FichesJsonProperty implements JsonProperty {

    private final PermissionSummary permissionSummary;
    private final Fiches fiches;
    private final CellConverter cellConverter;
    private final Lang lang;

    public FichesJsonProperty(PermissionSummary permissionSummary, Fiches fiches, CellConverter cellConverter, Lang lang) {
        this.fiches = fiches;
        this.lang = lang;
        this.cellConverter = cellConverter;
        this.permissionSummary = permissionSummary;
    }

    @Override
    public String getName() {
        return "fiches";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        AccessJson.properties(jw, fiches, permissionSummary, cellConverter, lang);
        jw.endObject();
    }

}
