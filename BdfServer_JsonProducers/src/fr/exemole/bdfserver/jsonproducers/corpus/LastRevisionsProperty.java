/* BdfServer_JsonProducers - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.json.HistoryJson;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.history.FicheHistory;
import net.fichotheque.history.HistoryUnit;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class LastRevisionsProperty implements JsonProperty {

    private final BdfServer bdfServer;
    private final Collection<FicheHistory> ficheHistories;

    public LastRevisionsProperty(BdfServer bdfServer, Collection<FicheHistory> ficheHistories) {
        this.bdfServer = bdfServer;
        this.ficheHistories = ficheHistories;
    }

    @Override
    public String getName() {
        return "lastRevisions";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        Set<String> globalIdSet = new LinkedHashSet<String>();
        jw.object();
        {
            jw.key("array");
            jw.array();
            for (FicheHistory ficheHistory : ficheHistories) {
                HistoryUnit ficheUnit = ficheHistory.getFicheUnit();
                jw.object();
                {
                    jw.key("corpus")
                            .value(ficheHistory.getCorpusKey().getSubsetName());
                    jw.key("id")
                            .value(ficheHistory.getId());
                    if (!ficheUnit.isEmpty()) {
                        jw.key("lastRevision");
                        jw.object();
                        HistoryJson.properties(jw, ficheUnit.getMostRecentRevision(), globalIdSet);
                        jw.endObject();
                    }
                }
                jw.endObject();
            }
            jw.endArray();
            HistoryJson.userMapProperty(jw, bdfServer.getFichotheque(), globalIdSet);
        }
        jw.endObject();
    }

}
