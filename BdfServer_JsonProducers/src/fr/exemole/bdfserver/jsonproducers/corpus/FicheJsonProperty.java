/* BdfServer_JsonProducers - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.corpus;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class FicheJsonProperty implements JsonProperty {

    private final PermissionSummary permissionSummary;
    private final FicheMeta ficheMeta;
    private final CellConverter cellConverter;

    public FicheJsonProperty(PermissionSummary permissionSummary, FicheMeta ficheMeta, CellConverter cellConverter) {
        this.ficheMeta = ficheMeta;
        this.cellConverter = cellConverter;
        this.permissionSummary = permissionSummary;
    }

    @Override
    public String getName() {
        return "fiche";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        AccessJson.properties(jw, ficheMeta, permissionSummary, cellConverter);
        jw.endObject();
    }

}
