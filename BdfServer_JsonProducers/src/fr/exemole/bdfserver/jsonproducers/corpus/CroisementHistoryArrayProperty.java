/* BdfServer_JsonProducers - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.corpus;

import fr.exemole.bdfserver.json.HistoryJson;
import java.io.IOException;
import java.util.Collection;
import net.fichotheque.croisement.CroisementKey;
import net.fichotheque.history.CroisementHistory;
import net.fichotheque.history.HistoryUnit;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class CroisementHistoryArrayProperty implements JsonProperty {

    private final Collection<CroisementHistory> croisementHistories;

    public CroisementHistoryArrayProperty(Collection<CroisementHistory> croisementHistories) {
        this.croisementHistories = croisementHistories;
    }

    @Override
    public String getName() {
        return "croisementHistoryArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (CroisementHistory croisementHistory : croisementHistories) {
            CroisementKey croisementKey = croisementHistory.getCroisementKey();
            HistoryUnit croisementUnit = croisementHistory.getCroisementUnit();
            jw.object();
            {
                jw.key("thesaurus")
                        .value(croisementKey.getSubsetKey2().getSubsetName());
                jw.key("id")
                        .value(croisementKey.getId2());
                jw.key("deleted")
                        .value(croisementHistory.isDeleted());
                if (!croisementUnit.isEmpty()) {
                    jw.key("lastRevision");
                    jw.object();
                    HistoryJson.properties(jw, croisementUnit.getMostRecentRevision(), null);
                    jw.endObject();
                }
            }
            jw.endObject();
        }
        jw.endArray();
    }

}
