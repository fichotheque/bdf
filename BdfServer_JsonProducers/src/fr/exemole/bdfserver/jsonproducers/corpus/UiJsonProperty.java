/* BdfServer_JsonProducers - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.corpus;

import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.json.CorpusFieldJson;
import java.io.IOException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class UiJsonProperty implements JsonProperty {

    private final Corpus corpus;
    private final Lang lang;
    private final UiComponents uiComponents;

    public UiJsonProperty(Corpus corpus, Lang lang, UiComponents uiComponents) {
        this.corpus = corpus;
        this.lang = lang;
        this.uiComponents = uiComponents;
    }

    @Override
    public String getName() {
        return "ui";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        CorpusMetadata corpusMetadata = corpus.getCorpusMetadata();
        jw.object();
        {
            jw.key("componentArray");
            jw.array();
            for (UiComponent uiComponent : uiComponents.getUiComponentList()) {
                if (uiComponent instanceof FieldUi) {
                    CorpusField corpusField = corpusMetadata.getCorpusField(((FieldUi) uiComponent).getFieldKey());
                    if (corpusField != null) {
                        jw.object();
                        {
                            CorpusFieldJson.properties(jw, corpusField, lang);
                        }
                        jw.endObject();
                    }
                }
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
