/* BdfServer_JsonProducers - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.pioche;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import java.util.Collection;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class MotcleArrayJsonProducer implements JsonProducer {

    private final Lang workingLang;
    private final Collection<Motcle> motcles;
    private final CellConverter cellConverter;


    public MotcleArrayJsonProducer(Lang workingLang, Collection<Motcle> motcles, CellConverter cellConverter) {
        this.workingLang = workingLang;
        this.motcles = motcles;
        this.cellConverter = cellConverter;
    }


    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("count")
                    .value(motcles.size());
            jw.key("array");
            jw.array();
            for (Motcle motcle : motcles) {
                String idalpha = motcle.getIdalpha();
                jw.object();
                {
                    AccessJson.properties(jw, motcle, cellConverter, workingLang);
                    if (idalpha != null) {
                        jw.key("code")
                                .value(idalpha);
                    }
                }
                jw.endObject();
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
