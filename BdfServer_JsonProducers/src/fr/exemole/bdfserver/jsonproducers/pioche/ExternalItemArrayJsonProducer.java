/* BdfServer_JsonProducers - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.pioche;

import java.io.IOException;
import java.util.Collection;
import net.fichotheque.externalsource.ExternalItem;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public class ExternalItemArrayJsonProducer implements JsonProducer {

    private final Collection<ExternalItem> externalItems;


    public ExternalItemArrayJsonProducer(Collection<ExternalItem> externalItems) {
        this.externalItems = externalItems;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("count")
                    .value(externalItems.size());
            jw.key("array");
            jw.array();
            for (ExternalItem item : externalItems) {
                jw.object();
                {
                    if (item instanceof ExternalItem.Id) {
                        jw.key("id")
                                .value(((ExternalItem.Id) item).getId());
                    } else {
                        jw.key("code")
                                .value(((ExternalItem.Idalpha) item).getIdalpha());
                    }
                    jw.key("title")
                            .value(item.getTitle());
                    jw.key("description")
                            .value(item.getDescription());
                }
                jw.endObject();
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
