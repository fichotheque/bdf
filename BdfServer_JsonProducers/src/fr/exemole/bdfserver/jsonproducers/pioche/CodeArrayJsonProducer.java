/* BdfServer_JsonProducers - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.pioche;

import fr.exemole.bdfserver.api.BdfServer;
import java.io.IOException;
import java.util.Collection;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.localisation.UserLangContext;


/**
 *
 * @author Vincent Calame
 */
public class CodeArrayJsonProducer implements JsonProducer {

    private final BdfServer bdfServer;
    private final UserLangContext userLangContext;
    private final Collection<String> codes;

    public CodeArrayJsonProducer(BdfServer bdfServer, UserLangContext userLangContext, Collection<String> codes) {
        this.bdfServer = bdfServer;
        this.userLangContext = userLangContext;
        this.codes = codes;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        MessageLocalisation messageLocalisation = bdfServer.getL10nManager().getMessageLocalisation(userLangContext);
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("count")
                    .value(codes.size());
            jw.key("array");
            jw.array();
            for (String code : codes) {
                String title = messageLocalisation.toString(code);
                if (title == null) {
                    title = "";
                }
                jw.object();
                {
                    jw.key("code")
                            .value(code);
                    jw.key("title")
                            .value(title);
                }
                jw.endObject();
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
