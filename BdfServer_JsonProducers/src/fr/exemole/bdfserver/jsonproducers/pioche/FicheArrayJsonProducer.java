/* BdfServer_JsonProducers - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.pioche;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import java.util.Collection;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public class FicheArrayJsonProducer implements JsonProducer {

    private final PermissionSummary permissionSummary;
    private final Collection<FicheMeta> fiches;
    private final CellConverter cellConverter;


    public FicheArrayJsonProducer(PermissionSummary permissionSummary, Collection<FicheMeta> fiches, CellConverter cellConverter) {
        this.permissionSummary = permissionSummary;
        this.fiches = fiches;
        this.cellConverter = cellConverter;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("count")
                    .value(fiches.size());
            jw.key("array");
            jw.array();
            for (FicheMeta ficheMeta : fiches) {
                jw.object();
                AccessJson.properties(jw, ficheMeta, permissionSummary, cellConverter);
                jw.key("code")
                        .value(ficheMeta.getSubsetName() + '/' + ficheMeta.getId());
                jw.endObject();
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
