/* BdfServer_JsonProducers - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.pioche;

import java.io.IOException;
import java.util.Collection;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurArrayJsonProducer implements JsonProducer {

    private final Collection<Redacteur> redacteurs;
    private final SubsetKey defaultSphereKey;

    public RedacteurArrayJsonProducer(Collection<Redacteur> redacteurs, SubsetKey defaultSphereKey) {
        this.redacteurs = redacteurs;
        this.defaultSphereKey = defaultSphereKey;
    }


    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("count")
                    .value(redacteurs.size());
            jw.key("array");
            jw.array();
            for (Redacteur redacteur : redacteurs) {
                SubsetKey sphereKey = redacteur.getSubsetKey();
                String code;
                if ((defaultSphereKey != null) && (sphereKey.equals(defaultSphereKey))) {
                    code = redacteur.getLogin();
                } else {
                    code = redacteur.getBracketStyle();
                }
                jw.object();
                {
                    jw.key("sphere")
                            .value(sphereKey.getSubsetName());
                    jw.key("id")
                            .value(redacteur.getId());
                    jw.key("login")
                            .value(redacteur.getLogin());
                    jw.key("code")
                            .value(code);
                    jw.key("title")
                            .value(redacteur.getPersonCore().toBiblioStyle(false));
                    jw.key("status")
                            .value(redacteur.getStatus());
                }
                jw.endObject();
            }
            jw.endArray();
        }
        jw.endObject();
    }

    /*
    public void init(Condition condition) {
        if (condition != null) {
            RedacteurQuery redacteurQuery = RedacteurQueryBuilder.init()
                    .setNomcompletCondition(condition)
                    .toRedacteurQuery();
            RedacteurSelector redacteurSelector = RedacteurSelectorBuilder.init(fichotheque, workingLang).add(redacteurQuery).toRedacteurSelector();
            result = selection(redacteurSelector);
        } else {
            result = new ArrayList<Redacteur>();
            for (Sphere sphere : sphereArray) {
                for (Redacteur redacteur : sphere.getRedacteurList()) {
                    if (redacteur != null) {
                        result.add(redacteur);
                    }
                }
            }
        }
    }



    this.workingLang = workingLang;
        if (sphereCollection != null) {
            this.sphereArray = sphereCollection.toArray(new Sphere[sphereCollection.size()]);
        } else {
            this.sphereArray = FichothequeUtils.toSphereArray(fichotheque);
        }

    private Collection<Redacteur> selection(RedacteurSelector redacteurSelector) {
        TreeSet<Redacteur> redacSet = new TreeSet<Redacteur>(new RedacteurComparator());
        for (Sphere sphere : sphereArray) {
            for (Redacteur redacteur : sphere.getRedacteurList()) {
                if (!redacteur.isActive()) {
                    continue;
                }
                if (redacteurSelector.test(redacteur)) {
                    redacSet.add(redacteur);
                }
            }
        }
        return redacSet;
    }


    private static class RedacteurComparator implements Comparator<Redacteur> {

        private RedacteurComparator() {
        }

        @Override
        public int compare(Redacteur r1, Redacteur r2) {
            String nc1 = r1.getPersonCore().toDirectoryStyle(true);
            String nc2 = r2.getPersonCore().toDirectoryStyle(true);
            int result = nc1.compareTo(nc2);
            if (result != 0) {
                return result;
            }
            String id1 = r1.getLogin();
            String id2 = r2.getLogin();
            result = id1.compareTo(id2);
            if (result != 0) {
                return result;
            }
            return r1.getSphere().getSubsetKey().compareTo(r2.getSphere().getSubsetKey());
        }

    }*/

}
