/* BdfServer_JsonProducers - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.pioche;

import java.io.IOException;
import java.util.Collection;
import net.fichotheque.addenda.Document;
import net.fichotheque.addenda.Version;
import net.fichotheque.utils.AddendaUtils;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public class DocumentArrayJsonProducer implements JsonProducer {

    private final Collection<Document> documents;


    public DocumentArrayJsonProducer(Collection<Document> documents) {
        this.documents = documents;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("count")
                    .value(documents.size());
            jw.key("array");
            jw.array();
            for (Document document : documents) {
                int documentid = document.getId();
                String addendaName = document.getSubsetName();
                jw.object();
                {
                    jw.key("addenda")
                            .value(addendaName);
                    jw.key("id")
                            .value(documentid);
                    jw.key("code")
                            .value(addendaName + '/' + documentid);
                    jw.key("title")
                            .value(AddendaUtils.toTitle(document));
                    jw.key("basename")
                            .value(document.getBasename());
                    jw.key("extensions");
                    jw.array();
                    for (Version version : document.getVersionList()) {
                        jw.value(version.getExtension());
                    }
                    jw.endArray();
                }
                jw.endObject();
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
