/* BdfServer_JsonProducers - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.importation;

import java.io.IOException;
import net.mapeadores.util.html.MetaReport;
import net.mapeadores.util.html.MetaReport.ProtocolValues;
import net.mapeadores.util.html.MetaReportUtils;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.MultiStringable;


/**
 *
 * @author Vincent Calame
 */
public class MetaReportJsonProperty implements JsonProperty {

    private final MetaReport metaReport;

    public MetaReportJsonProperty(MetaReport metaReport) {
        this.metaReport = metaReport;
    }

    @Override
    public String getName() {
        return "metaReport";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        writeProperty(jw, "title", MetaReportUtils.getTitle(metaReport));
        writeProperty(jw, "description", MetaReportUtils.getDescription(metaReport));
        Lang lang = MetaReportUtils.getLang(metaReport);
        if (lang != null) {
            jw.key("lang")
                    .value(lang.toString());
        }
        FuzzyDate date = MetaReportUtils.getDate(metaReport);
        if (date != null) {
            jw.key("date")
                    .value(date.toString());
        }
        writeProperty(jw, "authors", MetaReportUtils.getAuthors(metaReport));
        writeProperty(jw, "publishers", MetaReportUtils.getKeywords(metaReport));
        writeProperty(jw, "keywords", MetaReportUtils.getKeywords(metaReport));
        for (String protocolName : metaReport.getAvalaibleProtocolSet()) {
            jw.key(protocolName);
            jw.object();
            ProtocolValues protocolValues = metaReport.getProtocolValues(protocolName);
            for (String paramName : protocolValues.getParamNameSet()) {
                MultiStringable multiStringable = protocolValues.getValues(paramName);
                jw.key(paramName);
                jw.array();
                for (String value : multiStringable.toStringArray()) {
                    jw.value(value);
                }
                jw.endArray();
            }
            jw.endObject();
        }

        jw.endObject();
    }

    private void writeProperty(JSONWriter jw, String key, String value) throws IOException {
        if (value != null) {
            jw.key(key)
                    .value(value);
        }
    }

    private void writeProperty(JSONWriter jw, String key, String[] values) throws IOException {
        if (values != null) {
            jw.key(key);
            jw.array();
            for (String value : values) {
                jw.value(value);

            }
            jw.endArray();
        }
    }

}
