/* BdfServer_JsonProducers - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.importation;

import java.io.IOException;
import java.util.List;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.syntax.FicheblockSyntax;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class FicheblockSyntaxJsonProperty implements JsonProperty {

    private final List<FicheBlock> ficheBlockList;

    public FicheblockSyntaxJsonProperty(List<FicheBlock> ficheBlockList) {
        this.ficheBlockList = ficheBlockList;
    }

    @Override
    public String getName() {
        return "ficheblockSyntax";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.value(FicheblockSyntax.toString(ficheBlockList, FicheblockSyntax.parameters().withSpecialParseChars(true)).trim());
    }

}
