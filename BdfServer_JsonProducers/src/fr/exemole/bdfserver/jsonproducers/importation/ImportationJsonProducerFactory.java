/* BdfServer_JsonProducers - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.importation;

import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.ImportationDomain;
import fr.exemole.bdfserver.tools.instruction.ResultJsonProducer;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.importation.ParseResult;
import net.mapeadores.util.html.MetaReport;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class ImportationJsonProducerFactory {

    private ImportationJsonProducerFactory() {
    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        String json = parameters.getOutput();
        ResultJsonProducer jsonProducer = new ResultJsonProducer(parameters);
        JsonProperty jsonProperty = getJsonProperty(parameters, json);
        if (jsonProperty == null) {
            return null;
        } else {
            jsonProducer.add(jsonProperty);
            return jsonProducer;
        }
    }

    public static JsonProperty getJsonProperty(OutputParameters parameters, String name) throws ErrorMessageException {
        switch (name) {
            case ImportationDomain.PARSERESULT_JSON: {
                ParseResult parseResult = (ParseResult) parameters.getResultObject(BdfInstructionConstants.PARSERESULT_OBJ);
                if (parseResult != null) {
                    return new ParseResultJsonProperty(parameters.getBdfServer(), parseResult);
                } else {
                    throw BdfErrors.missingCommandResultJson(ImportationDomain.PARSERESULT_JSON);
                }
            }
            case ImportationDomain.METAREPORT_JSON: {
                MetaReport metaReport = (MetaReport) parameters.getResultObject(BdfInstructionConstants.METAREPORT_OBJ);
                if (metaReport != null) {
                    return new MetaReportJsonProperty(metaReport);
                } else {
                    throw BdfErrors.missingCommandResultJson(ImportationDomain.METAREPORT_JSON);
                }
            }
            case ImportationDomain.FICHEBLOCKSYNTAX_JSON: {
                FicheBlocks ficheBlocks = (FicheBlocks) parameters.getResultObject(BdfInstructionConstants.FICHEBLOCKS_OBJ);
                if (ficheBlocks != null) {
                    return new FicheblockSyntaxJsonProperty(ficheBlocks);
                } else {
                    throw BdfErrors.missingCommandResultJson(ImportationDomain.FICHEBLOCKSYNTAX_JSON);
                }
            }
            default:
                return null;
        }
    }

}
