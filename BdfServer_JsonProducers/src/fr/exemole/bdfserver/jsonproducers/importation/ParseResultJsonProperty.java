/* BdfServer_JsonProducers - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.importation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.json.ImportationJson;
import java.io.IOException;
import net.fichotheque.importation.ParseResult;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class ParseResultJsonProperty implements JsonProperty {

    private final ParseResult parseResult;

    public ParseResultJsonProperty(BdfServer bdfServer, ParseResult parseResult) {
        this.parseResult = parseResult;
    }

    @Override
    public String getName() {
        return "parseResult";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        ImportationJson.properties(jw, parseResult);
        jw.endObject();
    }

}
