/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.json.TransformationJson;
import java.io.IOException;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TemplateDescriptionJsonProperty implements JsonProperty {

    private final TemplateDescription templateDescription;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;

    public TemplateDescriptionJsonProperty(TemplateDescription templateDescription, Lang lang, MessageLocalisation messageLocalisation) {
        this.templateDescription = templateDescription;
        this.lang = lang;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "templateDescription";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        TransformationJson.properties(jw, templateDescription, messageLocalisation);
        CommonJson.title(jw, templateDescription.getTemplateDef().getTitleLabels(), lang);
        jw.endObject();
    }

}
