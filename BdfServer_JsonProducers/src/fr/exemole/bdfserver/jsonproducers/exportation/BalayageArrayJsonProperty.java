/* BdfServer_JsonProducers - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.BalayageManager;
import fr.exemole.bdfserver.json.BalayageJson;
import java.io.IOException;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 *
 * @author Vincent Calame
 */
public class BalayageArrayJsonProperty implements JsonProperty {

    private final BalayageManager balayageManager;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;

    public BalayageArrayJsonProperty(BdfServer bdfServer, Lang lang, MessageLocalisation messageLocalisation) {
        this.balayageManager = bdfServer.getBalayageManager();
        this.lang = lang;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "balayageArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (BalayageDescription balayageDescription : balayageManager.getBalayageDescriptionList()) {
            jw.object();
            BalayageJson.properties(jw, balayageDescription, messageLocalisation);
            CommonJson.title(jw, balayageDescription.getBalayageDef().getTitleLabels(), lang);
            jw.endObject();
        }
        jw.endArray();
    }

}
