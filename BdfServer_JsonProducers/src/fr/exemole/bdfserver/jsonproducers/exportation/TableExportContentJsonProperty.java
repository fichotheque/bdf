/* BdfServer_JsonProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.json.TableExportJson;
import java.io.IOException;
import net.fichotheque.exportation.table.TableExportContentDescription;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TableExportContentJsonProperty implements JsonProperty {

    private final BdfServer bdfServer;
    private final TableExportContentDescription tableExportContentDescription;
    private final MessageLocalisation messageLocalisation;

    public TableExportContentJsonProperty(BdfServer bdfServer, TableExportContentDescription tableExportContentDescription, MessageLocalisation messageLocalisation) {
        this.bdfServer = bdfServer;
        this.tableExportContentDescription = tableExportContentDescription;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "tableExportContent";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        String contentPath = tableExportContentDescription.getPath();
        String tableExportName = tableExportContentDescription.getTableExportName();
        String content = bdfServer.getTableExportManager().getTableExportContent(tableExportName, contentPath);
        if (content == null) {
            content = "";
        }
        jw.object();
        {
            jw.key("tableExportName")
                    .value(tableExportName);
            jw.key("path")
                    .value(contentPath);
            TableExportJson.stateAndMessagesProperties(jw, tableExportContentDescription, messageLocalisation);
            jw.key("content")
                    .value(content);
        }
        jw.endObject();
    }

}
