/* BdfServer_JsonProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.json.TableExportJson;
import java.io.IOException;
import net.fichotheque.exportation.table.TableExportDescription;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TableExportJsonProperty implements JsonProperty {

    private final TableExportDescription tableExportDescription;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;


    public TableExportJsonProperty(TableExportDescription tableExportDescription, Lang lang, MessageLocalisation messageLocalisation) {
        this.tableExportDescription = tableExportDescription;
        this.lang = lang;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "tableExport";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        TableExportJson.properties(jw, tableExportDescription, messageLocalisation);
        CommonJson.title(jw, tableExportDescription.getTableExportDef().getTitleLabels(), lang);
        jw.endObject();
    }

}
