/* BdfServer_JsonProducers - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.json.AccessJson;
import java.io.IOException;
import net.fichotheque.exportation.access.AccessDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class AccessJsonProperty implements JsonProperty {

    private final AccessDef accessDef;
    private final Lang lang;


    public AccessJsonProperty(AccessDef accessDef, Lang lang) {
        this.accessDef = accessDef;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "access";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        AccessJson.properties(jw, accessDef);
        CommonJson.title(jw, accessDef.getTitleLabels(), lang);
        jw.endObject();
    }

}
