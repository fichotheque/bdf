/* BdfServer_JsonProducers - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.json.BalayageJson;
import java.io.IOException;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 *
 * @author Vincent Calame
 */
public class BalayageJsonProperty implements JsonProperty {

    private final BalayageDescription balayageDescription;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;

    public BalayageJsonProperty(BalayageDescription balayageDescription, Lang lang, MessageLocalisation messageLocalisation) {
        this.balayageDescription = balayageDescription;
        this.lang = lang;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "balayage";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        BalayageJson.properties(jw, balayageDescription, messageLocalisation);
        CommonJson.title(jw, balayageDescription.getBalayageDef().getTitleLabels(), lang);
        jw.endObject();
    }

}
