/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.SqlExportManager;
import fr.exemole.bdfserver.json.SqlExportJson;
import java.io.IOException;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class SqlExportArrayJsonProperty implements JsonProperty {

    private final SqlExportManager sqlExportManager;
    private final Lang lang;

    public SqlExportArrayJsonProperty(BdfServer bdfServer, Lang lang) {
        this.sqlExportManager = bdfServer.getSqlExportManager();
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "sqlExportArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (SqlExportDef sqlExportDef : sqlExportManager.getSqlExportDefList()) {
            jw.object();
            SqlExportJson.properties(jw, sqlExportDef);
            CommonJson.title(jw, sqlExportDef.getTitleLabels(), lang);
            jw.endObject();
        }
        jw.endArray();

    }

}
