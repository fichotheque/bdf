/* BdfServer_JsonProducers - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.ScrutariExportManager;
import fr.exemole.bdfserver.json.ScrutariExportJson;
import java.io.IOException;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariExportArrayJsonProperty implements JsonProperty {

    private final ScrutariExportManager scrutariExportManager;
    private final Lang lang;

    public ScrutariExportArrayJsonProperty(BdfServer bdfServer, Lang lang) {
        this.scrutariExportManager = bdfServer.getScrutariExportManager();
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "scrutariExportArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (ScrutariExportDef scrutariExportDef : scrutariExportManager.getScrutariExportDefList()) {
            jw.object();
            ScrutariExportJson.properties(jw, scrutariExportDef);
            CommonJson.title(jw, scrutariExportDef.getTitleLabels(), lang);
            jw.endObject();
        }
        jw.endArray();
    }

}
