/* BdfServer_JsonProducers - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.configuration.Target;
import fr.exemole.bdfserver.tools.runners.ScrutariExportRunner;
import java.io.File;
import java.io.IOException;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariExportPathsJsonProperty implements JsonProperty {

    private final ScrutariExportDef scrutariExportDef;
    private final PathConfiguration pathConfiguration;
    private final Target target;

    public ScrutariExportPathsJsonProperty(ScrutariExportDef scrutariExportDef, PathConfiguration pathConfiguration) {
        this.scrutariExportDef = scrutariExportDef;
        this.pathConfiguration = pathConfiguration;
        this.target = ConfigurationUtils.getTarget(pathConfiguration, scrutariExportDef);
    }

    @Override
    public String getName() {
        return "scrutariExportPaths";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        {
            addObject(jw, "info", ScrutariExportRunner.INFO_SUFFIX);
            addObject(jw, "data", ScrutariExportRunner.DATA_SUFFIX);
        }
        jw.endObject();
    }

    private void addObject(JSONWriter jw, String key, String suffix) throws IOException {
        String fileName = scrutariExportDef.getName() + suffix + ".xml";
        File destinationFile = new File(target.getFile(), fileName);
        String baseUrl = target.getUrl();
        jw.key(key);
        jw.object();
        {
            jw.key("path")
                    .value(destinationFile.getAbsolutePath());
            jw.key("url");
            if (!baseUrl.isEmpty()) {
                jw.value(baseUrl + fileName);
            } else {
                jw.value("");
            }
        }
        jw.endObject();
    }

}
