/* BdfServer_JsonProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.TableExportManager;
import fr.exemole.bdfserver.json.TableExportJson;
import java.io.IOException;
import net.fichotheque.exportation.table.TableExportDescription;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TableExportArrayJsonProperty implements JsonProperty {

    private final TableExportManager manager;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;

    public TableExportArrayJsonProperty(BdfServer bdfServer, Lang lang, MessageLocalisation messageLocalisation) {
        this.manager = bdfServer.getTableExportManager();
        this.lang = lang;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "tableExportArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (TableExportDescription tableExportDescription : manager.getTableExportDescriptionList()) {
            jw.object();
            TableExportJson.properties(jw, tableExportDescription, messageLocalisation);
            CommonJson.title(jw, tableExportDescription.getTableExportDef().getTitleLabels(), lang);
            jw.endObject();
        }
        jw.endArray();
    }

}
