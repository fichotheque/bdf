/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.api.storage.StorageContent;
import fr.exemole.bdfserver.json.TransformationJson;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TemplateContentJsonProperty implements JsonProperty {

    private final BdfServer bdfServer;
    private final TemplateContentDescription templateContentDescription;
    private final MessageLocalisation messageLocalisation;

    public TemplateContentJsonProperty(BdfServer bdfServer, TemplateContentDescription templateContentDescription, MessageLocalisation messageLocalisation) {
        this.bdfServer = bdfServer;
        this.templateContentDescription = templateContentDescription;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "templateContent";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        TemplateKey templateKey = templateContentDescription.getTemplateKey();
        String contentPath = templateContentDescription.getPath();
        String content = getTextContent(templateKey, contentPath);
        jw.object();
        {
            jw.key("templateKey")
                    .value(templateKey.getKeyString());
            jw.key("path")
                    .value(contentPath);
            jw.key("mandatory")
                    .value(templateContentDescription.isMandatory());
            TransformationJson.stateAndMessagesProperties(jw, templateContentDescription, messageLocalisation);
            jw.key("content")
                    .value(content);
        }
        jw.endObject();
    }

    private String getTextContent(TemplateKey templateKey, String contentPath) {
        StorageContent storageContent = bdfServer.getTransformationManager().getTemplateStorageContent(templateKey, contentPath);
        if (storageContent != null) {
            try (InputStream is = storageContent.getInputStream()) {
                return IOUtils.toString(is, "UTF-8");
            } catch (IOException ioe) {
                throw new BdfStorageException(ioe);
            }
        } else {
            return "";
        }
    }

}
