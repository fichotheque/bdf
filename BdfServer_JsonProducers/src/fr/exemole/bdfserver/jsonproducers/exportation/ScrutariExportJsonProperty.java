/* BdfServer_JsonProducers - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.json.ScrutariExportJson;
import java.io.IOException;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariExportJsonProperty implements JsonProperty {

    private final ScrutariExportDef scrutariExportDef;
    private final Lang lang;

    public ScrutariExportJsonProperty(ScrutariExportDef scrutariExportDef, Lang lang) {
        this.scrutariExportDef = scrutariExportDef;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "scrutariExport";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        ScrutariExportJson.properties(jw, scrutariExportDef);
        CommonJson.title(jw, scrutariExportDef.getTitleLabels(), lang);
        jw.endObject();
    }

}
