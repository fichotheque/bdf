/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.json.TransformationJson;
import java.io.IOException;
import net.fichotheque.exportation.transformation.TransformationDescription;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TransformationDescriptionJsonProperty implements JsonProperty {

    private final TransformationDescription transformationDescription;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;


    public TransformationDescriptionJsonProperty(TransformationDescription transformationDescription, Lang lang, MessageLocalisation messageLocalisation) {
        this.transformationDescription = transformationDescription;
        this.lang = lang;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "transformationDescription";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        TransformationJson.properties(jw, transformationDescription, messageLocalisation, lang);
        jw.endObject();
    }

}
