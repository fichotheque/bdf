/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.managers.TransformationManager;
import fr.exemole.bdfserver.json.TransformationJson;
import java.io.IOException;
import java.util.Set;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class TransformationDescriptionsJsonProperty implements JsonProperty {

    private final TransformationManager transformationManager;
    private final Set<TransformationKey> transformationKeySet;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;


    public TransformationDescriptionsJsonProperty(TransformationManager transformationManager, Set<TransformationKey> transformationKeySet, Lang lang, MessageLocalisation messageLocalisation) {
        this.transformationManager = transformationManager;
        this.transformationKeySet = transformationKeySet;
        this.lang = lang;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "transformationDescriptions";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        for (TransformationKey transformationKey : transformationKeySet) {
            jw.key(transformationKey.getKeyString());
            jw.object();
            TransformationJson.properties(jw, transformationManager.getTransformationDescription(transformationKey), messageLocalisation, lang);
            jw.endObject();
        }
        jw.endObject();
    }


}
