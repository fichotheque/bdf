/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.configuration.Target;
import java.io.IOException;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class SqlExportPathsJsonProperty implements JsonProperty {

    private final SqlExportDef sqlExportDef;
    private final PathConfiguration pathConfiguration;
    private final Target target;

    public SqlExportPathsJsonProperty(SqlExportDef sqlExportDef, PathConfiguration pathConfiguration) {
        this.sqlExportDef = sqlExportDef;
        this.pathConfiguration = pathConfiguration;
        this.target = ConfigurationUtils.getTarget(pathConfiguration, sqlExportDef);
    }

    @Override
    public String getName() {
        return "sqlExportPaths";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        {
            jw.key("path")
                    .value(target.getFile().getAbsolutePath());
            jw.key("url")
                    .value(target.getUrl());
        }
        jw.endObject();
    }

}
