/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.json.SqlExportJson;
import java.io.IOException;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class SqlExportJsonProperty implements JsonProperty {

    private final SqlExportDef sqlExportDef;
    private final Lang lang;

    public SqlExportJsonProperty(SqlExportDef sqlExportDef, Lang lang) {
        this.sqlExportDef = sqlExportDef;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "sqlExport";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        SqlExportJson.properties(jw, sqlExportDef);
        CommonJson.title(jw, sqlExportDef.getTitleLabels(), lang);
        jw.endObject();
    }

}
