/* BdfServer_JsonProducers - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.json.BalayageJson;
import java.io.IOException;
import net.fichotheque.exportation.balayage.BalayageContentDescription;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class BalayageContentJsonProperty implements JsonProperty {

    private final BdfServer bdfServer;
    private final BalayageContentDescription balayageContentDescription;
    private final MessageLocalisation messageLocalisation;

    public BalayageContentJsonProperty(BdfServer bdfServer, BalayageContentDescription balayageContentDescription, MessageLocalisation messageLocalisation) {
        this.bdfServer = bdfServer;
        this.balayageContentDescription = balayageContentDescription;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public String getName() {
        return "balayageContent";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        String balayageName = balayageContentDescription.getBalayageName();
        String contentPath = balayageContentDescription.getPath();
        String content = bdfServer.getBalayageManager().getBalayageContent(balayageName, contentPath);
        if (content == null) {
            content = "";
        }
        jw.object();
        {
            jw.key("balayageName")
                    .value(balayageName);
            jw.key("path")
                    .value(contentPath);
            BalayageJson.stateAndMessagesProperties(jw, balayageContentDescription, messageLocalisation);
            jw.key("content")
                    .value(content);
        }
        jw.endObject();
    }

}
