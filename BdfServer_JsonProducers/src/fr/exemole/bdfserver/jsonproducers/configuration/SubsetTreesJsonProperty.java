/* BdfServer_JsonProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.json.SubsetTreeJson;
import java.io.IOException;
import net.fichotheque.permission.PermissionSummary;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class SubsetTreesJsonProperty implements JsonProperty {

    private final BdfServer bdfServer;
    private final PermissionSummary permissionSummary;
    private final short[] categoryArray;
    private final boolean withDetails;
    private final Lang lang;
    private final MessageLocalisation messageLocalisation;

    public SubsetTreesJsonProperty(BdfServer bdfServer, PermissionSummary permissionSummary, Lang lang, short[] categoryArray, boolean withDetails) {
        this.bdfServer = bdfServer;
        this.permissionSummary = permissionSummary;
        this.categoryArray = categoryArray;
        this.withDetails = withDetails;
        this.lang = lang;
        this.messageLocalisation = bdfServer.getL10nManager().getMessageLocalisation(lang);
    }

    @Override
    public String getName() {
        return "subsetTrees";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        SubsetTreeJson.Parameters parameters = SubsetTreeJson.initParameters(bdfServer, lang, messageLocalisation)
                .setWithPermissionSummary(permissionSummary)
                .setWithDetails(withDetails);
        jw.object();
        int length = categoryArray.length;
        for (int i = 0; i < length; i++) {
            short category = categoryArray[i];
            SubsetTreeJson.properties(jw, parameters, category);
        }
        jw.endObject();
    }

}
