/* BdfServer_JsonProducers - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.ExternalScriptManager;
import java.io.IOException;
import java.util.SortedSet;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class ExternalScriptArrayJsonProperty implements JsonProperty {

    private final ExternalScriptManager externalScriptManager;

    public ExternalScriptArrayJsonProperty(BdfServer bdfServer) {
        this.externalScriptManager = bdfServer.getExternalScriptManager();
    }

    @Override
    public String getName() {
        return "externalScriptArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        SortedSet<String> nameSet = externalScriptManager.getScriptNameSet();
        jw.array();
        for (String name : nameSet) {
            jw.object();
            jw.key("name")
                    .value(name);
            jw.endObject();
        }
        jw.endArray();
    }

}
