/* BdfServer_JsonProducers - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.configuration;

import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.json.ConfigurationJson;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class PathConfigurationJsonProperty implements JsonProperty {

    private final PathConfiguration pathConfiguration;

    public PathConfigurationJsonProperty(PathConfiguration pathConfiguration) {
        this.pathConfiguration = pathConfiguration;
    }

    @Override
    public String getName() {
        return "pathConfiguration";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        ConfigurationJson.properties(jw, pathConfiguration);
        jw.endObject();
    }

}
