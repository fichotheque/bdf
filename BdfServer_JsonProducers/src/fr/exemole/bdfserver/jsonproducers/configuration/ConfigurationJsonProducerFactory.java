/* BdfServer_JsonProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.domains.ConfigurationDomain;
import fr.exemole.bdfserver.tools.configuration.PathConfigurationBuilder;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.ResultJsonProducer;
import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class ConfigurationJsonProducerFactory {

    private ConfigurationJsonProducerFactory() {
    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        String json = parameters.getOutput();
        ResultJsonProducer jsonProducer = new ResultJsonProducer(parameters);
        JsonProperty jsonProperty = getJsonProperty(requestHandler, json);
        if (jsonProperty == null) {
            return null;
        } else {
            jsonProducer.add(jsonProperty);
            return jsonProducer;
        }
    }

    public static JsonProperty getJsonProperty(OutputRequestHandler requestHandler, String name) throws ErrorMessageException {
        BdfServer bdfServer = requestHandler.getBdfServer();
        RequestMap requestMap = requestHandler.getRequestMap();
        switch (name) {
            case ConfigurationDomain.SUBSETTREES_JSON: {
                boolean withDetails = requestMap.isTrue(ConfigurationDomain.WITHDETAILS_PARAMNAME);
                return new SubsetTreesJsonProperty(bdfServer, requestHandler.getPermissionSummary(), requestHandler.getWorkingLang(), getCategoryArray(requestHandler), withDetails);
            }
            case ConfigurationDomain.LANGCONFIGURATION_JSON: {
                return new LangConfigurationJsonProperty(bdfServer);
            }
            case ConfigurationDomain.PATHCONFIGURATION_JSON: {
                return new PathConfigurationJsonProperty(PathConfigurationBuilder.build(bdfServer));
            }
            case ConfigurationDomain.EXTERNALSCRIPT_ARRAY_JSON: {
                return new ExternalScriptArrayJsonProperty(bdfServer);
            }
            default:
                return null;
        }
    }

    private static short[] getCategoryArray(OutputRequestHandler requestHandler) throws ErrorMessageException {
        String[] categoriesArray = requestHandler.getTokens("categories");
        Set<Short> categorySet = new LinkedHashSet<Short>();
        int categoriesLength = categoriesArray.length;
        for (int j = 0; j < categoriesLength; j++) {
            String[] tokensArray = StringUtils.getTechnicalTokens(categoriesArray[j], true);
            int length = tokensArray.length;
            for (int i = 0; i < length; i++) {
                try {
                    categorySet.add(SubsetKey.categoryToShort(tokensArray[i]));
                } catch (IllegalArgumentException iae) {
                    throw BdfErrors.unknownParameterValue("categories", tokensArray[i]);
                }
            }
        }
        short[] result = new short[categorySet.size()];
        int p = 0;
        for (Short sh : categorySet) {
            result[p] = sh;
            p++;
        }
        return result;
    }

}
