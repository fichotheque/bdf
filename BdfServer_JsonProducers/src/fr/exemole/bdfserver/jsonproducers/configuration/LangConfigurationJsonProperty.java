/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.json.ConfigurationJson;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class LangConfigurationJsonProperty implements JsonProperty {

    private final BdfServer bdfServer;

    public LangConfigurationJsonProperty(BdfServer bdfServer) {
        this.bdfServer = bdfServer;
    }

    @Override
    public String getName() {
        return "langConfiguration";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        ConfigurationJson.properties(jw, bdfServer.getLangConfiguration());
        jw.endObject();
    }

}
