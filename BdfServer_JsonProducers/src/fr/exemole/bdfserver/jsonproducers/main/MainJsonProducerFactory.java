/* BdfServer_JsonProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.main;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.MainDomain;
import fr.exemole.bdfserver.api.managers.TableExportManager;
import fr.exemole.bdfserver.tools.exportation.table.BdfTableExportUtils;
import fr.exemole.bdfserver.tools.exportation.table.CellEngineUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.ResultJsonProducer;
import net.fichotheque.Subset;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.exportation.table.TableExportContext;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.extraction.ExtractionContext;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class MainJsonProducerFactory {

    private MainJsonProducerFactory() {
    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        String json = parameters.getOutput();
        ResultJsonProducer jsonProducer = new ResultJsonProducer(parameters);
        JsonProperty jsonProperty = getJsonProperty(parameters, json);
        if (jsonProperty == null) {
            return null;
        } else {
            jsonProducer.add(jsonProperty);
            return jsonProducer;
        }
    }

    public static JsonProperty getJsonProperty(OutputParameters parameters, String name) throws ErrorMessageException {
        switch (name) {
            case MainDomain.AVAILABLEPATTERNS_JSON: {
                return new AvailablePatternsJsonProperty(parameters.getBdfServer(), parameters.getBdfUser());
            }
            case MainDomain.TABLE_JSON: {
                return getTableJsonProperty(parameters);
            }
            default:
                return null;
        }
    }

    private static JsonProperty getTableJsonProperty(OutputParameters parameters) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        BdfServer bdfServer = parameters.getBdfServer();
        Subset subset = requestHandler.getMandatorySubset();
        if (!parameters.getPermissionSummary().hasAccess(subset)) {
            return null;
        }
        TableExportManager manager = bdfServer.getTableExportManager();
        SubsetTable subsetTable = null;
        TableExportDef tableExportDef = null;
        String tableExportName = requestHandler.getTrimedParameter(InteractionConstants.TABLEEXPORT_PARAMNAME);
        if (!tableExportName.isEmpty()) {
            TableExport tableExport = manager.getTableExport(tableExportName);
            if (tableExport != null) {
                tableExportDef = tableExport.getTableExportDef();
                subsetTable = tableExport.getSubsetTable(subset.getSubsetKey());
            }
            if (subsetTable == null) {
                throw BdfErrors.error("_ tableexport_err_notableexportcontent", tableExportName, subset.getSubsetKeyString());
            }
        } else {
            subsetTable = BdfTableExportUtils.toDefaultSubsetTable(bdfServer, subset, parameters.getBdfUser().getPrefs().getDefaultFicheTableParameters(), parameters.getPermissionSummary());
        }
        TableExportContext tableExportContext = bdfServer.getTableExportContext();
        ExtractionContext extractionContext = parameters.getDefaultExtractionContext();
        CellEngine cellEngine = CellEngineUtils.newCellEngine(bdfServer, extractionContext, tableExportDef, subsetTable);
        return new TableJsonProperty(bdfServer, parameters.getBdfUser(), subsetTable, tableExportContext, cellEngine);
    }

}
