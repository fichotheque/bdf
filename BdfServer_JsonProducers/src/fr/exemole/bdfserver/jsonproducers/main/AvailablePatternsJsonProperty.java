/* BdfServer_JsonProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.main;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.json.PatternDefJson;
import java.io.IOException;
import java.util.List;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.format.PatternDef;
import net.fichotheque.tools.format.patterndefs.PatternDefs;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class AvailablePatternsJsonProperty implements JsonProperty {

    private final BdfServer bdfServer;
    private final BdfUser bdfUser;

    public AvailablePatternsJsonProperty(BdfServer bdfServer, BdfUser bdfUser) {
        this.bdfServer = bdfServer;
        this.bdfUser = bdfUser;
    }

    @Override
    public String getName() {
        return "availablePatterns";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        {
            valueProperties(jw);
            properties(jw, CorpusField.PERSONNE_FIELD);
            properties(jw, CorpusField.LANGUE_FIELD);
            properties(jw, CorpusField.DATATION_FIELD);
            properties(jw, CorpusField.PAYS_FIELD);
            properties(jw, CorpusField.NOMBRE_FIELD);
            properties(jw, CorpusField.MONTANT_FIELD);
            properties(jw, CorpusField.COURRIEL_FIELD);
            properties(jw, CorpusField.LINK_FIELD);
            properties(jw, CorpusField.ITEM_FIELD);
            properties(jw, CorpusField.GEOPOINT_FIELD);
            properties(jw, CorpusField.PARA_FIELD);
            properties(jw, CorpusField.IMAGE_FIELD);
            properties(jw, "section", PatternDefs.FICHEBLOCK);
            properties(jw, "fiche", PatternDefs.FICHEMETA);
            properties(jw, "motcle_multi", PatternDefs.MOTCLE_MULTI);
            properties(jw, "motcle_idalpha", PatternDefs.MOTCLE_IDALPHA);
            properties(jw, "motcle_babelien", PatternDefs.MOTCLE_BABELIEN);
            properties(jw, "illustration", PatternDefs.ILLUSTRATION);
            properties(jw, "document", PatternDefs.DOCUMENT);
            properties(jw, "extraction", PatternDefs.EXTRACTION);
            properties(jw, "idalpha", PatternDefs.IDALPHA);
        }
        jw.endObject();
    }

    private void valueProperties(JSONWriter jw) throws IOException {
        jw.key("value");
        jw.array();
        {
            jw.object();
            {
                jw.key("name")
                        .value("value");
            }
            jw.endObject();
        }
        jw.endArray();
    }

    private void properties(JSONWriter jw, short ficheItemType) throws IOException {
        List<PatternDef> patternDefList = PatternDefs.ficheItem(ficheItemType);
        AvailablePatternsJsonProperty.this.properties(jw, CorpusField.ficheItemTypeToString(ficheItemType), patternDefList);
    }

    private void properties(JSONWriter jw, String key, List<PatternDef> patternDefList) throws IOException {
        jw.key(key);
        jw.array();
        for (PatternDef patternDef : patternDefList) {
            jw.object();
            PatternDefJson.properties(jw, patternDef, bdfServer);
            jw.endObject();
        }
        jw.endArray();
    }


}
