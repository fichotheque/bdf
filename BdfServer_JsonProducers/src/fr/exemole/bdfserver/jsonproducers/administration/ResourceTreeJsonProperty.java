/* BdfServer_JsonProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.administration;

import fr.exemole.bdfserver.json.ResourceTreeJson;
import java.io.IOException;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class ResourceTreeJsonProperty implements JsonProperty {

    private final ResourceStorages resourceStorages;

    public ResourceTreeJsonProperty(ResourceStorages resourceStorages) {
        this.resourceStorages = resourceStorages;
    }

    @Override
    public String getName() {
        return "resourceTree";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        ResourceTreeJson.properties(jw, resourceStorages);
        jw.endObject();
    }

}
