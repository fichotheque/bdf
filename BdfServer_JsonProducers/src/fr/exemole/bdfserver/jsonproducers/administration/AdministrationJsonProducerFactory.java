/* BdfServer_JsonProducers - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.AdministrationDomain;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.tools.BdfURI;
import fr.exemole.bdfserver.tools.diagnostic.urlscan.UrlInfo;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.OutputRequestHandler;
import fr.exemole.bdfserver.tools.instruction.ResultJsonProducer;
import java.net.URI;
import java.util.Collection;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class AdministrationJsonProducerFactory {

    private AdministrationJsonProducerFactory() {
    }

    public static JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        String json = parameters.getOutput();
        ResultJsonProducer jsonProducer = new ResultJsonProducer(parameters);
        JsonProperty jsonProperty = getJsonProperty(parameters, json);
        if (jsonProperty == null) {
            return null;
        } else {
            jsonProducer.add(jsonProperty);
            return jsonProducer;
        }
    }

    public static JsonProperty getJsonProperty(OutputParameters parameters, String name) throws ErrorMessageException {
        OutputRequestHandler requestHandler = OutputRequestHandler.init(parameters);
        BdfServer bdfServer = parameters.getBdfServer();
        Lang lang = parameters.getWorkingLang();
        switch (name) {
            case AdministrationDomain.RESOURCE_TREE_JSON: {
                bdfServer.getL10nManager().update();
                bdfServer.getTransformationManager().update();
                bdfServer.getJsAnalyser().clearCache();
                return new ResourceTreeJsonProperty(bdfServer.getResourceStorages());
            }
            case AdministrationDomain.STREAMTEXT_JSON: {
                String path = requestHandler.getMandatoryPath();
                URI uri = BdfURI.toAbsoluteBdfURI(path);
                if (uri == null) {
                    throw BdfErrors.wrongParameterValue(InteractionConstants.PATH_PARAMNAME, path);
                }
                return new StreamTextJsonProperty(bdfServer, uri);
            }
            case AdministrationDomain.ROLE_ARRAY_JSON: {
                parameters.checkFichothequeAdmin();
                return new RoleArrayJsonProperty(bdfServer, lang);
            }
            case AdministrationDomain.ROLE_JSON: {
                parameters.checkFichothequeAdmin();
                Role role = requestHandler.getRole();
                return new RoleJsonProperty(role, lang);
            }
            case AdministrationDomain.URLINFOS_JSON: {
                parameters.checkFichothequeAdmin();
                Collection<UrlInfo> infos = (Collection<UrlInfo>) parameters.getResultObject(BdfInstructionConstants.URLINFOCOLLECTION_OBJ);
                if (infos != null) {
                    return new UrlInfosJsonProperty(infos, parameters.getBdfUser());
                } else {
                    throw BdfErrors.missingCommandResultJson(AdministrationDomain.URLINFOS_JSON);
                }
            }
            default:
                return null;
        }
    }

}
