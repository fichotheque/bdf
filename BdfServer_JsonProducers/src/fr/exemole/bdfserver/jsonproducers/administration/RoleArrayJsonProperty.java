/* BdfServer_JsonProducers - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.json.RoleJson;
import java.io.IOException;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class RoleArrayJsonProperty implements JsonProperty {

    private final BdfServer bdfServer;
    private final Lang lang;


    public RoleArrayJsonProperty(BdfServer bdfServer, Lang lang) {
        this.bdfServer = bdfServer;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "roleArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (Role role : bdfServer.getPermissionManager().getRoleList()) {
            jw.object();
            RoleJson.properties(jw, role);
            CommonJson.title(jw, role.getTitleLabels(), lang);
            jw.endObject();
        }
        jw.endArray();
    }

}
