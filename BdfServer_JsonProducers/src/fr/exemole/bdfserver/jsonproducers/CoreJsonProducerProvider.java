/* BdfServer_JsonProducers - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.jsonproducers;

import fr.exemole.bdfserver.api.instruction.OutputParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.providers.JsonProducerProvider;
import fr.exemole.bdfserver.jsonproducers.addenda.AddendaJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.administration.AdministrationJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.album.AlbumJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.configuration.ConfigurationJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.corpus.CorpusJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.edition.EditionJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.exportation.ExportationJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.importation.ImportationJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.main.MainJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.misc.MiscJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.pioche.PiocheJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.selection.SelectionJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.session.SessionJsonProducerFactory;
import fr.exemole.bdfserver.jsonproducers.thesaurus.ThesaurusJsonProducerFactory;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public class CoreJsonProducerProvider implements JsonProducerProvider {

    public final static JsonProducerProvider UNIQUE_INSTANCE = new CoreJsonProducerProvider();

    public CoreJsonProducerProvider() {
    }

    @Override
    public JsonProducer getJsonProducer(OutputParameters parameters) throws ErrorMessageException {
        switch (parameters.getDomain().getFirstPart()) {
            case Domains.ADDENDA:
                return AddendaJsonProducerFactory.getJsonProducer(parameters);
            case Domains.ADMINISTRATION:
                return AdministrationJsonProducerFactory.getJsonProducer(parameters);
            case Domains.ALBUM:
                return AlbumJsonProducerFactory.getJsonProducer(parameters);
            case Domains.CONFIGURATION:
                return ConfigurationJsonProducerFactory.getJsonProducer(parameters);
            case Domains.CORPUS:
                return CorpusJsonProducerFactory.getJsonProducer(parameters);
            case Domains.EDITION:
                return EditionJsonProducerFactory.getJsonProducer(parameters);
            case Domains.EXPORTATION:
                return ExportationJsonProducerFactory.getJsonProducer(parameters);
            case Domains.IMPORTATION:
                return ImportationJsonProducerFactory.getJsonProducer(parameters);
            case Domains.MAIN:
                return MainJsonProducerFactory.getJsonProducer(parameters);
            case Domains.MISC:
                return MiscJsonProducerFactory.getJsonProducer(parameters);
            case Domains.PIOCHE:
                return PiocheJsonProducerFactory.getJsonProducer(parameters);
            case Domains.SESSION:
                return SessionJsonProducerFactory.getJsonProducer(parameters);
            case Domains.THESAURUS:
                return ThesaurusJsonProducerFactory.getJsonProducer(parameters);
            case Domains.SELECTION:
                return SelectionJsonProducerFactory.getJsonProducer(parameters);
            default:
                return null;
        }
    }

}
