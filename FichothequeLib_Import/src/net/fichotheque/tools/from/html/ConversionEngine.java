/* FichothequeLib_Import - Copyright (c) 2019-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.from.html;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.corpus.fiche.P;
import net.fichotheque.corpus.fiche.S;
import net.fichotheque.tools.from.html.handlers.DefaultHandler;
import net.fichotheque.utils.FicheUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;


/**
 *
 * @author Vincent Calame
 */
public final class ConversionEngine {

    private ConversionEngine() {

    }

    public static FicheBlocks convertHtmlPage(String htmlPage) {
        return convertDocument(Jsoup.parse(htmlPage));
    }

    public static FicheBlocks convertHtmlFragment(String htmlFragment) {
        return convertDocument(Jsoup.parseBodyFragment(htmlFragment));
    }

    public static FicheBlocks convertDocument(Document document) {
        return convertElement(document.body(), DefaultHandler.INSTANCE);
    }

    public static FicheBlocks convertElement(Element parent, ElementHandler elementHandler) {
        InternalFlowBuffer flowBuffer = new InternalFlowBuffer();
        flowBuffer.run(parent, elementHandler);
        return FicheUtils.toFicheBlocks(flowBuffer.result);
    }

    public static void convertElement(Element parent, ElementHandler elementHandler, FlowBuffer parentFlowBuffer) {
        InternalFlowBuffer flowBuffer = new InternalFlowBuffer();
        flowBuffer.run(parent, elementHandler);
        for (FicheBlock ficheBlock : flowBuffer.result) {
            parentFlowBuffer.addFicheBlock(ficheBlock);
        }
    }


    private static class InternalFlowBuffer implements FlowBuffer {

        private final List<FicheBlock> result = new ArrayList<FicheBlock>();
        private final List<Node> inlineNodeList = new ArrayList<Node>();
        private int emptyLines = 0;

        private InternalFlowBuffer() {

        }

        private void run(Element element, ElementHandler elementHandler) {
            int skip = 0;
            for (Node node : element.childNodes()) {
                if (skip > 0) {
                    skip--;
                    continue;
                }
                if (node instanceof TextNode) {
                    inlineNodeList.add(node);
                } else if (node instanceof Element) {
                    Element child = (Element) node;
                    skip = elementHandler.handle(child, this);
                    if (skip == 0) {
                        if (InlineConversion.canConvert(child)) {
                            inlineNodeList.add(child);
                        } else {
                            String text = child.text();
                            if (text.length() > 0) {
                                inlineNodeList.add(new TextNode(text));
                            }
                        }
                    } else {
                        skip--;
                    }
                }
            }
            flushInline();
        }

        @Override
        public void flushInline() {
            if (inlineNodeList.isEmpty()) {
                return;
            }
            ParagraphBuffer buf = new ParagraphBuffer();
            InlineConversion.populate(buf, inlineNodeList);
            if (buf.size() > 0) {
                P p = new P();
                buf.flush(p);
                result.add(p);
                emptyLines = 0;
            }
            inlineNodeList.clear();
        }

        @Override
        public void addFicheBlock(FicheBlock ficheBlock) {
            flushInline();
            if (ficheBlock != null) {
                if (isWhiteLine(ficheBlock)) {
                    emptyLines++;
                } else {
                    if ((emptyLines > 1) && (ficheBlock instanceof P)) {
                        for (int i = 1; i < emptyLines; i++) {
                            result.add(new P());
                        }
                    }
                    emptyLines = 0;
                    result.add(ficheBlock);
                }
            }
        }

    }

    private static boolean isWhiteLine(FicheBlock ficheBlock) {
        if (!(ficheBlock instanceof P)) {
            return false;
        }
        P p = (P) ficheBlock;
        int size = p.size();
        if (size == 0) {
            return true;
        }
        if (size == 1) {
            Object first = p.get(0);
            if ((first instanceof S) && (((S) first).getType() == S.BR)) {
                return true;
            }
        }
        return false;
    }

}
