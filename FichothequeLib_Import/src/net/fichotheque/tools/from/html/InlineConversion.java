/* FichothequeLib_Import - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.from.html;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.corpus.fiche.P;
import net.fichotheque.corpus.fiche.S;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;


/**
 *
 * @author Vincent Calame
 */
public final class InlineConversion {

    private InlineConversion() {

    }

    public static P toP(List<Node> inlineNodeList) {
        P p = new P();
        ParagraphBuffer buf = new ParagraphBuffer();
        populate(buf, inlineNodeList);
        buf.flush(p);
        return p;
    }

    public static boolean canConvert(Element element) {
        switch (element.tagName()) {
            case "a":
            case "abbr":
            case "b":
            case "br":
            case "cite":
            case "code":
            case "del":
            case "dfn":
            case "em":
            case "font":
            case "i":
            case "img":
            case "ins":
            case "kbd":
            case "q":
            case "samp":
            case "span":
            case "strong":
            case "sub":
            case "sup":
            case "u":
            case "var":
                return true;
            default:
                return false;
        }
    }

    public static boolean populate(ParagraphBuffer paragraphBuffer, List<Node> nodeList) {
        boolean result = false;
        for (Node node : nodeList) {
            if (node instanceof TextNode) {
                boolean done = paragraphBuffer.addPart(((TextNode) node).text());
                if (done) {
                    result = true;
                }
            } else {
                boolean done = appendTo(paragraphBuffer, (Element) node);
                if (done) {
                    result = true;
                }
            }
        }
        return result;
    }

    public static boolean appendTo(ParagraphBuffer paragraphBuffer, Element element) {
        if (isIgnorable(element)) {
            return populate(paragraphBuffer, element.childNodes());
        }
        if (isSplitable(element)) {
            return populate(paragraphBuffer, split(element));
        }
        String wholeText = element.wholeText();
        int wholeTextLength = wholeText.length();
        if (wholeTextLength > 0) {
            if (Character.isWhitespace(wholeText.charAt(0))) {
                paragraphBuffer.addPart(" ");
            }
        }
        S s = convert(element);
        boolean done;
        if (s != null) {
            done = paragraphBuffer.addPart(s);
        } else {
            done = paragraphBuffer.addPart(element.text());
        }
        if (wholeTextLength > 0) {
            if (Character.isWhitespace(wholeText.charAt(wholeTextLength - 1))) {
                boolean spaceDone = paragraphBuffer.addPart(" ");
                if (spaceDone) {
                    done = true;
                }
            }
        }
        return done;
    }

    private static boolean isIgnorable(Element element) {
        switch (element.tagName()) {
            case "font":
            case "span":
                return true;
            default:
                return false;
        }
    }

    private static boolean isSplitable(Element element) {
        switch (element.tagName()) {
            case "i":
            case "u":
            case "em":
            case "b":
            case "strong":
                if (element.childNodeSize() > 1) {
                    return true;
                } else {
                    return false;
                }
            default:
                return false;
        }
    }

    private static List<Node> split(Element element) {
        String wrap = "<" + element.tagName() + "></" + element.tagName() + ">";
        List<Node> result = new ArrayList<Node>();
        for (Node node : element.childNodes()) {
            result.add(node.wrap(wrap).parent());
        }
        return result;
    }

    private static S convert(Element element) {
        switch (element.tagName()) {
            case "a":
                return convertLink(element);
            case "b":
            case "strong":
                return convertStrg(element);
            case "i":
            case "u":
            case "em":
                return convertEm(element);
            case "img":
                return convertImage(element, null);
            case "abbr":
                return new S(S.ABBR);
            case "br":
                return new S(S.BR);
            case "cite":
                return convert(S.CITE, element);
            case "code":
                return convert(S.CODE, element);
            case "del":
                return convert(S.DELETE, element);
            case "dfn":
                return convert(S.DEFINITION, element);
            case "ins":
                return convert(S.INSERT, element);
            case "kbd":
                return convert(S.KEYBOARD, element);
            case "q":
                return convert(S.QUOTE, element);
            case "samp":
                return convert(S.SAMPLE, element);
            case "sub":
                return convert(S.SUB, element);
            case "sup":
                return convert(S.SUP, element);
            case "var":
                return convert(S.VAR, element);
            default:
                return null;
        }
    }

    private static S convert(short type, Element element) {
        return toS(type, element, getRefElement(element));
    }

    private static S convertImage(Element element, Element linkElement) {
        S span = new S(S.IMAGE);
        span.setRef(element.attr("src"));
        addTitle(span, element, linkElement);
        span.setValue(element.attr("alt"));
        if (linkElement != null) {
            addLinkAttributes(linkElement, span);
            addAttribute(linkElement, span, "href");
        }
        return span;
    }

    private static S convertLink(Element element) {
        short type = S.LINK;
        Element loneChild = getLoneChild(element);
        if (loneChild != null) {
            if (loneChild.tagName().equals("img")) {
                return convertImage(loneChild, element);
            }
            type = checkEmStrg(type, loneChild);
        }
        return toS(type, element, element);
    }

    private static S convertStrg(Element element) {
        Element loneChild = getLoneChild(element);
        short type = S.STRONG;
        Element refChild = null;
        if (loneChild != null) {
            refChild = getRefElement(loneChild);
            type = checkEmStrg(type, loneChild);
        }
        return toS(type, element, refChild);
    }

    private static S convertEm(Element element) {
        Element loneChild = getLoneChild(element);
        short type = S.EMPHASIS;

        Element refChild = null;
        if (loneChild != null) {
            refChild = getRefElement(loneChild);
            type = checkEmStrg(type, loneChild);
        }
        if (element.hasClass("STRG")) {
            type = S.EMSTRG;
        }
        return toS(type, element, refChild);
    }

    private static S toS(short type, Element mainElement, Element refElement) {
        S span = new S(type);
        if (refElement != null) {
            span.setRef(refElement.attr("href"));
            addLinkAttributes(refElement, span);
        }
        addTitle(span, mainElement, refElement);
        span.setValue(mainElement.text());
        return span;
    }

    private static Element getLoneChild(Element parent) {
        if (parent.childNodeSize() == 1) {
            Node child = parent.childNode(0);
            if (child instanceof Element) {
                return (Element) child;
            }
        }
        Element loneChild = null;
        for (Node node : parent.childNodes()) {
            if (node instanceof Element) {
                if (loneChild == null) {
                    loneChild = (Element) node;
                } else {
                    return null;
                }
            } else if (node instanceof TextNode) {
                String text = ((TextNode) node).text();
                if (text.trim().length() > 0) {
                    return null;
                }
            }
        }
        return loneChild;
    }

    private static void addTitle(S s, Element firstElement, Element secondElement) {
        String title = firstElement.attr("title");
        if ((title.isEmpty()) && (secondElement != null)) {
            title = secondElement.attr("title");
        }
        if (!title.isEmpty()) {
            s.putAtt("title", title);
        }
    }

    private static Element getRefElement(Element element) {
        if (element.tagName().equals("a")) {
            return element;
        }
        Element loneChild = getLoneChild(element);
        if (loneChild != null) {
            return getRefElement(loneChild);
        }
        return null;
    }

    private static short checkEmStrg(short type, Element element) {
        switch (element.tagName()) {
            case "b":
            case "strong":
                if (type == S.EMPHASIS) {
                    type = S.EMSTRG;
                } else {
                    type = S.STRONG;
                }
                break;
            case "i":
            case "u":
            case "em":
                if (type == S.STRONG) {
                    type = S.EMSTRG;
                } else {
                    type = S.EMPHASIS;
                }
                break;
        }
        if (type == S.EMSTRG) {
            return type;
        }
        Element loneChild = getLoneChild(element);
        if (loneChild != null) {
            type = checkEmStrg(type, loneChild);
        }
        return type;
    }

    private static void addLinkAttributes(Element linkElement, S span) {
        addAttribute(linkElement, span, "hreflang");
        addAttribute(linkElement, span, "rel");
        addAttribute(linkElement, span, "target");
    }

    private static void addAttribute(Element element, S span, String name) {
        String value = element.attr(name);
        if (!value.isEmpty()) {
            span.putAtt(name, value);
        }
    }

}
