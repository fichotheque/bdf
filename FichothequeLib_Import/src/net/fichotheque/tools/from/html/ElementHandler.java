/* FichothequeLib_Import - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.from.html;

import org.jsoup.nodes.Element;


/**
 *
 * @author Vincent Calame
 */
@FunctionalInterface
public interface ElementHandler {

    public int handle(Element element, FlowBuffer flowBuffer);

}
