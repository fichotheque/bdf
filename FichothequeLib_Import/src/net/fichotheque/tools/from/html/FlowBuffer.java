/* FichothequeLib_Import - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.from.html;

import net.fichotheque.corpus.fiche.FicheBlock;


/**
 *
 * @author Vincent Calame
 */
public interface FlowBuffer {

    public void flushInline();

    public void addFicheBlock(FicheBlock ficheBlock);

}
