/* BDF - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.servlets;

import fr.exemole.bdfserver.conf.WebappDirs;
import fr.exemole.bdfserver.email.SmtpManager;
import java.util.List;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public class BDFParameters {

    private final boolean multiBdf;
    private final boolean loginPasswordDisabled;
    private final String authentificationSharing;
    private final List<SubsetKey> centralSphereList;
    private final SmtpManager smtpManager;
    private final WebappDirs webappDirs;

    public BDFParameters(WebappDirs webappDirs, boolean loginPasswordDisabled, String authentificationSharing, List<SubsetKey> centralSphereList, SmtpManager smtpManager) {
        this.multiBdf = webappDirs.isMultiBdf();
        this.webappDirs = webappDirs;
        this.loginPasswordDisabled = loginPasswordDisabled;
        this.authentificationSharing = authentificationSharing;
        this.centralSphereList = centralSphereList;
        this.smtpManager = smtpManager;
    }

    public boolean multiBdf() {
        return multiBdf;
    }


    public boolean loginPasswordDisabled() {
        return loginPasswordDisabled;
    }

    public String authentificationSharing() {
        return authentificationSharing;
    }

    public List<SubsetKey> centralSphereList() {
        return centralSphereList;
    }

    public SmtpManager smtpManager() {
        return smtpManager;
    }

    public WebappDirs webappDirs() {
        return webappDirs;
    }

}
