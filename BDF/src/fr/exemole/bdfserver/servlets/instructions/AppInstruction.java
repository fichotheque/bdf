/* BDF - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.servlets.instructions;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.html.AppHtmlProducer;
import fr.exemole.bdfserver.html.BdfServerHtmlProducer;
import fr.exemole.bdfserver.html.MessageHtmlProducer;
import fr.exemole.bdfserver.tools.apps.AppConf;
import fr.exemole.bdfserver.tools.apps.AppConfUtils;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import fr.exemole.bdfserver.tools.instruction.DefaultBdfParameters;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;


/**
 *
 * @author Vincent Calame
 */
public class AppInstruction implements BdfInstruction {

    private final static CommandMessage INACTIVE_MESSAGE = LogUtils.error("_ error.unsupported.inactiveapp");
    private final BdfServer bdfServer;
    private final RequestMap requestMap;
    private final AppConf appConf;

    private AppInstruction(BdfServer bdfServer, RequestMap requestMap, AppConf appConf) {
        this.bdfServer = bdfServer;
        this.requestMap = requestMap;
        this.appConf = appConf;
    }

    @Override
    public short getBdfUserNeed() {
        String userNeedString = appConf.getString(AppConf.CORE_BDFUSERNEED, "mandatory");
        if (userNeedString.equals("none")) {
            return BdfInstructionConstants.NONE_BDFUSER;
        } else if (requestMap.isTrue(InteractionConstants.BDF_EXIT_PARAMNAME)) {
            return BdfInstructionConstants.EXIT_BDFUSER;
        } else if (userNeedString.equals("if_any")) {
            return BdfInstructionConstants.IF_ANY_BDFUSER;
        } else {
            return BdfInstructionConstants.MANDATORY_BDFUSER;
        }
    }

    @Override
    public ResponseHandler runInstruction(BdfUser bdfUser) {
        boolean isActive = appConf.getBoolean(AppConf.ACTIVE);
        BdfServerHtmlProducer appHtmlProducer;
        if (bdfUser != null) {
            BdfParameters bdfParameters = new DefaultBdfParameters(bdfServer, bdfUser);
            if (isActive) {
                appHtmlProducer = new AppHtmlProducer(bdfParameters, appConf);
            } else {
                appHtmlProducer = initInactiveMessage(new MessageHtmlProducer(bdfParameters));
            }
        } else {
            LangPreference langPreference = BdfInstructionUtils.getLangPreference(requestMap, bdfServer);
            if (isActive) {
                appHtmlProducer = new AppHtmlProducer(bdfServer, langPreference, appConf);
            } else {
                appHtmlProducer = initInactiveMessage(new MessageHtmlProducer(bdfServer, langPreference));
            }
        }
        if (requestMap.isTrue("reload")) {
            bdfServer.getL10nManager().update();
            bdfServer.getTransformationManager().update();
            bdfServer.getJsAnalyser().clearCache();
        }
        return ServletUtils.wrap(appHtmlProducer);
    }

    @Override
    public Object getLoginOption(String optionName) {
        switch (optionName) {
            case SESSIONMESSAGE_OPTION: {
                String messageKey = appConf.getString(AppConf.LOGIN_SESSIONMESSAGE);
                if (messageKey != null) {
                    return LogUtils.done(messageKey);
                } else {
                    return null;
                }
            }
            case AVAILABLESPHERES_OPTION:
                return appConf.getArray(AppConf.LOGIN_AVAILABLESPHERES);
            case DEFAULTSPHERE_OPTION:
                return appConf.getString(AppConf.LOGIN_DEFAULTSPHERE);
            case TITLEPHRASENAME_OPTION:
                String titlePhraseName = appConf.getString(AppConf.CORE_TITLEPHRASENAME);
                if (titlePhraseName == null) {
                    titlePhraseName = appConf.getString(AppConf.LOGIN_TITLEPHRASENAME);
                }
                return titlePhraseName;
            default:
                return null;
        }
    }

    public static AppInstruction build(BdfServer bdfServer, RequestMap requestMap, String appName) {
        if (appName.isEmpty()) {
            return null;
        }
        AppConf appConf = AppConfUtils.getAppConf(bdfServer.getResourceStorages(), appName);
        if (appConf != null) {
            return new AppInstruction(bdfServer, requestMap, appConf);
        } else {
            return null;
        }
    }

    private static BdfServerHtmlProducer initInactiveMessage(MessageHtmlProducer messageHtmlProducer) {
        messageHtmlProducer
                .addMessages(INACTIVE_MESSAGE)
                .setBodyCssClass("global-body-Message");
        return messageHtmlProducer;
    }

}
