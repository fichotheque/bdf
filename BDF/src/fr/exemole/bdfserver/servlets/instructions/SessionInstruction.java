/* BDF - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.servlets.instructions;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.htmlproducers.main.FramesetHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.main.IframesHtmlProducer;
import fr.exemole.bdfserver.htmlproducers.main.MainHtmlUtils;
import fr.exemole.bdfserver.jsonproducers.session.PingJsonProducer;
import fr.exemole.bdfserver.tools.BdfHrefProvider;
import fr.exemole.bdfserver.tools.instruction.DefaultBdfParameters;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.request.OutputInfo;
import net.mapeadores.util.request.RequestConstants;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.request.RequestUtils;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;
import net.mapeadores.util.servlets.handlers.JsonResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public class SessionInstruction implements BdfInstruction {

    private final BdfServer bdfServer;
    private final RequestMap requestMap;

    public SessionInstruction(BdfServer bdfServer, RequestMap requestMap) {
        this.bdfServer = bdfServer;
        this.requestMap = requestMap;
    }

    @Override
    public short getBdfUserNeed() {
        if (requestMap.isTrue(InteractionConstants.BDF_EXIT_PARAMNAME)) {
            return BdfInstructionConstants.EXIT_BDFUSER;
        } else {
            return BdfInstructionConstants.MANDATORY_BDFUSER;
        }
    }

    @Override
    public ResponseHandler runInstruction(BdfUser bdfUser) {
        OutputInfo outputInfo = OutputInfo.buildFromRequest(requestMap);
        if (outputInfo.getType() == OutputInfo.JSON_TYPE) {
            short jsonType = RequestUtils.getJsonType(requestMap);
            String callback = requestMap.getParameter(RequestConstants.CALLBACK_PARAMETER);
            JsonProducer jsonProducer = PingJsonProducer.authentified(bdfServer, bdfUser);
            JsonResponseHandler jsonResponseHandler = new JsonResponseHandler(jsonProducer, jsonType, callback);
            ServletUtils.allowCredentials(jsonResponseHandler, requestMap);
            return jsonResponseHandler;
        } else {
            BdfParameters bdfParameters = new DefaultBdfParameters(bdfServer, bdfUser);
            BdfHrefProvider bdfHrefProvider = MainHtmlUtils.getMainProvider(requestMap);
            HtmlProducer htmlProducer;
            if (BdfUserUtils.isWithJavascript(bdfUser)) {
                htmlProducer = new IframesHtmlProducer(bdfParameters, bdfHrefProvider);
            } else {
                htmlProducer = new FramesetHtmlProducer(bdfParameters, bdfHrefProvider);
            }
            return ServletUtils.wrap(htmlProducer);
        }
    }

}
