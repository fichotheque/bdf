/* BDF - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.servlets.instructions;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.api.instruction.BdfInstruction;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.managers.BalayageManager;
import fr.exemole.bdfserver.api.managers.ScrutariExportManager;
import fr.exemole.bdfserver.api.managers.SqlExportManager;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.configuration.PathConfigurationBuilder;
import fr.exemole.bdfserver.tools.runners.BalayageRunner;
import fr.exemole.bdfserver.tools.runners.ScrutariExportRunner;
import fr.exemole.bdfserver.tools.runners.SqlExportRunner;
import java.io.IOException;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;


/**
 *
 * @author Vincent Calame
 */
public class RunInstruction implements BdfInstruction {

    private final BdfServer bdfServer;
    private final RequestMap requestMap;
    private final PathConfiguration pathConfiguration;

    public RunInstruction(BdfServer bdfServer, RequestMap requestMap) {
        this.bdfServer = bdfServer;
        this.requestMap = requestMap;
        this.pathConfiguration = PathConfigurationBuilder.build(bdfServer);
    }

    @Override
    public short getBdfUserNeed() {
        return BdfInstructionConstants.NONE_BDFUSER;
    }

    @Override
    public ResponseHandler runInstruction(BdfUser bdfUser) {
        String[] sqlExportArray = requestMap.getParameterValues("sqlexport");
        int result = -1;
        if (sqlExportArray != null) {
            if (result == -1) {
                result = 0;
            }
            result = result + runSqlExportArray(bdfServer, sqlExportArray);
        }
        String[] scrutariExportArray = requestMap.getParameterValues("scrutariexport");
        if (scrutariExportArray != null) {
            if (result == -1) {
                result = 0;
            }
            result = result + runScrutariExportArray(bdfServer, scrutariExportArray);
        }
        String[] balayageArray = requestMap.getParameterValues("balayage");
        if (balayageArray != null) {
            if (result == -1) {
                result = 0;
            }
            result = result + runBalayageArray(bdfServer, balayageArray);
        }
        return ServletUtils.wrap(String.valueOf(result));
    }

    private int runSqlExportArray(BdfServer bdfServer, String[] array) {
        int errorCount = 0;
        SqlExportManager sqlExportManager = bdfServer.getSqlExportManager();
        for (int i = 0; i < array.length; i++) {
            SqlExportDef sqlExportDef = sqlExportManager.getSqlExportDef(array[i]);
            if (sqlExportDef != null) {
                try {
                    CommandMessage message = SqlExportRunner.run(sqlExportDef, bdfServer, pathConfiguration, true);
                    if (message.isErrorMessage()) {
                        errorCount++;
                    }
                } catch (IOException ioe) {
                    errorCount++;
                }
            } else {
                errorCount++;
            }
        }
        return errorCount;
    }

    private int runScrutariExportArray(BdfServer bdfServer, String[] array) {
        int result = 0;
        ScrutariExportManager scrutariExportManager = bdfServer.getScrutariExportManager();
        for (int i = 0; i < array.length; i++) {
            ScrutariExportDef scrutariExportDef = scrutariExportManager.getScrutariExportDef(array[i]);
            if (scrutariExportDef != null) {
                try {
                    ScrutariExportRunner.run(scrutariExportDef, bdfServer, pathConfiguration);
                } catch (IOException ioe) {
                    result++;
                }
            } else {
                result++;
            }
        }
        return result;
    }

    private int runBalayageArray(BdfServer bdfServer, String[] array) {
        int result = 0;
        BalayageManager balayageManager = bdfServer.getBalayageManager();
        for (int i = 0; i < array.length; i++) {
            String balayageName = array[i];
            String mode = null;
            int idx = balayageName.indexOf('/');
            if (idx != -1) {
                mode = balayageName.substring(idx + 1);
                balayageName = balayageName.substring(0, idx);
            }
            BalayageDescription balayageDescription = balayageManager.getBalayage(balayageName);
            if (balayageDescription != null) {
                BalayageRunner.Result balayageResult = BalayageRunner.run(balayageDescription, mode, bdfServer, pathConfiguration);
                if (balayageResult.hasError()) {
                    result++;
                }
            } else {
                result++;
            }
        }
        return result;
    }

}
