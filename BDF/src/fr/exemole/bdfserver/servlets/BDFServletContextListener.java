/* BDF - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import net.mapeadores.util.css.SacParserInit;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.servlets.ServletUtils;
import net.mapeadores.util.text.tableparser.TableParser;


/**
 *
 * @author Vincent Calame
 */
public class BDFServletContextListener implements ServletContextListener {

    public BDFServletContextListener() {

    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        File implementationPropFile = ServletUtils.getRealFile(servletContext, "WEB-INF/implementation.properties");
        if ((implementationPropFile != null) && (!implementationPropFile.isDirectory())) {
            Properties implementationProp = new Properties();
            try (InputStream is = new FileInputStream(implementationPropFile)) {
                implementationProp.load(is);
            } catch (IOException ioe) {
                throw new NestedIOException(ioe);
            }
            String sacparser = implementationProp.getProperty("sacparser");
            if (sacparser != null) {
                SacParserInit.init(sacparser);
            }
            String csvparser = implementationProp.getProperty("csvparser");
            if (csvparser != null) {
                TableParser.initCsvParser(csvparser);
            }
        }
        BDFWebapp bdfWebapp = BDFWebapp.build(servletContext);
        servletContext.setAttribute(BDFWebapp.BDFWEBAPP_KEY, bdfWebapp);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
