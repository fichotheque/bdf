/* BDF - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.servlets;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.servlets.HttpAccessException;
import net.mapeadores.util.servlets.HttpServletRequestMap;
import net.mapeadores.util.servlets.MultiPartParser;
import net.mapeadores.util.servlets.exceptions.NotFoundException;
import net.mapeadores.util.servlets.multipart.DefaultMultiPartParser;


/**
 *
 * @author Vincent Calame
 */
public class BDFServlet extends HttpServlet {

    private final MultiPartParser multiPartParser = new DefaultMultiPartParser();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            processRequest(request, response);
        } catch (HttpAccessException hae) {
            sendError(request, response, hae);
        }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            processRequest(request, response);
        } catch (HttpAccessException hae) {
            sendError(request, response, hae);
        }
    }

    private void sendError(HttpServletRequest request, HttpServletResponse response, HttpAccessException hae) throws IOException {
        String messageText = hae.getMessage();
        if (messageText == null) {
            messageText = request.getServletPath() + request.getPathInfo();
        }
        response.sendError(hae.getHttpType(), messageText);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            throw new NotFoundException();
        }
        BDFWebapp bdfWebapp = (BDFWebapp) getServletContext().getAttribute(BDFWebapp.BDFWEBAPP_KEY);
        if (pathInfo.equals("/")) {
            if (bdfWebapp.isMultiBdf()) {
                String fichothequeRedirect = BDFUtils.getFichothequeRedirect(bdfWebapp.getMulti());
                if (fichothequeRedirect != null) {
                    response.sendRedirect(fichothequeRedirect + "/" + Domains.SESSION);
                } else {
                    response.sendRedirect("multi-admin");
                }
            } else {
                response.sendRedirect(Domains.SESSION);
            }
            return;
        }
        pathInfo = pathInfo.substring(1);
        HttpServletRequestMap requestMap = new HttpServletRequestMap(request, multiPartParser);
        if (bdfWebapp.isMultiBdf()) {
            try {
                BDFRoutes.resolveMulti(bdfWebapp, pathInfo, requestMap, response);
            } catch (HttpAccessException hae) {
                if (hae.isUnresolvedMessage()) {
                    hae.resolveMessage(bdfWebapp.getMulti().getAdminMessageLocalisation());
                }
                throw hae;
            }
        } else {
            BdfServer bdfServer = bdfWebapp.getUnique().getBdfServer();
            try {
                BDFRoutes.resolveBdfServer(bdfWebapp, bdfServer, pathInfo, requestMap, response);
            } catch (HttpAccessException hae) {
                if (hae.isUnresolvedMessage()) {
                    hae.resolveMessage(BdfInstructionUtils.getMessageLocalisation(requestMap, bdfServer, null));
                }
                throw hae;
            }
        }
    }

}
