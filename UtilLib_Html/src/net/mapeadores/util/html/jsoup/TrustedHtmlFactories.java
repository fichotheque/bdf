/* UtilLib_Html - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html.jsoup;

import net.mapeadores.util.html.TrustedHtmlFactory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


/**
 *
 * @author Vincent Calame
 */
public final class TrustedHtmlFactories {

    public final static TrustedHtmlFactory WELLFORMED = new WellformedTrustedHtmlFactory();
    public final static TrustedHtmlFactory TEXT = new TextTrustedHtmlFactory();

    private TrustedHtmlFactories() {

    }


    private static class WellformedTrustedHtmlFactory extends TrustedHtmlFactory {

        private WellformedTrustedHtmlFactory() {

        }

        @Override
        public String check(String html) {
            Document doc = Jsoup.parseBodyFragment(html);
            return doc.body().html();
        }

    }


    private static class TextTrustedHtmlFactory extends TrustedHtmlFactory {

        private TextTrustedHtmlFactory() {

        }

        @Override
        public String check(String html) {
            Document doc = Jsoup.parseBodyFragment(html);
            return doc.body().text();
        }

    }

}
