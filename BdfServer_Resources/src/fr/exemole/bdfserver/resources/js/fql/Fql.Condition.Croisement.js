/* global Fql */

Fql.Condition.Croisement = function (data) {
  this.modeArray = new Array();
  this.weightRangeCondition = null;
  this.custom = {};
  if (data) {
        if (typeof data === "string") {
            this.addModes(data);
        } else if (typeof data === "number") {
            this.setWeightRangeCondition(data);
        } else {
            for(let key in data) {
                let value = data[key];
                switch(key) {
                    case "modes":
                        this.addModes(value);
                        break;
                    case "weight":
                        this.setWeightRangeCondition(value);
                        break;
                    default:
                        this.custom[key] = value;
                }
            }
        }
    }
};

Fql.Condition.Croisement.prototype.addModes = function (value) {
    var array = Fql.toCleanArray(value);
    for(let token of array) {
        this.modeArray.push(token);
    }
    return this;
};

Fql.Condition.Croisement.prototype.setWeightRangeCondition = function (obj) {
    if (!obj) {
        this.weightRangeCondition = null;
    } else if (obj instanceof Fql.Condition.Range) {
        this.weightRangeCondition = obj;
    } else {
        this.weightRangeCondition = new Fql.Condition.Range(obj);
    }
    return this;
};

Fql.Condition.Croisement.prototype.isEmpty = function () {
    return ((this.modeArray.length === 0) && (!this.weightRangeCondition));
};

Fql.Condition.Croisement.prototype.isWeightExclude = function () {
    if (this.weightRangeCondition) {
        return this.weightRangeCondition.exclude;
    } else {
        return false;
    }
}

Fql.Condition.Croisement.prototype.modeToText = function (separator, endWithSeparator) {
    var result = this.modeArray.join(separator);
    if ((endWithSeparator) && (result.length > 0)) {
        result += separator;
    }
    return result;
};

Fql.Condition.Croisement.prototype.weightToText = function (withExclude) {
    if (this.weightRangeCondition) {
        return this.weightRangeCondition.toText(withExclude);
    } else {
        return "";
    }
};

Fql.Condition.Croisement.prototype.toText = function () {
    var text = "";
    var modeText = this.modeToText(";", false);
    var weightText = this.weightToText(true);
    if (modeText) {
        text += modeText;
        if (weightText) {
            text += " / ";
        }
    }
    text += weightText;
    return text;
};

Fql.Condition.Croisement.prototype.toXml = function (indent) {
    if ((!indent) && (indent !== 0)) {
        indent = -999;
    }
    var xml = '';
    for(let mode of this.modeArray) {
        _indent();
        xml += '<mode>' + Fql.escape(mode) + '</mode>';
    }
    if ((this.weightRangeCondition) && (this.weightRangeCondition.toXml)) {
        xml += this.weightRangeCondition.toXml("weight", indent);
    }
    return xml;
    
    
    function _indent() {
        if (indent >= 0) {
            xml += '\n';
            for(let i = 0; i < indent; i++) {
                xml += '\t';
            }
        }
    }
    
};
