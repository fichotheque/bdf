/* global Fql */

Fql.FicheQuery = function (data) {
    this.discardFilter = Fql.DISCARDFILTER_ALL;
    this.withGeoloc  = false;
    this.corpusCondition = new Fql.Condition.Subset();
    this.fieldContentCondition = null;
    this.motcleCondition = null;
    this.ficheCondition = null;
    this.periodCondition = null;
    this.rangeCondition = null;
    this.userCondition = null;
    this.userFilter = Fql.USERFILTER_ALL;
    this.withSatellite = false;
    this.croisementCondition = null;
    this.custom = {};
    if (data) {
        for(let key in data) {
            let value = data[key];
            switch(key)  {
                case "content":
                    this.setFieldContentCondition(value);
                    break;
                case "corpus":
                    this.setCorpusCondition(value);
                    break;
                case "croisement":
                    this.setCroisementCondition(value);
                    break;
                case "discard":
                    this.setDiscardFilter(value);
                    break;
                case "fiche":
                    this.setFicheCondition(value);
                    break;
                case "geoloc":
                    this.withGeoloc = value;
                    break;
                case "mode":
                    this.setCroisementCondition({
                        modes: value
                    });
                    break;
                case "motcle":
                    this.setMotcleCondition(value);
                    break;
                case "period":
                    this.setPeriodCondition(value);
                    break;
                case "id":
                case "range":
                    this.setRangeCondition(value);
                    break;
                case "satellite":
                    this.withSatellite= value;
                    break;
                case "users":
                    this.setUserCondition(value);
                    break;
                default:
                    this.custom[key] = value;
            }
        }
    }
};

Fql.FicheQuery.prototype.setCorpusCondition = function (obj) {
    if (!obj) {
        this.corpusCondition = new Fql.Condition.Subset();
    } else if (obj instanceof Fql.Condition.Subset) {
        this.corpusCondition = obj;
    } else {
        this.corpusCondition = new Fql.Condition.Subset(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.setFieldContentCondition = function (obj) {
    if (!obj) {
        this.fieldContentCondition = null;
    } else if (obj instanceof Fql.Condition.FieldContent) {
        this.fieldContentCondition = obj;
    } else {
        this.fieldContentCondition = new Fql.Condition.FieldContent(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.setMotcleCondition = function (obj) {
    if (!obj) {
        this.motcleCondition = null;
    } else if (obj instanceof Fql.Condition.Motcle) {
        this.motcleCondition = obj;
    } else {
        this.motcleCondition = new Fql.Condition.Motcle(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.setFicheCondition = function (obj) {
    if (!obj) {
        this.ficheCondition = null;
    } else if (obj instanceof Fql.Condition.Fiche) {
        this.ficheCondition = obj;
    } else {
        this.ficheCondition = new Fql.Condition.Fiche(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.addMotcleQueries = function (motcleQueries) {
    if (!this.motcleCondition) {
        this.motcleCondition = new Fql.Condition.Motcle();
    }
    this.motcleCondition.addMotcleQueries(motcleQueries);
    return this;
};

Fql.FicheQuery.prototype.addFicheQueries = function (ficheQueries) {
    if (!this.ficheCondition) {
        this.ficheCondition = new Fql.Condition.Fiche();
    }
    this.ficheCondition.addFicheQueries(ficheQueries);
    return this;
};

Fql.FicheQuery.prototype.setPeriodCondition = function (obj) {
    if (!obj) {
        this.periodCondition = null;
    } else if (obj instanceof Fql.Condition.Period) {
        this.periodCondition = obj;
    } else {
        this.periodCondition = new Fql.Condition.Period(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.setUserCondition = function (obj) {
    if (!obj) {
        this.userCondition = null;
    } else if (obj instanceof Fql.Condition.User) {
        this.userCondition = obj;
    } else {
        this.userCondition = new Fql.Condition.User(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.setCroisementCondition = function (obj) {
    if (!obj) {
        this.croisementCondition = null;
    } else if (obj instanceof Fql.Condition.Croisement) {
        this.croisementCondition = obj;
    } else {
        this.croisementCondition = new Fql.Condition.Croisement(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.setRangeCondition = function (obj) {
    if (!obj) {
        this.rangeCondition = null;
    } else if (obj instanceof Fql.Condition.Range) {
        this.rangeCondition = obj;
    } else {
        this.rangeCondition = new Fql.Condition.Range(obj);
    }
    return this;
};

Fql.FicheQuery.prototype.setDiscardFilter = function (value) {
    switch(value) {
        case Fql.DISCARDFILTER_ONLY:
        case Fql.DISCARDFILTER_NONE:
            this.discardFilter = value;
            break;
        default:
            this.discardFilter = Fql.DISCARDFILTER_ALL;
    }
    return this;
};

Fql.FicheQuery.prototype.toXml = function (indent) {
    if ((!indent) && (indent !== 0)) {
        indent = -999;
    }
    var xml = '';
    _indent();
    if (indent === -1) {
        indent = 0;
    }
    xml += '<fiche-query>';
    indent++;
    if (this.withSatellite) {
         _indent();
         xml += '<satellite/>';
    }
    if ((this.croisementCondition) && (this.croisementCondition.toXml)) {
        xml += this.croisementCondition.toXml(indent);
    }
    if ((this.corpusCondition) && (this.corpusCondition.toXml)) {
        xml += this.corpusCondition.toXml("corpus", indent);
    }
    if ((this.rangeCondition) && (this.rangeCondition.toXml)) {
        xml += this.rangeCondition.toXml("range", indent);
    }
    if (this.discardFilter !== Fql.DISCARDFILTER_ALL) {
        _indent();
        xml += '<discard filter="' + Fql.escape(this.discardFilter) + '"/>';
    }
    if ((this.periodCondition) && (this.periodCondition.toXml)) {
        xml += this.periodCondition.toXml(indent);
    }
    if ((this.fieldContentCondition) && (this.fieldContentCondition.toXml)) {
        xml += this.fieldContentCondition.toXml(indent);
    }
    if ((this.userCondition) && (this.userCondition.toXml)) {
        xml += this.userCondition.toXml(indent);
    }
    if (this.withGeoloc) {
         _indent();
         xml += '<geoloc/>';
    }
    if ((this.motcleCondition) && (this.motcleCondition.toXml)) {
        xml += this.motcleCondition.toXml(indent);
    }
    if ((this.ficheCondition) && (this.ficheCondition.toXml)) {
        xml += this.ficheCondition.toXml(indent);
    }
    indent--;
    _indent();
    xml += '</fiche-query>';
    return xml;
    
    
    function _indent() {
        if (indent >= 0) {
            xml += '\n';
            for(let i = 0; i < indent; i++) {
                xml += '\t';
            }
        }
    }
    
};
