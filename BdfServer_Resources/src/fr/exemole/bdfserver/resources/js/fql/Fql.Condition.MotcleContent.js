/* global Fql */

Fql.Condition.MotcleContent = function (data) {
    this.q = "*";
    this.operator = Fql.LOGICALOPERATOR_AND;
    this.scope = Fql.SCOPE_IDALPHA_ONLY;
    this.qType = Fql.QTYPE_DEFAULT;
    this.custom = {};
    if (data) {
        if (typeof data === "string") {
            this.q = data;
        } else {
            for(var key in data) {
                let value = data[key];
                switch(key) {
                    case "scope" :
                        switch(value) {
                            case Fql.SCOPE_IDALPHA_ONLY:
                            case Fql.SCOPE_IDALPHA_WITH:
                            case Fql.SCOPE_IDALPHA_WITHOUT:
                                this.scope = value;
                                break;
                        }
                        break;
                    case "operator":
                        this.operator = value;
                        break;
                    case "q":
                        this.q = value;
                        break;
                    case "type":
                        this.qType = value;
                        break;
                    default:
                        this.custom[key] = value;
                }
            }
        }
    }
};

Fql.Condition.MotcleContent.prototype.toText = function () {
    var text = "";
    text +=  this.q;
    return text;
};

Fql.Condition.MotcleContent.prototype.toXml = function (indent) {
    if ((!indent) && (indent !== 0)) {
        indent = -999;
    }
    var xml = '';
    _indent();
    xml += '<content';
    if (this.scope !== Fql.SCOPE_IDALPHA_ONLY) {
        xml += ' scope="' + Fql.escape(this.scope) + '"';
    }
    xml += ' q="' + Fql.escape(this.q) + '"';
    xml += ' operator="' + Fql.escape(this.operator) + '"';
    if (this.qType !== Fql.QTYPE_DEFAULT) {
         xml += ' q-type="' + Fql.escape(this.qType) + '"';
    }
    xml += '>';
    indent++;
    indent--;
    _indent();
    xml += '</content>';
    return xml;
    
    
    function _indent() {
        if (indent >= 0) {
            xml += '\n';
            for(let i = 0; i < indent; i++) {
                xml += '\t';
            }
        }
    }
    
};
