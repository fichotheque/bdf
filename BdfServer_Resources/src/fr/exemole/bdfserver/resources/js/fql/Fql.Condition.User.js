/* global Fql */

Fql.Condition.User = function (data) {
    this.filter = Fql.USERFILTER_SOME;
    this.userArray = new Array();
    if (data) {
        if (typeof data === "string") {
            let value = data.trim();
            switch(value) {
               case Fql.USERFILTER_NONE:
               case "!*":
                   this.filter = Fql.USERFILTER_NONE;
                   break;
               case Fql.USERFILTER_ANY:
               case "*":
                   this.filter = Fql.USERFILTER_ANY;
                   break;
               default:
                   this.addUsers(value);
           }
        } else if (Array.isArray(data)) {
            this.addUsers(data);
        }
    }
};

Fql.Condition.User.SPHERE_REGEX = new RegExp('^\\[([a-z][a-z0-9]*)\\]$');
Fql.Condition.User.GLOBALID_REGEX = new RegExp('^([a-z][a-z0-9]*)/([0-9]+)$');
Fql.Condition.User.BRACKETS_REGEX = new RegExp('^([a-zA-Z][a-zA-Z0-9]*)\\[([a-z]+)\\]$');
Fql.Condition.User.UNDESCORE_REGEX = new RegExp('^([a-z][a-z0-9]*)_([a-zA-Z][a-zA-Z0-9]*)$');

Fql.Condition.User.prototype.addUsers = function (value) {
    var array = Fql.toCleanArray(value);
    for(let token of array) {
        if (typeof token === "string") {
            token = token.trim();
            if (token.length > 0) {
                this.userArray.push(_check(token));
            }
        } else if (typeof token === "object") {
            if (token.hasOwnProperty("sphere")) {
                this.userArray.push(token);
            }
        }
    }
    
    function _check(token) {
        let matchArray = token.match(Fql.Condition.User.SPHERE_REGEX);
        if (matchArray) {
            return {
                sphere: matchArray[1]
            };
        }
        matchArray = token.match(Fql.Condition.User.GLOBALID_REGEX);
        if (matchArray) {
            return {
                sphere: matchArray[1],
                id: matchArray[2]
            };
        }
        matchArray = token.match(Fql.Condition.User.BRACKETS_REGEX);
        if (matchArray) {
            return {
                sphere: matchArray[2],
                login: matchArray[1]
            };
        }
        matchArray = token.match(Fql.Condition.User.UNDESCORE_REGEX);
        if (matchArray) {
            return {
                sphere: matchArray[1],
                login: matchArray[2]
            };
        }
        return token;
    }
};

Fql.Condition.User.prototype.toText = function () {
    var text = "";
    switch(this.filter) {
        case Fql.USERFILTER_NONE:
            text = "!*";
            break;
        case Fql.USERFILTER_ANY:
            text = "*";
            break;
        default:
            for(let user of this.userArray) {
                if (typeof user === "string") {
                    text += Fql.escape(user);
                } else {
                    if (user.hasOwnProperty("login")) {
                        text += user.login + "[" + user.sphere + "]";
                    } else if (user.hasOwnProperty("id")) {
                        text += user.sphere + "/" + user.id;
                    } else {
                        text += "[" + user.sphere + "]";
                    }
                }
                text += "; ";
            }
    }
    return text;
};

Fql.Condition.User.prototype.toXml = function (indent) {
    if ((!indent) && (indent !== 0)) {
        indent = -999;
    }
    var xml = '';
    if (this.filter === Fql.USERFILTER_SOME) {
        if (this.userArray.length > 0) {
            _indent();
            xml += '<users>';
            indent++;
            for(let user of this.userArray) {
                _indent();
                if (typeof user === "string") {
                    xml += '<user>' +  Fql.escape(user) + '</user>';
                } else {
                    if (user.hasOwnProperty("login")) {
                        xml += '<user sphere="' + Fql.escape(user.sphere) + '" login="' + Fql.escape(user.login) + '"/>' ;
                    } else if (user.hasOwnProperty("id")) {
                        xml += '<user sphere="' + Fql.escape(user.sphere) + '" id="' + Fql.escape(user.id) + '"/>' ;
                    } else {
                        xml += '<sphere>' +  user.sphere + '</sphere>';
                    }
                }
            }
            indent--;
            _indent();
            xml += '</users>';
        }
    } else {
        _indent();
        xml += '<users filter="' +  Fql.escape(this.filter) + '"/>';
    }
    return xml;
    
    function _indent() {
        if (indent >= 0) {
            xml += '\n';
            for(let i = 0; i < indent; i++) {
                xml += '\t';
            }
        }
    }
    
};
