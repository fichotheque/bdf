/* global Fql */

Fql.Condition.Status = function (data) {
    this.statusArray = new Array();
    if (data) {
        if (typeof data === "string") {
            this.statusArray = data.split(/[;,]/);
        } else if (Array.isArray(data)) {
            this.statusArray = data;
        }
    }
};

Fql.Condition.Status.prototype.isEmpty = function () {
    return (this.statusArray.length === 0);
};

Fql.Condition.Status.prototype.toText = function () {
    return this.statusArray.join(",");
};

Fql.Condition.Status.prototype.toXml = function (indent) {
    if ((!indent) && (indent !== 0)) {
        indent = -999;
    }
    if (this.isEmpty()) {
        return "";
    }
    var xml = '';
    for(let status of this.statusArray) {
        _indent();
        xml += '<status>';
        xml += Fql.escape(status);
        xml += '</status>';
    }
    return xml;
    
    
    function _indent() {
        if (indent >= 0) {
            xml += '\n';
            for(let i = 0; i < indent; i++) {
                xml += '\t';
            }
        }
    }
    
};