/**
 * Objet global définissant l'espace de nom Fql
 * 
 * @namespace Fql
 */
var Fql = {};

/*
 * Rétrocompatibilité
 */
var FicheQL = Fql;

Fql.LOGICALOPERATOR_AND = 'and';
Fql.LOGICALOPERATOR_OR = 'or';
Fql.DISCARDFILTER_ALL = 'all';
Fql.DISCARDFILTER_NONE = 'none';
Fql.DISCARDFILTER_ONLY = 'only';
Fql.DATE_ANY = '*';
Fql.DATE_SAME = 'same';
Fql.USERFILTER_NONE = 'none';
Fql.USERFILTER_ANY = 'any';
Fql.USERFILTER_SOME = 'some';
Fql.SCOPE_TITRE = 'titre';
Fql.SCOPE_ENTETE = 'entete';
Fql.SCOPE_FICHE = 'fiche';
Fql.SCOPE_SELECTION = 'selection';
Fql.SCOPE_IDALPHA_ONLY = "idalpha_only";
Fql.SCOPE_IDALPHA_WITH = "idalpha_with";
Fql.SCOPE_IDALPHA_WITHOUT = "idalpha_without";
Fql.QTYPE_DEFAULT = '';
Fql.QTYPE_SIMPLE = 'simple';

Fql.escape = function (text) {
    text = String(text);
    var result = '';
    for(let i = 0; i < text.length; i++) {
        let carac = text.charAt(i);
        switch (carac) {
            case '&':
                result += '&amp;';
                break;
            case '"':
                result += '&quot;';
                break;
            case '<':
                result += '&lt;';
                break;
            case '>':
                result += '&gt;';
                break;
            case '\'':
                result += '&#x27;';
                break;
            default:
                result += carac;
        }
    }
    return result;
};

Fql.toCleanArray = function (value) {
    var result = new Array();
    var array;
    if (Array.isArray(value)) {
       array = value;
    } else {
        array = value.toString().split(/[;\s]/);
    }
    for(let val of array) {
        if (typeof val === "string") {
            val = val.trim();
            if (val.length > 0) {
                result.push(val);
            }
        } else {
            result.push(val);
        }
    }
    return result;
};
