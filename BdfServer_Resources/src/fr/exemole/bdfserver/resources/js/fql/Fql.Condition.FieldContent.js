/* global Fql */

Fql.Condition.FieldContent = function (data) {
    this.fieldArray = new Array();
    this.q = "*";
    this.operator = Fql.LOGICALOPERATOR_AND;
    this.scope = Fql.SCOPE_TITRE;
    this.qType = Fql.QTYPE_DEFAULT;
    this.custom = {};
    if (data) {
        for(var key in data) {
            let value = data[key];
            switch(key) {
                case "scope" :
                    switch(value) {
                        case Fql.SCOPE_TITRE:
                        case Fql.SCOPE_ENTETE:
                        case Fql.SCOPE_FICHE:
                            this.scope = value;
                            break;
                        default:
                            this.scope = Fql.SCOPE_SELECTION;
                            this.fieldArray = Fql.toCleanArray(value);
                    }
                    break;
                case "operator":
                case "mode":
                    this.operator = value;
                    break;
                case "q":
                    this.q = value;
                    break;
                case "type":
                    this.qType = value;
                    break;
                default:
                    this.custom[key] = value;
            }
        }
    }
};

Fql.Condition.FieldContent.prototype.toText = function () {
    var text = "";
    text +=  this.q;
    return text;
};

Fql.Condition.FieldContent.prototype.toXml = function (indent) {
    if ((!indent) && (indent !== 0)) {
        indent = -999;
    }
    var xml = '';
    _indent();
    xml += '<content';
    xml += ' scope="' + Fql.escape(this.scope) + '"';
    xml += ' q="' + Fql.escape(this.q) + '"';
    xml += ' operator="' + Fql.escape(this.operator) + '"';
    if (this.qType !== Fql.QTYPE_DEFAULT) {
         xml += ' q-type="' + Fql.escape(this.qType) + '"';
    }
    xml += '>';
    indent++;
    if (this.scope === Fql.SCOPE_SELECTION) {
        for(let field of this.fieldArray) {
            _indent();
            xml += '<field>' + Fql.escape(field) + '</field>';
        }
    }
    indent--;
    _indent();
    xml += '</content>';
    return xml;
    
    
    function _indent() {
        if (indent >= 0) {
            xml += '\n';
            for(let i = 0; i < indent; i++) {
                xml += '\t';
            }
        }
    }
    
};