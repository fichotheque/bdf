/* global Fql */

Fql.MotcleQuery = function (data) {
    this.ficheCondition = null;
    this.thesaurusCondition = new Fql.Condition.Subset();
    this.contentCondition = null;
    this.withMaster = false;
    this.croisementCondition = null;
    this.idRangeCondition = null;
    this.levelRangeCondition = null;
    this.statusCondition = null;
    this.custom = {};
    if (data) {
        for(let key in data) {
            let value = data[key];
            switch(key) {
                case "content":
                    this.setContentCondition(value);
                    break;
                case "croisement":
                    this.setCroisementCondition(value);
                    break;
                case "fiche":
                    this.setFicheCondition(value);
                    break;
                case "id":
                case "range":
                    this.setIdRangeCondition(value);
                    break;
                case "idalpha":
                    this.setContentCondition({
                        scope: Fql.SCOPE_IDALPHA_ONLY,
                        q: value,
                        type: Fql.QTYPE_DEFAULT
                    });
                    break;
                case "level":
                    this.setLevelRangeCondition(value);
                    break;
                case "master":
                    this.withMaster = value;
                    break;
                case "mode":
                    this.setCroisementCondition({
                        modes: value
                    });
                    break;
                case "thesaurus":
                    this.setThesaurusCondition(value);
                    break;
                case "status":
                    this.setStatusCondition(value);
                    break;
                default:
                    this.custom[key] = value;
            }
        }
    }
};

Fql.MotcleQuery.prototype.setContentCondition = function (obj) {
    if (!obj) {
        this.contentCondition = null;
    } else if (obj instanceof Fql.Condition.MotcleContent) {
        this.contentCondition = obj;
    } else {
        this.contentCondition = new Fql.Condition.MotcleContent(obj);
    }
    return this;
};

Fql.MotcleQuery.prototype.setFicheCondition = function (obj) {
    if (!obj) {
        this.ficheCondition = null;
    } else if (obj instanceof Fql.Condition.Fiche) {
        this.ficheCondition = obj;
    } else {
        this.ficheCondition = new Fql.Condition.Fiche(obj);
    }
    return this;
};

Fql.MotcleQuery.prototype.setThesaurusCondition = function (obj) {
    
    if (!obj) {
        this.thesaurusCondition = new Fql.Condition.Subset();
    } else if (obj instanceof Fql.Condition.Subset) {
        this.thesaurusCondition = obj;
    } else {
        this.thesaurusCondition = new Fql.Condition.Subset(obj);
    }
    return this;
};

Fql.MotcleQuery.prototype.addFicheQueries = function (ficheQueries) {
    if (!this.ficheCondition) {
        this.ficheCondition = new Fql.Condition.Fiche();
    }
    this.ficheCondition.addFicheQueries(ficheQueries);
    return this;
};

Fql.MotcleQuery.prototype.setIdRangeCondition = function (obj) {
    if (!obj) {
        this.idRangeCondition = null;
    } else if (obj instanceof Fql.Condition.Range) {
        this.idRangeCondition = obj;
    } else {
        this.idRangeCondition = new Fql.Condition.Range(obj);
    }
    return this;
};

Fql.MotcleQuery.prototype.setLevelRangeCondition = function (obj) {
    if (!obj) {
        this.levelRangeCondition = null;
    } else if (obj instanceof Fql.Condition.Range) {
        this.levelRangeCondition = obj;
    } else {
        this.levelRangeCondition = new Fql.Condition.Range(obj);
    }
    return this;
};

Fql.MotcleQuery.prototype.setStatusCondition = function (obj) {
    if (!obj) {
        this.statusCondition = null;
    } else if (obj instanceof Fql.Condition.Status) {
        this.statusCondition = obj;
    } else {
        this.statusCondition = new Fql.Condition.Status(obj);
    }
    return this;
};

Fql.MotcleQuery.prototype.setCroisementCondition = function (obj) {
    if (obj instanceof Fql.Condition.Croisement) {
        this.croisementCondition = obj;
    } else {
        this.croisementCondition = new Fql.Condition.Croisement(obj);
    }
};

Fql.MotcleQuery.prototype.toXml = function (indent) {
    if ((!indent) && (indent !== 0)) {
        indent = -999;
    }
    var xml = '';
    _indent();
    if (indent === -1) {
        indent = 0;
    }
    xml += '<motcle-query>';
    indent++;
    if (this.withMaster) {
         _indent();
         xml += '<master/>';
    }
    if ((this.croisementCondition) && (this.croisementCondition.toXml)) {
        xml += this.croisementCondition.toXml(indent);
    }
    if ((this.thesaurusCondition) && (this.thesaurusCondition.toXml)) {
        xml += this.thesaurusCondition.toXml("thesaurus", indent);
    }
    if ((this.idRangeCondition) && (this.idRangeCondition.toXml)) {
        xml += this.idRangeCondition.toXml("range", indent);
    }
    if ((this.levelRangeCondition) && (this.levelRangeCondition.toXml)) {
        xml += this.levelRangeCondition.toXml("level", indent);
    }
    if ((this.statusCondition) && (this.statusCondition.toXml)) {
        xml += this.statusCondition.toXml(indent);
    }
    if ((this.contentCondition) && (this.contentCondition.toXml)) {
        xml += this.contentCondition.toXml(indent);
    }
    if ((this.ficheCondition) && (this.ficheCondition.toXml)) {
        xml += this.ficheCondition.toXml(indent);
    }
    indent--;
    _indent();
    xml += '</motcle-query>';
    return xml;
    
    
    function _indent() {
        if (indent >= 0) {
            xml += '\n';
            for(let i = 0; i < indent; i++) {
                xml += '\t';
            }
        }
    }
};
