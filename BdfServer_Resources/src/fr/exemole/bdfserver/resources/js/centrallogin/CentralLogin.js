/* global Bdf,$$,Overlay */
/**
 * Objet global définissant l'espace de nom Multi
 * 
 * @namespace CentralLogin
 */
var CentralLogin = {};

CentralLogin.ARGS = {
  sphereName: "",
  title: ""
};

CentralLogin.init = function (args) {
    var sphereName = args.sphereName;
    var pingURL = "index-" + sphereName;
   
    _showLogin();

    function _showLogin() {
        var genId = Bdf.generateId();
        Overlay.start({
            header: args.title,
            content: Bdf.render("centrallogin:overlay-authentification", {
                genId: genId,
                sphereName: sphereName
            }),
            footer: Bdf.render("bdf:overlay-footer", {
                submit: {
                    locKey: "_ submit.session.login"
                }}),
            escapeClose: false,
            clickClose: false,
            showClose: false,
            formAttrs: {
                action: pingURL,
                method: "POST"
            },
            ajaxForm: {
                dataType: "json",
                data: {
                    json: "ping"
                },
                success: function (data, textStatus, jqXHR, $form) {
                    if (data.authentified) {
                        Overlay.end($form, function () {
                             window.location.reload();
                         });
                    } else {
                        Bdf.checkError(data);
                    }
                }
            }
        });
    }

};


$(function(){
    Bdf.initTemplates();
    CentralLogin.init(CentralLogin.ARGS);
});

