/* global Bdf,$$ */
/**
 * Objet global définissant l'espace de nom Memento
 * 
 * @namespace Memento
 */
var Memento = {};

Memento.init = function (mementoUnitInfoArray) {
    var localStorage = Bdf.getMainLocalStorage();
    if (!localStorage) {
        localStorage = new Object();
    }
    var unitMap = {};
    var $body = $("body");
    for(let mementoUnit of mementoUnitInfoArray) {
        let path = mementoUnit.path;
        if (!localStorage.hasOwnProperty(path)) {
            let newObject = new Object();
            newObject.nodes = {};
            newObject.open = false;
            localStorage[path] = newObject;
            mementoUnit.localStorage  = newObject;
        } else {
            let unitLocalStorage = localStorage[path];
            if (!unitLocalStorage.nodes) {
                unitLocalStorage.nodes = {};
            }
            mementoUnit.open = unitLocalStorage.open;
            mementoUnit.localStorage = unitLocalStorage;
        }
        mementoUnit.subnodeArray = false;
        mementoUnit.onLoad = false;
        unitMap[path] = mementoUnit;
    }
    $body.append(Bdf.render("memento:unit", {mementoUnitInfoArray: mementoUnitInfoArray}));
    for(let mementoUnit of mementoUnitInfoArray) {
        if (mementoUnit.open) {
            _loadSubnodeArray(mementoUnit, $$($body, {role:"unit", path: mementoUnit.path}));
        }
    }
    $$($body, {role:"unit"}).on("toggle", function () {
        let unitElement = this;
        let path = unitElement.dataset.path;
        let unit = unitMap[path];
        if (!unit.subnodeArray) {
            _loadSubnodeArray(unit, unitElement);
        }
        localStorage[path].open = this.open;
        Bdf.putMainLocalStorage(localStorage);
    });
    
    
    function _loadSubnodeArray(unit, unitElement) {
        if (unit.onLoad) {
            return;
        }
        unit.onLoad = true;
        Bdf.Ajax.loadMementoUnit(unit.path, function (mementoUnit) {
            unit.subnodeArray = mementoUnit.subnodeArray;
            __completeNodes(unit.subnodeArray);
            $$(unitElement, {role:"tree"}).html(Bdf.render("memento:node", unit.subnodeArray));
            $$(unitElement, {_element:"details"}).on("toggle", function () {
                unit.localStorage.nodes[this.dataset.name] = this.open;
                Bdf.putMainLocalStorage(localStorage);
            });
        });
        
        
        function __completeNodes(nodeArray) {
            for(let node of nodeArray) {
                node.generatedId = Bdf.generateId();
                if (!node.leaf) {
                    __completeNodes(node.subnodeArray);
                }
                let open = false;
                if (unit.localStorage.nodes.hasOwnProperty(node.name)) {
                    if (unit.localStorage.nodes[node.name]) {
                        open = true;
                    }
                }
                node.open = open;
            }
        }

    }

};


/******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Bdf.Ajax.loadMementoUnitInfoArray(function (mementoUnitInfoArray) {
        Memento.init(mementoUnitInfoArray);
    });
});