/* global Bdf,Role,Overlay,Pane */

Role.Overlay = {};

/******************************************************************************
* Appel des différents formulaires
******************************************************************************/
Role.Overlay.showCreationForm = function () {
    if (!Bdf.confirmUnload()) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ ROL-01"),
        content: Bdf.render("role:overlay/creationform", {
            commandKey: "ROL-01",
            commandUrl: Bdf.getCommandUrl("ROL-01")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.administration.rolecreation'
            }}),
        formAttrs: {
            action: Bdf.URL + "administration",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Pane.clearMain();
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        Role.insertRole(data.role);
                    });
                }
            }
        }
    });
};

Role.Overlay.showRemoveConfirm = function (roleName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var role = Role.getRole(roleName);
    if (!role) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ ROL-03"),
        content: Bdf.render("role:overlay/removeconfirm", {
            name: roleName,
            title: role.title,
            commandKey: "ROL-03",
            commandUrl: Bdf.getCommandUrl("ROL-03")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.administration.roleremove'
            }}),
        formAttrs: {
            action: Bdf.URL + "administration",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        Role.removeRole(roleName);
                    });
                }
            }
        }
    });
};