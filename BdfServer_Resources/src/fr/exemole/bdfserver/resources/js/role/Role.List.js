/* global Bdf,$$,Role,Pane */

Role.List = {};

Role.List.init = function (callback) {
    Bdf.Ajax.loadRoleArray(function (roleArray) {
        roleArray = Role.initArray(roleArray);
        let $listBody = $$("listpane_body");
        $listBody.html(Bdf.render("role:list/role", roleArray));
        Role.List.initActions();
        Bdf.Deploy.init($listBody);
        if (callback) {
            callback();
        }
    });
};

Role.List.initActions = function () {
    $$("listpane_button_newrole").click(Role.Overlay.showCreationForm);
    var $body = $$("listpane_body");
    _onClick("metadata", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let name = Pane.getEntryName(this);
        Pane.loadOrDestock(name, function () {
            Role.Form.load(name);
        });
    });
    _onClick("roleremove", function () {
        Role.Overlay.showRemoveConfirm(Pane.getEntryName(this));
    });
    
    function _onClick(actionName, clickFunction) {
        $body.on("click", $$.toCssSelector({action: actionName}), clickFunction);
    }
};

Role.List.updateRole = function (newRole) {
    var $currentRole =  $$({role: "entry", name: newRole.name});
    $$($currentRole, {role: "entry-title"}).html(newRole.title);
};