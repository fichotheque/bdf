/* global Bdf,$$ */
/**
 * Objet global définissant l'espace de nom Dashboard
 * 
 * @namespace Dashboard
 */

var Dashboard = {};

Dashboard.init = function (options) {
     /*
     * Include loc keys: "_ @phrases@"
     */
    options = Object.assign({
        titleKey: "fichotheque",
        mainHtml: "",
        navHtml: "",
        toolbarHtml: "",
        withRefresh: false,
        withNav: true
    }, options);
    $$("dashboard").html(Bdf.render("dashboard:layout", {
        withRefresh: options.withRefresh,
        withNav: options.withNav
    }));
    if (options.withNav) {
        $$("dashboard_header_burger").click(function () {
            let $navPanel = $$("dashboard_body_nav");
            let isHidden = $navPanel.hasClass("hidden");
            if (isHidden) {
                $navPanel.removeClass("hidden");
                $$("dashboard_header_toolbar").removeClass("hidden_small");
            } else  {
                $navPanel.addClass("hidden");
                $$("dashboard_header_toolbar").addClass("hidden_small");
            }
        });
    }
    if ((options.withRefresh) && (options.refreshFunction)) {
        $$("dashboard_header_session_refresh").click(options.refreshFunction);
    }
    $$("dashboard_header_title").text(Bdf.Loc.get(options.titleKey));
    if (options.mainHtml) {
            Dashboard.updateMain(options.mainHtml);
    }
    if (options.navHtml) {
            Dashboard.updateNav(options.navHtml);
    }
    if (options.toolbarHtml) {
            Dashboard.updateToolbar(options.toolbarHtml);
    }
};

Dashboard.updateMain = function (html) {
    $$("dashboard_body_main").html(html);
};

Dashboard.updateNav= function (html) {
    $$("dashboard_body_nav").html(html);
};

Dashboard.updateToolbar = function (html) {
    $$("dashboard_header_toolbar").html(html);
};

