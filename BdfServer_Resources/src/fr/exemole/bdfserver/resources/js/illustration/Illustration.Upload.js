/* global Bdf,$$,Illustration */

Illustration.Upload = {};

Illustration.Upload.ARGS = {
    clientId: "",
    appelant: "",
    newUpload: false,
    album: "",
    errorPage: "",
    resultPage: "",
    errorMessage: null
};

Illustration.Upload.init = function (args) {
    var genId = Bdf.generateId();
    var isWindow = false;
    if (window.opener) {
        isWindow = true;
    }
    $$(args.clientId).html(Bdf.render("illustration:client-upload", {
        args: args,
        genId: genId,
        isWindow: isWindow,
        titleKey: (args.newUpload)?"_ title.album.newupload":"_ title.album.replaceupload"
    }));
    if (isWindow) {
        $$(genId, "cancel").click(function () {
            window.close();
        });
    }
};

$(function () {
    Bdf.initTemplates();
    Illustration.Upload.init(Illustration.Upload.ARGS);
});
