/* global Bdf,$$,Illustration */

Illustration.UploadConfirm = {};

Illustration.UploadConfirm.ARGS = {
    clientId: "",
    appelant: "",
    tmpFileName: "",
    imagePath: ""
};

Illustration.UploadConfirm.init = function (args) {
    var genId = Bdf.generateId();
    $$(args.clientId).html(Bdf.render("illustration:client-uploadconfirm", {
        args: args,
        genId: genId
    }));
    $$(genId, "cancel").click(function () {
        _close(false);
    });
    $$(genId, "confirm").click(function () {
        _close(true);
    });
    
    
    function _close(validate) {
        let tmpFileName;
        if (validate) {
            tmpFileName = args.tmpFileName;
        }
        let appelantWindow;
        if (window.opener) {
            appelantWindow = window.opener;
        } else {
            appelantWindow = window.parent;
        }
        let callback = appelantWindow.Bdf.getAppelantCallback(args.appelant);
        if (callback) {
            callback(tmpFileName);
        }
        if (window.opener) {
            window.close();
        }
    }
    
};

$(function () {
    Bdf.initTemplates();
    Illustration.UploadConfirm.init(Illustration.UploadConfirm.ARGS);
});