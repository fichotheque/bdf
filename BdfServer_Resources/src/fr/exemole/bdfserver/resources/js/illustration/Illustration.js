/* global Bdf */
/**
 * Objet global définissant l'espace de nom Illustration
 * 
 * @namespace Illustration
 */
var Illustration = {};

Illustration.CREATION_CHANGETYPE = "creation";

Illustration.is_int = function (value){
   for (let i = 0 ; i < value.length ; i++) {
      if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) return false;
    }
   return true;
};
