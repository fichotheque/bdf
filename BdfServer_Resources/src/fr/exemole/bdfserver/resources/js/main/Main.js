/* global Bdf,$$,Ficheframe */
/**
 * Objet global définissant l'espace de nom Main
 * 
 * @namespace Main
 */
var Main = {};

Main.initLayout = function () {
    Bdf.Api.init();
    $$("main_body_tool").html(Bdf.render("main:tool", {}));
    $$("main_body_tool_onoff").click(function () {
        _onoff("main_body_list");
    });
    $$("main_body_tool_decrease").click(function () {
        _resize("main_body_list", -50);
    });
    $$("main_body_tool_increase").click(function () {
        _resize("main_body_list", 50);
    });
    $$("main_body_edition_iframe").on("load", function (event) {
        Ficheframe.testToolbar({
            iframe: this,
            toolbarId: "main_body_edition_toolbar",
            fieldeditId: "main_body_edition_fieldedit",
            editTarget: "Edition",
            tocTarget: "Edition"
        });
        let locationCheck = Bdf.checkIframeLocation(this);
        if (locationCheck.withBorder) {
            this.classList.add("main-WebContent");
        } else {
            this.classList.remove("main-WebContent");
        }
        if (locationCheck.queryPart) {
            if (locationCheck.queryPart.indexOf("bdf-flag=updateCorpusTree") > -1) {
                Main.updateCorpusTree();
            }
            if (locationCheck.queryPart.indexOf("bdf-flag=updateCollections") > -1) {
                Main.updateCollections();
            }
        }
    });
    _initWidth();
    
    
    function _onoff(layoutId) {
        let $layout = $$(layoutId);
        if ($layout.hasClass("hidden")) {
            $layout.removeClass("hidden");
        } else {
            $layout.addClass("hidden");
        }
    }
    
    function _resize(layoutId, widthDiff) {
        let $layout = $$(layoutId);
        let newWidth = $layout.width() + widthDiff;
        if (newWidth > 199) {
            $layout.width(newWidth);
            let localData = Bdf.getMainLocalStorage();
            if (localData) {
                localData[layoutId] = newWidth;
                Bdf.putMainLocalStorage(localData);
            }
        }
    }
    
    function _initWidth() {
        let localData = Bdf.getMainLocalStorage();
        let layoutId = "main_body_list";
        if (localData) {
            if (localData.hasOwnProperty(layoutId)) {
                let width = parseInt(localData[layoutId]);
                if (width > 199) {
                    $$(layoutId).width(width);
                }
            }
        }
    }
};

Main.updateCollections = function () {
    try {
        let listWindow = $$.one("main_body_list_iframe").contentWindow;
        let search = listWindow.location.search;
        if ((search) && (search.indexOf("page=collections") > -1)) {
            listWindow.location.reload();
        } else {
            listWindow.location = "main?page=collections";
        }
     }  catch(error) {
    }
};

Main.updateCorpusTree = function () {
    Bdf.Ajax.loadSubsetTrees("corpus", false, _update);
    
    function _update(subsetTrees) {
        let menuCorpusUpdate = Bdf.getFunction("menu:updateCorpusTree");
        if (menuCorpusUpdate) {
            menuCorpusUpdate(subsetTrees.corpus);
        }
    }
};


/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Main.initLayout();
});
