/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom Balayage
 * 
 * @namespace Balayage
 */
var Balayage = {};

Balayage.array = null;
Balayage.UNITTYPE_ARRAY = [
    {
        value: "unique",
        labelKey: "_ label.exportation.balayageunit_unique"
    },
    {
        value: "corpus",
        labelKey: "_ label.exportation.balayageunit_corpus"
    },
    {
        value: "fiche",
        labelKey: "_ label.exportation.balayageunit_fiche"
    },
    {
        value: "thesaurus",
        labelKey: "_ label.exportation.balayageunit_thesaurus"
    },
    {
        value: "motcle",
        labelKey: "_ label.exportation.balayageunit_motcle"
    },
    {
        value: "document",
        labelKey: "_ label.exportation.balayageunit_document"
    },
    {
        value: "illustration",
        labelKey: "_ label.exportation.balayageunit_illustration"
    }
];

Balayage.FIELD_MAP = {
    extraction: {
        labelKey: "_ label.exportation.extractionfile",
        size: 20,
        attributeName: "extraction"
    },
    extensions: {
        labelKey: "_ link.configuration.extensionsform",
        size: 15,
        tagName: "extension"
    },
    corpus: {
        labelKey: "_ title.global.corpus_collection",
        size: 30,
        tagName: "corpus"
    },
    thesaurus: {
        labelKey: "_ title.global.thesaurus_collection",
        size: 30,
        tagName: "thesaurus"
    },
    addenda: {
        labelKey: "_ title.global.addenda_collection",
        size: 30,
        tagName: "addenda"
    },
    album: {
        labelKey: "_ title.global.album_collection",
        size: 30,
        tagName: "album"
    },
    albumdim: {
        labelKey: "_ label.exportation.albumdim",
        size: 30,
        tagName: "albumdim"
    },
    path: {
        labelKey: "_ label.exportation.path",
        size: 20,
        attributeName: "path"
    },
    pattern: {
        labelKey: "_ label.exportation.filepattern",
        size: 20,
        attributeName: "pattern"
    },
    xslt: {
        labelKey: "_ label.exportation.xsltfile",
        size: 20,
        attributeName: "xslt"
    }
}; 


/******************************************************************************
 * Actions sur Balayage.array
 ******************************************************************************/

Balayage.initArray = function (array) {
    for(var i = 0, len = array.length; i < len; i++) {
        Balayage.completeBalayage(array[i]);
    }
    Balayage.array = array;
    return array;
};

Balayage.getBalayage = function (balayageName) {
    if (!Balayage.array) {
        return null;
    }
    for(let i = 0, len = Balayage.array.length; i < len; i++) {
        let balayage = Balayage.array[i];
        if (balayage.name === balayageName) {
            return balayage;
        }
    }
    return null;
};

Balayage.updateBalayage = function (newBalayage) {
    newBalayage = Balayage.completeBalayage(newBalayage);
    Bdf.updateInArray(Balayage.array, newBalayage);
    Balayage.List.updateBalayage(newBalayage);
    return newBalayage;
};

Balayage.removeBalayage = function (name) {
    Bdf.removeInArray(Balayage.array,name);
    Pane.removeContents(name);
    Pane.removeEntry($$("listpane_body"), name);
};

Balayage.insertBalayage = function (balayage) {
    balayage = Balayage.completeBalayage(balayage);
    Bdf.insertInArray(Balayage.array, balayage);
    var $entry = Pane.insertEntry($$("listpane_body"), balayage.name, Bdf.render("balayage:list/balayage", balayage));
    Bdf.Deploy.ensureLinkVisibility($entry, true);
    $$("listpane_body").animate({
        scrollTop: $entry.offset().top
    }, 1000);
    Balayage.MetadataForm.load(balayage.name);
    return balayage;
};

Balayage.completeBalayage = function (balayageObj) {
    var balayageName = balayageObj.name;
    _completeSubdir("_core");
    _completeSubdir("extraction");
    _completeSubdir("xslt");
    return balayageObj;
    
    function _completeSubdir(key) {
        if (balayageObj.hasOwnProperty(key)) {
            var array = balayageObj[key].array;
            for (var j = 0, len = array.length; j < len; j++) {
                array[j].balayageName = balayageName;
            }
        }
    }
    
};

Balayage.getLangOptions = function (currentValue) {
   var result = new Array();
  _addOption("none");
  _addOption("dir");
  return result;
  
  function _addOption(name) {
      result.push({
          value: name,
          title: name,
          selected: (name === currentValue)
      });
  }
    
};


/******************************************************************************
 * Enregistrement de fonction
 ******************************************************************************/

Pane.registerCheckFunction(function (state) {
    var mainContentKey, balayageName, contentPath;
    $(".pane-list-ActiveItem").removeClass("pane-list-ActiveItem");
    if (state.withMain()) {
        _init(state.mainContentKey);
        let $balayage = $$({role: "entry", name: balayageName});
        $$($balayage, {role: "entry-header"}).addClass("pane-list-ActiveItem");
        let $reference = $$($balayage, {role: "content-reference", contentPath: contentPath});
        $$($reference, {role: "content-title"}).addClass("pane-list-ActiveItem");
        Pane.ensureVisibility($reference);
    }
    
    
    function _init(mainContentKey) {
        let idx = mainContentKey.indexOf("/");
        if (idx > 0) {
            balayageName = mainContentKey.substring(0, idx);
            contentPath = mainContentKey.substring(idx + 1);
        } else {
            balayageName = mainContentKey;
            contentPath = false;
        }
    }
    
});


/******************************************************************************
 * Initialisation
 ******************************************************************************/

Bdf.addTemplateOptions({
    helpers: {
    }
});

$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    $$("layout_list").html(Bdf.render("balayage:list/pane", {}));
    Bdf.initBeforeUnload();
    Bdf.runSerialFunctions(
        Bdf.Ajax.initLangConfiguration,
        Bdf.Ajax.initPathConfiguration,
        Bdf.Collections.initScrutariExportArray,
        Bdf.Collections.initSelectionDefArray,
        Bdf.Collections.initSqlExportArray,
        Bdf.Collections.initExternalScriptArray,
        Balayage.List.init
    );
    Pane.initShortcuts();
});
