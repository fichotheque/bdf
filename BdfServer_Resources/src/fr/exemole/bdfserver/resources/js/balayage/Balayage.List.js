/* global Bdf,Balayage,$$,Pane,Overlay */

Balayage.List = {};

Balayage.List.init = function (callback) {
    Bdf.Ajax.loadBalayageArray(function (balayageArray) {
        balayageArray = Balayage.initArray(balayageArray);
        let $listBody = $$("listpane_body");
        $listBody.html(Bdf.render("balayage:list/balayage", balayageArray));
        Balayage.List.initActions();
        Bdf.Deploy.init($listBody);
        if (callback) {
            callback();
        }
    });
};

Balayage.List.initActions = function () {
    $$("listpane_button_newbalayage").click(Balayage.Overlay.showBalayageCreationForm);
    var $body = $$("listpane_body");
    _onClick("metadata", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let name = Pane.getEntryName(this);
        Pane.loadOrDestock(name, function () {
            Balayage.MetadataForm.load(name);
        });
    });
    _onClick("run", function () {
        Balayage.Overlay.showRun(Pane.getEntryName(this));
    });
    _onClick("balayageremove", function () {
        Balayage.Overlay.showBalayageRemoveConfirm(Pane.getEntryName(this));
    });
    _onClick("newcontent", function () {
        Balayage.Overlay.showContentCreationForm(Pane.getEntryName(this));
    });
    _onClick("load-main", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let reference = _getReferenceAncestor(this);
        Pane.loadOrDestock(reference.dataset.contentKey, function () {
            _loadContent(reference, false);
        });
        return false;
    });
    _onClick("load-stock", function () {
        let reference = _getReferenceAncestor(this);
        let state = Pane.State.build();
        if (state.isLoaded(reference.dataset.contentKey)) {
            return;
        }
        _loadContent(reference, true);
    });
    
    function _onClick(actionName, clickFunction) {
        $body.on("click", $$.toCssSelector({action: actionName}), clickFunction);
    }
    
    function _getReferenceAncestor(element) {
        return  Bdf.getMandatoryAncestor(element, {role: "content-reference"});
    }
    
    
    function _loadContent(reference, isInStock) {
        let overlayId = Bdf.startRunningOverlay();
        Bdf.Ajax.loadBalayageContent(reference.dataset.balayageName, reference.dataset.contentPath, function (balayageContent) {
            Balayage.ContentForm.load(balayageContent, isInStock);
            Overlay.end(overlayId);
        });
    }

};

Balayage.List.updateBalayage = function (newBalayageObj) {
    var $currentBalayage = $$({role: "entry", name: newBalayageObj.name});
    $$($currentBalayage, {role: "entry-title"}).html(newBalayageObj.title);
    Pane.updateStateClass($currentBalayage, "balayage-state-" + newBalayageObj.state);
    _checkList("_core");
    _checkList("extraction");
    _checkList("xslt");


    function _checkList(subdirName) {
        let $subdirBlock = $$($currentBalayage, {role: "subdir-block", subdirName: subdirName});
        Pane.updateStateClass($$($subdirBlock, {role: "subdir-name"}), "balayage-state-" + newBalayageObj[subdirName].state);
        let $subdirUl = $$($currentBalayage, {role: "content-list", subdirName: subdirName});
        Pane.updateContentList("balayage", $subdirUl, newBalayageObj[subdirName].array);
        if ($subdirUl.children("li").length > 0) {
            $$($subdirBlock, {role: "subdir-name"}).removeClass("balayage-Empty");
        } else {
            $$($subdirBlock, {role: "subdir-name"}).addClass("balayage-Empty");
        }
    }

};