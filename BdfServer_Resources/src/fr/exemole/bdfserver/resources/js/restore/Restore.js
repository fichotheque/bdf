/* global Bdf,$$ */
/**
 * Objet global définissant l'espace de nom Restore
 * Suppose l'existence d'un objet ARGS avec les paramètres
 * 
 * @namespace History
 */
var Restore = {};

Restore.ARGS = {
    clientId: "",
    subsetKey: "",
    subsetCategory: "",
    subsetName: ""
};

Restore.init = function (args) {
    var removedListId = Bdf.generateId();
    var $client = $$(args.clientId);
    $client.html(Bdf.render("restore:client", {
        removedListId: removedListId,
        args: args
    }));
    Restore.loadRemovedList(Restore.ARGS.subsetName, function(lastRevisions) {
        if (lastRevisions.array.length === 0) {
            $$(removedListId).html(Bdf.render("restore:empty", {}));
        } else {
            for(let fiche of lastRevisions.array) {
                Restore.completeHistory(fiche);
            }
            lastRevisions.array.sort(Restore.compareHistory);
            $$(removedListId).html(Bdf.render("restore:table", lastRevisions));
            $$($client, {action: "croisement"}).click(function () {
                _loadCroisement(this.dataset.id);
            });
        }
    });
    
    
    
    function _loadCroisement(id) {
        var thesaurusName = $$($client, {role: "thesaurus", id: id}).val();
        Bdf.log(thesaurusName);
        if (thesaurusName.length > 0) {
            Restore.loadCroisementHistoryList(args.subsetName, id, thesaurusName, function (croisementHistoryArray) {
                for(let fiche of croisementHistoryArray) {
                    Restore.completeHistory(fiche);
                }
                croisementHistoryArray.sort(Restore.compareHistory);
                $$("croisement").html(Bdf.render("restore:croisements", {
                    croisementHistoryArray: croisementHistoryArray
                }));
            });
        }
    }
};

Restore.loadRemovedList = function (corpus, callback) {
    $.ajax({
        url: Bdf.URL + "corpus",
        data: {
           json: "removedlist",
           corpus: corpus
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "ficheHistoryArray", callback);
        }
    });
};

Restore.loadCroisementHistoryList = function (corpus, id, thesaurus, callback) {
    $.ajax({
        url: Bdf.URL + "corpus",
        data: {
           json: "croisementhistorylist",
           corpus: corpus,
           id: id,
           thesaurus: thesaurus
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "croisementHistoryArray", callback);
        }
    });
};

Restore.completeHistory = function (historyEntry) {
    historyEntry.lastEdit = historyEntry.lastRevision.editArray[0];
};

Restore.compareHistory = function(fiche1, fiche2) {
    let time1 = fiche1.lastEdit.time;
    let time2 = fiche2.lastEdit.time;
    if (time1 > time2) {
        return -1;
    } else if (time1 < time2) {
        return 1;
    } else {
        return (fiche2.id - fiche1.id);
    }
};

Restore.loadRemovedList = function (corpus, callback) {
    $.ajax({
        url: Bdf.URL + "corpus",
        data: {
           json: "removedlist",
           corpus: corpus
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "lastRevisions", callback);
        }
    });
};

Bdf.addTemplateOptions({
    converters: {
        localDate:  function (time) {
            if (!time) {
                return "";
            }
            var date = new Date(time);
            var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
            return date.toLocaleDateString(window.navigator.language, options);
        },
        localTime: function (time) {
            if (!time) {
                return "";
            }
            var date = new Date(time);
            return date.toLocaleTimeString();
        }
    }
});

$(function () {
    Bdf.initTemplates();
    Restore.init(Restore.ARGS);
});