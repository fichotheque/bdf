/* global Bdf,$$, Ficheframe */

Ficheframe.Info = function (corpus, id, path) {
    this.corpus = corpus;
    this.id = id;
    this.path = path;
    let idx = path.lastIndexOf("/fiches/");
    this.fichothequeUrl = path.substring(0, idx + 1);
    this.editable = false;
};

Ficheframe.Info.prototype.checkEditable = function(callback) {
    var ficheInfo = this;
    var fapi = new Fapi(ficheInfo.fichothequeUrl);
    fapi.loadFiche({
        corpus: ficheInfo.corpus,
        id: ficheInfo.id,
        callback: function (data) {
            if (data.fiche) {
                ficheInfo.editable = data.fiche.editable;
                if (callback) {
                    callback();
                }
            }
        }
    });
};

Ficheframe.Info.parse = function (iframe) {
    var pathName;
    try {
        pathName = iframe.contentWindow.location.pathname;
    } catch(error) {
        return null;
    }
    var ficheTest = pathName.match(/\/fiches\/([a-z][a-z0-9]*)-([0-9]+)(-[_a-z0-9]+)?\.html$/);
    if (!ficheTest) {
        return null;
    }
    var corpusName = ficheTest[1];
    var ficheId = ficheTest[2];
    return new Ficheframe.Info(corpusName, ficheId, pathName);
};
