/* global Bdf,$$, Ficheframe */

Ficheframe.Position = function (element, gotoObject, dimArray) {
    this.element = element;
    this.goto = gotoObject.goto;
    this.corpusName = gotoObject.corpusName;
    this.ficheId = gotoObject.ficheId;
    this.startY = dimArray[0];
    this.endY = dimArray[1];
    this.startX = dimArray[2];
    this.startMargin = this.startY;
    this.endMargin = this.endY;
};

Ficheframe.Position.prototype.contains = function (y) {
    if ((y >= this.startY) && (y <= this.endY)) {
        return true;
    } else {
        return false;
    }
};

Ficheframe.Position.prototype.containsWithMargin = function (y) {
    if ((y >= this.startMargin) && (y <= this.endMargin)) {
        return true;
    } else {
        return false;
    }
};

Ficheframe.Position.prototype.compare = function (y) {
    if (y < this.startY) {
        return -1;
    }
    if (y > this.endY) {
        return 1;
    }
    return 0;
};

Ficheframe.Position.prototype.setBackground = function (color) {
    this.element.style.backgroundColor = color;
};

Ficheframe.Position.prototype.checkCorpusName = function (defaultCorpusName) {
    if (this.corpusName) {
        return this.corpusName;
    } else {
        return defaultCorpusName;
    }
};

Ficheframe.Position.prototype.checkFicheId = function (defaultFicheId) {
    if (this.ficheId) {
        return this.ficheId;
    } else {
        return defaultFicheId;
    }
};

