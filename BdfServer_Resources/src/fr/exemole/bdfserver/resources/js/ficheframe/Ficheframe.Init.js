/* global Bdf,$$,Ficheframe */
/**
 * Objet global définissant l'espace de nom Ficheframe
 * 
 * @namespace Ficheframe,Bdf
 */

Ficheframe.init = function () {
    var toolbarId = $$({ficheframeRole: "toolbar"}).attr("id");
    var fieldeditId = $$({ficheframeRole: "fieldedit"}).attr("id");
    $$({ficheframeRole: "iframe"}).on("load", function (event) {
        let iframe = this;
        Ficheframe.testToolbar({
            iframe: iframe,
            toolbarId: toolbarId,
            fieldeditId: fieldeditId,
            editTarget: "",
            pageResultOptions: Ficheframe.ARGS.pageResultOptions,
            callback: function () {
                if (Ficheframe.ARGS.goto) {
                    try {
                        let $goto = $$(iframe.contentWindow.document, {bdfComponentName: Ficheframe.ARGS.goto});
                        if ($goto.length > 0) {
                            let top = $goto.offset().top;
                            $(iframe.contentWindow).scrollTop(top - 10);
                        }
                    } catch(error) {

                    }
                } 
            }
        });
    });
};

$(function () {
    Bdf.initTemplates();
    Ficheframe.init();
});
