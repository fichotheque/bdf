/* global Bdf,$$, Ficheframe */

Ficheframe.GotoManager = function (iframe, fieldeditId, startingLeft) {
    if (!startingLeft) {
        startingLeft = Ficheframe.GotoManager.startingLeft;
    }
    this.iframe = iframe;
    this.fieldeditId = fieldeditId;
    this.startingLeft = startingLeft;
    this.positions = null;
};

Ficheframe.GotoManager.startingLeft = 5;

Ficheframe.GotoManager.prototype.hide = function () {
   Ficheframe.show(this.fieldeditId, false);
    if (this.positions) {
        this.positions.highlightCurrent(false);
    }
  
};

Ficheframe.GotoManager.prototype.check = function () {
    let display = this.positions.isCurrentActive;
    Ficheframe.show(this.fieldeditId, display);
    this.positions.highlightCurrent(display);
};

Ficheframe.GotoManager.prototype.clearAll = function() {
    $$(this.fieldeditId).html("");
};

Ficheframe.GotoManager.prototype.initPositions = function () {
    if (!this.positions) {
        this.positions = Ficheframe.Positions.build(this.iframe.contentWindow.document);
    }
    return this.positions;
};

Ficheframe.GotoManager.prototype.clearPositions = function() {
    if (this.positions) {
        this.positions.clear();
        this.hide();
        this.positions = null;
    }
};

Ficheframe.GotoManager.getStartingLeft = function (fieldeditId) {
    return parseInt($$(fieldeditId).css("left"));
};   

Ficheframe.GotoManager.prototype.init = function(editable, gotoCallback) {
    if (!editable) {
        $$(fieldeditId).html("");
    }
    var gotoManager = this;
    var iframe = gotoManager.iframe;
    var contentWindow = iframe.contentWindow;
    var fieldeditId = gotoManager.fieldeditId;
    let timer;
    $(contentWindow).on("mousemove", function(event) {
        if (timer) {
            clearTimeout(timer);
            timer = false;
        }
        if (event.buttons) {
            gotoManager.hide();
            return;
        }
        let currentSelection = contentWindow.getSelection();
        if ((currentSelection) && (currentSelection.type === "Range")) {
            gotoManager.hide();
            return;
        }
        let target = event.target;
        if ((target) && (target.localName === "a")) {
                gotoManager.hide();
            return;
        }
        if ((gotoManager.positions) && (gotoManager.positions.isChanged(event.pageY))) {
            gotoManager.hide();
        }
        if (event.pageX < 30) {
            __testPosition(event);
        } else {
            timer = setTimeout(function () {
                __testPosition(event);
            }, 150);            
        }
    });
    $(contentWindow).on("scroll resize", function() {
        gotoManager.clearPositions();
    });
    
    function __testPosition(event) {
        let pageY = event.pageY;
        let positions = gotoManager.initPositions();
        if (positions.isChanged(pageY)) {
            let newPosition = positions.updatePosition(pageY);
            if ((newPosition) && (newPosition.goto)) {
                let top = newPosition.startY - $(contentWindow.document).scrollTop() + $(iframe).position().top;
                if (top > 3) {
                    positions.activateCurrent(true);
                    gotoCallback(newPosition, top, ___getLeft(newPosition));
                }
            }
        }
        gotoManager.check();
        
        function ___getLeft(newPosition) {
            if (newPosition.startX > 50) {
                return gotoManager.startingLeft + newPosition.startX - 50;
            }
            return gotoManager.startingLeft;
        }
    }
};
