/* global Bdf,$$ */
/**
 * Objet global définissant l'espace de nom Preview
 * 
 * @namespace Preview
 */
var Preview = {};

Preview.update = function (html, codeMirror, overlayId) {
    $$("preview_section").html(html);
    var lineNumber = codeMirror.getCursor().line + 1;
    while(lineNumber > 0) {
        let lineElement = _getLineElement(lineNumber);
        if (lineElement) {
            setTimeout(function () {
                _gotTo(lineElement);
            }, 250);
            break;
        } else {
            lineNumber--;
        }
    }
    setTimeout(_addBackToEditorButton, 350);
    
    
    function _getLineElement(number) {
        let $line = $$({bdfLinenumber: number});
        let length = $line.length;
        if (length > 0) {
            return $line[length - 1];
        } else {
            return null;
        }
    }
    
    function _gotTo(lineElement) {
        let $line = $(lineElement);
        let $container = $$("preview_container");
        let top = $line.position().top;
        let marginTop = parseInt($line.css("margin-top"), 10);
        let height = $line.height();
        let reminderHtml = Bdf.render("preview:reminder");
        $(reminderHtml)
                .appendTo($$("preview_section"))
                .css("top", top +  marginTop - 3)
                .height(height + 6);
        let scroll = _getScroll();
        if (scroll > 0) {
            $container.scrollTop(scroll);
        }
        
        function _getScroll() {
            let containerHeight = $container.height();
            let margin = ((containerHeight - height)/3);
            if (margin < 0) {
                return top;
            }
            return top - margin;
        }
        
    }
    
    function _addBackToEditorButton() {
        $$({_element: "p", bdfLinenumber: true}).each(function (index, element) {
            __append(element);
        });
        $$({_element: "span", bdfLinenumber: true}).each(function (index, element) {
            __append(element);
        });
        $$({_element: "tr", bdfLinenumber: true}).each(function (index, element) {
            __append(element);
        });
        $$({_element: "dt", bdfLinenumber: true}).each(function (index, element) {
            __append(element);
        });
        $$({_element: "figure", bdfLinenumber: true}).each(function (index, element) {
            if ((element.classList.contains("fbe-figure-Image")) || (element.classList.contains("fbe-figure-Cdata")) || (element.classList.contains("fbe-figure-Audio"))) {
                if ($(element).css("float") === "none") {
                    __append(element);
                }
            }
        });
        
        
        function __append(element) {
            let $element = $(element);
            let top = $element.position().top;
            let marginTop = parseInt($element.css("margin-top"), 10);
            let lineNumber = element.dataset.bdfLinenumber;
            $(Bdf.render("preview:backtoeditor", {lineNumber: lineNumber}))
                    .appendTo($$("preview_section"))
                    .css("top", top +  marginTop)
                    .click(function () {
                        _close(lineNumber);
                    })
                    .on("mouseover", function () {
                        $element.addClass("preview-Highlight")
                    })
                    .on("mouseout", function () {
                        $element.removeClass("preview-Highlight")
                    });
        }
        
        
    }
    
    function _close(lineNumber) {
        window.parent.Ficheform.Section.Overlay.endSectionPreview(overlayId, codeMirror, lineNumber);
    }
};

/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
});

