/* global Bdf,$$,AddendaDoc */

AddendaDoc.Upload = {};

AddendaDoc.Upload.ARGS = {
    clientId: "",
    appelant: "",
    newUpload: false,
    addenda: "",
    errorPage: "",
    resultPage: "",
    errorMessage: null
};

AddendaDoc.Upload.init = function (args) {
    var genId = Bdf.generateId();
    $$(args.clientId).html(Bdf.render((args.newUpload)?"document:client-upload-new":"document:client-upload-version", {
        args: args,
        genId: genId
    }));
    if (args.newUpload) {
        $$(genId, "cancel").click(function () {
            window.close();
        });
    }
};

$(function () {
    Bdf.initTemplates();
    AddendaDoc.Upload.init(AddendaDoc.Upload.ARGS);
});