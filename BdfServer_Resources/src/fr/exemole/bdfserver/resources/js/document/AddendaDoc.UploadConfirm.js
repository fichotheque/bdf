/* global Bdf,AddendaDoc */

AddendaDoc.UploadConfirm = {};

AddendaDoc.UploadConfirm.ARGS = {
     clientId: "",
     appelant: "",
     tmpFileName: "",
     extension: "",
     tmpUrl: "",
     size: "",
     unitSize: ""
     
};

AddendaDoc.UploadConfirm.init = function (args) {
    var genId = Bdf.generateId();
    $$(args.clientId).html(Bdf.render("document:client-uploadconfirm", {
        args: args,
        genId: genId
    }));
    $$(genId, "cancel").click(_cancel);
    $$(genId, "confirm").click(_confirm).focus();

    
    function _cancel() {
        let callback = _getCallback();
        if (callback) {
            callback(false);
        }
        if (window.opener) {
            window.close();
        }
    }
    
    function _confirm() {
        let callback = _getCallback();
        if (callback) {
            callback({
                appelant: args.appelant,
                tmpFileName: args.tmpFileName,
                extension: args.extension
            });
        }
        if (window.opener) {
            window.close();
        }
    }
    
    function _getCallback() {
        let appelantWindow;
        if (window.opener) {
            appelantWindow = window.opener;
        } else {
            appelantWindow = window.parent;
        }
        return appelantWindow.Bdf.getAppelantCallback(args.appelant);
    }
};
    
$(function () {
    Bdf.initTemplates();
    AddendaDoc.UploadConfirm.init(AddendaDoc.UploadConfirm.ARGS );
});