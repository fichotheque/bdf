/* global Bdf */
/**
 * Objet global définissant l'espace de nom AddendaDoc.
 * Note : cet espace de nom aurait dû s'appeler Document suivant la logique des autres
 * bibliothèques Javascript. Cependant la proximité entre Document et document, l'objet global
 * Javascript global a fait préférer le terme AddendaDoc pour éviter toute confusion
 * 
 * @namespace AddendaDoc
 */
var AddendaDoc = {};

AddendaDoc.NOCHANGE_STATE = "nochange";
AddendaDoc.UPDATE_STATE = "update";
AddendaDoc.REMOVE_STATE = "remove";
AddendaDoc.CREATE_STATE = "create";
AddendaDoc.DOCUMENT_CHANGETYPE = "document";
AddendaDoc.CREATION_CHANGETYPE = "creation";
    
AddendaDoc.testBasename = function (basename) {
	return basename.search(/^[-_.a-z0-9]+$/) !== -1;
};

AddendaDoc.toMap = function (extensionArray) {
    var map = {};
    for(let extensionObj of extensionArray) {
        map[extensionObj.extension] = extensionObj;
    }
    return map;
};

AddendaDoc.isLastVersion = function (extensionArray, extension) {
    for(let extensionObj of extensionArray) {
        if (extensionObj.extension === extension) {
            continue;
        }
        if (extensionObj.state !== AddendaDoc.REMOVE_STATE) {
            return false;
        }
    }
    return true;
};

Bdf.addTemplateOptions({
    helpers: {
        getClass: function (state) {
            if (state === AddendaDoc.REMOVE_STATE) {
                return "document-extension-Removed";
            } else if (state === AddendaDoc.UPDATE_STATE) {
                return "document-extension-Changed";
            } else if (state === AddendaDoc.CREATE_STATE) {
                return "document-extension-Added";
            } else {
                return "";
            }
        }
    }
});