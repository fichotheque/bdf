/* global Bdf,$$,TableExport,Overlay,Pane */

TableExport.Overlay = {};


TableExport.Overlay.showTableExportCreationForm = function () {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var corpusOptions = TableExport.subsetTrees.toSelectOptions("corpus", false);
    var thesaurusOptions = TableExport.subsetTrees.toSelectOptions("thesaurus", false);
    var submitId = Bdf.generateId();
    var corpusSelectId = Bdf.generateId();
    var thesaurusSelectId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-11"),
        content: Bdf.render("tableexport:overlay/tableexportcreationform", {
            corpusOptions: corpusOptions,
            thesaurusOptions: thesaurusOptions,
            corpusSelectId: corpusSelectId,
            thesaurusSelectId: thesaurusSelectId,
            commandKey: "EXP-11",
            commandUrl: Bdf.getCommandUrl("EXP-11")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.tableexport.creation',
                id: submitId,
                disabled: true
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Pane.clearMain();
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        TableExport.insertTableExport(data.tableExport);
                    });
                }
            }
        },
        afterStart: function () {
            $$(corpusSelectId).change(_testSubsetListSelection);
            $$(thesaurusSelectId).change(_testSubsetListSelection);
        }
    });
    
    
    function _testSubsetListSelection () {
        var count = $( "#" + corpusSelectId + " :selected").length +  $( "#" + thesaurusSelectId + " :selected").length;
        $$(submitId).prop("disabled", (count === 0));
    }
    
};

TableExport.Overlay.showContentRemoveConfirm = function (tableExportName, contentPath) {
    var tableExport = TableExport.getTableExport(tableExportName);
    if (!tableExport) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-16"),
        content: Bdf.render("tableexport:overlay/contentremoveconfirm", {
            tableExportName: tableExportName,
            title: tableExport.title,
            contentPath: contentPath,
            commandKey: "EXP-16",
            commandUrl: Bdf.getCommandUrl("EXP-16")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.tableexport.contentremove'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        Pane.clearMain();
                        Bdf.showCommandMessage(data.commandMessage);
                        TableExport.updateTableExport(data.tableExport);
                    });
                }
            }
        }
    });
};

TableExport.Overlay.showTableExportRemoveConfirm = function (tableExportName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var tableExport = TableExport.getTableExport(tableExportName);
    if (!tableExport) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-12"),
        content: Bdf.render("tableexport:overlay/tableexportremoveconfirm", {
            tableExportName: tableExportName,
            title: tableExport.title,
            commandKey: "EXP-12",
            commandUrl: Bdf.getCommandUrl("EXP-12")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.tableexport.remove'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        TableExport.removeTableExport(tableExportName);
                    });
                }
            }
        }
    });
};

TableExport.Overlay.showTableExportCopyForm = function (tableExportName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var tableExport = TableExport.getTableExport(tableExportName);
    if (!tableExport) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-18"),
        content: Bdf.render("tableexport:overlay/tableexportcopyform", {
            tableExportName: tableExportName,
            title: tableExport.title,
            commandKey: "EXP-18",
            commandUrl: Bdf.getCommandUrl("EXP-18")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.tableexport.copy'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Pane.clearMain();
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        TableExport.insertTableExport(data.tableExport);
                    });
                }
            }
        }
    });
};

TableExport.Overlay.showCorpusContentCreationForm = function (tableExportName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var tableExport = TableExport.getTableExport(tableExportName);
    if (!tableExport) {
        return;
    }
    var corpusOptions = TableExport.subsetTrees.toSelectOptions("corpus", true, TableExport.List.getExistingSubsetMap(tableExportName));
    var disabled = (corpusOptions.count === 0);
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-14"),
        content: Bdf.render("tableexport:overlay/corpuscontentcreationform", {
            tableExportName: tableExportName,
            title: tableExport.title,
            disabled: disabled,
            options: corpusOptions,
            commandKey: "EXP-14",
            commandUrl: Bdf.getCommandUrl("EXP-14")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.tableexport.corpuscontentcreation',
                disabled: disabled
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    let valueArray = data.commandMessage.values;
                    let contentPath = valueArray[1] + ".txt";
                    TableExport.updateTableExport(data.tableExport);
                    Pane.clearMain();
                    Bdf.Ajax.loadTableExportContent(tableExportName, contentPath, function (tableExportContent) {
                        TableExport.ContentForm.load(tableExportContent, false);
                        Overlay.end($form, function () {
                            Bdf.showCommandMessage(data.commandMessage);
                        });
                    });
                }
            }
        }
    });
};

TableExport.Overlay.showThesaurusContentCreationForm = function (tableExportName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var tableExport = TableExport.getTableExport(tableExportName);
    if (!tableExport) {
        return;
    }
    var thesaurusOptions = TableExport.subsetTrees.toSelectOptions("thesaurus", true, TableExport.List.getExistingSubsetMap(tableExportName));
    var disabled = (thesaurusOptions.count === 0);
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-15"),
        content: Bdf.render("tableexport:overlay/thesauruscontentcreationform", {
            tableExportName: tableExportName,
            title: tableExport.title,
            disabled: disabled,
            options: thesaurusOptions,
            commandKey: "EXP-15",
            commandUrl: Bdf.getCommandUrl("EXP-15")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.tableexport.thesauruscontentcreation',
                disabled: disabled
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    let valueArray = data.commandMessage.values;
                    let contentPath = valueArray[1] + ".txt";
                    Pane.clearMain();
                    TableExport.updateTableExport(data.tableExport);
                    Bdf.Ajax.loadTableExportContent(tableExportName, contentPath, function (tableExportContent) {
                        TableExport.ContentForm.load(tableExportContent, false);
                        Overlay.end($form, function () {
                            Bdf.showCommandMessage(data.commandMessage);
                        });
                    });
                }
            }
        }
    });
};
    
TableExport.Overlay.showTableTest = function (tableExportName, contentPath) {
    var dataDestinationId = Bdf.generateId();
    var subsetKeyObj = TableExport.parseSubsetKey(contentPath);
    Overlay.start({
            header: Bdf.Loc.escape("_ title.tableexport.tabletest"),
            content: Bdf.render("tableexport:overlay/tabletest", {
                dataDestinationId: dataDestinationId
            }),
            closeKey: "F1",
            isWaiting: true,
            afterStart: function (overlayId, waitingCallback) {
                $.ajax ({
                    url: "tables/get-htmlfragment",
                    data: {
                        tableexport: tableExportName,
                        subset: subsetKeyObj.subsetKey,
                        type: "echantillon",
                        header: "keyheader"
                    },
                    dataType: "html",
                    success: function (data) {
                        $$(dataDestinationId).html(data);
                        waitingCallback();
                     }
                 });
            }
    });
    
};