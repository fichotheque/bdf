/* global Bdf,TableExport,Overlay,$$,Pane */

TableExport.List = {};

TableExport.List.init = function (callback) {
    Bdf.Ajax.loadTableExportArray(function (tableExportArray) {
        tableExportArray = TableExport.initArray(tableExportArray);
        let $listBody = $$("listpane_body");
        $listBody.html(Bdf.render("tableexport:list/tableexport", tableExportArray));
        TableExport.List.initActions();
        Bdf.Deploy.init($listBody);
        if (callback) {
            callback();
        }
    });
};

TableExport.List.initActions = function () {
    $$("listpane_button_newtableexport").click(TableExport.Overlay.showTableExportCreationForm);
    var $body = $$("listpane_body");
    _onClick("metadata", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let name = Pane.getEntryName(this);
        Pane.loadOrDestock(name, function () {
            TableExport.MetadataForm.load(name);
        });
    });
    _onClick("tableexportremove", function () {
        TableExport.Overlay.showTableExportRemoveConfirm(Pane.getEntryName(this));
    });
    _onClick("newcorpuscontent", function () {
        TableExport.Overlay.showCorpusContentCreationForm(Pane.getEntryName(this));
    });
    _onClick("newthesauruscontent", function () {
        TableExport.Overlay.showThesaurusContentCreationForm(Pane.getEntryName(this));
    });
    _onClick("tableexportcopy", function () {
        TableExport.Overlay.showTableExportCopyForm(Pane.getEntryName(this));
    });
    _onClick("load-main", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let reference = _getReferenceAncestor(this);
        Pane.loadOrDestock(reference.dataset.contentKey, function () {
            _loadContent(reference, false);
        });
        return false;
    });
    _onClick("load-stock", function () {
        let reference = _getReferenceAncestor(this);
        let state = Pane.State.build();
        if (state.isLoaded(reference.dataset.contentKey)) {
            return;
        }
        _loadContent(reference, true);
    });
    
    function _onClick(actionName, clickFunction) {
        $body.on("click", $$.toCssSelector({action: actionName}), clickFunction);
    }
    
    function _getReferenceAncestor(element) {
        return  Bdf.getMandatoryAncestor(element, {role: "content-reference"});
    }
    
    function _loadContent(reference, isInStock) {
        let overlayId = Bdf.startRunningOverlay();
        Bdf.Ajax.loadTableExportContent(reference.dataset.tableExportName, reference.dataset.contentPath, function (tableExportContent) {
            TableExport.ContentForm.load(tableExportContent, isInStock);
            Overlay.end(overlayId);
        });
    }
    
};

TableExport.List.updateTableExport = function (newTableExportObj) {
    var $currentTableExport = $$({role: "entry", name: newTableExportObj.name});
    $$($currentTableExport, {role: "entry-title"}).html(newTableExportObj.title);
    Pane.updateStateClass($currentTableExport, "tableexport-state-" + newTableExportObj.state);
    var $contentUl = $$($currentTableExport, {role: "content-list"});
    Pane.updateContentList("tableexport", $contentUl, newTableExportObj.contentArray);
};

/**
 * Retourne l'ensemble des contenus existants d'une exportation
 * sous la forme d'un objet Map avec les noms des contenus comme clé (qui ont la valeur true)
 */
TableExport.List.getExistingSubsetMap = function (tableExportName) {
    var result = new Map();
    var $tableExport = Pane.getEntry(tableExportName);
    $$($tableExport, {role: "content-reference"}).each(function (index, element) {
        let subsetKeyObj = TableExport.parseSubsetKey(element.dataset.contentPath);
        result.set(subsetKeyObj.subsetKey, true);
    });
    return result;
};
