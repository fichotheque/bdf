/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom TableExport
 * 
 * @namespace TableExport
 */
var TableExport = {};

TableExport.subsetTrees = null;
TableExport.array = null;


/******************************************************************************
 * Actions sur TableExport
 ******************************************************************************/

TableExport.initArray = function (array) {
    for(let tableExportObj of array) {
        TableExport.completeTableExport(tableExportObj);
    }
    TableExport.array = array;
    return array;
};

TableExport.getTableExport = function (tableExportName) {
    for(let tableExport of TableExport.array) {
        if (tableExport.name === tableExportName) {
            return tableExport;
        }
    }
    return null;
};

TableExport.updateTableExport = function (newTableExport) {
    newTableExport = TableExport.completeTableExport(newTableExport);
    Bdf.updateInArray(TableExport.array, newTableExport);
    TableExport.List.updateTableExport(newTableExport);
    return newTableExport;
};

TableExport.removeTableExport = function (name) {
    Bdf.removeInArray(TableExport.array, name);
    Pane.removeContents(name);
    Pane.removeEntry($$("listpane_body"), name);
};

TableExport.insertTableExport = function (tableExport) {
    tableExport = TableExport.completeTableExport(tableExport);
    Bdf.insertInArray(TableExport.array, tableExport);
    var $entry = Pane.insertEntry($$("listpane_body"), tableExport.name, Bdf.render("tableexport:list/tableexport", tableExport));
    $$("listpane_body").animate({
        scrollTop: $entry.offset().top
    }, 1000, function () {
        Bdf.Deploy.ensureLinkVisibility($entry, true);
    });
    TableExport.MetadataForm.load(tableExport.name);
    return tableExport;
};

TableExport.completeTableExport = function (tableExportObj) {
    var tableExportName = tableExportObj.name;
    for(let contentObj of tableExportObj.contentArray) {
        contentObj.tableExportName = tableExportName;
        let subsetKeyObj = TableExport.parseSubsetKey(contentObj.path);
        contentObj.subsetKey = subsetKeyObj.subsetKey;
        contentObj.subsetCategory = subsetKeyObj.subsetCategory;
        contentObj.subsetName = subsetKeyObj.subsetName;
        let subset = TableExport.subsetTrees.getSubset(subsetKeyObj.subsetKey);
        if (subset) {
            contentObj.title = subset.title;
        }
    }
    return tableExportObj;
};


/******************************************************************************
 * Enregistrement de fonction
 ******************************************************************************/

Pane.registerCheckFunction(function (state) {
    var tableExportName, contentPath;
    $(".pane-list-ActiveItem").removeClass("pane-list-ActiveItem");
    if (state.withMain()) {
        _init(state.mainContentKey);
        let $tableExport = $$({role: "entry", name: tableExportName});
        $$($tableExport, {role: "entry-header"}).addClass("pane-list-ActiveItem");
        if (contentPath) {
            let $reference = $$($tableExport, {role: "content-reference", contentPath: contentPath});
            $$($reference, {role: "content-title"}).addClass("pane-list-ActiveItem");
            Pane.ensureVisibility($reference);
        }
    }
    
    
    function _init(mainContentKey) {
        let idx = mainContentKey.indexOf("/");
        if (idx > 0) {
            tableExportName = mainContentKey.substring(0, idx);
            contentPath = mainContentKey.substring(idx + 1);
        } else {
            tableExportName = mainContentKey;
            contentPath = false;
        }
    }
});


/******************************************************************************
 * Fonctions utilitaires
 ******************************************************************************/

TableExport.parseSubsetKey = function (contentPath) {
    var subsetKey = contentPath;
    var subsetCategory, subsetName;
    var idx = contentPath.indexOf(".txt");
    if (idx > 0) {
        subsetKey = contentPath.substring(0, idx);
    }
    idx = subsetKey.indexOf("_");
    subsetCategory = subsetKey.substring(0, idx);
    subsetName = subsetKey.substring(idx +1);
    return {
        subsetKey: subsetKey,
        subsetCategory: subsetCategory,
        subsetName: subsetName
    };
};

/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    $$("layout_list").html(Bdf.render("tableexport:list/pane", {}));
    Bdf.initBeforeUnload();
    Bdf.runSerialFunctions(
        Bdf.Ajax.initLangConfiguration,
        _initSubsetTrees,
        _initMemento,
        TableExport.List.init
    );
    Pane.initShortcuts();
    
    
    function _initSubsetTrees(callback) {
        Pane.loadSubsetTrees(function (subsetTrees) {
            TableExport.subsetTrees = subsetTrees;
            if (callback) {
                callback();
            }
        });
    }
    
    function _initMemento(callback) {
        Pane.loadMemento("xml/memento/tableexport", function () {
            if (callback) {
                callback();
            }
        });
    }
    
});