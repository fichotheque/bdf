/* global Bdf,L */
/**
 * Objet global définissant l'espace de nom Geo
 * 
 * @namespace Geo
 */
var Geo = {};

Geo.init = function (mapId, centerLat, centerLon, zoom) {
    var map = L.map(mapId).setView([centerLat, centerLon], zoom);
    var osmUrl = "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png";
    var osmAttrib = "Map data © <a href='http://openstreetmap.org'>OpenStreetMap</a> contributors";
    if (/^fr/.test(Bdf.WORKING_LANG)) {
        osmUrl = "//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png";
        osmAttrib = "donn&eacute;es &copy; <a href='/osm.org/copyright'>OpenStreetMap</a>/ODbL - rendu <a href='//openstreetmap.fr'>OSM France</a>";
    }
    var osm = new L.TileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
    osm.addTo(map);
    return map;
};

Geo.cleanString = function (val) {
    val =val.replace(/[\n\t]/g,",");
    val = val.replace(/^\s+|\s+$/g,"");
    val = val.replace(/\s+/g, " ");
    val = val.replace(/,,+/g, ",");
    val = val.replace(/^,+|,+$/g,"");
    val = val.replace(/,/g, ", ");
    return val;
};

Geo.accentToNoAccent = function (str) {
    var norm = new Array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï', 'Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','Þ','ß', 'à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ð','ñ', 'ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ý','þ','ÿ','Œ','œ');
    var spec = new Array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I', 'D','N','O','O','O','0','O','O','U','U','U','U','Y','b','s', 'a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','d','n', 'o','o','o','o','o','o','u','u','u','u','y','y','b','y','OE','oe');
    for (let i = 0, len =spec.length ; i < len; i++) {
        str = _replaceAll(str, norm[i], spec[i]);
    }
    return str;
    
    
    function _replaceAll(text, search, repl) {
        while (text.indexOf(search) !== -1)
            text = str.replace(search, repl);
        return text;
    }
};
