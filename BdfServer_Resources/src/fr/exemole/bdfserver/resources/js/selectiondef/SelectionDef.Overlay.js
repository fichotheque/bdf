/* global Bdf,$$,SelectionDef,Overlay,Pane */

SelectionDef.Overlay = {};

SelectionDef.Overlay.showCreationForm = function () {
    if (!Bdf.confirmUnload()) {
        return;
    }
    Overlay.start({
        header:Bdf.Loc.escape("_ EXP-01"),
        content: Bdf.render("selectiondef:overlay/creationform", {
            commandKey: "EXP-01",
            commandUrl: Bdf.getCommandUrl("EXP-01")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.selection.selectiondefcreation'
            }}),
        formAttrs: {
            action: Bdf.URL + "selection",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Pane.clearMain();
                    Overlay.end($form, function() {
                        Bdf.showCommandMessage(data.commandMessage);
                        SelectionDef.insertSelectionDef(data.selectionDef);
                    });
                }
            }
        }
    });
};

SelectionDef.Overlay.showRemoveConfirm = function (selectionDefName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var selectionDef = SelectionDef.getSelectionDef(selectionDefName);
    if (!selectionDef) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-02"),
        content: Bdf.render("selectiondef:overlay/removeconfirm", {
            name: selectionDefName,
            title: selectionDef.title,
            commandKey: "EXP-02",
            commandUrl: Bdf.getCommandUrl("EXP-02")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.selection.selectiondefremove'
            }}),
        formAttrs: {
            action: Bdf.URL + "selection",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                var done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function() {
                        Bdf.showCommandMessage(data.commandMessage);
                        SelectionDef.removeSelectionDef(selectionDefName);
                    });
                }
            }
        }
    });
};

SelectionDef.Overlay.showSelectionTest = function (selectionDefName) {
    var resultId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ title.selection.selectiontest"),
        content: Bdf.render("selectiondef:overlay/selectiontest", {
            resultId: resultId
        }),
        closeKey: "F1",
        isWaiting: true,
        afterStart: function (overlayId, waitingCallback) {
            $.ajax ({
                url: "corpus",
                data: {
                    json: "fiches",
                    selection: selectionDefName
                },
                dataType: "json",
                method: "GET",
                success: function (data) {
                    let done = Bdf.checkError(data);
                    if (done) {
                        $$(resultId).html(Bdf.render("selectiondef:overlay/testresult", {
                            fiches: data.fiches
                        }));
                    }
                    waitingCallback();
                 }
             });
        }
    });
    
};
