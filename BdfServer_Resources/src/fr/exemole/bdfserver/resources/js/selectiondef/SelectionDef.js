/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom SelectionDef
 * 
 * @namespace SelectionDef
 */
var SelectionDef = {};

SelectionDef.array = null;

SelectionDef.initArray = function (array) {
    for(let selectionDefObj of array) {
        SelectionDef.completeSelectionDef(selectionDefObj);
    }
    SelectionDef.array = array;
    return array;
};

SelectionDef.getSelectionDef = function (selectionDefName) {
    for(let selectionDef of SelectionDef.array) {
        if (selectionDef.name === selectionDefName) {
            return selectionDef;
        }
    }
    return null;
};

SelectionDef.updateSelectionDef = function (newSelectionDef) {
    newSelectionDef = SelectionDef.completeSelectionDef(newSelectionDef);
    Bdf.updateInArray(SelectionDef.array, newSelectionDef);
    SelectionDef.List.updateSelectionDef(newSelectionDef);
    return newSelectionDef;
};

SelectionDef.removeSelectionDef = function (name) {
    Bdf.removeInArray(SelectionDef.array, name);
    Pane.removeContents(name);
    Pane.removeEntry($$("listpane_body"), name);
};

SelectionDef.insertSelectionDef = function (selectionDef) {
    selectionDef = SelectionDef.completeSelectionDef(selectionDef);
    Bdf.insertInArray(SelectionDef.array, selectionDef);
    var $entry = Pane.insertEntry($$("listpane_body"), selectionDef.name, Bdf.render("selectiondef:list/selectiondef", selectionDef));
    $$("listpane_body").animate({
        scrollTop: $entry.offset().top
    }, 1000, function () {
        Bdf.Deploy.ensureLinkVisibility($entry, true);
    });
    SelectionDef.Form.load(selectionDef.name);
    return selectionDef;
};

SelectionDef.completeSelectionDef = function (selectionDefObj) {
    return selectionDefObj;
};


/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    $$("layout_list").html(Bdf.render("selectiondef:list/pane", {}));
    Bdf.initBeforeUnload();
    Bdf.runSerialFunctions(
        Bdf.Ajax.initLangConfiguration,
        Bdf.Ajax.initPathConfiguration,
        SelectionDef.List.init
    );
    Pane.initShortcuts();
});
