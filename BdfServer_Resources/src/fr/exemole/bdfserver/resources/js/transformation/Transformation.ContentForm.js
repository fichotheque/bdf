/* global Bdf,$$,Transformation,CodeMirror,Pane */

Transformation.ContentForm = {};

Transformation.ContentForm.load = function (templateContent, isInStock) {
    if (!isInStock) {
        isInStock = false;
    }
    var templateKey = templateContent.templateKey;
    var contentPath = templateContent.path;
    var formId = Bdf.generateId();
    var mandatory = false;
    var testFunction = null;
    var odFileTestFunction = null;
    _checkContentPath();
    var saveId = formId + "_button_save";
    var textareaId = formId + "_content";
    var htmlContent = Bdf.render("transformation:contentform", {
        action: Bdf.URL + "exportation",
        formId: formId,
        templateKey: templateKey,
        split: Transformation.split(templateKey),
        path: contentPath,
        content: templateContent.content,
        title: _getTitle(),
        withTestButton: (testFunction !== null),
        withDeleteButton: !mandatory,
        withOdFileTestButton: (odFileTestFunction !== null)
    });
    if (isInStock) {
        Pane.addStockUnit(htmlContent);
    } else {
        $$("layout_main").html(htmlContent);
    }
    $$(formId, "button_delete").click(function () {
        Transformation.Overlay.showTemplateContentRemoveConfirm(templateKey, contentPath);
    });
    if (testFunction) {
        $$(formId, "button_test").click(testFunction);
    }
    if (odFileTestFunction) {
        $$(formId, "button_odcontenttest").click(function () {
            odFileTestFunction("content.xml");
        });
        $$(formId, "button_odstylestest").click(function () {
            odFileTestFunction("styles.xml");
        });
    }
    $$(formId, "button_stock").click(function () {
        Pane.moveMainToStock();
    });
    $$(formId).ajaxForm({
        beforeSerialize: function ($form, options) {
            if (isInStock) {
                return false;
            } else {
                codeMirror.save();
                return true;
            }
        },
        dataType: "json",
        success: function (data, textStatus, jqXHR, $form) {
            if (Bdf.checkError(data)) {
                Pane.setChange(false);
                Bdf.showCommandMessage(data.commandMessage);
                Transformation.updateTemplateDescription(data.templateDescription);
            }
        }
    });
    var codeMirror = Bdf.toCodeMirror(textareaId, {
        lineWrapping: false,
        lineNumbers: true,
        indentUnit: 4,
        theme: 'bdf',
        mode: Bdf.getMatchingCodeMirrorMode(contentPath)
    });
    codeMirror.on("change",function () {
        Pane.setChange(true);
    });
    $$(formId).data("stockHandler", _stockHandler);
    Pane.checkState();
    Pane.checkTooltip($$(formId));

    
    function _checkContentPath() {
        switch(contentPath) {
            case "transformer.xsl":
                mandatory = true;
                if (Transformation.getTestPath(templateKey)) {
                    testFunction = function () {
                        Transformation.Overlay.showSimpleTemplateTest(templateKey);
                    };
                }
                break;
            case "content.xsl":
                mandatory = true;
                if (Transformation.getTestPath(templateKey)) {
                    testFunction = function () {
                        let testPath = Transformation.getTestPath(templateKey);
                        if (Transformation.isCompilation(templateKey)) {
                            testPath += "?limit=1";
                        }
                        window.location.href = testPath;
                    };
                    odFileTestFunction = function (file) {
                        Transformation.Overlay.showOdFileTest(templateKey, file);
                    };
                }
                break;
            case "extraction.xml":
                testFunction = function () {
                    Transformation.Overlay.showExtractionTest(templateKey);
                };
                break;
        }
    }
    
    function _getTitle() {
        let templateDescription = Transformation.getTemplateDescription(templateKey);
        if (templateDescription && templateDescription.title) {
            return templateDescription.title;
        } else {
            return "";
        }
    }
    
    function _stockHandler(stock) {
        isInStock = stock;
        codeMirror.setOption("readOnly", stock);
    }
    
};
