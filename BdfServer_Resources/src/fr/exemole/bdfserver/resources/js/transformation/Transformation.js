/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom Transformation
 * 
 * @namespace Transformation
 */
var Transformation = {};

Transformation.EMPTY_STATE = 0;
Transformation.OK_STATE = 1;
Transformation.WARNING_STATE = 2;
Transformation.ERROR_STATE = 3;
Transformation.templateDescriptionMap = new Map();
Transformation.subsetTrees = null;

Transformation.initTemplateDescriptionMap = function (transformationDescriptions) {
    for(let transformationKey in transformationDescriptions) {
        let transformationDescription = transformationDescriptions[transformationKey];
        for(let templateDescription of transformationDescription.simpleArray) {
            Transformation.putTemplateDescription(templateDescription);
        }
        for(let extension of transformationDescription.extensionArray) {
            for(let templateDescription of extension.templateArray) {
                Transformation.putTemplateDescription(templateDescription);
            }
        }
    }
};

Transformation.containsTemplateDescription = function (key) {
    return Transformation.templateDescriptionMap.has(key);
}

Transformation.updateTemplateDescription = function (newTemplateDescription) {
    newTemplateDescription = Transformation.putTemplateDescription(newTemplateDescription);
    Transformation.List.update(newTemplateDescription);
    return newTemplateDescription;
};

Transformation.putTemplateDescription = function (templateDescription) {
    templateDescription = Transformation.completeTemplateDescription(templateDescription);
    Transformation.templateDescriptionMap.set(templateDescription.key, templateDescription);
    return templateDescription;
};

Transformation.getTemplateDescription = function (templateKey) {
    return Transformation.templateDescriptionMap.get(templateKey);
};

Transformation.insertTemplateDescription = function (newTemplateDescription) {
    newTemplateDescription = Transformation.putTemplateDescription(newTemplateDescription);
    Transformation.List.insert(newTemplateDescription);
    return newTemplateDescription;
};

Transformation.removeTemplate = function (templateKey) {
    Transformation.templateDescriptionMap.delete(templateKey);
    Pane.removeContents(templateKey, ':');
    Transformation.List.remove(templateKey);
};

Transformation.hasExtractionDef = function (transformationKey) {
    if (transformationKey.startsWith("corpus_")) {
        return true;
    } else if (transformationKey === "compilation") {
        return true;
    } else if (transformationKey === "statthesaurus") {
        return true;
    } else {
        return false;
    }
};

Transformation.withExtractionReinit = function (templateDescription) {
    if (templateDescription.type !== "xslt") {
        return false;
    }
    if (!Transformation.hasExtractionDef(templateDescription.transformationKey)) {
        return false;
    }
    for(let content of templateDescription.contentArray) {
        if (content.path === "extraction.xml") {
            return false;
        }
    }
    return true;
};

Transformation.extractionReinit = function (templateKey) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            cmd: "TemplateExtractionReinit",
            json: "transformation-templatedescription",
            template: templateKey
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            if (Bdf.checkError(data)) {
                Transformation.updateTemplateDescription(data.templateDescription);
                Bdf.showCommandMessage(data.commandMessage);
            }
        }
    });
};

Transformation.hasOdtTemplate = function (transformationKey) {
    if (transformationKey.startsWith("corpus_")) {
        return true;
    } else if (transformationKey === "compilation") {
        return true;
    } else {
        return false;
    }
};

Transformation.hasFragmentTemplate = function (transformationKey) {
    if (transformationKey === "section") {
        return true;
    } else if (transformationKey === "format") {
        return true;
    } else {
        return false;
    }
};

Transformation.hasPropertiesOdtTemplate = function (transformationKey) {
    if (transformationKey.startsWith("corpus_")) {
        return true;
    } else {
        return false;
    }
};

Transformation.isCompilation = function (templateKey) {
    return (Transformation.split(templateKey).transformationKey === "compilation");
};

Transformation.isCorpus = function (templateKey) {
    return (Transformation.split(templateKey).transformationKey.startsWith("corpus_"));
};

Transformation.split = function (templateKey) {
    var transformationKey, templateName, subsetCategory, subsetName;
    _splitTemplateKey();
    _splitTransformationKey();
    return {
        transformationKey: transformationKey,
        templateName: templateName,
        subsetCategory: subsetCategory,
        subsetName: subsetName
    };

    function _splitTemplateKey() {
        let idx = templateKey.indexOf('/');
        transformationKey = templateKey.substring(0, idx);
        templateName = templateKey.substring(idx + 1);
    }
    
    function _splitTransformationKey() {
        let idx = transformationKey.indexOf("_");
        if (idx > 0) {
            subsetCategory = transformationKey.substring(0, idx);
            subsetName = transformationKey.substring(idx + 1);
        } else {
            subsetCategory = false;
            subsetName = false;
        }
    }
};

Transformation.getTestPath = function (templateKey) {
    var split = Transformation.split(templateKey);
    var testPath = "fiches/";
    if (split.transformationKey === "compilation") {
        testPath += "_compilation";
    } else if (split.transformationKey.indexOf("corpus_") === 0) {
        testPath += split.transformationKey.substring("corpus_".length);
        testPath += "-test";
    } else {
        return false;
    }
    testPath += "-" + split.templateName;
    return testPath;
};


/******************************************************************************
 * Rajout de champ
 ******************************************************************************/

Transformation.completeTemplateDescription = function (templateDescriptionObj) {
    var templateKey = templateDescriptionObj.key;
    templateDescriptionObj.completeName = Transformation.split(templateKey).templateName;
    var transformationKey = templateDescriptionObj.transformationKey;
    for(let contentObj of templateDescriptionObj.contentArray) {
        contentObj.templateKey = templateKey;
        contentObj.transformationKey = transformationKey;
    }
    templateDescriptionObj.withConvertToOdt = isWithConvertToOdt();
    return templateDescriptionObj;
    
    function isWithConvertToOdt() {
        if (templateDescriptionObj.extension) {
            return false;
        }
        if ((transformationKey === "compilation") || (transformationKey.startsWith("corpus_"))) {
            return true;
        } else {
            return false;
        }
    }
};


/******************************************************************************
 * Initialisation
 ******************************************************************************/

Bdf.addTemplateOptions({
    helpers: {
        withExtractionReinit: Transformation.withExtractionReinit
    }
});

Pane.registerCheckFunction(function (state) {
    var transformationKey, templateKey, contentPath;
    $(".pane-list-ActiveItem").removeClass("pane-list-ActiveItem");
    if (state.withMain()) {
        _init(state.mainContentKey);
        let $transformation = $$({role: "transformation", transformationKey: transformationKey});
        $$($transformation, {role: "entry-header"}).addClass("pane-list-ActiveItem");
        let $template = $$({role: "template", templateKey: templateKey});
        $$($template, {role: "template-header"}).addClass("pane-list-ActiveItem").parents("details").attr("open", "open");
        if (contentPath) {
            let $reference = $$($template, {role: "content-reference", contentPath: contentPath});
            $$($reference, {role: "content-title"}).addClass("pane-list-ActiveItem");
            Pane.ensureVisibility($reference);
        }
    }
     
     
    function _init(mainContentKey) {
        let idx = mainContentKey.indexOf(":");
        if (idx > 0) {
            templateKey = mainContentKey.substring(0, idx);
            contentPath = mainContentKey.substring(idx + 1);
        } else {
            templateKey = mainContentKey;
            contentPath = false;
        }
        transformationKey = Transformation.split(templateKey).transformationKey;
    }
});


$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    $$("layout_list").html(Bdf.render("transformation:list/pane", {}));
    Bdf.initBeforeUnload();
    Bdf.runSerialFunctions(
        Bdf.Ajax.initLangConfiguration,
        Bdf.Collections.initTableExportArray,
        _initSubsetTrees,
        Transformation.List.init
    );
    Pane.initShortcuts();
    
    
    function _initSubsetTrees(callback) {
        Pane.loadSubsetTrees(function (subsetTrees) {
             Transformation.subsetTrees = subsetTrees;
            if (callback) {
                callback();
            }
        });
    }

});