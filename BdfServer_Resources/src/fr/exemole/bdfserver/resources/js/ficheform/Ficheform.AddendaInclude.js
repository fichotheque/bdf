/* global Bdf,Ficheform,$$ */

Ficheform.AddendaInclude = {};

Ficheform.AddendaInclude.NOCHANGE_STATE = "nochange";
Ficheform.AddendaInclude.UPDATE_STATE = "update";
Ficheform.AddendaInclude.REMOVE_STATE = "remove";
Ficheform.AddendaInclude.CREATE_STATE = "create";
Ficheform.AddendaInclude.DOCUMENT_CHANGETYPE = "document";
Ficheform.AddendaInclude.CREATION_CHANGETYPE = "creation";

Ficheform.AddendaInclude.init = function () {
    Ficheform.AddendaInclude.bindFileEvents($$({ficheformRole: "document-file"}).find("input"));
    $$({ficheformEntry: "addenda-include"}).each(function (index, element) {
        Ficheform.AddendaInclude.Manager.init(element);
    });
};

Ficheform.AddendaInclude.bindFileEvents = function ($element) {
    $element.on("dragover", function (event) {
        $(this).addClass("global-FileInput_dragover");
        event.preventDefault();
    });
    $element.on("dragleave", function () {
        _removeDragOverClass($(this));
    });
    $element.on("drop", function () {
        _removeDragOverClass($(this));
    });
    $element.on("change", _change);
    
    function _change() {
        let changedFileInput = this;
        let $changedFileInput = $(this);
        _removeDragOverClass($changedFileInput);
        $changedFileInput.addClass("global-FileInput_filled");
        if (changedFileInput.files.length > 1) {
            let array = new Array();
            for(let file of changedFileInput.files) {
                array.push({
                    name: file.name
                });
            }
            let html = Bdf.render("ficheform:addenda/filelist", {
                inputId: changedFileInput.id,
                files: array
            });
            let $currentList = $$(changedFileInput.id, "filelist");
            if ($currentList.length > 0) {
                $currentList.replaceWith(html);
            } else {
                $changedFileInput.after(html);
            }
            $changedFileInput.addClass("global-FileInput_filled_many");
        } else {
            $$(changedFileInput.id, "filelist").replaceWith("");
            $changedFileInput.removeClass("global-FileInput_filled_many");
        }
        let $parent = $(this).parent();
        let isFull = true;
        let $fileInputList = $parent.find("input");
        for(let i = 0; i < $fileInputList.length; i++) {
            let fileInput = $fileInputList[i];
            if (fileInput.files.length === 0) {
                isFull = false;
            }
        }
        if (isFull) {
            let texte = '<input type="file" class="ficheform-Full global-FileInput TEMPORARY" name="' + this.name + '" id="' + Bdf.generateId() + '" size="70" multiple>';
            $parent.append(texte);
        }
        let $temporary = $parent.find("input.TEMPORARY");
        Ficheform.AddendaInclude.bindFileEvents($temporary);
        $temporary.removeClass("TEMPORARY");
    }
    
    function _removeDragOverClass($input) {
        $input.removeClass("global-FileInput_dragover");
    }
    
};

Ficheform.AddendaInclude.changeObjectToString = function (changeObject) {
    var extensionMap = changeObject.extensionMap;
    var text = "";
    for(let extension in extensionMap) {
        let extensionObj = extensionMap[extension];
        let state = extensionObj.state;
        if (state === Ficheform.AddendaInclude.REMOVE_STATE) {
            if (extensionObj.previousState !== Ficheform.AddendaInclude.CREATE_STATE) {
                text += "re=" + extension + ",";
            }
        } else if ((state === Ficheform.AddendaInclude.UPDATE_STATE) || (state === Ficheform.AddendaInclude.CREATE_STATE)) {
            text += "tf=" + extensionObj.tmpFileName + ",";
        }
    }
    if (changeObject.basename) {
        text += "bn=" + changeObject.basename;
    }
    return text;
};
