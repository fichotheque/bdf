/* global Bdf,Ficheform,$$,Fql,Fapi */

Ficheform.CorpusInclude = {};

Ficheform.CorpusInclude.init = function () {
    Bdf.Api.init();
    $$({ficheformEntry: "corpus-include", ficheformMode: "check"}).each(function (index, element) {
         Ficheform.CorpusInclude.Manager.init(element, "check");
    });
    $$({ficheformEntry: "corpus-include", ficheformMode: "table"}).each(function (index, element) {
        Ficheform.CorpusInclude.Manager.init(element, "table");
    });
   $$({ficheformEntry: "liage"}).each(function (index, element) {
        Ficheform.CorpusInclude.Manager.init(element, "liage");
    });  
};

Ficheform.CorpusInclude.showFicheCreation = function (inputId) {
    try {
        let subsetReference = Bdf.getMandatoryAncestor($$.one(inputId), {subsetName: true} );
        let subsetName = subsetReference.dataset.subsetName;
        Bdf.Overlay.showFicheEditor(subsetName, -1, {
            doneCallback: function (subsetName, id) {
                Bdf.appendValue($$.one(inputId), id);
            }
        });
    } catch(e) {
        
    }
};

Ficheform.CorpusInclude.parseColDefList = function (text) {
    var result = new Array();
    if (!text) {
        return result;
    }
    var tokens = text.split(';');
    for(let token of tokens) {
        let name,format;
        let idx = token.indexOf('|');
        if (idx > 0) {
            name = token.substring(0, idx).trim();
            format = token.substring(idx + 1).trim();
        } else {
            name = token.trim();
            format = "";
        }
        result.push({
            name: name,
            format: format
        });
    }
    return result;
};

