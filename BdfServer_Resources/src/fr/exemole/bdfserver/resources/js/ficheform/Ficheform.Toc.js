/* global Bdf,Ficheform,$$ */

Ficheform.Toc = {};


Ficheform.Toc.init = function () {
    var array = new Array();
    $("form h1, form h2").each(function (index, element) {
        let id = element.id;
        if (!id) {
            id = Bdf.generateId();
            element.id = id;
        }
        let $element = $(element);
        let text = $element.text();
        if (!$element.is(":hidden")) {
            array.push({
                id: id,
                text: text
            });
        }
    });
    $("header").after(Bdf.render("ficheform:toc", {
        array: array
    }));
};
