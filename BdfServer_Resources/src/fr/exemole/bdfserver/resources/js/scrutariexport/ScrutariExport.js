/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom ScrutariExport
 * 
 * @namespace ScrutariExport
 */
var ScrutariExport = {};

ScrutariExport.subsetTrees = null;
ScrutariExport.array = null;


/******************************************************************************
 * Actions sur ScrutariExport
 ******************************************************************************/

ScrutariExport.initArray = function (array) {
    for(let scrutariExportObj of array) {
        ScrutariExport.completeScrutariExport(scrutariExportObj);
    }
    ScrutariExport.array = array;
    return array;
};

ScrutariExport.getScrutariExport = function (scrutariExportName) {
    for(let scrutariExport of ScrutariExport.array) {
        if (scrutariExport.name === scrutariExportName) {
            return scrutariExport;
        }
    }
    return null;
};

ScrutariExport.updateScrutariExport = function (newScrutariExport) {
    newScrutariExport = ScrutariExport.completeScrutariExport(newScrutariExport);
    Bdf.updateInArray(ScrutariExport.array, newScrutariExport);
    ScrutariExport.List.updateScrutariExport(newScrutariExport);
    return newScrutariExport;
};

ScrutariExport.removeScrutariExport = function (name) {
    Bdf.removeInArray(ScrutariExport.array, name);
    Pane.removeContents(name);
    Pane.removeEntry($$("listpane_body"), name);
};

ScrutariExport.insertScrutariExport = function (scrutariExport) {
    scrutariExport = ScrutariExport.completeScrutariExport(scrutariExport);
    Bdf.insertInArray(ScrutariExport.array, scrutariExport);
    var $entry = Pane.insertEntry($$("listpane_body"), scrutariExport.name, Bdf.render("scrutariexport:list/scrutariexport", scrutariExport));
    $$("listpane_body").animate({
        scrollTop: $entry.offset().top
    }, 1000, function () {
        Bdf.Deploy.ensureLinkVisibility($entry, true);
    });
    ScrutariExport.Form.load(scrutariExport.name);
    return scrutariExport;
};

ScrutariExport.completeScrutariExport = function (scrutariExportObj) {
    return scrutariExportObj;
};


/******************************************************************************
 * Enregistrement de fonction
 ******************************************************************************/

Pane.registerCheckFunction(function (state) {
    $(".pane-list-ActiveItem").removeClass("pane-list-ActiveItem");
    if (state.withMain()) {
        let $scrutariExport = $$({role: "entry", name: state.mainContentKey});
        $$($scrutariExport, {role: "entry-header"}).addClass("pane-list-ActiveItem");
    }
});


/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    Bdf.Deploy.addVisibilityListener(_visibilityListener);
    $$("layout_list").html(Bdf.render("scrutariexport:list/pane", {}));
    Bdf.initBeforeUnload();
    Bdf.runSerialFunctions(
        Bdf.Ajax.initLangConfiguration,
        Bdf.Ajax.initPathConfiguration,
        Bdf.Collections.initSelectionDefArray,
        _initSubsetTrees,
        _initMemento,
        ScrutariExport.List.init
    );
    Pane.initShortcuts();
    
    
    function _initSubsetTrees(callback) {
        Pane.loadSubsetTrees(function (subsetTrees) {
            ScrutariExport.subsetTrees = subsetTrees;
            if (callback) {
                callback();
            }
        });
    }
    
    function _initMemento(callback) {
        Pane.loadMemento("xml/memento/tableexport", function () {
            if (callback) {
                callback();
            }
        });
    }
    
    function _visibilityListener(element, visible) {
        if (visible) {
            Bdf.refreshCodeMirror(element);
        }
    };
    
});