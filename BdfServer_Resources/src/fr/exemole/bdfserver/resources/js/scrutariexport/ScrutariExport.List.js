/* global Bdf,$$,ScrutariExport,Pane */

ScrutariExport.List = {};

ScrutariExport.List.init = function (callback) {
    Bdf.Ajax.loadScrutariExportArray(function (scrutariExportArray) {
        scrutariExportArray = ScrutariExport.initArray(scrutariExportArray);
        let $listBody = $$("listpane_body");
        $listBody.html(Bdf.render("scrutariexport:list/scrutariexport", scrutariExportArray));
        Bdf.Deploy.init($listBody);
        ScrutariExport.List.initActions();
        if (callback) {
            callback();
        }
    });
};

ScrutariExport.List.initActions = function () {
    $$("listpane_button_newscrutariexport").click(ScrutariExport.Overlay.showCreationForm);
    var $body = $$("listpane_body");
    _onClick("metadata", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let name = Pane.getEntryName(this);
        Pane.loadOrDestock(name, function () {
            ScrutariExport.Form.load(name);
        });
    });
    _onClick("run", function () {
        ScrutariExport.Overlay.showRun(Pane.getEntryName(this));
    });
    _onClick("scrutariexportremove", function () {
        ScrutariExport.Overlay.showRemoveConfirm(Pane.getEntryName(this));
    });
    
    function _onClick(actionName, clickFunction) {
        $body.on("click", $$.toCssSelector({action: actionName}), clickFunction);
    }
};

ScrutariExport.List.updateScrutariExport = function (newScrutariExport) {
    var $currentScrutariExport =  $$({role: "entry", name: newScrutariExport.name});
    $$($currentScrutariExport, {role: "entry-title"}).html(newScrutariExport.title);
};