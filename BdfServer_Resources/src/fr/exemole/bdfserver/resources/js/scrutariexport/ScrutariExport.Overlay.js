/* global Bdf,$$,ScrutariExport,Overlay,Pane */

ScrutariExport.Overlay = {};

/******************************************************************************
* Appel des différents formulaires
******************************************************************************/
ScrutariExport.Overlay.showCreationForm = function () {
    if (!Bdf.confirmUnload()) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-21"),
        content: Bdf.render("scrutariexport:overlay/creationform", {
            commandKey: "EXP-21",
            commandUrl: Bdf.getCommandUrl("EXP-21")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.exportation.scrutariexportcreation'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Pane.clearMain();
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        ScrutariExport.insertScrutariExport(data.scrutariExport);
                    });
                }
            }
        }
    });
};

ScrutariExport.Overlay.showRemoveConfirm = function (scrutariExportName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var scrutariExport = ScrutariExport.getScrutariExport(scrutariExportName);
    if (!scrutariExport) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-22"),
        content: Bdf.render("scrutariexport:overlay/removeconfirm", {
            name: scrutariExportName,
            title: scrutariExport.title,
            commandKey: "EXP-22",
            commandUrl: Bdf.getCommandUrl("EXP-22")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.exportation.scrutariexportremove'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        ScrutariExport.removeScrutariExport(scrutariExportName);
                    });
                }
            }
        }
    });
};

ScrutariExport.Overlay.showRun = function (scrutariExportName) {
    var resultDestinationId = Bdf.generateId();
    var scrutariExport = ScrutariExport.getScrutariExport(scrutariExportName);
    if (!scrutariExport) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ title.exportation.rundialog"),
        content: Bdf.render("scrutariexport:overlay/run", {
            name: scrutariExportName,
            title: scrutariExport.title,
            resultDestinationId: resultDestinationId
        }),
        isWaiting: true,
        afterStart: function (overlayId, waitingCallback) {
            $.ajax ({
                url: Bdf.URL + "exportation",
                data: {
                    scrutariexport:  scrutariExportName,
                    cmd: "ScrutariExportRun",
                    json: "scrutariexport-paths"
                },
                dataType: "json",
                success: function (data, textStatus) {
                    Bdf.checkData(data, "scrutariExportPaths", function (scrutariExportPaths) {
                        $$(resultDestinationId).html(Bdf.render("scrutariexport:overlay/run_result", {
                            rootUrl: Bdf.URL,
                            paths:  scrutariExportPaths
                        }));
                    });
                    waitingCallback();
                }
            });
        }
    });
    
};
