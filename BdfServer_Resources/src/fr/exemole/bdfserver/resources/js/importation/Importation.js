/* global Bdf */
/**
 * Objet global définissant l'espace de nom Importation
 * 
 * @namespace Importation
 */
var Importation = {};

Importation.getPageFromType = function (type) {
    if (type === "labelimport") {
        return "labelimport";
    }
    if (type === "thesaurusimport") {
        return "thesaurusimport";
    }
    if (type === "corpusimport") {
        return "corpusimport";
    }
    return "";
}
;
/******************************************************************************
 * Traitement du json du formulaire
 ******************************************************************************/

Importation.processJson = function (data, $form) {
     if (data.commandMessage) {
         alert(data.commandMessage.text);
     } else if (data.parseResult) {
         Importation.Overlay.showParseResult(data.parseResult);
     }
};


/******************************************************************************
 * Initialisation
 ******************************************************************************/
$(function () {
    Bdf.initTemplates();
    var pageInput = $('input[name="page"]');
    pageInput.attr('name', 'json');
    pageInput.attr('value', 'parseresult');
   $('form').ajaxForm({
            dataType:  'json',
            success:   function (data, textStatus, jqXHR, $form) {
                Importation.processJson(data, $form);
            }
        });
});