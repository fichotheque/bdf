/* global Bdf,Overlay,Importation */

Importation.Overlay = {};

Importation.Overlay.showParseResult = function (parseResult) {
    var contentHtml  = "";
    var footerHtml, formAttrs;
    var withError = false;
    var withWarning = false;
    if (parseResult.initErrorArray) {
        withError = true;
        contentHtml += Bdf.render("importation:initerrors", {
            array: parseResult.initErrorArray
        });
    }
    if (parseResult.initWarningArray) {
        withWarning = true;
        contentHtml += Bdf.render("importation:initwarnings", {
            array: parseResult.initWarningArray
        });
    }
    if (parseResult.parseErrorArray) {
        withError = true;
        contentHtml += Bdf.render("importation:parseerrors", {
            array: parseResult.parseErrorArray
        });
    }
    if (parseResult.bdfErrorArray) {
        withError = true;
        contentHtml += Bdf.render("importation:bdferrors", {
            array: parseResult.bdfErrorArray
        });
    }
    if ((parseResult.tmpFile || parseResult.tmpDir)  && ((!withWarning) && (!withError))) {
        formAttrs = {
            action: Bdf.URL + "importation",
            method: "POST"
        };
        contentHtml += Bdf.render("importation:confirm", {
           type: parseResult.type,
           page: Importation.getPageFromType(parseResult.type),
           tmpFile: parseResult.tmpFile,
           tmpDir: parseResult.tmpDir,
           subsetHtml: _getSubsetHtml()
        });
        footerHtml = Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.importation.confirm'
            }
        });
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ title.importation.confirm"),
        content: contentHtml,
        footer: footerHtml,
        formAttrs: formAttrs
    });
    
    
    function _getSubsetHtml() {
        var $select = $$({_element: "select", subset: "selection"});
        var $option = $select.find("option").filter(":selected"); 
        if ($option.length === 0) {
            return "";
        }
        var optionText = $option.text();
        var idx = optionText.indexOf(']');
        var subsetKey, subsetTitle;
        if (idx !== -1) {
            subsetKey = optionText.substring(0, idx + 1);
            subsetTitle = optionText.substring(idx + 1);
        }
        return Bdf.render("importation:subset", {
            text: Bdf.Loc.get(_getLabelKey($select.attr("name"))),
            subsetKey: subsetKey,
            subsetTitle: subsetTitle
        });
    }
    
    function _getLabelKey(name) {
        switch(name) {
            case "corpus":
                return "_ label.importation.corpus";
            case "thesaurus":
                return "_ label.importation.thesaurus";
            default:
                return "";
        }
    }
    
};
