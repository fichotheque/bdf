/**
 * Objet global définissant l'espace de nom OpenDocument
 * 
 * @namespace OpenDocument
 */
var OpenDocument = {};

OpenDocument.DEFAULT_CELLSTYLE_NAME = "Default";
OpenDocument.COLUMNSTYLE_PREFIX = "co";
OpenDocument.CELLSTYLE_PREFIX = "ce";
OpenDocument.DATASTYLE_PREFIX = "N";
OpenDocument.SPREADSHEET_MIMETYPE = "application/vnd.oasis.opendocument.spreadsheet";

OpenDocument.checkSheetName = function (name) {
    var result = "";
    var carac;
    for (let i = 0, len = name.length; i < len; i++) {
        carac = name.charAt(i);
        switch (carac) {
            case '[':
                carac = '(';
                break;
            case ']':
                carac = ')';
                break;
            case '*':
            case ':':
            case '/':
            case '?':
            case '\\':
                carac = '-';
                break;
        }
        result += carac;
    }
    return result;
};

OpenDocument.checkHiddenValue = function (element) {
    let odHidden = element.dataset["odHidden"];
    if (!odHidden) {
        return 0;
    }
    switch(odHidden.toLowerCase()) {
        case 'true':
        case '1':
        case 'yes':
            return 1;
        case 'false':
        case '0':
        case '-1':
        case 'no':
            return -1;
        default:
            return 0;
    }
};

OpenDocument.toChar = function (columnNumber) {
    return (columnNumber + 64);
};
