/* global OpenDocument,Currency,XmlWriter */

OpenDocument.CellCounter = function () {
    this.row = 0;
    this.column = 0;
    this.rowMap = new Map();
};

OpenDocument.CellCounter.prototype.reinit = function () {
    this.row = 0;
    this.column = 0;
    this.rowMap.clear();
};

OpenDocument.CellCounter.prototype.newRow = function () {
    this.row = this.row + 1;
    this.column = 1;
};

OpenDocument.CellCounter.prototype.newCell = function (rowSpan, colSpan) {
    let currentRow = this.row;
    let currentColumn = this.column;
    let jump = 0;
    let coveredCellArray = this.rowMap.get(currentRow);
    if (coveredCellArray) {
        while(true) {
            if (coveredCellArray.indexOf(currentColumn) !== -1) {
                currentColumn++;
                jump++;
            } else {
                break;
            }
        }
    }
    if (rowSpan > 1) {
        for(let i = 1; i < rowSpan; i++) {
            for(let j = 0; j < colSpan; j++) {
                this.putCoveredCell(currentRow + i, currentColumn + j);
            }
        }
    }
    if (colSpan > 1) {
        currentColumn += (colSpan - 1);
    }
    this.column = currentColumn + 1;
    return jump;
};

OpenDocument.CellCounter.prototype.putCoveredCell = function (row, column) {
    if (this.rowMap.has(row)) {
        this.rowMap.get(row).push(column);
    } else {
        let array = new Array();
        array.push(column);
        this.rowMap.set(row, array);
    }
};
