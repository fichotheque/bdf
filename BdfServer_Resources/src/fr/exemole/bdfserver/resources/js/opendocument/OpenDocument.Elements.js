/* global OpenDocument */

OpenDocument.Elements = {};

OpenDocument.Elements.Cell = function (value, styleName, rowSpan, colSpan) {
    this.value = value;
    this.styleName = styleName;
    this.rowSpan = rowSpan;
    this.colSpan = colSpan;
};

/***********************************************************
 * 
 * @constructor
 * @param {String} styleName
 * @param {Number} columnsRepeated
 * @param {String} defaultCellStyleName
 * @returns {OpenDocument.TableColumn}
 */
OpenDocument.Elements.TableColumn = function (styleName, columnsRepeated, defaultCellStyleName) {
    this.styleName = styleName;
    this.columnsRepeated = columnsRepeated;
    if (defaultCellStyleName) {
        this.defaultCellStyleName = defaultCellStyleName;
    } else {
        this.defaultCellStyleName = "Default";
    }
};

