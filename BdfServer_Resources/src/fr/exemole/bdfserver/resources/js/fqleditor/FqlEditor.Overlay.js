/* global Bdf,$$,Fql,FqlEditor,Overlay */

FqlEditor.Overlay = {};

FqlEditor.Overlay.start = function (formId, options) {
    var startOptions = Object.assign({
        header: "",
        footer: Bdf.render("fqleditor:overlay/footer", {
            formId: formId
        }),
        classes: {
            footer: "fqleditor-overlay-Footer"
        },
        supplementaryClasses: {
            blocker: "fqleditor-overlay-Blocker"
        }},
        options
    );
    Overlay.start(startOptions);
};

FqlEditor.Overlay.showCorpusEdit = function (corpusCondition, model, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/corpusedit", _getFormContext()),
        afterStart: function (overlayId) {
            let $form = $$(formId);
            Bdf.Deploy.init($form);
            $$(formId, "button_edit").click(function () {
                let corpusCondition = _getNewCorpusCondition();
                Overlay.end(overlayId, function () {
                    callback(corpusCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let corpusArray = FqlEditor.getSubsetArray("corpus", corpusCondition, model);
        
        return {
            formId: formId,
            corpusArray: corpusArray,
            all: corpusCondition.isAll(),
            exclude: corpusCondition.isExclude,
            current: corpusCondition.withCurrent
        };
    }
    
    function _getNewCorpusCondition() {
        let $form = $$(formId);
        let scope = $$($form, {_name:"scope", _checked: true}).val();
        let corpusCondition = new Fql.Condition.Subset();
        if (scope === "selection") {
            corpusCondition.withCurrent = $$.one(formId, "current").checked;
            corpusCondition.isExclude = $$.one(formId, "exclude").checked;
            let corpus = "";
            $$($form, {_name:"corpus", _checked: true}).each(function (index, element) {
                corpus += element.value + " ";
            });
            corpusCondition.addSubsets(corpus);
        }
        return corpusCondition;
    }
};

FqlEditor.Overlay.showThesaurusEdit = function (thesaurusCondition, model, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/thesaurusedit", _getFormContext()),
        afterStart: function (overlayId) {
            let $form = $$(formId);
            Bdf.Deploy.init($form);
            $$(formId, "button_edit").click(function () {
                let thesaurusCondition = _getThesaurusCondition();
                Overlay.end(overlayId, function () {
                    callback(thesaurusCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let thesaurusArray = FqlEditor.getSubsetArray("thesaurus", thesaurusCondition, model);
        
        return {
            formId: formId,
            thesaurusArray: thesaurusArray,
            all: thesaurusCondition.isAll(),
            exclude: thesaurusCondition.isExclude,
            current: thesaurusCondition.withCurrent
        };
    }
    
    function _getThesaurusCondition() {
        let $form = $$(formId);
        let scope = $$($form, {_name:"scope", _checked: true}).val();
        let thesaurusCondition = new Fql.Condition.Subset();
        if (scope === "selection") {
            thesaurusCondition.withCurrent = $$.one(formId, "current").checked;
            thesaurusCondition.isExclude = $$.one(formId, "exclude").checked;
            let thesaurus = "";
            $$($form, {_name:"thesaurus", _checked: true}).each(function (index, element) {
                thesaurus += element.value + " ";
            });
            thesaurusCondition.addSubsets(thesaurus);
        }
        return thesaurusCondition;
    }
};


FqlEditor.Overlay.showCheckEdit = function (checked, l10nKey, callback) {
    var formId = Bdf.generateId();
   FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/checkedit", _getFormContext()),
        afterStart: function (overlayId) {
            $$(formId, "checkbox").focus();
            $$(formId, "button_edit").click(function () {
                let newChecked = _getNewCheckedValue();
                Overlay.end(overlayId, function () {
                    callback(newChecked);
                });
            });
        }
    });
    
    function _getFormContext() {
        return {
            formId: formId,
            l10nKey: l10nKey,
            checked: checked
        };
    }
    
    function _getNewCheckedValue() {
        return $$.one(formId, "checkbox").checked;
    }
};

FqlEditor.Overlay.showLogicalOperatorEdit = function (value, l10nKey, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/logicaloperatoredit", _getFormContext()),
        afterStart: function (overlayId) {
            $$(formId, "button_edit").click(function () {
                let newOperator = _getNewOperatorValue();
                Overlay.end(overlayId, function () {
                    callback(newOperator);
                });
            });
        }
    });
    
    function _getFormContext() {
        return {
            formId: formId,
            l10nKey: l10nKey,
            operator: value
        };
    }
    
    function _getNewOperatorValue() {
        let $form = $$(formId);
        let operator = $$($form, {_name:"operator", _checked: true}).val();
        return operator;
    }
};

FqlEditor.Overlay.showDiscardEdit = function (discardFilter, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/discardedit", _getFormContext()),
        afterStart: function (overlayId) {
            $$(formId, "button_edit").click(function () {
                let newDiscardFilter = _getNewDiscardFilter();
                Overlay.end(overlayId, function () {
                    callback(newDiscardFilter);
                });
            });
        }
    });
    
    function _getFormContext() {
        let discardArray = new Array();
        __addDiscard(Fql.DISCARDFILTER_ALL);
        __addDiscard(Fql.DISCARDFILTER_NONE);
        __addDiscard(Fql.DISCARDFILTER_ONLY);
    
        function __addDiscard(value) {
            discardArray.push({
                name: value,
                l10nKey: FqlEditor.getMatchingLocKey(value),
                checked: (value === discardFilter)
            });
        }
    }
    
    function _getNewDiscardFilter() {
        return $$(formId, {_name:"discard", _checked: true}).val();
    }
};

FqlEditor.Overlay.showCroisementEdit = function (croisementCondition, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/croisementedit", _getFormContext()),
        afterStart: function (overlayId) {
            $$(formId, "button_edit").click(function () {
                let newCroisementCondition = _getNewCroisementCondition();
                Overlay.end(overlayId, function () {
                    callback(newCroisementCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let modes = "";
        let weight = "";
        let weightExclude = false;
        if (croisementCondition) {
            modes = croisementCondition.modeToText("; ", true);
            weight = croisementCondition.weightToText(false);
            weightExclude = croisementCondition.isWeightExclude();
        }
    
        return {
            formId: formId,
            modes: modes,
            weight: weight,
            weightExclude: weightExclude
        };
    }
    
    function _getNewCroisementCondition() {
        let modes = $$.one(formId, "modes").value;
        let weightValues = $$.one(formId, "weight").value;
        let weightExclude = $$.one(formId, "weightexclude").checked;
        let croisementCondition = new Fql.Condition.Croisement({
            modes: modes,
            weight: {
                exclude: weightExclude,
                values:  weightValues
            }
        });
        if (croisementCondition.isEmpty()) {
            return null;
        } else {
            return croisementCondition;
        }
    }
};

FqlEditor.Overlay.showPeriodEdit = function (periodCondition, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/periodedit", _getFormContext()),
        afterStart: function (overlayId) {
            let $form = $$(formId);
            Bdf.Deploy.init($form);
            Bdf.Appelant.init($form);
            $$(formId, "button_edit").click(function () {
                let newPeriodCondition = _getNewPeriodCondition();
                Overlay.end(overlayId, function () {
                    callback(newPeriodCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let scopeArray = new Array();
        let onCreation = true;
        let onModification = false;
        let fields = "";
        let start = "";
        let end = "";
        if (periodCondition) {
            onCreation = periodCondition.onCreationDate;
            onModification = periodCondition.onModificationDate;
            fields = periodCondition.fieldArray.join(" ");
            start = periodCondition.start;
            end =periodCondition.end;
            if (end === Fql.DATE_SAME) {
                end = "";
            }
        }
        __addScope("creation", "_ label.selection.period_onchrono_creation", onCreation);
        __addScope("modification", "_ label.selection.period_onchrono_modification", onModification);
        let withFields = (fields.length > 0);
        
        return {
            formId: formId,
            scopeArray: scopeArray,
            withFields: withFields,
            fields: fields,
            start: start,
            end: end
        };
        
        function __addScope(name, inputL10nKey, checked) {
            scopeArray.push({
               name: name,
               l10nKey: inputL10nKey,
               checked: checked
            });
        }
    }
    
    function _getNewPeriodCondition() {
        let newStart = $$.one(formId, "start").value.trim();
        let newEnd = $$.one(formId, "end").value.trim();
        let scope = "";
        if ($$.one(formId, "scope_creation").checked) {
            scope += "creation";
        }
        if ($$.one(formId, "scope_modification").checked) {
            scope += " modification";
        }
        if ($$.one(formId, "scope_fields").checked) {
            scope += " " + $$.one(formId, "input_fields").value;
        }
        if (newStart) {
            if (!newEnd) {
                newEnd = Fql.DATE_SAME;
            }
            return new Fql.Condition.Period({
                start: newStart,
                end: newEnd,
                scope: scope
            });
        } else {
            return null;
        }
    }
};

FqlEditor.Overlay.showRangeEdit = function (rangeCondition, l10nKey, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/rangeedit", _getFormContext()),
        afterStart: function (overlayId) {
            $$(formId, "range").focus();
            $$(formId, "button_edit").click(function () {
                let newRangeCondition = _getNewRangeCondition();
                Overlay.end(overlayId, function () {
                    callback(newRangeCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let rangeText = "";
        let exclude = false;
        if (rangeCondition) {
            rangeText = rangeCondition.toText(false);
            exclude = rangeCondition.exclude;
        }
        
        return {
            formId: formId,
            l10nKey: l10nKey,
            range: rangeText,
            exclude: exclude
        };
    }
    
    function _getNewRangeCondition() {
        let newRangeCondition;
        let range = $$.one(formId, "range").value;
        if (range) {
            newRangeCondition = new Fql.Condition.Range(range);
            if (newRangeCondition.isEmpty()) {
                newRangeCondition = null;
            } else if ($$.one(formId, "exclude").checked) {
                newRangeCondition.exclude = true;
            }
        }
        return newRangeCondition;
    }
};

FqlEditor.Overlay.showStatusEdit = function (statusCondition, l10nKey, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/statusedit", _getFormContext()),
        afterStart: function (overlayId) {
            $$(formId, "status").focus();
            $$(formId, "button_edit").click(function () {
                let newStatusCondition = _getNewStatusCondition();
                Overlay.end(overlayId, function () {
                    callback(newStatusCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let statusText = "";
        if (statusCondition) {
            statusText = statusCondition.toText();
        }
        
        return {
            formId: formId,
            l10nKey: l10nKey,
            status: statusText
        };
    }
    
    function _getNewStatusCondition() {
        let newStatusCondition;
        let status = $$.one(formId, "status").value;
        if (status) {
            newStatusCondition = new Fql.Condition.Status(status);
            if (newStatusCondition.isEmpty()) {
                newStatusCondition = null;
            }
        }
        return newStatusCondition;
    }
};

FqlEditor.Overlay.showUserEdit = function (userCondition, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/useredit", _getFormContext()),
        afterStart: function (overlayId) {
            let $form = $$(formId);
            Bdf.Appelant.init($form);
            $$(formId, "users").focus();
            $$(formId, "button_edit").click(function () {
                let newUserCondition = _getNewUserCondition();
                Overlay.end(overlayId, function () {
                    callback(newUserCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let users = "";
        if (userCondition) {
            users = userCondition.toText();
        }
        return {
            formId: formId,
            users: users
        };
    }
    
    function _getNewUserCondition() {
        let users = $$.one(formId, "users").value.trim();
        if (users.length > 0) {
            return new Fql.Condition.User(users);
        } else {
            return null;
        }
    }
};

FqlEditor.Overlay.showFieldContentEdit = function (fieldContentCondition, callback) {
    var formId = Bdf.generateId();
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/contentedit", _getFormContext()),
        afterStart: function (overlayId) {
            let $form = $$(formId);
            Bdf.Deploy.init($form);
            Bdf.Appelant.init($form);
            $form.submit(function () {
                return false;
            });
            $$(formId, "q1").focus();
            $$(formId, "button_edit").click(function () {
                let newFieldContentCondition = _getNewFieldContentCondition();
                Overlay.end(overlayId, function () {
                    callback(newFieldContentCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let operator = Fql.LOGICALOPERATOR_AND;
        let scope = Fql.SCOPE_TITRE;
        let q = "";
        if (fieldContentCondition) {
            operator = fieldContentCondition.operator;
            scope = fieldContentCondition.scope;
            q = fieldContentCondition.q;
        }
        let scopeArray = new Array();
        __addScope(Fql.SCOPE_TITRE, "_ label.selection.scope_titre");
        __addScope(Fql.SCOPE_ENTETE, "_ label.selection.scope_entete");
        __addScope(Fql.SCOPE_FICHE, "_ label.selection.scope_fiche");
        let selectionChecked = (scope === "selection");
        let fields = "";
        if (selectionChecked) {
            fields = fieldContentCondition.fieldArray.join("; ");
        }
        
        return {
            formId: formId,
            operator: operator,
            q: q,
            scopeArray: scopeArray,
            selectionChecked: selectionChecked,
            fields: fields,
            withFieldSelection: true
        };
        
        function __addScope(name, inputL10nKey) {
            let checked = (name === scope);
            scopeArray.push({
               name: name,
               l10nKey: inputL10nKey,
               checked: checked
            });
        }
    }
    
    function _getNewFieldContentCondition() {
        let q = $$.one(formId, "q1").value.trim();
        let newFieldContentCondition;
        if (q.length === 0) {
            newFieldContentCondition = null;
        } else {
            let $form = $$(formId);
            let operator = $$($form, {_name:"operator", _checked: true}).val();
            let scope = $$($form, {_name:"scope", _checked: true}).val();
            if (scope === Fql.SCOPE_SELECTION) {
                scope = $$.one(formId, "selection_fields").value;
            }
            newFieldContentCondition = new Fql.Condition.FieldContent({
                q: q,
                scope: scope,
                operator: operator
            });
        }
        return newFieldContentCondition;
    }
    
};

FqlEditor.Overlay.showMotcleContentEdit = function (contentCondition, callback) {
    var formId = Bdf.generateId();
    
    FqlEditor.Overlay.start(formId, {
        content: Bdf.render("fqleditor:overlay/contentedit", _getFormContext()),
        afterStart: function (overlayId) {
            $$(formId).submit(function () {
                return false;
            });
            $$(formId, "q1").focus();
            $$(formId, "button_edit").click(function () {
                let newMotcleContentCondition = _getNewMotcleContentCondition();
                Overlay.end(overlayId, function () {
                    callback(newMotcleContentCondition);
                });
            });
        }
    });
    
    function _getFormContext() {
        let operator = Fql.LOGICALOPERATOR_AND;
        let scope = Fql.SCOPE_IDALPHA_ONLY;
        let q = "";
        if (contentCondition) {
            operator = contentCondition.operator;
            scope = contentCondition.scope;
            q = contentCondition.q;
        }
        let scopeArray = new Array();
        __addScope(Fql.SCOPE_IDALPHA_ONLY, "_ label.selection.scope_idalpha_only");
        __addScope(Fql.SCOPE_IDALPHA_WITHOUT, "_ label.selection.scope_idalpha_without");
        __addScope(Fql.SCOPE_IDALPHA_WITH, "_ label.selection.scope_idalpha_with");
        
        return {
            formId: formId,
            operator: operator,
            q: q,
            scopeArray: scopeArray,
            withFieldSelection: false
        };
        
        function __addScope(name, inputL10nKey) {
            let checked = (name === scope);
            scopeArray.push({
               name: name,
               l10nKey: inputL10nKey,
               checked: checked
            });
        }
    }
    
    function _getNewMotcleContentCondition() {
        let q = $$.one(formId, "q1").value.trim();
        let newMotcleContentCondition;
        if (q.length === 0) {
            newMotcleContentCondition = null;
        } else {
            let $form = $$(formId);
            let operator = $$($form, {_name:"operator", _checked: true}).val();
            let scope = $$($form, {_name:"scope", _checked: true}).val();
            newMotcleContentCondition = new Fql.Condition.MotcleContent({
                q: q,
                scope: scope,
                operator: operator
            });
        }
        return newMotcleContentCondition;
    }
    
    
};