/* global Bdf,$$ */

/**
 * Objet global définissant l'espace de nom Menu
 * 
 * @namespace SelectionIndexation
 */
var SelectionIndexation = {};

SelectionIndexation.select = function (name, action, corpusArray) {
    var checkBool;
    if (action === "check") {
        checkBool = true;
    } else if (action === "uncheck") {
        checkBool = false;
    }
    for (let corpusName of corpusArray) {
        $$({_element: "input", _name: name + "_" + corpusName}).each(function (index, element) {
            element.checked = checkBool;
            SelectionIndexation.testCheck(element);
        });
    }
};

SelectionIndexation.testCheck = function (element) {
    var $label = $$({_element: "label", _for: element.id});
    if (element.checked) {
        $label.addClass("selectionindexation-Checked");
    } else {
        $label.removeClass("selectionindexation-Checked");
    }
};


/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    var corpusArray = new Array();
    $$({selectionindexationCorpus: true}).each(function (index, element) {
        corpusArray.push(element.dataset.selectionindexationCorpus);
    });
    $$("selectionindexationToolbar").html(Bdf.render("selectionindexation:toolbar",{}));
    $$({_element: "button", selectionindexationName: true}).click(function () {
        SelectionIndexation.select(this.dataset.selectionindexationName, this.dataset.selectionindexationAction, corpusArray);
    });
    $$({_element: "input", selectionindexationPoids: true}).on("input", function () {
        try {
            let checkbox = $$.one(this.dataset.selectionindexationPoids);
            checkbox.checked = true;
            SelectionIndexation.testCheck(checkbox);
        } catch(e) {
            
        }
    });
    $$({_element: "input", _type: "checkbox"}).change(function () {
        SelectionIndexation.testCheck(this);
    });
});