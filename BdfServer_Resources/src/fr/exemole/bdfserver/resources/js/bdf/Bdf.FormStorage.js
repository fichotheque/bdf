/* global Bdf,$$,HTMLInputElement,HTMLSelectElement,HTMLTextAreaElement */

Bdf.FormStorage = {};

Bdf.FormStorage.memorize = function (form) {
    var formKey = form.dataset.formstorageKey;
    if (!formKey) {
        return;
    }
    var localData = Bdf.getMainLocalStorage();
    if (localData) {
        var formstorageObject;
        if (!localData.hasOwnProperty("formstorage")) {
            formstorageObject = new Object();
            localData["formstorage"] = formstorageObject;
        } else {
            formstorageObject = localData["formstorage"];
        }
        var formArray = new Array();
        for(let element of form.elements) {
            if (element instanceof HTMLInputElement) {
                if ((element.type === "radio") && (element.checked)) {
                    formArray.push({
                        type: "radio",
                        name: element.name,
                        value: element.value
                    });
                } else if (element.type === "checkbox") {
                    formArray.push({
                        type: "checkbox",
                        name: element.name,
                        value: element.value,
                        checked: element.checked
                    });
                } else if (element.type === "text") {
                    if (_include(element)) {
                            formArray.push({
                            type: "text",
                            name: element.name,
                            value: element.value
                        });
                    }
                }
            } else if (element instanceof HTMLSelectElement) {
                let select = {
                    type: "select",
                    name: element.name
                };
                let map = new Object();
                for(let option of element.options) {
                    if (option.selected === true) {
                        map[option.value] = true;
                    }
                }
                select.selection = map;
                formArray.push(select);
            } else if (element instanceof HTMLTextAreaElement) {
                if (_include(element)) {
                    formArray.push({
                        type: "textarea",
                        name: element.name,
                        value: element.value
                    });
                }
            }
        }
        formstorageObject[formKey] = formArray;
        Bdf.putMainLocalStorage(localData);
    }
    
    
    function _include(element) {
        return (element.dataset.formstorage === "include");
    }
};

Bdf.FormStorage.remember = function (form) {
    var formKey = form.dataset.formstorageKey;
    if (!formKey) {
        return;
    }
    var obj;
    var localData = Bdf.getMainLocalStorage();
    if ((localData) && (localData.hasOwnProperty("formstorage"))) {
        if (localData["formstorage"].hasOwnProperty(formKey)) {
            for(let obj of localData["formstorage"][formKey]) {
                if (obj.type === "radio") {
                    $$(form, {_element: "input", _type: "radio", _name: obj.name, _value: obj.value}).prop("checked", true).change();
                } else if (obj.type === "checkbox") {
                    $$(form, {_element: "input", _type: "checkbox", _name: obj.name, _value: obj.value}).prop("checked", obj.checked).change();
                } else if (obj.type === "select") {
                    let $select = $$(form, {_element: "select", _name: obj.name});
                    if ($select.length > 0) {
                        for(let option of $select.get(0).options) {
                            if (obj.selection.hasOwnProperty(option.value)) {
                                option.selected = true;
                            }
                        }
                        $select.change();
                    }
                } else if (obj.type === "textarea") {
                    $$(form, {_element: "textarea", _name: obj.name}).val(obj.value).change();
                } else if (obj.type === "text") {
                    $$(form, {_element: "input", _type: "text", _name: obj.name}).val(obj.value).change();
                }
            }
        }
    }
};

$(function() {
     $("form").each(function (index, element) {
        if (element.dataset.formstorageKey) {
            Bdf.FormStorage.remember(element);
            $(element).submit(function () {
                Bdf.FormStorage.memorize(this);
            });
        }
    });
});

