/* global Bdf */

Bdf.Collections = {};

Bdf.Collections.externalScriptArray = new Array();
Bdf.Collections.scrutariExportArray = new Array();
Bdf.Collections.selectionDefArray = new Array();
Bdf.Collections.sqlExportArray = new Array();
Bdf.Collections.tableExportArray = new Array();

Bdf.Collections.initExternalScriptArray = function (callback) {
    Bdf.Ajax.loadExternalScriptArray(function (externalScriptArray) {
        Bdf.Collections.externalScriptArray = externalScriptArray;
        if (callback) {
            callback();
        }
    });
};

Bdf.Collections.initScrutariExportArray = function (callback) {
    Bdf.Ajax.loadScrutariExportArray(function (scrutariExportArray) {
        Bdf.Collections.scrutariExportArray = scrutariExportArray;
        if (callback) {
            callback();
        }
    });
};

Bdf.Collections.initSelectionDefArray = function (callback) {
    Bdf.Ajax.loadSelectionDefArray(function (selectionDefArray) {
        Bdf.Collections.selectionDefArray = selectionDefArray;
        if (callback) {
            callback();
        }
    });
};

Bdf.Collections.initSqlExportArray = function (callback) {
    Bdf.Ajax.loadSqlExportArray(function (sqlExportArray) {
        Bdf.Collections.sqlExportArray = sqlExportArray;
        if (callback) {
            callback();
        }
    });
};

Bdf.Collections.initTableExportArray = function (callback) {
    Bdf.Ajax.loadTableExportArray(function (tableExportArray) {
        Bdf.Collections.tableExportArray = tableExportArray;
        if (callback) {
            callback();
        }
    });
};
