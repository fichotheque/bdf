/* global Bdf */

Bdf.Ajax = {};

Bdf.Ajax.loadLangConfiguration = function (langConfigurationCallback) {
    $.ajax({
        url: Bdf.URL + "configuration",
        data: {
            json: "langconfiguration"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "langConfiguration", langConfigurationCallback);
        }
    });
};

Bdf.Ajax.loadPathConfiguration = function (pathConfigurationCallback) {
    $.ajax({
        url: Bdf.URL + "configuration",
        data: {
            json: "pathconfiguration"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "pathConfiguration", pathConfigurationCallback);
        }
    });
};

Bdf.Ajax.loadAccessArray = function (accessArrayCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "access-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "accessArray", accessArrayCallback);
        }
    });
};

Bdf.Ajax.loadTableExportArray = function (tableExportArrayCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "tableexport-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "tableExportArray", tableExportArrayCallback);
        }
    });
};

Bdf.Ajax.loadTableExportContent = function (tableExportName, path, tableExportContentCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "tableexport-content",
            tableexport: tableExportName,
            path: path
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "tableExportContent", tableExportContentCallback);
        }
    });
};

Bdf.Ajax.loadAvailablePatterns = function (availablePatternsCallback) {
     $.ajax({
        url: Bdf.URL + "main",
        data: {
            json: "availablepatterns"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "availablePatterns", availablePatternsCallback);
        }
    });
};

Bdf.Ajax.loadSubsetTrees = function (categories, withDetails, subsetTreesCallback) {
     $.ajax({
        url: Bdf.URL + "configuration",
        data: {
            json: "subsettrees",
            categories: categories,
            withdetails: withDetails
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "subsetTrees", subsetTreesCallback);
        }
    });
};

Bdf.Ajax.loadSelectionDefArray = function (selectionDefArrayCallback) {
    $.ajax({
        url: Bdf.URL + "selection",
        data: {
            json: "selectiondef-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "selectionDefArray", selectionDefArrayCallback);
        }
    });
};

Bdf.Ajax.loadExternalScriptArray = function (externalScriptArrayCallback) {
    $.ajax({
        url: Bdf.URL + "configuration",
        data: {
            json: "externalscript-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "externalScriptArray", externalScriptArrayCallback);
        }
    });
};

Bdf.Ajax.loadScrutariExportArray = function (scrutariExportArrayCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "scrutariexport-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "scrutariExportArray", scrutariExportArrayCallback);
        }
    });
};

Bdf.Ajax.loadSqlExportArray = function (sqlExportArrayCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "sqlexport-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "sqlExportArray", sqlExportArrayCallback);
        }
    });
};

Bdf.Ajax.loadRoleArray = function (roleArrayCallback) {
    $.ajax({
        url: Bdf.URL + "administration",
        data: {
            json: "role-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "roleArray", roleArrayCallback);
        }
    });
};

Bdf.Ajax.loadBalayageArray = function (balayageArrayCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "balayage-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "balayageArray", balayageArrayCallback);
        }
    });
};

Bdf.Ajax.loadBalayageContent = function (balayageName, path, balayageContentCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "balayage-content",
            balayage: balayageName,
            path: path
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "balayageContent", balayageContentCallback);
        }
    });
};

Bdf.Ajax.loadTransformationDescription = function (transformationDescriptionCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "transformation-description"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "transformationDescription", transformationDescriptionCallback);
        }
    });
};

Bdf.Ajax.loadTransformationDescriptions = function (keys, transformationDescriptionsCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "transformation-descriptions",
            keys: keys
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "transformationDescriptions", transformationDescriptionsCallback);
        }
    });
};

Bdf.Ajax.loadTemplateDescription = function (templateKey, templateDescriptionCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "transformation-templatedescription",
            template: templateKey
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "templateDescription", templateDescriptionCallback);
        }
    });
};

Bdf.Ajax.loadTemplateContent = function (templateKey, path, templateContentCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "transformation-templatecontent",
            template: templateKey,
            path: path
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "templateContent", templateContentCallback);
        }
    });
};

Bdf.Ajax.loadScrutariExport = function (scrutariExportName, scrutariExportCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "scrutariexport",
            scrutariexport: scrutariExportName
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "scrutariExport", scrutariExportCallback);
        }
    });
};

Bdf.Ajax.loadSqlExport = function (sqlExportName, sqlExportCallback) {
    $.ajax({
        url: Bdf.URL + "exportation",
        data: {
            json: "sqlexport",
            sqlexport: sqlExportName
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "sqlExport", sqlExportCallback);
        }
    });
};

Bdf.Ajax.loadRole = function (roleName, roleCallback) {
    $.ajax({
        url: Bdf.URL + "administration",
        data: {
            json: "role",
            role: roleName
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "role", roleCallback);
        }
    });
};

Bdf.Ajax.loadResourceTree = function (resourceTreeCallback) {
    $.ajax({
        url: Bdf.URL + "administration",
        data: {
            json: "resource-tree"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "resourceTree", resourceTreeCallback);
        }
    });
};

Bdf.Ajax.loadStreamText = function (resourcePath, streamTextCallback) {
    $.ajax({
        url: Bdf.URL + "administration",
        data: {
            json: "streamtext",
            path: resourcePath
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "streamText", streamTextCallback);
        }
    });
};

Bdf.Ajax.loadMementoUnitInfoArray = function (mementoUnitInfoArrayCallback) {
    $.ajax({
        url: Bdf.URL + "misc",
        data: {
            json: "memento-unitinfo-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "mementoUnitInfoArray", mementoUnitInfoArrayCallback);
        }
    });
};

Bdf.Ajax.loadMementoUnit = function (path, mementoUnitCallback) {
    $.ajax({
        url: Bdf.URL + "misc",
        data: {
            json: "memento-unit",
            path: path
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "mementoUnit", mementoUnitCallback);
        }
    });
};

Bdf.Ajax.loadTable = function (tableExportName, subset, tableCallback) {
    $.ajax({
        url: Bdf.URL + "main",
        data: {
           json: "table",
           tableexport: tableExportName,
           subset: subset
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "table", tableCallback);
        }
    });
};

Bdf.Ajax.loadHistory = function (subset, id, historyCallback) {
    $.ajax({
        url: Bdf.URL + "misc",
        data: {
           json: "history",
           subset: subset,
           id: id
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "history", historyCallback);
        }
    });
};

Bdf.Ajax.loadFicheRevisions = function (corpus, id, revisions, ficheRevisionsCallback) {
    $.ajax({
        url: Bdf.URL + "misc",
        data: {
           json: "revisions-fiche",
           corpus: corpus,
           id: id,
           revisions: revisions
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "ficheRevisions", ficheRevisionsCallback);
        }
    });
};

Bdf.Ajax.loadUi = function (corpus, uiCallback) {
    $.ajax({
        url: Bdf.URL + "corpus",
        data: {
           json: "ui",
           corpus: corpus
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "ui", uiCallback);
        }
    });
};

Bdf.Ajax.storeUserPref = function (storeName, storeKey, storeValue) {    
    $.ajax({
        url: Bdf.URL + "sphere",
        data: {
            jsontype: Bdf.jsonDataType,
            cmd: "UserStore",
            name: storeName,
            key: storeKey,
            value: storeValue
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
             
        }
    });
};


/*******************************************************************************
* Initialisation de configuration
*******************************************************************************/

Bdf.Ajax.initLangConfiguration = function (callback) {
    Bdf.Ajax.loadLangConfiguration(function (langConfiguration) {
        Bdf.langConfiguration = langConfiguration;
        if (callback) {
            callback();
        }
    });
};

Bdf.Ajax.initPathConfiguration = function (callback) {
    Bdf.Ajax.loadPathConfiguration(function (pathConfiguration) {
        Bdf.pathConfiguration = pathConfiguration;
        if (callback) {
            callback();
        }
    });
};

