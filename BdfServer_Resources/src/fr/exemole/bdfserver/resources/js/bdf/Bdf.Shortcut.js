/* global Bdf,$$ */

Bdf.Shortcut = {};

/* 
 * Les quatre raccourcis acceptés car n'entrant pas en conflit important avec le navigateur
 */
Bdf.Shortcut.ESCAPE = "Escape";
Bdf.Shortcut.F1 = "F1";
Bdf.Shortcut.F2 = "F2";
Bdf.Shortcut.MOD_S = "mod+s";
Bdf.Shortcut.MAP = new Map();

Bdf.Shortcut.addListener = function (element) {
    $(element).on('keydown.shortcut', function (event) {
        let eventName = Bdf.Shortcut.testEvent(event);
        if (eventName) {
            let eventFunction = Bdf.Shortcut.MAP.get(eventName);
            if (eventFunction) {
                event.preventDefault();
                event.stopPropagation();
                eventFunction(event);
            }
        }
    });
};

Bdf.Shortcut.putEventFunction = function (name, eventFunction) {
    Bdf.Shortcut.MAP.set(name, eventFunction);
};

Bdf.Shortcut.checkElements = function (jqArgument) {
    var $element = $$.convert(jqArgument);
    $$($element, {shortcutKey: true}).each(function () {
        let button = this;
        let shortcut = button.dataset.shortcutKey;
        Bdf.Shortcut.MAP.set(shortcut, function () {
            button.click();
        });
    });
    $$($element, {shortcutRole: "iframe"}).each(function () {
        let iframe = this;
        $(iframe).on("load", function (event) {
            try {
                let iframeWindow = this.contentWindow;
                Bdf.Shortcut.addListener(iframeWindow);
            }  catch(error) {
                Bdf.log(error);
            }
        });
    });
    Bdf.Shortcut.checkTooltip($element);
};

Bdf.Shortcut.checkTooltip = function (jqArgument) {
    var modReplace = Bdf.MAC_PLATFORM ? '⌘' : 'Ctrl';
    var $element = $$.convert(jqArgument);
    $$($element, {shortcutTooltip: true}).each(function () {
        let button = this;
        let shortcutTooltip = " (" + button.dataset.shortcutTooltip.replace("MOD", modReplace) + ")";
        if (button.title) {
            if (button.title.indexOf(shortcutTooltip) === -1) {
                button.title = button.title + shortcutTooltip;
            }
        } else {
            button.title = $(button).text().trim() + shortcutTooltip;
        }
    });
};

Bdf.Shortcut.testEvent = function (keyEvent) {
    switch(keyEvent.key) {
        case "F1":
        case "F2":
        case "Escape":
            if (_withNoModifier()) {
                return keyEvent.key;
            } else {
                return null;
            }
        case "s":
        case "S":
            if (_withMod()) {
                return Bdf.Shortcut.MOD_S;
            } else {
                return null;
            }
        default:
            return null;
            
    }
    
    function _withNoModifier() {
        if ((!keyEvent.shiftKey) && (!keyEvent.altKey) && (!keyEvent.ctrlKey) && (!keyEvent.metaKey)) {
            return true;
        } else {
            return false;
        }
    }
    
    function _withMod() {
        if ((!keyEvent.shiftKey) && (!keyEvent.altKey)) {
            if (Bdf.MAC_PLATFORM) {
                return (keyEvent.metaKey);
            } else {
                return (keyEvent.ctrlKey);
            }
        } else {
            return false;
        }
    }
};

$(function () {
    Bdf.Shortcut.addListener(window);
    Bdf.Shortcut.checkElements("body");
});


