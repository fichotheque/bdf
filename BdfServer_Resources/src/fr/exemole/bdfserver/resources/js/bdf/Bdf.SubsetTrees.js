/* global Bdf */

/**
 * Objet définissant un ensemble de collections ordonnées de façon arborescente
 * 
 * @constructor
 */
Bdf.SubsetTrees = function () {
    this.apiObject = null;
    this.availablePatterns = null;
 /* subsetMap est un tableau associés d'instances d'objets subset
 * où la clé du tableau est celle du corpus ou du thésaurus
 */
    this.subsetMap = new Map();
};

/**
 * 
 * @param {string} category
 * @returns {configuration?json=subsettrees/property}
 */
Bdf.SubsetTrees.prototype.getTree = function (category) {
    return this.apiObject[category];
};

Bdf.SubsetTrees.prototype.toSelectOptions = function (category, withWholeKey, disabledSubsetMap, defaultSelected) {
    var array = new Array();
    var count = 0;
    _convert(array, this.apiObject[category]);
    return {
        array: array,
        count: count
    };
    
    
    function _convert(currentArray, nodeArray) {
        for(let obj of nodeArray) {
            var nodeType = obj.node;
            if (nodeType === "group") {
                let label = obj.indent + obj.title;
                let subArray = new Array();
                _convert(subArray, obj.array);
                currentArray.push({
                    type: "optgroup",
                    label: label,
                    array: subArray
                });
            } else if (nodeType === "subset") {
                let value, label, disabled, selected;
                if (withWholeKey) {
                    value = obj.key;
                } else {
                    value = obj.name;
                }
                if (obj.title) {
                    label = obj.indent + obj.title + " [" + obj.name + "]";
                } else {
                    label = obj.indent + "[" + obj.name + "]";
                }
                if ((disabledSubsetMap) && (disabledSubsetMap.has(obj.key))) {
                    disabled = true;
                } else {
                    disabled = false;
                    count++;
                }
                if ((defaultSelected) && (!disabled) && (value === defaultSelected)) {
                    selected = true;
                }
                currentArray.push({
                    type: "option",
                    value: value,
                    label: label,
                    disabled: disabled,
                    selected: selected
                });
            }
        }
    }

};

/*
 * 
 * @param {string} subsetKeyString
 * @returns {Object}
 */
Bdf.SubsetTrees.prototype.getSubset = function (subsetKey) {
    return this.subsetMap.get(subsetKey);
};

/*
 * 
 * @param {string} name
 * @returns {Object}
 */
Bdf.SubsetTrees.prototype.getCorpus = function (name) {
    return this.subsetMap.get("corpus_" + name);
};

/*
 * 
 * @param {string} name
 * @returns {Object}
 */
Bdf.SubsetTrees.prototype.getThesaurus = function (name) {
    return this.subsetMap.get("thesaurus_" + name);
};

/*
 * 
 * @param {string} name
 * @returns {Object}
 */
Bdf.SubsetTrees.prototype.getAlbum = function (name) {
    return this.subsetMap.get("album_" + name);
};

/*
 * 
 * @param {string} name
 * @returns {Object}
 */
Bdf.SubsetTrees.prototype.getAddenda = function (name) {
    return this.subsetMap.get("addenda_" + name);
};

/*
 * 
 * @param {string} name
 * @returns {Object}
 */
Bdf.SubsetTrees.prototype.getSphere = function (name) {
    return this.subsetMap.get("sphere_" + name);
};

/**
 * 
 * @param {string} patternType
 * @returns {main?json=availablepatterns}
 */
Bdf.SubsetTrees.prototype.getPatternArray = function (patternType) {
    if (this.availablePatterns.hasOwnProperty(patternType)) {
        return this.availablePatterns[patternType];
    } else {
        return null;
    }
};

Bdf.SubsetTrees.load = function (callback) {
    var subsetTrees = new Bdf.SubsetTrees();
    Bdf.Ajax.loadAvailablePatterns(function (availablePatterns) {
        subsetTrees.availablePatterns = availablePatterns;
        Bdf.Ajax.loadSubsetTrees("corpus,thesaurus,album,addenda,sphere", 1, function (subsetTreesApiObject) {
            subsetTrees.apiObject = subsetTreesApiObject;
            _complete("corpus", subsetTreesApiObject.corpus, "");
            _complete("thesaurus", subsetTreesApiObject.thesaurus, "");
            _complete("album", subsetTreesApiObject.album, "");
            _complete("addenda", subsetTreesApiObject.addenda, "");
            _complete("sphere", subsetTreesApiObject.sphere, "");
            callback(subsetTrees);
        });
    });
    
    function _complete(category, nodeArray, indent) {
        for(let obj of nodeArray) {
            obj.indent = indent;
            let nodeType = obj.node;
            if (nodeType === "subset") {
                _completeSubset(obj, category);
                subsetTrees.subsetMap.set(obj.key, obj);
            } else if (nodeType === "group") {
                _complete(category, obj.array, indent + "\u00A0 \u00A0 ");
            }
        }
    }
    
    function _completeSubset(subsetObj, category) {
        subsetObj.category = category;
        subsetObj.generatedId = Bdf.generateId();
        for(let detailName in subsetObj.details) {
            let detailArray = subsetObj.details[detailName];
            for(let detail of detailArray) {
                detail.patternArray = subsetTrees.getPatternArray(detail.patternType);
                detail.withPatterns = ((detail.patternArray) && (detail.patternArray.length > 0));
                detail.generatedId = Bdf.generateId();
                detail.cmSpanArray = Bdf.parseCmSpanArray(detail.key);
            }
        }
    }

};
