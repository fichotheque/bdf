/* global Bdf,$$ */

Bdf.Subsetchange = {};

Bdf.Subsetchange.init = function($element) {
  $$($element, {subsetchangeRole: "select"}).change(function () {
    var name = this.name;
    var option = this.options[this.selectedIndex];
    var url = "";
    if (name === "corpus") {
        url = "corpus?corpus=";
    }
    else if (name === "thesaurus") {
        url = "thesaurus?thesaurus=";
    }
    else if (name === "sphere") {
        url = "sphere?sphere=";
    } else if (name === "album") {
        url = "album?album=";
    } else if (name === "addenda") {
        url = "addenda?addenda=";
    }
    url = url + option.value;
    var page = this.dataset.subsetchangePage;
    if ((page) && (page.length > 0)) {
        url = url + "&page=" + page;
    }
    window.location = url;
  });
};

$(function () {
   Bdf.Subsetchange.init($("body"));
});