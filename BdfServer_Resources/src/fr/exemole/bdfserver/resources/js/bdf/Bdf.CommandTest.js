/* global Bdf */

Bdf.CommandTest = {};

$(function () {
    $("form[data-submit-process=test]").submit(function(event) {
        var form = this;
        Bdf.CommandWait.standby(form, true);
        if (Bdf.CodeMirrorMode) {
            Bdf.CodeMirrorMode.beforeSerialize(form);
        }
        event.preventDefault();
        $(form).ajaxSubmit({
            filtering: function(el, index) {
                if ((!el.type) || (!el.type.toLowerCase) || (el.type.toLowerCase() !== 'file')) {
                    return el;
                }
            },
            dataType: "json",
            data: {
                jsontype: "json",
            	test: "true"
            },
            success: function (data) {
                let success = Bdf.checkError(data);
                if (success) {
                    form.submit();
                } else {
                    Bdf.CommandWait.standby(form, false);
                }
            }
        });
    });
});
