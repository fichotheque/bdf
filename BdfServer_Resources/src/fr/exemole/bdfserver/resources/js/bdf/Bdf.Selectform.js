/* global Bdf,$$ */

Bdf.Selectform = {};

Bdf.Selectform.JsManagerInclude = ["_ @title_thesaurus@"];

Bdf.Selectform.init = function ($element) {
    $$($element,{selectformRole: "thesaurus-select"}).change(function () {
         _checkSubsets(this.name);
    });
    $$($element, {selectformRole: "thesaurus-checkbox"}).click(function () {
         _checkSubsets(this.name);
    });
    $$($element, {selectformRole: "detail-radio"}).click(function () {
        var $this = $(this);
        var target = $this.attr("data-selectform-target");
        var $target = $(target);
        if (target.indexOf("thesaurusList_") !== -1) {
            var name = this.name;
            var suffix = name.substring(name.lastIndexOf("_") + 1);
            var all = this.checked;
            if (this.value !== "1") all = !all;
            if (all) {
                Bdf.Selectform.updateAppelant(suffix, new Array());
            }
            else {
                var subsets = Bdf.Selectform.getSubsets(suffix);
                Bdf.Selectform.updateAppelant(suffix, subsets);
            }
        }
        var display = this.checked;
        if (this.value === "1") display = !display;
        if (display) {
            $target.show();
        }
        else {
            $target.hide();
        }
    });
    $$($element, {selectformRole: "date"}).each(function (index, element) {
       Bdf.replaceWithDateInputs(element); 
    });
    
    function _checkSubsets(name) {
        var suffix = name.substring(name.lastIndexOf("_") + 1);
        var subsets = Bdf.Selectform.getSubsets(suffix);
        if (subsets.length === 0) {
            alert(Bdf.Loc.get("_ error.empty.thesaurusselection"));
        }
        Bdf.Selectform.updateAppelant(suffix, subsets);
    }
};

Bdf.Selectform.updateAppelant = function (suffix, subsets) {
    var appelant = $$.one("motcleInput", suffix);
    var options = appelant.dataset.options;
    options.subsets = subsets;
    var newTitle = Bdf.Appelant.getTitle(appelant.dataset.type, options);
    $$(appelant.dataset.icon).attr("title", newTitle);
};

Bdf.Selectform.getSubsets = function (suffix) {
    var motcleSelection = document.getElementsByName("motcle_selection_" + suffix);
    var subsets = new Array();
    for(let i = 0; i < motcleSelection.length;i++) {
        var element = motcleSelection[i];
        if (element.options) {
            for(var j = 0; j < element.options.length;j++) {
                var option = element.options[j];
                if (option.selected) {
                    subsets.push(option.value);
                }
            }
        }
        else {
            if (element.checked) {
                subsets.push(element.value);
            }
        }
    }
    return subsets;
};

Bdf.Selectform.addNewSelection = function () {
    var $motcleLogicalOperator = $$("motcleLogicalOperator");
    $motcleLogicalOperator.removeClass("hidden");
    var $motcleFieldsets = $$("motcleFieldsets");
    var newFieldSetHtml = $motcleFieldsets.attr("data-selectform-new-fieldset");
    var suffixMax = 0;
    $motcleFieldsets.children("fieldset").each(function () {
        if (this.id) {
            var idx = this.id.lastIndexOf("_");
            suffixMax = Math.max(suffixMax, this.id.substring(idx + 1));
        }
    });
    newFieldSetHtml = newFieldSetHtml.replace(/nnn/g, suffixMax + 1);
    var $newFieldSet = $(newFieldSetHtml).insertBefore($motcleLogicalOperator);
    Bdf.Deploy.init($newFieldSet);
    Bdf.Appelant.init($newFieldSet);    
    Bdf.Selectform.init($newFieldSet);
};



$(function () {
    Bdf.initTemplates();
    Bdf.Selectform.init($("body"));
    var newMotcleConditionId = Bdf.generateId();
    $$("motcleFieldsets").append(Bdf.render("selectform:newmotclecondition", {
        buttonId: newMotcleConditionId
    }));
    $$(newMotcleConditionId).click(function () {
        Bdf.Selectform.addNewSelection();
        return false;
    });
    Bdf.Deploy.addVisibilityListener(function (element, visible) {
        if ((element.id) && (element.id.indexOf("thesaurusList_") !== -1)) {
            var suffix = element.id.substring(element.id.lastIndexOf("_") + 1);
            if (!visible) {
                Bdf.Selectform.updateAppelant(suffix, new Array());
            }
            else {
                var subsets = Bdf.Selectform.getSubsets(suffix);
                Bdf.Selectform.updateAppelant(suffix, subsets);
            }
        }
    });
});
