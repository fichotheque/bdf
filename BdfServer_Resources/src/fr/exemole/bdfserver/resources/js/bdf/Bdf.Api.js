/* global Bdf, Fapi */

Bdf.Api = {};

Bdf.api = null;

Bdf.Api.init = function () {
    if (!Bdf.api) {
        Bdf.api = new Fapi(Bdf.URL);
    }
};