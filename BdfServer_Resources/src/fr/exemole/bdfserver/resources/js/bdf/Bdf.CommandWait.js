/* global Bdf,$$ */

Bdf.CommandWait = {};

Bdf.CommandWait.standby = function (form, start) {
    _standbyButton(form);
    form.classList.toggle("global-Wait", start);
    $$(form, {_element: "button", _type: "submit"}).each(function (index, element) {
        _standbyButton(element);
    });
    let id = form.getAttribute("id"); // form.id can be overloaded by <input name="id">
    if (id) {
        $$({_element: "button", _form: id}).each(function (index, element) {
             _standbyButton(element);
        });
    }
    
    function _standbyButton(button) {
        button.classList.toggle("global-button-Wait", start);
        button.disabled = start;
    }
};

$(function () {
    $("form[data-submit-process=wait]").submit(function() {
        Bdf.CommandWait.standby(this, true);
        return true;
    });
});
