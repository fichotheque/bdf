/* global Bdf,$$ */

Bdf.Appelant = {};

Bdf.Appelant.JsManagerInclude = ["_ @title_thesaurus@"];

Bdf.Appelant.init = function (jqArgument) {
    var $element = $$.convert(jqArgument);
    $$($element, {appelantRole: "input"}).each(function () {
        var appelant = this;
        var $appelant = $(appelant);
        var id = $appelant.attr("id");
        if (!id) {
            return;
        }
        var type = appelant.dataset.appelantType;
        if (!_isValidType(type)) {
            return;
        }
        if ($appelant.data("appelantInitialized")) {
            return;
        } else {
            $appelant.data("appelantInitialized", true);
        }
        $appelant.data("type", type);
        var options = {};
        let limit = appelant.dataset.appelantLimit;
        if (limit) {
            options.limit = limit;
        }
        let subsets = appelant.dataset.appelantSubsets;
        if (subsets) {
            options.subsets = subsets;
        }
        if (type === 'user') {
            let sphere = appelant.dataset.appelantSphere;
            if (sphere) {
                options.defaultsphere = "sphere_" + sphere;
            }
        } else if (type === 'motcle') {
            let wanted = appelant.dataset.appelantWanted;
            if (wanted) {
                options.wanted = wanted;
            }
        } else if (type === 'field') {
            let fieldtype = appelant.dataset.appelantFieldtype;
            if (fieldtype) {
                options.fieldtype = fieldtype;
            }
        }
        $appelant.data("options", options);
        var iconId = Bdf.generateId();
        var title = Bdf.Appelant.getTitle(type, options);
        var $anchor = $appelant;
        var anchorId = appelant.dataset.appelantAnchor;
        if (anchorId) {
            $anchor = $$(anchorId);
            if ($anchor.length === 0) {
                $anchor = $appelant;
            }
        }
        $("<button/>", {
            type: "button",
            "class": "global-button-Transparent global-button-Transparent32 " + _getActionClass(type),
            id: iconId,
            title: title,
            click: function () {
                Bdf.Appelant.pioche(id, type, options);
            }
        }).html("<span class='global-button-Icon'></span>").insertAfter($anchor);
        $appelant.data("icon", iconId);
    });
    $$($element, {appelantRole: "subset-select"}).change(function () {
        var subsets = new Array();
        subsets.push(this.options[this.selectedIndex].value);
        $(this.dataset.appelantTarget).each(function () {
            var $this = $(this);
            var options = $this.data("options");
            options.subsets = subsets;
            var newTitle = Bdf.Appelant.getTitle($this.data("type"), options);
            $$($this.data("icon")).attr("title", newTitle);
        });
    });
    
    function _isValidType(type) {
        switch(type) {
            case "user":
            case "redacteur":
            case "motcle":
            case "field":
            case "fiche":
                return true;
            default :
                return false;
        }
    }
    
    function _getActionClass(type) {
        switch(type) {
            case "user":
            case "redacteur":
                return "action-pioche-Redacteur";
            case "motcle":
                return "action-pioche-Motcle";
            case "field":
                return "action-pioche-Field";
            case "fiche":
                return "action-pioche-Fiche";
        }
    }
};

Bdf.Appelant.pioche = function (appelantId, piocheType, options) {
    var dialogUrl = "pioche?page=" + _getPiochePath(piocheType) + "&appelant=" + appelantId;
    if (options.hasOwnProperty("limit")) {
        dialogUrl += "&limit=" + options.limit;
    }
    if (options.hasOwnProperty("defaultsphere")) {
        dialogUrl += "&defaultsphere=" + options.defaultsphere;
    }
    if (options.hasOwnProperty("fieldtype")) {
        dialogUrl += "&fieldtype=" + options.fieldtype;
    }
    if (options.hasOwnProperty("wanted")) {
        dialogUrl += "&wanted=" + options.wanted;
    }
    dialogUrl += "&subsets=";
    if (options.hasOwnProperty("subsets")) {
        dialogUrl += options.subsets;
    }
    var dialogName = "pioche_" + appelantId;
    var width = 430;
    var height = 510;
    var w = window.open(dialogUrl, dialogName, "resizable=yes,width=" + width + ",height=" + height + ",scrollbars");
    w.focus();
    
    function _getPiochePath(type) {
        switch(type) {
            case "user":
            case "redacteur":
                return "redacteur";
            case "motcle":
                return "motcle";
            case "field":
                return "field";
            case "fiche":
                return "fiche";
        }
    }
};

Bdf.Appelant.getTitle = function (type, options) {
    var messageKey = _getMessageKey(type, options);
    var title = Bdf.Loc.get(messageKey);
    if ((options.hasOwnProperty("subsets")) && (options.subsets.length === 1)) {
        var remplacement = Bdf.Loc.get("subset:thesaurus_" + options.subsets[0]);
        var idx = title.indexOf("{0}");
        if (idx !== -1) {
            title = title.substring(0, idx) + remplacement + title.substring(idx + 3);
        }
    }
    return title;
    
    function _getMessageKey(type, options) {
         switch(type) {
            case "user":
            case "redacteur":
                return "_ link.pioche.redacteur";
            case "motcle":
                if ((!options.hasOwnProperty("subsets")) || (options.subsets.length === 0)) {
                    return "_ link.pioche.motcle_all";
                } else if (options.subsets.length === 1) {
                    return "_ link.pioche.motcle_one";
                } else {
                    return "_ link.pioche.motcle_selection";
                }
            case "field":
                if ((options.fieldtype) && (options.fieldtype === "datation")) {
                    return "_ link.pioche.field_datation";
                } else {
                    return "_ link.pioche.field";
                }
            case "fiche":
                if ((!options.hasOwnProperty("subsets")) || (options.subsets.length === 0)) {
                    return "_ link.pioche.fiche_all";
                } else if (options.subsets.length === 1) {
                    return "_ link.pioche.fiche_one";
                } else {
                    return "_ link.pioche.fiche_selection";
                }
        }
    }
};


$(function () {
    Bdf.Appelant.init("body");
});