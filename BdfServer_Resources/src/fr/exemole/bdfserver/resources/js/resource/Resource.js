/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom Resource
 * 
 * @namespace Resource
 */
var Resource = {};

Resource.originsToString = function (obj) {
    if (!obj.origins) {
        return "";
    }
    return obj.origins.join(" ");
};

Resource.getMatchingClass = function (origins) {
    if (origins === "dist") {
        return "resource-origin-Dist";
    }
    if (origins.indexOf("var") > -1) {
        if (origins.indexOf("etc") > -1) {
            return "resource-origin-VarOver";
        } else if (origins.indexOf("dist") > -1) {
            return "resource-origin-VarOver";
        } else {
            return "resource-origin-VarOnly";
        }
    } else if (origins.indexOf("etc") > -1) {
        return "resource-origin-Etc";
    }
    return "resource-origin-Unknown";
};


/******************************************************************************
 * Rajout de champ
 ******************************************************************************/

Resource.completeFolder = function (folderObj, parentPath) {
    if (!parentPath) {
        parentPath = "";
    }
    var originsString = Resource.originsToString(folderObj);
    folderObj.parentPath = parentPath;
    folderObj.path = parentPath + folderObj.name;
    folderObj.generatedId = Bdf.generateId();
    folderObj.originsString = originsString;
    folderObj.originsClass = Resource.getMatchingClass(originsString);
    Resource.completeFolderArrays(folderObj, folderObj.path + "/");
    return folderObj;
};

Resource.completeFolderArrays = function (folderObj, parentPath) {
    if (!parentPath) {
        parentPath = "";
    }
    for (let subfolder of folderObj.subfolderArray) {
        Resource.completeFolder(subfolder, parentPath);
    }
    for (let resource of folderObj.resourceArray) {
        Resource.completeResource(resource, parentPath);
    }
    return folderObj;
};

Resource.completeResource = function (resourceObj, parentPath) {
    var originsString = Resource.originsToString(resourceObj);
    resourceObj.parentPath = parentPath;
    resourceObj.path = parentPath + resourceObj.name;
    resourceObj.originsString = originsString;
    resourceObj.originsClass = Resource.getMatchingClass(originsString);
    return resourceObj;
};


/******************************************************************************
 * Enregistrement de fonction
 ******************************************************************************/

Pane.registerCheckFunction(function (state) {
    $(".pane-list-ActiveItem").removeClass("pane-list-ActiveItem");
    if (state.withMain()) {
        let $resource = $$("layout_list", {role: "content-reference", path: state.mainContentKey});
        $resource.addClass("pane-list-ActiveItem");
        let $parentFolders = $resource.parents($$.toCssSelector({role: "folder"}));
        $parentFolders.children($$.toCssSelector({role: "folder-title"})).addClass("pane-list-ActiveItem");
    }
});


/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    $$("layout_list").html(Bdf.render("resource:list/pane", {}));
    Bdf.initBeforeUnload({
        unloadKey: "_ warning.resource.unsaved"
    });
    Bdf.runSerialFunctions(
        Resource.List.init
   );
   Pane.initShortcuts();
});