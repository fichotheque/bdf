/* global Fql,Fapi,Bdf,$$,Overlay,Index,Ficheframe */

Index.Search = {};

Index.Search.SORT_ARRAY = [
	{
		name: "creationdate-desc",
		locKey: "_ label.selection.sorttype_creationdate"
	},
	{
		name: "titre-asc",
		locKey: "_ label.selection.sorttype_titre"
	},
	{
		name: "id-asc",
		locKey: "_ label.selection.sorttype_id"
	}
];
Index.Search.SCOPE_ARRAY = [
	{
		name: "titre",
		locKey: "_ label.selection.scope_titre"
	},
	{
		name: "entete",
		locKey: "_ label.selection.scope_entete"
	},
	{
		name: "fiche",
		locKey: "_ label.selection.scope_fiche"
	}
];

Index.Search.initSubmit = function (formId, fichothequeScope) {
    $$(formId).submit(function(event) {
        event.preventDefault();
        let q = this["q"].value;
        if (q) {
            q = q.trim();
        }
        if (q) {
            let scopeSelect = this["scope"];
            let contentScope = scopeSelect.options[scopeSelect.selectedIndex].value;
            let sortSelect = this["sort"];
            let sortType = sortSelect.options[sortSelect.selectedIndex].value;
            Index.Search.show(q, contentScope, sortType, fichothequeScope);
        }
    });
};

Index.Search.show = function (q, contentScope, sortType, fichothequeScope) {
    var formId = Bdf.generateId();
    var targetArray = new Array();
    if (fichothequeScope) {
        _initScopeTargets();
    } else {
        _initDefaultTargets();
    }
    var max = targetArray.length;
    var count = 0;
    var contentScopeArray = Index.toOptionArray(Index.Search.SCOPE_ARRAY, contentScope);
    var sortTypeArray = Index.toOptionArray(Index.Search.SORT_ARRAY, sortType);
    Overlay.start({
        content: Bdf.render("index:overlay/search", {
            q: q,
            formId: formId,
            targetArray: targetArray,
            contentScopeArray: contentScopeArray,
            sortTypeArray: sortTypeArray            
        }),
        isWaiting: true,
        supplementaryClasses: {
            content: "index-search-OverlayContent"
        },
        afterStart: function (overlayId, waitingCallback) {
            $$(formId, "search").submit(function(event) {
                event.preventDefault();
                let newQ = this["q"].value;
                if (newQ) {
                        newQ = newQ.trim();
                }
                if (newQ) {
                    count = 0;
                    q = newQ;
                    let scopeSelect = this["scope"];
                    contentScope = scopeSelect.options[scopeSelect.selectedIndex].value;
                    let sortSelect = this["sort"];
                    sortType = sortSelect.options[sortSelect.selectedIndex].value;
                    $$(formId, "button_submit").prop("disabled", true);
                    for(let target of targetArray) {
                        $$(formId, "target", target.name).html("");
                        $$(formId, "fichecount", target.name).text("");
                       _search(target, waitingCallback);
                    }
                }
            });
            for(let target of targetArray) {
                _search(target, waitingCallback);
            }
            $$(formId, "iframe").on("load", function (event) {
                let iframe = this;
                Ficheframe.testToolbar({
                        iframe: iframe,
                        toolbarId: formId + "_iframe_toolbar",
                        fieldeditId: formId + "_iframe_fieldedit"
                });
                let locationCheck = Bdf.checkIframeLocation(iframe);
                if (locationCheck.withBorder) {
                    iframe.classList.add("index-search-WithBorder");
                } else {
                    iframe.classList.remove("index-search-WithBorder");
                }
            });
        }
    });
    
    function _search(target, waitingCallback) {
        var ficheQueryArray = new Array();
        for(let query of target.queries) {
            let fq = Object.assign({
                content: {
                    scope: contentScope,
                    q: q,
                    type: "simple"
                }
            }, query);
            ficheQueryArray.push(new Fql.FicheQuery(fq));
        }
        Fapi.Ajax.loadFiches(new Fapi(target.name + "/"), ficheQueryArray, {
            callback: function (data) {
                count++;
                if (count === max) {
					$$(formId, "button_submit").prop("disabled", false);
                    waitingCallback();
                }
                $$(formId, "fichecount", target.name).text("(" + data.fiches.count + ")");
                let $target = $$(formId, "target", target.name);
                $target.html(Bdf.render("index:search-results", {
                    name: target.name,
                    fiches: data.fiches
                }));
                $$($target, {role: "fiche"}).click(_showFiche);
            },
            requestParameters: {
                sort: sortType
            }
        });
    }    

    function _showFiche() {
        let link = this;
        $$(formId, "button_blank").attr("href", link.href).removeClass("hidden");
        $$(formId, "button_odt").attr("href", link.dataset.fichotheque + "/fiches/" + link.dataset.corpus + "-" + link.dataset.id + ".odt").removeClass("hidden");
        if (link.dataset.editable) {
            $$(formId, "button_edit").attr("href", link.dataset.fichotheque + "/edition?page=fiche-change&page-result-options=standalone&corpus=" + link.dataset.corpus + "&id=" + link.dataset.id).removeClass("hidden");
        } else {
            $$(formId, "button_edit").addClass("hidden");
        }
    }
    
    function _initDefaultTargets() {
        for(let fichotheque of Index.FICHOTHEQUE_MAP.values()) {
            let title = fichotheque.phrases.long;
            if (!title) {
                title = fichotheque.title;
            }
            targetArray.push({
                name: fichotheque.name,
                title: title,
                queries: [{}],
                fichotheque: fichotheque
            });
        }
    }
    
    function _initScopeTargets() {
        for(let scope of fichothequeScope) {
            let fichotheque = Index.FICHOTHEQUE_MAP.get(scope.name);
            if (fichotheque) {
                let title = fichotheque.phrases.long;
                if (!title) {
                    title = scope.name;
                }
                targetArray.push({
                    name: scope.name,
                    title: title,
                    queries: scope.queries,
                    fichotheque: fichotheque
                });
            }
        }
    }
    
};
