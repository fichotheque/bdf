/* global Bdf,Fql,Fapi,$$,Overlay,Index */

Index.Client = {};

Index.Client.ARGS = {
    sphereName: "",
    title: "",
    withLogo: false,
    centralUser: {
        sphere: "",
        login: "",
        name: "",
        lang: "",
        locale: ""
    }
};

Index.Client.init = function (args) {
    Index.URL = "index-" + args.sphereName;
    var searchFormId = Bdf.generateId();
    $$("layout").html(Bdf.render("index:layout", {
        title: args.title,
        withLogo: args.withLogo,
        centralUser: args.centralUser,
        contentScopeArray: Index.toOptionArray(Index.Search.SCOPE_ARRAY, "titre"),
	sortTypeArray: Index.toOptionArray(Index.Search.SORT_ARRAY, "creationdate-desc"),
        formId: searchFormId
    }));
    Index.Search.initSubmit(searchFormId);
    Index.initExitButton("button_exit");
    Index.loadPresence(function () {
        let array = new Array();
        for(let fichotheque of Index.FICHOTHEQUE_MAP.values()) {
            array.push(toLink(fichotheque));
        }
        $$("presence").html(Bdf.render("index:presence", {
            list: array
        }));
    });
    
    function toLink(fichothequeInfo) {
        var text;
        if (fichothequeInfo.title) {
            text = fichothequeInfo.title;
        } else {
            text = fichothequeInfo.name;
        }
        var title = "";
        if (fichothequeInfo.phrases.long) {
            title = fichothequeInfo.phrases.long;
        }
        return {
            name: fichothequeInfo.name,
            href: fichothequeInfo.name + "/session",
            text: text,
            title: title,
            init: fichothequeInfo.init
        };
    }
    
};

$(function(){
    Bdf.initTemplates();
    Index.Client.init(Index.Client.ARGS);
});
