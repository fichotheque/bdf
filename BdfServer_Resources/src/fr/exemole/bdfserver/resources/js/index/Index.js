/* global Bdf,$$,Overlay */
/**
 * Objet global définissant l'espace de nom Multi
 * 
 * @namespace Index
 */
var Index = {};

Index.URL = "";

Index.FICHOTHEQUE_MAP = new Map();

Index.initExitButton = function (buttonId) {
    $$(buttonId).click(function () {
        $.ajax({
            url: Index.URL,
            data: {
               cmd: "exit",
               json: "ping"
            },
            xhrFields: {
               withCredentials: true
            },
            dataType: Bdf.jsonDataType,
            success: function (data, textStatus) {
                window.location.reload();
            }
        });
    });
};

Index.toOptionArray = function (array, selectedValue) {
    let optionArray = new Array();
    for(let option of array) {
        let selected = (option.name === selectedValue);
        optionArray.push({
                value: option.name,
                locKey: option.locKey,
                selected: selected
        });
    }
    return optionArray;
};

Index.loadPresence = function (callback) {
    $.ajax({
        url: Index.URL,
        data: {
           json: "presence"
        },
        xhrFields: {
           withCredentials: true
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            Bdf.checkData(data, "presenceMap", function (presenceMap) {
                _listPresence(presenceMap);
            });
        }
    });
    
    function _listPresence(presenceMap) {
        var array = new Array();
        for(let prop in presenceMap) {
            let fichotheque = presenceMap[prop];
            Index.FICHOTHEQUE_MAP.set(prop, fichotheque);
        }
        if (callback) {
            callback();
        }
    }
};




