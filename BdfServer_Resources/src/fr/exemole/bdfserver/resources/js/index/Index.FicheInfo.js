/* global Bdf,$$,Overlay,Index,Ficheframe */

Index.FicheInfo = function (fichotheque, corpus, id, editable, options) {
    this.fichotheque = fichotheque;
    this.corpus = corpus;
    this.id = id;
    this.editable = editable;
    this.htmlTemplate = "";
    this.odtTemplate = "";
    this.resultToolbar = "";
    if (options) {
        if (options.htmlTemplate) {
            this.htmlTemplate = options.htmlTemplate;
        }
        if (options.odtTemplate) {
            this.odtTemplate = options.odtTemplate;
        }
        if (options.resultToolbar) {
            this.resultToolbar = options.resultToolbar;
        }
    }
};

Index.FicheInfo.prototype.getIframeHref = function () {
    var href = `${this.fichotheque}/fiches/${this.corpus}-` + this.id;
    if (this.htmlTemplate) {
        href = href + "-" + this.htmlTemplate;
    }
    return href + ".html";
};

Index.FicheInfo.prototype.getBlankHref = function () {
    var href = `${this.fichotheque}/main-iframes?fiche=${this.corpus}-${this.id}`;
    if (this.htmlTemplate) {
        href = href + "-" + this.htmlTemplate;
    }
    return href;
};


Index.FicheInfo.prototype.getOdtHref = function () {
    var href = `${this.fichotheque}/fiches/${this.corpus}-${this.id}`;
    if (this.odtTemplate) {
        href = href + "-" + this.odtTemplate;
    }
    return href + ".odt";
};

Index.FicheInfo.prototype.getEditHref = function () {
    if (!this.editable) {
        return null;
    }
     var href =  `${this.fichotheque}/edition?page=fiche-change&page-result-options=${encodeURIComponent(this.resultToolbar)}&corpus=${this.corpus}&id=${this.id}`;
     return href;
};

Index.FicheInfo.prototype.toPopupSettings = function (defaultSettings) {
    if (!defaultSettings) {
        defaultSettings = {};
    }
    var iframeCallback = null;
    if (Ficheframe) {
        iframeCallback = Ficheframe.fichePopupIframeCallback;
    }
    return Object.assign({}, defaultSettings, {
        iframeHref: this.getIframeHref(),
        blankHref: this.getBlankHref(),
        odtHref: this.getOdtHref(),
        editHref: this.getEditHref(),
        iframeCallback: iframeCallback
    });
};
