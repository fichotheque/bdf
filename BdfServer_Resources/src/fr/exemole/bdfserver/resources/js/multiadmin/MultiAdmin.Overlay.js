/* global Bdf,$$,MultiAdmin,Overlay */

MultiAdmin.Overlay = {};

MultiAdmin.Overlay.showCreationForm = function () {
    var genId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ title.multi.newfichotheque"),
        content: Bdf.render("multiadmin:overlay/creationform", {
            genId: genId,
            availableFichothequeArray: _getAvailableFichotheArray(),
            availableCentralSphereArray: _getAvailableSphereArray()
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.multi.fichothequecreation"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-admin",
            method: "POST"
        },
        afterStart: function (overlayId) {
            Bdf.Deploy.initOverlay(overlayId);
            $$(overlayId, "dialog", {_element: "input", _name: "sphereorigin"}).change(function () {
                let required = (this.value === "creation");
                $$(genId, "creation_panel", {_element: "input"}).prop("required", required);
            });
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                if (!data.authentified) {
                    Overlay.end($form, function () {
                        MultiAdmin.testAuthentification(data);
                    });
                } else if (Bdf.checkError(data)) {
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        MultiAdmin.List.update(data.fichothequeArray);
                    });
                }
            }
        }
    });
    
    
    function _getAvailableFichotheArray() {
        let result = new Array();
        if (MultiAdmin.fichothequeArray) {
            for(let fichotheque of  MultiAdmin.fichothequeArray) {
                if (fichotheque.init) {
                    result.push(fichotheque);
                }
            }
        }
        if (result.length === 0) {
            return null;
        } else {
            return result;
        }
    }
    
    function _getAvailableSphereArray() {
        let result = new Array();
        return null;
    }
    
};

MultiAdmin.Overlay.showDuplicationForm = function (fichothequeName) {
    if (!MultiAdmin.fichothequeArray) {
        Bdf.log("MultiAdmin.fichothequeArray is not initialized");
        return;
    }
    var fichotheque;
    var genId = Bdf.generateId();
    for(let existing of  MultiAdmin.fichothequeArray) {
        if (existing.name === fichothequeName) {
            fichotheque = existing;
            break;
        }
    }
    if ((!fichotheque) || (!fichotheque.init)) {
        return;
    }
    _completeFichotheque();
    Overlay.start({
        header: Bdf.Loc.escape("_ title.multi.duplicatefichotheque"),
        content: Bdf.render("multiadmin:overlay/duplicationform", {
            source: fichotheque,
            genId: genId
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.multi.fichothequeduplication"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-admin",
            method: "POST"
        },
         ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                if (!data.authentified) {
                    Overlay.end($form, function () {
                        MultiAdmin.testAuthentification(data)
                    });
                } else if (Bdf.checkError(data)) {
                    Overlay.end($form, function () {
                        MultiAdmin.List.update(data.fichothequeArray, function () {
                            Bdf.showCommandMessage(data.commandMessage);
                        });
                    });
                }
            }
        },
        afterStart: function (overlayId) {
            var $dialog = $$(overlayId, "dialog");
            $$($dialog, {_element: "input", _name: "subsets"}).click(function () {
                var id = this.id;
                var $label = $$({_element: "label", _for: id});
                var key = this.value;
                var $block = $$({role: "block-items", key: key});
                if (this.checked) {
                    $label.removeClass("multi-ExcludeSubset").addClass("multi-IncludeSubset");
                    $block.show();
                } else {
                    $label.removeClass("multi-IncludeSubset").addClass("multi-ExcludeSubset");
                    $block.hide();
                }
            });
            $$($dialog, {_element: "input", role: "items"}).click(function () {
                var checked = this.checked;
                var id = this.id;
                var name = this.name;
                $$({_element: "input", _name: name}).each(function (index, element) {
                    let inputId = element.id;
                    if (inputId === id) {
                        $$({_element: "label", _for: inputId}).addClass("multi-Checked");
                    } else {
                        $$({_element: "label", _for: inputId}).removeClass("multi-Checked");
                    }
                });
            });
        }
    });
    
    
    function _completeFichotheque() {
        _completeTree("corpus");
        _completeTree("thesaurus");
        _completeTree("sphere");
        _completeTree("addenda");
        _completeTree("album");
    }
    
    function _completeTree(category) {
        for(let node of fichotheque[category]) {
            __completeNode(node);
        }
        
        function __completeNode(node) {
            if (node.node === "subset") {
                node._category = category;
            } else {
                for(let subnode of node.array) {
                    __completeNode(subnode);
                }
            }
        }
    }
    
};
