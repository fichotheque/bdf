/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom MultiAdmin
 * 
 * @namespace MultiAdmin
 */
var MultiAdmin = {};

MultiAdmin.ARGS = {
    authentificationSharing: "none",
    personManagementAllowed: false,
    centralSphereArray: []
};

MultiAdmin.fichothequeArray = {};

MultiAdmin.initPanes = function () {
    Pane.setLayoutVisible(true);
    $$("layout_list").html(Bdf.render("multiadmin:list/pane", {
        personManagementAllowed: MultiAdmin.ARGS.personManagementAllowed,
        centralSphereArray: MultiAdmin.ARGS.centralSphereArray,
        withCentralSpheres: (MultiAdmin.ARGS.centralSphereArray.length > 0)
    }));
    Bdf.runSerialFunctions(
       MultiAdmin.List.init
    );
    $$("listpane_button_newfichotheque").click(MultiAdmin.Overlay.showCreationForm);
    $$("listpane_button_newfichotheque2").click(MultiAdmin.Overlay.showCreationForm);
    $$("listpane_button_initall").click(MultiAdmin.List.initAll);
    $$("listpane_button_listreload").click(MultiAdmin.List.reload);
    $$("listpane_button_personmanager").click(function () {
        if (!Bdf.confirmUnload()) {
             return false;
        }
        Pane.loadOrDestock("personmanager", MultiAdmin.PersonManager.load);
    });
    $$("listpane_button_metadata").click(function () {
         if (!Bdf.confirmUnload()) {
              return false;
         }
         Pane.loadOrDestock("metadata", function () {
             MultiAdmin.Metadata.init();
         });
    });
};

MultiAdmin.testAuthentification = function (data) {
    if (!data.authentified) {
        $$("layout_main").html("");
        Bdf.Multi.showAuthentificationOverlay(MultiAdmin.initPanes);
        return false;
    } else {
        return true;
    }
};

MultiAdmin.getKey = function (category, value) {
    switch(category) {
        case "corpus":
            if (!value) {
                return "_ label.multi.fiches";
            } else {
                switch(value) {
                    case "none":
                        return "_ label.multi.fiches_none";
                    case "all":
                        return "_ label.multi.fiches_all";
                    default:
                        return "";
                }
            }
        case "thesaurus":
            if (!value) {
                return "_ label.multi.motcles";
            } else {
                switch(value) {
                    case "none":
                        return "_ label.multi.motcles_none";
                    case "all":
                        return "_ label.multi.motcles_all";
                    default:
                        return "";
                }
            }
        case "sphere":
            if (!value) {
                return "_ label.multi.redacteurs";
            } else {
                switch(value) {
                    case "none":
                        return "_ label.multi.redacteurs_none";
                    case "all":
                        return "_ label.multi.redacteurs_all";
                    default:
                        return "";
                }
            }
        case "album":
            if (!value) {
                return "_ label.multi.illustrations";
            } else {
                switch(value) {
                    case "none":
                        return "_ label.multi.illustrations_none";
                    case "all":
                        return "_ label.multi.illustrations_all";
                    case "selection":
                        return "_ label.multi.illustrations_selection";
                    default:
                        return "";
                }
            }
        case "addenda":
            if (!value) {
                return "_ label.multi.documents";
            } else {
                switch(value) {
                    case "none":
                        return "_ label.multi.documents_none";
                    case "all":
                        return "_ label.multi.documents_all";
                    case "selection":
                        return "_ label.multi.documents_selection";
                    default:
                        return "";
                }
            }
        default:
            return "";
    }
};

Bdf.addTemplateOptions({
    helpers: {
        isChecked: function (category, value) {
            switch(category) {
                case "corpus":
                    return (value === "none");
                case "album":
                case "addenda":
                    return (value === "selection");
                default:
                    return (value === "all");
            }
        },
        itemsLoc: function(category, value) {
            var key = MultiAdmin.getKey(category, value);
            if (key) {
                return Bdf.Loc.get(key);
            } else {
                return "?" + category + "/" + value + "?";
            }
        }
    }   
});

$(function(){
    Bdf.initTemplates();
    Pane.initLayout({
        hiddenAtStart: true
    });
    Bdf.initBeforeUnload();
    Bdf.Multi.checkAuthentification(MultiAdmin.initPanes);
});
