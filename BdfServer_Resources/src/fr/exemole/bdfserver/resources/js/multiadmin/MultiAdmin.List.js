/* global Bdf,$$,MultiAdmin,Pane */

MultiAdmin.List = {};

MultiAdmin.List.init = function(callback) {
    MultiAdmin.List.loadAjax(function (fichothequeArray) {
        MultiAdmin.fichothequeArray = fichothequeArray;
        $$("listpane_fichothequelist").html(Bdf.render("multiadmin:list/fichotheque", fichothequeArray));
        MultiAdmin.List.initActions();
        if (callback) {
            callback();
        }
    });
};

MultiAdmin.List.initActions = function() {
    var $fichothequeList = $$("listpane_fichothequelist");
    _onClick("duplicate", function () {
        MultiAdmin.Overlay.showDuplicationForm(Pane.getEntryName(this));
    });
    _onClick("init", function () {
        MultiAdmin.List.initFichotheque(Pane.getEntryName(this));
    });
    
    function _onClick(actionName, clickFunction) {
        $fichothequeList.on("click", $$.toCssSelector({action: actionName}), clickFunction);
    }
};

MultiAdmin.List.reload = function () {
    MultiAdmin.List.loadAjax(MultiAdmin.List.update);
};

MultiAdmin.List.initAll = function () {
    var notInitArray = new Array();
    var currentIndex = 0;
    for(let fichotheque of MultiAdmin.fichothequeArray) {
        if (!fichotheque.init) {
            notInitArray.push(fichotheque);
        }
    }
    _initNext();
    
    
    function _initNext() {
        if (currentIndex < notInitArray.length) {
            let nextFichotheque = notInitArray[currentIndex];
            currentIndex++;
            MultiAdmin.List.initFichotheque(nextFichotheque.name, _initNext);
        }
    }
    
};

MultiAdmin.List.initFichotheque = function (fichothequeName, callback) {
    $.ajax({
        url: Bdf.URL + "_admin",
        data: {
            json: "fichotheque-array",
            cmd: "FichothequeInit",
            name: fichothequeName
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            if (MultiAdmin.testAuthentification(data)) {
                Bdf.checkData(data, "fichothequeArray", function (fichothequeArray) {
                    MultiAdmin.List.update(fichothequeArray);
                    if (callback) {
                        callback();
                    }
                });
            }
        }
    });
};

MultiAdmin.List.loadAjax = function (callback) {
    $.ajax({
        url: Bdf.URL + "multi-admin",
        data: {
            json: "fichotheque-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            if (MultiAdmin.testAuthentification(data)) {
                Bdf.checkData(data, "fichothequeArray", callback);
            }
        }
    });
};

MultiAdmin.List.update = function (fichothequeArray) {
    MultiAdmin.fichothequeArray = fichothequeArray;
    var $fichothequeList = $$("listpane_fichothequelist");
    var $currentLis = $fichothequeList.children("div");
    var newLength = fichothequeArray.length;
    if (newLength === 0) {
        $fichothequeList.empty();
        return;
    }
    var currentLength = $currentLis.length;
    var p = 0;
    for(let newFichotheque of fichothequeArray) {
        while(true) {
            if (p >= currentLength) {
                $fichothequeList.append(Bdf.render("multiadmin:list/fichotheque", newFichotheque));
                break;
            } else {
                let currentFichotheque = $currentLis[p];
                let currentFichothequeName = currentFichotheque.dataset.name;
                if (newFichotheque.name === currentFichothequeName) {
                    $(currentFichotheque).replaceWith(Bdf.render("multiadmin:list/fichotheque", newFichotheque));
                        p++;
                    break;
                } else if (newFichotheque.name < currentFichothequeName) {
                    $(currentFichotheque).before(Bdf.render("multiadmin:list/fichotheque", newFichotheque));
                    break;
                } else {
                    $(currentFichotheque).remove();
                    p++;
                }
            }
        }
    }
};
