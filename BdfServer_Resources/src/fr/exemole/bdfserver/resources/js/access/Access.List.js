/* global Bdf,$$,Access,Pane */

Access.List = {};

Access.List.init = function (callback) {
    Bdf.Ajax.loadAccessArray(function (accessArray) {
        accessArray = Access.initArray(accessArray);
        let $listBody = $$("listpane_body");
        $listBody.html(Bdf.render("access:list/access", accessArray));
        Bdf.Deploy.init($listBody);
        Access.List.initActions();
        if (callback) {
            callback();
        }
    });
};

Access.List.initActions = function () {
    $$("listpane_button_newaccess").click(Access.Overlay.showCreationForm);
    var $body = $$("listpane_body");
     _onClick("form", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let name = Pane.getEntryName(this);
        Pane.loadOrDestock(name, function () {
            Access.Form.load(name);
        });
    });
     _onClick("remove", function () {
        Access.Overlay.showRemoveConfirm(Pane.getEntryName(this));
    });
    
    function _onClick(actionName, clickFunction) {
        $body.on("click", $$.toCssSelector({action: actionName}), clickFunction);
    }
};

Access.List.updateAccess = function (newAccess) {
    var $currentAccess =  $$({role: "entry", name: newAccess.name});
    $$($currentAccess, {role: "entry-title"}).html(newAccess.title);
};