/* global Bdf,$$,Access,Overlay,Pane */

Access.Overlay = {};

Access.Overlay.showCreationForm = function () {
    if (!Bdf.confirmUnload()) {
        return;
    }
    Overlay.start({
        header:Bdf.Loc.escape("_ EXP-61"),
        content: Bdf.render("access:overlay/creationform", {
            commandKey: "EXP-61",
            commandUrl: Bdf.getCommandUrl("EXP-61")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.exportation.accesscreation'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Pane.clearMain();
                    Overlay.end($form, function() {
                        Bdf.showCommandMessage(data.commandMessage);
                        Access.insertAccess(data.access);
                    });
                }
            }
        }
    });
};

Access.Overlay.showRemoveConfirm = function (accesName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var access = Access.getAccess(accesName);
    if (!access) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-62"),
        content: Bdf.render("access:overlay/removeconfirm", {
            name: accesName,
            title: access.title,
            commandKey: "EXP-62",
            commandUrl: Bdf.getCommandUrl("EXP-62")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.exportation.accessremove'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                var done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function() {
                        Bdf.showCommandMessage(data.commandMessage);
                        Access.removeAccess(accesName);
                    });
                }
            }
        }
    });
};
