/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom Access
 * 
 * @namespace Access
 */
var Access = {};

Access.array = null;

Access.initArray = function (array) {
    for(let accessObj of array) {
        Access.completeAccess(accessObj);
    }
    Access.array = array;
    return array;
};

Access.getAccess = function (accessName) {
    for(let access of Access.array) {
        if (access.name === accessName) {
            return access;
        }
    }
    return null;
};

Access.updateAccess = function (newAccess) {
    newAccess = Access.completeAccess(newAccess);
    Bdf.updateInArray(Access.array, newAccess);
    Access.List.updateAccess(newAccess);
    return newAccess;
};

Access.removeAccess = function (name) {
    Bdf.removeInArray(Access.array, name);
    Pane.removeContents(name);
    Pane.removeEntry($$("listpane_body"), name);
};

Access.insertAccess = function (access) {
    access = Access.completeAccess(access);
    Bdf.insertInArray(Access.array, access);
    var $entry = Pane.insertEntry($$("listpane_body"), access.name, Bdf.render("access:list/access", access));
    $$("listpane_body").animate({
        scrollTop: $entry.offset().top
    }, 1000, function () {
        Bdf.Deploy.ensureLinkVisibility($entry, true);
    });
    Access.Form.load(access.name);
    return access;
};

Access.completeAccess = function (accessObj) {
    return accessObj;
};


/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    $$("layout_list").html(Bdf.render("access:list/pane", {}));
    Bdf.initBeforeUnload();
    Bdf.runSerialFunctions(
        Bdf.Ajax.initLangConfiguration,
        Bdf.Ajax.initPathConfiguration,
        Bdf.Collections.initSelectionDefArray,
        Bdf.Collections.initTableExportArray,
        Access.List.init
    );
    Pane.initShortcuts();
});
