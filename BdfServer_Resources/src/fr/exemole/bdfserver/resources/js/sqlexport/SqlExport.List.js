/* global Bdf,$$,SqlExport,Pane */

SqlExport.List = {};

SqlExport.List.init = function (callback) {
    Bdf.Ajax.loadSqlExportArray(function (sqlExportArray) {
        sqlExportArray = SqlExport.initArray(sqlExportArray);
        let $listBody = $$("listpane_body");
        $listBody.html(Bdf.render("sqlexport:list/sqlexport", sqlExportArray));
        Bdf.Deploy.init($listBody);
        SqlExport.List.initActions();
        if (callback) {
            callback();
        }
    });
};

SqlExport.List.initActions = function () {
    $$("listpane_button_newsqlexport").click(SqlExport.Overlay.showCreationForm);
    var $body = $$("listpane_body");
    _onClick("metadata", function () {
        if (!Bdf.confirmUnload()) {
            return false;
        }
        let name = Pane.getEntryName(this);
        Pane.loadOrDestock(name, function () {
            SqlExport.Form.load(name);
        });
    });
    _onClick("run", function () {
        SqlExport.Overlay.showRun(Pane.getEntryName(this));
    });
    _onClick("sqlexportremove", function () {
        SqlExport.Overlay.showRemoveConfirm(Pane.getEntryName(this));
    });
    
    function _onClick(actionName, clickFunction) {
        $body.on("click", $$.toCssSelector({action: actionName}), clickFunction);
    }
};

SqlExport.List.updateSqlExport = function (newSqlExport) {
    var $currentSqlExport =  $$({role: "entry", name: newSqlExport.name});
    $$($currentSqlExport, {role: "entry-title"}).html(newSqlExport.title);
};
