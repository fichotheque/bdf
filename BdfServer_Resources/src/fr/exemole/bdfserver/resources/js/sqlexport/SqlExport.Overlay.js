/* global Bdf,$$,SqlExport,Overlay,Pane */

SqlExport.Overlay = {};

SqlExport.Overlay.showCreationForm = function () {
    if (!Bdf.confirmUnload()) {
        return;
    }
    Overlay.start({
        header:Bdf.Loc.escape("_ EXP-41"),
        content: Bdf.render("sqlexport:overlay/creationform", {
            commandKey: "EXP-41",
            commandUrl: Bdf.getCommandUrl("EXP-41")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.exportation.sqlexportcreation'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Pane.clearMain();
                    Overlay.end($form, function() {
                        Bdf.showCommandMessage(data.commandMessage);
                        SqlExport.insertSqlExport(data.sqlExport);
                    });
                }
            }
        }
    });
};

SqlExport.Overlay.showRemoveConfirm = function (sqlExportName) {
    if (!Bdf.confirmUnload()) {
        return;
    }
    var sqlExport = SqlExport.getSqlExport(sqlExportName);
    if (!sqlExport) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ EXP-42"),
        content: Bdf.render("sqlexport:overlay/removeconfirm", {
            name: sqlExportName,
            title: sqlExport.title,
            commandKey: "EXP-42",
            commandUrl: Bdf.getCommandUrl("EXP-42")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: '_ submit.exportation.sqlexportremove'
            }}),
        formAttrs: {
            action: Bdf.URL + "exportation",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                var done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function() {
                        Bdf.showCommandMessage(data.commandMessage);
                        SqlExport.removeSqlExport(sqlExportName);
                    });
                }
            }
        }
    });
};

SqlExport.Overlay.showRun = function (sqlExportName) {
    var resultDestinationId = Bdf.generateId();
    var sqlExport = SqlExport.getSqlExport(sqlExportName);
    if (!sqlExport) {
        return;
    }
    Overlay.start({
        header: Bdf.Loc.escape("_ title.exportation.rundialog"),
        content: Bdf.render("sqlexport:overlay/run", {
            name: sqlExportName,
            title: sqlExport.title,
            resultDestinationId: resultDestinationId
        }),
        isWaiting: true,
        afterStart: function (overlayId, waitingCallback) {
            $.ajax ({
                url: Bdf.URL + "exportation",
                data: {
                    sqlexport:  sqlExportName,
                    cmd: "SqlExportRun",
                    json: "sqlexport-paths"
                },
                dataType: "json",
                success: function (data, textStatus) {
                    var done = Bdf.checkData(data, "sqlExportPaths", function (sqlExportPaths) {
                        $$(resultDestinationId).html(Bdf.render("sqlexport:overlay/run_result", {
                            rootUrl: Bdf.URL,
                            paths: sqlExportPaths
                        }));
                    });
                    if (!done) {
                        Overlay.end(overlayId);
                    } else {
                        waitingCallback();
                    }
                }
            });
        }
    });
    
};