/* global Bdf,$$,SqlExport,CodeMirror,Pane */

SqlExport.Form = {};

SqlExport.Form.load = function (sqlExportName) {
    var sqlExport = SqlExport.getSqlExport(sqlExportName);
    if (!sqlExport) {
        return;
    }
    var isInStock = false;
    var formId = Bdf.generateId();
    $$("layout_main").html(Bdf.render("sqlexport:form", {
        action: Bdf.URL + "exportation",
        formId: formId,
        sqlExportName: sqlExport.name,
        title: sqlExport.title,
        labelArray: Bdf.toLabelArray(sqlExport.labelMap),
        labelsLocKey: "_ title.exportation.sqlexportlabels",
        currentTableExportName : sqlExport.tableExportName,
        tableExportArray: Bdf.Collections.tableExportArray,
        customClassName: sqlExport.sqlExportClassName,
        path: sqlExport.path,
        url: sqlExport.url,
        postCommand: sqlExport.postCommand,
        params: _paramsToString(),
        selectionDefArray: Bdf.Collections.selectionDefArray,
        currentSelectionDefName: sqlExport.selectionDefName,
        queryXml: sqlExport.query.ficheXml + sqlExport.query.motcleXml,
        targetName: sqlExport.targetName,
        targetPath: sqlExport.targetPath,
        fileName: sqlExport.fileName,
        availableTargetArray: Bdf.pathConfiguration.targetArray,
        attributes: Bdf.attrMapToString(sqlExport.attrMap),
        attributesLocKey: "_ title.exportation.sqlexportattributes",
        commandKey: "EXP-43",
        commandUrl: Bdf.getCommandUrl("EXP-43")
    }));
    Bdf.Deploy.init($$("layout_main"));
    $$(formId, "button_stock").click(function () {
        Pane.moveMainToStock();
    });
    $$(formId).ajaxForm({
        beforeSerialize: function ($form, options) {
            if (isInStock) {
                return false;
            } else {
                queryxmlCodeMirror.save();
                attributesCodeMirror.save();
                return true;
            }
        },
        dataType: "json",
        success: function (data, textStatus, jqXHR, $form) {
            if (Bdf.checkError(data)) {
                Pane.setChange(false);
                if (Bdf.showCommandMessage(data.commandMessage)) {
                    Bdf.updateDefaultValues($$(formId));
                    let newSqlExport = SqlExport.updateSqlExport(data.sqlExport);
                    $$(formId, {role: "metadata-title"}).html(newSqlExport.title);
                }
            }
        }
    });
    var queryxmlCodeMirror = Pane.initQueryDetails(formId);
    var attributesCodeMirror = Pane.initAttributesDetails(formId);
    $$(formId).data("stockHandler", _stockHandler);
    Pane.checkState();
    Pane.checkTooltip($$(formId));

    
    function _paramsToString() {
        var params = "";
        for(var i = 0, len =  sqlExport.paramArray.length ; i < len; i++) {
            var param = sqlExport.paramArray[i];
            if (i > 0) {
                params += "\n";
            }
            params += param.name + "=" + param.value;
        }
        return params;
    }
    
    function _stockHandler(stock) {
        isInStock = stock;
        queryxmlCodeMirror.setOption("readOnly", stock);
        attributesCodeMirror.setOption("readOnly", stock);
        Pane.setReadOnly($$(formId), stock);
    }
    
};