/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom SqlExport
 * 
 * @namespace SqlExport
 */
var SqlExport = {};

SqlExport.array = null;

/******************************************************************************
 * Actions sur SqlExport
 ******************************************************************************/

SqlExport.initArray = function (array) {
    for(let sqlExportObj of array) {
        SqlExport.completeSqlExport(sqlExportObj);
    }
    SqlExport.array = array;
    return array;
};

SqlExport.getSqlExport = function (sqlExportName) {
    for(let sqlExport of SqlExport.array) {
        if (sqlExport.name === sqlExportName) {
            return sqlExport;
        }
    }
    return null;
};

SqlExport.updateSqlExport = function (newSqlExport) {
    newSqlExport = SqlExport.completeSqlExport(newSqlExport);
    Bdf.updateInArray(SqlExport.array, newSqlExport);
    SqlExport.List.updateSqlExport(newSqlExport);
    return newSqlExport;
};

SqlExport.removeSqlExport = function (name) {
    Bdf.removeInArray(SqlExport.array, name);
    Pane.removeContents(name);
    Pane.removeEntry($$("listpane_body"), name);
};

SqlExport.insertSqlExport = function (sqlExport) {
    sqlExport = SqlExport.completeSqlExport(sqlExport);
    Bdf.insertInArray(SqlExport.array, sqlExport);
    var $entry = Pane.insertEntry($$("listpane_body"), sqlExport.name, Bdf.render("sqlexport:list/sqlexport", sqlExport));
    $$("listpane_body").animate({
        scrollTop: $entry.offset().top
    }, 1000, function () {
        Bdf.Deploy.ensureLinkVisibility($entry, true);
    });
    SqlExport.Form.load(sqlExport.name);
    return sqlExport;
};

SqlExport.completeSqlExport = function (sqlExportObj) {
    return sqlExportObj;
};


/******************************************************************************
 * Enregistrement de fonction
 ******************************************************************************/

Pane.registerCheckFunction(function (state) {
    $(".pane-list-ActiveItem").removeClass("pane-list-ActiveItem");
    if (state.withMain()) {
        let $sqlExport = $$({role: "entry", name: state.mainContentKey});
        $$($sqlExport, {role: "entry-header"}).addClass("pane-list-ActiveItem");
    }
});


/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Pane.initLayout();
    $$("layout_list").html(Bdf.render("sqlexport:list/pane", {}));
    Bdf.initBeforeUnload();
    Bdf.runSerialFunctions(
        Bdf.Ajax.initLangConfiguration,
        Bdf.Ajax.initPathConfiguration,
        Bdf.Collections.initSelectionDefArray,
        Bdf.Collections.initTableExportArray,
        SqlExport.List.init
    );
    Pane.initShortcuts();
});