/* global Bdf,$$,CentralSphere,Pane */

CentralSphere.List = {};

CentralSphere.List.init = function (sphereName, callback) {
    CentralSphere.Ajax.loadUserArray(sphereName,  function (userArray) {
        let dispatch = CentralSphere.List.dispatch(userArray);
        _populateList("active", dispatch.active);
        _populateList("readonly", dispatch.readonly);
        _populateList("inactive", dispatch.inactive);
        CentralSphere.List.initActions();
        if (callback) {
            callback();
        }
    });
    
    function _populateList(name, array) {
        $$("listpane_count", name).html(array.length);
        if (array.length > 0) {
            $$("listpane_unit", name).removeClass("hidden");
            $$("listpane_userlist", name).html(Bdf.render("centralsphere:list/user", array));
        } else {
            $$("listpane_unit", name).addClass("hidden");
        }
    }
};

CentralSphere.List.initActions = function() {
    _initUserState("active");
    _initUserState("readonly");
    _initUserState("inactive");
    
    function _initUserState(unitName) {
        $$("listpane_userlist", unitName).on("click", $$.toCssSelector({action: "userstate"}), function () {
            if (!Bdf.confirmUnload()) {
                return false;
            }
            let name = Pane.getEntryName(this);
            Pane.loadOrDestock(name, function () {
                CentralSphere.User.init(name);
            });
        });
    }
};

CentralSphere.List.update = function (userArray) {
    let dispatch = CentralSphere.List.dispatch(userArray);
    _update("active", dispatch.active);
    _update("readonly", dispatch.readonly);
    _update("inactive", dispatch.inactive);
    
    function _update(name, array) {
        let $userList = $$("listpane_userlist", name);
        let $currentLis = $userList.children("div");
        let newLength = array.length;
        $$("listpane_count", name).html(newLength);
        if (newLength === 0) {
            $userList.empty();
            $$("listpane_unit", name).addClass("hidden");
            return;
        } else {
            $$("listpane_unit", name).removeClass("hidden");
        }
        let currentLength = $currentLis.length;
        let p = 0;
        for(let newUser of array) {
            let newUserName = newUser.sphere + "/" + newUser.login;
            while(true) {
                if (p >= currentLength) {
                    $userList.append(Bdf.render("centralsphere:list/user", newUser));
                    break;
                } else {
                    let currentUser = $currentLis[p];
                    let currentUserName = currentUser.dataset.name;
                    if (newUserName === currentUserName) {
                        $(currentUser).replaceWith(Bdf.render("centralsphere:list/user", newUser));
                            p++;
                        break;
                    } else if (newUserName < currentUserName) {
                        $(currentUser).before(Bdf.render("centralsphere:list/user", newUser));
                        break;
                    } else {
                        $(currentUser).remove();
                        p++;
                    }
                }
            }
        }
    }
};

CentralSphere.List.dispatch = function (userArray) {
    let result = {
        active: [],
        readonly: [],
        inactive: []
    };
    for(let user of userArray) {
        switch(user.status) {
            case "inactive":
                result.inactive.push(user);
                break;
            case "readonly":
                result.readonly.push(user);
                break;
            default:
                result.active.push(user);
        }
    }
    return result;
};
