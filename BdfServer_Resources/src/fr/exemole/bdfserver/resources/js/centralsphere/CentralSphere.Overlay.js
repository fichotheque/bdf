/* global Bdf,$$,CentralSphere,Overlay,Pane */

CentralSphere.Overlay = {};

CentralSphere.Overlay.showPersonChangeForm = function (user, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        content: Bdf.render("centralsphere:overlay/personchangeform", {
            formId: formId,
            user: user
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.sphere.redacteurchange"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(data.user);
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.showEmailChangeForm = function (user, callback) {
    var formId = Bdf.generateId();
    var email = "";
    if (user.email) {
        email = user.email.complete;
    }
    Overlay.start({
        content: Bdf.render("centralsphere:overlay/emailchangeform", {
            formId: formId,
            user: user,
            email: email
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.sphere.emailchange"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(data.user);
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.showPasswordChangeForm = function (user, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        content: Bdf.render("centralsphere:overlay/passwordchangeform", {
            formId: formId,
            user: user
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.sphere.passwordchange"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(data.user);
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.showStatusChangeForm = function (user, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        content: Bdf.render("centralsphere:overlay/statuschangeform", {
            formId: formId,
            user: user,
            levelArray: CentralSphere.Overlay.getUserLevelOptionArray("central", formId, user.status)
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.sphere.redacteurstatus"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(data.user, data.userstate);
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.showUserCreationForm = function (sphere, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ SPH-04"),
        content: Bdf.render("centralsphere:overlay/usercreationform", {
            formId: formId,
            sphere: sphere
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.sphere.redacteurcreation"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let newlogin = $$(formId, "newlogin").val().trim();
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(newlogin, data.userArray);
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.showAddToFichothequeConfirm = function (user, fichothequeStatus, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ title.multi.addtofichotheque"),
        content: Bdf.render("centralsphere:overlay/addtofichothequeconfirm", {
            formId: formId,
            user: user,
            fichothequeName: fichothequeStatus.fichothequeName,
            fichothequeTitle: fichothequeStatus.fichothequeTitle,
            levelArray: CentralSphere.Overlay.getUserLevelOptionArray("fichotheque", formId, "user")
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.multi.addtofichotheque"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(data.userstate);
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.showStatusInFichothequeChangeForm = function (user, fichothequeStatus, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ title.multi.statusinfichothequechange"),
        content: Bdf.render("centralsphere:overlay/statusinfichothequechangeform", {
            formId: formId,
            user: user,
            fichothequeName: fichothequeStatus.fichothequeName,
            fichothequeTitle: fichothequeStatus.fichothequeTitle,
            levelArray: CentralSphere.Overlay.getUserLevelOptionArray("fichotheque", formId, fichothequeStatus.level)
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.sphere.redacteurstatus"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(data.userstate);
                        }
                    });
                }
            }
        }
    });

};

CentralSphere.Overlay.showCopyConfirm = function (sphereName, fichothequeStatus, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ link.multi.copyfromfichotheque"),
        content: Bdf.render("centralsphere:overlay/copyconfirm", {
            formId: formId,
            sphereName: sphereName,
            fichothequeName: fichothequeStatus.fichothequeName,
            fichothequeTitle: fichothequeStatus.fichothequeTitle,
            logins: fichothequeStatus.loginArrays.outside
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ link.global.confirm"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        Bdf.showCommandMessage(data.commandMessage);
                        if (callback) {
                            callback(data.userArray, data.stats)
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.showFichothequeSphereCreateConfirm = function (sphereName, fichotheque, callback) {
    var formId = Bdf.generateId();
    Overlay.start({
        header: Bdf.Loc.escape("_ title.multi.addtofichotheque"),
        content: Bdf.render("centralsphere:overlay/fichothequespherecreateconfirm", {
            formId: formId,
            sphereName: sphereName,
            fichothequeName: fichotheque.fichothequeName,
            fichothequeTitle: fichotheque.fichothequeTitle
        }),
        footer: Bdf.render("bdf:overlay-footer", {
            submit: {
                locKey: "_ submit.multi.addtofichotheque"
            }}),
        formAttrs: {
            action: Bdf.URL + "multi-sphere",
            method: "POST"
        },
        ajaxForm: {
            dataType: "json",
            success: function (data, textStatus, jqXHR, $form) {
                let done = Bdf.checkError(data);
                if (done) {
                    Overlay.end($form, function () {
                        if (callback) {
                            callback(data.stats);
                        }
                    });
                }
            }
        }
    });
};

CentralSphere.Overlay.getUserLevelOptionArray = function (type, formId, checkedValue) {
    let array = [];
    if (type === "fichotheque") {
        array.push(_getLevelOption("admin"));
        array.push(_getLevelOption("user"));
    } else {
        array.push(_getLevelOption("active"));
    }
    array.push(_getLevelOption("readonly"));
    array.push(_getLevelOption("inactive"));
    return array;
    
    function _getLevelOption(name) {
        let level = CentralSphere.getUserLevel(name);
        let checked = false;
        if (name === checkedValue) {
            checked = true;
        }
        return {
            level: level,
            checked: checked,
            formId: formId
        };
    }
};
