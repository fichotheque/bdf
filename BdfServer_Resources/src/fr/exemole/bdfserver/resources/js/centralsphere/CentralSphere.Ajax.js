/* global Bdf,$$,CentralSphere */

CentralSphere.Ajax = {};

CentralSphere.Ajax.loadMetadata = function (sphereName, callback) {
    $.ajax({
        url: Bdf.URL + "multi-sphere",
        data: {
            sphere: sphereName,
            json: "centralspheremetadata"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            if (CentralSphere.testAuthentification(data)) {
                Bdf.checkData(data, "metadata", callback);
            }
        }
    });
};

CentralSphere.Ajax.loadStats = function (sphereName, callback) {
    $.ajax({
        url: Bdf.URL + "multi-sphere",
        data: {
            sphere: sphereName,
            json: "centralspherestats"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            if (CentralSphere.testAuthentification(data)) {
                Bdf.checkData(data, "stats", callback);
            }
        }
    });
};

CentralSphere.Ajax.loadUserArray = function (sphereName, callback) {
    $.ajax({
        url: Bdf.URL + "multi-sphere",
        data: {
            sphere: sphereName,
            json: "user-array"
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            if (CentralSphere.testAuthentification(data)) {
                Bdf.checkData(data, "userArray", callback);
            }
        }
    });
};

CentralSphere.Ajax.loadUser = function (sphereName, login, json, callback) {
    $.ajax({
        url: Bdf.URL + "multi-sphere",
        data: {
            sphere: sphereName,
            login: login,
            json: json
        },
        dataType: Bdf.jsonDataType,
        success: function (data, textStatus) {
            if (CentralSphere.testAuthentification(data)) {
                Bdf.checkData(data, json, callback);
            }
        }
    });
};

