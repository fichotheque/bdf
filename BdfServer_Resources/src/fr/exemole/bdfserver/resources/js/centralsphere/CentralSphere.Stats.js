/* global Bdf,$$,CentralSphere,Pane */

CentralSphere.Stats = {};

CentralSphere.Stats.init = function (sphereName, callback) {
    CentralSphere.Ajax.loadStats(sphereName, function (stats) {
        if (callback) {
            callback(sphereName, stats);
        } else {
            CentralSphere.Stats.load(sphereName, stats);
        }
    });
};

CentralSphere.Stats.load = function (sphereName, stats) {
    var formId = Bdf.generateId();
    var currentSphereStats = CentralSphere.Stats.completeStats(stats);
    var $main = $$("layout_main");
    $main.html(Bdf.render("centralsphere:statspane/main", {
        sphereName: sphereName,
        formId: formId,
        stats: currentSphereStats
    }));
    _initActions($main);
    $$(formId, "table").tablesorter({
        theme: "blue",
        textAttribute: "data-value",
        widgets: ['zebra'],
        sortList: [[1,0]]
    });
    
    function _initActions($container) {
        $$($container, {action: "fichothequespherecreate"}).click(function () {
            let fichothequeName = this.dataset.fichotheque;
            let button = this;
            CentralSphere.Overlay.showFichothequeSphereCreateConfirm(sphereName, _getFichotheque(fichothequeName), function (stats) {
                currentSphereStats = CentralSphere.Stats.completeStats(stats);
                _updatePresenceCell(button, fichothequeName);
            });
        });
        $$($container, {action: "copy"}).click(function () {
            let fichothequeName = this.dataset.fichotheque;
            CentralSphere.Overlay.showCopyConfirm(sphereName, _getFichotheque(fichothequeName), function (userArray, sphereStats) {
                CentralSphere.List.update(userArray);
                currentSphereStats = CentralSphere.Stats.completeStats(sphereStats);
                $$(formId, "tbody").html(Bdf.render("centralsphere:statspane/fichothequerow", currentSphereStats.fichothequeArray));
                $.tablesorter.update($$.one(formId, "table").config, true);
            });
        });
    }
    
    function _getFichotheque(fichothequeName) {
        for(let fichotheque of currentSphereStats.fichothequeArray) {
            if (fichotheque.fichothequeName === fichothequeName) {
                return fichotheque;
            }
        }
        return null;
    }
    
    function _updatePresenceCell(button, fichothequeName) {
        let newFichotheque = _getFichotheque(fichothequeName);
        let $cell = $(Bdf.getMandatoryAncestor(button, {role: "presencecell"}));
        $cell.attr("data-value", newFichotheque._level.letter);
        $cell.html(Bdf.render("centralsphere:statspane/presencecell", newFichotheque));
        _initActions($cell);
        $.tablesorter.updateCell($$.one(formId, "table").config, $cell, false);
    }
};

CentralSphere.Stats.completeStats = function (sphereStats) {
    for(let fichotheque of sphereStats.fichothequeArray) {
        fichotheque._level = CentralSphere.getSphereLevel(_getLevelName(fichotheque));
    }
    return sphereStats;
    
    function _getLevelName(fichotheque) {
        if (fichotheque.here) {
            if (fichotheque.fichothequeInit) {
                return "here";
            } else {
                return "notinit";
            }
        } else {
            return "nosphere";
        }
    }
};

