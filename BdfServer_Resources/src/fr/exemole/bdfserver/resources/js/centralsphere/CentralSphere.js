/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom CentralSphere
 * 
 * @namespace CentralSphere
 */
var CentralSphere = {};

CentralSphere.ARGS = {
    sphereName: "",
    langConfiguration: {
        workingLangArray: ["en"],
        supplementaryLangArray: []
    }
};

CentralSphere.USERLEVELS = {
    admin: {
        name: "admin",
        letter: "A",
        symbol: "🅐",
        locKey: "_ label.sphere.admin"
    },
    user: {
        name: "user",
        letter: "B",
        symbol: "◆",
        locKey: "_ label.sphere.status_active"
    },
   active: {
        name: "active",
        letter: "B",
        symbol: "◆",
        locKey: "_ label.sphere.status_active"
    },
    readonly: {
        name: "readonly",
        letter: "C",
        symbol: "⬗",
        locKey: "_ label.sphere.status_readonly"
    },
    inactive: {
        name: "inactive",
        letter: "D",
        symbol: "◇",
        locKey: "_ label.sphere.status_inactive"
    },
    here: {
        name: "here",
        letter: "E",
        symbol: "!",
        locKey: "_ label.multi.status_undeterminedhere"
    },
    nouser: {
        name: "nouser",
        letter: "F",
        symbol: " "
    },
    unknown: {
        name: "unknown",
        letter: "G",
        symbol: "?"
    }
};

CentralSphere.SPHERELEVELS = {
    here: {
        name: "here",
        letter: "A",
        symbol: "◼",
        locKey: "_ label.multi.sphere_here"
    },
    notinit: {
        name: "notinit",
        letter: "B",
        symbol: "!",
        locKey: "_ label.multi.sphere_notinit"
    },
    nosphere: {
        name: "nosphere",
        letter: "C",
        symbol: " "
    },
    unknown: {
        name: "unknown",
        letter: "D",
        symbol: "?"
    }
};

CentralSphere.getUserLevel = function (name) {
    if (CentralSphere.USERLEVELS.hasOwnProperty(name)) {
        return CentralSphere.USERLEVELS[name];
    } else {
        return CentralSphere.USERLEVELS.unknown;
    }
};

CentralSphere.getSphereLevel = function (name) {
    if (CentralSphere.SPHERELEVELS.hasOwnProperty(name)) {
        return CentralSphere.SPHERELEVELS[name];
    } else {
        return CentralSphere.SPHERELEVELS.unknown;
    }
};

CentralSphere.initPanes = function () {
    var sphereName = CentralSphere.ARGS.sphereName;
    Pane.setLayoutVisible(true);
    $$("layout_list").html(Bdf.render("centralsphere:list/pane", {
        sphereName: sphereName
    }));
    Bdf.runSerialFunctions(
       _initCentralSphere
    );
    $$("listpane_button_metadata").click(function () {
        if (!Bdf.confirmUnload()) {
             return false;
        }
        Pane.loadOrDestock("metadata", function () {
            CentralSphere.Metadata.init(sphereName);
        });
    });
    $$("listpane_button_stats").click(function () {
        if (!Bdf.confirmUnload()) {
             return false;
        }
        Pane.loadOrDestock("stats", function () {
            CentralSphere.Stats.init(sphereName);
        });
    });
    $$("listpane_button_usercreation").click(function () {
        if (!Bdf.confirmUnload()) {
             return false;
        }
        CentralSphere.Overlay.showUserCreationForm(sphereName, function (newLogin, userArray) {
            CentralSphere.List.update(userArray);
            Pane.loadOrDestock(newLogin, function () {
                CentralSphere.User.init(sphereName + "/" + newLogin);
            });
        });
    });
    
    
    function _initCentralSphere() {
        CentralSphere.List.init(sphereName);
    }
    
};

CentralSphere.testAuthentification = function (data) {
    if (!data.authentified) {
        $$("layout_main").html("");
        Bdf.Multi.showAuthentificationOverlay(CentralSphere.initPanes);
        return false;
    } else {
        return true;
    }
};


$(function(){
    Bdf.initTemplates();
    Bdf.langConfiguration = CentralSphere.ARGS.langConfiguration;
    Pane.initLayout({
        hiddenAtStart: true
    });
    Bdf.initBeforeUnload();
    Bdf.Multi.checkAuthentification(CentralSphere.initPanes);
});
