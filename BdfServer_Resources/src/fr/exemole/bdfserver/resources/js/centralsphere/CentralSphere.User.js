/* global Bdf,$$,CentralSphere,Pane */

CentralSphere.User = {};

CentralSphere.User.init = function (name, callback) {
    var idx = name.indexOf("/");
    var sphereName = name.substring(0, idx);
    var login = name.substring(idx + 1);
    CentralSphere.Ajax.loadUser(sphereName, login, "user,userstate", function (user, userstate) {
        if (callback) {
            callback(user, userstate);
        } else {
            CentralSphere.User.load(user, userstate);
        }
    });
};

CentralSphere.User.load = function (user, userstate) {
    var currentUser = user;
    currentUser._level = CentralSphere.getUserLevel(currentUser.status);
    var currentUserstate = CentralSphere.User.completeUserState(userstate);
    var formId = Bdf.generateId();
    var $main = $$("layout_main");
    $main.html(Bdf.render("centralsphere:userpane/main", {
        formId: formId,
        user: currentUser,
        userstate: currentUserstate
    }));
    _initActions($main);
    $$(formId, "table").tablesorter({
        theme: "blue",
        textAttribute: "data-value",
        widgets: ['zebra'],
        sortList: [[1,0], [0,0]]
    });
    $$(formId, "button_personchange").click(function () {
        CentralSphere.Overlay.showPersonChangeForm(currentUser, function (newUser) {
            _updateUser(newUser);
            $$(formId, "text_surname").text(newUser.person.surname);
            $$(formId, "text_forname").text(newUser.person.forename);
        });
    });
    $$(formId, "button_emailchange").click(function () {
        CentralSphere.Overlay.showEmailChangeForm(currentUser, function (newUser) {
            _updateUser(newUser);
            let newEmail = "";
            if (newUser.email) {
                newEmail = newUser.email.complete;
            }
            $$(formId, "text_email").text(newEmail);
        });
    });
    $$(formId, "button_statuschange").click(function () {
        CentralSphere.Overlay.showStatusChangeForm(currentUser, function (newUser, newUserstate) {
            CentralSphere.User.load(newUser, newUserstate);
            /*_updateUser(newUser);
            $$(formId, "userstatus").html(Bdf.render("centralsphere:userpane/userstatus", currentUser._level));
            CentralSphere.Ajax.loadUserArray(currentUser.sphere,  function (userArray) {
                CentralSphere.List.update(userArray);
            });
            CentralSphere.Ajax.loadUser(currentUser.sphere, currentUser.login, "userstate", function (userstate) {
                if (callback) {
                    callback(user, userstate);
                } else {
                    CentralSphere.User.load(user, userstate);
                }
            });*/
        });
    });
    $$(formId, "button_passwordchange").click(function () {
        CentralSphere.Overlay.showPasswordChangeForm(currentUser, function (newUser) {
            _updateUser(newUser);
        });
    });
    
    function _updateUser(newUser) {
        $$({field: "user_name", login: newUser.login}).html(newUser.name);
        currentUser = newUser;
        currentUser._level = CentralSphere.getUserLevel(currentUser.status);
    }
    
    function _updateStatusCell(button, fichothequeName) {
        let newFichothequeStatus = _getFichothequeStatus(fichothequeName);
        let $cell = $(Bdf.getMandatoryAncestor(button, {role: "presencecell"}));
        $cell.attr("data-value", newFichothequeStatus._level.letter);
        $cell.html(Bdf.render("centralsphere:userpane/presencecell", newFichothequeStatus));
        _initActions($cell);
        $.tablesorter.updateCell($$.one(formId, "table").config, $cell, false);
    }
    
    function _getFichothequeStatus(fichothequeName) {
        for(let status of currentUserstate.statusArray) {
            if (status.fichothequeName === fichothequeName) {
                return status;
            }
        }
        return null;
    }
    
    function _initActions($container) {
        $$($container, {action: "addtofichotheque"}).click(function () {
            let fichothequeName = this.dataset.fichotheque;
            let button = this;
            CentralSphere.Overlay.showAddToFichothequeConfirm(currentUser, _getFichothequeStatus(fichothequeName), function (userstate) {
                currentUserstate = CentralSphere.User.completeUserState(userstate);
                _updateStatusCell(button, fichothequeName);
            });
        });
        $$($container, {action: "statusedit"}).click(function () {
            let fichothequeName = this.dataset.fichotheque;
            let button = this;
            CentralSphere.Overlay.showStatusInFichothequeChangeForm(currentUser, _getFichothequeStatus(fichothequeName), function (userstate) {
                currentUserstate = CentralSphere.User.completeUserState(userstate);
                _updateStatusCell(button, fichothequeName);
            });
        });
    }

};

CentralSphere.User.completeUserState = function (userstate) {
    let newArray = [];
    for(let status of userstate.statusArray) {
        if (status.level !== "nosphere") {
            status._level = CentralSphere.getUserLevel(status.level);
            newArray.push(status);
        }
    }
    userstate.statusArray = newArray;
    return userstate; 
};
