/* global Bdf,$$ */
/**
 * Objet global définissant l'espace de nom History
 * Suppose l'existence d'un objet ARGS avec les paramètres
 * 
 * @namespace History
 */
var History = {};

History.ARGS = {
    clientId: "",
    subsetKey: "",
    subsetCategory: "",
    subsetName: "",
    id: 0
};

History.init = function (args) {
    var getRevisionFunction = null;
    var historyId = Bdf.generateId();
    $$(args.clientId).html(Bdf.render("history:client", {
        historyId: historyId
    }));
    var $revisions = $$(historyId, "revisions");
    if (args.subsetCategory === "corpus") {
        History.Fiche.init(historyId, args, function () {
            _loadHistory(History.Fiche.showDiff);
        });
        getRevisionFunction = History.Fiche.getRevisionRow;
    } else {
        _loadHistory();
    }
    
    
    function _loadHistory(showDiffFunction) {
        Bdf.Ajax.loadHistory(args.subsetKey, args.id, function (history) {
            $revisions.html(Bdf.render("history:revisions", {
                history: history,
                historyId: historyId
            }));
            $$($revisions, {_element: "input", _name: "before"}).click(_clickBefore);
            $$($revisions, {_element: "input", _name: "after"}).click(_clickAfter);
            $$($revisions, {_element: "button", action: "show"}).click(_show);
            if (showDiffFunction) {
                $$(historyId, "button_showdiff").click(function () {
                    showDiffFunction(historyId, args);
                });
            }
        });
    }
    
    function _clickBefore() {
        let beforeIndex = this.dataset.index;
        $(".history-BeforeRow").removeClass("history-BeforeRow");
        $$($revisions, {_element: "tr", index: beforeIndex}).addClass("history-BeforeRow");
        let currentAfter = $$.first($revisions, {_element: "input", _name: "after", _checked: true});
        if ((currentAfter) && (currentAfter.dataset.index >= beforeIndex)) {
            $$($revisions, {_element: "input", _name: "after", index: (beforeIndex - 1)}).click();
        }
    }
    
    function _clickAfter() {
        let afterIndex = this.dataset.index;
        $(".history-AfterRow").removeClass("history-AfterRow");
        $$($revisions, {_element: "tr", index: afterIndex}).addClass("history-AfterRow");
        let currentBefore = $$.first($revisions, {_element: "input", _name: "before", _checked: true});
        if ((currentBefore) && (currentBefore.dataset.index <= afterIndex)) {
            $$($revisions, {_element: "input", _name: "before", index: (afterIndex + 1)}).click();
        }
    }
    
    function _show() {
        let index = this.dataset.index;
        let revisionName = this.dataset.revisionName;
        let $revisionBlock =  $$($revisions, {role: "content", revisionName: revisionName});
        if ($revisionBlock.length > 0) {
            History.hide($revisionBlock, !$revisionBlock.hasClass("hidden"));
        } else if (getRevisionFunction) {
            getRevisionFunction(revisionName, args, function (html) {
                $$($revisions, {_element: "tr", index: index}).after(html);
            });
        }
    }
    
};

History.hide = function ($element, hide) {
    if (hide) {
        $element.addClass("hidden");
    } else {
        $element.removeClass("hidden");
    }
};

Bdf.addTemplateOptions({
    helpers: {
        locRevision: function(name) {
            if (name === 'current') {
                return Bdf.Loc.get('_ label.history.revisionname_current');
            } else if (name === 'penultimate') {
                return Bdf.Loc.get('_ label.history.revisionname_penultimate');
            } else if (name.indexOf("v_") ===0) {
                return Bdf.Loc.get('_ label.history.revisionname_version', name.substring(2));
            } else {
                return name;
            }
        }
    },
    converters: {
        localDate:  function (time) {
            if (!time) {
                return "";
            }
            var date = new Date(time);
            var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
            return date.toLocaleDateString(window.navigator.language, options);
        },
        localTime: function (time) {
            if (!time) {
                return "";
            }
            var date = new Date(time);
            return date.toLocaleTimeString();
        }
    }
});

$(function () {
    Bdf.initTemplates();
    History.init(History.ARGS);
});
