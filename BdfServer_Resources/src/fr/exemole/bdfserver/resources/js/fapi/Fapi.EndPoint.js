/* global Fapi */

Fapi.EndPoint = function (url, jsonName, version) {
    this.url = url;
    this.jsonName = jsonName;
    this.version = version;
};