/* global Fapi, Fql */

/**
 * Contient des méthodes statiques pour l'interrogation de l'API du serveur BDF.
 * Les options sont les suivantes :
 * - requestParameters : paramètres supplémentaires passés à la requête
 * - callback: fonction appelée à la fin du traitement
 * 
 * @type Object
 */
Fapi.Ajax = {};


/**
 * Retourne l'identité du l'utilisateur actuellement connecté
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {Object} options Options de la requête
 * @param {function} options.callback Fonction de rappel
 * @param {Object} options.requestParameters Surcharge des paramètres de la requête
 * @param {Object} options.ignoreLogError Ne pas tenir compte des erreurs
 * @returns {undefined}
 */
Fapi.Ajax.ping = function (fapi, options) {
    var requestParameters = Fapi.Ajax.buildRequestParameters({jsonName: "ping", version: "action"}, options);
    $.ajax({
        url: fapi.getActionUrl("session"),
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: true
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, false);
        }
    });
};


/**
 * Déconnecte l'utilisateur en cours
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {Object} options Options de la requête
 * @param {function} options.callback Fonction de rappel
 * @param {Object} options.requestParameters Surcharge des paramètres de la requête
 * @param {Object} options.ignoreLogError Ne pas tenir compte des erreurs
 * @returns {undefined}
 */
Fapi.Ajax.exit = function (fapi, options) {
    var requestParameters = Fapi.Ajax.buildRequestParameters({jsonName: "ping", version: "action"}, options);
    requestParameters["bdf-exit"] = 1;
    $.ajax({
        url: fapi.getActionUrl("session"),
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: true
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, false);
        }
    });
};


/**
 * Récupère une fiches seule
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {string} corpus Nom du corpus
 * @param {number} id Numéro de la fiche
 * @param {function} options.callback Fonction de rappel
 * @param {Object} options.requestParameters Surcharge des paramètres de la requête
 * @param {Object} options.ignoreLogError Ne pas tenir compte des erreurs
 * @returns {undefined}
 */
Fapi.Ajax.loadFiche = function (fapi, options) {
    var endPoint = fapi.getEndPoint("fiche");
    var requestParameters = Fapi.Ajax.buildRequestParameters(endPoint, options);
    $.ajax({
        url: endPoint.url,
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: !fapi.isPublicApi()
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, true);
        }
    });
};


/**
 * Récupère le tableau des fiches correspondantes aux critères de sélection
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {Fql.FicheQuery} ficheQuery 
 * @param {Object} options Options de la requête
 * @param {function} options.callback Fonction de rappel
 * @param {Object} options.requestParameters Surcharge des paramètres de la requête
 * @param {Object} options.ignoreLogError Ne pas tenir compte des erreurs
 * @returns {undefined}
 */
Fapi.Ajax.loadFicheArray = function (fapi, ficheQuery, options) {
    var endPoint = fapi.getEndPoint("fiche-array");
    var requestParameters = Fapi.Ajax.buildRequestParameters(endPoint, options);
    requestParameters.xml = ficheQuery.toXml(-999);
    $.ajax({
        url: endPoint.url,
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: !fapi.isPublicApi()
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, true);
        }
    });
};


Fapi.Ajax.loadMotcle = function (fapi, options) {
    var endPoint = fapi.getEndPoint("motcle");
    var requestParameters = Fapi.Ajax.buildRequestParameters(endPoint, options);
    $.ajax({
        url: endPoint.url,
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: !fapi.isPublicApi()
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, true);
        }
    });
};


/**
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {Fql.MotcleQuery} motcleQuery
 * @param {Object} options Options de la requête
 * @returns {undefined}
 */
Fapi.Ajax.loadMotcleArray = function (fapi, motcleQuery, options) {
    var endPoint = fapi.getEndPoint("motcle-array");
    var requestParameters = Fapi.Ajax.buildRequestParameters(endPoint, options);
    requestParameters.xml = motcleQuery.toXml(-999);
    $.ajax({
        url: endPoint.url,
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: !fapi.isPublicApi()
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, true);
        }
    });
};


/**
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {String} thesaurusName Nom du thésaurus
 * @param {Object} options Options de la requête
 * @param {function} options.callback Fonction de rappel
 * @param {Object} options.requestParameters Surcharge des paramètres de la requête
 * @param {Object} options.ignoreLogError Ne pas tenir compte des erreurs
 * @returns {undefined}
 */
Fapi.Ajax.loadThesaurus = function (fapi, thesaurusName, options) {
    var endPoint = fapi.getEndPoint("thesaurus");
    var requestParameters = Fapi.Ajax.buildRequestParameters(endPoint, options);
    requestParameters.thesaurus = thesaurusName;        
    $.ajax({
        url: endPoint.url,
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: !fapi.isPublicApi()
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, true);
        }
    });
};


Fapi.Ajax.loadFiches = function (fapi, queries, options) {
    var endPoint = fapi.getEndPoint("fiches");
    var requestParameters = Fapi.Ajax.buildRequestParameters(endPoint, options);
    var xml;
    if (typeof queries === "string") {
        xml = queries;
    } else if (queries instanceof Fql.FicheQuery) {
        xml = queries.toXml(-999);
    } else if (Array.isArray(queries)) {
        xml = "<queries>";
        for(let query of queries) {
            if (typeof query === "string") {
                xml += query;
            } else if (query instanceof Fql.FicheQuery) {
                xml  += query.toXml(-999);
            } else if (typeof query === "object") {
                let ficheQuery = new Fql.FicheQuery(query);
                xml  += ficheQuery.toXml(-999);
            }
        }
        xml += "</queries>";
    } else if (typeof queries === "object") {
        let ficheQuery = new Fql.FicheQuery(queries);
        xml = ficheQuery.toXml(-999);
    }
    requestParameters.xml = xml;
    $.ajax({
        url: endPoint.url,
        method: "GET",
        dataType: "json",
        data: requestParameters,
        xhrFields: {
           withCredentials: !fapi.isPublicApi()
        },
        success: function (data, textStatus) {
            Fapi.Ajax.handleResponseData(data, options, true);
        }
    });
};


/**
 * Construit l'objet destiné à la propriété data d'une requête Ajax.
 * Si l'objet options existe, rajoute les propriétés de la propriété requestParameters de l'objet options
 * 
 * @param {String} jsonName
 * @param {Object} options Objet définissant les options
 * @param {Object} options.requestParameters Surcharge des paramètres de la requête
 * @param {Object} defaultRequestParameters Paramètres par défaut de la requête
 * @returns {Object} Objet construit
 */
Fapi.Ajax.buildRequestParameters = function (endPoint, options, defaultRequestParameters) {
    var requestParameters = {};
    if (defaultRequestParameters) {
        Object.assign(requestParameters, defaultRequestParameters);
    }
    if (options) {
        if (options.requestParameters) {
            Object.assign(requestParameters, options.requestParameters);
        }
        if (options.propertiesMapping) {
            if (endPoint.version === "action")  {
                let propertiesMapping = options.propertiesMapping;
                if (propertiesMapping.indexOf(':') === -1) {
                    propertiesMapping = "tableexport:" + propertiesMapping;
                }
                requestParameters.properties = propertiesMapping;
            }
        }
    }
    if (endPoint.jsonName) {
        requestParameters.json = endPoint.jsonName;
    }
    return requestParameters;
};

/**
 * Traite les données de la réponse Ajax
 * 
 * @param {Object} data Données transmises par la réponse
 * @param {Object} options Options du traitement de la réponse
 * @param {function} options.callback Fonction de rappel appliquée aux données de la réponse
 * @param {function} options.ignoreLogError Ne pas tenir compte des erreurs (surcharge logError)
 * @param {boolean} logError Ne pas tenir compte des erreurs par défaut
 * @returns {undefined}
 */
Fapi.Ajax.handleResponseData = function (data, options, logError) {
    if (!options) {
        return;
    }
    if (logError && (!options.ignoreLogError) && (Fapi.hasErrorCommandMessage(data))) {
        Fapi.logCommandMessage(data.commandMessage);
        return;
    }
    if (options.callback) {
        options.callback(data);
    }
};
