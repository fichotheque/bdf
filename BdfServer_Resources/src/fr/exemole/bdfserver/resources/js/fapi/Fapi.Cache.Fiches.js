/* global Fapi,Fql */

Fapi.Cache.Fiches = function (apiData) {
    this.apiData = apiData;
    var codeMap = new Map();
    this.codeMap = codeMap;
    this.firstCorpus = null;
    for(let entry of apiData.fiches.entries) {
        for(let fiche of entry.array) {
            codeMap.set(fiche.corpus + "/" + fiche.id, fiche);
        }
    }
    if (apiData.fiches.entries.length > 0) {
        this.firstCorpus = apiData.fiches.entries[0].name;
    }
};

Fapi.Cache.Fiches.prototype.getFicheCount = function () {
    return this.codeMap.size;
};

Fapi.Cache.Fiches.prototype.getFiche = function (corpus, id) {
    if (this.codeMap.size === 0) {
        return null;
    }
    if (arguments.length === 1) {
        id = corpus;
        corpus = this.firstCorpus;
    }
    return this.codeMap.get(corpus + "/" + id);
};

Fapi.Cache.Fiches.prototype.getCorpusEntry = function (corpus) {
    if (!corpus) {
        corpus = this.firstCorpus;
    }
    for(let entry of this.apiData.fiches.entries) {
        if (entry.name === corpus) {
            return entry;
        }
    }
    return null;
};

Fapi.Cache.Fiches.prototype.getFicheArray = function (corpus) {
    if (this.codeMap.size === 0) {
        return [];
    }
    let entry = this.getCorpusEntry(corpus);
    if (!entry) {
        return [];
    } else {
        return entry.array;
    }
};

Fapi.Cache.Fiches.prototype.complete = function (completeFunction) {
    for(let fiche of this.codeMap.values()) {
        completeFunction(fiche);
    }
};
