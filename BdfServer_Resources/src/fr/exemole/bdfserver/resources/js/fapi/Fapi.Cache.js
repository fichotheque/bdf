/* global Fapi,Fql */

/**
 * Objet permettant de stocker des éléments récupérés par l'API Ajax * 
 * 
 * @constructor
  */
Fapi.Cache = function () {
    this.fapiMap = new Map();
    this.thesaurusMap = new Map();
    this.fichesMap = new Map();
};


/**
 * 
 * Rétrocompatibilité
 */
Fapi.Wrapper = Fapi.Cache;

Fapi.Cache.prototype.getFapi = function (name) {
    name = this.checkName(name);
    return this.fapiMap.get(name);
};


/**
 * Retourne le thésaurus correspondant à la clé en argument
 * 
 * @param {string} key Clé du thésaurus 
 * @returns {Fapi.Cache.Thesaurus} Objet Thesaurus
 */
Fapi.Cache.prototype.getThesaurus = function (key) {
    return this.thesaurusMap.get(key);
};


/**
 * Retourne l'objet Fiches correspondant à la clé en argument
 * 
 * @param {string} key Clé d'un ensemble fiches
 * @returns {Fapi.Cache.Fiches} Objet Fiches
 */
Fapi.Cache.prototype.getFiches = function (key) {
    return this.fichesMap.get(key);
};

Fapi.Cache.prototype.putFapi = function (name, fapiArguments) {
    name = this.checkName(name);
    var fapiMap = this.fapiMap;
    if (fapiMap.has(name)) {
        let currentFapi = fapiMap.get(name);
        if (fapiArguments.fichothequeUrl) {
            currentFapi.fichothequeUrl = fapiArguments.fichothequeUrl;
        }
        currentFapi.checkOptions(fapiArguments);
        return currentFapi;
    } else {
        let newFapi = new Fapi(fapiArguments);
        fapiMap.set(name, newFapi);
        return newFapi;
    }
};


/**
 * Retourne une fonction d'appel de l'API, cette fonction prend comme argument
 * une fonction de retour pour permettre le chainage 
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {string} thesaurusName Nom du thésaurus
 * @param {Object} options Options de l'appel
 * @param {string} options.thesaurusKey Clé du thésaurus à utiliser au lieu du nom du thésaurus
 * @param {string} options.propertiesMapping Nom de l'export tabulaire à utiliser pour définir les propriétés d'un mot-clé
 * @returns {function} Fonction d'appel de l'API
 */
Fapi.Cache.prototype.initThesaurus = function (fapi, thesaurusName, options) {
    var thesaurusMap = this.thesaurusMap;
    var thesaurusKey = thesaurusName;
    if ((options) && (options.thesaurusKey)) {
        thesaurusKey = options.thesaurusKey;
    }
    return function (callback) {
        var thesaurusArgs = {
            thesaurus: thesaurusName,
            callback: function (data) {
                let thesaurus = new Fapi.Cache.Thesaurus(fapi, options, data);
                thesaurusMap.set(thesaurusKey, thesaurus);
                if (callback) {
                    callback();
                }
            }
        };
        if (options) {
            if (options.propertiesMapping) {
                thesaurusArgs.propertiesMapping = options.propertiesMapping;
            }
            if (options.requestParameters) {
                thesaurusArgs.requestParameters =  Object.assign({}, options.requestParameters);
            }
        }
        fapi.loadThesaurus(thesaurusArgs);
    };
};


/**
 * Retourne une fonction d'appel de l'API, cette fonction prend comme argument
 * une fonction de retour pour permettre le chainage 
 * 
 * @param {Fapi} fapi Configuration à utiliser
 * @param {string} fichesKey Clé à utiliser pour conserver le résultat
 * @param {Object} options Options de l'appel
 * @param {Object} options.propertiesMapping Nom de l'export tabulaire à utiliser pour définir les propriétés d'un mot-clé
 * @param {Fql.FicheQuery} options.Query Instance de Fql.FicheQuery indiquant les fiches à récupérer
 * @param {Object} options.ql Objet correspondant à un objet Options utilisé en argument du constructeur de Fql.FicheQuery 
 * @returns {function} Fonction d'appel de l'API
 */
Fapi.Cache.prototype.initFiches = function (fapi, fichesKey, options) {
    var fichesMap = this.fichesMap;
    return function (callback) {
        var fichesArgs = {
            callback: function (data) {
                let fiches = new Fapi.Cache.Fiches(data);
                fichesMap.set(fichesKey,fiches);
                if (callback) {
                    callback();
                }
            }
        };
        if (options) {
            if (options.queries) {
                fichesArgs.ficheQueries = options.queries;
            }
            if (options.propertiesMapping) {
                fichesArgs.propertiesMapping = options.propertiesMapping;
            }
            if (options.requestParameters) {
                fichesArgs.requestParameters = Object.assign({}, options.requestParameters);
            }
            if (options.sort) {
                if (fichesArgs.requestParameters) {
                    fichesArgs.requestParameters.sort = options.sort;
                } else {
                    fichesArgs.requestParameters = {
                        sort: options.sort
                    };
                }
            }
        }
        if (!fichesArgs.ficheQueries) {
            fichesArgs.ficheQueries = {
                corpus: fichesKey
            };
        }
        fapi.loadFiches(fichesArgs);
    };
};

Fapi.Cache.prototype.checkName = function (name) {
    if (!name) {
        return "_current";
    }
    switch(name) {
        case "_default":
            return "_current";
        default:
            return name;
    }
};

/*
{
    fichotheques:  [
        {
            name: "",
            url: "",
            options: {
                version: "action",
                propertiesMapping: ""
            },
            thesaurus: [
                {
                    name: "",
                    options: {

                    }
                }
            ],
            fiches: [
                {
                    key: "",
                    queries: "",
                    query: "",
                    sort: ""
                }
            ]
        }
    ]
}
 */

Fapi.Cache.init = function (initObject, callback) {
    var cache = new Fapi.Cache();
    var thesaurusInitArray = [];
    var fichesInitArray = [];
    var callbackDone = false;
    if (initObject.fichotheques) {
        for(let fichotheque of initObject.fichotheques) {
            _addFichotheque(fichotheque);
        }
    } else {
        _addFichotheque(initObject);
    }
    
    if ((thesaurusInitArray.length > 0) || (fichesInitArray.length > 0)) {
        for(let thesaurusInit of thesaurusInitArray) {
            let initFunction = cache.initThesaurus(thesaurusInit.fapi, thesaurusInit.name, thesaurusInit.options);
            initFunction(function () {
               thesaurusInit.done = true;
               _checkInit();
            });
        }
        for(let fichesInit of fichesInitArray) {
            let initFunction = cache.initFiches(fichesInit.fapi, fichesInit.key, fichesInit.options);
            initFunction(function () {
               fichesInit.done = true;
               _checkInit();
            });
        }
    } else {
        callback(cache);
    }
    
    function _addFichotheque(fichotheque) {
        let fichothequeName = cache.checkName(fichotheque.name);
        let fichothequePrefix = "";
        if (fichothequeName !== "_current") {
            fichothequePrefix = fichothequeName + "/";
        }
        let fapi = cache.putFapi(fichothequeName, _buildFapiArguments(fichotheque));
        if (fichotheque.thesaurus) {
            for(let thesaurus of fichotheque.thesaurus) {
                if (typeof thesaurus === "string") {
                    thesaurusInitArray.push({
                        name: thesaurus,
                        options: {
                            thesaurusKey: fichothequePrefix + thesaurus
                        },
                        fapi: fapi,
                        done: false
                    });
                } else {
                    let name = thesaurus.name;
                    if (name) {
                        let options = thesaurus.options;
                        if (!options) {
                            options = {
                                thesaurusKey: fichothequePrefix + name
                            }
                        } else if (!options.thesaurusKey) {
                            options = Object.assign({
                                thesaurusKey: fichothequePrefix + name
                            }, options);
                        }
                        thesaurusInitArray.push({
                            name: name,
                            options: options,
                            fapi: fapi,
                            done: false
                        });
                    }
                }
            }
        }
        if (fichotheque.fiches) {
            for(let fiches of fichotheque.fiches) {
                let options = {};
                if (fiches.options) {
                    options = Object.assign(fiches.options, options);
                }
                if (fiches.query) {
                    options.queries = fiches.query;
                } else if (fiches.queries) {
                    options.queries = fiches.queries;
                }
                if (fiches.sort) {
                    options.sort = fiches.sort;
                }
                fichesInitArray.push({
                    key: fiches.key,
                    options: options,
                    fapi: fapi,
                    done: false 
                });
            }
        }
    }
    
    function _buildFapiArguments(fichotheque) {
        let fapiArguments = {};
        let fichothequeUrl = fichotheque.url;
        if (!fichothequeUrl) {
            fichothequeUrl = "";
        }
        fapiArguments.fichothequeUrl = fichothequeUrl;
        if (fichotheque.api) {
            fapiArguments.api = fichotheque.api;
        }
        let options = fichotheque.options;
        if (options) {
            for(let propKey in options) {
                fapiArguments[propKey] = options[propKey];
            }
        }
        return fapiArguments;
    }
    
    function _checkInit() {
        let allDone = true;
        if (allDone) {
            for(let thesaurusInit of thesaurusInitArray) {
                if (!thesaurusInit.done) {
                    allDone = false;
                    break;
                }
            }
        }
        if (allDone) {
            for(let fichesInit of fichesInitArray) {
                if (!fichesInit.done) {
                    allDone = false;
                    break;
                }
            }
        }
        if ((allDone) && (!callbackDone)) {
            callbackDone = true;
            callback(cache);
        }
    }

};
