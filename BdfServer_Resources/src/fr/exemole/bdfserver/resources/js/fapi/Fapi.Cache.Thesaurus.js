/* global Fapi,Fql */

Fapi.Cache.Thesaurus = function (apiConfig, options, apiData) {
    var idMap = new Map();
    var idalphaMap = new Map();
    this.apiConfig = apiConfig;
    this.options = options;
    this.apiData = apiData;
    this.idMap = idMap;
    this.idalphaMap = idalphaMap;
    _populate(apiData.thesaurus.array);


    function _populate(array) {
        for(let motcle of array) {
            idMap.set(motcle.id, motcle);
            if (motcle.idalpha) {
                idalphaMap.set(motcle.idalpha, motcle);
            }
            _populate(motcle.children);
        }
    }
    
};

Fapi.Cache.Thesaurus.prototype.getName = function () {
    return this.apiData.thesaurus.name;
};

Fapi.Cache.Thesaurus.prototype.getPropertiesMapping = function () {
    if (this.options.propertiesMapping) {
        return this.options.propertiesMapping;
    }
    if (this.apiConfig.options.propertiesMapping) {
        return this.apiConfig.options.propertiesMapping;
    }
    return "";
};

Fapi.Cache.Thesaurus.prototype.getFirstLevelArray = function () {
    return this.apiData.thesaurus.array;
};

Fapi.Cache.Thesaurus.prototype.getMotcleById = function (id) {
	return this.idMap.get(id);
};

Fapi.Cache.Thesaurus.prototype.getMotcleByIdalpha = function (idalpha) {
    return this.idalphaMap.get(idalpha);
};

Fapi.Cache.Thesaurus.prototype.complete = function (completeFunction) {
    for(let motcle of this.idMap.values()) {
        completeFunction(motcle);
    }
};

Fapi.Cache.Thesaurus.prototype.toMotcleArray = function () {
    var array = new Array();
    for(let motcle of this.idMap.values()) {
        array.push(motcle);
    }
    return array;
};

