/* global Bdf,Edition,CodeMirror,Infinity,Ficheform,$$ */
/**
 * Objet global définissant l'espace de nom History
  * 
 * @namespace Zoomedit
 */
var Zoomedit = {};

Zoomedit.ARGS = {
    clientId: "",
    appelant: "",
    corpus: ""
};

Zoomedit.init = function (args) {
    var zoomeditId = Bdf.generateId();
    var appelantTextArea = _getAppelantTextArea(args.appelant); 
    var codeMirrorUnit, sectionTextArea;
    $$(args.clientId).html(Bdf.render("zoomedit:client", {
        zoomeditId: zoomeditId
    }));
    Ficheform.Section.ficheElement = window.opener.document;
    sectionTextArea = _initSectionTextArea();
    codeMirrorUnit = Ficheform.Section.initSectionCodeMirror("body", sectionTextArea, false, Zoomedit.ARGS.corpus);
    $$(zoomeditId, "save").click(_updateAppelant);
    $(window).on('unload', _updateAppelant);
    Bdf.Shortcut.checkElements("body");
    
      
    function _initSectionTextArea() {
        let textArea = $$.one(zoomeditId, "section");
        textArea.value = appelantTextArea.value;
        appelantTextArea.classList.add("ficheform-ZoomedArea");
        let spellchekAttribute = appelantTextArea.getAttribute("spellcheck");
        if (spellchekAttribute) {
            textArea.setAttribute("spellcheck", spellchekAttribute);
        }
        if (appelantTextArea.lang) {
            textArea.lang = appelantTextArea.lang;
            $$.one(zoomeditId, "body").lang = appelantTextArea.lang;
        }
        return textArea;
    }
    
    function _updateAppelant() {
        codeMirrorUnit.codeMirror.save();
        appelantTextArea.value = sectionTextArea.value;
        appelantTextArea.classList.remove("ficheform-ZoomedArea");
        window.close();
    }
    
    function _getAppelantTextArea(appelantId) {
        return window.opener.document.getElementById(appelantId);
    }
};


$(function () {
    Bdf.initTemplates();
    Zoomedit.init(Zoomedit.ARGS);
});
