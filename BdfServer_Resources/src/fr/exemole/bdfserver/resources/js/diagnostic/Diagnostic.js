/* global Bdf,$$,Pane */
/**
 * Objet global définissant l'espace de nom Resource
 * 
 * @namespace Diagnostic
 */
var Diagnostic = {};

/******************************************************************************
 * Initialisation
 ******************************************************************************/

$(function () {
    Bdf.initTemplates();
    Pane.initLayout({
        ignoreStock: true
    });
    $$("layout_list").html(Bdf.render("diagnostic:list/pane", {}));
    Bdf.runSerialFunctions(
        Diagnostic.List.init
   );
});
