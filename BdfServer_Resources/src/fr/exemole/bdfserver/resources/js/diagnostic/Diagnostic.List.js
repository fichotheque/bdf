/* global Bdf,$$,Diagnostic,Pane */

Diagnostic.List = {};

Diagnostic.List.init = function (callback) {
    Diagnostic.List.initActions();
    if (callback) {
        callback();
    }
};

Diagnostic.List.initActions = function () {
    $$("listpane_button_urlscan_selection").click(function () {
        Pane.loadOrDestock("urlscan", function () {
            Diagnostic.UrlScan.load();
        });
    });
    
};

