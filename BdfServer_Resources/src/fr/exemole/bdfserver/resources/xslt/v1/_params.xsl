<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

    <xsl:param name="BDF_FICHOTHEQUEPATH" select="''"/>
    <xsl:param name="BDF_VERSION" select="''"/>
    <xsl:param name="EXTERNALTARGET" select="''"/>
    <xsl:param name="INCLUDESCRIPTS" select="''"/>
    <xsl:param name="SHOWEMPTY" select="1"/>
    <xsl:param name="STARTLEVEL" select="0"/>
    <xsl:param name="WORKINGLANG" select="'fr'"/>
    <xsl:param name="USER_SPHERE" select="''"/>
    <xsl:param name="USER_LOGIN" select="'default'"/>

</xsl:stylesheet>
