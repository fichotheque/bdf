<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- use: $WORKINGLANG  -->
<!-- dependency: _common_ficheblock.xsl -->


<!--
********************************************************************************
* Public modes (v1)
********************************************************************************
-->

    <xsl:template match="*" mode="MODE_BLOCK">
        <xsl:param name="PARAM_CLASS" select=""/>
        <p class="{$PARAM_CLASS}"><xsl:apply-templates select="."/></p>
    </xsl:template>

    <xsl:template match="*" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <li>
            <p class="{$PARAM_CLASS}"><xsl:apply-templates select="."/></p>
        </li>
    </xsl:template>

    <xsl:template match="*" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="', '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$PARAM_SEP"/></xsl:if>
        <xsl:apply-templates select="."/>
    </xsl:template>
    
    
<!--
********************************************************************************
* Public modes (v2)
********************************************************************************
-->

    <xsl:template match="*" mode="ficheitem-Block">
        <xsl:param name="param_Class" select=""/>
        <p class="{$param_Class}"><xsl:apply-templates select="."/></p>
    </xsl:template>

    <xsl:template match="*" mode="ficheitem-Listitem">
        <xsl:param name="param_Class" select=""/>
        <li>
            <p class="{$param_Class}"><xsl:apply-templates select="."/></p>
        </li>
    </xsl:template>

    <xsl:template match="*" mode="ficheitem-Inline">
        <xsl:param name="param_Separator" select="', '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:apply-templates select="."/>
    </xsl:template>


<!--
********************************************************************************
* Default
********************************************************************************
-->

    <xsl:template match="item">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="personne">
        <xsl:apply-templates select="." mode="ficheitem-person-Name"/>
        <xsl:apply-templates select="organism"/>
    </xsl:template>

    <xsl:template match="personne[nonlatin]">
        <xsl:if test="surname or forename">
            <xsl:apply-templates select="." mode="ficheitem-person-Name"/>
            <xsl:text> / </xsl:text>
        </xsl:if>
        <xsl:value-of select="nonlatin"/>
        <xsl:apply-templates select="organism"/>
    </xsl:template>
    
    <xsl:template match="organism">
        <xsl:text> (</xsl:text><xsl:value-of select="."/><xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="langue">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="pays">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="@country"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="image[@base-url]">
        <img src="{@base-url}{@src}" alt="{alt}" title="{title}"/>
    </xsl:template>

    <xsl:template match="image">
        <img src="{@src}" alt="{alt}" title="{title}"/>
    </xsl:template>

    <xsl:template match="para">
        <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
    </xsl:template>

    <xsl:template match="datation">
        <time datetime="{@iso}">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
        </time>
    </xsl:template>

    <xsl:template match="montant">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="nombre">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="geopoint">
        <a href="https://www.openstreetmap.org/?mlat={@lat}&amp;mlon={@lon}#map=12/{@lat}/{@lon}" target="_blank">
            <xsl:apply-templates select="latitude"/><xsl:text> &#x00A0; </xsl:text><xsl:apply-templates select="longitude"/>
        </a>
    </xsl:template>

    <xsl:template match="latitude">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="../@lat"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="longitude">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="../@lon"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="courriel">
        <a href="mailto:{.}"><xsl:value-of select="."/></a>
    </xsl:template>
    
    <xsl:template match="link[@base-url]">
        <a href="{@base-url}{@href}" target="_blank"><xsl:value-of select="title"/></a>
        <xsl:if test="string-length(comment) &gt; 0"><xsl:text> (</xsl:text><xsl:value-of select="comment"/>)</xsl:if>
    </xsl:template>

    <xsl:template match="link">
        <a href="{@href}" target="_blank"><xsl:value-of select="title"/></a>
        <xsl:if test="string-length(comment) &gt; 0"><xsl:text> (</xsl:text><xsl:value-of select="comment"/>)</xsl:if>
    </xsl:template>


 <!--
********************************************************************************
* ficheitem-person-
********************************************************************************
-->
    
    <xsl:template match="personne[surname/@surname-first]" mode="ficheitem-person-Name">
        <xsl:value-of select="surname"/>
        <xsl:if test="forename">
            <xsl:text> </xsl:text>
            <xsl:value-of select="forename"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="personne[not(surname)]" mode="ficheitem-person-Name">
        <xsl:value-of select="forename"/>
    </xsl:template>

    <xsl:template match="personne" mode="ficheitem-person-Name">
        <xsl:if test="forename">
            <xsl:value-of select="forename"/>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:value-of select="surname"/>
    </xsl:template>


</xsl:stylesheet>