<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v1/_params.xsl"/>
    <xsl:import href="bdf://this/xslt/v1/_common_ficheitem.xsl"/>
    <xsl:import href="bdf://this/xslt/v1/_common_ficheblock.xsl"/>
    <xsl:import href="bdf://this/xslt/v1/_common_field.xsl"/>
    <xsl:import href="bdf://this/xslt/v1/_common_include.xsl"/>
    <xsl:import href="bdf://this/xslt/v1/_common_motcle.xsl"/>
    
    <xsl:include href="bdf://this/xslt/v1/_scripts.xsl"/>
    <xsl:include href="bdf://this/xslt/v1/_urls.xsl"/>
    <xsl:include href="bdf://this/custom/import.xsl"/>
</xsl:stylesheet>
