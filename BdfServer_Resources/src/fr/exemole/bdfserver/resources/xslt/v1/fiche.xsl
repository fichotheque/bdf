<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v1/_import_fiche.xsl"/>
    
    <xsl:output method="html" encoding="UTF-8" indent="no"/>

    <xsl:variable name="LABELS" select="document(concat('bdf://this/dyn/labels/corpus_',/extraction/*[@corpus][1]/@corpus, '_', $WORKINGLANG,'.xml'))/labels"/>

    <xsl:variable name="USER" select="document(concat('bdf://this/dyn/users/', $USER_SPHERE, '_', $USER_LOGIN, '/ui/corpus_', /extraction/*[@corpus][1]/@corpus, '.xml'))/user"/>
    
    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;&#10;</xsl:text>
        <html>
            <head>
                <title>
                    <xsl:apply-templates select="extraction/extraitcorpus/fiche|extraction/fiches/fiche|extraction/fiche" mode="include-fiche-Phrase"/>
                </title>
                <xsl:call-template name="SCRIPTS"/>
                <style type="text/css">
                    <xsl:text>&#10;</xsl:text>
                    <xsl:value-of select="document('bdf://this/xml-pack/css/_ficheblockelements.css')/xml-pack" disable-output-escaping="yes"/>
                    <xsl:value-of select="document('bdf://this/xml-pack/css/_predefinedclasses.css')/xml-pack" disable-output-escaping="yes"/>
                    <xsl:value-of select="document('bdf://this/xml-pack/theme/css/_codemirror.css')/xml-pack" disable-output-escaping="yes"/>
                    <xsl:value-of select="document('bdf://this/xml-pack/css/fiche-elements.css')/xml-pack" disable-output-escaping="yes"/>
                    <xsl:value-of select="document('bdf://this/xml-pack/css/fiche-classes.css')/xml-pack" disable-output-escaping="yes"/>
                    <xsl:value-of select="document('bdf://this/xml-pack/custom/fiche.css')/xml-pack" disable-output-escaping="yes"/>
                </style>
                <xsl:apply-templates select="." mode="custom-Head"/>
                <xsl:apply-templates select="." mode="_Head"/>
            </head>
            <body>
                <xsl:apply-templates select="extraction/extraitcorpus/fiche|extraction/fiches/fiche|extraction/fiche"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="/" mode="_Head">
    </xsl:template>
        
</xsl:stylesheet>