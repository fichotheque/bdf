<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" version="1.0">


<!--
********************************************************************************
* Public modes (v1)
********************************************************************************
-->

    <xsl:template match="extraitcorpus|fiches" mode="MODE_TITLE">
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="include-Title">
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>


    <xsl:template match="fiche" mode="MODE_TITLE">
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="include-fiche-Title">
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="MODE_BLOCK">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="." mode="include-Block">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="." mode="include-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' ; '"/>
        <xsl:apply-templates select="." mode="include-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="fiche" mode="MODE_BLOCK">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="." mode="include-fiche-Block">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="fiche" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="." mode="include-fiche-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="fiche" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' ; '"/>
        <xsl:apply-templates select="." mode="include-fiche-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="fiche" mode="MODE_FICHELINK">
        <xsl:apply-templates select="." mode="include-fiche-Link"/>
    </xsl:template>

    <xsl:template match="extraitaddenda|documents" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:param name="PARAM_VERSIONSEP" select="' / '"/>
        <xsl:apply-templates select="." mode="include-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
            <xsl:with-param name="param_VersionSeparator" select="$PARAM_VERSIONSEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitaddenda|documents" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' ; '"/>
        <xsl:param name="PARAM_VERSIONSEP" select="' / '"/>
        <xsl:apply-templates select="." mode="include-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
            <xsl:with-param name="param_VersionSeparator" select="$PARAM_VERSIONSEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="document" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:param name="PARAM_VERSIONSEP" select="' / '"/>
        <xsl:apply-templates select="." mode="include-document-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
            <xsl:with-param name="param_VersionSeparator" select="$PARAM_VERSIONSEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="document" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' ; '"/>
        <xsl:param name="PARAM_VERSIONSEP" select="' / '"/>
        <xsl:apply-templates select="." mode="include-document-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
            <xsl:with-param name="param_VersionSeparator" select="$PARAM_VERSIONSEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="document" mode="MODE_DOCUMENTLINK">
        <xsl:param name="PARAM_VERSIONSEP" select="' / '"/>
        <xsl:apply-templates select="." mode="include-document-link">
            <xsl:with-param name="param_VersionSeparator" select="$PARAM_VERSIONSEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="version" mode="MODE_VERSIONLINK">
        <xsl:param name="PARAM_VERSIONSEP" select="' / '"/>
        <xsl:apply-templates select="." mode="include-document-link">
            <xsl:with-param name="param_VersionSeparator" select="$PARAM_VERSIONSEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="version" mode="MODE_VERSIONSIZE">
        <xsl:apply-templates select="." mode="include-document-Size"/>
    </xsl:template>

    <xsl:template match="size" mode="MODE_VERSIONSIZE">
        <xsl:apply-templates select="." mode="include-document-LangLabel"/>
    </xsl:template>
    
    <xsl:template match="extraitalbum|illustrations" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="." mode="include-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitalbum|illustrations" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' '"/>
        <xsl:apply-templates select="." mode="include-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="illustration" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="." mode="include-illustration-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="illustration" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' '"/>
        <xsl:apply-templates select="." mode="include-illustration-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
        </xsl:apply-templates>
    </xsl:template>   

    <xsl:template match="intitule" mode="MODE_LABEL">
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="include-LangLabel">
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>


<!--
********************************************************************************
* Public modes (v2)
********************************************************************************
-->

    <xsl:template match="extraitcorpus|fiches" mode="include-Title">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="intitule|title" mode="include-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="include-Block">
        <xsl:param name="param_Class" select=""/>
        <xsl:apply-templates select="fiche" mode="include-fiche-Block">
            <xsl:with-param name="param_Class" select="$param_Class"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="include-Listitem">
        <xsl:param name="param_Class" select=""/>
        <xsl:apply-templates select="fiche" mode="include-fiche-Listitem">
            <xsl:with-param name="param_Class" select="$param_Class"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="include-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:apply-templates select="fiche" mode="include-fiche-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitaddenda|documents" mode="include-Listitem">
        <xsl:param name="param_Class" select=""/>
        <xsl:param name="param_VersionSeparator" select="' / '"/>
        <xsl:apply-templates select="document" mode="include-document-Listitem">
            <xsl:with-param name="param_Class" select="$param_Class"/>
            <xsl:with-param name="param_VersionSeparator" select="$param_VersionSeparator"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitaddenda|documents" mode="include-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:param name="param_VersionSeparator" select="' / '"/>
        <xsl:apply-templates select="document" mode="include-document-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
            <xsl:with-param name="param_VersionSeparator" select="$param_VersionSeparator"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="extraitalbum|illustrations" mode="include-Listitem">
        <xsl:param name="param_Class" select=""/>
        <xsl:apply-templates select="illustration" mode="include-illustration-Listitem">
            <xsl:with-param name="param_Class" select="$param_Class"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitalbum|illustrations" mode="include-Inline">
        <xsl:param name="param_Separator" select="' '"/>
        <xsl:apply-templates select="illustration" mode="include-illustration-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="intitule|title|phrase" mode="include-LangLabel">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $param_Lang]"><xsl:value-of select="label[@xml:lang = $param_Lang]"/></xsl:when>
            <xsl:when test="label[@xml:lang = substring-before($param_Lang, '-')]"><xsl:value-of select="label[@xml:lang = substring-before($param_Lang, '-')]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="label[1]"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

<!--
********************************************************************************
* include-fiche-
********************************************************************************
-->

    <xsl:template match="fiche" mode="include-fiche-Block">
        <xsl:param name="param_Class" select=""/>
        <text:p text:style-name="{$param_Class}"><xsl:apply-templates select="." mode="include-fiche-Link"/></text:p>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Listitem">
        <xsl:param name="param_Class" select=""/>
        <text:list-item><text:p text:style-name="{$param_Class}"><xsl:apply-templates select="." mode="include-fiche-Link"/></text:p></text:list-item>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:apply-templates select="." mode="include-fiche-Link"/>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Link">
        <text:a xlink:type="simple">
            <xsl:attribute name="xlink:href">
                <xsl:call-template name="TEMPLATE_FICHEURL">
                    <xsl:with-param name="PARAM_CORPUS" select="@corpus"/>
                    <xsl:with-param name="PARAM_ID" select="@id"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="include-fiche-Title"/>
        </text:a>
    </xsl:template>
    
    <xsl:template match="fiche" mode="include-fiche-Title">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="include-fiche-Phrase">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
        <xsl:text> – </xsl:text>
        <xsl:value-of select="titre"/>
    </xsl:template>
    
    <xsl:template match="fiche" mode="include-fiche-Phrase">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="intitule|phrase" mode="include-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    
<!--
********************************************************************************
* include-document-
********************************************************************************
-->

<xsl:template match="document" mode="include-document-Listitem">
        <xsl:param name="param_Class" select=""/>
        <xsl:param name="param_VersionSeparator" select="' / '"/>
        <text:list-item><text:p text:style-name="{$param_Class}">
                <xsl:apply-templates select="." mode="include-document-Link">
                    <xsl:with-param name="param_VersionSeparator" select="$param_VersionSeparator"/>
                </xsl:apply-templates>
        </text:p></text:list-item>
    </xsl:template>

    <xsl:template match="document" mode="include-document-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:param name="param_VersionSeparator" select="' / '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:apply-templates select="." mode="include-document-Link">
            <xsl:with-param name="param_VersionSeparator" select="$param_VersionSeparator"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="document" mode="include-document-Link">
        <xsl:param name="param_VersionSeparator" select="' / '"/>
        <xsl:value-of select="@basename"/><xsl:text> [&#x00A0;</xsl:text>
        <xsl:apply-templates select="version" mode="include-document-Link">
            <xsl:with-param name="param_VersionSeparator" select="$param_VersionSeparator"/>
        </xsl:apply-templates>
        <xsl:text>&#x00A0;]</xsl:text>
    </xsl:template>

    <xsl:template match="version" mode="include-document-Link">
        <xsl:param name="param_VersionSeparator" select="' / '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_VersionSeparator"/></xsl:if>
        <text:a xlink:type="simple">
            <xsl:attribute name="xlink:href">
                <xsl:call-template name="TEMPLATE_VERSIONURL">
                    <xsl:with-param name="PARAM_ADDENDA" select="../@addenda"/>
                    <xsl:with-param name="PARAM_ID" select="../@id"/>
                    <xsl:with-param name="PARAM_BASENAME" select="../@basename"/>
                    <xsl:with-param name="PARAM_EXTENSION" select="@extension"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:text>.</xsl:text><xsl:value-of select="@extension"/>
        </text:a>
        <xsl:text> (</xsl:text><xsl:apply-templates select="." mode="include-document-Size"/>)
    </xsl:template>

    <xsl:template match="version" mode="include-document-Size">
        <xsl:apply-templates select="size" mode="include-document-LangLabel"/>
    </xsl:template>

    <xsl:template match="size" mode="include-document-LangLabel">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>


<!--
********************************************************************************
* include-illustration-
********************************************************************************
-->

    <xsl:template match="illustration" mode="include-illustration-Listitem">
        <xsl:param name="param_Class" select=""/>
        <text:list-item><text:p text:style-name="{$param_Class}">
            <xsl:apply-templates select="." mode="include-illustration-Inline"/>
        </text:p></text:list-item>
    </xsl:template>

    <xsl:template match="illustration" mode="include-illustration-Inline">
        <draw:frame draw:name="{generate-id(.)}" draw:style-name="FbeIllustration" text:anchor-type="as-char" svg:width="{@ratio}cm" svg:height="1cm" draw:z-index="0">
            <draw:image xlink:href="bdf://this/illustrations/{@album}-{@id}" xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad"/>
        </draw:frame>
    </xsl:template>

</xsl:stylesheet>