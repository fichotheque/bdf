<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v1/_import_section.xsl"/>
    
    <xsl:output method="html" encoding="UTF-8" indent="no"/>
    
    <xsl:template match="/">
        <xsl:apply-templates select="section/*"/>
    </xsl:template>
</xsl:stylesheet>
