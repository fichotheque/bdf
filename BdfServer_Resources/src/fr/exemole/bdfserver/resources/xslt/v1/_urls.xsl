<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:template name="TEMPLATE_FICHEURL">
		<xsl:param name="PARAM_CORPUS"/>
		<xsl:param name="PARAM_ID"/>
                <xsl:param name="PARAM_IDALPHA"/>
		<xsl:param name="PARAM_FRAGMENT" select="''"/>
		<xsl:value-of select="$BDF_FICHOTHEQUEPATH"/><xsl:text>fiches/</xsl:text><xsl:value-of select="$PARAM_CORPUS"/><xsl:text>-</xsl:text><xsl:value-of select="$PARAM_ID"/><xsl:text>.html</xsl:text><xsl:if test="string-length($PARAM_FRAGMENT) &gt; 0">
			<xsl:text>#</xsl:text><xsl:value-of select="$PARAM_FRAGMENT"/></xsl:if>
	</xsl:template>

	<xsl:template name="TEMPLATE_MOTCLEURL">
		<xsl:param name="PARAM_THESAURUS"/>
		<xsl:param name="PARAM_ID"/>
                <xsl:param name="PARAM_IDALPHA"/>
		<xsl:param name="PARAM_FRAGMENT" select="''"/>
		<xsl:text>#</xsl:text><xsl:value-of select="$PARAM_FRAGMENT"/>
	</xsl:template>

	<xsl:template name="TEMPLATE_ILLUSTRATIONURL">
		<xsl:param name="PARAM_ALBUM"/>
		<xsl:param name="PARAM_ID"/>
		<xsl:param name="PARAM_FORMAT"/>
		<xsl:param name="PARAM_DIM" select="''"/>
		<xsl:value-of select="$BDF_FICHOTHEQUEPATH"/><xsl:text>illustrations/</xsl:text><xsl:if test="string-length($PARAM_DIM) &gt; 0"><xsl:value-of select="$PARAM_DIM"/><xsl:text>/</xsl:text></xsl:if><xsl:value-of select="$PARAM_ALBUM"/><xsl:text>-</xsl:text><xsl:value-of select="$PARAM_ID"/><xsl:text>.</xsl:text><xsl:value-of select="$PARAM_FORMAT"/>
	</xsl:template>

	<xsl:template name="TEMPLATE_VERSIONURL">
		<xsl:param name="PARAM_ADDENDA"/>
		<xsl:param name="PARAM_ID"/>
		<xsl:param name="PARAM_BASENAME"/>
		<xsl:param name="PARAM_EXTENSION"/>
		<xsl:value-of select="$BDF_FICHOTHEQUEPATH"/>documents/<xsl:value-of select="$PARAM_ADDENDA"/>/<xsl:value-of select="$PARAM_BASENAME"/>.<xsl:value-of select="$PARAM_EXTENSION"/>
	</xsl:template>

</xsl:stylesheet>

