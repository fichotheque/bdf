<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0">

    <!-- nécessite la variable $WORKINGLANG -->

<!--
********************************************************************************
* public modes (v1)
********************************************************************************
-->

    <xsl:template match="*" mode="MODE_BLOCK">
        <xsl:param name="PARAM_CLASS" select=""/>
        <text:p text:style-name="{$PARAM_CLASS}"><xsl:apply-templates select="."/></text:p>
    </xsl:template>

    <xsl:template match="*" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <text:list-item><text:p text:style-name="{$PARAM_CLASS}"><xsl:apply-templates select="."/></text:p></text:list-item>
    </xsl:template>

    <xsl:template match="*" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="', '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$PARAM_SEP"/></xsl:if>
        <xsl:apply-templates select="."/>
    </xsl:template>
    
    
<!--
********************************************************************************
* public modes (v2)
********************************************************************************
-->

    <xsl:template match="*" mode="ficheitem-Block">
        <xsl:param name="param_Class" select=""/>
        <text:p text:style-name="{$param_Class}"><xsl:apply-templates select="."/></text:p>
    </xsl:template>

    <xsl:template match="*" mode="ficheitem-Listitem">
        <xsl:param name="param_Class" select=""/>
        <text:list-item><text:p text:style-name="{$param_Class}"><xsl:apply-templates select="."/></text:p></text:list-item>
    </xsl:template>

    <xsl:template match="*" mode="ficheitem-Inline">
        <xsl:param name="param_Separator" select="', '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:apply-templates select="."/>
    </xsl:template>


<!--
********************************************************************************
* Default
********************************************************************************
-->

    <xsl:template match="item">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="personne">
        <xsl:apply-templates select="." mode="ficheitem-person-Name"/>
        <xsl:apply-templates select="organism"/>
    </xsl:template>
    
    <xsl:template match="personne[nonlatin]">
        <xsl:if test="surname or forename">
            <xsl:apply-templates select="." mode="ficheitem-person-Name"/>
            <xsl:text> / </xsl:text>
        </xsl:if>
        <xsl:value-of select="nonlatin"/>
        <xsl:apply-templates select="organism"/>
    </xsl:template>

    <xsl:template match="organism">
        <xsl:text> (</xsl:text><xsl:value-of select="."/><xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="langue">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="@lang"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="pays">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="@country"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="image[@base-url]">
        <draw:frame draw:name="{generate-id(.)}" draw:style-name="FbeImage" text:anchor-type="as-char" draw:z-index="0" svg:width="2cm" svg:height="3cm">
            <draw:image xlink:href="{@base-url}{@src}" xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad"/>
        </draw:frame>
    </xsl:template>

    <xsl:template match="image">
        <xsl:value-of select="@src"/>
    </xsl:template>

    <xsl:template match="para">
        <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
    </xsl:template>

    <xsl:template match="datation">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="montant">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="nombre">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="geopoint">
        <xsl:apply-templates select="latitude"/><xsl:text> </xsl:text><xsl:apply-templates select="longitude"/>
    </xsl:template>

    <xsl:template match="latitude">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="../@lat"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="longitude">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="../@lon"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="courriel">
        <text:a xlink:type="simple" xlink:href="mailto:{.}"><xsl:value-of select="."/></text:a>
    </xsl:template>
    
    <xsl:template match="link[@base-url]">
        <text:a xlink:type="simple" xlink:href="{@base-url}{@href}"><xsl:value-of select="title"/></text:a>
        <xsl:if test="string-length(comment) &gt; 0"><xsl:text> (</xsl:text><xsl:value-of select="comment"/>)</xsl:if>
    </xsl:template>

    <xsl:template match="link">
        <text:a xlink:type="simple" xlink:href="{@href}"><xsl:value-of select="title"/></text:a>
        <xsl:if test="string-length(comment) &gt; 0"><xsl:text> (</xsl:text><xsl:value-of select="comment"/>)</xsl:if>
    </xsl:template>


 <!--
********************************************************************************
* ficheitem-person-
********************************************************************************
-->

    <xsl:template match="personne[surname/@surname-first]" mode="ficheitem-person-Name">
        <xsl:value-of select="surname"/>
        <xsl:if test="forename">
            <xsl:text> </xsl:text>
            <xsl:value-of select="forename"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="personne[not(surname)]" mode="ficheitem-person-Name">
        <xsl:value-of select="forename"/>
    </xsl:template>

    <xsl:template match="personne" mode="ficheitem-person-Name">
        <xsl:if test="forename">
            <xsl:value-of select="forename"/>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:value-of select="surname"/>
    </xsl:template>
    
</xsl:stylesheet>