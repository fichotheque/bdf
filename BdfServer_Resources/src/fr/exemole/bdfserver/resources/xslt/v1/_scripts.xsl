<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

        <xsl:template name="SCRIPTS">
            <xsl:param name="PARAM_INCLUDE" select="$INCLUDESCRIPTS"/>
            <xsl:if test="string-length($PARAM_INCLUDE) &gt; 0">
                <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/jquery/jquery.js?v={$BDF_VERSION}"></script>
                <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}js/extraction/Extraction.js?v={$BDF_VERSION}"></script>
                <xsl:if test="contains($PARAM_INCLUDE, 'codemirror')">
                    <xsl:call-template name="SCRIPTS_CODEMIRROR"/>
                </xsl:if>
            </xsl:if>
        </xsl:template>
    
	<xsl:template name="SCRIPTS_CODEMIRROR">
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/lib/codemirror.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/mode/meta.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/mode/xml/xml.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/mode/css/css.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/mode/javascript/javascript.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/mode/properties/properties.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/mode/clike/clike.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/mode/htmlmixed/htmlmixed.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}js/codemirror/tableexport.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}js/codemirror/ficheblock.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}js/codemirror/attributes.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}js/codemirror/uicomponents.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}js/codemirror/subsettree.js?v={$BDF_VERSION}"></script>
            <script type="text/javascript" src="{$BDF_FICHOTHEQUEPATH}js/extraction/Extraction.CodeMirror.js?v={$BDF_VERSION}"></script>
            <link href="{$BDF_FICHOTHEQUEPATH}third-lib/codemirror/lib/codemirror.css?v={$BDF_VERSION}" rel="stylesheet" type="text/css"/>
	</xsl:template>

</xsl:stylesheet>


