<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v1/_params.xsl"/>
    <xsl:import href="bdf://this/xslt/v1/_common_ficheblock_odt.xsl"/>
    <xsl:import href="bdf://this/xslt/v1/_common_motcle_odt.xsl"/>
    <xsl:include href="bdf://this/xslt/v1/_urls.xsl"/>
    <xsl:include href="bdf://this/custom/import_odt.xsl"/>
</xsl:stylesheet>
