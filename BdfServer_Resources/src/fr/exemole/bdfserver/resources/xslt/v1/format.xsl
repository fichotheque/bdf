<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v1/_params.xsl"/>
    <xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes"/>
    
    <xsl:template match="/">
        <xsl:copy-of select="*"/>
    </xsl:template>
    
</xsl:stylesheet>
