<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
    
    <!--
Templates needed :
TEMPLATE_FICHEURL
TEMPLATE_MOTCLEURL
TEMPLATE_ILLUSTRATIONURL
TEMPLATE_VERSIONURL
-->

<!--
********************************************************************************
* Public modes (v1)
********************************************************************************
-->
    <xsl:template match="*" mode="MODE_PARAGRAPHCONTENT">
        <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
    </xsl:template>

<!--
********************************************************************************
* Public modes (v2)
********************************************************************************
-->

    <xsl:template match="*" mode="ficheblock-ParagraphContent">
        <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
    </xsl:template>
    
    
<!--
********************************************************************************
* Default span
********************************************************************************
-->
    
    <xsl:template match="s[@ref-error]" priority="10">
        <span class="cm-error">#Ref. err: <xsl:value-of select="@ref-error"/>#</span><em><xsl:value-of select="."/></em>
    </xsl:template>
    
    <xsl:template match="s[@type='em']">
        <em>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </em>
    </xsl:template>

    <xsl:template match="s[@type='emstrg']">
        <em>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <strong>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
            </strong>
        </em>
    </xsl:template>

    <xsl:template match="s[@type='strg']">
        <strong>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </strong>
    </xsl:template>

    <xsl:template match="s[@type='link']">
        <a href="{@ref}">
            <xsl:for-each select="@*[name() != 'att-href' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@link = 'external' and @att-rel = false()">
                <xsl:attribute name="rel">external</xsl:attribute>
            </xsl:if>
            <xsl:if test="string-length($EXTERNALTARGET) &gt; 0 and @link = 'external' and @att-target = false()">
                <xsl:attribute name="target"><xsl:value-of select="$EXTERNALTARGET"/></xsl:attribute>
            </xsl:if>
            <xsl:value-of select="."/>
        </a>
    </xsl:template>

    <xsl:template match="s[@type='iref']">
        <sup>
            <xsl:attribute name="class">
                <xsl:text>fbe-link-Iref</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <a href="#iref:{@ref}">
                <xsl:value-of select="."/>
            </a>
        </sup>
    </xsl:template>

    <xsl:template match="s[@type='anchor']">
        <xsl:choose>
            <xsl:when test="string-length(.) &gt; 0">
                <sup>
                    <xsl:attribute name="class">
                        <xsl:text>fbe-link-Anchor</xsl:text>
                        <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
                    </xsl:attribute>
                    <xsl:attribute name="id">
                        <xsl:choose>
                            <xsl:when test="@att-id = true()"><xsl:value-of select="@att-id"/></xsl:when>
                            <xsl:otherwise>iref:<xsl:value-of select="@ref"/></xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:for-each select="@*[name() != 'att-class' and name() != 'id' and starts-with(name(), 'att-')]">
                        <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
                    </xsl:for-each>
                    <xsl:value-of select="."/>
                </sup>
            </xsl:when>
            <xsl:otherwise><span id="iref:{@ref}"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="s[@type='cite']">
        <cite>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </cite>
    </xsl:template>

    <xsl:template match="s[@type='dfn']">
        <dfn>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </dfn>
    </xsl:template>

    <xsl:template match="s[@type='samp']">
        <samp>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </samp>
    </xsl:template>

    <xsl:template match="s[@type='kbd']">
        <kbd>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </kbd>
    </xsl:template>

    <xsl:template match="s[@type='code']">
        <code>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </code>
    </xsl:template>

    <xsl:template match="s[@type='var']">
        <var>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </var>
    </xsl:template>

    <xsl:template match="s[@type='sup']">
        <sup>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </sup>
    </xsl:template>

    <xsl:template match="s[@type='sub']">
        <sub>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </sub>
    </xsl:template>

    <xsl:template match="s[@type='quote']">
        <q>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@att-cite = false() and @ref=true()"><xsl:attribute name="cite"><xsl:value-of select="@ref"/></xsl:attribute></xsl:if>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </q>
    </xsl:template>

    <xsl:template match="s[@type='del']">
        <del>
           <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </del>
    </xsl:template>

    <xsl:template match="s[@type='ins']">
        <ins>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </ins>
    </xsl:template>

    <xsl:template match="s[@type='abbr']">
        <abbr>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </abbr>
    </xsl:template>

    <xsl:template match="s[@type='span']">
        <span>
            <xsl:for-each select="@*[name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="." mode="ficheblock-span-LinkTest"/>
        </span>
    </xsl:template>

    <xsl:template match="s[@type='br']">
        <br>
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
        </br>
    </xsl:template>

    <xsl:template match="s[@type='img' and boolean(@att-href)=true()]">
        <a href="{@att-href}">
            <xsl:if test="@att-hreflang = true()"><xsl:attribute name="hreflang"><xsl:value-of select="@att-hreflang"/></xsl:attribute></xsl:if>
            <xsl:if test="@att-rel = true()"><xsl:attribute name="rel"><xsl:value-of select="@att-rel"/></xsl:attribute></xsl:if>
            <xsl:if test="@att-target = true()"><xsl:attribute name="target"><xsl:value-of select="@att-target"/></xsl:attribute></xsl:if>
            <xsl:attribute name="class">
                <xsl:text>fbe-link-Img</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:if test="@link = 'external' and @att-rel = false()">
                <xsl:attribute name="rel">external</xsl:attribute>
            </xsl:if>
            <xsl:if test="string-length($EXTERNALTARGET) &gt; 0 and @link = 'external' and @att-target = false()">
                <xsl:attribute name="target"><xsl:value-of select="$EXTERNALTARGET"/></xsl:attribute>
            </xsl:if>
            <img alt="{.}" src="{@base}{@ref}">
                <xsl:for-each select="@*[name() != 'att-href' and name() != 'att-hreflang' and name() != 'att-rel' and name() != 'att-target' and name() != 'att-alt' and name() != 'att-src' and starts-with(name(), 'att-')]">
                    <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
                </xsl:for-each>
            </img>
        </a>
    </xsl:template>

    <xsl:template match="s[@type='img' and boolean(@att-href)=false()]">
        <img alt="{.}" src="{@base}{@ref}">
            <xsl:for-each select="@*[name() != 'att-alt' and name() != 'att-src' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
        </img>
    </xsl:template>

    <xsl:template match="s[@type='fiche']">
        <xsl:variable name="var_Fragment" select="@fragment"/>
        <xsl:variable name="var_Id" select="@id"/>
        <xsl:variable name="var_Corpus" select="@corpus"/>
        <xsl:variable name="var_Idalpha" select="@idalpha"/>
        <a>
            <xsl:attribute name="class">
                <xsl:text>fbe-link-Fiche</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@att-hreflang = false() and @lang=true()"><xsl:attribute name="hreflang"><xsl:value-of select="@lang"/></xsl:attribute></xsl:if>
            <xsl:if test="@att-href = false()"><xsl:attribute name="href">
                <xsl:call-template name="TEMPLATE_FICHEURL">
                    <xsl:with-param name="PARAM_CORPUS" select="$var_Corpus"/>
                    <xsl:with-param name="PARAM_ID" select="$var_Id"/>
                    <xsl:with-param name="PARAM_IDALPHA" select="$var_Idalpha"/>
                    <xsl:with-param name="PARAM_FRAGMENT" select="$var_Fragment"/>
                </xsl:call-template>
            </xsl:attribute></xsl:if>
            <xsl:value-of select="."/>
        </a>
    </xsl:template>

    <xsl:template match="s[@type='motcle']">
        <xsl:variable name="var_Ref" select="@ref"/>
        <xsl:variable name="var_Fragment" select="@fragment"/>
        <xsl:variable name="var_Id" select="motcle/@id"/>
        <xsl:variable name="var_Thesaurus" select="motcle/@thesaurus"/>
        <xsl:variable name="var_Idalpha"  select="motcle/@idalpha"/>
        <a>
            <xsl:attribute name="class">
                <xsl:text>fbe-link-Motcle</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@att-href = false()">
                <xsl:choose>
                    <xsl:when test="motcle/@url">
                        <xsl:attribute name="href">
                            <xsl:value-of select="motcle/@url"/>
                        </xsl:attribute>
                        <xsl:if test="@att-rel = false()">
                            <xsl:attribute name="rel">external</xsl:attribute>
                        </xsl:if>
                    </xsl:when>
                    <xsl:when test="motcle/satellite">
                        <xsl:variable name="var_Satellite">
                            <xsl:choose>
                                <xsl:when test="motcle/satellite[@lang = $WORKINGLANG]"><xsl:value-of select="motcle/satellite[@lang = $WORKINGLANG]/@corpus"/></xsl:when>
                                <xsl:when test="motcle/satellite[@xml:lang = substring-before($WORKINGLANG, '-')]"><xsl:value-of select="motcle/satellite[@lang = substring-before($WORKINGLANG, '-')]/@corpus"/></xsl:when>
                                <xsl:otherwise><xsl:value-of select="motcle/satellite[1]/@corpus"/></xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:attribute name="href">
                            <xsl:call-template name="TEMPLATE_FICHEURL">
                                <xsl:with-param name="PARAM_CORPUS" select="$var_Satellite"/>
                                <xsl:with-param name="PARAM_ID" select="$var_Id"/>
                                <xsl:with-param name="PARAM_IDALPHA" select="$var_Idalpha"/>
                                <xsl:with-param name="PARAM_FRAGMENT" select="$var_Fragment"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="href">
                            <xsl:call-template name="TEMPLATE_MOTCLEURL">
                                <xsl:with-param name="PARAM_THESAURUS" select="$var_Thesaurus"/>
                                <xsl:with-param name="PARAM_ID" select="$var_Id"/>
                                <xsl:with-param name="PARAM_IDALPHA" select="$var_Idalpha"/>
                                <xsl:with-param name="PARAM_FRAGMENT" select="$var_Fragment"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="value"><xsl:value-of select="value"/></xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="motcle" mode="motcle-Text"/>
                </xsl:otherwise>
            </xsl:choose>
        </a>
    </xsl:template>


<!--
********************************************************************************
* ficheblock-span-
********************************************************************************
-->

    <xsl:template match="s" mode="ficheblock-span-Content">
        <xsl:value-of select="."/>
    </xsl:template>
    
    <xsl:template match="s[@cdata]" mode="ficheblock-span-Content">
        <xsl:value-of select="@cdata" disable-output-escaping="yes" />
    </xsl:template>
    
    <xsl:template match="s" mode="ficheblock-span-LinkTest">
        <xsl:choose>
            <xsl:when test="@ref = true()">
                <a href="{@ref}">
                    <xsl:if test="@att-hreflang = true()"><xsl:attribute name="hreflang"><xsl:value-of select="@att-hreflang"/></xsl:attribute></xsl:if>
                    <xsl:if test="@att-rel = true()"><xsl:attribute name="rel"><xsl:value-of select="@att-rel"/></xsl:attribute></xsl:if>
                    <xsl:if test="@att-target = true()"><xsl:attribute name="target"><xsl:value-of select="@att-target"/></xsl:attribute></xsl:if>
                    <xsl:if test="@att-title = true()"><xsl:attribute name="title"><xsl:value-of select="@att-title"/></xsl:attribute></xsl:if>
                    <xsl:if test="@link = 'external' and @att-rel = false()">
                        <xsl:attribute name="rel">external</xsl:attribute>
                    </xsl:if>
                    <xsl:if test="string-length($EXTERNALTARGET) &gt; 0 and @link = 'external' and @att-target = false()">
                        <xsl:attribute name="target"><xsl:value-of select="$EXTERNALTARGET"/></xsl:attribute>
                    </xsl:if>
                    <xsl:apply-templates select="."  mode="ficheblock-span-Content"/>
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="."  mode="ficheblock-span-Content"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


<!--
********************************************************************************
* paragraphs
********************************************************************************
-->
    
    <xsl:template match="p">
        <p>
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@source">
                <span class="fbe-p-Source">
                    <xsl:value-of select="@source"/>
                    <xsl:text> : </xsl:text>
                </span>
            </xsl:if>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </p>
    </xsl:template>

    <xsl:template match="p[@type='question']">
        <p>
            <xsl:attribute name="class">
                <xsl:text>fbe-p-Question</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@source">
                <span class="SOURCE">
                    <xsl:value-of select="@source"/>
                    <xsl:text> : </xsl:text>
                </span>
            </xsl:if>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </p>
    </xsl:template>

    <xsl:template match="p[@type='remarque']">
        <p>
            <xsl:attribute name="class">
                <xsl:text>fbe-p-Remark</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@source">
                <span class="fbe-p-Source">
                    <xsl:value-of select="@source"/>
                    <xsl:text> : </xsl:text>
                </span>
            </xsl:if>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </p>
    </xsl:template>
   
    <xsl:template match="p[@type='citation']">
        <blockquote>
            <xsl:attribute name="class">
                <xsl:text>fbe-p-Blockquote</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <p><xsl:call-template name="sub_ficheblock-ParagraphContent"/></p>
            <xsl:apply-templates select="following-sibling::*[1][@type='citation' and @source=false()]" mode="ficheblock-paragraph-NextQuote"/>
            <xsl:if test="@source">
                <p class="fbe-p-BlockquoteSource">
                    <xsl:value-of select="@source"/>
                </p>
            </xsl:if>
        </blockquote>
    </xsl:template>

    <xsl:template match="p[@type='citation' and @source=false() and preceding-sibling::*[1]/@type='citation']">
    </xsl:template>

    <xsl:template match="p[@type='note']">
        <p>
            <xsl:attribute name="class">
                <xsl:text>fbe-p-Note</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@source">
                <span class="fbe-Source">
                    <xsl:value-of select="@source"/>
                    <xsl:text> : </xsl:text>
                </span>
            </xsl:if>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </p>
    </xsl:template>

    <xsl:template match="h">
        <xsl:variable name="var_Start" select="number($STARTLEVEL)"/>
        <xsl:variable name="var_Level" select="$var_Start + number(@level) - 1"/>
        <xsl:choose>
            <xsl:when test="$var_Start &gt;0 and $var_Level &lt; 7">
                <xsl:call-template name="sub_ficheblock-HtoH">
                    <xsl:with-param name="param_Level" select="$var_Level"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="sub_ficheblock-HtoP"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    
    
    <xsl:template match="numero">
        <span class="fbe-caption-Number">
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
            <xsl:if test="count(../legende) &gt; 0"><xsl:text> : </xsl:text></xsl:if>
        </span>
    </xsl:template>

    <xsl:template match="legende">
        <span class="fbe-caption-Legend">
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </span>
    </xsl:template>
    
    <xsl:template match="alt">
        <p><xsl:call-template name="sub_ficheblock-ParagraphContent"/></p>
    </xsl:template>
    
    <xsl:template match="credit">
        <div class="fbe-figure-Credit"><xsl:call-template name="sub_ficheblock-ParagraphContent"/></div>
    </xsl:template>


<!--
********************************************************************************
* ficheblock-paragraph-                                                           
********************************************************************************
-->

    <xsl:template match="p" mode="ficheblock-paragraph-NextQuote">
        <p><xsl:call-template name="sub_ficheblock-ParagraphContent"/></p>
        <xsl:apply-templates select="following-sibling::*[1][@type='citation' and @source=false()]" mode="ficheblock-paragraph-NextQuote"/>
    </xsl:template>


<!--
********************************************************************************
* lists                                                             
********************************************************************************
-->

    <xsl:template match="ul">
        <xsl:param name="param_Depth" select="1"/>
        <ul>
            <xsl:attribute name="class">
                <xsl:text>fbe-depth-</xsl:text><xsl:value-of select="$param_Depth"/>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select='li'>
                <xsl:with-param name="param_Depth" select="$param_Depth"/>
            </xsl:apply-templates>
        </ul>
    </xsl:template>

    <xsl:template match="ul[@variant='ol']">
        <xsl:param name="param_Depth" select="1"/>
        <ol>
            <xsl:attribute name="class">
                <xsl:text>fbe-depth-</xsl:text><xsl:value-of select="$param_Depth"/>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select='li'>
                <xsl:with-param name="param_Depth" select="$param_Depth"/>
            </xsl:apply-templates>
        </ol>
    </xsl:template>
    
    <xsl:template match="ul[@variant='dl']">
        <xsl:param name="param_Depth" select="1"/>
        <dl>
            <xsl:attribute name="class">
                <xsl:text>fbe-depth-</xsl:text><xsl:value-of select="$param_Depth"/>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select='li' mode="ficheblock-list-Dl">
                <xsl:with-param name="param_Depth" select="$param_Depth"/>
            </xsl:apply-templates>
        </dl>
    </xsl:template>

    <xsl:template match="li">
        <xsl:param name="param_Depth" select="1"/>
        <li>
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:for-each select='*'>
                <xsl:apply-templates select='.' mode="ficheblock-list-Li">
                    <xsl:with-param name="param_Depth" select="$param_Depth +1"/>
                </xsl:apply-templates>
            </xsl:for-each>
        </li>
    </xsl:template>
    
    
    
<!--
********************************************************************************
* ficheblock-list-                                                           
********************************************************************************
-->

    <xsl:template match="p" mode="ficheblock-list-Dl">
        <dt>
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@source">
                <span class="fbe-Source">
                    <xsl:value-of select="@source"/>
                    <xsl:text> : </xsl:text>
                </span>
            </xsl:if>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </dt>
    </xsl:template>
    
    <xsl:template match="li" mode="ficheblock-list-Dl">
        <xsl:param name="param_Depth" select="1"/>
        <xsl:apply-templates select="p[1]" mode="ficheblock-list-Dl"/>
        <dd>
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:for-each select='*'>
                <xsl:if test="position() &gt; 1">
                    <xsl:apply-templates select='.' mode="ficheblock-list-Li">
                        <xsl:with-param name="param_Depth" select="$param_Depth +1"/>
                    </xsl:apply-templates>
                </xsl:if>
        </xsl:for-each>
        </dd>
    </xsl:template>
    
    <xsl:template match="p" mode="ficheblock-list-Li">
        <p>
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="@source">
                <span class="fbe-Source">
                    <xsl:value-of select="@source"/>
                    <xsl:text> : </xsl:text>
                </span>
            </xsl:if>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </p>
    </xsl:template>

    <xsl:template match="ul" mode="ficheblock-list-Li">
        <xsl:param name="param_Depth" select="2"/>
        <xsl:apply-templates select=".">
            <xsl:with-param name="param_Depth" select="$param_Depth"/>
        </xsl:apply-templates>
    </xsl:template>
    
    
<!--
********************************************************************************
* table                                                            
********************************************************************************
-->

    <xsl:template match="table">
        <table>
            <xsl:attribute name="class">
                <xsl:text>fbe-table-Table</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <caption class="fbe-table-Caption" align="bottom">
                <xsl:apply-templates select="numero"/>
                <xsl:apply-templates select="legende"/>
            </caption>
            <xsl:apply-templates select="tr"/>
        </table>
    </xsl:template>

    <xsl:template match="tr">
        <tr>
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:apply-templates select="*"/>
        </tr>
    </xsl:template>

    <xsl:template match="td">
        <td>
            <xsl:attribute name="class">
                <xsl:text>fbe-table-Cell</xsl:text>
                <xsl:if test="@format = 'number'"><xsl:text> fbe-table-Number</xsl:text></xsl:if>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </td>
    </xsl:template>

    <xsl:template match="td[@type='header']">
        <th>
            <xsl:attribute name="class">
                <xsl:text>fbe-tableheader</xsl:text>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </th>
    </xsl:template>


<!--
********************************************************************************
* ln                                                            
********************************************************************************
-->
    <xsl:template match="ln">
        <xsl:if test="@indent &gt;0"><xsl:call-template name="sub_ficheblock-Indent">
                <xsl:with-param name="param_String" select="'  '"/>
                <xsl:with-param name="param_Count" select="@indent"/>
            </xsl:call-template>
        </xsl:if>
        <xsl:choose>
            <xsl:when test="count(@*[starts-with(name(), 'att-')]) &gt; 0">
                <span>
                    <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                        <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
                    </xsl:for-each>
                    <xsl:value-of select="."/>
                </span>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="position() != last()"><xsl:text>&#10;</xsl:text></xsl:if>
    </xsl:template>
   
    
<!--
********************************************************************************
* figures                                                           
********************************************************************************
-->
    <xsl:template match="code">
        <xsl:call-template name="sub_ficheblock-Figure">
            <xsl:with-param name="param_Class" select="'fbe-figure-Code'"/>
            <xsl:with-param name="param_CaptionClass" select="'fbe-figure-CodeCaption'"/>
            <xsl:with-param name="param_CaptionAtStart" select="1"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="div">
        <xsl:call-template name="sub_ficheblock-Figure">
            <xsl:with-param name="param_Class" select="'fbe-figure-Div'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="div[@variant='blockquote']">
        <xsl:call-template name="sub_ficheblock-Figure">
            <xsl:with-param name="param_Class" select="'fbe-figure-Blockquote'"/>
            <xsl:with-param name="param_CaptionClass" select="'fbe-figure-BlockquoteCaption'"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="cdatadiv">
        <xsl:call-template name="sub_ficheblock-Figure">
            <xsl:with-param name="param_Class" select="'fbe-figure-Cdata'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="cdatadiv[@variant='raw']">
        <xsl:value-of select="cdata" disable-output-escaping="yes"/>
    </xsl:template>

    <xsl:template match="insert[@type='image']">
        <xsl:call-template name="sub_ficheblock-Figure">
            <xsl:with-param name="param_Class" select="'fbe-figure-Image'"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="insert[@type='lettrine']">
        <xsl:variable name="var_Class">
            <xsl:text>fbe-figure-Lettrine </xsl:text>
            <xsl:choose>
                <xsl:when test="@position = 'right'">fbe-figure-Right</xsl:when>
                <xsl:otherwise>fbe-figure-Left</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:call-template name="sub_ficheblock-Figure">
            <xsl:with-param name="param_Class" select="$var_Class"/>
            <xsl:with-param name="param_CaptionClass" select="'fbe-figure-LettrineCaption'"/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template match="insert[@type='audio']">
        <xsl:call-template name="sub_ficheblock-Figure">
            <xsl:with-param name="param_Class" select="'fbe-figure-Audio'"/>
        </xsl:call-template>
    </xsl:template>

    


<!--
********************************************************************************
* ficheblock-figure-                                                        
********************************************************************************
-->

    <xsl:template match="*" mode="ficheblock-figure-Caption">
        <xsl:param name="param_Class"/>
        <xsl:if test="numero = true() or legende = true()">
            <figcaption class="{$param_Class}">
                <xsl:apply-templates select="numero"/>
                <xsl:apply-templates select="legende"/>
            </figcaption>
        </xsl:if>
    </xsl:template>

    <xsl:template match="cdatadiv" mode="ficheblock-figure-Content">
        <div class="fbe-figure-CdataContent">
            <xsl:value-of select="cdata" disable-output-escaping="yes"/>
        </div>
    </xsl:template>
    
    <xsl:template match="code" mode="ficheblock-figure-Content">
        <pre class="fbe-figure-CodeContent"><code><xsl:apply-templates select="ln"/></code></pre>
    </xsl:template>
    
    <xsl:template match="div" mode="ficheblock-figure-Content">
        <div class="fbe-figure-DivContent">
            <xsl:apply-templates select="fbl/*"/>
        </div>
    </xsl:template>
    
    <xsl:template match="div[@variant='blockquote']" mode="ficheblock-figure-Content">
        <blockquote class="fbe-figure-BlockquoteContent">
            <xsl:apply-templates select="fbl/*"/>
        </blockquote>
    </xsl:template>
    
    <xsl:template match="insert[@type='image']" mode="ficheblock-figure-Content">
        <xsl:apply-templates select="." mode="ficheblock-figure-ImgContent">
            <xsl:with-param name="param_Class" select="'fbe-figure-ImageContent'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="insert[@type='lettrine']" mode="ficheblock-figure-Content">
        <xsl:apply-templates select="." mode="ficheblock-figure-ImgContent">
            <xsl:with-param name="param_Class" select="'fbe-figure-LettrineContent'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="insert[@type='audio']" mode="ficheblock-figure-Content">
        <div class="fbe-figure-AudioContent">
            <audio src="{@base}{@src}" controls="controls">
                <xsl:apply-templates select="alt"/>
            </audio>
        </div>
        <xsl:apply-templates select="credit"/>
    </xsl:template>
    
    <xsl:template match="insert[@type='audio' and @addenda]" mode="ficheblock-figure-Content">
        <xsl:variable name="var_Addenda" select="@addenda"/>
        <xsl:variable name="var_Id" select="@id"/>
        <xsl:variable name="var_Basename" select="@basename"/>
        <div class="fbe-figure-AudioContent">
            <audio controls="controls">
                <xsl:for-each select="insertversion">
                    <xsl:variable name="var_Src">
                        <xsl:call-template name="TEMPLATE_VERSIONURL">
                                <xsl:with-param name="PARAM_ADDENDA" select="$var_Addenda"/>
                                <xsl:with-param name="PARAM_ID" select="$var_Id"/>
                                <xsl:with-param name="PARAM_BASENAME" select="$var_Basename"/>
                                <xsl:with-param name="PARAM_EXTENSION" select="@extension"/>
                            </xsl:call-template>
                    </xsl:variable>
                    <xsl:call-template name="sub_ficheblock-SourceTag">
                        <xsl:with-param name="param_Type" select="@type"/>
                        <xsl:with-param name="param_Src" select="$var_Src"/>
                    </xsl:call-template>
                </xsl:for-each>
                <xsl:apply-templates select="alt"/>
            </audio>
        </div>
        <xsl:apply-templates select="credit"/>
    </xsl:template>

    <xsl:template match="insert" mode="ficheblock-figure-ImgContent">
        <xsl:param name="param_Class"/>
        <div class="{$param_Class}">
            <xsl:choose>
                <xsl:when test="string-length(@ref) &gt; 0">
                    <a href="{@ref}">
                        <xsl:if test="@att-rel = true()"><xsl:attribute name="rel"><xsl:value-of select="@att-rel"/></xsl:attribute></xsl:if>
                        <xsl:if test="@att-target = true()"><xsl:attribute name="target"><xsl:value-of select="@att-target"/></xsl:attribute></xsl:if>
                        <xsl:if test="@link = 'external' and @att-rel = false()">
                            <xsl:attribute name="rel">external</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="string-length($EXTERNALTARGET) &gt; 0 and @link = 'external' and @att-target = false()">
                            <xsl:attribute name="target"><xsl:value-of select="$EXTERNALTARGET"/></xsl:attribute>
                        </xsl:if>
                        <xsl:apply-templates select="." mode="ficheblock-figure-ImgTag"/>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="." mode="ficheblock-figure-ImgTag"/>
                </xsl:otherwise>
            </xsl:choose>
        </div>
        <xsl:apply-templates select="credit"/>
    </xsl:template>
    
    <xsl:template match="insert" mode="ficheblock-figure-ImgTag">
        <img alt="{alt}" src="{@base}{@src}">
            <xsl:if test="@width = true()"><xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute></xsl:if>
            <xsl:if test="@height = true()"><xsl:attribute name="height"><xsl:value-of select="@height"/></xsl:attribute></xsl:if>
        </img>
    </xsl:template>
    
    <xsl:template match="insert[@album]" mode="ficheblock-figure-ImgTag">
        <xsl:variable name="var_Album" select="@album"/>
        <xsl:variable name="var_Id" select="@id"/>
        <xsl:variable name="var_Format" select="@format"/>
        <xsl:variable name="var_Dim" select="@albumdim"/>
        <img alt="{alt}">
            <xsl:attribute name="src">
                <xsl:call-template name="TEMPLATE_ILLUSTRATIONURL">
                    <xsl:with-param name="PARAM_ALBUM" select="$var_Album"/>
                    <xsl:with-param name="PARAM_ID" select="$var_Id"/>
                    <xsl:with-param name="PARAM_FORMAT" select="$var_Format"/>
                    <xsl:with-param name="PARAM_DIM" select="$var_Dim"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:if test="@width = true()"><xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute></xsl:if>
            <xsl:if test="@height = true()"><xsl:attribute name="height"><xsl:value-of select="@height"/></xsl:attribute></xsl:if>
        </img>
    </xsl:template>


<!--
********************************************************************************
* sub_ficheblock-                                                        
********************************************************************************
-->

    <xsl:template name="sub_ficheblock-Figure">
        <xsl:param name="param_Class"/>
        <xsl:param name="param_CaptionClass" select="'fbe-figure-Caption'"/>
        <xsl:param name="param_CaptionAtStart" select="0"/>
        <figure>
            <xsl:if test="@xml:lang = true()"><xsl:attribute name="lang"><xsl:value-of select="@xml:lang"/></xsl:attribute></xsl:if>
            <xsl:attribute name="class">
                <xsl:value-of select="$param_Class"/>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and name() != 'att-rel' and name() != 'att-target' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:if test="$param_CaptionAtStart = 1">
                <xsl:apply-templates select="." mode="ficheblock-figure-Caption">
                    <xsl:with-param name="param_Class" select="$param_CaptionClass"/>
                </xsl:apply-templates>
            </xsl:if>
            <xsl:apply-templates select="." mode="ficheblock-figure-Content"/>
            <xsl:if test="$param_CaptionAtStart = 0">
                <xsl:apply-templates select="." mode="ficheblock-figure-Caption">
                    <xsl:with-param name="param_Class" select="$param_CaptionClass"/>
                </xsl:apply-templates>
            </xsl:if>
        </figure>
    </xsl:template>
    
    <xsl:template name="sub_ficheblock-HtoH">
        <xsl:param name="param_Level" select="1"/>
        <xsl:element name="h{$param_Level}">
            <xsl:for-each select="@*[starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </xsl:element>
    </xsl:template>

    <xsl:template name="sub_ficheblock-HtoP">
        <p>
            <xsl:attribute name="class">
                <xsl:text>fbe-p-H fbe-p-Level</xsl:text><xsl:value-of select="@level"/>
                <xsl:if test="@att-class = true()"><xsl:text> </xsl:text><xsl:value-of select="@att-class"/></xsl:if>
            </xsl:attribute>
            <xsl:for-each select="@*[name() != 'att-class' and starts-with(name(), 'att-')]">
                <xsl:attribute name="{substring(name(), 5)}"><xsl:value-of select="."/></xsl:attribute>
            </xsl:for-each>
            <xsl:call-template name="sub_ficheblock-ParagraphContent"/>
        </p>
    </xsl:template>

    <xsl:template name="sub_ficheblock-Indent">
        <xsl:param name="param_String"/>
        <xsl:param name="param_Count" select="1"/>
        <xsl:choose>
            <xsl:when test="not($param_Count) or not($param_String)"/>
            <xsl:when test="$param_Count = 1">
                <xsl:value-of select="$param_String"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$param_Count mod 2"><xsl:value-of select="$param_String"/></xsl:if>
                <xsl:call-template name="sub_ficheblock-Indent">
                    <xsl:with-param name="param_String" select="concat($param_String,$param_String)"/>
                    <xsl:with-param name="param_Count" select="floor($param_Count div 2)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="sub_ficheblock-ParagraphContent">
        <xsl:if test='(count(*) = 0) and (string-length(.) =0)'><xsl:text>&#x00A0;</xsl:text></xsl:if>
        <xsl:for-each select="node()">
            <xsl:apply-templates select="." />
        </xsl:for-each>
    </xsl:template>

     <xsl:template name="sub_ficheblock-SourceTag">
        <xsl:param name="param_Type"/>
        <xsl:param name="param_Src"/>
        <xsl:text disable-output-escaping="yes">&lt;source type=&quot;</xsl:text><xsl:value-of select="$param_Type"/><xsl:text disable-output-escaping="yes">&quot; src=&quot;</xsl:text><xsl:value-of select="$param_Src"/><xsl:text disable-output-escaping="yes">&quot;&gt;</xsl:text>
    </xsl:template>

</xsl:stylesheet>