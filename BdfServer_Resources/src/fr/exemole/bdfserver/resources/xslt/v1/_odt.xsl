<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <xsl:template match="fiche" mode="_Title">
        <text:h text:style-name="FicheTitle" text:outline-level="1"><xsl:apply-templates select="." mode="include-fiche-Title"/></text:h>
    </xsl:template>
    
    <xsl:template match="soustitre" mode="_Inline">
        <text:p text:style-name="FicheSubtitle"><xsl:apply-templates select="." mode="field-Inline"/></text:p>
    </xsl:template>
    
    <xsl:template match="propriete" mode="_Inline">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheField"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@field-key=concat('propriete_', $var_Name)]"/> : </text:span><xsl:apply-templates select="." mode="field-Inline"/></text:p>
    </xsl:template>
    
    <xsl:template match="extraitthesaurus|motcles" mode="_Inline">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheField">
            <text:span text:style-name="FicheLabel">➢ 
                <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@include-key=$var_Name]"/> : 
            </text:span>
            <xsl:apply-templates select="." mode="motcle-Inline"/>
        </text:p>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="_Listitem">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheFieldLabel"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@include-key=$var_Name]"/> :</text:span></text:p>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="." mode="include-Listitem">
                <xsl:with-param name="param_Class" select="'FicheFieldLi'"/>
            </xsl:apply-templates>
            </text:list>
    </xsl:template>
    
    <xsl:template match="extraitaddenda|documents" mode="_Listitem">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheFieldLabel"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@include-key=$var_Name]"/> :</text:span></text:p>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="." mode="include-Listitem">
                <xsl:with-param name="param_Class" select="'FicheFieldLi'"/>
            </xsl:apply-templates>
        </text:list>
    </xsl:template>

    <xsl:template match="extraitalbum|illustrations" mode="_Inline">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheField">
            <text:span text:style-name="FicheLabel">➢ 
                <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@include-key=$var_Name]"/> : 
            </text:span>
            <xsl:apply-templates select="." mode="include-Inline"/>
        </text:p>
    </xsl:template>
    
    <xsl:template match="redacteurs" mode="_Inline">
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheField"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@field-key='redacteurs']"/> : </text:span><xsl:apply-templates select="." mode="field-Inline"/></text:p>
    </xsl:template>
    
    <xsl:template match="lang" mode="_Inline">
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheField"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@field-key='lang']"/> : </text:span><xsl:apply-templates select="." mode="field-Inline"/></text:p>
    </xsl:template>
    
    <xsl:template match="information" mode="_Inline">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheField"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@field-key=concat('information_', $var_Name)]"/> : </text:span><xsl:apply-templates select="." mode="field-Inline"/></text:p>
    </xsl:template>
    
    <xsl:template match="information" mode="_Listitem">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheFieldLabel"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@field-key=concat('information_', $var_Name)]"/> :</text:span></text:p>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="." mode="field-Listitem">
                <xsl:with-param name="param_Class" select="'FicheFieldLi'"/>
            </xsl:apply-templates>
        </text:list>
    </xsl:template>
    
    <xsl:template match="information" mode="_Block">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:p text:style-name="FicheFieldLabel"><text:span text:style-name="FicheLabel">➢ <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@field-key=concat('information_', $var_Name)]"/> :</text:span></text:p>
            <xsl:apply-templates select="." mode="field-Block">
                <xsl:with-param name="param_Class" select="'FicheFieldItem'"/>
            </xsl:apply-templates>
    </xsl:template>
       
    <xsl:template match="section" mode="_Section">
        <xsl:variable name="var_Name" select="@name"/>
        <xsl:variable name="var_Corpus" select="../@corpus"/>
        <text:h text:style-name="FicheSectionLabel" text:outline-level="2"><xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@field-key=concat('section_', $var_Name)]"/></text:h>
        <xsl:apply-templates select="."/>
        <text:p text:style-name="FicheSectionEnd">❖ ❖ ❖</text:p>
    </xsl:template>
    
    <xsl:template match="fiche" mode="_Chrono">
        <xsl:variable name="var_Corpus" select="@corpus"/>
        <text:p text:style-name="FicheChrono"><xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@include-key='date_creation']"/> : <xsl:apply-templates select="chrono/datation[1]" mode="ficheitem-Inline"/> — <xsl:value-of select="$LABELS[@corpus=$var_Corpus]/label[@include-key='date_modification']"/> : <xsl:apply-templates select="chrono/datation[position() = last()]" mode="ficheitem-Inline"/></text:p>
    </xsl:template>
    
</xsl:stylesheet>