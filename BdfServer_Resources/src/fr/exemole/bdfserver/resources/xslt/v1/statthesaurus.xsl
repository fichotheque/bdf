<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v1/_import_statthesaurus.xsl"/>
    
    <xsl:output method="html" encoding="UTF-8" indent="no"/>

    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;&#10;</xsl:text>
        <html>
            <head>
                <title>STAT</title>
                <style type="text/css">
                    <xsl:text>&#10;</xsl:text>
                    <xsl:value-of select="document('bdf://this/xml-pack/css/statthesaurus.css')/xml-pack" disable-output-escaping="yes"/><xsl:value-of select="document('bdf://this/xml-pack/custom/fiche.css')/xml-pack" disable-output-escaping="yes"/>
                </style>
                <xsl:apply-templates select="." mode="custom-Head"/>
            </head>
            <body>
                <xsl:apply-templates select="extraction/extraitthesaurus|extraction/motcles"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="extraitthesaurus|motcles">
        <h1><xsl:apply-templates select="." mode="motcle-Title"/></h1>
        <ul class="MotcleUl">
            <xsl:apply-templates select="motcle"/>
        </ul>
    </xsl:template>

    <xsl:template match="motcle">
        <li>
            <p><xsl:apply-templates select="." mode="motcle-Link"/></p>  
            <xsl:if test="extraitcorpus|fiches">
                <ul class="CorpusUl">
                    <xsl:apply-templates select="extraitcorpus|fiches"/>
                </ul>
            </xsl:if>
            <xsl:if test="count(motcle) &gt; 0">
                <ul class="MotcleUl">
                    <xsl:apply-templates select="motcle"/>
                </ul>
            </xsl:if>
        </li>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches">
        <li>
            <p>
                <xsl:apply-templates select="." mode="include-Title"/> [<xsl:value-of select="count(fiche)"/>]</p>
            <ul class="FicheUl">
                <xsl:apply-templates select="fiche"/>
            </ul>
        </li>
    </xsl:template>

    <xsl:template match="fiche">
        <li><p><a>
        <xsl:attribute name="href">
            <xsl:call-template name="TEMPLATE_FICHEURL">
                <xsl:with-param name="PARAM_CORPUS" select="@corpus"/>
                <xsl:with-param name="PARAM_ID" select="@id"/>
            </xsl:call-template>
        </xsl:attribute>
        <xsl:apply-templates select="." mode="include-Title"/>
        </a></p></li>
    </xsl:template>

</xsl:stylesheet>