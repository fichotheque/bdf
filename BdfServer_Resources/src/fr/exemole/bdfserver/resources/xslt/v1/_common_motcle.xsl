<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>


<!--
********************************************************************************
* Public modes (v1)
********************************************************************************
-->

    <xsl:template match="extraitthesaurus|motcles" mode="MODE_TITLE">
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Title">
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitthesaurus|motcles" mode="MODE_BLOCK">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Block">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitthesaurus|motcles" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitthesaurus|motcles" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' ; '"/>
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="motcle" mode="MODE_BLOCK">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Block">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="motcle" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="motcle" mode="MODE_LISTITEM_TEXTONLY">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Listitem_TextOnly">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="motcle" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="' ; '"/>
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="motcle" mode="MODE_LABEL">
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Link">
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>
        
    <xsl:template match="motcle" mode="MODE_TEXT">
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Text">
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>
    

    <xsl:template match="motcle" mode="MODE_LANGLABEL">
        <xsl:param name="PARAM_LANG" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-LangLabel">
            <xsl:with-param name="param_Lang" select="$PARAM_LANG"/>
        </xsl:apply-templates>
    </xsl:template>
    
    
<!--
********************************************************************************
* Public modes (v2)
********************************************************************************
-->

    <xsl:template match="extraitthesaurus|motcles" mode="motcle-Title">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="intitule|phrase" mode="motcle-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="extraitthesaurus|motcles" mode="motcle-Block">
        <xsl:param name="param_Class" select=""/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="motcle" mode="motcle-Block">
            <xsl:with-param name="param_Class" select="$param_Class"/>
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitthesaurus|motcles" mode="motcle-Listitem">
        <xsl:param name="param_Class" select=""/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="motcle" mode="motcle-Listitem">
            <xsl:with-param name="param_Class" select="$param_Class"/>
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitthesaurus|motcles" mode="motcle-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="motcle" mode="motcle-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="motcle" mode="motcle-Block">
        <xsl:param name="param_Class" select=""/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <p class="{$param_Class}">
            <xsl:apply-templates select="." mode="motcle-Link">
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
        </p>
    </xsl:template>

    <xsl:template match="motcle" mode="motcle-Listitem">
        <xsl:param name="param_Class" select=""/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <li>
            <p class="{$param_Class}">
                <xsl:apply-templates select="." mode="motcle-Link">
                    <xsl:with-param name="param_Lang" select="$param_Lang"/>
                </xsl:apply-templates>
            </p>
        </li>
    </xsl:template>
    
    <xsl:template match="motcle" mode="motcle-Listitem_TextOnly">
        <xsl:param name="param_Class" select=""/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <li>
            <p class="{$param_Class}">
                <xsl:apply-templates select="." mode="motcle-Text">
                    <xsl:with-param name="param_Lang" select="$param_Lang"/>
                </xsl:apply-templates>
            </p>
        </li>
    </xsl:template>

    <xsl:template match="motcle" mode="motcle-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:apply-templates select="." mode="motcle-Link">
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="motcle" mode="motcle-Link">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="motcle-Text">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="motcle[@url]" mode="motcle-Link">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <a href="{@url}" rel="external" target="_blank">
            <xsl:apply-templates select="." mode="motcle-Text">
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
        </a>
    </xsl:template>
    
    <xsl:template match="motcle[satellite]" mode="motcle-Link">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:variable name="var_Satellite">
            <xsl:choose>
                <xsl:when test="satellite[@lang = $param_Lang]"><xsl:value-of select="satellite[@lang = $param_Lang]/@corpus"/></xsl:when>
                <xsl:when test="satellite[@xml:lang = substring-before($param_Lang, '-')]"><xsl:value-of select="satellite[@lang = substring-before($param_Lang, '-')]/@corpus"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="satellite[1]/@corpus"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <a>
            <xsl:attribute name="href">
                <xsl:call-template name="TEMPLATE_FICHEURL">
                    <xsl:with-param name="PARAM_CORPUS" select="$var_Satellite"/>
                    <xsl:with-param name="PARAM_ID" select="@id"/>
                    <xsl:with-param name="PARAM_IDALPHA" select="@idalpha"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="motcle-Text">
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
        </a>
    </xsl:template>

    <xsl:template match="motcle" mode="motcle-Text">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:if test="intitule|phrase">
            <xsl:apply-templates select="intitule|phrase" mode="motcle-LangLabel">
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
            <xsl:text> – </xsl:text>
        </xsl:if>
        <xsl:if test="boolean(@idalpha = true())">
            <xsl:choose>
                <xsl:when test="@style = 'brackets'">[<xsl:value-of select="@idalpha"/>]<xsl:if test="label"><xsl:text> </xsl:text></xsl:if></xsl:when>
                <xsl:otherwise><xsl:value-of select="@idalpha"/><xsl:if test="label"><xsl:text> – </xsl:text></xsl:if></xsl:otherwise>
            </xsl:choose>
        </xsl:if>
        <xsl:apply-templates select="." mode="motcle-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="intitule|motcle|phrase" mode="motcle-LangLabel">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $param_Lang]"><xsl:value-of select="label[@xml:lang = $param_Lang]"/></xsl:when>
            <xsl:when test="label[@xml:lang = substring-before($param_Lang, '-')]"><xsl:value-of select="label[@xml:lang = substring-before($param_Lang, '-')]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="label[1]"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>