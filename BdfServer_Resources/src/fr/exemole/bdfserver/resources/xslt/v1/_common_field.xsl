<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- dependency: _common_ficheitem.xsl -->
    
<!--
********************************************************************************
* Public modes (v1)
********************************************************************************
-->

    <xsl:template match="titre|soustitre|propriete|information|redacteurs|lang" mode="MODE_BLOCK">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="*" mode="ficheitem-Block">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="titre|soustitre|propriete|information|redacteurs|lang" mode="MODE_LISTITEM">
        <xsl:param name="PARAM_CLASS" select=""/>
        <xsl:apply-templates select="*" mode="ficheitem-Listitem">
            <xsl:with-param name="param_Class" select="$PARAM_CLASS"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="titre|soustitre|propriete|information|redacteurs|lang" mode="MODE_INLINE">
        <xsl:param name="PARAM_SEP" select="', '"/>
        <xsl:apply-templates select="*" mode="ficheitem-Inline">
            <xsl:with-param name="param_Separator" select="$PARAM_SEP"/>
        </xsl:apply-templates>
    </xsl:template>
    
    
<!--
********************************************************************************
* Public modes (v2)
********************************************************************************
-->

    <xsl:template match="*" mode="field-Block">
        <xsl:param name="param_Class" select=""/>
        <xsl:apply-templates select="*" mode="ficheitem-Block">
            <xsl:with-param name="param_Class" select="$param_Class"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="*" mode="field-Listitem">
        <xsl:param name="param_Class" select=""/>
        <xsl:apply-templates select="*" mode="ficheitem-Listitem">
            <xsl:with-param name="param_Class" select="$param_Class"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="*" mode="field-Inline">
        <xsl:param name="param_Separator" select="', '"/>
        <xsl:apply-templates select="*" mode="ficheitem-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
        </xsl:apply-templates>
    </xsl:template>

<!--
********************************************************************************
* Default
********************************************************************************
-->

    <xsl:template match="section">
        <xsl:apply-templates select="*"/>
    </xsl:template>

    <xsl:template match="titre|soustitre|propriete|information|redacteurs|lang">
        <xsl:apply-templates select="*" mode="ficheitem-Inline"/>
    </xsl:template>

</xsl:stylesheet>