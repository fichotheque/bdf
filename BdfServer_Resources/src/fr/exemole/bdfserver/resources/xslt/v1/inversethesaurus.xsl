<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v1/_params.xsl"/>
    <xsl:output method="html" encoding="UTF-8" indent="no"/>
    <xsl:key name="sources" match="/lexie-distribution/sources/source" use="@source-id"/>

    <xsl:template match="/">
        <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;&#10;</xsl:text>
        <html>
            <head>
            <title>inversethesaurus Transformation</title>
            <style type="text/css">
                <xsl:text>&#10;</xsl:text>
                <xsl:value-of select="document('bdf://this/xml-pack/css/inversethesaurus.css')/xml-pack" disable-output-escaping="yes"/><xsl:value-of select="document('bdf://this/xml-pack/custom/fiche.css')/xml-pack" disable-output-escaping="yes"/>
            </style>
            </head>
            <body>
                <table>
                    <xsl:apply-templates select="lexie-distribution/lexies/lexie"/>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="lexie">
        <xsl:apply-templates select="occurrence"/>
    </xsl:template>

    <xsl:template match="occurrence">
        <xsl:variable name="SourceId" select="@source-id"/>
        <tr>
             <xsl:apply-templates select="key('sources',$SourceId)/text">
                 <xsl:with-param name="LexieId" select="../@lexie-id"/>
             </xsl:apply-templates>
        </tr>
    </xsl:template>

    <xsl:template match="text">
        <xsl:param name="LexieId"/>
        <xsl:apply-templates select="lx[@lexie-id=$LexieId]" mode="Pivot"/>
    </xsl:template>

    <xsl:template match="lx" mode="Pivot">
        <td class="avant">
             <xsl:apply-templates select="preceding-sibling::node()"/>
         </td>
         <td class="apres">
             <strong>
                <xsl:value-of select="."/>
             </strong>
             <xsl:apply-templates select="following-sibling::node()"/>
         </td>
    </xsl:template>

    <xsl:template match="lx">
        <xsl:value-of select="."/>
    </xsl:template>
    
</xsl:stylesheet>
