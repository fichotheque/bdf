<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
version="1.0">

<xsl:variable name="NAMESPACES" select="document('namespaces.xml')"/>

<xsl:variable name="ROOT" select="/xsl:stylesheet"/>

<xsl:template match="/">
    <xsl:text>&#10;</xsl:text>
    <xsl:for-each select="$NAMESPACES/xsl:stylesheet">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="$ROOT/node()"/>
        </xsl:copy>
    </xsl:for-each>
</xsl:template>

<xsl:template match="@*">
	<xsl:copy/>
</xsl:template>

<xsl:template match="*"  mode="inline">
    <xsl:apply-templates select="*|text()" mode="inline"/>
</xsl:template>

<xsl:template match="text()">
	<xsl:copy/>
</xsl:template>

<xsl:template match="text()" mode="inline">
    <xsl:copy/>
</xsl:template>

<xsl:template match="xsl:*">
    <xsl:copy>
        <xsl:apply-templates select="@*"/>
        <xsl:apply-templates select="*|text()"/>
    </xsl:copy>
</xsl:template>

<xsl:template match="xsl:*" mode="inline">
    <xsl:apply-templates select="."/>
</xsl:template>

<xsl:template match="xsl:include|xsl:import">
    <xsl:copy>
        <xsl:for-each select="@*[name() != 'href']">
            <xsl:copy/>
        </xsl:for-each>
        <xsl:if test="@href">
            <xsl:attribute name="href">
                <xsl:choose>
                    <xsl:when test="contains(@href, '/html/')">
                        <xsl:value-of select="substring-before(@href, '/html/')" />
                        <xsl:text>/odt/</xsl:text>
                        <xsl:value-of select="substring-after(@href, '/html/')" />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@href" />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
        </xsl:if>
    </xsl:copy>
</xsl:template>

<xsl:template match="xsl:value-of[@disable-output-escaping='yes']">
</xsl:template>
    

<xsl:template match="p">
    <xsl:param name="style" select="'FbeTxt'"/>
    <text:p text:style-name="{$style}">
        <xsl:apply-templates select="node()"/>
    </text:p>
</xsl:template>

<xsl:template match="p[data-od-style]">
    <xsl:param name="style" select="'FbeTxt'"/>
    <text:p text:style-name="{@data-od-style}">
        <xsl:apply-templates select="node()"/>
    </text:p>
</xsl:template>

<xsl:template match="h1">
    <text:h text:outline-level="1" text:style-name="Heading_20_1">
        <xsl:apply-templates select="node()"/>
    </text:h>
</xsl:template>

<xsl:template match="h2">
    <text:h text:outline-level="2" text:style-name="Heading_20_2">
        <xsl:apply-templates select="node()"/>
    </text:h>
</xsl:template>

<xsl:template match="h3">
    <text:h text:outline-level="3" text:style-name="Heading_20_3">
        <xsl:apply-templates select="node()"/>
    </text:h>
</xsl:template>

<xsl:template match="h4">
    <text:h text:outline-level="4" text:style-name="Heading_20_4">
        <xsl:apply-templates select="node()"/>
    </text:h>
</xsl:template>

<xsl:template match="h5">
    <text:h text:outline-level="5" text:style-name="Heading_20_5">
        <xsl:apply-templates select="node()"/>
    </text:h>
</xsl:template>

<xsl:template match="h6">
    <text:h text:outline-level="6" text:style-name="Heading_20_6">
        <xsl:apply-templates select="node()"/>
    </text:h>
</xsl:template>

<xsl:template match="strong">
    <text:span text:style-name="FbeTxtStrg">
        <xsl:apply-templates select="node()" mode="inline"/>
    </text:span>
</xsl:template>

<xsl:template match="em">
    <text:span text:style-name="FbeTxtEm">
        <xsl:apply-templates select="node()" mode="inline"/>
    </text:span>
</xsl:template>

<xsl:template match="span">
    <xsl:apply-templates select="node()" mode="inline"/>
</xsl:template>

<xsl:template match="span[data-od-style]">
    <text:span text:style-name="{@data-od-style}">
        <xsl:apply-templates select="node()" mode="inline"/>
    </text:span>
</xsl:template>

<xsl:template match="ul">
    <text:list text:style-name="FbeLstUl">
        <xsl:apply-templates select="li|text()|xsl:*"/>
    </text:list>
</xsl:template>

<xsl:template match="ol">
    <text:list text:style-name="FbeLstOl">
        <xsl:apply-templates select="li|text()|xsl:*"/>
    </text:list>
</xsl:template>

<xsl:template match="li">
    <text:list-item>
         <xsl:apply-templates select="." mode="mixedblock">
            <xsl:with-param name="pstyle" select="'FbeTxtLi'"/>
        </xsl:apply-templates>
    </text:list-item>
</xsl:template>

<xsl:template match="table">
    <xsl:variable name="var_Style">FbeTable<xsl:if test="caption">Sans</xsl:if></xsl:variable>
    <table:table table:style-name="{$var_Style}">
        <xsl:attribute name="table:name">Table&#123;generate-id(.)&#125;</xsl:attribute>
        <xsl:apply-templates select="col|colgroup|tr|text()|xsl:*"/>
    </table:table>
</xsl:template>

<xsl:template match="tr">
    <table:table-row table:style-name="FbeTableRow">
        <xsl:apply-templates select="td|th|text()|xsl:*"/>
    </table:table-row>
</xsl:template>

<xsl:template match="td|th">
    <xsl:variable name="pstyle">
        <xsl:choose>
            <xsl:when test="local-name() = 'th'">FbeTableHeader</xsl:when>
            <xsl:otherwise>FbeTableString</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    <table:table-cell table:style-name="FbeTableCell" office:value-type="string">
        <xsl:if test="@colspan">
            <xsl:attribute name="table:number-columns-spanned"><xsl:value-of select="@colspan"/></xsl:attribute>
        </xsl:if>
        <xsl:if test="@rowspan">
            <xsl:attribute name="table:number-rows-spanned"><xsl:value-of select="@rowspan"/></xsl:attribute>
        </xsl:if>
        <xsl:apply-templates select="." mode="mixedblock">
            <xsl:with-param name="pstyle" select="$pstyle"/>
        </xsl:apply-templates>
    </table:table-cell>
</xsl:template>

<xsl:template match="col">
    <table:table-column>
        <xsl:if test="@span">
            <xsl:attribute name="table:number-columns-repeated"><xsl:value-of select="@span"/></xsl:attribute>
        </xsl:if>
        <xsl:if test="@data-od-style">
            <xsl:attribute name="table:style-name"><xsl:value-of select="@data-od-style"/></xsl:attribute>
        </xsl:if>
    </table:table-column>
</xsl:template>

<xsl:template match="colgroup">
    <xsl:apply-templates select="col"/>
</xsl:template>

<xsl:template match="colgroup[@span]">
    <table:table-column table:number-columns-repeated="{@span}">
        <xsl:if test="@data-od-style">
            <xsl:attribute name="table:style-name"><xsl:value-of select="@data-od-style"/></xsl:attribute>
        </xsl:if>
    </table:table-column>
</xsl:template>

<xsl:template match="style|script|link">
</xsl:template>

<xsl:template match="*" mode="mixedblock">
    <xsl:param name="pstyle" select="'FbeTxt'"/>
    <xsl:choose>
        <xsl:when test="(count(p|ul) &gt; 0) or (@data-od-mode='no-p')">
            <xsl:apply-templates select="node()">
                <xsl:with-param name="style" select="$pstyle"/>
            </xsl:apply-templates>
        </xsl:when>
        <xsl:otherwise>
            <text:p text:style-name= "{$pstyle}"><xsl:apply-templates select="node()"/></text:p>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

</xsl:stylesheet>
