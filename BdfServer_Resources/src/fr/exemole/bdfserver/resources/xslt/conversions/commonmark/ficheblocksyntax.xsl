<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:ns="http://commonmark.org/xml/1.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

<xsl:output indent="no" omit-xml-declaration="yes" method="text" encoding="utf-8"/>

<xsl:template match="*" priority="-1">
</xsl:template>

<!-- root -->

<xsl:template match="/">
    <xsl:apply-templates select="ns:document/*"/>
</xsl:template>


<!-- block elements -->

<xsl:template match="ns:heading">
    <xsl:if test="position() != 1"><xsl:text>&#10;</xsl:text></xsl:if>
    <xsl:choose>
        <xsl:when test="@level = 1"><xsl:text>#</xsl:text></xsl:when>
        <xsl:when test="@level = 2"><xsl:text>##</xsl:text></xsl:when>
        <xsl:when test="@level = 3"><xsl:text>###</xsl:text></xsl:when>
        <xsl:when test="@level = 4"><xsl:text>####</xsl:text></xsl:when>
        <xsl:when test="@level = 5"><xsl:text>#####</xsl:text></xsl:when>
        <xsl:when test="@level = 6"><xsl:text>######</xsl:text></xsl:when>
    </xsl:choose>
    <xsl:apply-templates select="*"/>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="ns:paragraph">
    <xsl:apply-templates select="*"/>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="ns:block_quote">
    <xsl:apply-templates select="*" mode="quote_child"/>
</xsl:template>

<xsl:template match="ns:code_block">
    <xsl:text>+++&#10;&#10;</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>+++++++++++&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="ns:list">
    <xsl:if test="@type = 'ordered'">
        <xsl:text>[(mode=ol)]&#10;</xsl:text>
    </xsl:if>
    <xsl:apply-templates select="ns:item"/>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="ns:item">
    <xsl:param name="list_sequence" select="'-'"/>
    <xsl:apply-templates select="*" mode="item_child">
        <xsl:with-param name="list_sequence" select="$list_sequence"/>
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="ns:thematic_break">
    <xsl:text>&#10;???&#10;&lt;hr&gt;&#10;???&#10;&#10;</xsl:text>
</xsl:template>


<!-- inline elements -->

<xsl:template match="ns:text">
    <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="ns:code">
    <xsl:text>[` </xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>]</xsl:text>
</xsl:template>

<xsl:template match="ns:linebreak">
    <xsl:text>[%]</xsl:text>
</xsl:template>

<xsl:template match="ns:softbreak">
    <xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="ns:image">
    <xsl:text>[i(</xsl:text>
    <xsl:value-of select="@destination"/>
    <xsl:if test="string-length(@title) &gt; 0">
        <xsl:text> title="</xsl:text>
        <xsl:value-of select="@title"/>
        <xsl:text>"</xsl:text>
    </xsl:if>
    <xsl:text>) </xsl:text>
    <xsl:apply-templates select="*" mode="inline_child"/>
    <xsl:text>]</xsl:text>
</xsl:template>

<xsl:template match="ns:link">
    <xsl:text>[a(</xsl:text>
    <xsl:value-of select="@destination"/>
    <xsl:if test="string-length(@title) &gt; 0">
        <xsl:text> title="</xsl:text>
        <xsl:value-of select="@title"/>
        <xsl:text>"</xsl:text>
    </xsl:if>
    <xsl:text>) </xsl:text>
    <xsl:apply-templates select="*" mode="inline_child"/>
    <xsl:text>]</xsl:text>
</xsl:template>

<xsl:template match="ns:emph">
    <xsl:text>{</xsl:text>
    <xsl:apply-templates select="*" mode="inline_child"/>
    <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="ns:strong">
    <xsl:text>{{</xsl:text>
    <xsl:apply-templates select="*" mode="inline_child"/>
    <xsl:text>}}</xsl:text>
</xsl:template>


<!-- item_child mode -->

<xsl:template match="*" priority="-1" mode="item_child">
</xsl:template>

<xsl:template match="ns:paragraph" mode="item_child">
    <xsl:param name="list_sequence" select="'-'"/>
    <xsl:value-of select="$list_sequence"/>
    <xsl:text> </xsl:text>
    <xsl:apply-templates select="*"/>
    <xsl:text>&#10;</xsl:text>
</xsl:template>

<xsl:template match="ns:list" mode="item_child">
    <xsl:param name="list_sequence" select="'-'"/>
    <xsl:apply-templates select="ns:item">
        <xsl:with-param name="list_sequence" select="concat($list_sequence, '-')"/>
    </xsl:apply-templates>    
</xsl:template>


<!-- quote_child mode -->

<xsl:template match="*" priority="-1" mode="quote_child">
</xsl:template>

<xsl:template match="ns:paragraph" mode="quote_child">
    <xsl:text>> </xsl:text>
    <xsl:apply-templates select="*"/>
    <xsl:text>&#10;&#10;</xsl:text>
</xsl:template>

<xsl:template match="ns:block_quote" mode="quote_child">
    <xsl:apply-templates select="*" mode="quote_child"/>
</xsl:template>


<!-- inline_child mode -->

<xsl:template match="*" priority="-1" mode="inline_child">
    <xsl:apply-templates select="*" mode="inline_child"/>
</xsl:template>

<xsl:template match="ns:text" mode="inline_child">
    <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="ns:code" mode="inline_child">
    <xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>
