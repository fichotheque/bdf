<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="bdf://this/xslt/v2/catalogs/odt/_import.xsl"/>

    <xsl:param name="BDF_FICHOTHEQUEPATH" select="''"/>
    <xsl:param name="BDF_VERSION" select="''"/>
    <xsl:param name="EXTERNALTARGET" select="''"/>
    <xsl:param name="INCLUDESCRIPTS" select="''"/>
    <xsl:param name="SHOWEMPTY" select="1"/>
    <xsl:param name="STARTLEVEL" select="3"/>
    <xsl:param name="WORKINGLANG" select="'fr'"/>
    <xsl:param name="USER_SPHERE" select="''"/>
    <xsl:param name="USER_LOGIN" select="'default'"/>
    <xsl:param name="TEXT_COLON" select="' :'"/>
    <xsl:param name="TEXT_FIELDBULLET" select="''"/>
    <xsl:param name="TEXT_SECTIONEND" select="'❖ ❖ ❖'"/>
    <xsl:param name="WITH_FICHENUMBER" select="'yes'"/>
    <xsl:param name="WITH_RESOURCELOGO" select="'yes'"/>

    <xsl:include href="bdf://this/custom/import_odt.xsl"/>

</xsl:stylesheet>

