<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0">

    <xsl:template match="fiche" mode="component-Header">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:variable name="headingStyle">
            <xsl:choose>
                <xsl:when test="soustitre">FicheTitleWithSubtitle</xsl:when>
                <xsl:otherwise>FicheTitle</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:if test="$WITH_FICHENUMBER = 'yes'">
            <text:p text:style-name="FicheNumber">
                <xsl:apply-templates select="." mode="include-fiche-Phrase">
                    <xsl:with-param name="param_Lang" select="$param_Lang"/>
                </xsl:apply-templates>
            </text:p>
        </xsl:if>
        <text:h text:style-name="{$headingStyle}" text:outline-level="1">
            <xsl:value-of select="titre"/>
        </text:h>
    </xsl:template>
    
    <xsl:template match="soustitre" mode="component-Header">
        <text:p text:style-name="FicheSubtitle">
            <xsl:apply-templates select="*" mode="ficheitem-Inline"/>
        </text:p>
    </xsl:template>
    
    <xsl:template match="extraitthesaurus|motcles" mode="component-Inline">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <text:p text:style-name="FicheField">
            <xsl:call-template name="component-addLabelInline">
                <xsl:with-param name="param_Label" select="$param_Label"/>
            </xsl:call-template>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="." mode="motcle-Inline">
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
        </text:p>
    </xsl:template>
    
    <xsl:template match="extraitthesaurus|motcles" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:call-template name="component-addLabelParagraph">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:call-template>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="." mode="motcle-Listitem">
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
        </text:list>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <xsl:call-template name="component-addLabelParagraph">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:call-template>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="." mode="include-Listitem"/>
        </text:list>
    </xsl:template>
    
    <xsl:template match="extraitaddenda|documents" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <xsl:call-template name="component-addLabelParagraph">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:call-template>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="." mode="include-Listitem"/>
        </text:list>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="component-Table">
        <xsl:param name="param_Label" select="''"/>
        <xsl:call-template name="component-addLabelParagraph">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:call-template>
         <table:table table:name="Tableau{generate-id(.)}" table:style-name="FicheTable">
            <table:table-column table:number-columns-repeated="{@cellmax}"/>
             <xsl:apply-templates select="." mode="include-table-Fiche"/>
        </table:table>
    </xsl:template>

    <xsl:template match="extraitalbum|illustrations" mode="component-Inline">
        <xsl:param name="param_Label" select="''"/>
        <text:p text:style-name="FicheField">
            <xsl:call-template name="component-addLabelInline">
                <xsl:with-param name="param_Label" select="$param_Label"/>
            </xsl:call-template>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="." mode="include-Inline"/>
        </text:p>
    </xsl:template>
    
    <xsl:template match="propriete|redacteurs|lang|information" mode="component-Inline">
        <xsl:param name="param_Label" select="''"/>
        <text:p text:style-name="FicheField">
            <xsl:call-template name="component-addLabelInline">
                <xsl:with-param name="param_Label" select="$param_Label"/>
            </xsl:call-template>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="*" mode="ficheitem-Inline"/>
        </text:p>
    </xsl:template>
    
    <xsl:template match="information" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <xsl:call-template name="component-addLabelParagraph">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:call-template>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="*" mode="ficheitem-Listitem"/>
        </text:list>
    </xsl:template>
    
    <xsl:template match="information" mode="component-Block">
        <xsl:param name="param_Label" select="''"/>
        <xsl:call-template name="component-addLabelParagraph">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:call-template>
        <xsl:apply-templates select="*" mode="ficheitem-Block"/>
    </xsl:template>
       
    <xsl:template match="section" mode="component-Section">
        <xsl:param name="param_Label" select="''"/>
        <xsl:variable name="labelStyle">
            <xsl:choose>
                <xsl:when test="local-name(*[1]) = 'h'">FicheSectionLabelMerge</xsl:when>
                <xsl:otherwise>FicheSectionLabel</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <text:h text:style-name="{$labelStyle}" text:outline-level="2">
            <xsl:value-of select="$param_Label"/>
        </text:h>
        <xsl:apply-templates select="*"/>
        <xsl:call-template name="component-addSectionEnd"/>
    </xsl:template>
    
    <xsl:template match="section" mode="component-Heading">
        <xsl:param name="param_Label" select="''"/>
        <xsl:apply-templates select="." mode="component-Section">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="section" mode="component-FieldReference">
        <xsl:apply-templates select="*"/>
    </xsl:template>
    
    <xsl:template match="fiche" mode="component-Chrono">
        <xsl:param name="param_CreationLabel" select="''"/>
        <xsl:param name="param_ModificationLabel" select="''"/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <text:p text:style-name="FicheChrono">
            <xsl:if test="$WITH_FICHENUMBER = 'end'">
                <xsl:apply-templates select="." mode="include-fiche-Phrase">
                    <xsl:with-param name="param_Lang" select="$param_Lang"/>
                </xsl:apply-templates>
                <xsl:text> — </xsl:text>
            </xsl:if>
            <xsl:value-of select="$param_CreationLabel"/>
            <xsl:call-template name="component-addColon"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="chrono/datation[1]" mode="ficheitem-Inline"/>
            <xsl:text> — </xsl:text>
            <xsl:value-of select="$param_ModificationLabel"/>
            <xsl:call-template name="component-addColon"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="chrono/datation[position() = last()]" mode="ficheitem-Inline"/>
        </text:p>
    </xsl:template>
    
    <xsl:template match="data" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Class" select="'FicheFieldLi'"/>
        <xsl:call-template name="component-addLabelParagraph">
            <xsl:with-param name="param_Label" select="$param_Label"/>
        </xsl:call-template>
        <text:list text:style-name="FicheUl">
            <xsl:apply-templates select="." mode="data-Listitem">
                <xsl:with-param name="param_Class" select="$param_Class"/>
            </xsl:apply-templates>
        </text:list>
    </xsl:template>
    
    <!--
********************************************************************************
* component- functions                                                       
********************************************************************************
-->

    <xsl:template name="component-addLabelParagraph">
        <xsl:param name="param_Label" select="''"/>
        <text:p text:style-name="FicheFieldLabel">
            <xsl:call-template name="component-addFieldBullet"/>
            <text:span text:style-name="FicheLabel">
                <xsl:value-of select="$param_Label"/>
                <xsl:call-template name="component-addColon"/>
            </text:span>
        </text:p>
    </xsl:template>
    
    <xsl:template name="component-addLabelInline">
        <xsl:param name="param_Label" select="''"/>
        <xsl:call-template name="component-addFieldBullet"/>
        <text:span text:style-name="FicheLabel">
            <xsl:value-of select="$param_Label"/>
            <xsl:call-template name="component-addColon"/>
        </text:span>
    </xsl:template>

    <xsl:template name="component-addFieldBullet">
        <xsl:if test="string-length($TEXT_FIELDBULLET) &gt; 0">
            <text:span text:style-name="FicheFieldBullet">
                <xsl:value-of select="$TEXT_FIELDBULLET"/>
            </text:span>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="component-addColon">
        <xsl:value-of select="$TEXT_COLON"/>
    </xsl:template>
    
    <xsl:template name="component-addSectionEnd">
        <xsl:if test="string-length($TEXT_SECTIONEND) &gt; 0">
            <text:p text:style-name="FicheSectionEnd">
                <xsl:value-of select="$TEXT_SECTIONEND"/>
            </text:p>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>
