<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <xsl:template match="fiche" mode="component-Header">
        <h1><xsl:apply-templates select="." mode="include-fiche-Title"/></h1>
    </xsl:template>
    
    <xsl:template match="soustitre" mode="component-Header">
        <p class="fiche-Description" data-bdf-field="{name(.)}" data-bdf-component-name="{name(.)}"><xsl:apply-templates select="*" mode="ficheitem-Inline"/></p>
    </xsl:template>
    
    <xsl:template match="extraitthesaurus|motcles" mode="component-Inline">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <p class="fiche-Field" data-bdf-field="thesaurus" data-bdf-component-name="{@name}">
            <span class="fiche-Label"><xsl:value-of select="$param_Label"/> : </span>
            <xsl:apply-templates select="." mode="motcle-Inline">
                <xsl:with-param name="param_Separator" select="$param_Separator"/>
                <xsl:with-param name="param_Lang" select="$param_Lang"/>
            </xsl:apply-templates>
        </p>
    </xsl:template>
    
    <xsl:template match="extraitthesaurus|motcles" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <div data-bdf-field="thesaurus" data-bdf-component-name="{@name}">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> : </p>
        <xsl:if test="boolean(*)">
            <ul class="fiche-List">
                <xsl:apply-templates select="." mode="motcle-Listitem">
                    <xsl:with-param name="param_Lang" select="$param_Lang"/>
                </xsl:apply-templates>
            </ul>
        </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="component-Table">
        <xsl:param name="param_Label" select="''"/>
        <div data-bdf-field="corpus" data-bdf-component-name="{@name}">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> : </p>
            <xsl:if test="boolean(*)">
                <table class="fiche-Table">
                    <xsl:apply-templates select="." mode="include-Table"/>
                </table>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches|extraitaddenda|documents" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <div data-bdf-field="corpus" data-bdf-component-name="{@name}">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> : </p>
            <xsl:if test="boolean(*)">
                <ul class="fiche-List">
                    <xsl:apply-templates select="." mode="include-Listitem" />
                </ul>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="extraitaddenda|documents" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <div data-bdf-field="addenda" data-bdf-component-name="{@name}">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> : </p>
            <xsl:if test="boolean(*)">
                <ul class="fiche-List">
                    <xsl:apply-templates select="." mode="include-Listitem" />
                </ul>
            </xsl:if>
        </div>
    </xsl:template>

    <xsl:template match="extraitalbum|illustrations" mode="component-Inline">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Separator" select="' '"/>
        <p class="fiche-Field" data-bdf-field="album" data-bdf-component-name="{@name}">
            <span class="fiche-Label"><xsl:value-of select="$param_Label"/> : </span>
            <xsl:apply-templates select="." mode="include-Inline">
                <xsl:with-param name="param_Separator" select="$param_Separator"/>
            </xsl:apply-templates>
        </p>
    </xsl:template>

    <xsl:template match="redacteurs|lang" mode="component-Inline">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Separator" select="', '"/>
        <p class="fiche-Field" data-bdf-field="{name(.)}" data-bdf-component-name="{name(.)}">
            <span class="fiche-Label"><xsl:value-of select="$param_Label"/> : </span>
            <xsl:apply-templates select="*" mode="ficheitem-Inline">
                <xsl:with-param name="param_Separator" select="$param_Separator"/>
            </xsl:apply-templates>
        </p>
    </xsl:template>
    
    <xsl:template match="propriete|information" mode="component-Inline">
        <xsl:param name="param_Label" select="''"/>
        <xsl:param name="param_Separator" select="', '"/>
        <p class="fiche-Field" data-bdf-field="{name(.)}" data-bdf-component-name="{name(.)}_{@name}">
            <span class="fiche-Label"><xsl:value-of select="$param_Label"/> : </span>
            <xsl:apply-templates select="*" mode="ficheitem-Inline">
                <xsl:with-param name="param_Separator" select="$param_Separator"/>
            </xsl:apply-templates>
        </p>
    </xsl:template>
    
    <xsl:template match="information" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <div data-bdf-field="information" data-bdf-component-name="information_{@name}">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> :</p>
            <xsl:if test="boolean(*)">
                <ul class="fiche-List">
                    <xsl:apply-templates select="*" mode="ficheitem-Listitem" />
                </ul>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="information" mode="component-Block">
        <xsl:param name="param_Label" select="''"/>
        <div data-bdf-field="information" data-bdf-component-name="information_{@name}">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> :</p>
            <xsl:if test="boolean(*)">
                <div class="fiche-Block">
                    <xsl:apply-templates select="*" mode="ficheitem-Block" />
                </div>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="section" mode="component-Section">
        <xsl:param name="param_Label" select="''"/>
        <div data-bdf-field="section" data-bdf-component-name="section_{@name}">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> :</p>
            <xsl:if test="boolean(*)">
                <section class="fiche-Section">
                    <xsl:apply-templates select="*"/>
                </section>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="section" mode="component-Heading">
        <xsl:param name="param_HeadingLevel" select="3"/>
        <xsl:param name="param_HeadingClass" select="''"/>
        <xsl:param name="param_StartLevel" select="''"/>
        <xsl:param name="param_Label" select="''"/>
        <xsl:variable name="var_StartLevel">
            <xsl:choose>
                <xsl:when test="string-length($param_StartLevel) = 0">
                    <xsl:value-of select="number($param_HeadingLevel) + 1"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$param_StartLevel"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <div data-bdf-field="section" data-bdf-component-name="section_{@name}" class="fiche-HeadingContainer">
            <xsl:element name="h{$param_HeadingLevel}">
                <xsl:if test="string-length($param_HeadingClass) &gt; 0">
                    <xsl:attribute name="class">
                        <xsl:value-of select="$param_HeadingClass"/>
                    </xsl:attribute>
                </xsl:if>
                <xsl:value-of select="$param_Label"/>
            </xsl:element>
            <xsl:if test="boolean(*)">
                <div class="fiche-Section">
                    <xsl:apply-templates select="*">
                        <xsl:with-param name="param_StartLevel" select="$var_StartLevel"/>
                    </xsl:apply-templates>
                </div>
            </xsl:if>
        </div>
    </xsl:template>
    
    <xsl:template match="section" mode="component-FieldReference">
        <div data-bdf-field="section" data-bdf-component-name="section_{@name}">
            <xsl:apply-templates select="*"/>
        </div>
    </xsl:template>
    
    <xsl:template match="fiche" mode="component-Chrono">
        <xsl:param name="param_CreationLabel" select="''"/>
        <xsl:param name="param_ModificationLabel" select="''"/>
        <p class="fiche-Time"><xsl:value-of select="$param_CreationLabel"/> : <xsl:apply-templates select="chrono/datation[1]" mode="ficheitem-Inline"/> — <xsl:value-of select="$param_ModificationLabel"/> : <xsl:apply-templates select="chrono/datation[position() = last()]" mode="ficheitem-Inline"/></p>
    </xsl:template>
    
    <xsl:template match="data" mode="component-Listitem">
        <xsl:param name="param_Label" select="''"/>
        <p class="fiche-Label"><xsl:value-of select="$param_Label"/> : </p>
        <xsl:if test="boolean(*)">
            <ul class="fiche-List">
                <xsl:apply-templates select="." mode="data-Listitem" />
            </ul>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="fiche" mode="component-Parentage">
        <xsl:param name="param_Label" select="''"/>
        <xsl:if test="fiche|motcle">
            <p class="fiche-Label"><xsl:value-of select="$param_Label"/> : </p>
            <xsl:if test="boolean(*)">
                <ul class="fiche-List">
                    <xsl:apply-templates select="motcle" mode="motcle-Listitem_TextOnly"/>
                    <xsl:apply-templates select="fiche" mode="include-fiche-Listitem"/>
                </ul>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="component-addRawText">
        <xsl:param name="param_ComponentName" select="''"/>
        <xsl:param name="param_Text" select="''"/>
        <xsl:choose>
            <xsl:when test="string-length($param_ComponentName) = 0">
            	<xsl:value-of select="$param_Text" disable-output-escaping="yes"/>
            </xsl:when>
            <xsl:otherwise>
                <div data-bdf-milestone="comment" data-bdf-component-name="{$param_ComponentName}">
                    <xsl:value-of select="$param_Text" disable-output-escaping="yes"/>
                </div>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
</xsl:stylesheet>

