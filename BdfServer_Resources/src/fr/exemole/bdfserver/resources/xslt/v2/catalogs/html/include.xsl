<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">


<!--
********************************************************************************
* include-
********************************************************************************
-->

    <xsl:template match="extraitcorpus|fiches" mode="include-Title">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="intitule|title" mode="include-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="include-Block">
        <xsl:apply-templates select="fiche" mode="include-fiche-Block"/>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="include-Listitem">
        <xsl:apply-templates select="fiche" mode="include-fiche-Listitem"/>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="include-Table">
        <xsl:apply-templates select="fiche" mode="include-table-Fiche"/>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="include-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:apply-templates select="fiche" mode="include-fiche-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="extraitaddenda|documents" mode="include-Listitem">
        <xsl:apply-templates select="document" mode="include-document-Listitem"/>
    </xsl:template>

    <xsl:template match="extraitaddenda|documents" mode="include-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:apply-templates select="document" mode="include-document-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="extraitalbum|illustrations" mode="include-Listitem">
        <xsl:apply-templates select="illustration" mode="include-illustration-Listitem"/>
    </xsl:template>

    <xsl:template match="extraitalbum|illustrations" mode="include-Inline">
        <xsl:param name="param_Separator" select="' &#x00A0; '"/>
        <xsl:apply-templates select="illustration" mode="include-illustration-Inline">
            <xsl:with-param name="param_Separator" select="$param_Separator"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="intitule|title|phrase" mode="include-LangLabel">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $param_Lang]"><xsl:value-of select="label[@xml:lang = $param_Lang]"/></xsl:when>
            <xsl:when test="label[@xml:lang = substring-before($param_Lang, '-')]"><xsl:value-of select="label[@xml:lang = substring-before($param_Lang, '-')]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="label[1]"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>


<!--
********************************************************************************
* include-fiche-
********************************************************************************
-->

    <xsl:template match="fiche" mode="include-fiche-Block">
        <p>
            <xsl:apply-templates select="." mode="include-fiche-Link"/>
        </p>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Listitem">
        <li>
            <xsl:apply-templates select="." mode="include-fiche-Link"/>
        </li>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:apply-templates select="." mode="include-fiche-Link"/>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Link">
        <a>
            <xsl:attribute name="href">
                <xsl:call-template name="url-getFicheHref">
                    <xsl:with-param name="param_Corpus" select="@corpus"/>
                    <xsl:with-param name="param_Id" select="@id"/>
                    <xsl:with-param name="param_Fichotheque" select="@fichotheque"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="include-fiche-Title"/>
        </a>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Title">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="include-fiche-Phrase">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
        <xsl:text> – </xsl:text>
        <xsl:value-of select="titre"/>
    </xsl:template>
    
    <xsl:template match="fiche" mode="include-fiche-Phrase">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="intitule|phrase" mode="include-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    
<!--
********************************************************************************
* include-document-
********************************************************************************
-->

    <xsl:template match="document" mode="include-document-Listitem">
        <li>
            <xsl:for-each select="version">
                 <xsl:if test="position() &gt; 1">
                     <br/>
                 </xsl:if>
                 <xsl:apply-templates select="." mode="include-document-VersionLink"/>
            </xsl:for-each>
        </li>
    </xsl:template>

    <xsl:template match="document" mode="include-document-Inline">
        <xsl:param name="param_Separator" select="' ; '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:for-each select="version">
            <xsl:if test="position() &gt; 1">
                <text> / </text>
            </xsl:if>
            <xsl:apply-templates select="." mode="include-document-VersionLink"/>
       </xsl:for-each>
    </xsl:template>

    <xsl:template match="version" mode="include-document-VersionLink">
        <a>
            <xsl:attribute name="href">
                <xsl:call-template name="url-getDocumentHref">
                    <xsl:with-param name="param_Addenda" select="../@addenda"/>
                    <xsl:with-param name="param_Id" select="../@id"/>
                    <xsl:with-param name="param_Basename" select="../@basename"/>
                    <xsl:with-param name="param_Extension" select="@extension"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:value-of select="../@basename"/>
            <xsl:text>.</xsl:text><xsl:value-of select="@extension"/>
        </a>
        <small><xsl:text> (</xsl:text><xsl:apply-templates select="." mode="include-document-Size"/>)</small>
    </xsl:template>

    <xsl:template match="version" mode="include-document-Size">
        <xsl:apply-templates select="size" mode="include-document-LangLabel"/>
    </xsl:template>

    <xsl:template match="size" mode="include-document-LangLabel">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
 
<!--
********************************************************************************
* include-illustration-
********************************************************************************
-->

    <xsl:template match="illustration" mode="include-illustration-Listitem">
        <li>
            <xsl:apply-templates select="." mode="include-illustration-Link"/>
        </li>
    </xsl:template>

    <xsl:template match="illustration" mode="include-illustration-Inline">
        <xsl:param name="param_Separator" select="' &#x00A0; '"/>
        <xsl:if test="position() &gt; 1"><xsl:value-of select="$param_Separator"/></xsl:if>
        <xsl:apply-templates select="." mode="include-illustration-Link"/>
    </xsl:template>

    <xsl:template match="illustration" mode="include-illustration-Link">
        <a>
            <xsl:attribute name="href">
                <xsl:call-template name="url-getIllustrationHref">
                    <xsl:with-param name="param_Album" select="@album"/>
                    <xsl:with-param name="param_Id" select="@id"/>
                    <xsl:with-param name="param_Format" select="@format"/>
                    <xsl:with-param name="param_Dim" select="''"/>
                </xsl:call-template>
            </xsl:attribute>
            <img>
                <xsl:attribute name="src">
                    <xsl:call-template name="url-getIllustrationHref">
                        <xsl:with-param name="param_Album" select="@album"/>
                        <xsl:with-param name="param_Id" select="@id"/>
                        <xsl:with-param name="param_Format" select="@format"/>
                        <xsl:with-param name="param_Dim" select="'_mini'"/>
                    </xsl:call-template>
                </xsl:attribute>
            </img>
        </a>
    </xsl:template>


<!--
********************************************************************************
* include-table-
********************************************************************************
-->

<xsl:template match="fiche" mode="include-table-Fiche">
    <tr>
        <xsl:call-template name="include-table-addCell">
            <xsl:with-param name="order" select="1"/>
            <xsl:with-param name="max" select="@cellmax"/>
        </xsl:call-template>
    </tr>
</xsl:template>

<xsl:template name="include-table-addCell">
    <xsl:param name="order"/>
    <xsl:param name="max"/>
    <xsl:if test="$order &lt;= $max">
        <td>
             <xsl:attribute name="class">
                <xsl:text>fbe-table-Cell</xsl:text>
                <xsl:if test="*[@cell-order=$order][1]/@cell-format='number'"><xsl:text> fbe-table-Number</xsl:text></xsl:if>
            </xsl:attribute>
            <xsl:apply-templates select="*[@cell-order=$order]" mode="include-table-Cell"/>
        </td>
        <xsl:call-template name="include-table-addCell">
            <xsl:with-param name="order" select="$order + 1"/>
            <xsl:with-param name="max" select="$max"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template match="propriete|redacteurs|lang|information|soustitre" mode="include-table-Cell">
    <xsl:apply-templates select="*" mode="ficheitem-Inline"/>
</xsl:template>

<xsl:template match="extraitcorpus|fiches|extraitaddenda|documents|extraitalbum|illustrations" mode="include-table-Cell">
    <xsl:apply-templates select="." mode="include-Inline"/>
</xsl:template>

<xsl:template match="extraitthesaurus|motcles" mode="include-table-Cell">
    <xsl:apply-templates select="." mode="motcle-Inline"/>
</xsl:template>

<xsl:template match="extraitthesaurus|motcles[@cell-format='icon']" mode="include-table-Cell">
    <xsl:apply-templates select="motcle/icon" mode="motcle-Icon"/>
</xsl:template>

<xsl:template match="section" mode="include-table-Cell">
    <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="titre" mode="include-table-Cell">
    <xsl:apply-templates select=".." mode="include-fiche-Link"/>
</xsl:template>


</xsl:stylesheet>