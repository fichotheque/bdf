<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="../url.xsl"/>
    <xsl:import href="ficheitem.xsl"/>
    <xsl:import href="ficheblock.xsl"/>
    <xsl:import href="include.xsl"/>
    <xsl:import href="motcle.xsl"/>
    <xsl:import href="component.xsl"/>
    <xsl:import href="resource.xsl"/>
    <xsl:import href="data.xsl"/>

    <xsl:param name="BDF_FICHOTHEQUEPATH" select="''"/>
    <xsl:param name="WORKINGLANG" select="'fr'"/>
    <xsl:param name="STARTLEVEL" select="3"/>
    <xsl:param name="TEXT_COLON" select="' :'"/>
    <xsl:param name="TEXT_FIELDBULLET" select="''"/>
    <xsl:param name="TEXT_SECTIONEND" select="'❖ ❖ ❖'"/>
    <xsl:param name="WITH_FICHENUMBER" select="'yes'"/>
    <xsl:param name="WITH_RESOURCELOGO" select="'yes'"/>
    
</xsl:stylesheet>


