<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" version="1.0">


<!--
********************************************************************************
* include-
********************************************************************************
-->

    <xsl:template match="extraitcorpus|fiches" mode="include-Title">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="intitule|title" mode="include-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="extraitcorpus|fiches" mode="include-Block">
        <xsl:param name="param_Class" select=""/>
        <xsl:apply-templates select="fiche" mode="include-fiche-Block">
            <xsl:with-param name="param_Class" select="$param_Class"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="include-Listitem">
        <xsl:apply-templates select="fiche" mode="include-fiche-Listitem"/>
    </xsl:template>

    <xsl:template match="extraitcorpus|fiches" mode="include-Inline">
        <xsl:apply-templates select="fiche" mode="include-fiche-Inline"/>
    </xsl:template>

    <xsl:template match="extraitaddenda|documents" mode="include-Listitem">
        <xsl:apply-templates select="document" mode="include-document-Listitem"/>
    </xsl:template>

    <xsl:template match="extraitaddenda|documents" mode="include-Inline">
        <xsl:apply-templates select="document" mode="include-document-Inline"/>
    </xsl:template>
    
    <xsl:template match="extraitalbum|illustrations" mode="include-Listitem">
        <xsl:apply-templates select="illustration" mode="include-illustration-Listitem"/>
    </xsl:template>

    <xsl:template match="extraitalbum|illustrations" mode="include-Inline">
        <xsl:apply-templates select="illustration" mode="include-illustration-Inline"/>
    </xsl:template>
    
    <xsl:template match="intitule|title|phrase" mode="include-LangLabel">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $param_Lang]"><xsl:value-of select="label[@xml:lang = $param_Lang]"/></xsl:when>
            <xsl:when test="label[@xml:lang = substring-before($param_Lang, '-')]"><xsl:value-of select="label[@xml:lang = substring-before($param_Lang, '-')]"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="label[1]"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>


<!--
********************************************************************************
* include-fiche-
********************************************************************************
-->

    <xsl:template match="fiche" mode="include-fiche-Block">
        <xsl:param name="param_Class" select=""/>
        <text:p text:style-name="{$param_Class}"><xsl:apply-templates select="." mode="include-fiche-Link"/></text:p>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Listitem">
        <text:list-item>
            <text:p text:style-name="FicheFieldLi">
                <xsl:apply-templates select="." mode="include-fiche-Link"/>
            </text:p>
        </text:list-item>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Inline">
        <xsl:call-template name="include-fiche-addSeparator"/>
        <xsl:apply-templates select="." mode="include-fiche-Link"/>
    </xsl:template>

    <xsl:template match="fiche" mode="include-fiche-Link">
        <text:a xlink:type="simple">
            <xsl:attribute name="xlink:href">
                <xsl:call-template name="url-getFicheHref">
                    <xsl:with-param name="param_Corpus" select="@corpus"/>
                    <xsl:with-param name="param_Id" select="@id"/>
                    <xsl:with-param name="param_Fichotheque" select="@fichotheque"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:apply-templates select="." mode="include-fiche-Title"/>
        </text:a>
    </xsl:template>
    
    <xsl:template match="fiche" mode="include-fiche-Title">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="." mode="include-fiche-Phrase">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
        <xsl:text> – </xsl:text>
        <xsl:value-of select="titre"/>
    </xsl:template>
    
    <xsl:template match="fiche" mode="include-fiche-Phrase">
        <xsl:param name="param_Lang" select="$WORKINGLANG"/>
        <xsl:apply-templates select="intitule|phrase" mode="include-LangLabel">
            <xsl:with-param name="param_Lang" select="$param_Lang"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template name="include-fiche-addSeparator">
        <xsl:if test="position() &gt; 1">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:template>
    
    
<!--
********************************************************************************
* include-document-
********************************************************************************
-->

    <xsl:template match="document" mode="include-document-Listitem">
        <text:list-item>
            <text:p text:style-name="FicheDocument">
                <xsl:apply-templates select="version" mode="include-document-VersionLink"/>
            </text:p>
        </text:list-item>
    </xsl:template>

    <xsl:template match="document" mode="include-document-Inline">
        <xsl:call-template name="include-document-addSeparator"/>
        <xsl:apply-templates select="version" mode="include-document-VersionLink"/>
    </xsl:template>

    <xsl:template match="version" mode="include-document-VersionLink">
        <xsl:call-template name="include-document-addVersionSeparator"/>
        <text:a xlink:type="simple">
            <xsl:attribute name="xlink:href">
                <xsl:call-template name="url-getDocumentHref">
                    <xsl:with-param name="param_Addenda" select="../@addenda"/>
                    <xsl:with-param name="param_Id" select="../@id"/>
                    <xsl:with-param name="param_Basename" select="../@basename"/>
                    <xsl:with-param name="param_Extension" select="@extension"/>
                </xsl:call-template>
            </xsl:attribute>
            <xsl:if test="position() = 1">
                <xsl:value-of select="../@basename"/>
            </xsl:if>
            <xsl:text>.</xsl:text><xsl:value-of select="@extension"/>
        </text:a>
        <text:span text:style-name="FicheSize"><xsl:text> (</xsl:text><xsl:apply-templates select="." mode="include-document-Size"/>)</text:span>
    </xsl:template>

    <xsl:template match="version" mode="include-document-Size">
        <xsl:apply-templates select="size" mode="include-document-LangLabel"/>
    </xsl:template>

    <xsl:template match="size" mode="include-document-LangLabel">
        <xsl:choose>
            <xsl:when test="label[@xml:lang = $WORKINGLANG]"><xsl:value-of select="label[@xml:lang = $WORKINGLANG]"/></xsl:when>
            <xsl:when test="label"><xsl:value-of select="label[1]"/></xsl:when>
            <xsl:otherwise></xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="include-document-addSeparator">
        <xsl:if test="position() &gt; 1">
            <xsl:text>, </xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="include-document-addVersionSeparator">
        <xsl:if test="position() &gt; 1">
            <xsl:text> / </xsl:text>
        </xsl:if>
    </xsl:template>


<!--
********************************************************************************
* include-illustration-
********************************************************************************
-->

    <xsl:template match="illustration" mode="include-illustration-Listitem">
        <text:list-item>
            <text:p text:style-name="FicheFieldLi">
                <xsl:apply-templates select="." mode="include-illustration-Inline"/>
            </text:p>
        </text:list-item>
    </xsl:template>

    <xsl:template match="illustration" mode="include-illustration-Inline">
        <draw:frame draw:name="{generate-id(.)}" draw:style-name="FbeIllustration" text:anchor-type="as-char" svg:width="{@ratio}cm" svg:height="1cm" draw:z-index="0">
            <draw:image xlink:href="bdf://this/illustrations/{@album}-{@id}" xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad"/>
        </draw:frame>
        <xsl:text> </xsl:text>
    </xsl:template>


<!--
********************************************************************************
* include-table-
********************************************************************************
-->

<xsl:template match="fiche" mode="include-table-Fiche">
    <table:table-row table:style-name="FbeTableRow">
        <xsl:call-template name="include-table-addCell">
            <xsl:with-param name="order" select="1"/>
            <xsl:with-param name="max" select="@cellmax"/>
        </xsl:call-template>
    </table:table-row>
</xsl:template>

<xsl:template name="include-table-addCell">
    <xsl:param name="order"/>
    <xsl:param name="max"/>
    <xsl:if test="$order &lt;= $max">
    <xsl:variable name="PStyle">FbeTable<xsl:choose><xsl:when test="*[@cell-order=$order][1]/@cell-format='number'">Number</xsl:when><xsl:otherwise>String</xsl:otherwise></xsl:choose></xsl:variable>
        <table:table-cell table:style-name="FbeTableCell" office:value-type="string">
            <xsl:apply-templates select="*[@cell-order=$order]" mode="include-table-Cell">
                <xsl:with-param name="param_PStyle" select="$PStyle"/>
            </xsl:apply-templates>
        </table:table-cell>
        <xsl:call-template name="include-table-addCell">
            <xsl:with-param name="order" select="$order + 1"/>
            <xsl:with-param name="max" select="$max"/>
        </xsl:call-template>
    </xsl:if>
</xsl:template>

<xsl:template match="propriete|redacteurs|lang|information|soustitre" mode="include-table-Cell">
    <xsl:param name="param_PStyle"/>
    <text:p text:style-name="{$param_PStyle}">
        <xsl:apply-templates select="*" mode="ficheitem-Inline"/>
    </text:p>
</xsl:template>

<xsl:template match="extraitcorpus|fiches|extraitaddenda|documents|extraitalbum|illustrations" mode="include-table-Cell">
    <xsl:param name="param_PStyle"/>
    <text:p text:style-name="{$param_PStyle}">
        <xsl:apply-templates select="." mode="include-Inline"/>
    </text:p>
</xsl:template>

<xsl:template match="extraitthesaurus|motcles" mode="include-table-Cell">
    <xsl:param name="param_PStyle"/>
    <text:p text:style-name="{$param_PStyle}">
        <xsl:apply-templates select="." mode="motcle-Inline"/>
    </text:p>
</xsl:template>

<xsl:template match="section" mode="include-table-Cell">
    <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="titre" mode="include-table-Cell">
    <xsl:param name="param_PStyle"/>
    <text:p text:style-name="{$param_PStyle}">
        <xsl:apply-templates select=".." mode="include-fiche-Link"/>
    </text:p>
</xsl:template>

</xsl:stylesheet>
