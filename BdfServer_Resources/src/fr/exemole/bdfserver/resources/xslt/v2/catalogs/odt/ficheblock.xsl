<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0">

<!--
********************************************************************************
* ficheblock-
********************************************************************************
-->

    <xsl:template match="*" mode="ficheblock-ParagraphContent">
        <xsl:if test='(count(*) = 0) and (string-length(.) =0)'><xsl:text>&#x00A0;</xsl:text></xsl:if>
        <xsl:for-each select="node()">
            <xsl:apply-templates select="." />
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="*" mode="ficheblock-Caption">
        <xsl:if test="numero = true() or legende = true()">
            <text:p text:style-name="FbeCpt">
                <xsl:apply-templates select="numero"/>
                <xsl:apply-templates select="legende"/>
            </text:p>
        </xsl:if>
    </xsl:template>

    

<!--
********************************************************************************
* spans
********************************************************************************
-->

    <xsl:template match="s[@ref-error]" priority="10">
        <text:span text:style-name="FbeTxtError">#Ref. err: <xsl:value-of select="@ref-error"/>#</text:span><text:span text:style-name="FbeTxtEm"><xsl:value-of select="."/></text:span>
    </xsl:template>
    
    <xsl:template match="s[@type='em']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Em'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="s[@type='emstrg']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'EmStrg'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="s[@type='strg']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Strg'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="s[@type='link']">
        <text:a xlink:type="simple" xlink:href="{@ref}"><text:span text:style-name="FbeTxtLink"><xsl:value-of select="."/></text:span></text:a>
    </xsl:template>

    <xsl:template match="s[@type='iref']">
        <xsl:variable name="var_Iref" select="@iref"/>
        <xsl:variable name="var_Target" select="//p[@type='note'][s[@type='anchor' and @iref=$var_Iref]]"/>
        <xsl:choose>
            <xsl:when test="count($var_Target) &gt; 0">
                <text:note text:note-class="footnote">
                    <text:note-citation>
                        <xsl:value-of select="$var_Iref"/>
                    </text:note-citation>
                    <text:note-body>
                        <xsl:apply-templates select="$var_Target" mode="ficheblock-paragraph-StyledP">
                            <xsl:with-param name="param_Class" select="'FbeFootnote'"/>
                        </xsl:apply-templates>
                    </text:note-body>
                </text:note>
            </xsl:when>
            <xsl:otherwise>
                <text:a xlink:type="simple" xlink:href="#iref_{$var_Iref}"><text:span text:style-name="FbeTxtIref"><xsl:value-of select="."/></text:span></text:a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="s[@type='iref'][parent::p[@type='note']]">
        <xsl:variable name="var_Iref" select="@iref"/>
        <text:a xlink:type="simple" xlink:href="#iref_{$var_Iref}"><text:span text:style-name="FbeTxtIref"><xsl:value-of select="."/></text:span></text:a>
    </xsl:template>
    
    <xsl:template match="s[@type='iref'][@variant='text']">
        <xsl:variable name="var_Iref" select="@iref"/>
        <text:a xlink:type="simple" xlink:href="#iref_{$var_Iref}"><text:span text:style-name="FbeTxtLink"><xsl:value-of select="."/></text:span></text:a>
    </xsl:template>

    <xsl:template match="s[@type='anchor']">
        <xsl:variable name="var_Iref" select="@iref"/>
        <xsl:variable name="var_Source" select="//s[@type='iref' and @iref=$var_Iref][1]"/>
        <xsl:if test="(count($var_Source) = 0) or ($var_Source/../@type = 'note') or ($var_Source/@variant = 'text')">
            <text:bookmark-start text:name="iref_{$var_Iref}"/>
            <text:span text:style-name="FbeTxtAnchor"><xsl:value-of select="."/></text:span>
            <text:bookmark-end text:name="iref_{$var_Iref}"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="s[@type='anchor'][@variant='text']">
        <xsl:variable name="var_Iref" select="@iref"/>
        <xsl:variable name="var_Source" select="//s[@type='iref' and @iref=$var_Iref][1]"/>
        <xsl:if test="(count($var_Source) = 0) or ($var_Source/../@type = 'note') or ($var_Source/@variant = 'text')">
            <text:bookmark-start text:name="iref_{$var_Iref}"/>
            <xsl:value-of select="."/>
            <text:bookmark-end text:name="iref_{$var_Iref}"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="s[@type='anchor'][@variant='empty']">
        <xsl:variable name="var_Iref" select="@iref"/>
        <text:bookmark text:name="iref_{$var_Iref}"/>
    </xsl:template>

    <xsl:template match="s[@type='cite']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Cite'"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="s[@type='quote']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Quote'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="s[@type='dfn']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Dfn'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="s[@type='samp']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Samp'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="s[@type='kbd']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Kbd'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="s[@type='code']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Code'"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="s[@type='var']">
        <xsl:apply-templates select="." mode="ficheblock-span-LinkWrap">
            <xsl:with-param name="param_Class" select="'Var'"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="s[@type='fiche']">
        <text:span text:style-name="FbeTxtFiche"><xsl:value-of select="."/></text:span>
    </xsl:template>
    
    <xsl:template match="s[@type='motcle']">
        <text:span text:style-name="FbeTxtMotcle">
        <xsl:choose>
            <xsl:when test="value"><xsl:value-of select="value"/></xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="motcle" mode="mode-Text"/>
            </xsl:otherwise>
        </xsl:choose>
        </text:span>
    </xsl:template>


<!--
********************************************************************************
* ficheblock-span-LinkWrap
********************************************************************************
-->

    <xsl:template match="*" mode="ficheblock-span-LinkWrap">
        <xsl:param name="param_Class"/>
        <xsl:choose>
            <xsl:when test="@ref = true()">
                <text:a xlink:type="simple" xlink:href="{@ref}"><text:span text:style-name="FbeTxt{$param_Class}A"><xsl:value-of select="."/></text:span></text:a>
            </xsl:when>
            <xsl:otherwise>
                <text:span text:style-name="FbeTxt{$param_Class}"><xsl:value-of select="."/></text:span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>


<!--
********************************************************************************
* paragraphs
********************************************************************************
-->

    <xsl:template match="p">
        <xsl:apply-templates select="." mode="ficheblock-paragraph-StyledP">
            <xsl:with-param name="param_Class" select="'FbeTxt'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="p[@type='question']">
        <xsl:apply-templates select="." mode="ficheblock-paragraph-StyledP">
            <xsl:with-param name="param_Class" select="'FbeTxtQuestion'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="p[@type='remarque']">
        <xsl:apply-templates select="." mode="ficheblock-paragraph-StyledP">
            <xsl:with-param name="param_Class" select="'FbeTxtRemark'"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="p[@type='note']">
        <xsl:variable name="var_Iref" select="s[@type='anchor'][1]/@iref"/>
        <xsl:variable name="var_Source" select="//s[@type='iref' and @iref=$var_Iref][1]"/>
        <xsl:if test="(count($var_Source) = 0) or ($var_Source/../@type = 'note') or ($var_Source/@variant = 'text')">
            <xsl:apply-templates select="." mode="ficheblock-paragraph-StyledP">
                <xsl:with-param name="param_Class" select="'FbeTxtNote'"/>
            </xsl:apply-templates>
        </xsl:if>
    </xsl:template>  

    <xsl:template match="p[@type='citation']">
        <text:p text:style-name="FbeTxtQuote"><xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/></text:p>

        <xsl:apply-templates select="following-sibling::*[1][@type='citation' and @source=false()]" mode="ficheblock-paragraph-NextQuote"/>

        <xsl:if test="string-length(@source) &gt;0">
            <text:p text:style-name="FbeTxtQuoteSource">
                <xsl:value-of select="@source"/>
            </text:p>
        </xsl:if>
    </xsl:template>

    <xsl:template match="p[@type='citation' and @source=false() and preceding-sibling::*[1]/@type='citation']">
    </xsl:template>

    <xsl:template match="h">
        <xsl:variable name="var_Start" select="number($STARTLEVEL)"/>
        <xsl:variable name="var_Level" select="$var_Start + number(@level) - 1"/>
        <xsl:choose>
            <xsl:when test="$var_Start &gt;0 and $var_Level &lt; 11">
                <text:h text:outline-level="{$var_Level}" text:style-name="Heading_20_{$var_Level}">
                    <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
                </text:h>
            </xsl:when>
            <xsl:otherwise>
                <text:p text:style-name="FbeTxt">
                    <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
                </text:p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="numero">
        <text:span text:style-name="FbeCptNumber">
            <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
            <xsl:if test="count(../legende) &gt; 0"><xsl:text> : </xsl:text></xsl:if>
        </text:span>
    </xsl:template>

    <xsl:template match="legende">
        <text:span text:style-name="FbeCptLegend">
            <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
        </text:span>
    </xsl:template>

    <xsl:template match="alt">
        <text:p text:style-name="FbeAlt">
            <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
        </text:p>
    </xsl:template>


<!--
********************************************************************************
* ficheblock-paragraph-                                                           
********************************************************************************
-->

    <xsl:template match="p" mode="ficheblock-paragraph-Div">
        <xsl:param name="param_Caption" select="false()"/>
        <xsl:variable name="PStyle">
            <xsl:text>FbeTxtDiv</xsl:text>
            <xsl:choose>
                <xsl:when test="position() = 1">1</xsl:when>
                <xsl:when test="($param_Caption = false()) and (position() = last())">L</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <text:p text:style-name="{$PStyle}"><xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/></text:p>
    </xsl:template>

    <xsl:template match="p" mode="ficheblock-paragraph-NextQuote">
        <text:p text:style-name="FbeTxtQuote"><xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/></text:p>
        <xsl:apply-templates select="following-sibling::*[1][@type='citation' and @source=false()]" mode="ficheblock-paragraph-NextQuote"/>
    </xsl:template>

    <xsl:template match="p" mode="ficheblock-paragraph-StyledP">
        <xsl:param name="param_Class"/>
        <text:p text:style-name="{$param_Class}">
            <xsl:apply-templates select="preceding-sibling::*[1]" mode="ficheblock-figure-Lettrine"/>
            <xsl:if test="string-length(@source) &gt;0">
                <text:span text:style-name="FbeTxtSource">
                    <xsl:value-of select="@source"/>
                    <xsl:text> : </xsl:text>
                </text:span>
            </xsl:if>
            <xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/>
        </text:p>
    </xsl:template>


<!--
********************************************************************************
* lists                                                             
********************************************************************************
-->

    <xsl:template match="ul">
        <xsl:param name="param_Depth" select="1"/>
        <xsl:variable name="StyleName">
            <xsl:text>FbeLst</xsl:text>
            <xsl:choose>
                <xsl:when test="@variant='ol'">Ol</xsl:when>
                <xsl:otherwise>Ul</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <text:list text:style-name="{$StyleName}">
            <xsl:apply-templates select='li'>
                <xsl:with-param name="param_Depth" select="$param_Depth + 1"/>
            </xsl:apply-templates>
        </text:list>
    </xsl:template>

    <xsl:template match="li">
        <xsl:param name="param_Depth" select="2"/>
        <text:list-item>
            <xsl:for-each select='*'>
                <xsl:apply-templates select='.' mode="ficheblock-list-Li">
                    <xsl:with-param name="param_Depth" select="$param_Depth"/>
                </xsl:apply-templates>
            </xsl:for-each>
        </text:list-item>
    </xsl:template>


<!--
********************************************************************************
* ficheblock-list-                                                           
********************************************************************************
-->

    <xsl:template match="p" mode="ficheblock-list-Li">
        <xsl:apply-templates select="." mode="ficheblock-paragraph-StyledP">
            <xsl:with-param name="param_Class" select="'FbeTxtLi'"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="ul" mode="ficheblock-list-Li">
        <xsl:apply-templates select="."/>
    </xsl:template>


<!--
********************************************************************************
* table                                                            
********************************************************************************
-->

    <xsl:template match="table">
        <xsl:variable name="var_Cols" select="@cols"/>
        <xsl:variable name="var_Caption" select="numero = true() or legende = true()"/>
        <xsl:variable name="var_Style">FbeTable<xsl:if test="$var_Caption = false()">Sans</xsl:if></xsl:variable>
        <xsl:if test="tr">
            <table:table table:name="Tableau{generate-id(.)}" table:style-name="{$var_Style}">
                <table:table-column table:number-columns-repeated="{$var_Cols}"/>
                <xsl:apply-templates select="tr"/>
            </table:table>
        </xsl:if>
        <xsl:apply-templates select="." mode="ficheblock-Caption"/>
    </xsl:template>

    <xsl:template match="tr">
        <table:table-row table:style-name="FbeTableRow">
            <xsl:apply-templates select="*"/>
        </table:table-row>
    </xsl:template>

    <xsl:template match="td">
        <xsl:variable name="PStyle">FbeTable<xsl:choose><xsl:when test="@type='number'">Number</xsl:when><xsl:when test="@type='header'">Header</xsl:when><xsl:otherwise>String</xsl:otherwise></xsl:choose></xsl:variable>
        <table:table-cell table:style-name="FbeTableCell" office:value-type="string">
            <xsl:if test="@att-colspan">
                <xsl:attribute name="table:number-columns-spanned"><xsl:value-of select="@att-colspan"/></xsl:attribute>
            </xsl:if>
            <xsl:if test="@att-rowspan">
                <xsl:attribute name="table:number-rows-spanned"><xsl:value-of select="@att-rowspan"/></xsl:attribute>
            </xsl:if>
            <text:p text:style-name="{$PStyle}"><xsl:apply-templates select="." mode="ficheblock-ParagraphContent"/></text:p>
        </table:table-cell>
    </xsl:template>


<!--
********************************************************************************
* ln                                                            
********************************************************************************
-->

    <xsl:template match="ln">
        <xsl:param name="param_Caption" select="false()"/>
        <xsl:variable name="PStyle">
            <xsl:text>FbeLn</xsl:text>
            <xsl:choose>
                <xsl:when test="position() = 1">1</xsl:when>
                <xsl:when test="($param_Caption = false()) and (position() = last())">L</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <text:p text:style-name="{$PStyle}"><xsl:if test="@indent &gt;0"><xsl:call-template name="ficheblock-indentLine">
                    <xsl:with-param name="param_String">&#xA0;&#xA0;&#xA0;</xsl:with-param>
                    <xsl:with-param name="param_Count" select="@indent"/>
                </xsl:call-template>
            </xsl:if>
        <xsl:value-of select="."/></text:p>
    </xsl:template>


<!--
********************************************************************************
* figures                                                           
********************************************************************************
-->

    <xsl:template match="div">
        <xsl:variable name="var_Caption" select="numero = true() or legende = true()"/>
        <xsl:apply-templates select="fbl/p" mode="ficheblock-paragraph-Div">
            <xsl:with-param name="param_Caption" select="$var_Caption"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="." mode="ficheblock-Caption"/>
    </xsl:template>

    <xsl:template match="cdatadiv">
        <xsl:apply-templates select="." mode="ficheblock-Caption"/>
    </xsl:template>

    <xsl:template match="code">
        <xsl:variable name="var_Caption" select="numero = true() or legende = true()"/>
        <xsl:apply-templates select="ln">
            <xsl:with-param name="param_Caption" select="$var_Caption"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="." mode="ficheblock-Caption"/>
    </xsl:template>

    <xsl:template match="insert">
        <xsl:apply-templates select="alt"/>
        <xsl:apply-templates select="." mode="ficheblock-Caption"/>
    </xsl:template>
    
    <xsl:template match="insert[@type='image'][@album-width]">
        <xsl:variable name="var_Width" select="@album-width div 37.8"/>
        <xsl:variable name="var_Height" select="@album-height div 37.8"/>
        <text:p text:style-name="FbeInsertImage"><draw:frame draw:name="{generate-id(.)}" draw:style-name="FbeIllustration" text:anchor-type="as-char" svg:width="{$var_Width}cm" svg:height="{$var_Height}cm" draw:z-index="0">
            <draw:image xlink:href="bdf://this/illustrations/{@album}-{@id}" xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad"/>
        </draw:frame></text:p>
        <xsl:apply-templates select="." mode="ficheblock-Caption"/>
    </xsl:template>
    
    <xsl:template match="insert[@type='lettrine'][@album-width]">
        <xsl:choose>
            <xsl:when test="following-sibling::*[1][name(.) = 'p']">lkjl d</xsl:when>
            <xsl:otherwise>
                <text:p><xsl:apply-templates select="." mode="ficheblock-figure-Lettrine"/></text:p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
<!--
********************************************************************************
* ficheblock-figure-                                                        
********************************************************************************
-->

    <xsl:template match="*" mode="ficheblock-figure-Lettrine"><!-- Do nothing --></xsl:template>
    
    <xsl:template match="insert[@type='lettrine'][@album-width]" mode="ficheblock-figure-Lettrine">
        <xsl:variable name="var_FrameStyle">
            <xsl:choose>
                <xsl:when test="@position = 'left'">FbeLettrineLeft</xsl:when>
                <xsl:otherwise>FbeLettrineRight</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="var_Width" select="@album-width div 37.8"/>
        <xsl:variable name="var_Height" select="@album-height div 37.8"/>
        <xsl:variable name="var_FrameWidth" select="$var_Width + 1"/>
        <draw:frame draw:style-name="{$var_FrameStyle}" draw:name="{generate-id(.)}_Frame" text:anchor-type="paragraph" svg:width="{$var_FrameWidth}cm" draw:z-index="0"><draw:text-box>
        <text:p text:style-name="FbeInsertLettrine"><draw:frame draw:name="{generate-id(.)}_Image" draw:style-name="FbeIllustration" text:anchor-type="as-char" svg:width="{$var_Width}cm" svg:height="{$var_Height}cm" draw:z-index="0">
            <draw:image xlink:href="bdf://this/illustrations/{@album}-{@id}" xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad"/>
        </draw:frame></text:p>
        <xsl:apply-templates select="." mode="ficheblock-Caption"/>
        </draw:text-box></draw:frame>
    </xsl:template>


<!--
********************************************************************************
* ficheblock- functions                                                     
********************************************************************************
-->

    <xsl:template name="ficheblock-indentLine">
        <xsl:param name="param_String"/>
        <xsl:param name="param_Count" select="1"/>
        <xsl:choose>
            <xsl:when test="not($param_Count) or not($param_String)"/>
            <xsl:when test="$param_Count = 1">
                <xsl:value-of select="$param_String"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:if test="$param_Count mod 2"><xsl:value-of select="$param_String"/></xsl:if>
                <xsl:call-template name="ficheblock-indentLine">
                    <xsl:with-param name="param_String" select="concat($param_String,$param_String)"/>
                    <xsl:with-param name="param_Count" select="floor($param_Count div 2)"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
