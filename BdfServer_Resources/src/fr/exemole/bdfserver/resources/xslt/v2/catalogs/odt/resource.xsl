<?xml version='1.0' encoding='UTF-8'?>
<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform' xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"  xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0">

     
<!--
********************************************************************************
* resource-
********************************************************************************
-->

<xsl:template match="/" mode="resource-Logo">
    <xsl:if test="$WITH_RESOURCELOGO = 'yes'">
        <xsl:variable name="Logo" select="document('bdf://this/dyn/logos/odt.xml')/resource"/>
        <xsl:if test="$Logo/@path">
            <text:p text:style-name="{$Logo/param[@name='style']}">
                <draw:frame draw:style-name="FicheLogo" text:anchor-type="{$Logo/param[@name='anchor']}" svg:width="{$Logo/param[@name='width']}" svg:height="{$Logo/param[@name='height']}" draw:z-index="0">
                    <draw:image xlink:href="bdf://this/{$Logo/@path}" xlink:type="simple" xlink:show="embed" xlink:actuate="onLoad"/>
                </draw:frame>
            </text:p>
        </xsl:if>
    </xsl:if>
</xsl:template>

</xsl:stylesheet>
