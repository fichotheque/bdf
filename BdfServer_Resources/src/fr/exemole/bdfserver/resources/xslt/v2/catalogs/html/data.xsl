<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="data[@type='fiches']" mode="data-Listitem">
        <xsl:apply-templates select="fiche" mode="include-fiche-Listitem" />
    </xsl:template>
    
    <xsl:template match="data[@type='error']" mode="data-Listitem">
        <li>
            <xsl:value-of select="error"/>
        </li>
    </xsl:template>

</xsl:stylesheet>
