<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template name="url-getFicheHref">
        <xsl:param name="param_Corpus"/>
        <xsl:param name="param_Id"/>
        <xsl:param name="param_Idalpha"/>
        <xsl:param name="param_Fragment" select="''"/>
        <xsl:param name="param_Fichotheque" select="''"/>

        <xsl:value-of select="$BDF_FICHOTHEQUEPATH"/>
        <xsl:if test="string-length($param_Fichotheque) &gt; 0">
            <xsl:text>../</xsl:text>
            <xsl:value-of select="$param_Fichotheque"/>
            <xsl:text>/</xsl:text>
        </xsl:if>
        <xsl:text>fiches/</xsl:text>
        <xsl:value-of select="$param_Corpus"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$param_Id"/>
        <xsl:text>.html</xsl:text>
        <xsl:if test="string-length($param_Fragment) &gt; 0">
                <xsl:text>#</xsl:text>
                <xsl:value-of select="$param_Fragment"/>
        </xsl:if>
    </xsl:template>

    <xsl:template name="url-getMotcleHref">
        <xsl:param name="param_Thesaurus"/>
        <xsl:param name="param_Id"/>
        <xsl:param name="param_Idalpha"/>
        <xsl:param name="param_Fragment" select="''"/>

        <xsl:text>#</xsl:text>
        <xsl:value-of select="$param_Fragment"/>
    </xsl:template>

    <xsl:template name="url-getIllustrationHref">
        <xsl:param name="param_Album"/>
        <xsl:param name="param_Id"/>
        <xsl:param name="param_Format"/>
        <xsl:param name="param_Dim" select="''"/>

        <xsl:value-of select="$BDF_FICHOTHEQUEPATH"/>
        <xsl:text>illustrations/</xsl:text>
        <xsl:if test="string-length($param_Dim) &gt; 0">
            <xsl:value-of select="$param_Dim"/>
            <xsl:text>/</xsl:text>
        </xsl:if>
        <xsl:value-of select="$param_Album"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$param_Id"/>
        <xsl:text>.</xsl:text>
        <xsl:value-of select="$param_Format"/>
    </xsl:template>

    <xsl:template name="url-getDocumentHref">
        <xsl:param name="param_Addenda"/>
        <xsl:param name="param_Id"/>
        <xsl:param name="param_Basename"/>
        <xsl:param name="param_Extension"/>

        <xsl:value-of select="$BDF_FICHOTHEQUEPATH"/>
        <xsl:text>documents/</xsl:text>
        <xsl:value-of select="$param_Addenda"/>
        <xsl:text>/</xsl:text>
        <xsl:value-of select="$param_Basename"/>
        <xsl:text>.</xsl:text>
        <xsl:value-of select="$param_Extension"/>
    </xsl:template>

</xsl:stylesheet>