<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0">

    <xsl:template match="data[@type='fiches']" mode="data-Listitem">
        <xsl:apply-templates select="fiche" mode="include-fiche-Listitem" />
    </xsl:template>
    
    <xsl:template match="data[@type='error']" mode="data-Listitem">
        <text:list-item>
            <text:p text:style-name="FicheFieldLi">
                <xsl:value-of select="error"/>
            </text:p>
        </text:list-item>
    </xsl:template>

</xsl:stylesheet>
