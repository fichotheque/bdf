<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="../url.xsl"/>
    <xsl:import href="ficheitem.xsl"/>
    <xsl:import href="ficheblock.xsl"/>
    <xsl:import href="include.xsl"/>
    <xsl:import href="motcle.xsl"/>
    <xsl:import href="script.xsl"/>
    <xsl:import href="component.xsl"/>
    <xsl:import href="data.xsl"/>
    
    <xsl:param name="BDF_FICHOTHEQUEPATH" select="''"/>
    <xsl:param name="BDF_VERSION" select="''"/>
    <xsl:param name="EXTERNALTARGET" select="''"/>
    <xsl:param name="INCLUDESCRIPTS" select="''"/>
    <xsl:param name="SHOWEMPTY" select="1"/>
    <xsl:param name="STARTLEVEL" select="0"/>
    <xsl:param name="WORKINGLANG" select="'fr'"/>
    
</xsl:stylesheet>
