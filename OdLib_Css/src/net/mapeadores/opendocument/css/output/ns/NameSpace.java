/* OdLib_Css- Copyright (c) 2008 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.css.output.ns;


/**
 *
 * @author Vincent Calame
 */
public class NameSpace {

    private String prefix = "";
    private String name;

    public NameSpace() {
    }

    public NameSpace(String prefix, String name) {
        setPrefix(prefix);
        this.name = name;
    }

    /**
     * N'est jamais null, la longueur nulle indique l'espace par défaut.
     */
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        if (prefix == null) {
            this.prefix = "";
        } else {
            this.prefix = prefix;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
