/* OdLib_Css- Copyright (c) 2008 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.css;

import net.mapeadores.opendocument.css.extraction.CssExtraction;
import java.io.File;


/**
 *
 *
 * @author Vincent Calame
 */
public class Main {

    private Main() {
    }

    public static void main(String[] args) throws Exception {
        System.setProperty("org.w3c.css.sac.parser", "com.steadystate.css.parser.SACParserCSS2");
        int length = args.length;
        if (length == 0) {
            missingArgument();
            return;
        }
        String options = args[0];

        if (options.startsWith("-")) {
            if (length == 1) {
                missingArgument();
                return;
            }
            options = options.substring(1);
            String[] temp = new String[length - 1];
            System.arraycopy(args, 1, temp, 0, (length - 1));
            args = temp;
        } else {
            options = "";
        }
        if (options.indexOf('e') != -1) {
            extract(options, args);
        }
    }

    private static void extract(String options, String[] args) throws Exception {
        boolean force = (options.indexOf('f') != -1);
        boolean withCss = (options.indexOf('c') != -1);
        boolean withXml = (options.indexOf('x') != -1);
        Arguments arguments = new Arguments();
        arguments.parse(args, 1);
        String originePath = args[0];
        File origine = new File(originePath);
        if (!origine.exists()) {
            System.out.println("Fichier inexistant : " + originePath);
            return;
        }
        if (origine.isDirectory()) {
            System.out.println("C'est un répertoire : " + originePath);
            return;
        }
        File cssDestination = null;
        File xmlDestination = null;
        if (withCss) {
            if (arguments.cssPath != null) {
                cssDestination = new File(arguments.cssPath);
            } else {
                cssDestination = new File(origine.getParentFile(), origine.getName() + ".css");
            }
            if (!testFile(cssDestination, force)) {
                return;
            }
        }
        if (withXml) {
            if (arguments.xmlPath != null) {
                xmlDestination = new File(arguments.xmlPath);
            } else {
                xmlDestination = new File(origine.getParentFile(), origine.getName() + ".styles.xml");
            }
            if (!testFile(xmlDestination, force)) {
                return;
            }
        }
        CssExtraction.extract(origine, cssDestination, xmlDestination);
    }

    private static boolean testFile(File f, boolean force) {
        if (f == null) {
            return true;
        }
        if (f.exists()) {
            if (f.isDirectory()) {
                System.out.println("C'est un répertoire : " + f.getPath());
                return false;
            } else {
                if (force) {
                    return true;
                } else {
                    System.out.println("Existe déjà : " + f.getPath());
                    return false;
                }
            }
        } else {
            return true;
        }
    }

    private static void missingArgument() {
        System.out.println("Nombre d'arguments insuffisants");
    }


    private static class Arguments {

        String cssPath;
        String xmlPath;

        Arguments() {
        }

        void parse(String[] args, int start) {
            int length = args.length - 1;
            for (int i = start; i < length; i++) {
                String arg = args[i];
                if (arg.equals("-C")) {
                    String arg2 = args[i + 1];
                    if (!arg2.startsWith("-")) {
                        cssPath = arg2;
                        i = i + 1;
                    }
                } else if (arg.equals("-X")) {
                    String arg2 = args[i + 1];
                    if (!arg2.startsWith("-")) {
                        xmlPath = arg2;
                        i = i + 1;
                    }
                }
            }
        }

    }

}
