/* OdLib_Css - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.css.parse;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


/**
 *
 * @author Vincent Calame
 */
public class DistCssSource implements CssSource {

    private final Class referenceClass;
    private final String path;

    public DistCssSource(Class referenceClass, String path) {
        if (referenceClass.getResource(path) == null) {
            throw new IllegalArgumentException("path does not exist: " + path);
        }
        this.referenceClass = referenceClass;
        this.path = path;
    }

    @Override
    public Reader getReader() throws IOException {
        InputStream inputStream = referenceClass.getResourceAsStream(path);
        return new InputStreamReader(inputStream, "UTF-8");
    }

    @Override
    public CssSource getImportCssSource(String url) {
        if (referenceClass.getResource(url) != null) {
            return new DistCssSource(referenceClass, url);
        } else {
            return null;
        }
    }

    @Override
    public String getCssSourceURI() {
        return referenceClass.getPackage().getName() + "/" + path;
    }

}
