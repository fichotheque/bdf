/* OdLib_Css - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.css.parse;

import net.mapeadores.opendocument.elements.OdLog;
import org.w3c.css.sac.CSSException;
import org.w3c.css.sac.CSSParseException;


/**
 *
 * @author Vincent Calame
 */
public class LogCssErrorHandler extends OdLog implements CssErrorHandler {

    private String currentURI;

    public LogCssErrorHandler() {
    }

    @Override
    public void setCurrentURI(String uri) {
        this.currentURI = uri;
    }

    @Override
    public void error(CSSParseException exception) throws CSSException {
        logItemList.add(new LogItem(currentURI, "css error: " + exception.getMessage()));
    }

    @Override
    public void fatalError(CSSParseException exception) throws CSSException {
        fatalErrorLogItem = new LogItem(currentURI, exception.getMessage());
    }

    @Override
    public void warning(CSSParseException exception) throws CSSException {
        logItemList.add(new LogItem(currentURI, "css warning: " + exception.getMessage()));
    }

    @Override
    public void unknownURIFatalError(String uri) {
        fatalErrorLogItem = new LogItem(uri, "Unknown URI");
    }

}
