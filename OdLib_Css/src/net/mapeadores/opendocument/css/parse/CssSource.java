/* OdLib_Css- Copyright (c) 2008-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.css.parse;

import java.io.IOException;
import java.io.Reader;


/**
 *
 * @author Vincent Calame
 */
public interface CssSource {

    public Reader getReader() throws IOException;

    public CssSource getImportCssSource(String url);

    public String getCssSourceURI();

}
