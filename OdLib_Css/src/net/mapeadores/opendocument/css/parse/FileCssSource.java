/* OdLib_Css- Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.css.parse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


/**
 *
 * @author Vincent Calame
 */
public class FileCssSource implements CssSource {

    private final File file;
    private final File directoryFile;

    public FileCssSource(File file) {
        this.file = file;
        directoryFile = file.getParentFile();
    }

    @Override
    public Reader getReader() throws IOException {
        InputStream inputStream = new FileInputStream(file);
        Reader reader = new InputStreamReader(inputStream, "UTF-8");
        return reader;
    }

    @Override
    public CssSource getImportCssSource(String url) {
        File newFile = getFile(url);
        if (!newFile.exists()) {
            return null;
        }
        return new FileCssSource(newFile);
    }

    public File getFile(String url) {
        return new File(directoryFile, url);
    }

    @Override
    public String getCssSourceURI() {
        return file.toURI().toString();
    }

}
