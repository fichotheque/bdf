/* OdLib_Css- Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.css.parse;

import org.w3c.css.sac.ErrorHandler;


/**
 *
 * @author Vincent Calame
 */
public interface CssErrorHandler extends ErrorHandler {

    public void setCurrentURI(String uri);

    public void unknownURIFatalError(String uri);

}
