<?xml version="1.0" encoding="UTF-8" ?>


<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:dom="http://www.w3.org/2001/xml-events">

    <xsl:output method="text"/>

    <xsl:template match="/">
        <xsl:apply-templates select="/office:document-styles/office:styles/style:style"/>
    </xsl:template>

    <xsl:template match="style:style">
        <xsl:apply-templates select="." mode="Selector"/>
        <xsl:text> {&#x0A;</xsl:text>
        <xsl:apply-templates select="@*" mode="Property"/>
        <xsl:apply-templates select="*/@*" mode="Property"/>
        <xsl:text>}&#x0A;&#x0A;</xsl:text>
    </xsl:template>


    <!-- Sélecteur -->

    <xsl:template match="style:style[@style:family='paragraph']" mode="Selector">
        <xsl:text>p</xsl:text>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>

    <xsl:template match="style:style[@style:family='paragraph' and @style:name='Heading']" mode="Selector">
        <xsl:text>h</xsl:text>
    </xsl:template>

    <xsl:template match="style:style[@style:family='paragraph' and starts-with(@style:name,'Heading_20_')]" mode="Selector">
        <xsl:text>h</xsl:text><xsl:value-of select="substring(@style:name,12)"/>
    </xsl:template>

    <xsl:template match="style:style[@style:family='text']" mode="Selector">
        <xsl:text>span</xsl:text>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>

    <xsl:template match="style:style[@style:family='table']" mode="Selector">
        <xsl:text>table</xsl:text>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>

    <xsl:template match="style:style[@style:family='table-cell']" mode="Selector">
        <xsl:text>column</xsl:text>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>

    <xsl:template match="style:style[@style:family='table-row']" mode="Selector">
        <xsl:text>td</xsl:text>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>

    <xsl:template match="style:style[@style:family='table-column']" mode="Selector">
        <xsl:text>tr</xsl:text>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>

    <xsl:template match="style:style[@style:family='graphic']" mode="Selector">
        <xsl:text>graphic</xsl:text>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>


    <xsl:template match="style:style" mode="Selector">
        <xsl:text>Unknwon = </xsl:text><xsl:value-of select="@style:family"/>
        <xsl:apply-templates select="." mode="Class"/>
    </xsl:template>


    <!-- Classes -->

    <xsl:template match="style:style" mode="Class">
        <xsl:text>.</xsl:text><xsl:call-template name="Replace"><xsl:with-param name="String" select="@style:name"/></xsl:call-template>
    </xsl:template>


    <!-- Propriétés -->

    <xsl:template match="@style:name" mode="Property">

    </xsl:template>

    <xsl:template match="@style:family" mode="Property">

    </xsl:template>

    <xsl:template match="@style:display-name" mode="Property">
        <xsl:text>display-name: &quot;</xsl:text><xsl:value-of select="."/><xsl:text>&quot;;&#x0A;</xsl:text>
    </xsl:template>

    <xsl:template match="@style:parent-style-name | @style:list-style-name | @style:next-style-name | @style:master-page-name" mode="Property">
        <xsl:value-of select="local-name(.)"/><xsl:text>: </xsl:text><xsl:apply-templates select="." mode="Replace"/><xsl:text>;&#x0A;</xsl:text>
    </xsl:template>

    <xsl:template match="@*[starts-with(local-name(), 'font-name')]" mode="Property">
        <xsl:value-of select="local-name(.)"/><xsl:text>: &quot;</xsl:text><xsl:value-of select="."/><xsl:text>&quot;;&#x0A;</xsl:text>
    </xsl:template>

    <xsl:template match="@*" mode="Property">
        <xsl:value-of select="local-name(.)"/><xsl:text>: </xsl:text><xsl:value-of select="."/><xsl:text>;&#x0A;</xsl:text>
    </xsl:template>


    <!-- Remplacement de l'espace (_20_) par le trait de soulignement(_) -->

    <xsl:template match="@*" mode="Replace">
        <xsl:variable name="String" select="string(.)"/>
        <xsl:choose>
            <xsl:when test="$String = 'Heading'">
                <xsl:text>h</xsl:text>
            </xsl:when>
            <xsl:when test="starts-with($String,'Heading_20_')">
                <xsl:text>h</xsl:text><xsl:value-of select="substring($String,12)"/>
            </xsl:when>
            <xsl:otherwise><xsl:call-template name="Replace"><xsl:with-param name="String" select="$String"/></xsl:call-template></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="Replace">
        <xsl:param name="String"/>
        <xsl:choose>
            <xsl:when test="contains($String,'_20_')">
                <xsl:value-of select="substring-before($String,'_20_')"/>
                <xsl:text>_</xsl:text>
                <xsl:call-template name="Replace">
                    <xsl:with-param name="String" select="substring-after($String,'_20_')"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="$String"/></xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>