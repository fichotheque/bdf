/* FichothequeLib_API - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;

import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;


/**
 *
 * @author Vincent Calame
 */
public interface Metadata {

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public Phrases getPhrases();


}
