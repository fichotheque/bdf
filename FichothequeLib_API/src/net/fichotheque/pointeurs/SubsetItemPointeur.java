/* FichothequeLib_API - Copyright (c) 2008-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.pointeurs;

import java.util.Collection;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.include.IncludeMode;
import net.fichotheque.utils.CroisementUtils;


/**
 *
 * @author Vincent Calame
 */
public interface SubsetItemPointeur {

    /**
     * @throw IllegalArgumentException
     */
    public SubsetItemPointeur getParentagePointeur(SubsetKey parentageSubsetKey);

    public SubsetItemPointeur getAssociatedPointeur(Subset subset);

    public void setCurrentSubsetItem(int id);

    public void setCurrentSubsetItem(SubsetItem subsetItem);

    public SubsetItem getCurrentSubsetItem();

    public Subset getSubset();

    /**
     * Liste des croisements de l'élément en cours avec une collection. Cette
     * liste peut être synchronisée, sans que cela soit obligatoire, il est
     * préférable de ne pas faire de modification sur les croisements lorsqu'on
     * fait une itération dessus.
     */
    public Croisements getCroisements(Subset subset);

    public Object getCurrentObject(String objectName);

    /**
     * Associe un objet à l'item courant. Les objets associés à un item sont «
     * vidés » quand l'item change.
     *
     * @param objectName
     * @param object
     */
    public void putCurrentObject(String objectName, Object object);

    public Object getPointeurObject(String objectName);

    /**
     * Associe un objet à l'instance. Les objets associés de cette manière le
     * demeure quelque soit les changements faits sur l'instance.
     *
     * @param objectName
     * @param object
     */
    public void putPointeurObject(String objectName, Object object);

    public default Collection<Liaison> getStandardLiaisons(Subset subset) {
        return CroisementUtils.filter(getCroisements(subset), "");
    }

    public default Collection<Liaison> getLiaisons(Subset subset, IncludeMode includeMode) {
        return CroisementUtils.filter(getCroisements(subset), includeMode);
    }

    public default Fichotheque getFichotheque() {
        return getSubset().getFichotheque();
    }

    public default SubsetKey getSubsetKey() {
        return getSubset().getSubsetKey();
    }

    public default boolean isEmpty() {
        return (getCurrentSubsetItem() == null);
    }

}
