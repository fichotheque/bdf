/* FichothequeLib_API - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.pointeurs;

import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.corpus.metadata.SubfieldKey;


/**
 *
 * @author Vincent Calame
 */
public interface FichePointeur extends SubsetItemPointeur {

    public boolean isWithSection();

    public void enableCache(boolean enable);

    /**
     * Valeurs possibles : String (si titre non vide), FicheItem, FicheItems
     * (avec au moins un élément), FicheBlocks (avec au moins un élément),
     * Integer (si ID). Si LANG, c'est un ficheItem de type Langue
     *
     */
    public Object getValue(FieldKey fieldKey);

    public default Corpus getCorpus() {
        return (Corpus) getSubset();
    }

    public default FicheMeta getCurrentFicheMeta() {
        return (FicheMeta) getCurrentSubsetItem();
    }

    public default Object getValue(CorpusField corpusField) {
        return getValue(corpusField.getFieldKey());
    }

    public default Object getValue(SubfieldKey subfieldKey) {
        return getValue(subfieldKey.getFieldKey());
    }

    public default FichePointeur setCurrentFicheMeta(FicheMeta ficheMeta) {
        setCurrentSubsetItem(ficheMeta);
        return this;
    }

}
