/* FichothequeLib_API - Copyright (c) 2010-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.pointeurs;

import net.fichotheque.Subset;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.thesaurus.metadata.ThesaurusFieldKey;


/**
 *
 * @author Vincent Calame
 */
public interface MotclePointeur extends SubsetItemPointeur {

    public Object getFieldValue(ThesaurusFieldKey thesaurusFieldKey);

    public Croisements getDescendantAxisCroisementList(Subset subset);

}
