/* FichothequeLib_API - Copyright (c) 2006-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.metadata;

import net.fichotheque.Fichotheque;
import net.fichotheque.Metadata;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeMetadata extends Metadata {

    /**
     * ne peut pas être null
     */
    public String getAuthority();

    /**
     * Ne peut pas être nul et doit être un nom technique
     * (net.mapeadores.util.text.checkTechnicalName() ne doit pas retourne
     * d'exception), le trait de soulignement étant autorisé.
     */
    public String getBaseName();

    public Fichotheque getFichotheque();

}
