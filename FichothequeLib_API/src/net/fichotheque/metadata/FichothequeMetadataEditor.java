/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.metadata;

import net.fichotheque.FichothequeEditor;
import net.fichotheque.MetadataEditor;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeMetadataEditor extends MetadataEditor {

    public FichothequeEditor getFichothequeEditor();

    /**
     *
     * @throws IllegalArgumentException si authority est incorrect
     */
    public boolean setAuthority(String authority);

    /**
     *
     * @throws IllegalArgumentException si baseName est incorrect
     */
    public boolean setBaseName(String baseName);

    @Override
    public default boolean removeAttribute(AttributeKey attributeKey) {
        return getFichothequeEditor().removeAttribute(getMetadata(), attributeKey);
    }

    @Override
    public default boolean putAttribute(Attribute attribute) {
        return getFichothequeEditor().putAttribute(getMetadata(), attribute);
    }

}
