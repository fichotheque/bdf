/* FichothequeLib_API - Copyright (c) 2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;


/**
 *
 * @author Vincent Calame
 */
public interface EditOrigin {

    public String getSessionId();

    public String getRedacteurGlobalId();

    public String getISOTime();

    public String getSource();

}
