/* FichothequeLib_API - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.history;

import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public interface FicheHistory {

    public SubsetKey getCorpusKey();

    public int getId();

    public HistoryUnit getFicheUnit();

    public HistoryUnit getAttributesUnit();

    public default boolean isDeleted() {
        return getFicheUnit().isDeleted();
    }

}
