/* FichothequeLib_API - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.history;

import net.fichotheque.croisement.CroisementKey;


/**
 *
 * @author Vincent Calame
 */
public interface CroisementHistory {

    public CroisementKey getCroisementKey();

    public HistoryUnit getCroisementUnit();

    public default boolean isDeleted() {
        return getCroisementUnit().isDeleted();
    }

}
