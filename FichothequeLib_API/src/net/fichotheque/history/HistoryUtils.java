/* FichothequeLib_API - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.history;

import java.util.Collections;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.croisement.CroisementKey;


/**
 *
 * @author Vincent Calame
 */
public final class HistoryUtils {

    public final static List<FicheHistory> EMPTY_FICHEHISTORYLIST = Collections.emptyList();
    public final static List<CroisementHistory> EMPTY_CROISEMENTHISTORYLIST = Collections.emptyList();

    private HistoryUtils() {

    }

    public static FicheHistory toFicheHistory(SubsetKey corpusKey, int id, HistoryUnit ficheUnit, HistoryUnit attributesUnit) {
        return new InternalFicheHistory(corpusKey, id, ficheUnit, attributesUnit);
    }

    public static CroisementHistory toCroisementHistory(CroisementKey croisementKey, HistoryUnit croisementUnit) {
        return new InternalCroisementHistory(croisementKey, croisementUnit);
    }


    private static class InternalFicheHistory implements FicheHistory {

        private final SubsetKey corpusKey;
        private final int id;
        private final HistoryUnit ficheUnit;
        private final HistoryUnit attributesUnit;

        private InternalFicheHistory(SubsetKey corpusKey, int id, HistoryUnit ficheUnit, HistoryUnit attributesUnit) {
            this.corpusKey = corpusKey;
            this.id = id;
            this.ficheUnit = ficheUnit;
            this.attributesUnit = attributesUnit;
        }

        @Override
        public SubsetKey getCorpusKey() {
            return corpusKey;
        }

        @Override
        public int getId() {
            return id;
        }

        @Override
        public HistoryUnit getFicheUnit() {
            return ficheUnit;
        }

        @Override
        public HistoryUnit getAttributesUnit() {
            return attributesUnit;
        }


    }


    private static class InternalCroisementHistory implements CroisementHistory {

        private final CroisementKey croisementKey;
        private final HistoryUnit croisementUnit;

        private InternalCroisementHistory(CroisementKey croisementKey, HistoryUnit croisementUnit) {
            this.croisementKey = croisementKey;
            this.croisementUnit = croisementUnit;
        }

        @Override
        public CroisementKey getCroisementKey() {
            return croisementKey;
        }

        @Override
        public HistoryUnit getCroisementUnit() {
            return croisementUnit;
        }

    }

}
