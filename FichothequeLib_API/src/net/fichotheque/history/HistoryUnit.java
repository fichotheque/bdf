/* FichothequeLib_API - Copyright (c) 2017-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.history;

import java.util.List;
import net.fichotheque.EditOrigin;


/**
 *
 * @author Vincent Calame
 */
public interface HistoryUnit {

    public final static String CURRENT_REVISION = "current";
    public final static String PENULTIMATE_REVISION = "penultimate";

    public boolean isDeleted();

    public List<Revision> getRevisionList();

    public default boolean isEmpty() {
        return getRevisionList().isEmpty();
    }

    public default HistoryUnit.Revision getMostRecentRevision() {
        List<Revision> list = getRevisionList();
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public default EditOrigin getMostRecentEditOrigin() {
        HistoryUnit.Revision revision = getMostRecentRevision();
        if (revision == null) {
            return null;
        }
        return revision.getEditOriginList().get(0);
    }


    public interface Revision {

        public String getName();

        public List<EditOrigin> getEditOriginList();

    }

}
