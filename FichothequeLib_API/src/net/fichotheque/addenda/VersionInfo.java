/* FichothequeLib_API - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.addenda;


/**
 *
 * @author Vincent Calame
 */
public class VersionInfo {

    private final String extension;
    private final long length;
    private final String md5Checksum;

    public VersionInfo(String extension, long length, String md5Checksum) {
        this.extension = extension;
        this.length = length;
        this.md5Checksum = md5Checksum;
    }

    public String getExtension() {
        return extension;
    }

    public long getLength() {
        return length;
    }

    public String getMd5Checksum() {
        return md5Checksum;
    }

}
