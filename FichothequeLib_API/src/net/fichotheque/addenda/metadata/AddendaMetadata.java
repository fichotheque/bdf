/* FichothequeLib_API - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.addenda.metadata;

import net.fichotheque.Metadata;
import net.fichotheque.addenda.Addenda;


/**
 *
 * @author Vincent Calame
 */
public interface AddendaMetadata extends Metadata {

    public Addenda getAddenda();

}
