/* FichothequeLib_API - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.addenda.metadata;

import net.fichotheque.MetadataEditor;
import net.fichotheque.addenda.AddendaEditor;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;


/**
 *
 * @author Vincent Calame
 */
public interface AddendaMetadataEditor extends MetadataEditor {

    public AddendaEditor getAddendaEditor();

    public AddendaMetadata getAddendaMetadata();

    @Override
    public default boolean removeAttribute(AttributeKey attributeKey) {
        return getAddendaEditor().getFichothequeEditor().removeAttribute(getMetadata(), attributeKey);
    }

    @Override
    public default boolean putAttribute(Attribute attribute) {
        return getAddendaEditor().getFichothequeEditor().putAttribute(getMetadata(), attribute);
    }

}
