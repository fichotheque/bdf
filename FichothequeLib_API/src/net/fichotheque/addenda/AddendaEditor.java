/* FichothequeLib_API - Copyright (c) 2006-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.addenda;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Collection;
import net.fichotheque.ExistingIdException;
import net.fichotheque.ExistingNameException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.addenda.metadata.AddendaMetadataEditor;


/**
 *
 * @author Vincent Calame
 */
public interface AddendaEditor {

    public Addenda getAddenda();

    public FichothequeEditor getFichothequeEditor();

    public AddendaMetadataEditor getAddendaMetadataEditor();

    /**
     * Retourne true si le document est supprimé, false sinon. Un document ne
     * peut pas être supprimé s'il est en lien avec des fiches.
     */
    public boolean removeDocument(Document document);

    /**
     * Retourne true si la version du document est supprimée, false sinon. Une
     * version d'un document ne peut pas être supprimée si c'est la dernière
     * version du document.
     */
    public boolean removeVersion(Document document, String extension);

    public Document createDocument(int id, Collection<VersionInfo> versionInfos) throws ExistingIdException;

    /**
     *
     * @throws IllegalArgumentException si newBasename est vide ou nul
     * @throws ParseException si newBasename comprend des caractères incorrects.
     */
    public boolean setBasename(Document document, String newBasename) throws ExistingNameException, ParseException;

    public void saveVersion(Document document, String extension, InputStream inputStream) throws ParseException, IOException;

}
