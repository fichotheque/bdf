/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.addenda;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import net.mapeadores.util.primitives.FileLength;


/**
 *
 * @author Vincent Calame
 */
public interface Version {

    public Document getDocument();

    public FileLength getFileLength();

    public String getExtension();

    public String getMd5Checksum();

    public InputStream getInputStream();

    public void linkTo(Path destination, boolean symbolicLink) throws IOException;

    public default String getFileName() {
        return getDocument().getBasename() + "." + getExtension();
    }

}
