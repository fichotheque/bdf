/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.addenda;

import java.util.List;
import net.fichotheque.SubsetItem;


/**
 *
 * @author Vincent Calame
 */
public interface Document extends SubsetItem {

    public String getBasename();

    public List<Version> getVersionList();

    public Version getVersionByExtension(String extension);

    public default Addenda getAddenda() {
        return (Addenda) getSubset();
    }

}
