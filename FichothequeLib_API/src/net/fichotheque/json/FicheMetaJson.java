/* FichothequeLib_API - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.table.Cell;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.PropertyEligibility;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class FicheMetaJson {

    private FicheMetaJson() {

    }

    public static void properties(JSONWriter jw, FicheMeta ficheMeta, PropertyEligibility propertyEligibility, @Nullable Cell[] propertiesCellArray) throws IOException {
        if (propertyEligibility.includeProperty("corpus")) {
            jw.key("corpus")
                    .value(ficheMeta.getSubsetName());
        }
        jw.key("id")
                .value(ficheMeta.getId());
        if (propertyEligibility.includeProperty("lang")) {
            Lang lang = ficheMeta.getLang();
            if (lang != null) {
                jw.key("lang")
                        .value(lang.toString());
            }
        }
        if (propertyEligibility.includeProperty("titre")) {
            jw.key("titre")
                    .value(ficheMeta.getTitre());
        }
        if (propertyEligibility.includeProperty("title")) {
            jw.key("title")
                    .value(ficheMeta.getTitre());
        }
        if (propertyEligibility.includeProperty("properties")) {
            CellJson.cellArrayMappingProperty(jw, propertiesCellArray);
        }
    }

}
