/* FichothequeLib_API - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.PropertyEligibility;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.ListLangContext;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public final class MotcleJson {

    private MotcleJson() {

    }

    public static void properties(JSONWriter jw, Motcle motcle, LangContext langContext, PropertyEligibility propertyEligibility, @Nullable Cell[] propertiesCellArray) throws IOException {
        if (propertyEligibility.includeProperty("thesaurus")) {
            jw.key("thesaurus")
                    .value(motcle.getSubsetName());
        }
        jw.key("id")
                .value(motcle.getId());
        if (propertyEligibility.includeProperty("idalpha")) {
            String idalpha = motcle.getIdalpha();
            if (idalpha == null) {
                idalpha = "_" + motcle.getId();
            }
            jw.key("idalpha")
                    .value(idalpha);
        } else if (propertyEligibility.includeProperty("idalpha_significant")) {
            String idalpha = motcle.getSignificantIdalpha();
            if (idalpha != null) {
                jw.key("idalpha")
                        .value(idalpha);
            }
        }
        if (propertyEligibility.includeProperty("labels")) {
            jw.key("labels");
            jw.object();
            if (langContext instanceof ListLangContext) {
                for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                    addLabel(jw, motcle, unit.getLang());
                }
            } else if (langContext instanceof UserLangContext) {
                UserLangContext userLangContext = (UserLangContext) langContext;
                addLabel(jw, motcle, userLangContext.getWorkingLang());
            }
            jw.endObject();
        }
        if (propertyEligibility.includeProperty("level")) {
            jw.key("level")
                    .value(motcle.getLevel());
        }
        if (propertyEligibility.includeProperty("properties")) {
            CellJson.cellArrayMappingProperty(jw, propertiesCellArray);
        }
    }

    private static void addLabel(JSONWriter jw, Motcle motcle, Lang lang) throws IOException {
        jw.key(lang.toString());
        Label label = motcle.getLabels().getLangPartCheckedLabel(lang);
        if (label != null) {
            jw.value(label.getLabelString());
        } else {
            jw.value("");
        }
    }

}
