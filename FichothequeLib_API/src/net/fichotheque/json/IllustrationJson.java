/* FichothequeLib_API - Copyright (c) 2018-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import net.fichotheque.album.Illustration;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.PropertyEligibility;


/**
 *
 * @author Vincent Calame
 */
public final class IllustrationJson {

    private IllustrationJson() {

    }

    public static void properties(JSONWriter jsonWriter, Illustration illustration, PropertyEligibility propertyEligibility) throws IOException {
        if (propertyEligibility.includeProperty("album")) {
            jsonWriter.key("album");
            jsonWriter.value(illustration.getSubsetName());
        }
        jsonWriter.key("id");
        jsonWriter.value(illustration.getId());
        if (propertyEligibility.includeProperty("format")) {
            jsonWriter.key("format");
            jsonWriter.value(illustration.getFormatTypeString());
        }
    }

}
