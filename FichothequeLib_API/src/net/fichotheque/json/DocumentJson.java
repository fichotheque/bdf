/* FichothequeLib_API - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Locale;
import net.fichotheque.addenda.Document;
import net.fichotheque.addenda.Version;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.PropertyEligibility;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.ListLangContext;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.primitives.FileLength;


/**
 *
 * @author Vincent Calame
 */
public final class DocumentJson {

    private DocumentJson() {

    }

    public static void properties(JSONWriter jsonWriter, Document document, LangContext langContext, PropertyEligibility propertyEligibility) throws IOException {
        if (propertyEligibility.includeProperty("addenda")) {
            jsonWriter.key("addenda");
            jsonWriter.value(document.getSubsetName());
        }
        jsonWriter.key("id");
        jsonWriter.value(document.getId());
        if (propertyEligibility.includeProperty("basename")) {
            jsonWriter.key("basename");
            jsonWriter.value(document.getBasename());
        }
        if (propertyEligibility.includeProperty("versions")) {
            jsonWriter.key("versions");
            jsonWriter.array();
            for (Version version : document.getVersionList()) {
                jsonWriter.object();
                jsonWriter.key("extension");
                jsonWriter.value(version.getExtension());
                if (propertyEligibility.includeProperty("size")) {
                    FileLength fileLength = version.getFileLength();
                    float value = fileLength.getRoundedValue();
                    boolean isKibiOctet = (fileLength.getRoundType() == FileLength.KB_ROUND);
                    jsonWriter.key("size");
                    jsonWriter.object();
                    jsonWriter.key("value");
                    jsonWriter.value(value);
                    jsonWriter.key("unit");
                    jsonWriter.value((isKibiOctet) ? "Ki" : "Mi");
                    jsonWriter.key("labels");
                    jsonWriter.object();
                    if (langContext instanceof ListLangContext) {
                        for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                            addSizeLabel(jsonWriter, unit.getLang(), unit.getFormatLocale(), value, isKibiOctet);
                        }
                    } else if (langContext instanceof UserLangContext) {
                        UserLangContext userLangContext = (UserLangContext) langContext;
                        addSizeLabel(jsonWriter, userLangContext.getWorkingLang(), userLangContext.getFormatLocale(), value, isKibiOctet);
                    }
                    jsonWriter.endObject();
                    jsonWriter.endObject();
                }
                jsonWriter.endObject();
            }
            jsonWriter.endArray();
        }
    }

    private static void addSizeLabel(JSONWriter jsonWriter, Lang lang, Locale formatLocale, float value, boolean isKibiOctet) throws IOException {
        jsonWriter.key(lang.toString());
        NumberFormat format = NumberFormat.getInstance(formatLocale);
        StringBuilder buf = new StringBuilder();
        buf.append(format.format(value));
        buf.append(' ');
        if (isKibiOctet) {
            buf.append(LocalisationUtils.getKibiOctet(lang));
        } else {
            buf.append(LocalisationUtils.getMebiOctet(lang));
        }
        jsonWriter.value(buf.toString());
    }

}
