/* FichothequeLib_API - Copyright (c) 2018-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import net.fichotheque.Metadata;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;


/**
 *
 * @author Vincent Calame
 */
public final class MetadataJson {

    private MetadataJson() {

    }

    public static void properties(JSONWriter jw, Metadata metadata) throws IOException {
        jw.key("labelMap");
        CommonJson.object(jw, metadata.getTitleLabels());
        jw.key("labelPhraseMap");
        CommonJson.object(jw, metadata.getPhrases());
        jw.key("attrMap");
        CommonJson.object(jw, metadata.getAttributes());
    }

}
