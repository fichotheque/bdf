/* FichothequeLib_API - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import net.fichotheque.exportation.table.Cell;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.json.JSONString;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.money.Amount;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public final class CellJson {

    private CellJson() {

    }

    public static void properties(JSONWriter jw, Cell cell) throws IOException {
        Object obj = cell.getValue();
        if (obj != null) {
            jw.key(cell.getColDef().getColName());
            switch (cell.getFormatCast()) {
                case FormatConstants.INTEGER_CAST:
                    jw.value((Long) obj);
                    break;
                case FormatConstants.DECIMAL_CAST:
                    jw.value((Decimal) obj);
                    break;
                case FormatConstants.DATE_CAST:
                    jw.value(((FuzzyDate) obj).toString());
                    break;
                case FormatConstants.MONEY_CAST: {
                    jw.object();
                    {
                        Amount amount = (Amount) obj;
                        jw.key("currency")
                                .value(amount.getCurrencyCode());
                        jw.key("decimal")
                                .value(amount.toDecimal(true));
                        jw.key("long")
                                .value(amount.getMoneyLong());

                    }
                    jw.endObject();
                    break;
                }
                case FormatConstants.PERCENTAGE_CAST:
                    jw.value((Decimal) obj);
                    break;
                case FormatConstants.JSON_CAST:
                    jw.value(new InternalJSONString((String) obj));
                    break;
                default:
                    jw.value((String) obj);
            }
        }
    }

    public static void cellArrayMappingProperty(JSONWriter jw, @Nullable Cell[] cellArray) throws IOException {
        cellArrayMappingProperty(jw, cellArray, "properties");
    }

    public static void cellArrayMappingProperty(JSONWriter jw, @Nullable Cell[] cellArray, String propertyKey) throws IOException {
        jw.key(propertyKey);
        jw.object();
        if (cellArray != null) {
            for (Cell cell : cellArray) {
                properties(jw, cell);
            }
        }
        jw.endObject();
    }

    public static void properties(JSONWriter jw, Cell[] cellArray) throws IOException {
        for (Cell cell : cellArray) {
            properties(jw, cell);
        }
    }


    private static class InternalJSONString implements JSONString {

        private final String jsonString;

        private InternalJSONString(String jsonString) {
            this.jsonString = jsonString;
        }

        @Override
        public String toJSONString() {
            return jsonString;
        }

    }

}
