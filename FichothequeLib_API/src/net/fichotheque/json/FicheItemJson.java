/* FichothequeLib_API - Copyright (c) 2016-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.fiche.Link;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.fiche.Nombre;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Pays;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.syntax.FormSyntax;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.PropertyEligibility;
import net.mapeadores.util.localisation.Country;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.ListLangContext;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.localisation.MessageLocalisationProvider;
import net.mapeadores.util.localisation.SpecialCodes;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.money.ExtendedCurrency;
import net.mapeadores.util.money.MoneyUtils;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.primitives.DegreSexagesimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.DateFormatBundle;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class FicheItemJson {

    private FicheItemJson() {

    }

    public static void object(JSONWriter jsonWriter, FicheItem ficheItem, PropertyEligibility propertyEligibility, LangContext langContext, Fichotheque fichotheque, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        jsonWriter.object();
        properties(jsonWriter, ficheItem, propertyEligibility, langContext, fichotheque, messageLocalisationProvider);
        jsonWriter.endObject();

    }

    public static void properties(JSONWriter jsonWriter, FicheItem ficheItem, PropertyEligibility propertyEligibility, LangContext langContext, Fichotheque fichotheque, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        if (ficheItem instanceof Item) {
            properties(jsonWriter, (Item) ficheItem, propertyEligibility);
        } else if (ficheItem instanceof Datation) {
            properties(jsonWriter, (Datation) ficheItem, propertyEligibility, langContext);
        } else if (ficheItem instanceof Pays) {
            properties(jsonWriter, (Pays) ficheItem, propertyEligibility, langContext, messageLocalisationProvider);
        } else if (ficheItem instanceof Langue) {
            properties(jsonWriter, (Langue) ficheItem, propertyEligibility, langContext, messageLocalisationProvider);
        } else if (ficheItem instanceof Geopoint) {
            properties(jsonWriter, (Geopoint) ficheItem, propertyEligibility, langContext, messageLocalisationProvider);
        } else if (ficheItem instanceof Link) {
            properties(jsonWriter, (Link) ficheItem, propertyEligibility);
        } else if (ficheItem instanceof Image) {
            properties(jsonWriter, (Image) ficheItem, propertyEligibility);
        } else if (ficheItem instanceof Courriel) {
            properties(jsonWriter, (Courriel) ficheItem, propertyEligibility);
        } else if (ficheItem instanceof Nombre) {
            properties(jsonWriter, (Nombre) ficheItem, propertyEligibility, langContext);
        } else if (ficheItem instanceof Montant) {
            properties(jsonWriter, (Montant) ficheItem, propertyEligibility, langContext);
        } else if (ficheItem instanceof Para) {
            properties(jsonWriter, (Para) ficheItem, propertyEligibility);
        } else if (ficheItem instanceof Personne) {
            properties(jsonWriter, (Personne) ficheItem, propertyEligibility, fichotheque);
        }
        if (propertyEligibility.includeProperty("formsyntax")) {
            jsonWriter.key("formsyntax");
            jsonWriter.value(FormSyntax.toString(ficheItem, fichotheque, null));
        }
    }

    public static void properties(JSONWriter jsonWriter, Personne personne, PropertyEligibility propertyEligibility, Fichotheque fichotheque) throws IOException {
        String redacteurGlobalId = personne.getRedacteurGlobalId();
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("person");
        }
        if (propertyEligibility.includeProperty("code")) {
            jsonWriter.key("code");
            if (redacteurGlobalId != null) {
                Redacteur redacteur = SphereUtils.getRedacteur(fichotheque, redacteurGlobalId);
                if (redacteur != null) {
                    jsonWriter.value(redacteur.getBracketStyle());
                } else {
                    jsonWriter.value(redacteurGlobalId);
                }
            } else {
                jsonWriter.value(personne.getPersonCore().toStandardStyle());
            }
        }
        if (propertyEligibility.includeProperty("sphere")) {
            jsonWriter.key("sphere");
            if (redacteurGlobalId != null) {
                try {
                    SubsetKey sphereKey = SphereUtils.getSubsetKey(redacteurGlobalId);
                    jsonWriter.value(sphereKey.getSubsetName());
                } catch (ParseException pe) {
                    jsonWriter.value("#ERR: wrong redacteurGlobaleId = " + redacteurGlobalId);
                }
            } else {
                jsonWriter.value("");
            }
        }
        if (propertyEligibility.includeProperty("login")) {
            jsonWriter.key("login");
            if (redacteurGlobalId != null) {
                Redacteur redacteur = SphereUtils.getRedacteur(fichotheque, redacteurGlobalId);
                if (redacteur != null) {
                    jsonWriter.value(redacteur.getLogin());
                } else {
                    jsonWriter.value("");
                }
            } else {
                jsonWriter.value("");
            }
        }
        PersonCore personCore = SphereUtils.toPersonCore(fichotheque, personne);
        if (propertyEligibility.includeProperty("standard")) {
            jsonWriter.key("standard");
            jsonWriter.value(personCore.toStandardStyle());
        }
        if (propertyEligibility.includeProperty("directory")) {
            jsonWriter.key("directory");
            jsonWriter.value(personCore.toDirectoryStyle(false));
        }
        if (propertyEligibility.includeProperty("updirectory")) {
            jsonWriter.key("updirectory");
            jsonWriter.value(personCore.toDirectoryStyle(true));
        }
        if (propertyEligibility.includeProperty("biblio")) {
            jsonWriter.key("biblio");
            jsonWriter.value(personCore.toBiblioStyle(false));
        }
        if (propertyEligibility.includeProperty("upbiblio")) {
            jsonWriter.key("upbiblio");
            jsonWriter.value(personCore.toBiblioStyle(true));
        }
        if (propertyEligibility.includeProperty("surname")) {
            jsonWriter.key("surname");
            jsonWriter.value(personCore.getSurname());
        }
        if (propertyEligibility.includeProperty("upsurname")) {
            jsonWriter.key("upsurname");
            jsonWriter.value(personCore.getSurname().toUpperCase());
        }
        if (propertyEligibility.includeProperty("forename")) {
            jsonWriter.key("forename");
            jsonWriter.value(personCore.getForename());
        }
        if (propertyEligibility.includeProperty("nonlatin")) {
            jsonWriter.key("nonlatin");
            jsonWriter.value(personCore.getNonlatin());
        }
        if (propertyEligibility.includeProperty("surnamefirst")) {
            jsonWriter.key("surnamefirst");
            jsonWriter.value(personCore.isSurnameFirst());
        }
        if (propertyEligibility.includeProperty("organism")) {
            jsonWriter.key("organism");
            jsonWriter.value(personne.getOrganism());
        }
    }

    public static void properties(JSONWriter jsonWriter, Datation datation, PropertyEligibility propertyEligibility, LangContext langContext) throws IOException {
        FuzzyDate date = datation.getDate();
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("date");
        }
        if (propertyEligibility.includeProperty("code")) {
            jsonWriter.key("code");
            jsonWriter.value(date.toString());
        }
        if (propertyEligibility.includeProperty("iso")) {
            jsonWriter.key("iso");
            jsonWriter.value(date.toISOString(false));
        }
        if (propertyEligibility.includeProperty("lastday")) {
            jsonWriter.key("lastday");
            jsonWriter.value(date.toISOString(true));
        }
        if (propertyEligibility.includeProperty("year")) {
            jsonWriter.key("year");
            jsonWriter.value(String.valueOf(date.getYear()));
        }
        if (propertyEligibility.includeProperty("isomonth")) {
            jsonWriter.key("isomonth");
            jsonWriter.value(date.toMonthString(false));
        }
        if (propertyEligibility.includeProperty("lastmonth")) {
            jsonWriter.key("lastmonth");
            jsonWriter.value(date.toMonthString(true));
        }
        if (propertyEligibility.includeProperty("labels")) {
            jsonWriter.key("labels");
            dateLabelsObject(jsonWriter, date, langContext);
        }
        if (propertyEligibility.includeProperty("monthlabels")) {
            jsonWriter.key("monthlabels");
            dateLabelsObject(jsonWriter, date.truncate(FuzzyDate.MONTH_TYPE), langContext);
        }
    }

    public static void properties(JSONWriter jsonWriter, Pays pays, PropertyEligibility propertyEligibility, LangContext langContext, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("country");
        }
        if (propertyEligibility.includeProperty("code")) {
            jsonWriter.key("code");
            jsonWriter.value(pays.getCountry().toString());
        }
        if (propertyEligibility.includeProperty("labels")) {
            jsonWriter.key("labels");
            countryLabelsObject(jsonWriter, pays.getCountry(), langContext, messageLocalisationProvider);
        }
    }

    public static void properties(JSONWriter jsonWriter, Langue langue, PropertyEligibility propertyEligibility, LangContext langContext, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("lang");
        }
        if (propertyEligibility.includeProperty("code")) {
            jsonWriter.key("code");
            jsonWriter.value(langue.getLang().toString());
        }
        if (propertyEligibility.includeProperty("labels")) {
            jsonWriter.key("labels");
            langLabelsObject(jsonWriter, langue.getLang(), langContext, messageLocalisationProvider);
        }
    }

    public static void properties(JSONWriter jsonWriter, Geopoint geopoint, PropertyEligibility propertyEligibility, LangContext langContext, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("geopoint");
        }
        if (propertyEligibility.includeProperty("lat")) {
            jsonWriter.key("lat");
            jsonWriter.value(geopoint.getLatitude().toString());
        }
        if (propertyEligibility.includeProperty("lon")) {
            jsonWriter.key("lon");
            jsonWriter.value(geopoint.getLongitude().toString());
        }
        if (propertyEligibility.includeProperty("latlabels")) {
            jsonWriter.key("latlabels");
            geoLabelsObject(jsonWriter, geopoint.getLatitude(), true, langContext, messageLocalisationProvider);
        }
        if (propertyEligibility.includeProperty("lonlabels")) {
            jsonWriter.key("lonlabels");
            geoLabelsObject(jsonWriter, geopoint.getLongitude(), false, langContext, messageLocalisationProvider);
        }
    }

    public static void properties(JSONWriter jsonWriter, Link link, PropertyEligibility propertyEligibility) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("link");
        }
        if (propertyEligibility.includeProperty("href")) {
            jsonWriter.key("href");
            jsonWriter.value(link.getHref());
        }
        if (propertyEligibility.includeProperty("title")) {
            jsonWriter.key("title");
            jsonWriter.value(link.getTitle());
        }
        if (propertyEligibility.includeProperty("comment")) {
            jsonWriter.key("comment");
            jsonWriter.value(link.getComment());
        }
    }

    public static void properties(JSONWriter jsonWriter, Image image, PropertyEligibility propertyEligibility) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("image");
        }
        if (propertyEligibility.includeProperty("src")) {
            jsonWriter.key("src");
            jsonWriter.value(image.getSrc());
        }
        if (propertyEligibility.includeProperty("title")) {
            jsonWriter.key("title");
            jsonWriter.value(image.getTitle());
        }
        if (propertyEligibility.includeProperty("alt")) {
            jsonWriter.key("alt");
            jsonWriter.value(image.getAlt());
        }
    }

    public static void properties(JSONWriter jsonWriter, Courriel courriel, PropertyEligibility propertyEligibility) throws IOException {
        EmailCore emailCore = courriel.getEmailCore();
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("email");
        }
        if (propertyEligibility.includeProperty("complete")) {
            jsonWriter.key("complete");
            jsonWriter.value(emailCore.toCompleteString());
        }
        if (propertyEligibility.includeProperty("address")) {
            jsonWriter.key("address");
            jsonWriter.value(emailCore.getAddrSpec());
        }
        if (propertyEligibility.includeProperty("name")) {
            jsonWriter.key("name");
            jsonWriter.value(emailCore.getRealName());
        }
    }

    public static void properties(JSONWriter jsonWriter, Nombre nombre, PropertyEligibility propertyEligibility, LangContext langContext) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("number");
        }
        if (propertyEligibility.includeProperty("code")) {
            jsonWriter.key("code");
            jsonWriter.value(nombre.getDecimal().toString());
        }
        if (propertyEligibility.includeProperty("labels")) {
            jsonWriter.key("labels");
            decimalLabelsObject(jsonWriter, nombre.getDecimal(), langContext);
        }
    }

    public static void properties(JSONWriter jsonWriter, Montant montant, PropertyEligibility propertyEligibility, LangContext langContext) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("amount");
        }
        if (propertyEligibility.includeProperty("code")) {
            jsonWriter.key("code");
            jsonWriter.value(montant.toString());
        }
        if (propertyEligibility.includeProperty("currency")) {
            jsonWriter.key("currency");
            jsonWriter.value(montant.getCurrency().getCurrencyCode());
        }
        if (propertyEligibility.includeProperty("decimal")) {
            jsonWriter.key("decimal");
            jsonWriter.value(montant.getDecimal().toString());
        }
        if (propertyEligibility.includeProperty("labels")) {
            jsonWriter.key("labels");
            amountLabelsObject(jsonWriter, montant.getDecimal(), montant.getCurrency(), langContext);
        }
        if (propertyEligibility.includeProperty("long")) {
            jsonWriter.key("long");
            jsonWriter.value(montant.toMoneyLong());
        }
    }

    public static void properties(JSONWriter jsonWriter, Para para, PropertyEligibility propertyEligibility) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("para");
        }
        if (propertyEligibility.includeProperty("raw")) {
            jsonWriter.key("para");
            jsonWriter.value(para.contentToString());
        }
    }

    public static void properties(JSONWriter jsonWriter, Item item, PropertyEligibility propertyEligibility) throws IOException {
        if (propertyEligibility.includeProperty("type")) {
            jsonWriter.key("type");
            jsonWriter.value("item");
        }
        jsonWriter.key("value");
        jsonWriter.value(item.getValue());
    }

    public static void dateLabelsObject(JSONWriter jsonWriter, FuzzyDate date, LangContext langContext) throws IOException {
        jsonWriter.object();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                DateFormatBundle dateFormatBundle = DateFormatBundle.getDateFormatBundle(unit.getFormatLocale());
                jsonWriter.key(unit.getLang().toString());
                jsonWriter.value(date.getDateLitteral(dateFormatBundle));
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            DateFormatBundle dateFormatBundle = DateFormatBundle.getDateFormatBundle(userLangContext.getFormatLocale());
            jsonWriter.key(userLangContext.getWorkingLang().toString());
            jsonWriter.value(date.getDateLitteral(dateFormatBundle));
        }
        jsonWriter.endObject();
    }

    public static void countryLabelsObject(JSONWriter jsonWriter, Country country, LangContext langContext, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        localisationLabelsObject(jsonWriter, country.toString(), langContext, messageLocalisationProvider);
    }

    public static void langLabelsObject(JSONWriter jsonWriter, Lang lang, LangContext langContext, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        localisationLabelsObject(jsonWriter, lang.toString(), langContext, messageLocalisationProvider);
    }

    private static void localisationLabelsObject(JSONWriter jsonWriter, String code, LangContext langContext, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        jsonWriter.object();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang currentLang = unit.getLang();
                MessageLocalisation messageLocalisation = messageLocalisationProvider.getMessageLocalisation(currentLang);
                String message = messageLocalisation.toString(code);
                if (message != null) {
                    message = StringUtils.getFirstPart(message);
                    jsonWriter.key(currentLang.toString());
                    jsonWriter.value(message);
                }
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            MessageLocalisation messageLocalisation = messageLocalisationProvider.getMessageLocalisation(userLangContext);
            String message = messageLocalisation.toString(code);
            if (message != null) {
                message = StringUtils.getFirstPart(message);
                jsonWriter.key(userLangContext.getWorkingLang().toString());
                jsonWriter.value(message);
            }
        }
        jsonWriter.endObject();
    }

    public static void geoLabelsObject(JSONWriter jsonWriter, DegreDecimal degreDecimal, boolean latitude, LangContext langContext, MessageLocalisationProvider messageLocalisationProvider) throws IOException {
        DegreSexagesimal sexa = DegreSexagesimal.fromDegreDecimal(degreDecimal);
        String value = sexa.toString(false, " ");
        String code;
        if (latitude) {
            code = SpecialCodes.getLatitudeSpecialCode(sexa);
        } else {
            code = SpecialCodes.getLongitudeSpecialCode(sexa);
        }
        jsonWriter.object();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                Lang lang = unit.getLang();
                MessageLocalisation messageLocalisation = messageLocalisationProvider.getMessageLocalisation(lang);
                String message = messageLocalisation.toString(code);
                if (message != null) {
                    jsonWriter.key(lang.toString());
                    jsonWriter.value(value + message);
                }
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            MessageLocalisation messageLocalisation = messageLocalisationProvider.getMessageLocalisation(userLangContext);
            String message = messageLocalisation.toString(code);
            if (message != null) {
                jsonWriter.key(userLangContext.getWorkingLang().toString());
                jsonWriter.value(value + message);
            }
        }
        jsonWriter.endObject();
    }

    public static void decimalLabelsObject(JSONWriter jsonWriter, Decimal decimal, LangContext langContext) throws IOException {
        jsonWriter.object();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                DecimalFormatSymbols symbols = new DecimalFormatSymbols(unit.getFormatLocale());
                jsonWriter.key(unit.getLang().toString());
                jsonWriter.value(decimal.toString(symbols));
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(userLangContext.getFormatLocale());
            jsonWriter.key(userLangContext.getWorkingLang().toString());
            jsonWriter.value(decimal.toString(symbols));
        }
        jsonWriter.endObject();
    }

    public static void amountLabelsObject(JSONWriter jsonWriter, Decimal decimal, ExtendedCurrency currency, LangContext langContext) throws IOException {
        jsonWriter.object();
        if (langContext instanceof ListLangContext) {
            for (ListLangContext.Unit unit : (ListLangContext) langContext) {
                DecimalFormatSymbols symbols = new DecimalFormatSymbols(unit.getFormatLocale());
                jsonWriter.key(unit.getLang().toString());
                jsonWriter.value(MoneyUtils.toLitteralString(decimal, currency, symbols));
            }
        } else if (langContext instanceof UserLangContext) {
            UserLangContext userLangContext = (UserLangContext) langContext;
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(userLangContext.getFormatLocale());
            jsonWriter.key(userLangContext.getWorkingLang().toString());
            jsonWriter.value(MoneyUtils.toLitteralString(decimal, currency, symbols));
        }
        jsonWriter.endObject();
    }

}
