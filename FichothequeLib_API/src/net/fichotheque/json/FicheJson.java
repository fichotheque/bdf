/* FichothequeLib_API - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.json;

import java.io.IOException;
import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Information;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Propriete;
import net.fichotheque.corpus.fiche.Section;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.syntax.FicheblockSyntax;
import net.fichotheque.syntax.FormSyntax;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SeparatorOptions;


/**
 *
 * @author Vincent Calame
 */
public final class FicheJson {

    private FicheJson() {

    }

    public static void properties(JSONWriter jsonWriter, Corpus corpus, FicheAPI fiche) throws IOException {
        Fichotheque fichotheque = corpus.getFichotheque();
        CorpusMetadata corpusMetadata = corpus.getCorpusMetadata();
        jsonWriter.key("titre");
        jsonWriter.value(fiche.getTitre());
        Para soustitre = fiche.getSoustitre();
        if (soustitre != null) {
            jsonWriter.key("soustitre");
            jsonWriter.value(FormSyntax.toString(soustitre, fichotheque, null));
        }
        Lang lang = fiche.getLang();
        if (lang != null) {
            jsonWriter.key("lang");
            jsonWriter.value(lang.toString());
        }
        FicheItems redacteurs = fiche.getRedacteurs();
        if (redacteurs != null) {
            jsonWriter.key(FieldKey.SPECIAL_REDACTEURS);
            jsonWriter.value(FormSyntax.toString(redacteurs, fichotheque, FormSyntax.DEFAULT_INLINE_SEPARATOROPTIONS, null));
        }
        for (Propriete propriete : fiche.getProprieteList()) {
            jsonWriter.key(propriete.getFieldKey().getKeyString());
            jsonWriter.value(FormSyntax.toString(propriete.getFicheItem(), fichotheque, null));
        }
        for (Information information : fiche.getInformationList()) {
            FieldKey informationFieldKey = information.getFieldKey();
            SeparatorOptions separatorOptions = FormSyntax.DEFAULT_INLINE_SEPARATOROPTIONS;
            CorpusField corpusField = corpusMetadata.getCorpusField(informationFieldKey);
            if (corpusField != null) {
                if (corpusField.isBlockDisplayInformationField()) {
                    separatorOptions = FormSyntax.DEFAULT_BLOCK_SEPARATOROPTIONS;
                }
            }
            jsonWriter.key(informationFieldKey.getKeyString());
            jsonWriter.value(FormSyntax.toString(information, fichotheque, separatorOptions, null));
        }
        FicheblockSyntax.Parameters ficheBlockFormSyntaxParameters = FicheblockSyntax.parameters().withSpecialParseChars(true);
        for (Section section : fiche.getSectionList()) {
            jsonWriter.key(section.getFieldKey().getKeyString());
            jsonWriter.value(FicheblockSyntax.toString(section, ficheBlockFormSyntaxParameters));
        }
    }

}
