/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.metadata;

import java.util.List;
import net.fichotheque.Metadata;
import net.fichotheque.corpus.Corpus;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusMetadata extends Metadata {

    public Corpus getCorpus();

    /**
     * @Nullable
     */
    public CorpusField getCorpusField(FieldKey fieldKey);

    public List<CorpusField> getProprieteList();

    public List<CorpusField> getInformationList();

    public List<CorpusField> getSectionList();

    /**
     * Ce doit être une propriété de type GEOPOINT_FIELD (ou égal à nul)
     *
     * @Nullable
     */
    public CorpusField getGeolocalisationField();

    public FieldGeneration getFieldGeneration();

    public default boolean isWithSoustitre() {
        return (getCorpusField(FieldKey.SOUSTITRE) != null);
    }


}
