/* FichothequeLib_API - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.metadata;

import java.util.List;
import net.fichotheque.format.FichothequeFormatDef;


/**
 *
 * @author Vincent Calame
 */
public interface FieldGeneration {

    public String getRawString();

    public List<Entry> getEntryList();

    public default boolean isEmpty() {
        return getEntryList().isEmpty();
    }


    public interface Entry {

        public FieldKey getFieldKey();

        public FichothequeFormatDef getFormatDef();

    }

}
