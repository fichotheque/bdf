/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.metadata;

import java.text.ParseException;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.money.Currencies;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.MultiStringable;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusField {

    public final static short NO_FICHEITEM_FIELD = 0;
    public final static short ITEM_FIELD = 1;
    public final static short PERSONNE_FIELD = 2;
    public final static short LANGUE_FIELD = 3;
    public final static short PAYS_FIELD = 4;
    public final static short DATATION_FIELD = 5;
    public final static short COURRIEL_FIELD = 6;
    public final static short LINK_FIELD = 7;
    public final static short NOMBRE_FIELD = 8;
    public final static short MONTANT_FIELD = 9;
    public final static short GEOPOINT_FIELD = 10;
    public final static short PARA_FIELD = 11;
    public final static short IMAGE_FIELD = 12;

    public FieldKey getFieldKey();

    public Labels getLabels();

    public Set<String> getOptionNameSet();

    /**
     * Instance de String ou MultiStringable
     */
    public Object getOption(String optionName);

    public CorpusMetadata getCorpusMetadata();

    /**
     * Le champ des rédacteurs retourne PERSONNE_FIELD, le champ Langue retourne
     * LANGUE_FIELD.
     *
     */
    public short getFicheItemType();

    public boolean isGenerated();

    public default String getFicheItemTypeString() {
        return ficheItemTypeToString(getFicheItemType());
    }

    public default boolean isSpecial() {
        return getFieldKey().isSpecial();
    }

    public default boolean isPropriete() {
        return getFieldKey().isPropriete();
    }

    public default boolean isInformation() {
        return getFieldKey().isInformation();
    }

    public default boolean isSection() {
        return getFieldKey().isSection();
    }

    public default String getFieldName() {
        return getFieldKey().getFieldName();
    }

    public default String getFieldString() {
        return getFieldKey().getKeyString();
    }

    public default short getCategory() {
        return getFieldKey().getCategory();
    }

    public default String getCategoryString() {
        return FieldKey.categoryToString(getFieldKey().getCategory());
    }

    public default String getStringOption(String optionName) {
        return (String) getOption(optionName);
    }

    public default boolean isBlockDisplayInformationField() {
        if (!isInformation()) {
            return false;
        }
        String value = getStringOption(FieldOptionConstants.INFORMATIONDISPLAY_OPTION);
        if (value == null) {
            return false;
        }
        return (value.equals(FieldOptionConstants.BLOCK_DISPLAY));
    }

    /**
     * Retourne l'étendue des langues disponibles pour les champs traitant des
     * langues. Les trois valeurs possibles sont:
     * <ul>
     * <li>FieldOptionsConstants.ALL_SCOPE : toutes les langues sont disponibles
     * <li>FieldOptionsConstants.CONFIG_SCOPE : les langues disponibles sont
     * celles de la configuration globale
     * <li>FieldOptionsConstants.LIST_SCOPE : les langues disponibles sont
     * définies par la liste retournée par getLangs() (sa valeur ne doit
     * impérativement pas être nulle)
     * </ul>
     * <p>
     * Cette méthode doit retourner une valeur pour le champ de la langue de la
     * fiche (FieldKey.LANG) et pour tous les champs Information ou Propriété de
     * type langue et nul pour tous les autres champs.
     * <p>
     * La valeur par défaut est différente suivant les champs : pour le champ de
     * la langue de la fiche (FieldKey.LANG), elle est vaut
     * FieldOptionsConstants.CONFIG_SCOPE et elle vaut
     * FieldOptionsConstants.ALL_SCOPE pour tous les autres champs de type
     * langue.
     *
     * @return l'indication de l'étendue des langues disponibles ou nul si le
     * champ n'est pas concerné
     */
    public default String getLangScope() {
        if (getFicheItemType() != CorpusField.LANGUE_FIELD) {
            return null;
        }
        String value = getStringOption(FieldOptionConstants.LANGSCOPE_OPTION);
        if (value == null) {
            return CorpusMetadataUtils.getDefaultLangScope(getFieldKey());
        }
        return value;
    }

    /**
     * Indique si le champ doit être décomposé en sous-champ dans lors de la
     * saisie. Cette option n'est pertinente que pour les Propriétés dont le
     * type possède des sous-champs (par exemple, un propriété de type «
     * Geopoint » possède deux sous-champs : la latitude et la longitude).
     *
     * @return true si la décomposition en sous-champ doit être utilisée, false
     * sinon
     */
    public default boolean isSubfieldDisplay() {
        String value = getStringOption(FieldOptionConstants.SUBFIELDDISPLAY_OPTION);
        return StringUtils.isTrue(value);
    }

    /**
     * Retourne la sphère par défaut pour le traitement des champs de type «
     * Personne ». Peut être nul (et dans ce cas la sphère par défaut sera celle
     * du rédacteur intervenant sur la fiche). SubsetKey doit désigner une
     * sphère et non une autre collecion.
     *
     * @return un objet SubsetKey indentifiant la sphère par défaut ou null
     */
    public default SubsetKey getDefaultSphereKey() {
        String value = getStringOption(FieldOptionConstants.DEFAULTSPHEREKEY_OPTION);
        if (value == null) {
            return null;
        }
        try {
            SubsetKey sphereKey = SubsetKey.parse(value);
            if (!sphereKey.isSphereSubset()) {
                return null;
            }
            return sphereKey;
        } catch (ParseException pe) {
            return null;
        }
    }

    /**
     * Retourne la liste des monnaies acceptées par le champ. Peut être nul mais
     * pas de longueur nul.
     *
     * @return une instance de Currencies listant les monnaies acceptées ou null
     */
    public default Currencies getCurrencies() {
        Object value = getOption(FieldOptionConstants.CURRENCYARRAY_OPTION);
        if (value == null) {
            return null;
        }
        return (Currencies) value;
    }

    public default Langs getLangs() {
        Object value = getOption(FieldOptionConstants.LANGARRAY_OPTION);
        if (value == null) {
            return null;
        }
        return (Langs) value;
    }

    /**
     * Retourne la liste des champs permettant de construire une adresse. Cette
     * options est utilisée pour le géocodage. L'adresse de géocodage sera
     * construite en concaténant les valeurs des champs indiqués par l'instance
     * de MultiStringable. Peut être nul.
     *
     * @return une instance de MultiStringable listant les champs pour
     * construire une adresse ou null
     */
    public default MultiStringable getAddressFieldNames() {
        Object value = getOption(FieldOptionConstants.ADDRESSFIELDARRAY_OPTION);
        if (value == null) {
            return null;
        }
        return (MultiStringable) value;
    }

    public static String ficheItemTypeToString(short ficheItemType) {
        switch (ficheItemType) {
            case CorpusField.PERSONNE_FIELD:
                return "personne";
            case CorpusField.LANGUE_FIELD:
                return "langue";
            case CorpusField.DATATION_FIELD:
                return "datation";
            case CorpusField.PAYS_FIELD:
                return "pays";
            case CorpusField.COURRIEL_FIELD:
                return "courriel";
            case CorpusField.LINK_FIELD:
                return "link";
            case CorpusField.ITEM_FIELD:
                return "item";
            case CorpusField.NOMBRE_FIELD:
                return "nombre";
            case CorpusField.MONTANT_FIELD:
                return "montant";
            case CorpusField.GEOPOINT_FIELD:
                return "geopoint";
            case CorpusField.PARA_FIELD:
                return "para";
            case CorpusField.IMAGE_FIELD:
                return "image";
            default:
                return "";
        }
    }

    public static short ficheItemTypeToShort(String s) {
        if (s.equals("item")) {
            return CorpusField.ITEM_FIELD;
        }
        if (s.equals("personne")) {
            return CorpusField.PERSONNE_FIELD;
        }
        if (s.equals("langue")) {
            return CorpusField.LANGUE_FIELD;
        }
        if (s.equals("pays")) {
            return CorpusField.PAYS_FIELD;
        }
        if (s.equals("datation")) {
            return CorpusField.DATATION_FIELD;
        }
        if (s.equals("courriel")) {
            return CorpusField.COURRIEL_FIELD;
        }
        if (s.equals("link")) {
            return CorpusField.LINK_FIELD;
        }
        if (s.equals("nombre")) {
            return CorpusField.NOMBRE_FIELD;
        }
        if (s.equals("montant")) {
            return CorpusField.MONTANT_FIELD;
        }
        if (s.equals("geopoint")) {
            return CorpusField.GEOPOINT_FIELD;
        }
        if (s.equals("para")) {
            return CorpusField.PARA_FIELD;
        }
        if (s.equals("image")) {
            return CorpusField.IMAGE_FIELD;
        }
        throw new IllegalArgumentException("unkonwn type : " + s);
    }

    public static void checkFicheItemType(short ficheItemType) {
        switch (ficheItemType) {
            case CorpusField.PERSONNE_FIELD:
            case CorpusField.LANGUE_FIELD:
            case CorpusField.DATATION_FIELD:
            case CorpusField.PAYS_FIELD:
            case CorpusField.COURRIEL_FIELD:
            case CorpusField.LINK_FIELD:
            case CorpusField.ITEM_FIELD:
            case CorpusField.NOMBRE_FIELD:
            case CorpusField.MONTANT_FIELD:
            case CorpusField.GEOPOINT_FIELD:
            case CorpusField.PARA_FIELD:
            case CorpusField.IMAGE_FIELD:
                return;
            case CorpusField.NO_FICHEITEM_FIELD:
                throw new IllegalArgumentException("FicheItemType cannot be NO_FICHEITEM_FIELD");
            default:
                throw new IllegalArgumentException("Wrong ficheItemType value : " + ficheItemType);
        }
    }

}
