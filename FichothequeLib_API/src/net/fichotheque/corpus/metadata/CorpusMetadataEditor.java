/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.metadata;

import net.fichotheque.MetadataEditor;
import net.fichotheque.corpus.CorpusEditor;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusMetadataEditor extends MetadataEditor {

    public CorpusEditor getCorpusEditor();

    /**
     *
     * @throws IllegalArgumentException si ficheItemType n'est pas valide
     * (uniquement pour les informations et les propriétés).
     */
    public CorpusField createCorpusField(FieldKey fieldKey, short ficheItemType) throws ExistingFieldKeyException;

    public boolean putFieldLabel(CorpusField corpusField, Label label);

    public boolean removeFieldLabel(CorpusField corpusField, Lang lang);

    /**
     * Retourne <em>true</em> si un changement a vraiment eu lieu,
     * <em>false</em> sinon. Pour l'option AddressFieldList, l'objet doit être
     * une instance de FieldKey[] ou de CorpusFieldList
     */
    public boolean setFieldOption(CorpusField corpusField, String optionName, Object optionValue) throws FieldOptionException;

    /**
     *
     * @throws IllegalArgumentException si corpusField n'appatient pas au corpus
     */
    public void removeCorpusField(CorpusField corpusField) throws MandatoryFieldException;

    /**
     * Le champ doit être une proprieté de type GEOPOINT_FIELD
     *
     * @throws IllegalArgumentException si corpusField est incorrect
     * @return true si le changement est effectif, false sinon
     */
    public boolean setGeolocalisationField(CorpusField corpusField);

    /**
     * Indique les champs à générer automatiquement.
     *
     * @param fieldGeneration la nouvelle définition
     * @return true si le changement est effectif, false sinon
     */
    public boolean setFieldGeneration(FieldGeneration fieldGeneration);

    @Override
    public default boolean removeAttribute(AttributeKey attributeKey) {
        return getCorpusEditor().getFichothequeEditor().removeAttribute(getMetadata(), attributeKey);
    }

    @Override
    public default boolean putAttribute(Attribute attribute) {
        return getCorpusEditor().getFichothequeEditor().putAttribute(getMetadata(), attribute);
    }

    public default boolean changeFieldLabels(CorpusField corpusField, LabelChange labelChange) {
        boolean done = false;
        for (Label label : labelChange.getChangedLabels()) {
            boolean stepDone = putFieldLabel(corpusField, label);
            if (stepDone) {
                done = true;
            }
        }
        for (Lang lang : labelChange.getRemovedLangList()) {
            boolean stepDone = removeFieldLabel(corpusField, lang);
            if (stepDone) {
                done = true;
            }
        }
        return done;
    }

}
