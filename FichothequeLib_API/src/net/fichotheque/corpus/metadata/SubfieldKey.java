/* FichothequeLib_API - Copyright (c) 2007-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.metadata;

import java.text.ParseException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.money.ExtendedCurrency;


/**
 *
 * @author Vincent Calame
 */
public final class SubfieldKey {

    public static final short SURNAME_SUBTYPE = 1;
    public static final short FORENAME_SUBTYPE = 2;
    public static final short SURNAMEFIRST_SUBTYPE = 3;
    public static final short NONLATIN_SUBTYPE = 4;
    public static final short VALUE_SUBTYPE = 11;
    public static final short CURRENCY_SUBTYPE = 12;
    public static final short LATITUDE_SUBTYPE = 21;
    public static final short LONGITUDE_SUBTYPE = 22;
    public static final short SRC_SUBTYPE = 31;
    public static final short ALT_SUBTYPE = 32;
    public static final short TITLE_SUBTYPE = 33;
    public static final short OTHERS_SUBTYPE = 99;
    public static final short AMOUNT_SUBTYPE = 111;
    public static final short LANG_SUBTYPE = 121;
    private final FieldKey fieldKey;
    private final short subtype;
    private final Lang lang;
    private final ExtendedCurrency currency;


    private SubfieldKey(FieldKey fieldKey, short subtype, Lang lang, ExtendedCurrency currency) {
        this.fieldKey = fieldKey;
        this.subtype = subtype;
        this.lang = lang;
        this.currency = currency;

    }

    public FieldKey getFieldKey() {
        return fieldKey;
    }

    public short getSubtype() {
        return subtype;
    }

    public ExtendedCurrency getCurrency() {
        return currency;
    }

    public Lang getLang() {
        return lang;
    }

    @Override
    public int hashCode() {
        if (lang != null) {
            return fieldKey.hashCode() + lang.hashCode();
        } else if (currency != null) {
            return fieldKey.hashCode() + currency.hashCode();
        } else {
            return fieldKey.hashCode() + subtype;
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        SubfieldKey fk = (SubfieldKey) other;
        if (fk.subtype != this.subtype) {
            return false;
        }
        if (fk.lang != null) {
            if (this.lang == null) {
                return false;
            } else if (!fk.lang.equals(this.lang)) {
                return false;
            }
        } else if (this.lang != null) {
            return false;
        }
        if (fk.currency != null) {
            if (this.currency == null) {
                return false;
            } else if (!fk.currency.equals(this.currency)) {
                return false;
            }
        } else if (this.currency != null) {
            return false;
        }
        return fk.fieldKey.equals(this.fieldKey);
    }

    public String getKeyString() {
        return toString();
    }

    @Override
    public String toString() {
        return fieldKey.getKeyString() + "_" + subtypeToString();
    }

    public static SubfieldKey toSubfieldKey(FieldKey fieldKey, short type) {
        switch (type) {
            case AMOUNT_SUBTYPE:
                throw new IllegalArgumentException("Not use with AMOUNT_SUBTYPE");
            case LANG_SUBTYPE:
                throw new IllegalArgumentException("Not use with LANG_SUBTYPE");
        }
        return new SubfieldKey(fieldKey, type, null, null);
    }

    public static SubfieldKey toAmountSubfieldKey(FieldKey fieldKey, ExtendedCurrency currency) {
        return new SubfieldKey(fieldKey, AMOUNT_SUBTYPE, null, currency);
    }

    public static SubfieldKey toLangSubfieldKey(FieldKey fieldKey, Lang lang) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        return new SubfieldKey(fieldKey, LANG_SUBTYPE, lang, null);
    }

    public static SubfieldKey parse(String s) throws ParseException {
        int idx0 = s.indexOf('_');
        if (idx0 == -1) {
            throw new ParseException("No '_' character", 0);
        }
        int idx1 = s.indexOf('_', idx0 + 1);
        if (idx1 == -1) {
            throw new ParseException("Missing second '_' character", idx0);
        }
        FieldKey fieldKey = FieldKey.parse(s.substring(0, idx1));
        String subtypeString = s.substring(idx1 + 1);
        int idx2 = subtypeString.indexOf('_');
        if (idx2 == -1) {
            try {
                short subtype = subtypeToShort(subtypeString);
                return SubfieldKey.toSubfieldKey(fieldKey, subtype);
            } catch (IllegalArgumentException iae) {
                throw new ParseException("Wrong subtype = " + subtypeString, idx1 + 1);
            }
        } else {
            String part1 = subtypeString.substring(0, idx2);
            String part2 = subtypeString.substring(idx2 + 1);
            if ((part1.equals("amount")) || (part1.equals("montant"))) {
                try {
                    ExtendedCurrency currency = ExtendedCurrency.parse(part2);
                    return toAmountSubfieldKey(fieldKey, currency);
                } catch (ParseException pe) {
                    throw new ParseException("Wrong currency = " + part2, idx2 + 1);
                }
            } else if (part1.equals("lang")) {
                try {
                    Lang lang = Lang.parse(part2);
                    return toLangSubfieldKey(fieldKey, lang);
                } catch (ParseException pe) {
                    throw new ParseException("Wrong lang = " + part2, idx2 + 1);
                }
            } else {
                throw new ParseException("Wrong subtype suffix = " + part1, idx1 + 1);
            }
        }

    }

    /**
     * Équivalent de parse() mais en envoyant IllegalArgumentException plutôt
     * que ParseException. à utiliser quand on est sûr de la syntaxe et permet
     * d'éviter un try {} catch {}
     */
    public static SubfieldKey build(String s) {
        try {
            return parse(s);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe.getMessage());
        }
    }

    private String subtypeToString() {
        switch (subtype) {
            case SURNAME_SUBTYPE:
                return "surname";
            case FORENAME_SUBTYPE:
                return "forename";
            case NONLATIN_SUBTYPE:
                return "nonlatin";
            case SURNAMEFIRST_SUBTYPE:
                return "surnamefirst";
            case VALUE_SUBTYPE:
                return "value";
            case CURRENCY_SUBTYPE:
                return "currency";
            case LATITUDE_SUBTYPE:
                return "lat";
            case LONGITUDE_SUBTYPE:
                return "lon";
            case SRC_SUBTYPE:
                return "src";
            case ALT_SUBTYPE:
                return "alt";
            case TITLE_SUBTYPE:
                return "title";
            case OTHERS_SUBTYPE:
                return "others";
            case AMOUNT_SUBTYPE:
                return "amount_" + currency.toString();
            case LANG_SUBTYPE:
                return "lang_" + lang.toString();
            default:
                throw new IllegalArgumentException("Wrong subtype = " + subtype);
        }
    }

    private static short subtypeToShort(String subtype) {
        switch (subtype) {
            case "surname":
            case "nom":
                return SURNAME_SUBTYPE;
            case "forename":
            case "prenom":
                return FORENAME_SUBTYPE;
            case "surnamefirst":
            case "nomavant":
                return SURNAMEFIRST_SUBTYPE;
            case "nonlatin":
            case "original":
                return NONLATIN_SUBTYPE;
            case "value":
                return VALUE_SUBTYPE;
            case "currency":
            case "cur":
                return CURRENCY_SUBTYPE;
            case "lat":
                return LATITUDE_SUBTYPE;
            case "lon":
                return LONGITUDE_SUBTYPE;
            case "src":
                return SRC_SUBTYPE;
            case "alt":
                return ALT_SUBTYPE;
            case "title":
                return TITLE_SUBTYPE;
            case "others":
                return OTHERS_SUBTYPE;
            default:
                throw new IllegalArgumentException("Wrong subtype = " + subtype);
        }

    }

    public static boolean isLegalSubfield(CorpusField corpusField, short subfieldType) {
        switch (corpusField.getCategory()) {
            case FieldKey.PROPRIETE_CATEGORY: {
                switch (corpusField.getFicheItemType()) {
                    case CorpusField.PERSONNE_FIELD:
                        return isLegalPersonnePropriete(subfieldType);
                    case CorpusField.MONTANT_FIELD:
                        return isLegalMontantPropriete(subfieldType);
                    case CorpusField.GEOPOINT_FIELD:
                        return isLegalGeopointPropriete(subfieldType);
                    case CorpusField.IMAGE_FIELD:
                        return isLegalImagePropriete(subfieldType);
                    default:
                        return false;
                }
            }
            case FieldKey.INFORMATION_CATEGORY: {
                switch (corpusField.getFicheItemType()) {
                    case CorpusField.MONTANT_FIELD:
                        return isLegalMontantInformation(subfieldType);
                    default:
                        return false;
                }
            }
            default:
                return false;
        }
    }


    private static boolean isLegalImagePropriete(short subfieldType) {
        switch (subfieldType) {
            case SRC_SUBTYPE:
            case ALT_SUBTYPE:
            case TITLE_SUBTYPE:
                return true;
            default:
                return false;
        }
    }

    private static boolean isLegalGeopointPropriete(short subfieldType) {
        switch (subfieldType) {
            case LATITUDE_SUBTYPE:
            case LONGITUDE_SUBTYPE:
                return true;
            default:
                return false;
        }
    }

    private static boolean isLegalPersonnePropriete(short subfieldType) {
        switch (subfieldType) {
            case SURNAME_SUBTYPE:
            case FORENAME_SUBTYPE:
            case SURNAMEFIRST_SUBTYPE:
            case NONLATIN_SUBTYPE:
                return true;
            default:
                return false;
        }
    }

    private static boolean isLegalMontantPropriete(short subfieldType) {
        switch (subfieldType) {
            case VALUE_SUBTYPE:
            case CURRENCY_SUBTYPE:
                return true;
            default:
                return false;
        }

    }

    private static boolean isLegalMontantInformation(short subfieldType) {
        switch (subfieldType) {
            case AMOUNT_SUBTYPE:
            case OTHERS_SUBTYPE:
                return true;
            default:
                return false;
        }

    }

}
