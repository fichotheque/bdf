/* FichothequeLib_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.metadata;


/**
 *
 * @author Vincent Calame
 */
public interface FieldOptionConstants {

    /**
     * Option propre au champ Information (et obligatoire pour ce champ)
     * indiquant si les éléments d'information sont de « en ligne « ou des «
     * blocs ». L'objet doit être une instance d'Integer et avoir comme valeur
     * INLINE_DISPLAY ou BLOCK_DISPLAY.
     */
    public final static String INFORMATIONDISPLAY_OPTION = "informationDisplay";
    /**
     * Option propre aux champs Propriété et Information qui sont de type
     * Personne. Indique la sphère à utilisateur par défaut. L'objet doit être
     * une instance de SubsetKey ou null, indiquant ainsi que c'est la sphère de
     * l'utilisateur qui doit être utilisée.
     */
    public final static String DEFAULTSPHEREKEY_OPTION = "defaultSphereKey";
    /**
     * Option propre aux champs Propriété qui indique que ce champ doit être
     * décomposé en sous-champs Les types acceptés sont « personne » et «
     * geopoint » La valeur renvoyée doit être Boolean.true ou null
     */
    public final static String SUBFIELDDISPLAY_OPTION = "subfieldDisplay";
    /**
     * Option propre au champ monétaire indiquant les monnaies possibles.
     */
    public final static String CURRENCYARRAY_OPTION = "currencyArray";
    /**
     * Option indiquant que le titre doit être généré à partir de la valeur
     * d'autres champs. La valeur est null si le titre ne doit pas être généré
     * et sinon une instance de FicheFormatDef
     */
    public final static String TITREGENERATIONFORMAT_OPTION = "titreGenerationFormat";
    /**
     * Option générale au corpus (non liée à un champ particulier) indiquant
     * l'existence d'un champ de géolocalisation.
     */
    public final static String GEOLOCALISATIONFIELD_OPTION = "geolocalisationField";
    public final static String ADDRESSFIELDARRAY_OPTION = "addressFieldArray";
    public final static String BASEURL_OPTION = "baseUrl";
    public final static String IMAGESOURCE_OPTION = "imageSource";
    public final static String BLOCK_DISPLAY = "block";
    public final static String ALL_SCOPE = "all";
    public final static String CONFIG_SCOPE = "config";
    public final static String LIST_SCOPE = "list";
    public final static String LANGSCOPE_OPTION = "langScope";
    /**
     * Option propre au champ LANG (valeurs possibles), aux champs proprieté ou
     * information de type langues (valeurs) ou données textuelles (différentes
     * langues disponibles)
     */
    public final static String LANGARRAY_OPTION = "langArray";

}
