/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.metadata;

import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.text.StringUtils;


/**
 * <p>
 * Identifie un champ d'un corpus. Il y a quatre types de champs :
 * <ul>
 * <li>Les champs spéciaux au nombre de cinq : ID, LANG, REDACTEURS, TITRE et
 * SOUTITRE. Les quatre premiers sont obligatoires et doivent toujours se
 * retrouver dans un corpus, SOUSTITRE est facultatif.
 * <li>Les chams Propriété, situés dans l'en-tête de la fiche ils contiennent au
 * maximum une valeur de fieldName FicheItem.
 * <li>Les champs Information, situés dans l'en-tête et qui contiennent zéro,
 * une ou plusieurs valeurs de fieldName FicheItem
 * <li>Les champs Section situés dans le corps de fiche, qui représentent des
 * blocs de texte
 * </ul>
 *
 * @author Vincent Calame
 */
public final class FieldKey implements Serializable {

    private static final Map<String, FieldKey> internMap = new HashMap<String, FieldKey>();
    public final static String SPECIAL_ID = "id";
    public final static String SPECIAL_TITRE = "titre";
    public final static String SPECIAL_SOUSTITRE = "soustitre";
    public final static String SPECIAL_LANG = "lang";
    public final static String SPECIAL_REDACTEURS = "redacteurs";
    public static final short SPECIAL_CATEGORY = 0;
    public static final short PROPRIETE_CATEGORY = 1;
    public static final short INFORMATION_CATEGORY = 2;
    public static final short SECTION_CATEGORY = 3;
    private final String fieldKeyString;
    private final short category;
    private final String fieldName;
    public final static FieldKey ID = new FieldKey(SPECIAL_ID);
    public final static FieldKey TITRE = new FieldKey(SPECIAL_TITRE);
    public final static FieldKey SOUSTITRE = new FieldKey(SPECIAL_SOUSTITRE);
    public final static FieldKey LANG = new FieldKey(SPECIAL_LANG);
    public final static FieldKey REDACTEURS = new FieldKey(SPECIAL_REDACTEURS);

    private FieldKey(String fieldKeyString) {
        this.fieldKeyString = fieldKeyString;
        this.category = SPECIAL_CATEGORY;
        this.fieldName = "";
        intern(this);
    }

    private FieldKey(String fieldKeyString, String fieldName, short category) {
        this.fieldKeyString = fieldKeyString;
        this.fieldName = fieldName;
        this.category = category;
        intern(this);
    }

    private synchronized static void intern(FieldKey fieldKey) {
        internMap.put(fieldKey.fieldKeyString, fieldKey);
    }

    /**
     *
     * @throws ParseException si s est incorrect
     */
    public static FieldKey parse(String s) throws ParseException {
        if (s.equals("texte")) {
            s = "section_texte";
        }
        if (s.equals("idcorpus")) {
            s = "id";
        }
        FieldKey current = internMap.get(s);
        if (current != null) {
            return current;
        }
        if (s.startsWith("propriete_")) {
            String fieldName = parseFieldName(s, 10);
            return new FieldKey(s, fieldName, PROPRIETE_CATEGORY);
        }
        if (s.startsWith("information_")) {
            String fieldName = parseFieldName(s, 12);
            return new FieldKey(s, fieldName, INFORMATION_CATEGORY);
        }
        if (s.startsWith("annexe_")) {
            String fieldName = parseFieldName(s, 7);
            return new FieldKey("section_" + fieldName, fieldName, SECTION_CATEGORY);
        }
        if (s.startsWith("section_")) {
            String fieldName = parseFieldName(s, 8);
            return new FieldKey(s, fieldName, SECTION_CATEGORY);
        }
        throw new ParseException("unkown fieldkey", 0);
    }

    public static FieldKey parse(short category, String fieldName) throws ParseException {
        StringUtils.checkTechnicalName(fieldName, false);
        return new FieldKey(categoryToString(category) + "_" + fieldName, fieldName, category);
    }

    /**
     * Équivalent de parse() mais en envoyant IllegalArgumentException plutôt
     * que ParseException. à utiliser quand on est sûr de la syntaxe et permet
     * d'éviter un try {} catch {}
     */
    public static FieldKey build(String s) {
        try {
            return parse(s);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe.getMessage());
        }
    }

    /**
     * Équivalent de parse() mais en envoyant IllegalArgumentException plutôt
     * que ParseException. à utiliser quand on est sûr de la syntaxe et permet
     * d'éviter un try {} catch {}
     */
    public static FieldKey build(short category, String fieldName) {
        try {
            return parse(category, fieldName);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe.getMessage());
        }
    }

    private static String parseFieldName(String s, int index) throws ParseException {
        String fieldName = s.substring(index);
        StringUtils.checkTechnicalName(fieldName, false);
        return fieldName;
    }

    public String getKeyString() {
        return fieldKeyString;
    }

    @Override
    public String toString() {
        return fieldKeyString;
    }

    public boolean isSpecial() {
        return (category == SPECIAL_CATEGORY);
    }

    public boolean isPropriete() {
        return (category == PROPRIETE_CATEGORY);
    }

    public boolean isInformation() {
        return (category == INFORMATION_CATEGORY);
    }

    public boolean isSection() {
        return (category == SECTION_CATEGORY);
    }

    public short getCategory() {
        return category;
    }

    /**
     * Retourne <em>true</em> si le champ est obligatoire dans une fiche.
     */
    public boolean isMandatoryField() {
        if (category != SPECIAL_CATEGORY) {
            return false;
        }
        if (fieldKeyString.equals(SPECIAL_SOUSTITRE)) {
            return false;
        }
        return true;
    }

    public String getFieldName() {
        return fieldName;
    }

    @Override
    public int hashCode() {
        return fieldKeyString.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FieldKey)) {
            return false;
        }
        FieldKey fk = (FieldKey) obj;
        return fk.fieldKeyString.equals(this.fieldKeyString);
    }

    /**
     * Retourne une expression sous forme de chaîne de la catégorie
     * correspondante.
     *
     * @throws IllegalArgumentException si la valeur de la catégorie est
     * incorrecte
     */
    public static String categoryToString(short category) {
        switch (category) {
            case SPECIAL_CATEGORY:
                return "special";
            case PROPRIETE_CATEGORY:
                return "propriete";
            case INFORMATION_CATEGORY:
                return "information";
            case SECTION_CATEGORY:
                return "section";
            default:
                throw new IllegalArgumentException("wrong Field category value");
        }
    }

    /**
     * Retourne une expression sous forme de chaîne de la catégorie
     * correspondante.
     *
     * @throws IllegalArgumentException si la valeur de la catégorie est
     * incorrecte
     */
    public static short categoryToShort(String s) {
        if (s.equals("special")) {
            return SPECIAL_CATEGORY;
        }
        if (s.equals("propriete")) {
            return PROPRIETE_CATEGORY;
        }
        if (s.equals("information")) {
            return INFORMATION_CATEGORY;
        }
        if (s.equals("section")) {
            return SECTION_CATEGORY;
        }
        throw new IllegalArgumentException("Wrong category string");
    }

}
