/* FichothequeLib_API - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.mapeadores.util.html.TrustedHtmlFactory;


/**
 *
 * @author Vincent Calame
 */
public interface ContentChecker {

    public TrustedHtmlFactory getTrustedHtmlFactory();

}
