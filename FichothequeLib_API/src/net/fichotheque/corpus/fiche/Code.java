/* FichothequeLib_API - Copyright (c) 2006-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public class Code extends AbstractList<Ln> implements ZoneBlock, RandomAccess, Serializable {

    private static final long serialVersionUID = 5L;
    public final static short TYPE_NONE = 0;
    public final static short TYPE_XML = 1;
    public final static short TYPE_SCRIPT = 2;
    private final ParagraphBlock numero = new ParagraphBlock();
    private final ParagraphBlock legende = new ParagraphBlock();
    private short type = TYPE_NONE;
    private Ln[] lnArray = new Ln[8];
    private int size = 0;
    private AttsImpl attsImpl;

    /**
     *
     * @throws IllegalArgumentException
     */
    public Code(short type) {
        if ((type < 0) || (type > 2)) {
            throw new IllegalArgumentException("wrong type value");
        }
        this.type = type;
    }

    public short getType() {
        return type;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Ln get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return lnArray[index];
    }

    @Override
    public boolean add(Ln ln) {
        if (ln == null) {
            throw new NullPointerException();
        }
        checkLength();
        lnArray[size] = ln;
        size++;
        return true;
    }

    @Override
    public TextContent getNumero() {
        return numero;
    }

    @Override
    public TextContent getLegende() {
        return legende;
    }

    @Override
    public TextContentBuilder getNumeroBuilder() {
        return numero;
    }

    @Override
    public TextContentBuilder getLegendeBuilder() {
        return legende;
    }

    @Override
    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

    private void checkLength() {
        if (size == lnArray.length) {
            Ln[] nv = new Ln[size * 2];
            System.arraycopy(lnArray, 0, nv, 0, size);
            lnArray = nv;
        }
    }

    public static short typeToShort(String strg_type) {
        if ((strg_type == null) || (strg_type.length() == 0)) {
            return TYPE_NONE;
        }
        if (strg_type.equals("xml")) {
            return TYPE_XML;
        }
        if (strg_type.equals("script")) {
            return TYPE_SCRIPT;
        }
        return TYPE_NONE;
    }

    public static String typeToString(short type) {
        switch (type) {
            case TYPE_XML:
                return "xml";
            case TYPE_SCRIPT:
                return "script";
            default:
                return "";
        }
    }

    static public char typeToInitiale(short type) {
        switch (type) {
            case TYPE_XML:
                return 'x';
            case TYPE_SCRIPT:
                return 's';
            default:
                return '0';
        }
    }

    static public short typeToShort(char initiale) {
        switch (initiale) {
            case 'x':
                return TYPE_XML;
            case 's':
                return TYPE_SCRIPT;
            default:
                return TYPE_NONE;
        }
    }

}
