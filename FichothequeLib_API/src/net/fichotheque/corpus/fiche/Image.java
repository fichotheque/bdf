/* FichothequeLib_API - Copyright (c) 2009-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.mapeadores.util.text.StringUtils;
import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
public final class Image implements FicheItem, Serializable {

    private static final long serialVersionUID = 2L;
    private String src = "";
    private String alt = "";
    private String title = "";

    public Image(String src, String alt, String title) {
        if (src == null) {
            throw new IllegalArgumentException("href argument is null");
        }
        if (alt == null) {
            throw new IllegalArgumentException("alt argument is null");
        }
        if (title == null) {
            throw new IllegalArgumentException("title argument is null");
        }
        this.src = StringUtils.cleanString(src);
        this.alt = StringUtils.cleanString(alt);
        this.title = StringUtils.cleanString(title);

    }

    /**
     * N'est jamais nul.
     */
    public String getSrc() {
        return src;
    }

    /**
     * N'est jamais nul.
     */
    public String getAlt() {
        return alt;
    }

    /**
     * N'est jamais nul.
     */
    public String getTitle() {
        return title;
    }

    public boolean hasSrcOnly() {
        return (alt.length() == 0) && (title.length() == 0);
    }

    @Override
    public int hashCode() {
        return src.hashCode() + alt.hashCode() + title.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Image)) {
            return false;
        }
        Image other = (Image) obj;
        return (other.src.equals(this.src)) && (other.alt.equals(this.alt)) && (other.title.equals(this.title));
    }

}
