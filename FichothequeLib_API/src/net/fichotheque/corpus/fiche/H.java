/* FichothequeLib_API - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 *
 * @author Vincent Calame
 */
public class H extends ParagraphBlock implements FicheBlock {

    private static final long serialVersionUID = 5L;
    private int level;

    /**
     * @throws IllegalArgumentException si level < 1
     */
    public H(int level) {
        if (level < 1) {
            throw new IllegalArgumentException("level < 1");
        }
        this.level = level;
    }

    /**
     * Retourne le niveau du titre.
     */
    public int getLevel() {
        return level;
    }

}
