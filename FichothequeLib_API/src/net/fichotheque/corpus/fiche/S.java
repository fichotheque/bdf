/* FichothequeLib_API - Copyright (c) 2006-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
public class S implements Serializable, AttConsumer {

    private static final long serialVersionUID = 6L;
    public final static short EMPHASIS = 1;
    public final static short STRONG = 2;
    public final static short LINK = 3;
    public final static short EMSTRG = 4;
    public final static short IREF = 5;
    public final static short ANCHOR = 6;
    public final static short IMAGE = 7;
    public final static short FICHE = 8;
    public final static short CITE = 9;
    public final static short DEFINITION = 10;
    public final static short SAMPLE = 11;
    public final static short KEYBOARD = 12;
    public final static short VAR = 13;
    public final static short ABBR = 14;
    public final static short SPAN = 16;
    public final static short DELETE = 17;
    public final static short CODE = 18;
    public final static short INSERT = 19;
    public final static short BR = 20;
    public final static short SUP = 21;
    public final static short SUB = 22;
    public final static short QUOTE = 23;
    public final static short MOTCLE = 24;
    private final short type;
    private AttsImpl attsImpl;
    private String value = "";
    private String ref = "";

    /**
     *
     * @param type type de l'élément S
     * @throws IllegalArgumentException si type n'est pas connu
     */
    public S(short type) {
        if (typeToString(type).isEmpty()) {
            throw new IllegalArgumentException("Unknown type: " + type);
        }
        this.type = type;
    }

    public short getType() {
        return type;
    }

    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    public String getRef() {
        return ref;
    }

    public String getValue() {
        return value;
    }

    public String getSyntax() {
        return AttsImpl.getSyntax(attsImpl);
    }

    public char getTypeInitial() {
        return typeToInitial(type);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

    public void setValue(String s) {
        if (s == null) {
            this.value = "";
        } else {
            this.value = s;
        }
    }

    public void setRef(String ref) {
        if (ref != null) {
            this.ref = ref;
        } else {
            this.ref = "";
        }
    }

    @Override
    public String toString() {
        return value;
    }

    public static String typeToString(short type) {
        switch (type) {
            case EMPHASIS:
                return "em";
            case STRONG:
                return "strg";
            case EMSTRG:
                return "emstrg";
            case LINK:
                return "link";
            case ANCHOR:
                return "anchor";
            case IREF:
                return "iref";
            case IMAGE:
                return "img";
            case FICHE:
                return "fiche";
            case CITE:
                return "cite";
            case DEFINITION:
                return "dfn";
            case SAMPLE:
                return "samp";
            case KEYBOARD:
                return "kbd";
            case VAR:
                return "var";
            case ABBR:
                return "abbr";
            case SPAN:
                return "span";
            case DELETE:
                return "del";
            case INSERT:
                return "ins";
            case BR:
                return "br";
            case SUP:
                return "sup";
            case SUB:
                return "sub";
            case CODE:
                return "code";
            case QUOTE:
                return "quote";
            case MOTCLE:
                return "motcle";
            default:
                return "";
        }
    }

    public static short typeToShort(String typeString) {
        if (typeString.equals("em")) {
            return EMPHASIS;
        }
        if (typeString.equals("strg")) {
            return STRONG;
        }
        if (typeString.equals("emstrg")) {
            return EMSTRG;
        }
        if (typeString.equals("link")) {
            return LINK;
        }
        if (typeString.equals("note")) {
            return IREF;
        }
        if (typeString.equals("iref")) {
            return IREF;
        }
        if (typeString.equals("anchor")) {
            return ANCHOR;
        }
        if (typeString.equals("img")) {
            return IMAGE;
        }
        if (typeString.equals("fiche")) {
            return FICHE;
        }
        if (typeString.equals("cite")) {
            return CITE;
        }
        if (typeString.equals("dfn")) {
            return DEFINITION;
        }
        if (typeString.equals("samp")) {
            return SAMPLE;
        }
        if (typeString.equals("kbd")) {
            return KEYBOARD;
        }
        if (typeString.equals("var")) {
            return VAR;
        }
        if ((typeString.equals("abbr")) || (typeString.equals("acronym"))) {
            return ABBR;
        }
        if (typeString.equals("span")) {
            return SPAN;
        }
        if (typeString.equals("code")) {
            return CODE;
        }
        if (typeString.equals("ins")) {
            return INSERT;
        }
        if (typeString.equals("del")) {
            return DELETE;
        }
        if (typeString.equals("quote")) {
            return QUOTE;
        }
        if (typeString.equals("motcle")) {
            return MOTCLE;
        }
        if (typeString.equals("br")) {
            return BR;
        }
        if (typeString.equals("sup")) {
            return SUP;
        }
        if (typeString.equals("sub")) {
            return SUB;
        }
        throw new IllegalArgumentException("Unknown type: " + typeString);
    }

    /**
     * Propose un code d'un caractère pour chaque élément.
     */
    private static char typeToInitial(short type) {
        switch (type) {
            case BR:
                return '%';
            case ANCHOR:
                return '#';
            case IREF:
                return '>';
            case SUP:
                return '^';
            case KEYBOARD:
                return '$';
            case SUB:
                return '_';
            case CITE:
                return '@';
            case LINK:
                return 'a';
            case STRONG:
                return 'b';
            case CODE:
                return '`';
            case DEFINITION:
                return '?';
            case EMPHASIS:
                return 'e';
            case FICHE:
                return 'f';
            case IMAGE:
                return 'i';
            case MOTCLE:
                return 'm';
            case QUOTE:
                return ':';
            case SAMPLE:
                return '<';
            case ABBR:
                return '=';
            case INSERT:
                return '+';
            case VAR:
                return '&';
            case EMSTRG:
                return 'w';
            case DELETE:
                return '-';
            case SPAN:
                return '*';
            default:
                throw new IllegalArgumentException("Wrong type value");
        }
    }

    public static short initialToType(char carac) {
        switch (carac) {
            case '%':
                return BR;
            case '#':
                return ANCHOR;
            case '>':
                return IREF;
            case '^':
                return SUP;
            case '$':
                return KEYBOARD;
            case '_':
                return SUB;
            case '@':
                return CITE;
            case 'a':
                return LINK;
            case 'b':
                return STRONG;
            case '`':
            case 'c':
                return CODE;
            case '?':
                return DEFINITION;
            case 'e':
                return EMPHASIS;
            case 'f':
                return FICHE;
            case 'i':
                return IMAGE;
            case 'm':
                return MOTCLE;
            case ':':
            case 'q':
                return QUOTE;
            case '<':
                return SAMPLE;
            case '=':
                return ABBR;
            case '+':
                return INSERT;
            case '&':
            case 'v':
                return VAR;
            case 'w':
                return EMSTRG;
            case '-':
                return DELETE;
            case '*':
                return SPAN;
            default:
                throw new IllegalArgumentException("Wrong carac value");
        }
    }

    public static boolean isSpecialRef(short type) {
        switch (type) {
            case ANCHOR:
            case IREF:
            case IMAGE:
            case FICHE:
            case BR:
            case MOTCLE:
                return true;
            default:
                return false;
        }
    }


    public static boolean isTypoType(short type) {
        switch (type) {
            case S.EMPHASIS:
            case S.STRONG:
            case S.EMSTRG:
            case S.SUP:
            case S.SUB:
            case S.DELETE:
            case S.INSERT:
            case S.QUOTE:
            case S.ABBR:
            case S.IMAGE:
            case S.CITE:
            case S.DEFINITION:
            case S.LINK:
            case S.FICHE:
            case S.MOTCLE:
            case S.ANCHOR:
            case S.IREF:
            case S.SPAN:
                return true;
            default:
                return false;
        }
    }

    public static boolean isRefDefaultValue(short type) {
        switch (type) {
            case S.ANCHOR:
            case S.IREF:
                return true;
            default:
                return false;
        }
    }

    public static boolean isIref(short type) {
        switch (type) {
            case S.ANCHOR:
            case S.IREF:
                return true;
            default:
                return false;
        }
    }

}
