/* FichothequeLib_API - Copyright (c) 2006-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class Item implements FicheItem, Serializable {

    private static final long serialVersionUID = 2L;
    private final String value;

    /**
     * Construit un nouvel objet Item. Valeur ne peut pas être null
     *
     *@throws IllegalArgumentException si value est nul.
     */
    public Item(String valeur) {
        if (valeur == null) {
            throw new IllegalArgumentException("valeur argument is null");
        }
        this.value = StringUtils.cleanString(valeur);
    }

    @Override
    public String toString() {
        return value;
    }

    /**
     * Retourne la valeur de l'Item.
     */
    public String getValue() {
        return value;
    }

    /**
     * Deux items sont égaux s'ils ont la même valeur.
     **/
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Item)) {
            return false;
        }
        Item other = (Item) obj;
        return value.equals(other.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

}
