/* FichothequeLib_API - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 *
 * @author Vincent Calame
 */
public class P extends ParagraphBlock implements FicheBlock {

    private static final long serialVersionUID = 5L;
    public static final short STANDARD = 0;
    public static final short CITATION = 1;
    public static final short NOTE = 2;
    public static final short QUESTION = 3;
    public static final short REMARK = 4;
    private short type;
    private String source = "";

    public P() {
        this.type = STANDARD;
    }

    /**
     * @throws IllegalArgumentException
     */
    public P(short type) {
        if ((type < 0) || (type > 4)) {
            throw new IllegalArgumentException("wrong type value");
        }
        if (type == NOTE) {
            type = STANDARD;
        }
        this.type = type;
    }

    @Override
    public void addS(S s) {
        super.addS(s);
        if ((partList.size() == 1) && (type == STANDARD)) {
            if (s.getType() == S.ANCHOR) {
                if (s.getValue().length() > 0) {
                    String variant = s.getAtts().getValue("variant");
                    if (variant == null) {
                        type = NOTE;
                    }
                }
            }
        }
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        if (source == null) {
            this.source = "";
        } else {
            this.source = source;
        }
    }

    public short getType() {
        return type;
    }

    public static String typeToString(short type) {
        switch (type) {
            case QUESTION:
                return "question";
            case CITATION:
                return "citation";
            case NOTE:
                return "note";
            case REMARK:
                return "remarque";
            default:
                return "";
        }
    }

    public static short typeToShort(String chtype) {
        if (chtype == null) {
            return STANDARD;
        }
        if (chtype.equals("citation")) {
            return CITATION;
        }
        if (chtype.equals("note")) {
            return NOTE;
        }
        if (chtype.equals("question")) {
            return QUESTION;
        }
        if (chtype.equals("remarque")) {
            return REMARK;
        }
        if (chtype.equals("cit")) {
            return CITATION; //compatibilité avec une ancienne version
        }
        return STANDARD;
    }

}
