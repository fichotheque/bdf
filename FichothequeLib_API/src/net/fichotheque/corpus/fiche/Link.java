/* FichothequeLib_API - Copyright (c) 2006-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.mapeadores.util.text.StringUtils;
import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
public final class Link implements FicheItem, Serializable {

    private static final long serialVersionUID = 2L;
    private String href = "";
    private String title = "";
    private String comment = "";

    public Link(String href, String title, String comment) {
        if (href == null) {
            throw new IllegalArgumentException("href argument is null");
        }
        if (title == null) {
            throw new IllegalArgumentException("title argument is null");
        }
        if (comment == null) {
            throw new IllegalArgumentException("comment argument is null");
        }
        this.href = StringUtils.cleanString(href);
        this.title = StringUtils.cleanString(title);
        this.comment = StringUtils.cleanString(comment);
    }

    /**
     * N'est jamais nul.
     */
    public String getHref() {
        return href;
    }

    /**
     * N'est jamais nul.
     */
    public String getTitle() {
        return title;
    }

    /**
     * N'est jamais nul.
     */
    public String getComment() {
        return comment;
    }

    public boolean hasHrefOnly() {
        return (title.length() == 0) && (comment.length() == 0);
    }

    @Override
    public String toString() {
        return "link: " + href;
    }

    @Override
    public int hashCode() {
        return href.hashCode() + title.hashCode() + comment.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Link)) {
            return false;
        }
        Link other = (Link) obj;
        return (other.href.equals(this.href)) && (other.title.equals(this.title)) && (other.comment.equals(this.comment));
    }

}
