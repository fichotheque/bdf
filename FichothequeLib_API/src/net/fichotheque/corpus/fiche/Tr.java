/* FichothequeLib_API - Copyright (c) 2006-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public class Tr extends AbstractList<Td> implements RandomAccess, AttConsumer, Serializable {

    private static final long serialVersionUID = 6L;
    private AttsImpl attsImpl;
    private Td[] tdArray = new Td[4];
    private int size = 0;

    public Tr() {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Td get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return tdArray[index];
    }

    @Override
    public boolean add(Td td) {
        if (td == null) {
            throw new NullPointerException();
        }
        checkLength();
        tdArray[size] = td;
        size++;
        return true;
    }

    private void checkLength() {
        if (size == tdArray.length) {
            Td[] nv = new Td[size * 2];
            System.arraycopy(tdArray, 0, nv, 0, size);
            tdArray = nv;
        }
    }

    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

}
