/* FichothequeLib_API - Copyright (c) 2006-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.models.PersonCoreUtils;
import net.mapeadores.util.text.StringUtils;


/**
 * Il y a deux types de personnes, celles construite avec une clé de rédacteur
 * et celle construite directement avec une instance de PersonneCore.
 *
 * @author Vincent Calame
 */
public final class Personne implements FicheItem, Serializable {

    private static final long serialVersionUID = 4L;
    private InternalPersonCore personCore;
    private String redacteurGlobalId;
    private String organism;

    public Personne(String redacteurGlobalId) {
        if (redacteurGlobalId == null) {
            throw new IllegalArgumentException("redacteurGlobalId is null");
        }
        this.redacteurGlobalId = redacteurGlobalId;
    }

    /**
     */
    public Personne(PersonCore personCore, String organism) {
        if (personCore == null) {
            throw new IllegalArgumentException("personCore is null");
        }
        this.personCore = new InternalPersonCore(personCore);
        organism = StringUtils.cleanString(organism);
        if ((organism != null) && (organism.length() == 0)) {
            organism = null;
        }
        this.organism = organism;
    }

    public String getRedacteurGlobalId() {
        return redacteurGlobalId;
    }

    public PersonCore getPersonCore() {
        if (personCore == null) {
            throw new UnsupportedOperationException();
        }
        return personCore;
    }

    /**
     * Retourne l'organisme de la personne.
     */
    public String getOrganism() {
        return organism;
    }

    @Override
    public String toString() {
        if (redacteurGlobalId != null) {
            return redacteurGlobalId;
        } else {
            return personCore.toBiblioStyle(false);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Personne)) {
            return false;
        }
        Personne other = (Personne) obj;
        if (other.redacteurGlobalId != null) {
            if (this.redacteurGlobalId == null) {
                return false;
            }
            return other.redacteurGlobalId.equals(this.redacteurGlobalId);
        } else {
            if (this.redacteurGlobalId != null) {
                return false;
            }
            boolean result = PersonCoreUtils.areEqual(this.personCore, other.personCore);
            if (!result) {
                return false;
            }
            if (other.organism == null) {
                return (this.organism == null);
            } else {
                if (this.organism == null) {
                    return false;
                }
                return other.organism.equals(this.organism);
            }
        }
    }

    @Override
    public int hashCode() {
        if (redacteurGlobalId != null) {
            return redacteurGlobalId.hashCode();
        }
        return personCore.getSurname().hashCode() + personCore.getForename().hashCode();
    }


    private static class InternalPersonCore implements PersonCore, Serializable {

        private final String surname;
        private final String forename;
        private final String nonlatin;
        private final boolean surnameFirst;

        private InternalPersonCore(PersonCore personCore) {
            this.surname = personCore.getSurname();
            this.forename = personCore.getForename();
            this.nonlatin = personCore.getNonlatin();
            this.surnameFirst = personCore.isSurnameFirst();
        }

        @Override
        public String getSurname() {
            return surname;
        }

        @Override
        public String getForename() {
            return forename;
        }

        @Override
        public String getNonlatin() {
            return nonlatin;
        }

        @Override
        public boolean isSurnameFirst() {
            return surnameFirst;
        }

    }

}
