/* FichothequeLib_API - Copyright (c) 2007-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 *
 * @author Vincent Calame
 */
public interface ZoneBlock extends FicheBlock {

    public TextContent getNumero();

    public TextContent getLegende();

    public TextContentBuilder getNumeroBuilder();

    public TextContentBuilder getLegendeBuilder();

}
