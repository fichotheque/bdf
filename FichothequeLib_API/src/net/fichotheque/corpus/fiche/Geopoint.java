/* FichothequeLib_API - Copyright (c) 2008-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.mapeadores.util.primitives.DegreDecimal;
import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
public final class Geopoint implements FicheItem, Serializable {

    private static final long serialVersionUID = 2L;
    private DegreDecimal latitude;
    private DegreDecimal longitude;

    public Geopoint(DegreDecimal latitude, DegreDecimal longitude) {
        if (latitude == null) {
            throw new IllegalArgumentException("latitude is null");
        }
        if (longitude == null) {
            throw new IllegalArgumentException("longitude is null");
        }
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public DegreDecimal getLatitude() {
        return latitude;
    }

    public DegreDecimal getLongitude() {
        return longitude;
    }

}
