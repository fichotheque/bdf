/* FichothequeLib_API - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
class AttsImpl implements Atts, Serializable {

    private static final long serialVersionUID = 1L;
    public final static Atts EMPTY_ATTS = new EmptyAtts();
    private int size = 0;
    private String[] nameArray = new String[4];
    private String[] valueArray = new String[4];

    AttsImpl() {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public String getName(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return nameArray[index];
    }

    @Override
    public String getValue(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return valueArray[index];
    }


    void put(String name, String value) {
        if (value == null) {
            value = "";
        }
        for (int i = 0; i < size; i++) {
            if (nameArray[i].equals(name)) {
                valueArray[i] = value;
                return;
            }
        }
        checkLength();
        nameArray[size] = name;
        valueArray[size] = value;
        size++;
    }

    private void checkLength() {
        if (size == nameArray.length) {
            String[] nvName = new String[size * 2];
            System.arraycopy(nameArray, 0, nvName, 0, size);
            this.nameArray = nvName;
            String[] nvValue = new String[size * 2];
            System.arraycopy(valueArray, 0, nvValue, 0, size);
            this.valueArray = nvValue;
        }
    }

    private String getSyntax() {
        for (int i = 0; i < size; i++) {
            switch (nameArray[i]) {
                case "mode":
                case "syntax":
                    return valueArray[i];
            }
        }
        return "";
    }

    static String getSyntax(AttsImpl impl) {
        if (impl == null) {
            return "";
        } else {
            return impl.getSyntax();
        }
    }

    static Atts check(AttsImpl impl) {
        if (impl == null) {
            return EMPTY_ATTS;
        } else {
            return impl;
        }
    }

    static AttsImpl put(AttsImpl impl, String name, String value) {
        if (impl == null) {
            impl = new AttsImpl();
        }
        impl.put(name, value);
        return impl;
    }


    private static class EmptyAtts implements Atts {

        private EmptyAtts() {

        }

        @Override
        public int size() {
            return 0;
        }

        @Override
        public String getName(int index) {
            throw new IndexOutOfBoundsException();
        }

        @Override
        public String getValue(int index) {
            throw new IndexOutOfBoundsException();
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

    }

}
