/* FichothequeLib_API - Copyright (c) 2008-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.mapeadores.util.primitives.Decimal;
import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
public final class Nombre implements FicheItem, Serializable {

    private static final long serialVersionUID = 2L;
    private Decimal decimal;

    public Decimal getDecimal() {
        return decimal;
    }

    public Nombre(Decimal decimal) {
        if (decimal == null) {
            throw new IllegalArgumentException("decimal argument is null");
        }
        this.decimal = decimal;
    }

    @Override
    public int hashCode() {
        return decimal.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Nombre)) {
            return false;
        }
        Nombre other = (Nombre) obj;
        return other.decimal.equals(this.decimal);
    }

    @Override
    public String toString() {
        return decimal.toString();
    }

}
