/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class Fiche implements FicheAPI, Serializable {

    private static final long serialVersionUID = 13L;
    private static final List<Propriete> EMPTY_PROPRIETELIST = Collections.emptyList();
    private static final List<Information> EMPTY_INFORMATIONLIST = Collections.emptyList();
    private static final List<Section> EMPTY_SECTIONLIST = Collections.emptyList();
    private final Map<String, Object> fieldMap = new HashMap<String, Object>();
    private ArrayList<Information> informationList;
    private ArrayList<Propriete> proprieteList;
    private ArrayList<Section> sectionList;
    private FicheItemsImpl redacteurs = null;
    private String titre = "";
    private Para soustitre = null;
    private Lang lang = null;

    public Fiche() {
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    @Override
    public Lang getLang() {
        return lang;
    }

    /**
     * Ne retourne jamais <em>null</em>
     */
    @Override
    public String getTitre() {
        return titre;
    }

    /**
     *
     * @throws IllegalArgumentException si titre est nul
     */
    public void setTitre(String titre) {
        if (titre == null) {
            throw new IllegalArgumentException("argument is null");
        }
        this.titre = StringUtils.cleanString(titre);
    }

    /**
     * Peut retourner <em>null</em>
     */
    @Override
    public Para getSoustitre() {
        return soustitre;
    }

    /**
     *
     * @throws IllegalArgumentException si soustitre est nul
     */
    public void setSoustitre(Para soustitrePara) {
        if ((soustitrePara == null) || (soustitrePara.isEmpty())) {
            this.soustitre = null;
        } else {
            this.soustitre = soustitrePara;
        }
    }

    /**
     * <em>null</em> ou non vide
     */
    @Override
    public FicheItems getRedacteurs() {
        return redacteurs;
    }

    @Override
    public List<Propriete> getProprieteList() {
        if (proprieteList == null) {
            return EMPTY_PROPRIETELIST;
        }
        return proprieteList;
    }

    @Override
    public List<Information> getInformationList() {
        if (informationList == null) {
            return EMPTY_INFORMATIONLIST;
        }
        return informationList;
    }

    @Override
    public List<Section> getSectionList() {
        if (sectionList == null) {
            return EMPTY_SECTIONLIST;
        }
        return sectionList;
    }

    public void appendRedacteurs(FicheItems ficheItems) {
        setRedacteurs(ficheItems, true);
    }

    public void setRedacteurs(FicheItems ficheItems) {
        setRedacteurs(ficheItems, false);
    }

    private void setRedacteurs(FicheItems ficheItems, boolean append) {
        if ((ficheItems != null) && (ficheItems.isEmpty())) {
            ficheItems = null;
        }
        if (ficheItems == null) {
            if (!append) {
                redacteurs = null;
            }
            return;
        }
        if (redacteurs == null) {
            redacteurs = new FicheItemsImpl();
        } else if (!append) {
            redacteurs.clear();
        }
        for (FicheItem ficheItem : ficheItems) {
            if ((ficheItem instanceof Item) || (ficheItem instanceof Personne)) {
                redacteurs.add(ficheItem);
            }
        }
        if (redacteurs.isEmpty()) {
            redacteurs = null;
        }
    }

    /**
     *
     * @throws IllegalArgumentException si fieldKey.isInformationFieldKey() ==
     * false
     */
    @Override
    public Information getInformation(FieldKey fieldKey) {
        if (!fieldKey.isInformation()) {
            throw new IllegalArgumentException("fieldKey is not an InformationFieldKey");
        }
        return (Information) getField(fieldKey);
    }

    /**
     *
     * @throws IllegalArgumentException si fieldKey.isProprieteFieldKey() ==
     * false
     */
    @Override
    public Propriete getPropriete(FieldKey fieldKey) {
        if (!fieldKey.isPropriete()) {
            throw new IllegalArgumentException("fieldKey is not an ProprieteFieldKey");
        }
        return (Propriete) getField(fieldKey);
    }

    /**
     *
     * @throws IllegalArgumentException si fieldKey.isSectionFieldKey() == false
     */
    @Override
    public Section getSection(FieldKey fieldKey) {
        if (!fieldKey.isSection()) {
            throw new IllegalArgumentException("fieldKey is not an SectionFieldKey");
        }
        return (Section) getField(fieldKey);
    }

    public void appendInformation(FieldKey fieldKey, FicheItems ficheItems) {
        setInformation(fieldKey, ficheItems, true);
    }

    public void setInformation(FieldKey fieldKey, FicheItems ficheItems) {
        setInformation(fieldKey, ficheItems, false);
    }

    private void setInformation(FieldKey fieldKey, FicheItems ficheItems, boolean append) {
        if (!fieldKey.isInformation()) {
            throw new IllegalArgumentException("Not a Information FieldKey");
        }
        if ((ficheItems != null) && (ficheItems.isEmpty())) {
            ficheItems = null;
        }
        InternalInformation current = (InternalInformation) getField(fieldKey);
        if (current == null) {
            if (ficheItems == null) {
                return;
            }
            if (informationList == null) {
                informationList = new ArrayList<Information>();
            }
            InternalInformation information = new InternalInformation(fieldKey);
            information.addFicheItems(ficheItems);
            putField(fieldKey, information);
            informationList.add(information);
        } else {
            if (ficheItems == null) {
                if (!append) {
                    removeField(fieldKey);
                    informationList.remove(current);
                }
            } else {
                if (!append) {
                    current.clear();
                }
                current.addFicheItems(ficheItems);
            }
        }
    }

    public void appendSection(FieldKey fieldKey, FicheBlocks ficheBlocks) {
        setSection(fieldKey, ficheBlocks, true);
    }

    public void setSection(FieldKey fieldKey, FicheBlocks ficheBlocks) {
        setSection(fieldKey, ficheBlocks, false);
    }

    private void setSection(FieldKey fieldKey, FicheBlocks ficheBlocks, boolean append) {
        if (!fieldKey.isSection()) {
            throw new IllegalArgumentException("Not a Section FieldKey");
        }
        if ((ficheBlocks != null) && (ficheBlocks.isEmpty())) {
            ficheBlocks = null;
        }
        InternalSection current = (InternalSection) getField(fieldKey);
        if (current == null) {
            if (ficheBlocks == null) {
                return;
            }
            if (sectionList == null) {
                sectionList = new ArrayList<Section>();
            }
            InternalSection section = new InternalSection(fieldKey);
            section.addAll(ficheBlocks);
            putField(fieldKey, section);
            sectionList.add(section);
        } else {
            if (ficheBlocks == null) {
                if (!append) {
                    removeField(fieldKey);
                    sectionList.remove(current);
                }
            } else {
                if (!append) {
                    current.clear();
                }
                current.addAll(ficheBlocks);
            }
        }
    }

    public void setPropriete(FieldKey fieldKey, FicheItem ficheItem) {
        if (!fieldKey.isPropriete()) {
            throw new IllegalArgumentException("Not a Propriete FieldKey");
        }
        InternalPropriete current = (InternalPropriete) getField(fieldKey);
        if (current == null) {
            if (ficheItem == null) {
                return;
            }
            if (proprieteList == null) {
                proprieteList = new ArrayList<Propriete>();
            }
            InternalPropriete propriete = new InternalPropriete(fieldKey);
            propriete.setFicheItem(ficheItem);
            putField(fieldKey, propriete);
            proprieteList.add(propriete);
        } else {
            if (ficheItem == null) {
                removeField(fieldKey);
                proprieteList.remove(current);
            } else {
                current.setFicheItem(ficheItem);
            }
        }
    }

    public void clear() {
        titre = "";
        soustitre = null;
        redacteurs = null;
        if (informationList != null) {
            informationList.clear();
            informationList = null;
        }
        if (proprieteList != null) {
            proprieteList.clear();
            proprieteList = null;
        }
        if (sectionList != null) {
            sectionList.clear();
            sectionList = null;
        }
        clearFieldMap();
    }

    private Object getField(FieldKey fieldKey) {
        return fieldMap.get(fieldKey.getKeyString());
    }

    private void putField(FieldKey fieldKey, Object obj) {
        fieldMap.put(fieldKey.getKeyString(), obj);
    }

    private void removeField(FieldKey fieldKey) {
        fieldMap.remove(fieldKey.getKeyString());
    }

    private void clearFieldMap() {
        fieldMap.clear();
    }


    private static class InternalPropriete implements Propriete, Serializable {

        private final FieldKey fieldKey;
        private FicheItem item;

        private InternalPropriete(FieldKey fieldKey) {
            this.fieldKey = fieldKey;
        }

        @Override
        public FieldKey getFieldKey() {
            return fieldKey;
        }

        @Override
        public FicheItem getFicheItem() {
            return item;
        }

        private void setFicheItem(FicheItem ficheitem) {
            this.item = ficheitem;
        }

    }


    private static class InternalInformation extends FicheItemsImpl implements Information, Serializable {

        private final FieldKey fieldKey;

        private InternalInformation(FieldKey fieldKey) {
            this.fieldKey = fieldKey;
        }

        @Override
        public FieldKey getFieldKey() {
            return fieldKey;
        }

    }


    private static class InternalSection extends FicheBlocksImpl implements Section, Serializable {

        private final FieldKey fieldKey;

        private InternalSection(FieldKey fieldKey) {
            this.fieldKey = fieldKey;
        }

        @Override
        public FieldKey getFieldKey() {
            return fieldKey;
        }

    }

}
