/* FichothequeLib_API - Copyright (c) 2006-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 *
 * @author Vincent Calame
 */
public class Ln extends ParagraphBlock {

    private static final long serialVersionUID = 5L;
    private int indentation = 0;

    public Ln(String value) {
        super.addText(value);
    }

    public Ln(String value, int indentation) {
        super.addText(value);
        if (indentation < 0) {
            throw new IllegalArgumentException("identation < 0");
        }
        this.indentation = indentation;
    }

    /**
     * @throws UnsupportedOperationException
     */
    @Override
    public void addS(S s) {
        throw new UnsupportedOperationException();
    }

    /**
     * @throws UnsupportedOperationException
     */
    @Override
    public void addText(String s) {
        throw new UnsupportedOperationException();
    }

    public String getValue() {
        return (String) partList.get(0);
    }

    public int getIndentation() {
        return indentation;
    }

    public void setIndentation(int indentation) {
        if (indentation < 0) {
            throw new IllegalArgumentException("identation < 0");
        }
        this.indentation = indentation;
    }

}
