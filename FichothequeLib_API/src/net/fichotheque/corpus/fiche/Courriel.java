/* FichothequeLib_API - Copyright (c) 2006-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.EmailCoreUtils;


/**
 * Un courriel est composé d'une adresse et d'un nom complet.
 *
 * @author Vincent Calame
 */
public final class Courriel implements FicheItem, Serializable {

    private static final long serialVersionUID = 4L;
    private InternalEmailCore emailCore;

    public Courriel(EmailCore emailCore) {
        this.emailCore = new InternalEmailCore(emailCore);
    }

    public Courriel(Courriel courriel, String realName) {
        if (realName == null) {
            realName = "";
        }
        this.emailCore = new InternalEmailCore(courriel.emailCore.addrSpec, realName);
    }

    public EmailCore getEmailCore() {
        return emailCore;
    }

    @Override
    public String toString() {
        return emailCore.toCompleteString();
    }

    @Override
    public int hashCode() {
        return emailCore.getAddrSpec().hashCode() + emailCore.getRealName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Courriel)) {
            return false;
        }
        return EmailCoreUtils.areEqual(((Courriel) obj).emailCore, this.emailCore);
    }


    private static class InternalEmailCore implements EmailCore, Serializable {

        private final String addrSpec;
        private final String realName;

        private InternalEmailCore(String addrSpec, String realName) {
            this.addrSpec = addrSpec;
            this.realName = realName;
        }

        private InternalEmailCore(EmailCore emailCore) {
            this.addrSpec = emailCore.getAddrSpec();
            this.realName = emailCore.getRealName();
        }

        @Override
        public String getRealName() {
            return realName;
        }

        @Override
        public String getAddrSpec() {
            return addrSpec;
        }

    }

}
