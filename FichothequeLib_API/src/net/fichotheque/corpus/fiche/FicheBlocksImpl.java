/* FichothequeLib_API - Copyright (c) 2006-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Arrays;


/**
 *
 * @author Vincent Calame
 */
class FicheBlocksImpl extends AbstractList<FicheBlock> implements FicheBlocks, Serializable {

    private static final long serialVersionUID = 3L;
    private FicheBlock[] blocks = new FicheBlock[8];
    private int size = 0;

    FicheBlocksImpl() {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public FicheBlock get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return blocks[index];
    }

    @Override
    public void clear() {
        Arrays.fill(blocks, null);
        size = 0;
    }

    @Override
    public boolean add(FicheBlock ficheBlock) {
        if (ficheBlock == null) {
            throw new NullPointerException();
        }
        if (!isValidFicheBlock(ficheBlock)) {
            return false;
        }
        checkLength();
        blocks[size] = ficheBlock;
        size++;
        return true;
    }

    public boolean isValidFicheBlock(FicheBlock ficheBlock) {
        return true;
    }

    public void addAll(FicheBlocks ficheBlocks) {
        int othersize = ficheBlocks.size();
        checkLength(othersize);
        for (int i = 0; i < othersize; i++) {
            FicheBlock ficheBlock = ficheBlocks.get(i);
            add(ficheBlock);
        }
    }

    private void checkLength(int addlength) {
        if (size + addlength >= blocks.length) {
            FicheBlock[] nv = new FicheBlock[size + addlength];
            System.arraycopy(blocks, 0, nv, 0, size);
            blocks = nv;
        }
    }

    private void checkLength() {
        if (size == blocks.length) {
            FicheBlock[] nv = new FicheBlock[size * 2];
            System.arraycopy(blocks, 0, nv, 0, size);
            blocks = nv;
        }
    }

}
