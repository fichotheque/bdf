/* FichothequeLib_API - Copyright (c) 2006-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 *
 * @author Vincent Calame
 */
public class Td extends ParagraphBlock {

    private static final long serialVersionUID = 6L;
    public static final short STANDARD = 1;
    public static final short HEADER = 2;
    private final short type;

    public Td() {
        this.type = STANDARD;
    }

    public Td(short type) {
        switch (type) {
            case STANDARD:
            case HEADER:
                this.type = type;
                break;
            default:
                this.type = STANDARD;
        }
    }

    public boolean isNumber() {
        if (partList.isEmpty()) {
            return false;
        }
        Object firstPart = partList.get(0);
        String value;
        if (firstPart instanceof String) {
            value = (String) firstPart;
        } else {
            value = ((S) firstPart).getValue();
        }
        if (value.isEmpty()) {
            return false;
        }
        char carac = value.charAt(0);
        if (Character.isDigit(carac)) {
            return true;
        }
        switch (carac) {
            case '-':
            case '$':
            case '£':
                return true;
            default:
                return false;
        }
    }

    public short getType() {
        return type;
    }

    public static short typeToShort(String strg_type) {
        if ((strg_type == null) || (strg_type.length() == 0)) {
            return STANDARD;
        }
        if (strg_type.equals("header")) {
            return HEADER;
        }
        return STANDARD;
    }

    public static String typeToString(short type) {
        switch (type) {
            case HEADER:
                return "header";
            default:
                return "";
        }
    }

}
