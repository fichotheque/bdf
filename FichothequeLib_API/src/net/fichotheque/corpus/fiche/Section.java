/* FichothequeLib_API - Copyright (c) 2006-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.fichotheque.corpus.metadata.FieldKey;


/**
 * Une instance de section est un champ contenant plusieurs blocs de texte. Une
 * section doit contenir toujours au moins un bloc.
 *
 * @author Vincent Calame
 */
public interface Section extends FicheBlocks {

    /**
     * Retourne la clé du champ correspondant à la section. Il est bien entendu
     * du type section.
     *
     * @return clé du champ
     */
    public FieldKey getFieldKey();

}
