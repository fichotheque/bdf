/* FichothequeLib_API - Copyright (c) 2006-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.fichotheque.corpus.metadata.FieldKey;


/**
 * Une propriété contient un objet FicheItem et un seul.
 *
 * @author Vincent Calame
 */
public interface Propriete {

    /**
     * Retourne la clé du champ correspondant à la propriété. Il est bien
     * entendu du type propriete.
     *
     * @return clé du champ
     */
    public FieldKey getFieldKey();


    /**
     * Retourne l'instance de FicheItem contenu dans la propriété. N'est jamais
     * nul.
     *
     * @return contenu de la propriété
     */
    public FicheItem getFicheItem();

}
