/* FichothequeLib_API - Copyright (c) 2007-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public class Insert implements ZoneBlock, Serializable {

    private static final long serialVersionUID = 6L;
    public final static short IMAGE_TYPE = 1;
    public final static short LETTRINE_TYPE = 2;
    public final static short AUDIO_TYPE = 3;
    public final static short POSITION_UNDETERMINED = 0;
    public final static short POSITION_LEFT = 1;
    public final static short POSITION_RIGHT = 2;
    private final ParagraphBlock numero = new ParagraphBlock();
    private final ParagraphBlock legende = new ParagraphBlock();
    private short type;
    private int width = -1;
    private int height = -1;
    private String src = "";
    private String ref = "";
    private SubsetKey subsetKey = null;
    private int id = 0;
    private String albumDimName = null;
    private short position = POSITION_UNDETERMINED;
    private final ParagraphBlock alt = new ParagraphBlock();
    private final ParagraphBlock credit = new ParagraphBlock();
    private AttsImpl attsImpl;

    public Insert(short type) {
        if ((type < 1) && (type > 2)) {
            throw new IllegalArgumentException("wrong type value");
        }
        this.type = type;
        if (type == LETTRINE_TYPE) {
            position = POSITION_LEFT;
        }
    }

    public short getType() {
        return type;
    }

    public String getSrc() {
        return src;
    }

    public String getRef() {
        return ref;
    }

    public TextContent getAlt() {
        return alt;
    }

    public TextContentBuilder getAltBuilder() {
        return alt;
    }

    public TextContent getCredit() {
        return credit;
    }

    public TextContentBuilder getCreditBuilder() {
        return credit;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public short getPosition() {
        return position;
    }

    public void setWidth(int width) {
        if (width < 0) {
            this.width = -1;
        } else {
            this.width = width;
        }
    }

    public void setHeight(int height) {
        if (height < 0) {
            this.height = -1;
        } else {
            this.height = height;
        }
    }

    public void setPosition(short position) {
        if (type != LETTRINE_TYPE) {
            return;
        }
        if (position != POSITION_RIGHT) {
            this.position = POSITION_LEFT;
        } else {
            this.position = POSITION_RIGHT;
        }
    }

    public void setSrc(String src) {
        if (src == null) {
            this.src = "";
        } else {
            this.src = src;
        }
    }

    public void setRef(String ref) {
        if (ref == null) {
            this.ref = "";
        } else {
            this.ref = ref;
        }
    }

    public void setSubsetItem(SubsetKey subsetKey, int id, String albumDimName) {
        this.subsetKey = subsetKey;
        this.id = id;
        if (albumDimName == null) {
            this.albumDimName = "";
        } else {
            this.albumDimName = albumDimName;
        }
    }

    public SubsetKey getSubsetKey() {
        return subsetKey;
    }

    public int getId() {
        return id;
    }

    public String getAlbumDimName() {
        return albumDimName;
    }

    @Override
    public TextContent getNumero() {
        return numero;
    }

    @Override
    public TextContent getLegende() {
        return legende;
    }

    @Override
    public TextContentBuilder getNumeroBuilder() {
        return numero;
    }

    @Override
    public TextContentBuilder getLegendeBuilder() {
        return legende;
    }

    @Override
    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

    public static String typeToString(short type) {
        switch (type) {
            case IMAGE_TYPE:
                return "image";
            case LETTRINE_TYPE:
                return "lettrine";
            case AUDIO_TYPE:
                return "audio";
            default:
                throw new IllegalArgumentException("wrong type value");
        }
    }

    public static char typeToInitiale(short type) {
        switch (type) {
            case IMAGE_TYPE:
                return 'i';
            case LETTRINE_TYPE:
                return 'l';
            case AUDIO_TYPE:
                return 'a';
            default:
                throw new IllegalArgumentException("wrong type value");
        }
    }

    public static String positionToString(short position) {
        switch (position) {
            case POSITION_LEFT:
                return "left";
            case POSITION_RIGHT:
                return "right";
            case POSITION_UNDETERMINED:
                return "";
            default:
                throw new IllegalArgumentException("wrong position value");
        }
    }

    public static char positionToInitiale(short position) {
        switch (position) {
            case POSITION_LEFT:
                return 'l';
            case POSITION_RIGHT:
                return 'r';
            case POSITION_UNDETERMINED:
                return '0';
            default:
                throw new IllegalArgumentException("wrong position value");
        }
    }

    public static short typeToShort(String typeString) {
        switch (typeString) {
            case "image":
                return IMAGE_TYPE;
            case "lettrine":
                return LETTRINE_TYPE;
            case "audio":
                return AUDIO_TYPE;
            default:
                throw new IllegalArgumentException("wrong type value");
        }
    }

    public static short typeToShort(char initiale) {
        switch (initiale) {
            case 'i':
                return IMAGE_TYPE;
            case 'l':
                return LETTRINE_TYPE;
            case 'a':
                return AUDIO_TYPE;
            default:
                return IMAGE_TYPE;
        }
    }

    public static short positionToShort(char initiale) {
        switch (initiale) {
            case 'r':
                return POSITION_RIGHT;
            case 'l':
                return POSITION_LEFT;
            default:
                return POSITION_UNDETERMINED;
        }
    }

    public static short positionToString(String typeString) {
        switch (typeString) {
            case "left":
                return POSITION_LEFT;
            case "right":
                return POSITION_RIGHT;
            default:
                return POSITION_UNDETERMINED;
        }
    }

    public static boolean isIllustrationType(short type) {
        switch (type) {
            case IMAGE_TYPE:
            case LETTRINE_TYPE:
                return true;
            default:
                return false;
        }
    }

    public static boolean isDocumentType(short type) {
        switch (type) {
            case AUDIO_TYPE:
                return true;
            default:
                return false;
        }
    }

}
