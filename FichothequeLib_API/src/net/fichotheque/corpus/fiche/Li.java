/* FichothequeLib_API - Copyright (c) 2006-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 *
 * @author Vincent Calame
 */
public class Li extends FicheBlocksImpl implements AttConsumer {

    private static final long serialVersionUID = 4L;
    private AttsImpl attsImpl;

    public Li(P firstP) {
        if (firstP == null) {
            throw new IllegalArgumentException("firstP is null");
        }
        add(firstP);
    }

    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

    @Override
    public boolean isValidFicheBlock(FicheBlock ficheBlock) {
        if ((ficheBlock instanceof P) || (ficheBlock instanceof Ul)) {
            return true;
        }
        return false;
    }

}
