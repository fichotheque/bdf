/* FichothequeLib_API - Copyright (c) 2006-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class Langue implements FicheItem, Serializable {

    private static final long serialVersionUID = 3L;
    private final Lang lang;

    public Langue(Lang lang) {
        this.lang = lang;
    }

    public Lang getLang() {
        return lang;
    }

    @Override
    public String toString() {
        return lang.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        Langue otherLangue = (Langue) other;
        return otherLangue.lang.equals(this.lang);
    }

    @Override
    public int hashCode() {
        return lang.hashCode();
    }

}
