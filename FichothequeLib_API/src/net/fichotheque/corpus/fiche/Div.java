/* FichothequeLib_API - Copyright (c) 2007-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class Div extends FicheBlocksImpl implements ZoneBlock, FicheBlocks, Serializable {

    private static final long serialVersionUID = 8L;
    private final ParagraphBlock numero = new ParagraphBlock();
    private final ParagraphBlock legende = new ParagraphBlock();
    private Lang lang = null;
    private AttsImpl attsImpl;

    public Div() {
    }

    public Lang getLang() {
        return lang;
    }

    public void setLang(Lang lang) {
        this.lang = lang;
    }

    @Override
    public TextContent getNumero() {
        return numero;
    }

    @Override
    public TextContent getLegende() {
        return legende;
    }

    @Override
    public TextContentBuilder getNumeroBuilder() {
        return numero;
    }

    @Override
    public TextContentBuilder getLegendeBuilder() {
        return legende;
    }

    @Override
    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        String mode = null;
        if (name.equals("class")) {
            mode = check(value, "BLOCKQUOTE", "blockquote");
        }
        if (mode != null) {
            attsImpl = AttsImpl.put(attsImpl, "mode", mode);
        } else {
            attsImpl = AttsImpl.put(attsImpl, name, value);
        }
    }

    @Override
    public boolean isValidFicheBlock(FicheBlock ficheBlock) {
        if (ficheBlock instanceof Div) {
            return false;
        } else {
            return true;
        }
    }

    private String check(String value, String prefix, String mode) {
        if (!value.startsWith(prefix)) {
            return null;
        }
        int length = prefix.length();
        if (value.length() > length) {
            attsImpl = AttsImpl.put(attsImpl, "class", value.substring(length + 1));
        }
        return mode;
    }

}
