/* FichothequeLib_API - Copyright (c) 2008-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.money.ExtendedCurrency;
import net.mapeadores.util.money.MoneyLong;
import net.mapeadores.util.primitives.Decimal;


/**
 *
 * @author Vincent Calame
 */
public final class Montant implements FicheItem, Serializable {

    private static final long serialVersionUID = 4L;
    private Decimal decimal;
    private ExtendedCurrency currency;

    public Montant(Decimal decimal, ExtendedCurrency currency) {
        if (decimal == null) {
            throw new IllegalArgumentException("decimal argument is null");
        }
        this.decimal = decimal;
        this.currency = currency;
    }

    public ExtendedCurrency getCurrency() {
        return currency;
    }

    @Override
    public int hashCode() {
        return decimal.hashCode();
    }

    @Override
    public String toString() {
        return decimal.toString() + " " + currency.getCurrencyCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Montant)) {
            return false;
        }
        Montant other = (Montant) obj;
        if (!other.currency.equals(this.currency)) {
            return false;
        }
        if (!other.decimal.equals(this.decimal)) {
            return false;
        }
        return true;
    }

    public Decimal getDecimal() {
        return decimal;
    }

    public long toMoneyLong() {
        return MoneyLong.toMoneyLong(decimal, currency.getDefaultFractionDigits());
    }

}
