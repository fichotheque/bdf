/* FichothequeLib_API - Copyright (c) 2006-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.Arrays;


/**
 *
 * @author Vincent Calame
 */
class FicheItemsImpl extends AbstractList<FicheItem> implements FicheItems, Serializable {

    private static final long serialVersionUID = 4L;
    private int size = 0;
    private FicheItem[] items = new FicheItem[4];

    FicheItemsImpl() {
    }

    @Override
    public FicheItem get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return items[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        Arrays.fill(items, null);
        size = 0;
    }

    @Override
    public boolean add(FicheItem ficheItem) {
        if (ficheItem == null) {
            throw new NullPointerException();
        }
        return innerAdd(ficheItem);
    }

    void addFicheItems(FicheItems ficheItems) {
        int othersize = ficheItems.size();
        for (int i = 0; i < othersize; i++) {
            innerAdd(ficheItems.get(i));
        }
    }

    private boolean innerAdd(FicheItem item) {
        for (int i = 0; i < size; i++) {
            if (item.equals(items[i])) {
                return false;
            }
        }
        checkLength();
        items[size] = item;
        size++;
        return true;
    }

    private void checkLength() {
        if (size == items.length) {
            FicheItem[] nv = new FicheItem[size * 2];
            System.arraycopy(items, 0, nv, 0, size);
            items = nv;
        }
    }

}
