/* FichothequeLib_API - Copyright (c) 2006-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.localisation.Country;


/**
 *
 * @author Vincent Calame
 */
public final class Pays implements FicheItem, Serializable {

    private static final long serialVersionUID = 3L;
    private final Country country;

    public Pays(Country country) {
        if (country == null) {
            throw new IllegalArgumentException("country is null");
        }
        this.country = country;
    }

    public Country getCountry() {
        return country;
    }

    @Override
    public String toString() {
        return country.toString();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        Pays otherPays = (Pays) other;
        return otherPays.country.toString().equals(this.country.toString());
    }

    @Override
    public int hashCode() {
        return country.hashCode();
    }

}
