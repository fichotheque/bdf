/* FichothequeLib_API - Copyright (c) 2009-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.utils.FicheUtils;


/**
 *
 * @author Vincent Calame
 */
public final class Para extends AbstractList<Object> implements FicheItem, TextContent, Serializable {

    private static final long serialVersionUID = 3L;
    private final Object[] partArray;

    private Para(Object[] partArray) {
        this.partArray = partArray;
    }

    @Override
    public int size() {
        return partArray.length;
    }

    @Override
    public Object get(int i) {
        return partArray[i];
    }

    public String contentToString() {
        StringBuilder buf = new StringBuilder();
        for (Object obj : partArray) {
            if (obj instanceof String) {
                buf.append((String) obj);
            } else if (obj instanceof S) {
                S s = (S) obj;
                buf.append(s.getValue());
            }
        }
        return buf.toString();
    }

    public FicheBlocks toFicheBlocks() {
        P p = new P();
        for (Object obj : partArray) {
            if (obj instanceof String) {
                p.addText((String) obj);
            } else if (obj instanceof S) {
                p.addS((S) obj);
            }
        }
        return FicheUtils.toFicheBlocks(p);
    }


    public static class Builder implements TextContentBuilder {

        private final List<Object> partList = new ArrayList<Object>();

        public Builder() {

        }

        @Override
        public void addText(String text) {
            if (text.length() == 0) {
                return;
            }
            int size = partList.size();
            boolean merge = false;
            if (size > 0) {
                Object obj = partList.get(size - 1);
                if (obj instanceof String) {
                    partList.set(size - 1, ((String) obj) + text);
                    merge = true;
                }
            }
            if (!merge) {
                partList.add(text);
            }
        }

        @Override
        public void addS(S s) {
            partList.add(s);
        }

        public Para toPara() {
            return new Para(partList.toArray());
        }

    }

}
