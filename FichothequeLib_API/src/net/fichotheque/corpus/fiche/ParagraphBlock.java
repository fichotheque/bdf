/* FichothequeLib_API - Copyright (c) 2006-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class ParagraphBlock extends AbstractList<Object> implements AttConsumer, TextContentBuilder, TextContent, Serializable {

    private static final long serialVersionUID = 6L;
    protected List<Object> partList = new ArrayList<Object>();
    private AttsImpl attsImpl;

    public ParagraphBlock() {
    }

    @Override
    public int size() {
        return partList.size();
    }

    @Override
    public Object get(int i) {
        return partList.get(i);
    }

    @Override
    public void addText(String text) {
        partList.add(text);
    }

    @Override
    public void addS(S s) {
        partList.add(s);
    }

    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

}
