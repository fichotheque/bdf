/* FichothequeLib_API - Copyright (c) 2006-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public class Table extends AbstractList<Tr> implements ZoneBlock, RandomAccess, Serializable {

    private static final long serialVersionUID = 5L;
    private final ParagraphBlock numero = new ParagraphBlock();
    private final ParagraphBlock legende = new ParagraphBlock();
    private Tr[] trArray = new Tr[4];
    private int size = 0;
    private AttsImpl attsImpl;

    public Table() {
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Tr get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return trArray[index];
    }

    @Override
    public boolean add(Tr tr) {
        if (tr == null) {
            throw new NullPointerException();
        }
        checkLength();
        trArray[size] = tr;
        size++;
        return true;
    }

    @Override
    public TextContent getNumero() {
        return numero;
    }

    @Override
    public TextContent getLegende() {
        return legende;
    }

    @Override
    public TextContentBuilder getNumeroBuilder() {
        return numero;
    }

    @Override
    public TextContentBuilder getLegendeBuilder() {
        return legende;
    }

    @Override
    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

    private void checkLength() {
        if (size == trArray.length) {
            Tr[] nv = new Tr[size * 2];
            System.arraycopy(trArray, 0, nv, 0, size);
            trArray = nv;
        }
    }

    public static int checkColCount(Table table) {
        int cols = 0;
        for (Tr tr : table) {
            int cur = 0;
            int tdSize = tr.size();
            for (int j = 0; j < tdSize; j++) {
                cur++;
                Td td = tr.get(j);
                Atts atts = td.getAtts();
                int attLength = atts.size();
                for (int k = 0; k < attLength; k++) {
                    String name = atts.getName(k);
                    if (name.equals("colspan")) {
                        try {
                            int colspan = Integer.parseInt(atts.getValue(k));
                            if (colspan > 1) {
                                cur = cur + colspan - 1;
                            }
                        } catch (NumberFormatException nfe) {
                        }
                    }
                }
            }
            cols = Math.max(cols, cur);
        }
        return cols;
    }

}
