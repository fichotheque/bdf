/* FichothequeLib_API - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 * Désigne une liste d'attributs au sens XMl (un couple clé-valeur). Le terme La
 * forme courte Att a été utilisé en référence à ATTLIST de la DTD et pour
 * éviter une confusion avec les attributs au sens de la classe Attribute de la
 * bibliothèque UtilLib Ces attributs seront enregistrés sous la forme att-*.
 *
 * @author Vincent Calame
 */
public interface Atts {

    public int size();

    public String getName(int index);

    public String getValue(int index);

    public default boolean hasOnlyAutomaticAtts() {
        return false;
    }

    public default boolean isEmpty() {
        return (size() == 0);
    }

    public default String getValue(String name) {
        int size = size();
        for (int i = 0; i < size; i++) {
            if (getName(i).equals(name)) {
                return getValue(i);
            }
        }
        return null;
    }

}
