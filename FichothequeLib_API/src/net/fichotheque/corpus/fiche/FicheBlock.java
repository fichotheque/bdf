/* FichothequeLib_API - Copyright (c) 2006-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;


/**
 * Cette interface sert à désigner les éléments à l'intérieur d'une zone de
 * texte.
 */
public interface FicheBlock extends AttConsumer {

    public Atts getAtts();

}
