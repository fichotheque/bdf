/* FichothequeLib_API - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import net.mapeadores.util.html.TrustedHtml;
import net.mapeadores.util.html.TrustedHtmlFactory;


/**
 *
 * @author Vincent Calame
 */
public class Cdatadiv implements ZoneBlock, Serializable {

    private static final long serialVersionUID = 6L;
    private final ParagraphBlock numero = new ParagraphBlock();
    private final ParagraphBlock legende = new ParagraphBlock();
    private String cdata;
    private AttsImpl attsImpl;

    public Cdatadiv() {
    }

    public String getCdata() {
        return cdata;
    }

    public void setCdata(TrustedHtmlFactory trustedHtmlFactory, String cdata) {
        this.cdata = trustedHtmlFactory.check(cdata);
    }

    public void setCdata(TrustedHtml trustedHtml) {
        this.cdata = trustedHtml.toString();
    }

    @Override
    public TextContent getNumero() {
        return numero;
    }

    @Override
    public TextContent getLegende() {
        return legende;
    }

    @Override
    public TextContentBuilder getNumeroBuilder() {
        return numero;
    }

    @Override
    public TextContentBuilder getLegendeBuilder() {
        return legende;
    }

    @Override
    public Atts getAtts() {
        return AttsImpl.check(attsImpl);
    }

    @Override
    public void putAtt(String name, String value) {
        attsImpl = AttsImpl.put(attsImpl, name, value);
    }

}
