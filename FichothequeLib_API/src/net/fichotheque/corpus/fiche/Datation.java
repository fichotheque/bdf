/* FichothequeLib_API - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import java.io.Serializable;
import java.time.LocalDate;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public final class Datation implements FicheItem, Serializable {

    private static final long serialVersionUID = 3L;
    private final FuzzyDate date;

    public Datation(FuzzyDate date) {
        if (date == null) {
            throw new IllegalArgumentException("date is null");
        }
        this.date = date;
    }

    public FuzzyDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return date.toString();
    }

    public LocalDate toLocalDate(boolean lastDay) {
        return date.toLocalDate(lastDay);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        Datation otherDatation = (Datation) other;
        return (this.date.compareTo(otherDatation.date) == 0);
    }

    @Override
    public int hashCode() {
        return date.hashCode();
    }

}
