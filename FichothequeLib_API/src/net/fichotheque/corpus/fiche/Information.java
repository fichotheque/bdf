/* FichothequeLib_API - Copyright (c) 2006-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus.fiche;

import net.fichotheque.corpus.metadata.FieldKey;


/**
 * Une instance d'information est un champ contenant plusieurs instances de
 * FicheItem. Elle doit toujours en contenir au moins un.
 *
 * @author Vincent Calame
 */
public interface Information extends FicheItems {

    /**
     * Retourne la clé du champ correspondant à l'information. Il est bien
     * entendu du type information.
     *
     * @return clé du champ
     */
    public FieldKey getFieldKey();

}
