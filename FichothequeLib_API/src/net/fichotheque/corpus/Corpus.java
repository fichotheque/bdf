/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus;

import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.CorpusMetadata;
import net.fichotheque.history.CroisementHistory;
import net.fichotheque.history.FicheHistory;


/**
 *
 * @author Vincent Calame
 */
public interface Corpus extends Subset {

    public Subset getMasterSubset();

    /**
     * Résultat équivalent à (FicheMeta) getSubsetItemById()
     */
    public FicheMeta getFicheMetaById(int id);

    public FicheAPI getFicheAPI(FicheMeta ficheMeta, boolean withSection);

    public Fiche getFiche(FicheMeta ficheMeta);

    public List<FicheMeta> getFicheMetaList();

    public FicheHistory getFicheHistory(int id);

    public Fiche getFicheRevision(int id, String revisionName);

    public List<FicheHistory> getRemovedFicheHistoryList();

    public List<CroisementHistory> getCroisementHistoryList(int ficheId, SubsetKey subsetKey);

    public default CorpusMetadata getCorpusMetadata() {
        return (CorpusMetadata) getMetadata();
    }

}
