/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus;

import java.util.List;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface FicheMeta extends SubsetItem {

    /**
     * Retourne le titre de la fiche. N'est jamais null.
     */
    public String getTitre();

    /**
     * Retourne null si la date de création n'est pas définie.
     */
    public FuzzyDate getCreationDate();

    /**
     * Retourne null si la date de modification n'est pas définie. La date de
     * modification ne peut pas être définie si et seulement si la date de
     * création ne l'est pas non plus. La date de modification doit toujours
     * être égale ou supérieure à la date de création.
     */
    public FuzzyDate getModificationDate();

    /**
     * Peut être nul.
     */
    public Lang getLang();

    public List<String> getRedacteurGlobalIdList();

    public boolean isDiscarded();

    public default Corpus getCorpus() {
        return (Corpus) getSubset();
    }

    public default FicheAPI getFicheAPI(boolean withSection) {
        return getCorpus().getFicheAPI(this, withSection);
    }

}
