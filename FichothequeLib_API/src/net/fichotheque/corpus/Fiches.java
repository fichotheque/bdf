/* FichothequeLib_API - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus;

import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;


/**
 *
 * @author Vincent Calame
 */
public interface Fiches extends Predicate<FicheMeta> {

    public int getFicheCount();

    public List<Entry> getEntryList();

    public default boolean isEmpty() {
        return getEntryList().isEmpty();
    }


    public interface Entry {

        public Corpus getCorpus();

        public List<FicheMeta> getFicheMetaList();

        public List<SubsetItem> getSubsetItemList();

    }

}
