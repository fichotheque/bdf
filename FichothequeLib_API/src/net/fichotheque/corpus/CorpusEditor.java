/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus;

import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.CorpusMetadataEditor;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusEditor {

    public CorpusMetadataEditor getCorpusMetadataEditor();

    public FichothequeEditor getFichothequeEditor();

    public Corpus getCorpus();

    public FicheMeta createFiche(int id) throws ExistingIdException, NoMasterIdException;

    public void saveFiche(FicheMeta ficheMeta, FicheAPI fiche);

    /**
     * Supprime la fiche. Retourne <em>false</em> si la fiche ne peut être
     * supprimée. Une fiche ne peut pas être supprimée si des fiches lui sont
     * rattachées ou si elle possède des liens avec d'autres éléments de la
     * base.
     *
     * @param ficheMeta fiche à supprimer
     * @return true si la fiche est supprimée, false sinon
     */
    public boolean removeFiche(FicheMeta ficheMeta);

    public void setDate(FicheMeta ficheMeta, FuzzyDate date, boolean modification);

}
