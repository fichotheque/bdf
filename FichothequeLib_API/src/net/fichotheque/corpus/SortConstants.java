/* FichothequeLib_API - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.corpus;


/**
 *
 * @author Vincent Calame
 */
public interface SortConstants {

    public final static String NONE = "none";
    public final static String TITRE_ASC = "titre-asc";
    public final static String TITRE_DESC = "titre-desc";
    public final static String ID_ASC = "id-asc";
    public final static String ID_DESC = "id-desc";
    public final static String CREATIONDATE_ASC = "creationdate-asc";
    public final static String CREATIONDATE_DESC = "creationdate-desc";
    public final static String MODIFICATIONDATE_ASC = "modificationdate-asc";
    public final static String MODIFICATIONDATE_DESC = "modificationdate-desc";

    public static String checkSortType(String sortType) {
        switch (sortType) {
            case SortConstants.NONE:
                return SortConstants.NONE;
            case SortConstants.TITRE_ASC:
            case "titre":
                return SortConstants.TITRE_ASC;
            case SortConstants.TITRE_DESC:
                return SortConstants.TITRE_DESC;
            case SortConstants.CREATIONDATE_ASC:
                return SortConstants.CREATIONDATE_ASC;
            case SortConstants.CREATIONDATE_DESC:
            case "creationdate":
                return SortConstants.CREATIONDATE_DESC;
            case SortConstants.MODIFICATIONDATE_ASC:
                return SortConstants.MODIFICATIONDATE_ASC;
            case SortConstants.MODIFICATIONDATE_DESC:
            case "modificationdate":
                return SortConstants.MODIFICATIONDATE_DESC;
            case SortConstants.ID_ASC:
            case "id":
            case "idcorpus":
                return SortConstants.ID_ASC;
            case SortConstants.ID_DESC:
                return SortConstants.ID_DESC;
            default:
                throw new IllegalArgumentException("Unknown sortType: " + sortType);
        }
    }

}
