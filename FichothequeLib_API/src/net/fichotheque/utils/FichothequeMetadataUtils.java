/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils;

import net.fichotheque.Fichotheque;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.metadata.FichothequeMetadata;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public final class FichothequeMetadataUtils {

    private FichothequeMetadataUtils() {
    }

    public static String getTitle(Fichotheque fichotheque, Lang lang) {
        return FichothequeMetadataUtils.getTitle(fichotheque.getFichothequeMetadata(), lang);
    }

    public static String getTitle(FichothequeMetadata fichothequeMetadata, Lang lang) {
        Label label = fichothequeMetadata.getTitleLabels().getLangPartCheckedLabel(lang);
        if (label != null) {
            return label.getLabelString();
        } else {
            return fichothequeMetadata.getBaseName();
        }
    }

    public static String getLongTitle(Fichotheque fichotheque, Lang lang) {
        FichothequeMetadata fichothequeMetadata = fichotheque.getFichothequeMetadata();
        String label = FichothequeUtils.getPhraseLabel(fichothequeMetadata.getPhrases(), FichothequeConstants.LONG_PHRASE, lang);
        if (label != null) {
            return label;
        } else {
            return fichothequeMetadata.getBaseName();
        }
    }

}
