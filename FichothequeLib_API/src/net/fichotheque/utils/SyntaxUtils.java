/* FichothequeLib_Tools - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public final class SyntaxUtils {

    private SyntaxUtils() {

    }

    public static void appendErrorSpan(Appendable buf, String value) throws IOException {
        appendSpan(buf, "cm-error", value);
    }

    public static void appendSpan(Appendable buf, String classes, String value) throws IOException {
        if (value.length() == 0) {
            return;
        }
        buf.append("<span class=\"");
        buf.append(classes);
        buf.append("\">");
        escape(buf, value);
        buf.append("</span>");
    }

    public static void escape(Appendable buf, String value) throws IOException {
        int length = value.length();
        for (int i = 0; i < length; i++) {
            char carac = value.charAt(i);
            switch (carac) {
                case '&':
                    buf.append("&amp;");
                    break;
                case '"':
                    buf.append("&quot;");
                    break;
                case '<':
                    buf.append("&lt;");
                    break;
                case '>':
                    buf.append("&gt;");
                    break;
                default:
                    buf.append(carac);
            }
        }
    }

}
