/* FichothequeLib_API - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils.selection;

import net.fichotheque.addenda.Document;
import net.fichotheque.selection.CroisementCondition;
import net.fichotheque.selection.DocumentQuery;
import net.fichotheque.selection.SelectionContext;
import net.mapeadores.util.conditions.ConditionsConstants;
import net.mapeadores.util.conditions.ConditionsUtils;
import net.mapeadores.util.conditions.TextTestEngine;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
class DocumentSelectEngine {

    private final CroisementCondition croisementCondition;
    private final short conditionType;
    private final TextTestEngine textTestEngine;

    DocumentSelectEngine(DocumentQuery documentQuery, CroisementCondition croisementCondition, SelectionContext selectionContext) {
        this.croisementCondition = croisementCondition;
        TextCondition nameCondition = documentQuery.getNameCondition();
        if (nameCondition != null) {
            this.conditionType = ConditionsUtils.getConditionType(nameCondition);
            if (ConditionsUtils.isPartialState(conditionType)) {
                this.textTestEngine = TextTestEngine.newInstance(nameCondition, selectionContext.getWorkingLang());
            } else {
                this.textTestEngine = null;
            }
        } else {
            this.conditionType = ConditionsConstants.UNKNOWN_CONDITION;
            this.textTestEngine = null;
        }
    }

    void filter(Document document, FilteredCroisementBuilder builder) {
        if (!isSelected(document)) {
            return;
        }
        builder.testCondition(croisementCondition);
    }

    boolean isSelected(Document document) {
        if (conditionType != ConditionsConstants.UNKNOWN_CONDITION) {
            if (!isNameSelected(document)) {
                return false;
            }
        }
        return true;
    }

    private boolean isNameSelected(Document document) {
        switch (conditionType) {
            case ConditionsConstants.IMPOSSIBLE_CONDITION:
                return false;
            case ConditionsConstants.NEUTRAL_CONDITION:
            case ConditionsConstants.UNKNOWN_CONDITION:
                return true;
            case ConditionsConstants.NOTEMPTY_CONDITION:
                return true;
            case ConditionsConstants.EMPTY_CONDITION:
                return false;
            case ConditionsConstants.PARTIAL_CONDITION:
            case ConditionsConstants.PARTIALOREMPTY_CONDITION:
                return textTestEngine.isSelected(document.getBasename());
            default:
                throw new SwitchException("state = " + conditionType);
        }
    }

}
