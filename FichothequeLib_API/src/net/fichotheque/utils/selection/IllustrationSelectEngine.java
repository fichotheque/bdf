/* FichothequeLib_API - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils.selection;

import net.fichotheque.album.Illustration;
import net.fichotheque.selection.CroisementCondition;
import net.fichotheque.selection.IllustrationQuery;
import net.fichotheque.selection.SelectionContext;


/**
 *
 * @author Vincent Calame
 */
class IllustrationSelectEngine {

    private final CroisementCondition croisementCondition;

    IllustrationSelectEngine(IllustrationQuery illustrationQuery, CroisementCondition croisementCondition, SelectionContext selectionContext) {
        this.croisementCondition = croisementCondition;
    }

    void filter(Illustration illustration, FilteredCroisementBuilder builder) {
        if (!isSelected(illustration)) {
            return;
        }
        builder.testCondition(croisementCondition);
    }

    boolean isSelected(Illustration illustration) {
        return true;
    }

}
