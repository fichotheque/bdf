/* FichothequeLib_API - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils.selection;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.croisement.CroisementKey;
import net.fichotheque.croisement.Lien;
import net.fichotheque.selection.CroisementCondition;
import net.fichotheque.selection.RangeCondition;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
class FilteredCroisementBuilder {

    private final Croisement source;
    private final Map<String, Lien> lienMap = new LinkedHashMap<String, Lien>();

    FilteredCroisementBuilder(Croisement source) {
        this.source = source;
    }

    void testCondition(CroisementCondition croisementCondition) {
        if (croisementCondition == null) {
            for (Lien lien : source.getLienList()) {
                addLien(lien);
            }
            return;
        }
        List<String> modeList = croisementCondition.getLienModeList();
        RangeCondition weightCondition = croisementCondition.getWeightRangeCondition();
        if (weightCondition != null) {
            if (modeList.isEmpty()) {
                for (Lien lien : source.getLienList()) {
                    if (weightCondition.accept(lien.getPoids())) {
                        addLien(lien);
                    }
                }
            } else {
                for (String mode : modeList) {
                    Lien lien = source.getLienByMode(mode);
                    if ((lien != null) && (weightCondition.accept(lien.getPoids()))) {
                        addLien(lien);
                    }
                }
            }
        } else {
            for (String mode : modeList) {
                Lien lien = source.getLienByMode(mode);
                if (lien != null) {
                    addLien(lien);
                }
            }
        }
    }

    void addLien(Lien lien) {
        lienMap.put(lien.getMode(), lien);
    }

    Croisement toCroisement() {
        int size = lienMap.size();
        if (size == 0) {
            return null;
        }
        return new InternalFilteredCroisement(source, lienMap.values().toArray(new Lien[size]));
    }


    private static class InternalFilteredCroisement implements Croisement {

        private final Croisement source;
        private final Lien[] lienArray;
        private final List<Lien> lienList;

        private InternalFilteredCroisement(Croisement source, Lien[] lienArray) {
            this.source = source;
            this.lienArray = lienArray;
            this.lienList = CroisementUtils.wrap(lienArray);
        }

        @Override
        public CroisementKey getCroisementKey() {
            return source.getCroisementKey();
        }

        @Override
        public List<Lien> getLienList() {
            return lienList;
        }

        @Override
        public Lien getLienByMode(String mode) {
            for (Lien lien : lienArray) {
                if (lien.getMode().equals(mode)) {
                    return lien;
                }
            }
            return null;
        }

        @Override
        public Attributes getAttributes() {
            return source.getAttributes();
        }

    }

}
