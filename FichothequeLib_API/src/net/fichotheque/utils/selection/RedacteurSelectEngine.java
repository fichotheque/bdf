/* FichothequeLib_API - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils.selection;

import net.fichotheque.selection.RedacteurQuery;
import net.fichotheque.selection.SelectionContext;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.conditions.ConditionsConstants;
import net.mapeadores.util.conditions.ConditionsUtils;
import net.mapeadores.util.conditions.TextTestEngine;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
class RedacteurSelectEngine {

    private final short conditionType;
    private final TextTestEngine textTestEngine;

    RedacteurSelectEngine(RedacteurQuery redacteurQuery, SelectionContext selectionContext) {
        TextCondition nameCondition = redacteurQuery.getNomcompletCondition();
        if (nameCondition != null) {
            this.conditionType = ConditionsUtils.getConditionType(nameCondition);
            if (ConditionsUtils.isPartialState(conditionType)) {
                this.textTestEngine = TextTestEngine.newInstance(nameCondition, selectionContext.getWorkingLang());
            } else {
                this.textTestEngine = null;
            }
        } else {
            this.conditionType = ConditionsConstants.UNKNOWN_CONDITION;
            this.textTestEngine = null;
        }
    }

    boolean isSelected(Redacteur redacteur) {
        if (conditionType != ConditionsConstants.UNKNOWN_CONDITION) {
            if (!isCompletenameSelected(redacteur)) {
                return false;
            }
        }
        return true;
    }

    private boolean isCompletenameSelected(Redacteur redacteur) {
        switch (conditionType) {
            case ConditionsConstants.IMPOSSIBLE_CONDITION:
                return false;
            case ConditionsConstants.NEUTRAL_CONDITION:
            case ConditionsConstants.UNKNOWN_CONDITION:
                return true;
            case ConditionsConstants.NOTEMPTY_CONDITION:
                return isNotEmpty(redacteur);
            case ConditionsConstants.EMPTY_CONDITION:
                return !isNotEmpty(redacteur);
            case ConditionsConstants.PARTIAL_CONDITION:
            case ConditionsConstants.PARTIALOREMPTY_CONDITION:
                return textTestEngine.isSelected(redacteur.getCompleteName());
            default:
                throw new SwitchException("state = " + conditionType);
        }
    }

    private boolean isNotEmpty(Redacteur redacteur) {
        PersonCore personCore = redacteur.getPersonCore();
        return (personCore.getSurname().length() != 0) || (personCore.getForename().length() != 0) || (personCore.getNonlatin().length() != 0);
    }

}
