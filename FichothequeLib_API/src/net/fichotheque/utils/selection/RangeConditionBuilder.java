/* FichothequeLib_API - Copyright (c) 2022-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils.selection;

import net.fichotheque.selection.RangeCondition;
import net.mapeadores.util.primitives.RangeUtils;
import net.mapeadores.util.primitives.Ranges;


/**
 *
 * @author Vincent Calame
 */
public class RangeConditionBuilder {

    private boolean exclude = false;
    private Ranges ranges;

    public RangeConditionBuilder(Ranges ranges) {
        if ((ranges == null) || (!RangeUtils.isPositiveRanges(ranges))) {
            throw new IllegalArgumentException("ranges is not positive");
        }
        this.ranges = ranges;
    }

    public RangeConditionBuilder setExclude(boolean exclude) {
        this.exclude = exclude;
        return this;
    }

    public RangeConditionBuilder setRanges(Ranges ranges) {
        if ((ranges == null) || (!RangeUtils.isPositiveRanges(ranges))) {
            throw new IllegalArgumentException("ranges is not positive");
        }
        this.ranges = ranges;
        return this;
    }

    public RangeCondition toRangeCondition() {
        return new InternalRangeCondition(exclude, ranges);
    }

    public static RangeConditionBuilder init(Ranges ranges) {
        return new RangeConditionBuilder(ranges);
    }


    private static class InternalRangeCondition implements RangeCondition {

        private final boolean exclude;
        private final Ranges ranges;

        private InternalRangeCondition(boolean exclude, Ranges ranges) {
            this.exclude = exclude;
            this.ranges = ranges;
        }

        @Override
        public boolean isExclude() {
            return exclude;
        }

        @Override
        public Ranges getRanges() {
            return ranges;
        }

    }

}
