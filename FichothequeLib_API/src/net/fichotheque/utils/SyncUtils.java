/* FichothequeLib_API - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils;

import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;
import net.fichotheque.thesaurus.sync.MotcleSync;


/**
 *
 * @author Vincent Calame
 */
public final class SyncUtils {

    private SyncUtils() {

    }

    public static List<MotcleSync> wrap(MotcleSync[] array) {
        return new MotcleSyncList(array);
    }


    private static class MotcleSyncList extends AbstractList<MotcleSync> implements RandomAccess {

        private final MotcleSync[] array;

        private MotcleSyncList(MotcleSync[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MotcleSync get(int index) {
            return array[index];
        }

    }

}
