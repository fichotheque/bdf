/* FichothequeLib_API - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import net.fichotheque.EditOrigin;


/**
 *
 * @author Vincent Calame
 */
public final class EditOriginUtils {

    private EditOriginUtils() {

    }

    public static EditOrigin newEditOrigin(String source) {
        return new InternalEditOrigin(null, null, getCurrentGMT(), source);
    }

    public static EditOrigin newEditOrigin(String sessionId, String redacteurGlobalId, String source) {
        return new InternalEditOrigin(sessionId, redacteurGlobalId, getCurrentGMT(), source);
    }

    private static String getCurrentGMT() {
        ZonedDateTime currentDateTime = ZonedDateTime.now(ZoneOffset.UTC).withNano(0);
        return currentDateTime.toString();
    }


    private static class InternalEditOrigin implements EditOrigin {

        private final String redacteurGlobalId;
        private final String isoTime;
        private final String source;
        private final String sessionId;

        private InternalEditOrigin(String sessionId, String redacteurGlobalId, String isoTime, String source) {
            this.sessionId = sessionId;
            this.redacteurGlobalId = redacteurGlobalId;
            this.isoTime = isoTime;
            this.source = source;
        }

        @Override
        public String getSessionId() {
            return sessionId;
        }

        @Override
        public String getRedacteurGlobalId() {
            return redacteurGlobalId;
        }

        @Override
        public String getISOTime() {
            return isoTime;
        }

        @Override
        public String getSource() {
            return source;
        }

    }


}
