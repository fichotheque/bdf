/* FichothequeLib_API - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils;

import net.fichotheque.Metadata;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesCache;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.LabelsCache;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.PhrasesCache;


/**
 *
 * @author Vincent Calame
 */
public class DefaultMetadata implements Metadata {

    private final LabelsCache titleLabelsCache = new LabelsCache();
    private final AttributesCache attributesCache = new AttributesCache();
    private final PhrasesCache phrasesCache = new PhrasesCache();

    public DefaultMetadata() {

    }

    @Override
    public Labels getTitleLabels() {
        return titleLabelsCache.getLabels();
    }

    @Override
    public Attributes getAttributes() {
        return attributesCache.getAttributes();
    }

    @Override
    public Phrases getPhrases() {
        return phrasesCache.getPhrases();
    }

    protected boolean removeAttribute(AttributeKey attributeKey) {
        return attributesCache.removeAttribute(attributeKey);
    }

    protected boolean putAttribute(Attribute attribute) {
        return attributesCache.putAttribute(attribute);
    }

    protected boolean putLabel(String name, Label label) {
        if (name == null) {
            return titleLabelsCache.putLabel(label);
        } else if (FichothequeUtils.isValidPhraseName(name)) {
            return phrasesCache.putLabel(name, label);
        } else {
            return false;
        }
    }

    protected boolean removeLabel(String name, Lang lang) {
        if (name == null) {
            return titleLabelsCache.removeLabel(lang);
        } else {
            return phrasesCache.removeLabel(name, lang);
        }
    }

}
