/* FichothequeLib_API - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils;

import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.RandomAccess;
import java.util.function.Predicate;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.exportation.table.CellConverter;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.CellEngineProvider;
import net.fichotheque.exportation.table.ColDef;
import net.fichotheque.exportation.table.CroisementTable;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.exportation.table.TableExportContentDescription;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.exportation.table.TableExportResult;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class TableExportUtils {

    public final static List<TableExportContentDescription> EMPTY_CONTENTDESCRIPTIONLIST = Collections.emptyList();
    public final static List<CroisementTable> EMPTY_CROISEMENTTABLE_LIST = Collections.emptyList();
    public final static TableExportResult EMPTY_TABLEEXPORTRESULT = new EmptyTableExportResult();
    public final static CellConverter EMPTY_CELLCONVERTER = new EmptyCellEngine();
    public final static CellEngine EMPTY_CELLENGINE = new EmptyCellEngine();
    public final static Cell[] EMPTY_CELLARRAY = new Cell[0];
    public final static CellEngineProvider EMPTY_CELLENGINEPROVIDER = new EmptyCellEngineProvider();
    public final static List<ColDef> EMPTY_COLDEFLIST = Collections.emptyList();

    private TableExportUtils() {

    }

    public static TableExportContentDescription getTableExportContentDescription(TableExportDescription tableExportDescription, String contentPath) {
        for (TableExportContentDescription description : tableExportDescription.getTableExportContentDescriptionList()) {
            if (description.getPath().equals(contentPath)) {
                return description;
            }
        }
        return null;
    }

    public static boolean isContentEditable(TableExportDescription tableExportDescription, String contentPath) {
        if (!tableExportDescription.isEditable()) {
            return false;
        }
        for (TableExportContentDescription tecd : tableExportDescription.getTableExportContentDescriptionList()) {
            if (tecd.getPath().equals(contentPath)) {
                return tecd.isEditable();
            }
        }
        return true;
    }

    public static Cell toCell(short formatCast, Object value, ColDef colDef) {
        return new InternalCell(formatCast, value, colDef);
    }

    public static TableExport toTableExport(TableExportDef tableExportDef, SubsetTable[] subsetTableArray, CroisementTable[] croisementTableArray) {
        return new InternalTableExport(tableExportDef, TableDefUtils.wrap(subsetTableArray), TableDefUtils.wrap(croisementTableArray));
    }

    public static TableExport toTableExport(TableExportDef tableExportDef, SubsetTable subsetTable) {
        return new InternalTableExport(tableExportDef, Collections.singletonList(subsetTable), EMPTY_CROISEMENTTABLE_LIST);
    }

    public static List<TableExportDescription> wrap(TableExportDescription[] array) {
        return new TableExportDescriptionList(array);
    }

    public static List<TableExportContentDescription> wrap(TableExportContentDescription[] array) {
        return new TableExportContentDescriptionList(array);
    }

    public static List<ColDef> wrap(ColDef[] array) {
        return new ColDefList(array);
    }

    public static CellConverter getPhraseConverter(Lang lang, Locale formatLocale) {
        return new PhraseCellConverter(lang, formatLocale);
    }


    private static class PhraseCellConverter implements CellConverter {

        private final Lang lang;
        private final Locale formatLocale;
        private final ColDef numberColDef;

        private PhraseCellConverter(Lang lang, Locale formatLocale) {
            this.lang = lang;
            this.formatLocale = formatLocale;
            this.numberColDef = TableDefUtils.toColDef("number", null);
        }

        @Override
        public Cell[] toCellArray(SubsetItem subsetItem) {
            if (subsetItem instanceof FicheMeta) {
                FicheMeta ficheMeta = (FicheMeta) subsetItem;
                String numberPhrase = FichothequeUtils.getNumberPhrase(ficheMeta, FichothequeConstants.FICHE_PHRASE, lang, formatLocale);
                Cell[] result = new Cell[1];
                result[0] = toCell(FormatConstants.NO_CAST, numberPhrase, numberColDef);
                return result;
            } else if (subsetItem instanceof Motcle) {
                Motcle motcle = (Motcle) subsetItem;
                String numberPhrase = FichothequeUtils.getNumberPhrase(motcle, FichothequeConstants.FICHESTYLE_PHRASE, lang, formatLocale, "");
                if (numberPhrase.isEmpty()) {
                    return EMPTY_CELLARRAY;
                } else {
                    Cell[] result = new Cell[1];
                    result[0] = toCell(FormatConstants.NO_CAST, numberPhrase, numberColDef);
                    return result;
                }
            } else {
                return null;
            }
        }

    }


    private static class TableExportDescriptionList extends AbstractList<TableExportDescription> implements RandomAccess {

        private final TableExportDescription[] array;

        private TableExportDescriptionList(TableExportDescription[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public TableExportDescription get(int index) {
            return array[index];
        }

    }


    private static class TableExportContentDescriptionList extends AbstractList<TableExportContentDescription> implements RandomAccess {

        private final TableExportContentDescription[] array;

        private TableExportContentDescriptionList(TableExportContentDescription[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public TableExportContentDescription get(int index) {
            return array[index];
        }

    }


    private static class InternalTableExport implements TableExport {

        private final TableExportDef tableExportDef;
        private final List<SubsetTable> subsetTableList;
        private final List<CroisementTable> croisementTableList;

        private InternalTableExport(TableExportDef tableExportDef, List<SubsetTable> subsetTableList, List<CroisementTable> croisementTableList) {
            this.tableExportDef = tableExportDef;
            this.subsetTableList = subsetTableList;
            this.croisementTableList = croisementTableList;
        }

        @Override
        public TableExportDef getTableExportDef() {
            return tableExportDef;
        }

        @Override
        public List<SubsetTable> getSubsetTableList() {
            return subsetTableList;
        }

        @Override
        public List<CroisementTable> getCroisementTableList() {
            return croisementTableList;
        }

    }


    private static class InternalCell implements Cell {

        private final short formatCast;
        private final Object value;
        private final ColDef colDef;

        private InternalCell(short formatCast, Object value, ColDef colDef) {
            this.formatCast = formatCast;
            this.value = value;
            this.colDef = colDef;
        }

        @Override
        public short getFormatCast() {
            return formatCast;
        }

        @Override
        public Object getValue() {
            return value;
        }

        @Override
        public ColDef getColDef() {
            return colDef;
        }

    }


    private static class EmptyCellEngine implements CellEngine {

        private EmptyCellEngine() {

        }

        @Override
        public Cell[] toCellArray(SubsetItem subsetItem, Predicate<SubsetItem> predicate) {
            return null;
        }

        @Override
        public TableExportResult getTableExportResult(SubsetKey subsetKey) {
            return EMPTY_TABLEEXPORTRESULT;
        }

        @Override
        public List<ColDef> getColDefList(SubsetKey subsetKey) {
            return EMPTY_COLDEFLIST;
        }

    }


    private static class EmptyCellEngineProvider implements CellEngineProvider {

        private EmptyCellEngineProvider() {

        }

        @Override
        public CellEngine getCellEngine(String name) {
            return null;
        }

    }


    private static class EmptyTableExportResult implements TableExportResult {

        private EmptyTableExportResult() {

        }

        @Override
        public boolean hasColumnSum() {
            return false;
        }

        @Override
        public int getColCount() {
            return 0;
        }

        @Override
        public ColDef getColDef(int i) {
            return null;
        }

        @Override
        public TableExportResult.ColumnSum getColumnSum(int i) {
            return null;
        }

    }


    private static class ColDefList extends AbstractList<ColDef> implements RandomAccess {

        private final ColDef[] array;

        private ColDefList(ColDef[] colDefArray) {
            this.array = colDefArray;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ColDef get(int i) {
            return array[i];
        }

    }

}
