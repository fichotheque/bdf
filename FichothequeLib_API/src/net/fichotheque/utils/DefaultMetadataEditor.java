/* FichothequeLib_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils;

import net.fichotheque.Metadata;
import net.fichotheque.MetadataEditor;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public class DefaultMetadataEditor implements MetadataEditor {

    private final DefaultMetadata defaultMetadata;
    private boolean withChanges = false;

    public DefaultMetadataEditor(DefaultMetadata defaultMetadata) {
        this.defaultMetadata = defaultMetadata;
    }

    @Override
    public Metadata getMetadata() {
        return defaultMetadata;
    }

    @Override
    public boolean putLabel(String name, Label label) {
        boolean done = defaultMetadata.putLabel(name, label);
        if (done) {
            fireChange();
        }
        return done;
    }

    @Override
    public boolean removeLabel(String name, Lang lang) {
        boolean done = defaultMetadata.removeLabel(name, lang);
        if (done) {
            fireChange();
        }
        return done;
    }

    @Override
    public boolean removeAttribute(AttributeKey attributeKey) {
        boolean done = defaultMetadata.removeAttribute(attributeKey);
        if (done) {
            fireChange();
        }
        return done;
    }

    @Override
    public boolean putAttribute(Attribute attribute) {
        boolean done = defaultMetadata.putAttribute(attribute);
        if (done) {
            fireChange();
        }
        return done;
    }

    public boolean isWithChanges() {
        return withChanges;
    }

    protected void fireChange() {
        withChanges = true;
    }

}
