/* FichothequeLib_API - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils.pointeurs;

import java.util.HashMap;
import java.util.Map;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.pointeurs.FichePointeur;


/**
 *
 * @author Vincent Calame
 */
class FichePointeurImpl extends AbstractPointeur implements FichePointeur {

    private boolean withSection;
    private Corpus corpus;
    private FicheAPI fiche;
    private FicheMeta currentFicheMeta;
    private final Map<FieldKey, Object> fieldBufferMap = new HashMap<FieldKey, Object>();
    private boolean cacheEnable = false;
    private Map<Integer, FicheAPI> cacheFicheMap;

    FichePointeurImpl(Corpus corpus, boolean withSection) {
        super(corpus);
        this.withSection = withSection;
        this.corpus = corpus;
    }

    FichePointeurImpl(FicheMeta ficheMeta, FicheAPI fiche) {
        this(ficheMeta.getCorpus(), true);
        setCurrentSubsetItem(ficheMeta);
        this.fiche = fiche;
        if (fiche != null) {
            initTitre(fiche.getTitre());
        }
    }

    @Override
    protected void initCurrentSubsetItem(SubsetItem subsetItem) {
        FicheMeta ficheMeta = (FicheMeta) subsetItem;
        this.currentFicheMeta = ficheMeta;
        fieldBufferMap.clear();
        this.fiche = null;
        if (ficheMeta != null) {
            initTitre(ficheMeta.getTitre());
            fieldBufferMap.put(FieldKey.ID, ficheMeta.getId());
        }
    }

    @Override
    public boolean isWithSection() {
        return withSection;
    }

    @Override
    public void enableCache(boolean enable) {
        if (cacheEnable == enable) {
            return;
        }
        this.cacheEnable = enable;
        if (enable) {
            cacheFicheMap = new HashMap<Integer, FicheAPI>();
        } else {
            cacheFicheMap.clear();
            cacheFicheMap = null;
        }
    }

    @Override
    public Object getValue(FieldKey fieldKey) {
        if (currentFicheMeta == null) {
            return null;
        }
        Object obj = fieldBufferMap.get(fieldKey);
        if (obj != null) {
            if (obj.equals(Boolean.FALSE)) {
                return null;
            } else {
                return obj;
            }
        }
        if ((!withSection) && (fieldKey.isSection())) {
            withSection = true;
            if (fiche != null) {
                fiche = null;
                if (cacheEnable) {
                    cacheFicheMap.remove(currentFicheMeta.getId());
                }
            }
        }
        initFiche();
        obj = fiche.getValue(fieldKey);
        if (obj == null) {
            fieldBufferMap.put(fieldKey, Boolean.FALSE);
        } else {
            fieldBufferMap.put(fieldKey, obj);
        }
        return obj;
    }

    private void initFiche() {
        if (fiche != null) {
            return;
        }
        if (cacheEnable) {
            FicheAPI cacheFiche = cacheFicheMap.get(currentFicheMeta.getId());
            if (cacheFiche != null) {
                fiche = cacheFiche;
                return;
            }
        }
        fiche = currentFicheMeta.getFicheAPI(withSection);
        if (cacheEnable) {
            cacheFicheMap.put(currentFicheMeta.getId(), fiche);
        }
    }

    private void initTitre(String titre) {
        if (titre.length() > 0) {
            fieldBufferMap.put(FieldKey.TITRE, titre);
        } else {
            fieldBufferMap.put(FieldKey.TITRE, Boolean.FALSE);
        }
    }


}
