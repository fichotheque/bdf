/* FichothequeLib_API - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.utils.pointeurs;

import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.album.Album;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.pointeurs.MotclePointeur;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.exceptions.SwitchException;


/**
 *
 * @author Vincent Calame
 */
public final class PointeurFactory {

    private PointeurFactory() {

    }

    public static SubsetItemPointeur newSubsetItemPointeur(Subset subset) {
        if (subset instanceof Corpus) {
            return new FichePointeurImpl((Corpus) subset, false);
        }
        if (subset instanceof Thesaurus) {
            return new MotclePointeurImpl((Thesaurus) subset);
        }
        if (subset instanceof Addenda) {
            return new DocumentPointeur((Addenda) subset);
        }
        if (subset instanceof Album) {
            return new IllustrationPointeur((Album) subset);
        }
        throw new SwitchException("Unknown subset implementation: " + subset.getClass().getName());
    }

    public static MotclePointeur newMotclePointeur(Thesaurus thesaurus) {
        return new MotclePointeurImpl(thesaurus);
    }

    public static FichePointeur newFichePointeur(Corpus corpus) {
        return newFichePointeur(corpus, false);
    }

    public static FichePointeur newFichePointeur(Corpus corpus, boolean withSection) {
        return new FichePointeurImpl(corpus, withSection);
    }

    public static FichePointeur toFichePointeur(FicheMeta ficheMeta, FicheAPI fiche) {
        FichePointeurImpl fichePointeur = new FichePointeurImpl(ficheMeta, fiche);
        return fichePointeur;
    }


    private static class IllustrationPointeur extends AbstractPointeur {

        public IllustrationPointeur(Album album) {
            super(album);
        }

        @Override
        protected void initCurrentSubsetItem(SubsetItem subsetItem) {
        }

    }


    private static class DocumentPointeur extends AbstractPointeur {

        private DocumentPointeur(Addenda addenda) {
            super(addenda);
        }

        @Override
        protected void initCurrentSubsetItem(SubsetItem subsetItem) {
        }

    }

}
