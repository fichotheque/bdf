/* FichothequeLib_Tools - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleIcon {

    public String getColor();

    public String getFontAwesomeIcon();

    public String getCharIcon();

    public default boolean hasColor() {
        return (getColor() != null);
    }

    public default boolean hasFontAweson() {
        return (getFontAwesomeIcon() != null);
    }

    public default String getCharIcon(String defaultChar) {
        String charIcon = getCharIcon();
        if (charIcon == null) {
            return defaultChar;
        }
        return charIcon;
    }

}
