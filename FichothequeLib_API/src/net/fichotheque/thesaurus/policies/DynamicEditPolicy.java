/* FichothequeLib_API - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus.policies;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.externalsource.ExternalSourceDef;


/**
 *
 * @author Vincent Calame
 */
public interface DynamicEditPolicy {


    public default boolean isLax() {
        return false;
    }


    public static interface None extends DynamicEditPolicy {

    }


    public static interface Allow extends DynamicEditPolicy {

        @Override
        public default boolean isLax() {
            return true;
        }

    }


    public static interface Check extends DynamicEditPolicy {

        public List<SubsetKey> getCheckSubseKeyList();

        @Override
        public default boolean isLax() {
            return true;
        }

    }


    public static interface Transfer extends DynamicEditPolicy {

        public SubsetKey getTransferThesaurusKey();

    }


    public static interface External extends DynamicEditPolicy {

        public ExternalSourceDef getExternalSourceDef();

    }

}
