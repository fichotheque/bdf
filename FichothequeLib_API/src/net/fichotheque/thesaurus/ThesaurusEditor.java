/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus;

import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadataEditor;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusEditor {

    /**
     * Retourne le thésaurus auquel est associé l'instance de ThesaurusEditor
     */
    public Thesaurus getThesaurus();

    public FichothequeEditor getFichothequeEditor();

    public ThesaurusMetadataEditor getThesaurusMetadataEditor();

    /**
     * si id &lt; 1, un nouvel identifiant est attribué. Si idalpha est nul ou
     * vide, un identifiant est construit à partir du numéro.
     *
     * @throws ExistingIdException si l'identifiant ou l'idalpha existent déjà
     * @throws ParseException si l'idalpha est incorrect
     */
    public Motcle createMotcle(int id, String idalpha) throws ExistingIdException, ParseException;

    public boolean putLabel(Motcle motcle, Label label);

    public boolean removeLabel(Motcle motcle, Lang lang);

    /**
     * Modifie le parent du mot-clé. Le mot-clé est placé à la fin de la liste
     * des enfants du mot-clé parent. Si parentMotcle a la valeur <em>null</em>,
     * le mot-clé est placé au premier niveau. Retourne <em>true</em> si le
     * parent a été effectivement modifié.
     */
    public boolean setParent(Motcle motcle, Motcle parentMotcle) throws ParentRecursivityException;

    /**
     * Modifie l'idalpha du motcle.
     *
     * @throws UnsupportedOperationException si le thésaurus n'est pas avec
     * idalpha
     * @throws IllegalArgumentException si nvidalpha est nul ou vide
     * @throws ParseException si l'idalpha est incorrect
     */
    public boolean setIdalpha(Motcle motcle, String newIdalpha) throws ExistingIdException, ParseException;

    /**
     * Modifie la position du mot-clé dans sa fratrie. Retourne <em>true</em>
     * s'il y a bien eu modification (autrement dit, si la nouvelle valeur de
     * l'index est différente de l'ancienne).
     *
     * @throws IndexOutOfBoundsException si index < 0 ou >=
     * getParent().getChildren().size()
     */
    public boolean setChildIndex(Motcle motcle, int index);

    /**
     * Supprime le mot-clé du thésaurus. Retourne <em>false</em> si le mot-clé
     * ne peut pas être supprimé. Pour pouvoir être supprimé un mot-clé ne doit
     * être présent dans aucun croisement avec des fiches et ne pas avoir de
     * mots-clés enfants. En revanche, les éventuelles fiches rattachées sont
     * supprimées en même temps.
     */
    public boolean removeMotcle(Motcle motcle);

    public boolean setStatus(Motcle motcle, String status);

    public default boolean putLabel(Motcle motcle, Lang lang, CharSequence charSequence) {
        CleanedString cleanedString = CleanedString.newInstance(charSequence);
        if (cleanedString != null) {
            return putLabel(motcle, LabelUtils.toLabel(lang, cleanedString));
        } else {
            return false;
        }
    }

}
