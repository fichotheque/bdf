/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus;

import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;


/**
 * Lorqu'un thésaurus utilise des identifiants alphanumériques, ceux-ci ne
 * doivent être constitué que des caractères suivants : 0-9, A-Z, a-z, _-.()[]/@
 * et l'espace ' '.
 * <p>
 * L'espace ' ' est autorisé sauf en début et en fin de chaîne et il ne peut y
 * avoir deux espaces à la suite.<p>
 * Il n'y a pas de limitation à la taille d'un identifiant alphanumérique. Les
 * caractères [] et _ sont susceptibles d'avoir des significations
 * particulières.
 *
 * @author Vincent Calame
 */
public interface Thesaurus extends Subset {

    public List<Motcle> getFirstLevelList();

    /**
     * Résultat équivalent à (Motcle) getSubsetItemById()
     */
    public Motcle getMotcleById(int id);

    /**
     *
     * @throws UnsupportedOperationException si le thésaurus n'a pas d'idalpha
     */
    public Motcle getMotcleByIdalpha(String idalpha);

    /**
     * Insensible à la casse, aux accents, etc.
     */
    public Motcle getMotcleByLabel(String labelString, Lang lang);

    /**
     * L'ordre est différent de celui de getSubsetItemList. Dans
     * getSubsetItemList, les mots-clés sont classés par identifiant alrs que
     * dans getMotcleList, c'est l'ordre d'apparition
     *
     * @return
     */
    public List<Motcle> getMotcleList();

    public default ThesaurusMetadata getThesaurusMetadata() {
        return (ThesaurusMetadata) getMetadata();
    }

    public default boolean isIdalphaType() {
        return (getThesaurusMetadata().getThesaurusType() == ThesaurusMetadata.IDALPHA_TYPE);
    }

    public default boolean isBabelienType() {
        return (getThesaurusMetadata().getThesaurusType() == ThesaurusMetadata.BABELIEN_TYPE);
    }

    public default boolean isMultiType() {
        return (getThesaurusMetadata().getThesaurusType() == ThesaurusMetadata.MULTI_TYPE);
    }

    public default Motcle seekMotcleByLabel(String labelString, Lang preferredLang) {
        Motcle motcle = getMotcleByLabel(labelString, preferredLang);
        if (motcle != null) {
            return motcle;
        }
        Lang rootLang = null;
        if (!preferredLang.isRootLang()) {
            rootLang = preferredLang.getRootLang();
            motcle = getMotcleByLabel(labelString, rootLang);
            if (motcle != null) {
                return motcle;
            }
        }
        Langs authorizedLangs = getThesaurusMetadata().getAuthorizedLangs();
        if (authorizedLangs != null) {
            for (Lang otherLang : authorizedLangs) {
                if (otherLang.equals(preferredLang)) {
                    continue;
                }
                if ((rootLang != null) && (otherLang.equals(rootLang))) {
                    continue;
                }
                motcle = getMotcleByLabel(labelString, otherLang);
                if (motcle != null) {
                    if (motcle.getLabels().getFirstLabel().getLang().equals(otherLang)) {
                        return motcle;
                    }
                }
            }
        }
        return null;
    }

}
