/* FichothequeLib_API - Copyright (c) 2006-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus;

import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Idalpha;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;


/**
 * Dans le cas des thésaurus babéliens, un mot-clé doit comporter un libellé et
 * un seul.
 *
 * @author Vincent Calame
 */
public interface Motcle extends SubsetItem {

    /**
     * Retourne le parent du mot-clé ou <em>null</em> si le mot-clé appartient
     * au premier niveau.
     */
    public Motcle getParent();

    public MotcleIcon getMotcleIcon();

    public List<Motcle> getChildList();

    public int getChildIndex();

    /**
     * Retourne l'identifiant alphanumérique du mot-clé.
     */
    public String getIdalpha();

    /**
     * Retourne le niveau du mot-clé, c'est à dire sa profondeur par rapport à
     * l'ensemble du thésaurus. Retourne 1 si le mot-clé appartient au premier
     * niveau (getParent() == null), sinon on a getLevel() ==
     * getParent().getLevel() + 1
     */
    public int getLevel();

    public String getStatus();

    public Labels getLabels();

    public default Thesaurus getThesaurus() {
        return (Thesaurus) getSubset();
    }

    public default boolean isBabelienType() {
        return ((Thesaurus) getSubset()).isBabelienType();
    }

    public default Label getBabelienLabel() {
        if (!isBabelienType()) {
            throw new UnsupportedOperationException("motcle.getThesaurus is not babelien");
        }
        Labels labels = getLabels();
        if (labels.isEmpty()) {
            throw new ImplementationException("motcle.getLabelCount() == 0 in a babélien thesaurus");
        }
        return labels.get(0);
    }

    public default String getLabelString(Lang lang) {
        return getLabelString(lang, "?");
    }

    public default String getLabelString(Lang lang, String defaultString) {
        if (isBabelienType()) {
            return getBabelienLabel().getLabelString();
        } else {
            return getLabels().seekLabelString(lang, defaultString);
        }
    }

    public default String getSignificantIdalpha() {
        String idalpha = getIdalpha();
        if (Idalpha.isSignificant(idalpha)) {
            return idalpha;
        } else {
            return null;
        }
    }

    public default boolean shouldNotCroisement(SubsetKey subsetKey) {
        switch (getStatus()) {
            case FichothequeConstants.GROUP_STATUS:
            case FichothequeConstants.OBSOLETE_STATUS:
                return true;
        }
        return CroisementUtils.isShouldNotCroisement(this, subsetKey);
    }

}
