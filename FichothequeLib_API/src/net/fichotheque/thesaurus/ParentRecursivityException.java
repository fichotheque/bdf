/* FichothequeLib_API - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus;


/**
 * Exception que le parent préssenti pour un un mot-clé se trouve faire partie de ses descendants.
 *
 * @author  Vincent Calame
 */
public class ParentRecursivityException extends Exception {

    Motcle motcle;
    Motcle candidateParent;
    int idmtcl;
    int idparentrecursif;

    public ParentRecursivityException(Motcle motcle, Motcle candidateParent) {
        this.motcle = motcle;
        this.candidateParent = candidateParent;
    }

}
