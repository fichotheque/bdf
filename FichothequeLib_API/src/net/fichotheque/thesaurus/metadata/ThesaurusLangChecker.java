/* FichothequeLib_API - Copyright (c) 2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus.metadata;

import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Langs;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusLangChecker {

    @Nullable
    public Langs getAuthorizedLangs(Thesaurus thesaurus);

}
