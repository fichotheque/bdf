/* FichothequeLib_API - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus.metadata;

import net.fichotheque.FichothequeConstants;
import net.fichotheque.Metadata;
import net.fichotheque.namespaces.BdfSpace;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.Phrase;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusMetadata extends Metadata {

    public final static short MULTI_TYPE = 1;
    public final static short IDALPHA_TYPE = 2;
    public final static short BABELIEN_TYPE = 3;

    public Thesaurus getThesaurus();

    public short getThesaurusType();

    /**
     * Retourne le tableau des langues autorisées pour les libéllés de
     * mots-clés. Retourne <em>null</em> si le thésaurus est de type babélien.
     * Si le type n'est pas babélien, la méthode doit retourner une liste avec
     * au moins une valeur. Le première valeur de la liste est considéré comme
     * la langue par défaut.
     */
    public Langs getAuthorizedLangs();

    public default boolean isWithFicheStyle() {
        Phrase phrase = getPhrases().getPhrase(FichothequeConstants.FICHESTYLE_PHRASE);
        return (phrase != null);
    }

    public default boolean isBracketsIdalphaStyle() {
        Attribute attribute = getAttributes().getAttribute(BdfSpace.IDALPHASTYLE_KEY);
        if (attribute == null) {
            return false;
        }
        return attribute.contains("brackets");
    }

    public default boolean isLongIdalphaStyle() {
        Attribute attribute = getAttributes().getAttribute(BdfSpace.IDALPHASTYLE_KEY);
        if (attribute == null) {
            return false;
        }
        return attribute.contains("long");
    }

}
