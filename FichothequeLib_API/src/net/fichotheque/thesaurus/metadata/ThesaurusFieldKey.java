/* FichothequeLib_API - Copyright (c) 2007-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus.metadata;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusFieldKey {

    private final static Map<String, ThesaurusFieldKey> internMap = new HashMap<String, ThesaurusFieldKey>();
    public final static ThesaurusFieldKey ID = new ThesaurusFieldKey("id");
    public final static ThesaurusFieldKey IDALPHA = new ThesaurusFieldKey("idalpha");
    public final static ThesaurusFieldKey PARENT_ID = new ThesaurusFieldKey("parent_id");
    public final static ThesaurusFieldKey PARENT_IDALPHA = new ThesaurusFieldKey("parent_idalpha");
    public final static ThesaurusFieldKey BABELIENLABEL = new ThesaurusFieldKey("label");
    public final static ThesaurusFieldKey BABELIENLANG = new ThesaurusFieldKey("lang");
    public final static ThesaurusFieldKey POSITION_LOCAL = new ThesaurusFieldKey("pos_loc");
    public final static ThesaurusFieldKey POSITION_GLOBAL = new ThesaurusFieldKey("pos_glob");
    public final static ThesaurusFieldKey LEVEL = new ThesaurusFieldKey("level");
    public final static ThesaurusFieldKey THIS = new ThesaurusFieldKey("this");
    public final static ThesaurusFieldKey STATUS = new ThesaurusFieldKey("status");
    public final static short SPECIAL_CATEGORY = 0;
    public final static short LABEL_CATEGORY = 1;
    private final String fieldKeyString;
    private final short category;
    private final Lang lang;

    private ThesaurusFieldKey(String fieldKeyString) {
        this.fieldKeyString = fieldKeyString;
        this.category = SPECIAL_CATEGORY;
        this.lang = null;
        intern(this);
    }

    private ThesaurusFieldKey(short category, Lang lang) {
        this(toKeyString(category, lang), category, lang);
    }

    private ThesaurusFieldKey(String fieldKeyString, short category, Lang lang) {
        this.fieldKeyString = fieldKeyString;
        this.category = category;
        this.lang = lang;
        intern(this);
    }

    private static String toKeyString(short category, Lang lang) {
        StringBuilder buf = new StringBuilder();
        if (category == LABEL_CATEGORY) {
            buf.append("label_");
        }
        buf.append(lang.toString());
        return buf.toString();
    }

    private synchronized static void intern(ThesaurusFieldKey thesaurusFieldKey) {
        internMap.put(thesaurusFieldKey.fieldKeyString, thesaurusFieldKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThesaurusFieldKey)) {
            return false;
        }
        ThesaurusFieldKey fk = (ThesaurusFieldKey) obj;
        return fk.fieldKeyString.equals(this.fieldKeyString);
    }

    @Override
    public int hashCode() {
        return fieldKeyString.hashCode();
    }

    @Override
    public String toString() {
        return fieldKeyString;
    }

    public boolean isSpecialThesaurusFieldKey() {
        return (category == SPECIAL_CATEGORY);
    }

    public boolean isLabelThesaurusFieldKey() {
        return (category == LABEL_CATEGORY);
    }

    public short getCategory() {
        return category;
    }

    public Lang getLang() {
        return lang;
    }

    /**
     *
     * @throws ParseException si s est incorrect
     */
    public static ThesaurusFieldKey parse(String s) throws ParseException {
        if (s.equals("idths")) {
            s = "id";
        } else if (s.equals("parent_idths")) {
            s = "parent_id";
        } else if (s.equals("lib")) {
            s = "label";
        }
        ThesaurusFieldKey current = (ThesaurusFieldKey) internMap.get(s);
        if (current != null) {
            return current;
        }
        String langString = null;
        if (s.startsWith("lib_")) {
            langString = s.substring(4);
        } else if (s.startsWith("label_")) {
            langString = s.substring(6);
        }
        if (langString != null) {
            try {
                Lang lang = Lang.parse(langString);
                return new ThesaurusFieldKey(LABEL_CATEGORY, lang);
            } catch (ParseException pe) {
                throw new ParseException("wrong lang :" + langString, 4);
            }
        }
        throw new ParseException("unkown thesaurusfieldkey : " + s, 0);
    }

    public static ThesaurusFieldKey toLabelThesaurusFieldKey(Lang lang) {
        return new ThesaurusFieldKey(LABEL_CATEGORY, lang);
    }

}
