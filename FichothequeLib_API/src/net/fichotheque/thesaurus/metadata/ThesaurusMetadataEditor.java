/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.thesaurus.metadata;

import net.fichotheque.MetadataEditor;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Langs;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusMetadataEditor extends MetadataEditor {

    public ThesaurusEditor getThesaurusEditor();

    /**
     * Retourne <em>true</em> si un changement a vraiment eu lieu. Un changement
     * a lieu si le contenu de la liste est différent de la liste actuelle. Un
     * changement a lieu si l'ordre change.
     *
     * @throws UnsupportedOperationException si le thésaurus est babélien
     * @throws IllegalArgumentException si langs est nul ou de longueur nulle.
     */
    public boolean setAuthorizedLangs(Langs langs);

    @Override
    public default boolean removeAttribute(AttributeKey attributeKey) {
        return getThesaurusEditor().getFichothequeEditor().removeAttribute(getMetadata(), attributeKey);
    }

    @Override
    public default boolean putAttribute(Attribute attribute) {
        return getThesaurusEditor().getFichothequeEditor().putAttribute(getMetadata(), attribute);
    }

}
