/* FichothequeLib_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format;

import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.fichotheque.include.IncludeKey;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class SubsetPathKey {

    public final static short PARENTAGE_SUBSETKEY = 1;
    public final static short CROISEMENT_INCLUDEKEY = 2;
    public final static short THESAURUS_TREE = 3;
    public final static String ANCESTORS = "ancestors";
    public final static String DESCENDANTS = "descendants";
    public final static String PARENT = "parent";
    public final static String CHILDREN = "children";
    public final static String SIBLINGS = "siblings";
    private final int length;
    private final String pathString;
    private final short[] typeArray;
    private final Object[] keyObjectArray;

    private SubsetPathKey(short[] typeArray, Object[] keyObjectArray) {
        this.typeArray = typeArray;
        this.keyObjectArray = keyObjectArray;
        this.length = typeArray.length;
        this.pathString = toString(keyObjectArray);
    }

    public int getLength() {
        return length;
    }

    public short getKeyType(int i) {
        return typeArray[i];
    }

    public Object getKeyObject(int i) {
        return keyObjectArray[i];
    }


    public SubsetKey getMatchingSubsetKey(int i) {
        Object obj = keyObjectArray[i];
        if (obj instanceof IncludeKey) {
            return ((IncludeKey) obj).getSubsetKey();
        } else if (obj instanceof SubsetKey) {
            return (SubsetKey) obj;
        } else if (i > 0) {
            return getMatchingSubsetKey(i - 1);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        return pathString;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != this.getClass()) {
            return false;
        }
        SubsetPathKey other = (SubsetPathKey) obj;
        return other.pathString.equals(this.pathString);
    }

    @Override
    public int hashCode() {
        return pathString.hashCode();
    }

    public static SubsetPathKey parse(String subsetPathString) throws ParseException {
        if ((subsetPathString == null) || (subsetPathString.isEmpty())) {
            return null;
        }
        String[] tokens = StringUtils.getTokens(subsetPathString, '/', StringUtils.EMPTY_EXCLUDE);
        int length = tokens.length;
        if (length == 0) {
            return null;
        }
        short[] typeArray = new short[length];
        Object[] keyObjectArray = new Object[length];
        for (int i = 0; i < length; i++) {
            String token = tokens[i];
            String thesaurusTreeToken = testThesaurusTree(token);
            if (thesaurusTreeToken != null) {
                typeArray[i] = THESAURUS_TREE;
                keyObjectArray[i] = thesaurusTreeToken;
            } else {
                try {
                    SubsetKey parentageSubsetKey = SubsetKey.parseParentageSubsetKey(token);
                    typeArray[i] = PARENTAGE_SUBSETKEY;
                    keyObjectArray[i] = parentageSubsetKey;
                } catch (ParseException pe) {
                    IncludeKey includeKey = IncludeKey.parse(token);
                    SubsetKey subsetKey = includeKey.getSubsetKey();
                    if (((!subsetKey.isCorpusSubset()) && (!subsetKey.isThesaurusSubset()))) {
                        throw new ParseException("Wrong includeKey: " + includeKey, 0);
                    }
                    typeArray[i] = CROISEMENT_INCLUDEKEY;
                    keyObjectArray[i] = includeKey;
                }
            }
        }
        return new SubsetPathKey(typeArray, keyObjectArray);
    }

    public static SubsetPathKey newParentagePath(SubsetKey parentageSubsetKey) {
        if (parentageSubsetKey == null) {
            throw new IllegalArgumentException("subsetKey is null");
        }
        short[] typeArray = new short[1];
        Object[] keyObjectArray = new Object[1];
        typeArray[0] = PARENTAGE_SUBSETKEY;
        keyObjectArray[0] = parentageSubsetKey;
        return new SubsetPathKey(typeArray, keyObjectArray);
    }

    private static String toString(Object[] keyObjectArray) {
        int length = keyObjectArray.length;
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                buf.append('/');
            }
            Object obj = keyObjectArray[i];
            if (obj instanceof SubsetKey) {
                buf.append(SubsetKey.toParentageString((SubsetKey) obj));
            } else {
                buf.append(obj.toString());
            }
        }
        return buf.toString();
    }

    private static String testThesaurusTree(String token) {
        switch (token) {
            case ANCESTORS:
                return ANCESTORS;
            case DESCENDANTS:
                return DESCENDANTS;
            case PARENT:
                return PARENT;
            case CHILDREN:
                return CHILDREN;
            case SIBLINGS:
                return SIBLINGS;
            default:
                return null;
        }
    }

}
