/* FichothequeLib_API - Copyright (c) 2011-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format;

import net.fichotheque.Subset;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public interface SourceLabelProvider {

    /**
     * N'est jamais nul. Au pire, propose une instance de Labels qui renvoie
     * formatSourceKey.toString() quelque soit la langue.
     */
    public Labels getLabels(Subset subset, FormatSourceKey formatSourceKey);

}
