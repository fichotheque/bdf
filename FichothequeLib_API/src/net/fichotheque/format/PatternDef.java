/* FichothequeLib_API - Copyright (c) 2012-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface PatternDef {

    public final static String IDALPHA_DIRECTVALUE = "idalpha";
    public final static String LANGS_DIRECTVALUE = "langs";
    public final static String LETTERS_DIRECTVALUE = "letters";
    public final static String POSITION_DIRECTVALUE = "position";
    public final static String TRANSFORMATION_FORMAT_DIRECTVALUE = "transformation_format";
    public final static String TRANSFORMATION_SECTION_DIRECTVALUE = "transformation_section";

    public String getName();

    @Nullable
    public String getDirectValueType();

    public List<ParameterDef> getParameterDefList();


    public static interface ParameterDef {

        public String getName();

        public List<String> getAvailableValueList();

    }

}
