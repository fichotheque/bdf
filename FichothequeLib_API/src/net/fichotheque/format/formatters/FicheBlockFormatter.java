/* FichothequeLib_API - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format.formatters;

import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.format.FormatSource;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
@FunctionalInterface
public interface FicheBlockFormatter {

    /**
     * Ne retourne jamais null.
     */
    public String formatFicheBlocks(FicheBlocks ficheBlocks, FormatSource formatSource, @Nullable SubsetKey defaultCorpusKey);

}
