/* FichothequeLib_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format.formatters;

import net.fichotheque.format.FormatSource;


/**
 * Convertisseur d'une source de données en chaine de caractères.
 * <p>
 * Classiquement, une instance de SourceFormatter va être construite à partir
 * d'une définition de formatage (via une instance de FichothequeFormatDef) mais
 * ce n'est en aucun cas obligatoire. En particulier, une extension qui voudra
 * proposer un mécanisme de formatage différent pour une source donnée.
 *
 * @author Vincent Calame
 */
@FunctionalInterface
public interface SourceFormatter {

    public String formatSource(FormatSource formatSource);

}
