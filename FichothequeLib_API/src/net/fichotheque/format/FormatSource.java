/* FichothequeLib_API - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format;

import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.exportation.table.CellEngineProvider;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.extraction.def.ExtractionDef;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.MessageLocalisation;


/**
 * Source de données d'un formatage.
 *
 *
 * @author Vincent Calame
 */
public interface FormatSource {

    public SubsetItemPointeur getSubsetItemPointeur();

    public ExtractionContext getExtractionContext();

    public Predicate<SubsetItem> getGlobalPredicate();

    public FormatContext getFormatContext();

    public CellEngineProvider getCellEngineProvider();

    public History getHistory();

    @Nullable
    public ExtractionInfo getExtractionInfo();

    public default Lang getDefaultLang() {
        return getLangContext().getDefaultLang();
    }

    public default Locale getDefaultFormatLocale() {
        return getLangContext().getDefaultFormatLocale();
    }

    public default LangContext getLangContext() {
        return getExtractionContext().getLangContext();
    }

    public default Predicate<Subset> getSubsetAccessPredicate() {
        return getExtractionContext().getSubsetAccessPredicate();
    }

    public default Fichotheque getFichotheque() {
        return getFormatContext().getFichotheque();
    }

    public default MessageLocalisation getMessageLocalisation(Lang lang) {
        return getFormatContext().getMessageLocalisationProvider().getMessageLocalisation(lang);
    }


    public interface History {

        public List<String> getPreviousFormatList();

    }


    public interface ExtractionInfo {

        public ExtractionDef getExtractionDef();

        public String getErrorLog();

    }

}
