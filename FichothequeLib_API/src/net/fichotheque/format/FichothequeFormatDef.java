/* FichothequeLib_API - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format;

import java.util.List;
import net.mapeadores.util.format.FormatDef;


/**
 * Définition d'un formatage de données issues d'une fichothèque.
 * <p>
 * Cette interface étend FormatDef en rajoutant l'information de la source des
 * données qui doivent être formatées. Cette information se présente sous la
 * forme d'une liste d'instances de FormatSourceKey. Une instance de
 * FichothequeFormatDef doit toujours proposer au moins une source de données.
 *
 * @author Vincent Calame
 */
public interface FichothequeFormatDef extends FormatDef {

    /**
     * Retourne la liste des sources de données différentes pour le formatage.
     * Doit contenir au moins un élément.
     *
     * @return la liste
     */
    public List<FormatSourceKey> getFormatSourceKeyList();

}
