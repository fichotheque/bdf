/* FichothequeLib_API - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeFormatConstants {

    public final static String NOITEM_PARAMKEY = "noitem";
    public final static String DEFAULTPROPRIETE_PARAMKEY = "defprop";
    public final static String ONLYITEM_PARAMKEY = "onlyitem";
    public final static String IDSORT_PARAMKEY = "idsort";
}
