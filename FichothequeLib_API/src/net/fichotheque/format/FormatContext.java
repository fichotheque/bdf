/* FichothequeLib_API - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.format;

import java.util.Map;
import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.format.formatters.ExtractionFormatter;
import net.fichotheque.format.formatters.FicheBlockFormatter;
import net.fichotheque.include.LiageTest;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.instruction.InstructionResolverProvider;
import net.mapeadores.util.instruction.InstructionUtils;
import net.mapeadores.util.localisation.MessageLocalisationProvider;


/**
 * Cette interface définit le contexte d'un formatage, c'est à dire tous les
 * éléments nécessaires à la résolution du formatage. Par exemple,
 * getFicheBlockFormatter() va renvoyer des formateurs basés sur des gabarits de
 * transformation.
 *
 * @author Vincent Calame
 */
public interface FormatContext {

    public final static String XML_SPECIAL_FORMATTER_NAME = "special:xml";

    /**
     * Retourne la fichothèque dans laquelle a lieu le formatage.
     */
    public Fichotheque getFichotheque();

    /**
     * Retourne une instance de InstructionResolverProvider. Celle-ci est
     * susceptible de fournir des interpétreurs alternatifs de certaines
     * instructions de formatage.
     */
    public InstructionResolverProvider getInstructionResolverProvider();

    /**
     * Renvoie le formateur par défaut si name est nul. Si name est égal à
     * XML_SPECIAL_FORMATTER_NAME, le formateur retourné ne modifie pas le code
     * XML initial et le renvoie tel que.
     */
    public ExtractionFormatter getExtractionFormatter(String name, Map<String, Object> options);

    /**
     * Renvoie le formateur par défaut si name est nul. Si name est égal à
     * XML_SPECIAL_FORMATTER_NAME, le formateur retourné ne modifie pas le code
     * XML initial et le renvoie tel que.
     */
    public FicheBlockFormatter getFicheBlockFormatter(String name, Map<String, Object> options);

    /**
     * Retourne le test de liage pour le corpus indiqué. Il est notamment
     * utilisé pour la source de formatage IncludeKey.LIAGE
     * (<code>liage</code>).
     */
    public LiageTest getLiageTest(Corpus corpus);

    public MessageLocalisationProvider getMessageLocalisationProvider();

    /**
     * Retourne des objets liés au contexte d'exécution par l'implémentation de
     * FormatContext.
     *
     * @param objectName
     * @return
     */
    @Nullable
    public Object getContextObject(String objectName);

    public default FicheBlockFormatter getFicheBlockFormatter(String name) {
        return getFicheBlockFormatter(name, InstructionUtils.EMPTY_OPTIONS);
    }

    public default ExtractionFormatter getExtractionFormatter(String name) {
        return getExtractionFormatter(name, InstructionUtils.EMPTY_OPTIONS);
    }

}
