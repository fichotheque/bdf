/* FichothequeLib_API - Copyright (c) 2007-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;

import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.croisement.Croisements;


/**
 *
 * @author Vincent Calame
 */
public interface Subset {

    public Fichotheque getFichotheque();

    public SubsetKey getSubsetKey();

    public Metadata getMetadata();

    public SubsetItem getSubsetItemById(int id);

    public int size();

    public List<Corpus> getSatelliteCorpusList();

    public List<SubsetItem> getSubsetItemList();

    public default String getSubsetName() {
        return getSubsetKey().getSubsetName();
    }

    public default String getSubsetKeyString() {
        return getSubsetKey().getKeyString();
    }

    public default boolean isRemoveable() {
        return getFichotheque().isRemoveable(this);
    }

    public default Croisements getCroisements(SubsetItem subsetItem) {
        return getFichotheque().getCroisements(subsetItem, this);
    }

}
