/* FichothequeLib_API - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.album;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumConstants {

    public final static String MINI_DIMNAME = "_mini";
    public final static String ORIGINAL_DIMNAME = "_original";
    public final static short JPG_FORMATTYPE = 1;
    public final static short PNG_FORMATTYPE = 2;
    public final static String FIXEDWIDTH_DIMTYPE = "fixed-width";
    public final static String FIXEDHEIGHT_DIMTYPE = "fixed-height";
    public final static String MAXWIDTH_DIMTYPE = "max-width";
    public final static String MAXHEIGHT_DIMTYPE = "max-height";
    public final static String MAXDIM_DIMTYPE = "max-dim";
    public final static short MINI_SPECIALDIM = 201;
    public final static short ORIGINAL_SPECIALDIM = 202;
    public final static String VIGNETTE_USAGE = "vignette";
    public final static String DETAIL_USAGE = "detail";
}
