/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.album;

import java.io.InputStream;
import net.fichotheque.SubsetItem;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.utils.AlbumUtils;


/**
 *
 * @author Vincent Calame
 */
public interface Illustration extends SubsetItem {

    public short getFormatType();

    public int getOriginalWidth();

    public int getOriginalHeight();

    public InputStream getInputStream(short specialDim);

    /**
     * AlbumDim doit être une dimension définie pour l'album auquel appartient
     * Illustration.
     *
     * @param albumDim dimension de l'image (si nul, renvoie l'image originale)
     */
    public InputStream getInputStream(AlbumDim albumDim);


    public default Album getAlbum() {
        return (Album) getSubset();
    }

    public default String getFormatTypeString() {
        return AlbumUtils.formatTypeToString(getFormatType());
    }

    public default String getFileName() {
        return getSubsetName() + "-" + getId() + "." + getFormatTypeString();
    }

}
