/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.album.metadata;

import java.text.ParseException;
import net.fichotheque.ExistingNameException;
import net.fichotheque.MetadataEditor;
import net.fichotheque.album.AlbumEditor;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumMetadataEditor extends MetadataEditor {

    public AlbumEditor getAlbumEditor();

    public AlbumMetadata getAlbumMetadata();

    public AlbumDim createAlbumDim(String name, String type) throws ExistingNameException, ParseException;

    public boolean removeAlbumDim(AlbumDim albumDim);

    public boolean setDim(AlbumDim albumDim, int width, int height);

    @Override
    public default boolean removeAttribute(AttributeKey attributeKey) {
        return getAlbumEditor().getFichothequeEditor().removeAttribute(getMetadata(), attributeKey);
    }

    @Override
    public default boolean putAttribute(Attribute attribute) {
        return getAlbumEditor().getFichothequeEditor().putAttribute(getMetadata(), attribute);
    }

}
