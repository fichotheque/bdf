/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.album.metadata;

import java.util.List;
import net.fichotheque.Metadata;
import net.fichotheque.album.Album;
import net.mapeadores.util.awt.ResizeInfo;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumMetadata extends Metadata {

    public Album getAlbum();

    public List<AlbumDim> getAlbumDimList();

    public AlbumDim getAlbumDimByName(String name);

    public ResizeInfo getResizeInfo(short specialDim);

}
