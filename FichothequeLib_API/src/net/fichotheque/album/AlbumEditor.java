/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.album;

import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.album.metadata.AlbumMetadataEditor;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumEditor {

    public Album getAlbum();

    public FichothequeEditor getFichothequeEditor();

    public AlbumMetadataEditor getAlbumMetadataEditor();

    public Illustration createIllustration(int id, short formatType) throws ExistingIdException;

    public void updateIllustration(Illustration illustration, InputStream inputStream, short formatType) throws IOException, ErrorMessageException;

    public boolean removeIllustration(Illustration illustration);

}
