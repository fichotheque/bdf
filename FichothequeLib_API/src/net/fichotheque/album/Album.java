/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.album;

import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.album.metadata.AlbumMetadata;


/**
 *
 * @author Vincent Calame
 */
public interface Album extends Subset {

    public Illustration getIllustrationById(int id);

    public List<Illustration> getIllustrationList();

    public default AlbumMetadata getAlbumMetadata() {
        return (AlbumMetadata) getMetadata();
    }

}
