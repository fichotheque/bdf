/* FichothequeLib_API - Copyright (c) 2012-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;

import net.fichotheque.corpus.fiche.ContentChecker;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeEditorProvider {

    public Fichotheque getFichotheque();

    public FichothequeEditor newFichothequeEditor(EditOrigin editOrigin);

    public ContentChecker getContentChecker();

}
