/* FichothequeLib_API - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public interface MetadataEditor {

    public Metadata getMetadata();

    /**
     * Si name est nul, cela correspond à la phrase de titre
     *
     * Retourne false si name est incorrect ou s'il n'y pas de changement
     */
    public boolean putLabel(@Nullable String name, Label label);

    public boolean removeLabel(@Nullable String name, Lang lang);

    public boolean removeAttribute(AttributeKey attributeKey);

    public boolean putAttribute(Attribute attribute);

    public default boolean changeLabels(@Nullable String name, LabelChange labelChange) {
        boolean done = false;
        for (Label label : labelChange.getChangedLabels()) {
            boolean stepdone = putLabel(name, label);
            if (stepdone) {
                done = true;
            }
        }
        for (Lang lang : labelChange.getRemovedLangList()) {
            boolean stepdone = removeLabel(name, lang);
            if (stepdone) {
                done = true;
            }
        }
        return done;
    }

    public default boolean changeAttributes(AttributeChange attributeChange) {
        boolean done = false;
        for (Attribute attribute : attributeChange.getChangedAttributes()) {
            boolean stepdone = putAttribute(attribute);
            if (stepdone) {
                done = true;
            }
        }
        for (AttributeKey removedAttributeKey : attributeChange.getRemovedAttributeKeyList()) {
            boolean stepdone = removeAttribute(removedAttributeKey);
            if (stepdone) {
                done = true;
            }
        }
        return done;
    }

}
