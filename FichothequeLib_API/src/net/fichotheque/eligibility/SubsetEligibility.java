/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.eligibility;

import java.util.function.Predicate;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public interface SubsetEligibility {

    public default boolean accept(Subset subset) {
        return accept(subset.getSubsetKey());
    }

    public boolean accept(SubsetKey subsetKey);

    public Predicate<SubsetItem> getPredicate(Subset subset);

}
