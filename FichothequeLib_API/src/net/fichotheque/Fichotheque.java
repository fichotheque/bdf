/* FichothequeLib_API - Copyright (c) 2006-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;

import java.util.List;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.album.Album;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.croisement.CroisementKey;
import net.fichotheque.croisement.CroisementRevision;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.metadata.FichothequeMetadata;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.thesaurus.Thesaurus;


/**
 *
 * @author Vincent Calame
 */
public interface Fichotheque {

    public FichothequeMetadata getFichothequeMetadata();

    public Subset getSubset(SubsetKey subsetKey);

    public Croisement getCroisement(SubsetItem subsetItem1, SubsetItem subsetItem2);

    public CroisementRevision getCroisementRevision(CroisementKey croisementKey, String revisionName);

    /**
     * Liste des croisements entre un élément et une collection. Cette liste
     * peut être synchronisée, sans que cela soit obligatoire, il est préférable
     * de ne pas faire de modification sur les croisements lorsqu'on fait une
     * itération dessus.
     */
    public Croisements getCroisements(SubsetItem subsetItem, Subset subset);

    public void addFichothequeListener(FichothequeListener fichothequeListener);

    public void removeFichothequeListener(FichothequeListener fichothequeListener);

    public boolean isRemoveable(Subset subset);

    public boolean isRemoveable(SubsetItem subsetItem);

    /**
     * Liste non synchronisée (immutable)
     *
     * @return
     */
    public List<Corpus> getCorpusList();

    public List<Thesaurus> getThesaurusList();

    public List<Sphere> getSphereList();

    public List<Album> getAlbumList();

    public List<Addenda> getAddendaList();

    public default boolean containsSubset(SubsetKey subsetKey) {
        return (getSubset(subsetKey) != null);
    }

}
