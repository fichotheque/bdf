/* FichothequeLib_API - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.alias;

import net.fichotheque.corpus.metadata.CorpusField;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public interface AliasChecker {

    public final static short MANDATORY = 2;
    public final static short OPTIONNAL = 1;
    public final static short UNKNOWN = 0;

    public short checkCorpusAlias(String alias);

    public short checkThesaurusAlias(String alias);

    public short checkFieldAlias(String corpusAlias, String fieldAlias);

    public boolean testFieldValidity(String alias, CorpusField corpusField, MessageHandler messageHandler);

}
