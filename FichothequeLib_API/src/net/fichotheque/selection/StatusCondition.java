/* FichothequeLib_API - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.Set;


/**
 *
 * @author Vincent Calame
 */
public interface StatusCondition {

    public Set<String> getStatusSet();

    public default boolean accept(String status) {
        return getStatusSet().contains(status);
    }

}
