/* FichothequeLib_API - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.croisement.Croisement;


/**
 *
 * @author Vincent Calame
 */
public interface IllustrationSelector extends Predicate<Illustration> {

    public List<Album> getAlbumList();

    public Croisement isSelected(Illustration illustration, Croisement croisement);

}
