/* FichothequeLib_API - Copyright (c) 2016-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface PeriodCondition {

    public final static String DATE_TYPE = "date";
    public final static String ANY_TYPE = "any";
    public final static String SAME_TYPE = "same";
    public final static String ANY_CHAR = "*";

    /**
     * Doit retourner DATE_TYPE ou ANY_TYPE
     *
     * @return
     */
    public String getStartType();

    /**
     * Non nul si type = DATE_TYPE, nul sinon
     *
     * @return
     */
    public FuzzyDate getStartDate();

    /**
     * Doit retourner DATE_TYPE, ANY_TYPE ou SAME_TYPE
     *
     * @return
     */
    public String getEndType();

    /**
     * Non nul si type = DATE_TYPE, nul sinon
     *
     * @return
     */
    public FuzzyDate getEndDate();

    public boolean isOnCreationDate();

    public boolean isOnModificationDate();

    public List<FieldKey> getFieldKeyList();

    public default String getStartString() {
        switch (getStartType()) {
            case PeriodCondition.ANY_TYPE:
                return PeriodCondition.ANY_CHAR;
            case PeriodCondition.DATE_TYPE:
                return getStartDate().toString();
            default:
                throw new IllegalStateException("getStartType() is wrong: " + getStartType());
        }
    }

    public default String getEndString() {
        switch (getEndType()) {
            case PeriodCondition.ANY_TYPE:
                return PeriodCondition.ANY_CHAR;
            case PeriodCondition.DATE_TYPE:
                return getEndDate().toString();
            case PeriodCondition.SAME_TYPE:
                return PeriodCondition.SAME_TYPE;
            default:
                throw new IllegalStateException("getEndType() is wrong: " + getEndType());
        }
    }

}
