/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
public interface DocumentQuery {

    public List<SubsetKey> getAddendaKeyList();

    @Nullable
    public TextCondition getNameCondition();

    public default boolean isEmpty() {
        if (!getAddendaKeyList().isEmpty()) {
            return false;
        }
        if (getNameCondition() != null) {
            return false;
        }
        return true;
    }

}
