/* FichothequeLib_API - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.Set;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface SubsetCondition {

    public boolean isExclude();

    public boolean isWithCurrent();

    public Set<SubsetKey> getSubsetKeySet();

    public default boolean isAll() {
        return ((getSubsetKeySet().isEmpty()) && (!isWithCurrent()));
    }

    public default boolean accept(SubsetKey subsetKey) {
        return accept(subsetKey, null);
    }

    public default boolean accept(SubsetKey subsetKey, @Nullable SubsetKey currentSubsetKey) {
        Set<SubsetKey> set = getSubsetKeySet();
        if ((isWithCurrent()) && (currentSubsetKey != null)) {
            if (subsetKey.equals(currentSubsetKey)) {
                return true;
            }
            if (set.isEmpty()) {
                return false;
            }
        }
        if (set.isEmpty()) {
            return true;
        }
        boolean here = set.contains(subsetKey);
        if (isExclude()) {
            return !here;
        } else {
            return here;
        }
    }


}
