/* FichothequeLib_API - Copyright (c) 2017-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.function.Predicate;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisationProvider;


/**
 *
 * @author Vincent Calame
 */
public interface SelectionContext {

    public Fichotheque getFichotheque();

    public MessageLocalisationProvider getMessageLocalisationProvider();

    public Lang getWorkingLang();

    public Predicate<Subset> getSubsetAccessPredicate();

    public Predicate<FicheMeta> getFichePredicate();

    @Nullable
    public Corpus getCurrentCorpus();

}
