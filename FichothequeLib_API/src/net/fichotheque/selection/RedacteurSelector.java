/* FichothequeLib_API - Copyright (c) 2007-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface RedacteurSelector extends Predicate<Redacteur> {

    public List<Sphere> getSphereList();

    @Nullable
    public Croisement isSelected(Redacteur redacteur, Croisement croisement);

}
