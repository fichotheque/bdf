/* FichothequeLib_API - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface FicheCondition {

    public String getLogicalOperator();

    public List<FicheCondition.Entry> getEntryList();


    public interface Entry {

        public FicheQuery getFicheQuery();

        @Nullable
        public CroisementCondition getCroisementCondition();

        public boolean includeSatellites();

    }

}
