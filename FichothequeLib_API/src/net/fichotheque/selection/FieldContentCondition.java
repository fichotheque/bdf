/* FichothequeLib_API - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
public interface FieldContentCondition {

    public final static String TITRE_SCOPE = "titre";
    public final static String ENTETE_SCOPE = "entete";
    public final static String FICHE_SCOPE = "fiche";
    public final static String SELECTION_SCOPE = "selection";

    public String getScope();

    public List<FieldKey> getFieldKeyList();

    public TextCondition getTextCondition();

    public default boolean isWithFieldKeyList() {
        return (getScope().equals(SELECTION_SCOPE));
    }

    public static String checkScope(String scope) {
        switch (scope) {
            case TITRE_SCOPE:
            case ENTETE_SCOPE:
            case FICHE_SCOPE:
            case SELECTION_SCOPE:
                return scope;
            default:
                throw new IllegalArgumentException("Unknown scope");
        }
    }

}
