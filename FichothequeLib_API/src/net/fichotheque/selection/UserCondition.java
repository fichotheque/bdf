/* FichothequeLib_API - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public interface UserCondition {

    public final static String FILTER_NONE = "none";
    public final static String FILTER_ANY = "any";
    public final static String FILTER_SOME = "some";

    public String getFilter();

    public List<Entry> getEntryList();

    public default boolean isSome() {
        return (getFilter().equals(FILTER_SOME));
    }


    public interface Entry {

        public SubsetKey getSphereKey();

        public default String getSphereName() {
            return getSphereKey().getSubsetName();
        }

    }


    public interface LoginEntry extends Entry {

        public String getLogin();

    }


    public interface IdEntry extends Entry {

        public int getId();

    }

}
