/* FichothequeLib_API - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeQueries {

    public List<FicheQuery> getFicheQueryList();

    public List<MotcleQuery> getMotcleQueryList();

    public default boolean isEmpty() {
        return (getFicheQueryList().isEmpty()) && (getMotcleQueryList().isEmpty());
    }

}
