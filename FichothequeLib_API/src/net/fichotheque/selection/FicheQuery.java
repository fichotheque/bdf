/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface FicheQuery {

    public final static String DISCARDFILTER_ALL = "all";
    public final static String DISCARDFILTER_NONE = "none";
    public final static String DISCARDFILTER_ONLY = "only";

    public SubsetCondition getCorpusCondition();

    public MotcleCondition getMotcleCondition();

    public FicheCondition getFicheCondition();

    @Nullable
    public FieldContentCondition getFieldContentCondition();

    @Nullable
    public PeriodCondition getPeriodCondition();

    @Nullable
    public RangeCondition getIdRangeCondition();

    @Nullable
    public UserCondition getUserCondition();

    public String getDiscardFilter();

    public boolean isWithGeoloc();

    public static String checkDiscardFilter(String discardFilter) {
        switch (discardFilter) {
            case DISCARDFILTER_ALL:
            case DISCARDFILTER_NONE:
            case DISCARDFILTER_ONLY:
                return discardFilter;

            default:
                throw new IllegalArgumentException("Unknown discardFilter: " + discardFilter);
        }
    }

    public default boolean isEmpty() {
        if (!isOnlyCorpusCondition()) {
            return false;
        }
        return getCorpusCondition().isAll();
    }

    public default boolean isOnlyCorpusCondition() {
        if (getIdRangeCondition() != null) {
            return false;
        }
        if (getMotcleCondition() != null) {
            return false;
        }
        if (getFicheCondition() != null) {
            return false;
        }
        if (getFieldContentCondition() != null) {
            return false;
        }
        if (getUserCondition() != null) {
            return false;
        }
        if (isWithGeoloc()) {
            return false;
        }
        if (getPeriodCondition() != null) {
            return false;
        }
        return true;
    }

}
