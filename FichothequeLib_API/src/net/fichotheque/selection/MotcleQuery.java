/* FichothequeLib_API - Copyright (c) 2006-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleQuery {

    public final static String SCOPE_IDALPHA_WITHOUT = "idalpha_without";
    public final static String SCOPE_IDALPHA_ONLY = "idalpha_only";
    public final static String SCOPE_IDALPHA_WITH = "idalpha_with";

    public SubsetCondition getThesaurusCondition();

    @Nullable
    public RangeCondition getIdRangeCondition();

    @Nullable
    public RangeCondition getLevelRangeCondition();

    @Nullable
    public ContentCondition getContentCondition();

    @Nullable
    public FicheCondition getFicheCondition();

    @Nullable
    public StatusCondition getStatusCondition();

    public static String checkScope(String scope) {
        switch (scope) {
            case SCOPE_IDALPHA_WITHOUT:
                return SCOPE_IDALPHA_WITHOUT;
            case SCOPE_IDALPHA_ONLY:
                return SCOPE_IDALPHA_ONLY;
            case SCOPE_IDALPHA_WITH:
                return SCOPE_IDALPHA_WITH;
            default:
                throw new IllegalArgumentException("Unknwon scope: " + scope);
        }
    }

    public default boolean isEmpty() {
        if (hasOtherConditions(true)) {
            return false;
        }
        return getThesaurusCondition().isAll();
    }

    public default boolean hasOtherConditions(boolean includeContentCondition) {
        if (getIdRangeCondition() != null) {
            return true;
        }
        if (getLevelRangeCondition() != null) {
            return true;
        }
        if (getFicheCondition() != null) {
            return true;
        }
        if (getStatusCondition() != null) {
            return true;
        }
        if ((includeContentCondition) && (getContentCondition() != null)) {
            return true;
        }
        return false;
    }


    public static interface ContentCondition {

        public String getScope();

        public TextCondition getTextCondition();

    }

}
