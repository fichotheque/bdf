/* FichothequeLib_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.selection;

import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public interface SelectionDef {

    public String getName();

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public FichothequeQueries getFichothequeQueries();

    public default String getTitle(Lang preferredLang) {
        return getTitleLabels().seekLabelString(preferredLang, getName());
    }

}
