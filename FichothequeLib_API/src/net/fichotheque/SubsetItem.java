/* FichothequeLib_API - Copyright (c) 2008-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;

import net.fichotheque.croisement.Croisements;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public interface SubsetItem {

    public Subset getSubset();

    public int getId();

    public Attributes getAttributes();

    public default SubsetKey getSubsetKey() {
        return getSubset().getSubsetKey();
    }

    public default String getSubsetName() {
        return getSubset().getSubsetName();
    }

    public default Fichotheque getFichotheque() {
        return getSubset().getFichotheque();
    }

    public default boolean isRemoveable() {
        return getFichotheque().isRemoveable(this);
    }

    public default Croisements getCroisements(Subset subset) {
        return getFichotheque().getCroisements(this, subset);
    }

    /**
     * Chaine sous la forme getSubsetKey.getSubsetName() + "/" + getId()
     */
    public default String getGlobalId() {
        return FichothequeUtils.toGlobalId(getSubsetKey(), getId());
    }

}
