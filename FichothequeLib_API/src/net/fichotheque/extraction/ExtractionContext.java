/* FichothequeLib_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction;

import java.util.function.Predicate;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.extraction.run.ExtractorProvider;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.policies.PolicyProvider;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.MessageLocalisationProvider;
import net.mapeadores.util.mimetype.MimeTypeResolver;


/**
 *
 * @author Vincent Calame
 */
public interface ExtractionContext {

    public Fichotheque getFichotheque();

    public LangContext getLangContext();

    public LinkAnalyser getLinkAnalyser();

    public ExtractorProvider getExtractorProvider();

    public MimeTypeResolver getMimeTypeResolver();

    public SyntaxResolver getSyntaxResolver();

    public MessageLocalisationProvider getMessageLocalisationProvider();

    public PermissionSummary getPermissionSummary();

    public DataResolverProvider getDataResolverProvider();

    public PolicyProvider getPolicyProvider();

    public default Predicate<Subset> getSubsetAccessPredicate() {
        return getPermissionSummary().getSubsetAccessPredicate();
    }

}
