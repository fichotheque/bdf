/* FichothequeLib_API - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import java.util.List;
import net.fichotheque.extraction.def.ThesaurusExtractDef;
import net.fichotheque.thesaurus.Thesaurus;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusExtractResult {

    public ThesaurusExtractDef getThesaurusExtractDef();

    public List<Entry> getEntryList();


    public interface Entry {

        public Thesaurus getThesaurus();

        public List<MotcleExtractInfo> getMotcleExtractInfoList();

    }

}
