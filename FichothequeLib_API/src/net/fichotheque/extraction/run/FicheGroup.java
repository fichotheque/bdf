/* FichothequeLib_API - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import java.util.Map;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.extraction.def.TagNameInfo;


/**
 *
 * @author Vincent Calame
 */
public interface FicheGroup {

    public String getName();

    public TagNameInfo getTagNameInfo();

    public FicheItem[] getMatchingFicheItemArray();

    public Map<String, String> getAttributesMap();

    public boolean isBottomGroup();

    /**
     * si isBottomGroup() = false, sinon retourne null
     */
    public FicheGroup[] getSubgroupArray();

    /**
     * si isBottomGroup() = true, sinon retourne null
     */
    public FicheExtractInfo[] getFicheExtractInfoArray();

}
