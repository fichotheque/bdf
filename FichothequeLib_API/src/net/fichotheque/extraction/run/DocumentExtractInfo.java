/* FichothequeLib_API - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import net.fichotheque.addenda.Document;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.def.DocumentFilter;


/**
 *
 * @author Vincent Calame
 */
public interface DocumentExtractInfo {

    public Document getDocument();

    public DocumentFilter getDocumentFilter();

    public Croisement getCroisement();

}
