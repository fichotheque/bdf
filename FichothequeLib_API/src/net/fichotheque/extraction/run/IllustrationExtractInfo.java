/* FichothequeLib_API - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import net.fichotheque.album.Illustration;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.def.IllustrationFilter;


/**
 *
 * @author Vincent Calame
 */
public interface IllustrationExtractInfo {

    public Illustration getIllustration();

    public IllustrationFilter getIllustrationFilter();

    public Croisement getCroisement();

}
