/* FichothequeLib_API - Copyright (c) 2010-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.extraction.def.GroupParams;


/**
 *
 * @author Vincent Calame
 */
public interface FicheItemGroupParams extends GroupParams {

    public FieldKey getFieldKey();

}
