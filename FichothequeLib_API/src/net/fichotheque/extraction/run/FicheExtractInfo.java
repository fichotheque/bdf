/* FichothequeLib_API - Copyright (c) 2006-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.def.FicheFilter;


/**
 *
 * Encapsule les informations nécessaires à l'écriture de l'extraction d'une
 * fiche, à savoir la fiche concernée (sous forme FicheMeta) et les informations
 * de filtre et le croisement dans lequel s'inscrit la fiche (dans les cas où
 * c'est pertinent).
 *
 * @author Vincent Calame
 */
public interface FicheExtractInfo {

    public FicheFilter getFicheFilter();

    public FicheMeta getFicheMeta();

    public Croisement getCroisement();

    public Object getGroupClauseObject();

}
