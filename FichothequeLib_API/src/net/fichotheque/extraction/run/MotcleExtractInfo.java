/* FichothequeLib_API - Copyright (c) 2006-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.def.MotcleFilter;
import net.fichotheque.thesaurus.Motcle;


/**
 * Encapsule les informations nécessaires à l'écriture de l'extraction d'un
 * mot-clé, à savoir le mot-clé concerné et les informations de filtre et de
 * croisement dans lequel s'inscrit le mot-clé (dans les cas où c'est pertinent.
 *
 * @author Vincent Calame
 */
public interface MotcleExtractInfo {

    public Motcle getMotcle();

    public MotcleFilter getMotcleFilter();

    public Croisement getCroisement();

}
