/* FichothequeLib_API - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.run;

import net.fichotheque.extraction.def.AddendaExtractDef;
import net.fichotheque.extraction.def.AlbumExtractDef;
import net.fichotheque.extraction.def.CorpusExtractDef;
import net.fichotheque.extraction.def.ThesaurusExtractDef;


/**
 *
 * @author Vincent Calame
 */
public interface ExtractorProvider {

    public AddendaExtractor getAddendaExtractor(AddendaExtractDef addendaExtractDef);

    public AlbumExtractor getAlbumExtractor(AlbumExtractDef albumExtractDef);

    public CorpusExtractor getCorpusExtractor(CorpusExtractDef corpusExtractDef);

    public ThesaurusExtractor getThesaurusExtractor(ThesaurusExtractDef thesaurusExtractDef);

}
