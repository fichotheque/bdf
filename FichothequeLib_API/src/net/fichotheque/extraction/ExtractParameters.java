/* FichothequeLib_API - Copyright (c) 2022-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction;

import java.util.function.Predicate;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.extraction.filterunit.FilterUnit;


/**
 *
 * @author Vincent Calame
 */
public interface ExtractParameters {

    public ExtractionContext getExtractionContext();

    public int getExtractVersion();

    public boolean isWithEmpty();

    public Predicate<FicheMeta> getFichePredicate();

    public IrefConverter newIrefConverter();

    public boolean isWithPosition();

    public default boolean hideIfEmpty(FilterUnit filterUnit) {
        boolean defaultValue = !isWithEmpty();
        if (filterUnit != null) {
            return filterUnit.hideEmpty(defaultValue);
        } else {
            return defaultValue;
        }
    }

}
