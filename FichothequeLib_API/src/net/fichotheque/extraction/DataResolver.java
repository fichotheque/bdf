/* FichothequeLib_API - Copyright (c) 2023-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction;

import java.io.IOException;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public interface DataResolver {

    public String getType();

    public Writer getWriter(SubsetItemPointeur pointeur, LangContext langContext);


    public interface Writer {

        public boolean isEmpty();

        public void write(XMLWriter xmlWriter) throws IOException;

    }

}
