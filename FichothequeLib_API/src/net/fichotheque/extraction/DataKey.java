/* FichothequeLib_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction;

import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public final class DataKey {

    private final SubsetKey subsetKey;
    private final String name;
    private final String keyString;

    public DataKey(SubsetKey subsetKey, String name) {
        if (subsetKey == null) {
            throw new IllegalArgumentException("subsetKey is null");
        }
        if (name == null) {
            throw new IllegalArgumentException("name is null");
        }
        this.subsetKey = subsetKey;
        this.name = name;
        this.keyString = subsetKey + "/" + name;
    }

    public SubsetKey getSubsetKey() {
        return subsetKey;
    }

    public String getName() {
        return name;
    }

    public String getKeyString() {
        return keyString;
    }

    public String toString() {
        return keyString;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        DataKey otherKey = (DataKey) other;
        return (otherKey.keyString.equals(this.keyString));
    }

    @Override
    public int hashCode() {
        return keyString.hashCode();
    }

}
