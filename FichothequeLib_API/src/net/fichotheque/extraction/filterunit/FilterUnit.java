/* FichothequeLib_API - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.filterunit;

import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.FilterParameters;


/**
 *
 * @author Vincent Calame
 */
public interface FilterUnit extends FilterParameters {


    public default boolean hideEmpty(boolean defaultValue) {
        String hide = getFirstValue(ExtractionConstants.HIDE_PARAM);
        if (hide != null) {
            switch (hide) {
                case ExtractionConstants.NEVER_HIDE:
                    return false;
                case ExtractionConstants.EMPTY_HIDE:
                    return true;
            }
        }
        return defaultValue;
    }

}
