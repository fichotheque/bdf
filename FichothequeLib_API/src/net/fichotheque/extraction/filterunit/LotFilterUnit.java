/* FichothequeLib_API - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.filterunit;

import java.util.List;
import net.fichotheque.extraction.def.TagNameInfo;


/**
 *
 * @author Vincent Calame
 */
public interface LotFilterUnit extends FilterUnit {

    public TagNameInfo getTagNameInfo();

    public List<FilterUnit> getFilterUnitList();

}
