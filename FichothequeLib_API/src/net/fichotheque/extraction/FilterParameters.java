/* FichothequeLib_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction;

import java.util.List;
import java.util.Set;


/**
 *
 * @author Vincent Calame
 */
public interface FilterParameters {

    public Set<String> getParameterNameSet();

    public default boolean isEmpty() {
        return getParameterNameSet().isEmpty();
    }

    public default boolean hasParameters() {
        return !getParameterNameSet().isEmpty();
    }


    /**
     * Ne renvoie pas nul mais une liste vide.
     *
     * @param name
     * @return
     */
    public List<String> getParameter(String name);

    public default String getFirstValue(String name) {
        List<String> list = getParameter(name);
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(0);
        }
    }

    public default int getIntValue(String name) {
        List<String> list = getParameter(name);
        if (list.isEmpty()) {
            return 0;
        } else {
            try {
                return Integer.parseInt(list.get(0));
            } catch (NumberFormatException nfe) {
                return 0;
            }
        }
    }

}
