/* FichothequeLib_API - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.def;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.selection.FicheCondition;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusExtractDef extends SubsetExtractDef {

    public final static String GROUP_CLAUSE = "group";
    public final static String PACK_CLAUSE = "pack";
    public final static String TITLE_CLAUSE = "title";
    public final static String DESCENDANTAXIS_BOOLEAN = "descendantaxis";
    public final static String MASTER_BOOLEAN = "master";
    public final static String HIDEEMPTY_BOOLEAN = "hideempty";

    @Nullable
    public String getName();

    public TagNameInfo getTagNameInfo();

    public FicheFilter getFicheFilter();

    public List<FicheCondition.Entry> getConditionEntryList();

    public boolean hasClauseObjects();

    @Nullable
    public Object getClauseObject(String name);

    public boolean hasBooleanParameters();

    public boolean getBooleanParameter(String name);

    @Override
    public default short getCategory() {
        return SubsetKey.CATEGORY_CORPUS;
    }

    public default GroupClause getGroupClause() {
        return (GroupClause) getClauseObject(GROUP_CLAUSE);
    }

    public default PackClause getPackClause() {
        return (PackClause) getClauseObject(PACK_CLAUSE);
    }

    public default TitleClause getTitleClause() {
        return (TitleClause) getClauseObject(TITLE_CLAUSE);
    }

    public default boolean isMaster() {
        return getBooleanParameter(MASTER_BOOLEAN);
    }

    public default boolean isDescendantAxis() {
        return getBooleanParameter(DESCENDANTAXIS_BOOLEAN);
    }

}
