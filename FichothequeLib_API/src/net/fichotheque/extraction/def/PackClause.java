/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.def;


/**
 *
 * @author Vincent Calame
 */
public interface PackClause {

    public TagNameInfo getTagNameInfo();

    /**
     * @return valeur 0 ou > 1
     */
    public int getModulo();

    /**
     *
     * @return valeur > 0
     */
    public int getPackSize();

}
