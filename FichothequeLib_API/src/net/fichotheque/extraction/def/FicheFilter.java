/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.def;

import java.util.List;
import net.fichotheque.extraction.filterunit.FilterUnit;


/**
 *
 * @author Vincent Calame
 */
public interface FicheFilter {

    public boolean isWithCorpsdefiche();

    public List<FilterUnit> getFilterUnitList();

    public TagNameInfo getTagNameInfo();

    public default int getCellMax() {
        return -1;
    }

}
