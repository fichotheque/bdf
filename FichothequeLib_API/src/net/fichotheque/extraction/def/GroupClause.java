/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.def;

import net.fichotheque.extraction.ExtractionConstants;


/**
 *
 * @author Vincent Calame
 */
public interface GroupClause {

    public String getGroupType();

    public String getSortOrder();

    public TagNameInfo getTagNameInfo();

    public GroupParams getGroupParams();

    public GroupClause getSubGroupClause();

    public default boolean isAscendingOrder() {
        return (getSortOrder().equals(ExtractionConstants.ASCENDING_SORT));
    }

}
