/* FichothequeLib_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.def;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.selection.IllustrationCondition;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface AlbumExtractDef extends SubsetExtractDef {

    @Nullable
    public String getName();

    public TagNameInfo getTagNameInfo();

    public IllustrationFilter getIllustrationFilter();

    public List<IllustrationCondition.Entry> getConditionEntryList();

    @Override
    public default short getCategory() {
        return SubsetKey.CATEGORY_ALBUM;
    }

}
