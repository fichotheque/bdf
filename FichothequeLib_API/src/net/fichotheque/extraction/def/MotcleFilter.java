/* FichothequeLib_API - Copyright (c) 2006-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.def;

import java.util.List;
import net.fichotheque.extraction.filterunit.FilterUnit;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleFilter {

    public final static short DEFAULT_TYPE = 1;
    public final static short PARENT_TYPE = 2;
    public final static short PREVIOUS_TYPE = 3;
    public final static short NEXT_TYPE = 4;

    public short getType();

    public TagNameInfo getTagNameInfo();

    public boolean isNoneFiltering();

    public boolean withIcon();

    public boolean withLevel();

    public boolean withLabels();

    public boolean withFicheStylePhrase();

    public MotcleFilter getChildrenFilter();

    public MotcleFilter getParentFilter();

    public MotcleFilter getNextFilter();

    public MotcleFilter getPreviousFilter();

    public List<FilterUnit> getFilterUnitList();

}
