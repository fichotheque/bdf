/* FichothequeLib_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction.def;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.selection.MotcleCondition;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusExtractDef extends SubsetExtractDef {

    public final static String MASTER_BOOLEAN = "master";
    public final static String WITHTHESAURUSTITLE_BOOLEAN = "withthesaurustitle";

    @Nullable
    public String getName();

    public TagNameInfo getTagNameInfo();

    public MotcleFilter getMotcleFilter();

    public List<MotcleCondition.Entry> getConditionEntryList();

    public boolean hasBooleanParameters();

    public boolean getBooleanParameter(String name);

    @Override
    public default short getCategory() {
        return SubsetKey.CATEGORY_THESAURUS;
    }

    public default boolean isMaster() {
        return getBooleanParameter(MASTER_BOOLEAN);
    }

    public default boolean isWithThesaurusTitle() {
        return getBooleanParameter(WITHTHESAURUSTITLE_BOOLEAN);
    }

}
