/* FichothequeLib_API - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.extraction;


/**
 *
 * @author Vincent Calame
 */
public interface ExtractionConstants {

    public final static int INITIAL_VERSION = 1;
    public final static int PLURAL_VERSION = 2;
    public final static String ASCENDING_SORT = "asc";
    public final static String DESCENDING_SORT = "desc";
    public final static String POIDS_TYPE = "poids";
    public final static String TITRE_TYPE = "titre";
    public final static String ANNEE_TYPE = "annee";
    public final static String LANG_TYPE = "lang";
    public final static String PAYS_TYPE = "pays";
    public final static String FIELDS_TYPE = "fields";
    public final static String POSITION_TYPE = "position";
    public final static String HIDE_PARAM = "hide";
    public final static String SUBUNIT_PARAM = "subunit";
    public final static String LABELTYPE_PARAM = "labeltype";
    public final static String GROUPS_PARAM = "groups";
    public final static String ROLES_PARAM = "roles";
    public final static String CELL_PARAMPREFIX = "cell-";
    public final static String CELL_ORDER_PARAM = "cell-order";
    public final static String CELL_FORMAT_PARAM = "cell-format";
    public final static String NEVER_HIDE = "never";
    public final static String EMPTY_HIDE = "empty";
    public final static String FORCE_SUBUNIT = "force";
}
