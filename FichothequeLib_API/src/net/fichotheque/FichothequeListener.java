/* FichothequeLib_API - Copyright (c) 2006-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;

import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeListener {

    public void subsetCreated(FichothequeEditor fichothequeEditor, Subset subset);

    public void subsetRemoved(FichothequeEditor fichothequeEditor, SubsetKey subsetKey, Subset masterSubset);

    public void corpusFieldCreated(FichothequeEditor fichothequeEditor, Corpus corpus, CorpusField corpusField);

    public void corpusFieldRemoved(FichothequeEditor fichothequeEditor, Corpus corpus, FieldKey fieldKey);

    public void subsetItemCreated(FichothequeEditor fichothequeEditor, SubsetItem subsetItem);

    public void ficheSaved(FichothequeEditor fichothequeEditor, FicheMeta ficheMeta, FicheAPI fiche);

    public void subsetItemRemoved(FichothequeEditor fichothequeEditor, Subset subset, int id);

}
