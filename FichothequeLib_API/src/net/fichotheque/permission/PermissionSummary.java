/* FichothequeLib_API - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.permission;

import java.util.function.Predicate;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.FicheMeta;


/**
 * Récapitulatif des permissions d'un rédacteur suivant ses différents rôles.
 * Cela prend en compte notamment le cas particulier du rôle par défaut.
 *
 * @author Vincent Calame
 */
public interface PermissionSummary {

    /**
     * Indique aucune permission.
     */
    public final static int SUMMARYLEVEL_0_NONE = 0;
    /**
     * Valeur uniquement pour les corpus : indique que la permission se limite
     * aux fiches du rédacteur concerné.
     */
    public final static int SUMMARYLEVEL_1_FICHE_OWN = 1;
    /**
     * Valeur uniquement pour les corpus : indique que la permission se limite
     * aux fiches appartenant à des rédacteurs de la même sphère.
     */
    public final static int SUMMARYLEVEL_2_FICHE_SPHERE = 2;
    /**
     * Valeur uniquement pour les albums et les addendas : indique que la
     * permission découle des permissions des fiches qui sont liées à
     * l'illustration ou au document.
     */
    public final static int SUMMARYLEVEL_3_CROISEMENT_TEST = 3;
    /**
     * Permission pour tous les éléments de la collection.
     */
    public final static int SUMMARYLEVEL_4_ALL = 4;
    /**
     * Permission pour tous les éléments de la collection plus l'administration
     * des métadonnées de la collection elle-même.
     */
    public final static int SUMMARYLEVEL_5_ADMIN = 5;


    /**
     * Retourne le récapitulatif de la permission de lecture. Prend les valeurs
     * de 0 à 5 (voir les constantes SUMMARYLEVEL_0_NONE,
     * SUMMARYLEVEL_1_FICHE_OWN, SUMMARYLEVEL_2_FICHE_SPHERE,
     * SUMMARYLEVEL_3_CROISEMENT_TEST, SUMMARYLEVEL_4_ALL et
     * SUMMARYLEVEL_5_ADMIN)
     *
     * @param subsetKey clé de la collection
     * @return niveau de permission en lecture
     */
    public int getReadLevel(SubsetKey subsetKey);

    /**
     * Retourne le récapitulatif de la permission d'écritre. Prend les valeurs
     * de 0 à 5 (voir les constantes SUMMARYLEVEL_0_NONE,
     * SUMMARYLEVEL_1_FICHE_OWN, SUMMARYLEVEL_2_FICHE_SPHERE,
     * SUMMARYLEVEL_3_CROISEMENT_TEST, SUMMARYLEVEL_4_ALL et
     * SUMMARYLEVEL_5_ADMIN)
     *
     * @param subsetKey clé de la collection
     * @return niveau de permission en écriture
     */
    public int getWriteLevel(SubsetKey subsetKey);


    /**
     * Indique s'il est possible de créer un nouvel élément pour la collection
     * en question.
     *
     * @param subsetKey clé de la collection
     * @return autorisation de création
     */
    public boolean canCreate(SubsetKey subsetKey);

    /**
     * Indique la permission d'administration de la collection (gestion des
     * métadonnées notamment).
     *
     * @param subsetKey clé de la collection
     * @return autorisation de création
     */
    public boolean isSubsetAdmin(SubsetKey subsetKey);


    /**
     * Indique qu'il s'agit d'un administrateur de la fichothèque (qui possède
     * toutes les permissions).
     *
     * @return true s'il s'agit d'un administrateur global, false sinon
     */
    public boolean isFichothequeAdmin();

    public boolean hasRole(String roleName);

    public boolean canDo(String actionName);

    /*public boolean can(FicheMeta ficheMeta, int level);*/

    public Predicate<Subset> getSubsetAccessPredicate();

    public default boolean hasAccess(SubsetKey subsetKey) {
        return (getReadLevel(subsetKey) != SUMMARYLEVEL_0_NONE);
    }

    public default boolean hasAccess(Subset subset) {
        return (getReadLevel(subset.getSubsetKey()) != SUMMARYLEVEL_0_NONE);
    }

    public default boolean canCreate(Subset subset) {
        return canCreate(subset.getSubsetKey());
    }

    public boolean canRead(FicheMeta ficheMeta);

    public boolean canWrite(FicheMeta ficheMeta);

    public default boolean isPartialAdmin() {
        if (canDo("balayagerun")) {
            return true;
        }
        return false;
    }

}
