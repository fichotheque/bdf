/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.sphere;

import net.fichotheque.FichothequeConstants;
import net.fichotheque.SubsetItem;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
public interface Redacteur extends SubsetItem {

    public String getLogin();

    public PersonCore getPersonCore();

    /**
     * Peut être nul.
     */
    public EmailCore getEmailCore();

    public String getStatus();

    public default Sphere getSphere() {
        return (Sphere) getSubset();
    }

    public default String getBracketStyle() {
        return getLogin() + "[" + getSubsetName() + "]";
    }

    public default String getCompleteName() {
        return getPersonCore().toStandardStyle();
    }

    public default boolean isInactive() {
        return getStatus().equals(FichothequeConstants.INACTIVE_STATUS);
    }

}
