/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.sphere;

import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.sphere.metadata.SphereMetadata;


/**
 *
 * @author Vincent Calame
 */
public interface Sphere extends Subset {

    public Redacteur getRedacteurById(int id);

    public Redacteur getRedacteurByLogin(String login);

    public List<Redacteur> getRedacteurList();

    public default SphereMetadata getSphereMetadata() {
        return (SphereMetadata) getMetadata();
    }

}
