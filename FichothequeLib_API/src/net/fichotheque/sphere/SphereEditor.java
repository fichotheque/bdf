/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.sphere;

import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.sphere.metadata.SphereMetadataEditor;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
public interface SphereEditor {

    public Sphere getSphere();

    public FichothequeEditor getFichothequeEditor();

    public SphereMetadataEditor getSphereMetadataEditor();

    /**
     * si id &lt; 1, un nouvel identifiant est attribué
     */
    public Redacteur createRedacteur(int id, String login) throws ExistingIdException, ParseException;

    public boolean setLogin(Redacteur redacteur, String newLogin) throws ExistingIdException, ParseException;

    public boolean setPerson(Redacteur redacteur, PersonCore person);

    public boolean setEmail(Redacteur redacteur, EmailCore email);

    public boolean removeRedacteur(Redacteur redacteur);

    public boolean setStatus(Redacteur redacteur, String status);

}
