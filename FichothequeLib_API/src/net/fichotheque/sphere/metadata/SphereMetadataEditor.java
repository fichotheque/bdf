/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.sphere.metadata;

import net.fichotheque.MetadataEditor;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;


/**
 *
 * @author Vincent Calame
 */
public interface SphereMetadataEditor extends MetadataEditor {

    public SphereEditor getSphereEditor();

    @Override
    public default boolean removeAttribute(AttributeKey attributeKey) {
        return getSphereEditor().getFichothequeEditor().removeAttribute(getMetadata(), attributeKey);
    }

    @Override
    public default boolean putAttribute(Attribute attribute) {
        return getSphereEditor().getFichothequeEditor().putAttribute(getMetadata(), attribute);
    }

}
