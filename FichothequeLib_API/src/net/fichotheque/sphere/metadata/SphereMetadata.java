/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.sphere.metadata;

import net.fichotheque.Metadata;
import net.fichotheque.sphere.Sphere;


/**
 *
 * @author Vincent Calame
 */
public interface SphereMetadata extends Metadata {

    public Sphere getSphere();

}
