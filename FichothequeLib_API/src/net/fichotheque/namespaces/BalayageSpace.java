/* FichothequeLib_API - Copyright (c) 2024-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class BalayageSpace {

    public final static CheckedNameSpace BALAYAGE_NAMESPACE = CheckedNameSpace.build("balayage");
    public final static AttributeKey MODES_KEY = AttributeKey.build(BALAYAGE_NAMESPACE, "modes");
    public final static AttributeKey SITEMAP_KEY = AttributeKey.build(BALAYAGE_NAMESPACE, "sitemap");

    private BalayageSpace() {

    }

}
