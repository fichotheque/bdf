/* FichothequeLib_API - Copyright (c) 2024-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class SyncSpace {

    public final static CheckedNameSpace SYNC_NAMESPACE = CheckedNameSpace.build("sync");
    public final static AttributeKey URL_KEY = AttributeKey.build(SyncSpace.SYNC_NAMESPACE, "url");
    public final static AttributeKey DATE_KEY = AttributeKey.build(SyncSpace.SYNC_NAMESPACE, "date");

    private SyncSpace() {

    }

}
