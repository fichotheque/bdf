/* FichothequeLib_API - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import java.text.ParseException;
import net.fichotheque.FichothequeConstants;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedLocalKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class PhraseSpace {

    public final static CheckedNameSpace PHRASE_NAMESPACE = CheckedNameSpace.build("phrase");
    public final static AttributeKey FICHE_NUMBER_ATTRIBUTEKEY = AttributeKey.build(PHRASE_NAMESPACE, FichothequeConstants.FICHE_PHRASE + ":number");
    public final static AttributeKey FICHESTYLE_NUMBER_ATTRIBUTEKEY = AttributeKey.build(PHRASE_NAMESPACE, FichothequeConstants.FICHESTYLE_PHRASE + ":number");

    private PhraseSpace() {

    }

    public static AttributeKey getNumberAttributeKey(String phraseName) {
        switch (phraseName) {
            case FichothequeConstants.FICHE_PHRASE:
                return FICHE_NUMBER_ATTRIBUTEKEY;
            case FichothequeConstants.FICHESTYLE_PHRASE:
                return FICHESTYLE_NUMBER_ATTRIBUTEKEY;
            default:
                try {
                CheckedLocalKey localKey = CheckedLocalKey.parse(phraseName + ":number");
                return AttributeKey.build(PHRASE_NAMESPACE, localKey);
            } catch (ParseException pe) {
                return null;
            }
        }
    }


}
