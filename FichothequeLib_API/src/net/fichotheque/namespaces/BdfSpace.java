/* FichothequeLib_API - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import net.fichotheque.SubsetKey;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class BdfSpace {

    public final static CheckedNameSpace BDF_NAMESPACE = CheckedNameSpace.build("bdf");
    public final static AttributeKey IMAGEBASEURL_KEY = AttributeKey.build(BDF_NAMESPACE, "imagebaseurl");
    public final static AttributeKey AUDIOBASEURL_KEY = AttributeKey.build(BDF_NAMESPACE, "audiobaseurl");
    public final static AttributeKey DISCARDTEXT_KEY = AttributeKey.build(BDF_NAMESPACE, "discardtext");
    public final static AttributeKey SENDPOLICY_KEY = AttributeKey.build(BDF_NAMESPACE, "sendpolicy");
    public final static AttributeKey IDALPHASTYLE_KEY = AttributeKey.build(BDF_NAMESPACE, "idalphastyle");
    public final static AttributeKey PHRASES_KEY = AttributeKey.build(BDF_NAMESPACE, "phrases");
    public final static AttributeKey URL_KEY = AttributeKey.build(BDF_NAMESPACE, "url");


    private BdfSpace() {
    }

    public static AttributeKey getPhrasesAttributeKey(SubsetKey subsetKey) {
        return AttributeKey.build(BDF_NAMESPACE, "phrases:" + subsetKey.getCategoryString());
    }

}
