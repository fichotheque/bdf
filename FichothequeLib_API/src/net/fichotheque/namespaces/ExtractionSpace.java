/* FichothequeLib_API - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import net.fichotheque.extraction.ExtractionConstants;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeKeyAlias;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class ExtractionSpace {

    public final static AttributeKeyAlias EXTRACTION_ALIAS = new ExtractionAttributeKeyAlias();
    public final static CheckedNameSpace EXTRACTION_NAMESPACE = CheckedNameSpace.build("extraction");
    public final static AttributeKey LABELTYPE_ATTRIBUTEKEY = AttributeKey.build(EXTRACTION_NAMESPACE, ExtractionConstants.LABELTYPE_PARAM);
    public final static AttributeKey SUBUNIT_ATTRIBUTEKEY = AttributeKey.build(EXTRACTION_NAMESPACE, ExtractionConstants.SUBUNIT_PARAM);
    public final static AttributeKey SATELLITE_ATTRIBUTEKEY = AttributeKey.build(EXTRACTION_NAMESPACE, "satellite");
    public final static AttributeKey LINKTEXTSOURCE_ATTRIBUTEKEY = AttributeKey.build(EXTRACTION_NAMESPACE, "linktextsource");
    public final static AttributeKey HIDE_ATTRIBUTEKEY = AttributeKey.build(EXTRACTION_NAMESPACE, ExtractionConstants.HIDE_PARAM);
    public final static AttributeKey GROUPS_ATTRIBUTEKEY = AttributeKey.build(EXTRACTION_NAMESPACE, ExtractionConstants.GROUPS_PARAM);
    public final static AttributeKey ROLES_ATTRIBUTEKEY = AttributeKey.build(EXTRACTION_NAMESPACE, ExtractionConstants.ROLES_PARAM);
    public final static String SATELLITE_ATTRIBUTEKEY_PREFIX = "extraction:satellite:";

    private ExtractionSpace() {

    }


    private static class ExtractionAttributeKeyAlias implements AttributeKeyAlias {

        private ExtractionAttributeKeyAlias() {

        }

        @Override
        public String checkNameSpace(String nameSpace) {
            if (nameSpace.equals("bdfxml")) {
                return "extraction";
            }
            return nameSpace;
        }

        @Override
        public String checkLocalKey(String localKey) {
            return localKey;
        }

    }


}
