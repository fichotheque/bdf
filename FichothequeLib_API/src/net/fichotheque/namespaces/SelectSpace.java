/* FichothequeLib_API - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class SelectSpace {

    public final static CheckedNameSpace SELECT_NAMESPACE = CheckedNameSpace.build("select");
    public final static AttributeKey IDALPHA_KEY = AttributeKey.build(SELECT_NAMESPACE, "idalpha");

    private SelectSpace() {

    }

}
