/* FichothequeLib_API - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class IconSpace {

    public final static CheckedNameSpace ICON_NAMESPACE = CheckedNameSpace.build("icon");
    public final static AttributeKey CARTOUCHE_KEY = AttributeKey.build(ICON_NAMESPACE, "cartouche");
    public final static AttributeKey TEXT_KEY = AttributeKey.build(ICON_NAMESPACE, "text");
    public final static AttributeKey COLOR_KEY = AttributeKey.build(ICON_NAMESPACE, "color");
    public final static AttributeKey FONTAWESOME_KEY = AttributeKey.build(ICON_NAMESPACE, "fa");
    public final static AttributeKey CHAR_KEY = AttributeKey.build(ICON_NAMESPACE, "char");
    public final static AttributeKey SOURCES_KEY = AttributeKey.build(ICON_NAMESPACE, "sources");

    private IconSpace() {

    }

}
