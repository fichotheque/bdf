/* FichothequeLib_API - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.namespaces;

import net.fichotheque.exportation.transformation.TransformationKey;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;
import net.mapeadores.util.text.ValidExtension;


/**
 *
 * @author Vincent Calame
 */
public final class TransformationSpace {

    public final static CheckedNameSpace TRANSFORMATION_NAMESPACE = CheckedNameSpace.build("transformation");
    public final static AttributeKey CORPUS_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "corpus");
    public final static AttributeKey TABLEEXPORT_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "tableexport");
    public final static AttributeKey EXTRACTVERSION_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "extractversion");
    public final static AttributeKey EMPTYCOMPONENTS_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "emptycomponents");
    public final static AttributeKey FIELDBULLET_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "fieldbullet");
    public final static AttributeKey SECTIONEND_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "sectionend");
    public final static AttributeKey FICHENUMBER_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "fichenumber");
    public final static AttributeKey RESOURCELOGO_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "resourcelogo");
    public final static AttributeKey VARIANT_KEY = AttributeKey.build(TRANSFORMATION_NAMESPACE, "variant");

    private TransformationSpace() {

    }

    public static AttributeKey toDefaultTemplateAttributeKey(TransformationKey transformationKey, ValidExtension validExtension) {
        String name;
        if (validExtension != null) {
            name = transformationKey.getKeyString() + "." + validExtension.toString();
        } else {
            name = transformationKey.getKeyString();
        }
        return AttributeKey.build(TRANSFORMATION_NAMESPACE, "default:" + name);
    }

}
