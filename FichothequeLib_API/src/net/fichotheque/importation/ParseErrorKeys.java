/* BdfServer_API - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.importation;


/**
 *
 * @author Vincent Calame
 */
public interface ParseErrorKeys {

    public final static String MALFORMED_FIELD_INIT_WARNING = "MALFORMED_FIELD_INIT_WARNING";
    public final static String DOUBLON_FIELD_INIT_WARNING = "DOUBLON_FIELD_INIT_WARNING";
    public final static String UNKNOWN_FIELD_INIT_WARNING = "UNKNOWN_FIELD_INIT_WARNING";
    public final static String BABELIEN_ONLY_FIELD_INIT_WARNING = "BABELIEN_ONLY_FIELD_INIT_WARNING";
    public final static String IDALPHA_ONLY_FIELD_INIT_WARNING = "IDALPHA_ONLY_FIELD_INIT_WARNING";
    public final static String NOT_BABELIEN_FIELD_INIT_WARNING = "NOT_BABELIEN_FIELD_INIT_WARNING";
    public final static String UNMODIFIABLE_FIELD_INIT_WARNING = "UNMODIFIABLE_FIELD_INIT_WARNING";
    public final static String MISSING_FIELDS_INIT = "MISSING FIELDS INIT";
    public final static String MISSING_ID_COLUMN = "MISSING ID COLUMN";
    public final static String UNKNOWN_IDALPHA = "UNKNOWN IDALPHA";
    public final static String UNKNOWN_ID = "UNKNOWN ID";
    public final static String NOT_INTEGER = "NOT INTEGER";
    public final static String MALFORMED_IDALPHA = "MALFORMED IDALPHA";
    public final static String EMPTY_IDALPHA = "EMPTY IDALPHA";
    public final static String EXISTING_IDALPHA = "EXISTING IDALPHA";
    public final static String EXISTING_ID = "EXISTING ID";
    public final static String NOT_LANG = "NOT LANG";
    public final static String SELF_PARENT = "SELF PARENT";
    public final static String UNKNOWN_SUBSET = "UNKNOWN SUBSET";
    public final static String NOT_AVAILABLE_INCLUDEKEY = "NOT AVAILABLE INCLUDEKEY";
    public final static String NOT_DATE = "NOT DATE";
    public final static String UNKNOWN_STATUS = "UNKNOWN STATUS";
}
