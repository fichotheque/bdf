/* FichothequeLib_API - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.importation;

import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheChange;
import net.fichotheque.corpus.FicheMeta;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusImport {

    public final static String CREATION_TYPE = "creation";
    public final static String CHANGE_TYPE = "change";
    public final static String REMOVE_TYPE = "remove";

    public Corpus getCorpus();

    public String getType();

    public List<FicheImport> getFicheImportList();

    public static String checkType(String type) {
        switch (type) {
            case CREATION_TYPE:
                return CREATION_TYPE;
            case CHANGE_TYPE:
                return CHANGE_TYPE;
            case REMOVE_TYPE:
                return REMOVE_TYPE;
            default:
                throw new IllegalArgumentException("Unknown type: " + type);
        }
    }


    public interface FicheImport {

        /**
         * Nul si getType() = CREATION_TYPE
         */
        public FicheMeta getFicheMeta();

    }


    public interface ChangeFicheImport extends FicheImport {

        public FicheChange getFicheChange();

        public AttributeChange getAttributeChange();

        public LiensImport getLiensImport();

        public FuzzyDate getCreationDate();

    }


    public interface CreationFicheImport extends FicheImport {

        public int getNewId();

        public FicheChange getFicheChange();

        public AttributeChange getAttributeChange();

        public LiensImport getLiensImport();

        public FuzzyDate getCreationDate();

    }

}
