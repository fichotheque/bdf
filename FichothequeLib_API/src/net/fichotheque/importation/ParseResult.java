/* BdfServer_API - Copyright (c) 2013-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.importation;

import java.io.File;


/**
 *
 * @author Vincent Calame
 */
public interface ParseResult {

    public String getType();

    public int getInitErrorCount();

    public InitError getInitError(int i);

    public int getInitWarningCount();

    public InitWarning getInitWarning(int i);

    public int getParseErrorCount();

    public ParseError getParseError(int i);

    public int getBdfErrorCount();

    public BdfError getBdfError(int i);

    public File getTmpFile();

    public File getTmpDir();


    public static interface InitError {

        public String getKey();

        public String getText();

    }


    public static interface InitWarning {

        public String getKey();

        public String getText();

    }


    public static interface ParseError {

        public String getRawText();

        public int getLineNumber();

    }


    public static interface BdfError {

        public String getKey();

        public String getText();

        public int getLineNumber();

    }


    public static interface ResultItem {

        public Object getKeyObject();

        public Object getValueObject();

        public int getLineNumber();

    }

}
