/* BdfServer_API - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.importation;

import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.include.IncludeKey;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public interface LabelImport {

    public Lang getLang();

    public List<PhraseImport> getFichothequePhraseImportList();

    public List<MetadataImport> getMetadataImportList();

    public List<CorpusImport> getCorpusImportList();


    public static interface MetadataImport {

        public Subset getSubset();

        public List<PhraseImport> getPhraseImportList();

    }


    public static interface CorpusImport {

        public Corpus getCorpus();

        public List<FieldKeyImport> getFieldKeyImportList();

        public List<IncludeKeyImport> getIncludeKeyImportList();

    }


    public static interface PhraseImport {

        /**
         * Peut être nul.
         *
         *
         */
        @Nullable
        public String getName();

        /**
         * Peut être nul.
         *
         */
        @Nullable
        public CleanedString getLabelString();

    }


    public static interface FieldKeyImport {

        public FieldKey getFieldKey();

        /**
         * Peut être nul.
         */
        @Nullable
        public CleanedString getLabelString();

    }


    public static interface IncludeKeyImport {

        public IncludeKey getIncludeKey();

        /**
         * Peut être nul.
         */
        @Nullable
        public CleanedString getLabelString();

    }

}
