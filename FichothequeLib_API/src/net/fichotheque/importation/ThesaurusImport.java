/* FichothequeLib_API - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.importation;

import java.util.List;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusImport {

    public final static String CREATION_TYPE = "creation";
    public final static String CHANGE_TYPE = "change";
    public final static String REMOVE_TYPE = "remove";
    public final static String MERGE_TYPE = "merge";
    public final static String MOVE_TYPE = "move";

    public Thesaurus getThesaurus();

    /**
     * Non nul si getType() = MERGE_TYPE ou MOVE_TYPE
     */
    public Thesaurus getDestinationThesaurus();

    public String getType();

    public List<MotcleImport> getMotcleImportList();

    public static String checkType(String type) {
        switch (type) {
            case CREATION_TYPE:
                return CREATION_TYPE;
            case CHANGE_TYPE:
                return CHANGE_TYPE;
            case REMOVE_TYPE:
                return REMOVE_TYPE;
            case MERGE_TYPE:
                return MERGE_TYPE;
            case MOVE_TYPE:
                return MOVE_TYPE;
            default:
                throw new IllegalArgumentException("Unknown type: " + type);
        }
    }


    public interface MotcleImport {

        /**
         * Nul si getType() = CREATION_TYPE
         */
        public Motcle getMotcle();

    }


    public interface ChangeMotcleImport extends MotcleImport {

        public String getNewIdalpha();

        /**
         * Instance de Motcle ou de Thesaurus (si le mot-clé doit être placé à
         * la racine), nul si le parent ne doit pas être modifié.
         */
        public Object getParent();

        public String getNewStatus();

        public LabelChange getLabelChange();

        public AttributeChange getAttributeChange();

        public LiensImport getLiensImport();

        public default boolean isEmpty() {
            if (getNewIdalpha() != null) {
                return false;
            }
            if (getParent() != null) {
                return false;
            }
            if (getNewStatus() != null) {
                return false;
            }
            if (!getLabelChange().isEmpty()) {
                return false;
            }
            if (!getAttributeChange().isEmpty()) {
                return false;
            }
            if (!getLiensImport().isEmpty()) {
                return false;
            }
            return true;
        }

    }


    public interface CreationMotcleImport extends MotcleImport {

        public int getNewId();

        public String getNewIdalpha();

        public int getParentId();

        public String getParentIdalpha();

        public String getNewStatus();

        public LabelChange getLabelChange();

        public AttributeChange getAttributeChange();

        public LiensImport getLiensImport();

    }


    public interface MergeMotcleImport extends MotcleImport {

        public Motcle getDestinationMotcle();

    }

}
