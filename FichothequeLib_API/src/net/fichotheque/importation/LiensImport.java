/* BdfServer_API - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.importation;

import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.include.IncludeKey;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public interface LiensImport {

    public boolean isLiageRemoved();

    public List<IncludeKey> getRemovedIncludeKeyList();

    public List<LienImport> getReplaceLienImportList();

    public List<LienImport> getAppendLienImportList();

    public default boolean isEmpty() {
        return ((!isLiageRemoved()) && (getRemovedIncludeKeyList().isEmpty()) && (getReplaceLienImportList().isEmpty()) && (getAppendLienImportList().isEmpty()));
    }


    public interface LienImport {

        public IncludeKey getOriginIncludeKey();

        public Subset getOtherSubset();

        public int getPoids();

        public SubsetItem getOtherSubsetItem();

        public Label getLabel();

        public default String getMode() {
            IncludeKey includeKey = getOriginIncludeKey();
            if (includeKey == null) {
                return "";
            }
            return includeKey.getMode();
        }

        public default boolean isLiageOrigin() {
            return (getOriginIncludeKey() == null);
        }

    }

}
