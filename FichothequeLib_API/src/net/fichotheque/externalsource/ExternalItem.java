/* FichothequeLib_API - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.externalsource;


/**
 *
 * @author Vincent Calame
 */
public interface ExternalItem {

    public String getTitle();

    public String getDescription();


    public static interface Id extends ExternalItem {

        public int getId();

    }


    public static interface Idalpha extends ExternalItem {

        public String getIdalpha();

    }

}
