/* FichothequeLib_API - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.externalsource;

import java.util.Collection;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface ExternalSource {

    public final static int ID_TYPE = 1;
    public final static int IDALPHA_TYPE = 2;

    public int getIdType();

    public Motcle getMotcle(FichothequeEditor fichothequeEditor, Thesaurus localThesaurus, int id);

    public Motcle getMotcle(FichothequeEditor fichothequeEditor, Thesaurus localThesaurus, String label, Lang lang);

    public Collection<ExternalItem> search(String query, Lang lang);

    public default boolean isWithId() {
        return (getIdType() == ID_TYPE);
    }

}
