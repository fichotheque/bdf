/* FichothequeLib_API - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.format.FormatContext;
import net.fichotheque.format.SourceLabelProvider;


/**
 *
 * @author Vincent Calame
 */
public interface TableExportContext {

    public FormatContext getFormatContext();

    public TableInclusionResolverProvider getTableInclusionResolverProvider();

    public FormatColDefChecker getFormatColDefChecker(Subset subset);

    public SourceLabelProvider getSourceLabelProvider();

    public default Fichotheque getFichotheque() {
        return getFormatContext().getFichotheque();
    }

}
