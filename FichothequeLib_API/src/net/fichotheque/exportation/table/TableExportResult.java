/* FichothequeLib_API - Copyright (c) 2015-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import net.mapeadores.util.money.Amount;
import net.mapeadores.util.primitives.Decimal;


/**
 * Interface décrivant le résultat de l'export tabulaire d'un corpus ou de tout
 * autre collection, en particulier les sommes de colonne. Une colonne peut être
 * totalisée s'il s'agit d'entiers, de décimaux ou de valeurs monétaires.
 *
 * @author Vincent Calame
 */
public interface TableExportResult {

    /**
     * Indique si une des colonnes a été totalisée.
     *
     * @return true si une des colonnnes a été totalisée, false sinon.
     */
    public boolean hasColumnSum();

    /**
     * Retourne le nombre de colonnes de l'export tabulaire.
     *
     * @return le nombre de colonnes de la table.
     */
    public int getColCount();

    /**
     * Retourne la définition de la i-ième colonne de l'export tabulaire.
     *
     * @param i index de la colonne.
     * @return la définition de la i-ème colonne
     */
    public ColDef getColDef(int i);

    /**
     * Retourne une instance de ColumnSum avec le résultat de la totalisation.
     * Retourne <em>null</em> si aucune somme n'a été faite sur cette colonne.
     *
     * @param i index de la colonne
     * @return le résultat de la i-ème colonne
     */
    public ColumnSum getColumnSum(int i);


    /**
     * Interface générique des sommes de colonne.
     */
    public static interface ColumnSum {

    }


    /**
     * Résultat de la somme d'une colonne d'entiers.
     */
    public static interface IntegerColumnSum extends ColumnSum {

        /**
         * Retourne le résultat de la somme d'une colonne d'entiers.
         *
         * @return total de la colonne
         */
        public long getResult();

    }


    /**
     * Résultat de la somme d'une colonne de décimaux.
     */
    public static interface DecimalColumnSum extends ColumnSum {

        /**
         * Retourne le résultat de la somme d'une colonne de décimaux.
         *
         * @return total de la colonne
         */
        public Decimal getResult();

    }


    /**
     * Résultat de la somme d'une colonne de pourcentages.
     */
    public static interface PercentageColumnSum extends ColumnSum {

        /**
         * Retourne le résultat de la somme d'une colonne de décimaux.
         *
         * @return total de la colonne
         */
        public Decimal getResult();

    }


    /**
     * Résultat de la somme d'une colonne de valeurs monétaires. Comme plusieurs
     * devises peuvent être présentes dans une colonne, chaque devise est
     * totalisée séparément.
     */
    public static interface MoneyColumnSum extends ColumnSum {

        /**
         * Retourne le nombre de sommes différentes (une par devise). Ce nombre
         * est toujours supérieur à 0.
         *
         * @return nombre de sommes différentes
         */
        public int getResultCount();

        /**
         * Retourne une instance de Amount décrivant la i-ème somme. Cette somme
         * correspond à une devise particulière.
         *
         * @param i index du résultat
         *
         * @return le i-ème résultat
         */
        public Amount getResult(int i);

    }


}
