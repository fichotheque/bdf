/* FichothequeLib_API - Copyright (c) 2016-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface TableInclusionResolverProvider {

    /**
     * Retourne le résolveur pour l'espace de nom de l'inclusion en argument.
     * Peut retourner null si l'espace de nom n'est pas traité par le
     * fournisseur. L'espace de nom corresond à la valeur de
     * TableInclusionDef.getNameSpace()
     *
     * @param inclusionNameSpace espace de nom de l'inclusion
     * @return résolveur ou null
     */
    @Nullable
    public TableInclusionResolver getTableInclusionResolver(String inclusionNameSpace);

}
