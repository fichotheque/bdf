/* FichothequeLib_API - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.logging.MessageByLine;


/**
 *
 * @author Vincent Calame
 */
public interface TableExportContentDescription {

    public final static String OK_STATE = "ok";
    public final static String UNKNOWN_NAME_STATE = "unknown_name";
    public final static String UNKNOWN_SUBSET_STATE = "unknown_subset";
    public final static String EMPTY_STATE = "empty";
    public final static String EMPTY_WITH_ERRORS_STATE = "empty_errors";
    public final static String OK_WITH_ERRORS_STATE = "ok_errors";

    public String getPath();

    public String getTableExportName();

    public String getState();

    /**
     * Peut être nul
     */
    public SubsetKey getSubsetKey();

    public TableDef getTableDef();

    /**
     * Ordonné suivant le numéro de ligne
     */
    public List<MessageByLine> getMessageByLineList();

    public boolean isEditable();

    public default boolean isValid() {
        switch (getState()) {
            case OK_STATE:
            case OK_WITH_ERRORS_STATE:
                return true;
            default:
                return false;
        }
    }

}
