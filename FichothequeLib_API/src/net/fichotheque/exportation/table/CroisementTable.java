/* FichothequeLib_API - Copyright (c) 2012-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import net.fichotheque.Subset;


/**
 *
 * @author Vincent Calame
 */
public interface CroisementTable {

    public String getTableName();

    public Subset getSubset1();

    public Subset getSubset2();

}
