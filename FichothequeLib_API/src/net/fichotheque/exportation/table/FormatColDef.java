/* FichothequeLib_API - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import net.fichotheque.format.FichothequeFormatDef;


/**
 *
 * @author Vincent Calame
 */
public interface FormatColDef extends ColDef, TableDefItem {

    public FichothequeFormatDef getFichothequeFormatDef();

    @Override
    public default Object getParameterValue(String paramKey) {
        return getFichothequeFormatDef().getParameterValue(paramKey);
    }

}
