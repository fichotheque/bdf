/* FichothequeLib_API - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;


/**
 *
 * @author Vincent Calame
 */
public interface TableExportConstants {

    public final static String NONE_HEADER = "none";
    public final static String COLUMNTITLE_HEADER = "columntitle";
    public final static String COLUMNKEY_HEADER = "columnkey";
    public final static String TABLENAME_PARAMETER = "table_name";
    public final static String EXTRACTION_PARAMETER = "extraction";
    public final static String LANGS_PARAMETER = "langs";

    public static String checkHeaderType(String headerType) {
        switch (headerType) {
            case "noheader":
            case NONE_HEADER:
                return NONE_HEADER;
            case "libheader":
            case COLUMNTITLE_HEADER:
                return COLUMNTITLE_HEADER;
            case "keyheader":
            case COLUMNKEY_HEADER:
                return COLUMNKEY_HEADER;
            default:
                throw new IllegalArgumentException("Unknown headerType: " + headerType);
        }
    }

}
