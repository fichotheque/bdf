/* FichothequeLib_API - Copyright (c) 2007-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.format.FormatUtils;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public interface ColDef {

    /**
     * N'est jamais nul.
     */
    public String getColName();

    /**
     * Nul s'il faut utiliser le libellé par défaut
     */
    @Nullable
    public Labels getCustomLabels();

    @Nullable
    public Object getParameterValue(String paramKey);

    public default short getCastType() {
        if (FormatUtils.toBoolean(getParameterValue(FormatConstants.JSONARRAY_PARAMKEY))) {
            return FormatConstants.JSON_CAST;
        }
        Object value = getParameterValue(FormatConstants.CAST_PARAMKEY);
        if (value == null) {
            return FormatConstants.NO_CAST;
        }
        try {
            return ((Short) value);
        } catch (ClassCastException cce) {
            return FormatConstants.NO_CAST;
        }
    }

}
