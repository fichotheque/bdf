/* FichothequeLib_API - Copyright (c) 2008-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface TableExport {

    /**
     * Peut être nul s'il s'agit de l'exportation par défaut.
     *
     * @return
     */
    @Nullable
    public TableExportDef getTableExportDef();

    public List<SubsetTable> getSubsetTableList();

    public List<CroisementTable> getCroisementTableList();

    @Nullable
    public default SubsetTable getSubsetTable(SubsetKey subsetKey) {
        for (SubsetTable subsetTable : getSubsetTableList()) {
            if (subsetTable.getSubset().getSubsetKey().equals(subsetKey)) {
                return subsetTable;
            }
        }
        return null;
    }

}
