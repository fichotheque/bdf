/* FichothequeLib_API - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface CellEngine extends CellConverter {

    @Nullable
    public Cell[] toCellArray(SubsetItem subsetItem, @Nullable Predicate<SubsetItem> predicate);

    public TableExportResult getTableExportResult(SubsetKey subsetKey);

    public List<ColDef> getColDefList(SubsetKey subsetKey);

    @Override
    public default Cell[] toCellArray(SubsetItem subsetItem) {
        return toCellArray(subsetItem, null);
    }

}
