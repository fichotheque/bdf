/* FichothequeLib_API - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import java.util.List;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface TableExportDescription {

    public final static String DISABLED_STATE = "disabled";
    public final static String CONTAINS_ERRORS_STATE = "contains_errors";
    public final static String OK_STATE = "ok";
    public final static String WORKING_LANGMODE = "working";
    public final static String SUPPLEMENTARY_LANGMODE = "supplementary";
    public final static String CUSTOMLIST_LANGMODE = "custom";

    public default String getName() {
        return getTableExportDef().getName();
    }

    public String getState();

    public TableExportDef getTableExportDef();

    public List<TableExportContentDescription> getTableExportContentDescriptionList();

    public boolean isEditable();

    public default boolean isValid() {
        return (!(getState().equals(DISABLED_STATE)));
    }

    public default String getTitle(Lang preferredLang) {
        return getTableExportDef().getTitleLabels().seekLabelString(preferredLang, getName());
    }

}
