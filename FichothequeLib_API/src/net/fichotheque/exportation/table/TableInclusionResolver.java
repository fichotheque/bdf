/* FichothequeLib_API - Copyright (c) 2012-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import net.fichotheque.Subset;
import net.mapeadores.util.logging.LineMessageHandler;


/**
 *
 * @author Vincent Calame
 */
public interface TableInclusionResolver {

    public final static short OK_TEST = 1;
    public final static short ERROR_TEST = 2;
    public final static short UNKNOWN_TEST = 3;

    public boolean test(TableInclusionDef tableInclusionDef, Subset subset, TableExportContext tableExportContext, int lineNumber, LineMessageHandler lineMessageHandler);

    /**
     * De longueur nul si et seulement si test est négatif.
     *
     * @param tableInclusionDef
     * @param subset
     * @param tableExportContext
     * @return
     */
    public Col[] resolve(TableInclusionDef tableInclusionDef, Subset subset, TableExportContext tableExportContext);

}
