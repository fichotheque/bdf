/* FichothequeLib_API - Copyright (c) 2010-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.table;

import java.util.List;
import net.fichotheque.Subset;


/**
 *
 * @author Vincent Calame
 */
public interface SubsetTable {

    public Subset getSubset();

    public List<ColDef> getColDefList();

    public List<Col> getColList();

    public String getParameterValue(String parameterName);

}
