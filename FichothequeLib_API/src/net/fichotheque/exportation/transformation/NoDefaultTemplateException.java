/* FichothequeLib_API - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.transformation;


/**
 *
 * @author Vincent Calame
 */
public class NoDefaultTemplateException extends Exception {

    public NoDefaultTemplateException(TransformationKey transformationKey, String extension) {
        super(transformationKey + "/" + extension);
    }

}
