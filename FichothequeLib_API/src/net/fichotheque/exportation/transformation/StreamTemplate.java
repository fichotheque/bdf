/* FichothequeLib_API - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.transformation;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.extraction.def.ExtractionDef;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public interface StreamTemplate {

    public String getMimeType();


    public interface Xslt extends StreamTemplate {

        public Attributes getAttributes();

        public ExtractionDef getCustomExtractionDef();

        public void transform(String extractionString, OutputStream outputStream, Map<String, Object> transformerParameters, Map<String, String> outputProperties) throws IOException;

    }


    public interface Properties extends StreamTemplate {


        public String getTableExportName();

        public void fill(Cell[] cellArray, OutputStream outputStream) throws IOException;

    }

}
