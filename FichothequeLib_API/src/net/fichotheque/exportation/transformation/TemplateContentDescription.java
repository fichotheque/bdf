/* FichothequeLib_API - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.transformation;

import java.util.List;
import net.mapeadores.util.logging.MessageByLine;


/**
 * Cette interface décrit un contenu nécessaire à la définition d'un gabarit de
 * transformation. Certains contenus sont obligatoires, d'autres optionnels. Par
 * exemple, la transformation XSLT simple se construit à partir d'un fichier
 * XSLT obligatoire (transformer.xsl) et d'un fichier d'extraction XML optionnel
 * (extraction.xlml)
 *
 * @author Vincent Calame
 */
public interface TemplateContentDescription {

    public final static String OK_STATE = "ok";
    public final static String XML_ERROR_STATE = "xml_error";
    public final static String TEXT_TYPE = "text";
    public final static String BINARY_TYPE = "binary";

    public TemplateKey getTemplateKey();

    /**
     * Indique le chemin du contenu.
     *
     * @return le chemin relatif du contenu
     */
    public String getPath();

    public String getType();

    public boolean isMandatory();

    public String getState();


    /**
     * Ordonné suivant le numéro de ligne
     */
    public List<MessageByLine> getMessageByLineList();


}
