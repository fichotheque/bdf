/* BdfServer_API - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.transformation;

import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public interface TemplateDescription {

    public final static String UNKNOWN_TEMPLATE_TYPE_STATE = "unknown_template_type";
    public final static String ERROR_STATE = "error";
    public final static String WITH_WARNINGS_STATE = "with_warnings";
    public final static String OK_STATE = "ok";

    public default TemplateKey getTemplateKey() {
        return getTemplateDef().getTemplateKey();
    }

    public TemplateDef getTemplateDef();

    public String getType();

    public String getState();

    public List<Message> getErrorMessageList();

    public List<Message> getWarningMessageList();

    public List<TemplateContentDescription> getTemplateContentDescriptionList();

    public default TemplateContentDescription getTemplateContentDescription(String path) {
        for (TemplateContentDescription templateContentDescription : getTemplateContentDescriptionList()) {
            if (templateContentDescription.getPath().equals(path)) {
                return templateContentDescription;
            }
        }
        return null;
    }

    public default String getTitle(Lang preferredLang) {
        return getTemplateDef().getTitleLabels().seekLabelString(preferredLang, getTemplateKey().getName());
    }

}
