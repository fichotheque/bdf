/* FichothequeLib_API - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.transformation;

import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.exceptions.ShouldNotOccurException;


/**
 * Clé indiquant le type de transformation à appliquer. Cette clé est utilisée
 * pour obtenir les modèles de transformation.
 *
 * @author Vincent Calame
 */
public final class TransformationKey {

    public final static String COMPILATION = "compilation";
    public final static String STATTHESAURUS = "statthesaurus";
    public final static String INVERSETHESAURUS = "inversethesaurus";
    public final static String SECTION = "section";
    public final static String FORMAT = "format";
    public final static String MEMENTO = "memento";
    public final static TransformationKey COMPILATION_INSTANCE = new TransformationKey(COMPILATION);
    public final static TransformationKey STATTHESAURUS_INSTANCE = new TransformationKey(STATTHESAURUS);
    public final static TransformationKey INVERSETHESAURUS_INSTANCE = new TransformationKey(INVERSETHESAURUS);
    public final static TransformationKey SECTION_INSTANCE = new TransformationKey(SECTION);
    public final static TransformationKey FORMAT_INSTANCE = new TransformationKey(FORMAT);
    public final static TransformationKey MEMENTO_INSTANCE = new TransformationKey(MEMENTO);
    private final String key;
    private final boolean isCorpusTransformationKey;

    private TransformationKey(String key) {
        this.key = key;
        this.isCorpusTransformationKey = false;
    }

    private TransformationKey(String key, boolean isCorpusTransformationKey) {
        this.key = key;
        this.isCorpusTransformationKey = isCorpusTransformationKey;
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public TransformationKey(SubsetKey corpusKey) {
        if (!corpusKey.isCorpusSubset()) {
            throw new IllegalArgumentException("corpusKey.isCorpusSubset() == false");
        }
        key = corpusKey.getKeyString();
        this.isCorpusTransformationKey = true;
    }

    public boolean isCorpusTransformationKey() {
        return isCorpusTransformationKey;
    }

    @Override
    public int hashCode() {
        return key.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        return (this.key.equals(((TransformationKey) other).key));
    }

    public String getKeyString() {
        return key;
    }

    @Override
    public String toString() {
        return key;
    }

    /**
     * @throws IllegalStateException
     */
    public SubsetKey toCorpusKey() {
        if (!isCorpusTransformationKey) {
            throw new IllegalStateException("transformationKey is not a corpus transformationKey");
        }
        try {
            return SubsetKey.parse(key);
        } catch (java.text.ParseException pe) {
            throw new ShouldNotOccurException(pe);
        }
    }

    public static TransformationKey parse(String s) throws ParseException {
        switch (s) {
            case COMPILATION:
                return COMPILATION_INSTANCE;
            case STATTHESAURUS:
                return STATTHESAURUS_INSTANCE;
            case INVERSETHESAURUS:
                return INVERSETHESAURUS_INSTANCE;
            case SECTION:
                return SECTION_INSTANCE;
            case FORMAT:
                return FORMAT_INSTANCE;
            case MEMENTO:
                return MEMENTO_INSTANCE;
            default:
                if (!s.startsWith("corpus_")) {
                    throw new ParseException("!s.startsWith(\"corpus_\")", 0);
                }
                return new TransformationKey(s, true);
        }
    }

}
