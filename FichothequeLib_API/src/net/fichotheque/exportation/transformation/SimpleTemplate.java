/* FichothequeLib_API - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.transformation;

import java.io.StringReader;
import java.util.Map;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import net.fichotheque.extraction.def.ExtractionDef;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public interface SimpleTemplate {

    public Attributes getAttributes();

    public ExtractionDef getCustomExtractionDef();

    public String getMimeType();

    public String getCharset();

    /**
     * paramMap peut être nul
     */
    public String transform(Source extractionSource, Map<String, Object> paramMap);

    public default String transform(String extractionString, Map<String, Object> paramMap) {
        return transform(new StreamSource(new StringReader(extractionString)), paramMap);
    }

}
