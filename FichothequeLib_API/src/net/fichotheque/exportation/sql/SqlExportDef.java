/* FichothequeLib_API - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.sql;

import java.text.ParseException;
import java.util.List;
import net.fichotheque.selection.SelectionOptions;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface SqlExportDef {

    public String getName();

    public Labels getTitleLabels();

    public Attributes getAttributes();

    /**
     * Non nul mais peut être de longueur nulle
     *
     * @return
     */
    public String getSqlExportClassName();


    /**
     * Non nul mais peut être de longueur nulle
     *
     * @return
     */
    public String getTableExportName();

    /**
     * Non nul mais peut être de longueur nulle
     *
     * @return
     */
    public String getTargetName();

    public RelativePath getTargetPath();

    /**
     * Non nul mais peut être de longueur nulle
     *
     * @return
     */
    public String getFileName();

    /**
     * Non nul mais peut être de longueur nulle
     *
     * @return
     */
    public String getPostCommand();

    public List<Param> getParamList();

    public SelectionOptions getSelectionOptions();


    public interface Param {

        public String getName();

        public String getValue();

    }

    public static void checkSqlExportName(String name) throws ParseException {
        StringUtils.checkTechnicalName(name, true);
    }

}
