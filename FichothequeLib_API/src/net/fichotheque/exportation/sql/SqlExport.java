/* FichothequeLib_API - Copyright (c) 2007-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.sql;

import java.io.IOException;
import java.io.OutputStream;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.TableExport;
import net.fichotheque.exportation.table.TableExportContext;
import net.fichotheque.extraction.ExtractionContext;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface SqlExport {

    public void setParameter(String name, String value);

    public void setTableExport(TableExport tableExport, TableExportContext tableExportContext);

    public void setExtractionContext(ExtractionContext extractionContext);

    public void setCellEngine(CellEngine cellEngine);

    public void setPredicate(@Nullable Predicate<SubsetItem> predicate);

    public void exportDump(OutputStream outputStream) throws IOException;

}
