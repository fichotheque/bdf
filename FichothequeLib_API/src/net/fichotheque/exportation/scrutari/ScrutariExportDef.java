/* FichothequeLib_API - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.scrutari;

import java.text.ParseException;
import java.util.List;
import net.fichotheque.selection.SelectionOptions;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariExportDef {

    /**
     * N'est ni nul, ni de longueur nulle.
     */
    public String getName();

    /**
     * N'est ni nul, ni de longueur nulle.
     */
    public String getAuthority();

    /**
     * N'est ni nul, ni de longueur nulle.
     */
    public String getBaseName();

    /**
     * Non nul mais peut être de longueur nulle
     */
    public String getBaseIcon();

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public List<CorpusScrutariDef> getCorpusScrutariDefList();

    public List<ThesaurusScrutariDef> getThesaurusScrutariDefList();

    public AccoladePattern getFicheHrefPattern();

    public AccoladePattern getMotcleHrefPattern();

    public SelectionOptions getSelectionOptions();

    public Langs getUiLangs();

    public Labels getCustomBaseIntitule(int baseIntituleType);

    /**
     * Non nul mais peut être de longueur nulle
     *
     * @return
     */
    public String getTargetName();

    public RelativePath getTargetPath();

    public List<String> getIncludeTokenList();

    public static void checkScrutariExportName(String name) throws ParseException {
        StringUtils.checkTechnicalName(name, true);
    }

}
