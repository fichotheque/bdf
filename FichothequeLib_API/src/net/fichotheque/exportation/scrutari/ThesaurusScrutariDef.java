/* FichothequeLib_API - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.scrutari;

import java.util.List;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public interface ThesaurusScrutariDef {

    /**
     * N'est jamais nul.
     */
    public SubsetKey getThesaurusKey();

    public String getFieldGenerationSource();

    public boolean isWholeThesaurus();

    public List<String> getIncludeTokenList();

}
