/* FichothequeLib_API - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.scrutari;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.format.FormatSourceKey;
import net.mapeadores.util.text.AccoladePattern;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusScrutariDef {

    public final static String LANGUI_MODE = "langui";
    public final static String THESAURUS_MODE = "thesaurus";
    public final static String FIELD_MODE = "field";
    public final static String CUSTOM_MODE = "custom";

    public SubsetKey getCorpusKey();

    /**
     * Peut-être nul.
     */
    public FormatSourceKey getDateFormatSourceKey();

    public String getFieldGenerationSource();

    public AccoladePattern getHrefPattern();

    public String getMultilangMode();

    public String getMultilangParam();

    public List<String> getIncludeTokenList();

}
