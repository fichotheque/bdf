/* FichothequeLib_API - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.balayage;

import java.util.List;
import net.fichotheque.selection.DocumentQuery;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.IllustrationQuery;
import net.fichotheque.selection.MotcleQuery;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageUnit {

    /**
     * Non nul, peut-être de longueur nulle
     */
    public String getName();

    public String getType();

    public boolean isGlobalSelect();

    public String getExtractionPath();

    public List<String> getModeList();

    public List<BalayageOutput> getOutputList();

    public FicheQuery getFicheQuery();

    public MotcleQuery getMotcleQuery();

    public IllustrationQuery getIllustrationQuery();

    public DocumentQuery getDocumentQuery();

    public List<String> getExtensionList();

    public default BalayageOutput getOutput(String outputName) {
        for (BalayageOutput output : getOutputList()) {
            String on = output.getName();
            if (!on.isEmpty()) {
                if (on.equals(outputName)) {
                    return output;
                }
            }
        }
        return null;
    }

    public default boolean hasMode(String mode) {
        for (String value : getModeList()) {
            if (value.equals(mode)) {
                return true;
            }
        }
        return false;
    }

}
