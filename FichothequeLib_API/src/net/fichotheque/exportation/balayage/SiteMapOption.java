/* BdfServer_API - Copyright (c) 2009-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.balayage;


/**
 *
 * @author Vincent Calame
 */
public interface SiteMapOption {

    public String getPath();

    /**
     *
     * @return Nom à utiliser pour le fichier SiteMap
     */
    public String getFileName();

    public String getBaseUrl();

}
