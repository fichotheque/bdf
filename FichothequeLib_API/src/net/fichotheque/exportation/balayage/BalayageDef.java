/* FichothequeLib_API - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.balayage;

import java.util.List;
import net.fichotheque.selection.SelectionOptions;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageDef {

    public String getName();

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public Langs getLangs();

    /**
     * Non nul mais peut être de longueur nulle
     *
     * @return
     */
    public String getTargetName();

    public RelativePath getTargetPath();

    public String getDefaultLangOption();

    public boolean ignoreTransformation();

    public SelectionOptions getSelectionOptions();

    public BalayagePostscriptum getPostscriptum();

    public List<BalayageUnit> getBalayageUnitList();

    public default BalayageUnit getBalayageUnit(String unitName) {
        for (BalayageUnit balayageUnit : getBalayageUnitList()) {
            String bn = balayageUnit.getName();
            if (!bn.isEmpty()) {
                if (bn.equals(unitName)) {
                    return balayageUnit;
                }
            }
        }
        return null;
    }

}
