/* FichothequeLib_API - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.balayage;

import java.util.List;
import net.mapeadores.util.xml.DocumentFragmentHolder;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageDescription {

    public final static String CONTAINS_ERRORS_STATE = "contains_errors";
    public final static String OK_STATE = "ok";

    public default String getName() {
        return getBalayageDef().getName();
    }

    public BalayageDef getBalayageDef();

    public String getState();

    public List<BalayageContentDescription> getBalayageContentDescriptionList();

    /**
     * N'est jamais nul. Vide si subDir n'existe pas. Classés par ordre
     * alphabétique du nom. La chaine vide pour subDir indique la racine.
     *
     * @param subDir
     * @return
     */
    public List<BalayageContentDescription> getBalayageContentDescriptionList(String subDir);

    public DocumentFragmentHolder getIncludeCatalog();

}
