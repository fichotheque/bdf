/* BdfServer_API - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.balayage;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageConstants {

    public final static String UNIQUE_TYPE = "unique";
    public final static String CORPUS_TYPE = "corpus";
    public final static String FICHE_TYPE = "fiche";
    public final static String THESAURUS_TYPE = "thesaurus";
    public final static String MOTCLE_TYPE = "motcle";
    public final static String SPHERE_TYPE = "sphere";
    public final static String REDACTEUR_TYPE = "redacteur";
    public final static String DOCUMENT_TYPE = "document";
    public final static String ILLUSTRATION_TYPE = "illustration";
    public final static String LANGOPTION_UNKNOWN = "";
    public final static String LANGOPTION_NONE = "none";
    public final static String LANGOPTION_DIR = "dir";

    public static String checkLangOption(String langOption) {
        if (langOption == null) {
            return LANGOPTION_UNKNOWN;
        }
        switch (langOption) {
            case LANGOPTION_DIR:
                return LANGOPTION_DIR;
            case LANGOPTION_NONE:
                return LANGOPTION_NONE;
            case LANGOPTION_UNKNOWN:
                return LANGOPTION_UNKNOWN;
            default:
                throw new IllegalArgumentException("wrong value : " + langOption);
        }
    }

    public static String checkUnitType(String unitType) {
        switch (unitType) {
            case UNIQUE_TYPE:
                return UNIQUE_TYPE;
            case CORPUS_TYPE:
                return CORPUS_TYPE;
            case FICHE_TYPE:
                return FICHE_TYPE;
            case THESAURUS_TYPE:
                return THESAURUS_TYPE;
            case MOTCLE_TYPE:
                return MOTCLE_TYPE;
            case SPHERE_TYPE:
                return SPHERE_TYPE;
            case REDACTEUR_TYPE:
                return REDACTEUR_TYPE;
            case DOCUMENT_TYPE:
                return DOCUMENT_TYPE;
            case ILLUSTRATION_TYPE:
                return ILLUSTRATION_TYPE;
            default:
                throw new IllegalArgumentException("wrong type value: " + unitType);
        }
    }

}
