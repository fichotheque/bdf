/* FichothequeLib_API - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.balayage;

import java.util.List;
import net.mapeadores.util.logging.MessageByLine;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageContentDescription {

    public final static short OK_STATE = 1;
    public final static short XML_ERROR_STATE = 2;

    public short getState();

    public String getBalayageName();

    public String getPath();

    /**
     * Ordonné suivant le numéro de ligne
     */
    public List<MessageByLine> getMessageByLineList();

}
