/* FichothequeLib_API - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.exportation.balayage;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.text.AccoladePattern;


/**
 *
 * @author Vincent Calame
 */
public interface BalayageOutput {

    public BalayageUnit getBalayageUnit();

    /**
     * Non nul mais peut-être de longueur nulle
     */
    public String getName();

    /**
     * Non nul mais peut-être de longueur nulle
     */
    public String getXsltPath();

    @Nullable
    public AccoladePattern getPattern();

    /**
     * Retourne une valeur nulle ou de longueur non nulle.
     */
    @Nullable
    public AccoladePattern getOutputPath();

    /**
     * Non nul mais peut-être de longueur nulle
     */
    public String getMode();

    /**
     * Non nul
     */
    public String getLangOption();

    public boolean isCleanBefore();

    public String getReversePath();

    public boolean isIncludeOutput();

    public List<Param> getParamList();

    @Nullable
    public Langs getCustomLangs();

    /**
     * Non nul mais peut-être de longueur nulle
     */
    public String getPosttransform();


    public interface Param {

        public String getName();

        public String getValue();

    }

}
