/* FichothequeLib_API - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque;


/**
 *
 * @author Vincent Calame
 */
public interface FichothequeConstants {

    public final static String LONG_PHRASE = "long";
    public final static String FICHE_PHRASE = "fiche";
    public final static String NEWFICHE_PHRASE = "newfiche";
    public final static String FICHESTYLE_PHRASE = "fichestyle";
    public final static String SATELLITE_PHRASE = "satellite";
    public final static String NAMING_PHRASE = "naming";
    public final static String SEVERE_FICHOTHEQUE = "severe.fichotheque";
    public final static String LIAGE_NAME = "liage";
    public final static String PARENTAGE_NAME = "parentage";
    public final static String DATECREATION_NAME = "date_creation";
    public final static String DATEMODIFICATION_NAME = "date_modification";
    public final static String ACTIVE_STATUS = "active";
    public final static String READONLY_STATUS = "readonly";
    public final static String INACTIVE_STATUS = "inactive";
    public final static String GROUP_STATUS = "group";
    public final static String OBSOLETE_STATUS = "obsolete";

    public static String checkRedacteurStatus(String status) {
        switch (status) {
            case ACTIVE_STATUS:
                return ACTIVE_STATUS;
            case READONLY_STATUS:
                return READONLY_STATUS;
            case INACTIVE_STATUS:
                return INACTIVE_STATUS;
            default:
                throw new IllegalArgumentException("Unknown status: " + status);
        }
    }

    public static String checkMotcleStatus(String status) {
        switch (status) {
            case ACTIVE_STATUS:
                return ACTIVE_STATUS;
            case GROUP_STATUS:
                return GROUP_STATUS;
            case OBSOLETE_STATUS:
                return OBSOLETE_STATUS;
            default:
                throw new IllegalArgumentException("Unknown status: " + status);
        }
    }

    public static String checkSpecialIncludeName(String name) {
        switch (name) {
            case FichothequeConstants.LIAGE_NAME:
                return FichothequeConstants.LIAGE_NAME;
            case FichothequeConstants.PARENTAGE_NAME:
            case "rattachement":
                return FichothequeConstants.PARENTAGE_NAME;
            case FichothequeConstants.DATECREATION_NAME:
                return FichothequeConstants.DATECREATION_NAME;
            case FichothequeConstants.DATEMODIFICATION_NAME:
                return FichothequeConstants.DATEMODIFICATION_NAME;
            default:
                throw new IllegalArgumentException("Unknown name: " + name);
        }
    }

}
