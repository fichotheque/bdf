/* FichothequeLib_API - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class CroisementRevision {

    private final List<Lien> lienList = new ArrayList<Lien>();

    public CroisementRevision() {

    }

    public List<Lien> getLienList() {
        return lienList;
    }

    public void addLien(Lien lien) {
        lienList.add(lien);
    }

}
