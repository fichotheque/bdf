/* FichothequeLib_API - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;


/**
 *
 * @author Vincent Calame
 */
public final class LiaisonSortKey implements Comparable<LiaisonSortKey> {

    private final int id;
    private final int position;

    public LiaisonSortKey(int id, int position) {
        this.id = id;
        this.position = position;
    }

    @Override
    public int hashCode() {
        return id * 1000 + position;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        LiaisonSortKey otherLiaisonSortKey = (LiaisonSortKey) other;
        return ((otherLiaisonSortKey.id == this.id) && (otherLiaisonSortKey.position == this.position));
    }

    @Override
    public String toString() {
        return id + "/" + position;
    }

    @Override
    public int compareTo(LiaisonSortKey otherLiaisonSortKey) {
        if (otherLiaisonSortKey.position < 1) {
            if (this.position < 1) {
                return compareId(otherLiaisonSortKey);
            } else {
                return -1;
            }
        } else if (this.position < 1) {
            return 1;
        } else if (otherLiaisonSortKey.position > this.position) {
            return -1;
        } else if (otherLiaisonSortKey.position < this.position) {
            return 1;
        } else {
            return compareId(otherLiaisonSortKey);
        }
    }

    private int compareId(LiaisonSortKey otherLiaisonSortKey) {
        if (otherLiaisonSortKey.id > this.id) {
            return -1;
        } else if (otherLiaisonSortKey.id < this.id) {
            return 1;
        } else {
            return 0;
        }
    }

}
