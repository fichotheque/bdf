/* FichothequeLib_API - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface CroisementChange {

    public List<String> getRemovedModeList();

    public List<Lien> getChangedLienList();

}
