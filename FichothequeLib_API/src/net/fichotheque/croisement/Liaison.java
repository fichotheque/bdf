/* FichothequeLib_API - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;

import net.fichotheque.SubsetItem;
import net.fichotheque.utils.CroisementUtils;


/**
 *
 * @author Vincent Calame
 */
public interface Liaison {

    public SubsetItem getSubsetItem();

    public Croisement getCroisement();

    public Lien getLien();

    public default int getPosition() {
        SubsetItem subsetItem = getSubsetItem();
        CroisementKey croisementKey = getCroisement().getCroisementKey();
        Lien lien = getLien();
        return CroisementUtils.getPosition(subsetItem, croisementKey, lien);
    }

    public default int getPositionNumber() {
        SubsetItem subsetItem = getSubsetItem();
        CroisementKey croisementKey = getCroisement().getCroisementKey();
        Lien lien = getLien();
        int id = subsetItem.getId();
        if ((croisementKey.getId1() == id) && (croisementKey.getSubsetKey1().equals(subsetItem.getSubsetKey()))) {
            return 1;
        } else {
            return 2;
        }
    }

}
