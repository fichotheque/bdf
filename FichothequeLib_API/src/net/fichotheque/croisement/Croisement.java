/* FichothequeLib_API - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;

import java.util.List;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public interface Croisement {

    public CroisementKey getCroisementKey();

    public List<Lien> getLienList();

    public Lien getLienByMode(String mode);

    public Attributes getAttributes();

    public default boolean containsMode(String mode) {
        return (getLienByMode(mode) != null);
    }

}
