/* FichothequeLib_API - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;

import java.util.List;
import net.fichotheque.SubsetItem;


/**
 *
 * @author Vincent Calame
 */
public interface CroisementChanges {

    public List<SubsetItem> getRemovedList();

    public List<Entry> getEntryList();


    public interface Entry {

        public SubsetItem getSubsetItem();

        public CroisementChange getCroisementChange();

    }

}
