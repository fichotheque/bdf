/* FichothequeLib_API - Copyright (c) 2008-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;

import java.util.List;
import net.fichotheque.SubsetItem;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface Croisements {


    public List<Entry> getEntryList();

    /**
     * Raccourci de getEntryList().isEmpty()
     *
     * @return
     */
    public default boolean isEmpty() {
        return getEntryList().isEmpty();
    }

    /**
     * Raccourci de getEntryList().get(0). À la différence près que cela
     * retourne null et non IndexNotFoundException s'il n'y a pas d'éléments
     *
     * @return
     */
    @Nullable
    public default SubsetItem getFirstSubsetItem() {
        List<Entry> list = getEntryList();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0).getSubsetItem();
    }


    public interface Entry {

        public SubsetItem getSubsetItem();

        public Croisement getCroisement();

    }

}
