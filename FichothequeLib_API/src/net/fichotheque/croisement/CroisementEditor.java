/* FichothequeLib_API - Copyright (c) 2006-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;

import net.fichotheque.FichothequeEditor;
import net.fichotheque.SubsetItem;


/**
 * Éditeur sur l'ensemble des croisements.
 *
 * @author Vincent Calame
 */
public interface CroisementEditor {

    public FichothequeEditor getFichothequeEditor();

    public boolean removeCroisement(SubsetItem subsetItem1, SubsetItem subsetItem2);

    public Croisement updateCroisement(SubsetItem subsetItem1, SubsetItem subsetItem2, CroisementChange croisementChange);

    public void updateCroisements(SubsetItem subsetItem, CroisementChanges croisementChanges);

}
