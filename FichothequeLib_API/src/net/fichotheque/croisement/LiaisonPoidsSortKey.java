/* FichothequeLib_API - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.croisement;


/**
 *
 * @author Vincent Calame
 */
public final class LiaisonPoidsSortKey implements Comparable<LiaisonPoidsSortKey> {

    private int poids;
    private final int id;
    private final int position;

    public LiaisonPoidsSortKey(int poids, int id, int position) {
        this.id = id;
        this.position = position;
    }

    @Override
    public int hashCode() {
        return id * 1000 + position * 10 + poids;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        LiaisonPoidsSortKey otherLiaisonPoidsSortKey = (LiaisonPoidsSortKey) other;
        return ((otherLiaisonPoidsSortKey.id == this.id) && (otherLiaisonPoidsSortKey.position == this.position) && (otherLiaisonPoidsSortKey.poids == this.poids));
    }

    @Override
    public String toString() {
        return id + "/" + position;
    }

    @Override
    public int compareTo(LiaisonPoidsSortKey otherLiaisonPoidsSortKey) {
        if (otherLiaisonPoidsSortKey.poids > this.poids) {
            return - 1;
        }
        if (otherLiaisonPoidsSortKey.poids < this.poids) {
            return 1;
        }
        if (otherLiaisonPoidsSortKey.position < 1) {
            if (this.position < 1) {
                return compareId(otherLiaisonPoidsSortKey);
            } else {
                return -1;
            }
        } else if (this.position < 1) {
            return 1;
        } else if (otherLiaisonPoidsSortKey.position > this.position) {
            return -1;
        } else if (otherLiaisonPoidsSortKey.position < this.position) {
            return 1;
        } else {
            return compareId(otherLiaisonPoidsSortKey);
        }
    }

    private int compareId(LiaisonPoidsSortKey otherLiaisonPoidsSortKey) {
        if (otherLiaisonPoidsSortKey.id > this.id) {
            return -1;
        } else if (otherLiaisonPoidsSortKey.id < this.id) {
            return 1;
        } else {
            return 0;
        }
    }

}
