/* FichothequeLib_API - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.syntax;

import java.io.IOException;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.fiche.Link;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.fiche.Nombre;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Pays;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.text.SeparatorOptions;


/**
 *
 * @author Vincent Calame
 */
public final class FormSyntax {

    public static final SeparatorOptions DEFAULT_INLINE_SEPARATOROPTIONS = new SeparatorOptions(';', true, true);
    public static final SeparatorOptions DEFAULT_BLOCK_SEPARATOROPTIONS = new SeparatorOptions('\n', true, false);

    private FormSyntax() {
    }

    public static String toString(FicheItems ficheItems, Fichotheque fichotheque, SeparatorOptions separatorOptions, Parameters parameters) {
        int size = ficheItems.size();
        if (size == 0) {
            return "";
        }
        StringBuilder buf = new StringBuilder(128);
        for (int k = 0; k < size; k++) {
            FicheItem ficheItem = ficheItems.get(k);
            buf.append(toString(ficheItem, fichotheque, parameters));
            if (k < (size - 1)) {
                buf.append(separatorOptions.getSeparator());
                if (separatorOptions.isSpaceInclude()) {
                    buf.append(' ');
                }
            } else if (separatorOptions.isLastInclude()) {
                buf.append(separatorOptions.getSeparator());
                if (separatorOptions.isSpaceInclude()) {
                    buf.append(' ');
                }
            }
        }
        return buf.toString();
    }

    public static String toString(FicheItem item, Fichotheque fichotheque, Parameters parameters) {
        if (item instanceof Item) {
            return toString((Item) item);
        } else if (item instanceof Personne) {
            return toString((Personne) item, fichotheque, getDefaultSphereKey(parameters));
        } else if (item instanceof Langue) {
            return toString((Langue) item);
        } else if (item instanceof Pays) {
            return toString((Pays) item);
        } else if (item instanceof Datation) {
            return toString((Datation) item);
        } else if (item instanceof Courriel) {
            return toString((Courriel) item);
        } else if (item instanceof Link) {
            return toString((Link) item);
        } else if (item instanceof Nombre) {
            return toString((Nombre) item, getDecimalChar(parameters));
        } else if (item instanceof Montant) {
            return toString((Montant) item, getDecimalChar(parameters));
        } else if (item instanceof Geopoint) {
            return toString((Geopoint) item, getDecimalChar(parameters));
        } else if (item instanceof Para) {
            return toString((Para) item);
        } else if (item instanceof Image) {
            return toString((Image) item);
        } else {
            throw new IllegalArgumentException("unknown FicheItem implementation");
        }
    }

    public static String toString(DegreDecimal degreDecimal, Parameters parameters) {
        return degreDecimal.toDecimal().toStringWithBlank(getDecimalChar(parameters));
    }

    public static String toString(Para para) {
        StringBuilder buf = new StringBuilder();
        try {
            FicheblockSyntax.appendTextContent(buf, para, true);
        } catch (IOException ioe) {
        }
        return buf.toString();
    }

    public static String toString(Item item) {
        String value = item.getValue();
        return escapeString(value);
    }

    public static String escapeString(String s) {
        StringBuilder buf = new StringBuilder(s.length() + 20);
        escapeString(buf, s);
        return buf.toString();
    }

    private static void escapeString(StringBuilder buf, String s) {
        try {
            escapeString((Appendable) buf, s);
        } catch (IOException ioe) {
        }
    }

    public static void escapeString(Appendable buf, String s) throws IOException {
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char carac = s.charAt(i);
            switch (carac) {
                case '\u00A0':
                case '\u202F':
                    buf.append('~');
                    break;
                default:
                    buf.append(carac);

            }
        }
    }

    public static String toString(Personne personne, Fichotheque fichotheque, SubsetKey defaultSphereKey) {
        String redacteurGlobalId = personne.getRedacteurGlobalId();
        if (redacteurGlobalId != null) {
            Redacteur redacteur = SphereUtils.getRedacteur(fichotheque, redacteurGlobalId);
            if (redacteur != null) {
                if ((defaultSphereKey != null) && (defaultSphereKey.equals(redacteur.getSubsetKey()))) {
                    return redacteur.getLogin();
                } else {
                    return redacteur.getBracketStyle();
                }
            } else {
                return redacteurGlobalId;
            }
        } else {
            PersonCore personCore = personne.getPersonCore();
            StringBuilder buf = new StringBuilder(64);
            escapeString(buf, personCore.getSurname());
            buf.append('*');
            if (personCore.isSurnameFirst()) {
                buf.append('%');
            }
            escapeString(buf, personCore.getForename());
            String organism = personne.getOrganism();
            if (organism != null) {
                buf.append('*');
                escapeString(buf, organism);
            }
            return buf.toString();
        }
    }

    public static String toString(Langue langue) {
        return langue.getLang().toString();
    }

    public static String toString(Pays pays) {
        return pays.getCountry().toString();
    }

    public static String toString(Datation datation) {
        return datation.toString();
    }

    public static String toString(Courriel courriel) {
        return courriel.toString();
    }

    public static String toString(Link link) {
        StringBuilder buf = new StringBuilder(128);
        String href = link.getHref();
        buf.append(href);
        String title = link.getTitle();
        if (title.length() == 0) {
            title = null;
        } else if (title.equals(href)) {
            title = null;
        } else if ((href.length() > 8) && (title.equals(href.substring(7)))) {
            title = null;
        } else {
            buf.append('*');
            escapeString(buf, title);
        }
        String comment = link.getComment();
        if (comment.length() > 0) {
            if (title == null) {
                buf.append('*');
            }
            buf.append('*');
            escapeString(buf, comment);
        }
        return buf.toString();
    }

    public static String toString(Image image) {
        StringBuilder buf = new StringBuilder(128);
        String src = image.getSrc();
        String alt = image.getAlt();
        String title = image.getTitle();
        buf.append(src);
        if (alt.length() > 0) {
            buf.append('*');
            escapeString(buf, alt);
        }
        if (title.length() > 0) {
            if (alt.length() == 0) {
                buf.append('*');
            }
            buf.append('*');
            escapeString(buf, alt);
        }
        return buf.toString();
    }

    public static String toString(Nombre nombre, char decimalChar) {
        String valString = nombre.getDecimal().toStringWithBlank(decimalChar);
        return valString;
    }

    public static String toString(Montant montant, char decimalChar) {
        String valString = montant.getDecimal().toStringWithBlank(decimalChar);
        String cur = montant.getCurrency().getCurrencyCode();
        return valString + cur;
    }

    public static String toString(Geopoint geopoint, char decimalChar) {
        StringBuilder buf = new StringBuilder();
        buf.append(geopoint.getLatitude().toDecimal().toStringWithBlank(decimalChar));
        buf.append("* ");
        buf.append(geopoint.getLongitude().toDecimal().toStringWithBlank(decimalChar));
        return buf.toString();
    }


    public static Parameters parameters() {
        return new Parameters();
    }

    public static Parameters parameters(Parameters others) {
        return new Parameters(others);
    }

    private static char getDecimalChar(Parameters parameters) {
        if (parameters == null) {
            return '.';
        }
        return parameters.decimalChar();
    }

    private static SubsetKey getDefaultSphereKey(Parameters parameters) {
        if (parameters == null) {
            return null;
        }
        return parameters.defautSphereKey();
    }


    public static class Parameters {

        private SubsetKey defautSphereKey = null;
        private char decimalChar = '.';

        private Parameters() {
        }

        private Parameters(Parameters other) {
            this.decimalChar = other.decimalChar;
            this.defautSphereKey = other.defautSphereKey;
        }

        public SubsetKey defautSphereKey() {
            return defautSphereKey;
        }

        public Parameters defautSphereKey(SubsetKey defautSphereKey) {
            this.defautSphereKey = defautSphereKey;
            return this;
        }

        public char decimalChar() {
            return decimalChar;
        }

        public Parameters decimalChar(char decimalChar) {
            this.decimalChar = decimalChar;
            return this;
        }

    }

}
