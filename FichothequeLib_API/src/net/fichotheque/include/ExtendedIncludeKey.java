/* FichothequeLib_API - Copyright (c) 2018-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.include;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public final class ExtendedIncludeKey implements IncludeMode {

    public final static String MASTER_PREFIX = "_master_";
    private final static Map<String, ExtendedIncludeKey> internMap = new HashMap<String, ExtendedIncludeKey>();
    private final String name;
    private final IncludeKey rootIncludeKey;
    private final boolean master;

    private ExtendedIncludeKey(String name, IncludeKey rootIncludeKey, boolean master) {
        this.name = name;
        this.rootIncludeKey = rootIncludeKey;
        this.master = master;
        intern(this);
    }

    public boolean isMaster() {
        return master;
    }

    public IncludeKey getRootIncludeKey() {
        return rootIncludeKey;
    }

    public SubsetKey getSubsetKey() {
        return rootIncludeKey.getSubsetKey();
    }

    public short getCategory() {
        return rootIncludeKey.getSubsetKey().getCategory();
    }

    @Override
    public String getMode() {
        return rootIncludeKey.getMode();
    }

    @Override
    public int getPoidsFilter() {
        return rootIncludeKey.getPoidsFilter();
    }

    public boolean hasPoidsFilter() {
        return rootIncludeKey.hasPoidsFilter();
    }

    public String getKeyString() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        ExtendedIncludeKey otherKey = (ExtendedIncludeKey) other;
        return (otherKey.name.equals(this.name));
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public static ExtendedIncludeKey parse(String name) throws ParseException {
        ExtendedIncludeKey current = internMap.get(name);
        if (current != null) {
            return current;
        }
        boolean master = false;
        IncludeKey rootIncludeKey;
        if (name.startsWith(MASTER_PREFIX)) {
            master = true;
            rootIncludeKey = IncludeKey.parse(name.substring(MASTER_PREFIX.length()));
            name = MASTER_PREFIX + rootIncludeKey;
        } else {
            rootIncludeKey = IncludeKey.parse(name);
            name = rootIncludeKey.getKeyString();
        }
        return new ExtendedIncludeKey(name, rootIncludeKey, master);
    }

    public static ExtendedIncludeKey build(String name) {
        try {
            return parse(name);
        } catch (ParseException pe) {
            return null;
        }
    }

    public static ExtendedIncludeKey newInstance(IncludeKey rootIncludeKey) {
        String name = rootIncludeKey.getKeyString();
        ExtendedIncludeKey current = internMap.get(name);
        if (current != null) {
            return current;
        }
        return new ExtendedIncludeKey(name, rootIncludeKey, false);
    }

    public static ExtendedIncludeKey newInstance(IncludeKey rootIncludeKey, boolean master) {
        String name;
        if (master) {
            name = MASTER_PREFIX + rootIncludeKey;
        } else {
            name = rootIncludeKey.getKeyString();
        }
        ExtendedIncludeKey current = internMap.get(name);
        if (current != null) {
            return current;
        }
        return new ExtendedIncludeKey(name, rootIncludeKey, master);
    }

    private synchronized static void intern(ExtendedIncludeKey includeKey) {
        internMap.put(includeKey.name, includeKey);
    }

}
