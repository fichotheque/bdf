/* FichothequeLib_API - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.include;

import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public interface LiageTest {

    /**
     * Retourne une instance de Boolean ou RangeList. Si Boolean, vaut true si
     * corpusKey doit faire partie du liage, false sinon Si c'est une instance
     * de RangeList, cela indique que cela doit faire partie du liage sauf pour
     * les poids indiqués.
     */
    public Object ownsToLiage(SubsetKey corpusKey);

}
