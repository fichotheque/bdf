/* BdfServer_Html - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.forms;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.users.BdfUser;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.UserLangContext;


/**
 *
 * @author Vincent Calame
 */
public class FormParameters {

    private final static Map<String, String> EMPTY_MAP = Collections.emptyMap();
    private final BdfServer bdfServer;
    private final UserLangContext userLangContext;
    private final BdfUser bdfUser;
    private final Map<SubsetKey, Map<String, String>> mapOfMap = new HashMap<SubsetKey, Map<String, String>>();
    private String namePrefix = "";
    private Lang ficheLang = null;
    private String gotoPrefix = "";

    public FormParameters(BdfServer bdfServer, BdfUser bdfUser) {
        this.bdfServer = bdfServer;
        this.bdfUser = bdfUser;
        this.userLangContext = bdfUser;
    }

    public FormParameters(BdfServer bdfServer, UserLangContext userLangContext) {
        this.bdfServer = bdfServer;
        this.bdfUser = null;
        this.userLangContext = userLangContext;
    }

    public FormParameters namePrefix(String namePrefix) {
        if (namePrefix == null) {
            namePrefix = "";
        }
        this.namePrefix = namePrefix;
        return this;
    }

    public String namePrefix() {
        return namePrefix;
    }

    public UserLangContext userLangContext() {
        return userLangContext;
    }

    public Lang workingLang() {
        return userLangContext.getWorkingLang();
    }

    public BdfServer bdfServer() {
        return bdfServer;
    }

    public Lang ficheLang() {
        return ficheLang;
    }

    public FormParameters ficheLang(Lang ficheLang) {
        this.ficheLang = ficheLang;
        return this;
    }

    public FormParameters gotoPrefix(String gotoPrefix) {
        if (gotoPrefix == null) {
            gotoPrefix = "";
        }
        this.gotoPrefix = gotoPrefix;
        return this;
    }

    public String gotoPrefix() {
        return gotoPrefix;
    }

    public Attributes getUserAttributes() {
        if (bdfUser != null) {
            return bdfUser.getPrefs().getAttributes();
        } else {
            return AttributeUtils.EMPTY_ATTRIBUTES;
        }
    }

    public Map<String, String> getStoreMap(SubsetKey subsetKey) {
        Map<String, String> map = mapOfMap.get(subsetKey);
        if (map == null) {
            if (bdfUser != null) {
                map = bdfServer.getStoredValues(bdfUser, "ficheform_" + subsetKey.toString());
            } else {
                map = EMPTY_MAP;
            }
            mapOfMap.put(subsetKey, map);
        }
        return map;
    }

    public static FormParameters init(BdfServer bdfServer, UserLangContext userLangContext) {
        return new FormParameters(bdfServer, userLangContext);
    }

    public static FormParameters init(BdfParameters bdfParameters) {
        return new FormParameters(bdfParameters.getBdfServer(), bdfParameters.getBdfUser());
    }

}
