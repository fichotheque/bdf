/* BdfServer_Html - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.forms;

import fr.exemole.bdfserver.api.exportation.table.FicheTableParameters;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.html.consumers.Choices;
import java.util.function.Consumer;
import net.fichotheque.exportation.table.TableExportConstants;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class TableExportFormHtml {

    public final static Consumer<HtmlPrinter> HEADERTYPE_TITLE = Choices.title("_ title.tableexport.headertype");
    public final static Consumer<HtmlPrinter> ODSSPECIFIC_TITLE = Choices.title("_ title.tableexport.odsspecific");

    private TableExportFormHtml() {
    }

    public static boolean printWithParameters(HtmlPrinter hp, FicheTableParameters ficheTableParameters) {
        hp
                .__(Choices.LIST, () -> {
                    printWithCheckbox(hp, ficheTableParameters, FicheTableParameters.WITH_SECTION, "_ label.tableexport.with_section");
                    printWithCheckbox(hp, ficheTableParameters, FicheTableParameters.WITH_CHRONO, "_ label.tableexport.with_chrono");
                    printWithCheckbox(hp, ficheTableParameters, FicheTableParameters.WITH_ADDENDAINCLUDE, "_ label.tableexport.with_addendainclude");
                    printWithCheckbox(hp, ficheTableParameters, FicheTableParameters.WITH_ALBUMINCLUDE, "_ label.tableexport.with_albuminclude");
                    printWithCheckbox(hp, ficheTableParameters, FicheTableParameters.WITH_CORPUSINCLUDE, "_ label.tableexport.with_corpusinclude");
                    printWithCheckbox(hp, ficheTableParameters, FicheTableParameters.WITH_THESAURUSINCLUDE, "_ label.tableexport.with_thesaurusinclude");
                });
        return true;
    }

    public static boolean printPatternModeRadios(HtmlPrinter hp, String currentPatternMode) {
        hp
                .__(Choices.LIST, () -> {
                    printPatternModeRadio(hp, FicheTableParameters.STANDARD_PATTERNMODE, "_ label.tableexport.patternmode_standard", currentPatternMode);
                    printPatternModeRadio(hp, FicheTableParameters.CODE_PATTERNMODE, "_ label.tableexport.patternmode_code", currentPatternMode);
                    printPatternModeRadio(hp, FicheTableParameters.FORMSYNTAX_PATTERNMODE, "_ label.tableexport.patternmode_formsyntax", currentPatternMode);
                });
        return true;
    }

    public static boolean printHeaderTypeRadios(HtmlPrinter hp, String currentHeaderType) {
        hp
                .__(Choices.LIST, () -> {
                    printHeaderTypeRadio(hp, TableExportConstants.COLUMNTITLE_HEADER, "_ label.tableexport.headertype_columntitle", currentHeaderType);
                    printHeaderTypeRadio(hp, TableExportConstants.COLUMNKEY_HEADER, "_ label.tableexport.headertype_columnkey", currentHeaderType);
                    printHeaderTypeRadio(hp, TableExportConstants.NONE_HEADER, "_ label.tableexport.headertype_none", currentHeaderType);
                });
        return true;
    }

    private static boolean printWithCheckbox(HtmlPrinter hp, FicheTableParameters ficheTableParameters, String withKey, String messageKey) {
        HtmlAttributes checkAttributes = hp.name(InteractionConstants.WITH_PARAMNAME).value(withKey).checked(ficheTableParameters.isWith(withKey));
        hp
                .__(Choices.checkboxLi(checkAttributes, messageKey));
        return true;
    }

    private static boolean printPatternModeRadio(HtmlPrinter hp, String patternMode, String messageKey, String currentPatternMode) {
        HtmlAttributes radioAttributes = hp.name(InteractionConstants.PATTERNMODE_PARAMNAME).value(patternMode).checked(currentPatternMode);
        hp
                .__(Choices.radioLi(radioAttributes, messageKey));
        return true;
    }

    private static boolean printHeaderTypeRadio(HtmlPrinter hp, String headerType, String messageKey, String currentHeaderType) {
        boolean checked = (headerType.equals(currentHeaderType));
        HtmlAttributes radioAttributes = hp.name(InteractionConstants.HEADERTYPE_PARAMNAME).value(headerType).checked(checked);
        hp
                .__(Choices.radioLi(radioAttributes, messageKey));
        return true;
    }

}
