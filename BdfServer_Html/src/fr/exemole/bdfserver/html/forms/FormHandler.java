/* BdfServer_Html - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.forms;

import fr.exemole.bdfserver.api.ficheform.FormElement;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.api.namespaces.FicheFormSpace;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.UserLangContext;


/**
 *
 * @author Vincent Calame
 */
public class FormHandler {

    private final FormParameters formParameters;

    public FormHandler(FormParameters formParameters) {
        this.formParameters = formParameters;
    }

    public FormParameters getFormParameters() {
        return formParameters;
    }

    public Lang getWorkingLang() {
        return formParameters.workingLang();
    }

    public UserLangContext getUserLangContext() {
        return formParameters.userLangContext();
    }

    public String getPrefixedName(String name) {
        String namePrefix = formParameters.namePrefix();
        if (!namePrefix.isEmpty()) {
            name = namePrefix + "_" + name;
        }
        return name;
    }

    public String getPrefixedName(FormElement formElement) {
        return getPrefixedName(getName(formElement));
    }

    public String getPrefixedName(FormElement.Field fieldElement) {
        CorpusField corpusField = fieldElement.getCorpusField();
        return getPrefixedName(corpusField.getFieldString());
    }

    public String getPrefixedName(FormElement.Include includeElement) {
        return getPrefixedName(includeElement.getIncludeName());
    }

    public String getPrefixedName(FieldKey fieldKey, short subtype) {
        SubfieldKey subfieldKey = SubfieldKey.toSubfieldKey(fieldKey, subtype);
        return getPrefixedName(subfieldKey.getKeyString());
    }

    public String getGoto(FormElement formElement) {
        String name = getName(formElement);
        String logoPrefix = formParameters.gotoPrefix();
        if (!logoPrefix.isEmpty()) {
            name = logoPrefix + "/" + name;
        }
        return name;
    }

    public HtmlAttributes getEntryAttributes(String otherName, FormElement formElement) {
        return getEntryAttributes(otherName.replace(':', '_'), formElement.isMandatory(), null, "ficheform-standard-Entry", formElement.getAttributes(), null);
    }

    public HtmlAttributes getEntryAttributes(FormElement formElement) {
        return getEntryAttributes(formElement, null, "ficheform-standard-Entry");
    }

    public HtmlAttributes getEntryAttributes(FormElement formElement, String entryType) {
        return getEntryAttributes(formElement, entryType, "ficheform-standard-Entry");
    }

    public HtmlAttributes getEntryAttributes(FormElement formElement, String entryType, String classes) {
        return getEntryAttributes(getPrefixedName(formElement).replace(':', '_'), formElement.isMandatory(), entryType, classes, formElement.getAttributes(), getGoto(formElement));
    }

    public HtmlAttributes getEntryAttributes(String key, boolean isMandatory, String entryType, String classes, Attributes attributes, String gotoString) {
        HtmlAttributes htmlAttributes = HA
                .attr("data-ficheform-role", "entry")
                .attr("data-ficheform-key", key)
                .attr(isMandatory, "data-ficheform-mandatory", "1")
                .attr("data-ficheform-entry", entryType)
                .attr("data-goto", gotoString)
                .classes(classes);
        Attribute attribute = attributes.getAttribute(FicheFormSpace.CLASSES_KEY);
        if (attribute != null) {
            for (String value : attribute) {
                htmlAttributes.addClass(value);
            }
        }
        return htmlAttributes;
    }

    public boolean printFormElement(HtmlPrinter hp, FormElement formElement) {
        if (formElement instanceof FormElement.Field) {
            return CorpusFieldFormHtml.print(hp, (FormElement.Field) formElement, this);
        } else if (formElement instanceof FormElement.Include) {
            return IncludeFormHtml.print(hp, (FormElement.Include) formElement, this);
        } else {
            return false;
        }
    }

    private String getName(FormElement formElement) {
        if (formElement instanceof FormElement.Field) {
            return ((FormElement.Field) formElement).getCorpusField().getFieldString();
        }
        if (formElement instanceof FormElement.Include) {
            return ((FormElement.Include) formElement).getIncludeName();
        }
        return "";
    }

    public static FormHandler init(FormParameters formParameters) {
        return new FormHandler(formParameters);
    }

    public static FormHandler build(BdfParameters bdfParameters) {
        return new FormHandler(FormParameters.init(bdfParameters));
    }

}
