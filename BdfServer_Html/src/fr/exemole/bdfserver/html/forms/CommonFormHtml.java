/* BdfServer_Html - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.forms;

import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.ficheform.FormElement;
import fr.exemole.bdfserver.api.namespaces.FicheFormSpace;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.TrustedHtml;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class CommonFormHtml {

    private CommonFormHtml() {
    }

    public static int getDefaultSize(String widthType) {
        switch (widthType) {
            case BdfServerConstants.WIDTH_SMALL:
                return 21;
            case BdfServerConstants.WIDTH_MEDIUM:
                return 39;
            case BdfServerConstants.WIDTH_LARGE:
                return 77;
            default:
                return 50;
        }
    }

    public static HtmlAttributes addTextInputAttributes(HtmlAttributes inputAttrs, Attributes attributes, Lang lang) {
        Attribute placeholderAttribute = attributes.getAttribute(FicheFormSpace.getPlaceholderAttributeKey(lang));
        Attribute maxlengthAttribute = attributes.getAttribute(FicheFormSpace.MAXLENGTH_KEY);
        int maxLength = -1;
        String placeholder = (placeholderAttribute != null) ? toString(placeholderAttribute) : null;
        if (maxlengthAttribute != null) {
            try {
                maxLength = Integer.parseInt(maxlengthAttribute.getFirstValue());
            } catch (NumberFormatException nfe) {

            }
        }
        return inputAttrs
                .attr("placeholder", placeholder)
                .attr((maxLength > 0), "maxlength", String.valueOf(maxLength));
    }

    private static String toString(Attribute attribute) {
        if (attribute.size() == 1) {
            return attribute.getFirstValue();
        } else {
            StringBuilder buf = new StringBuilder();
            for (String value : attribute) {
                if (buf.length() > 0) {
                    buf.append('\n');
                }
                buf.append(value);
            }
            return buf.toString();
        }
    }

    public static boolean printStandardLabel(HtmlPrinter hp, FormElement formElement, Lang lang, String inputId) {
        HtmlAttributes labelAttrs = HA.forId(inputId).attr("data-ficheform-role", "label").classes("ficheform-standard-Label");
        addTooltip(labelAttrs, formElement.getAttributes(), lang);
        hp
                .__breakLine()
                .LABEL(labelAttrs)
                .__escape(formElement.getLabel())
                ._LABEL()
                .__breakLine()
                .SPAN("ficheform-standard-Colon")
                .__escape(':')
                ._SPAN()
                .__breakLine();
        return true;
    }

    public static HtmlAttributes addTooltip(HtmlAttributes attrs, Attributes attributes, Lang lang) {
        Attribute tooltipAttribute = attributes.getAttribute(FicheFormSpace.getTooltipAttributeKey(lang));
        String tooltip = (tooltipAttribute != null) ? toString(tooltipAttribute) : null;
        return attrs
                .attr("data-tooltip", tooltip);
    }

    public static boolean printComment(HtmlPrinter hp, TrustedHtml html, String goTo) {
        hp
                .DIV(HA.classes("ficheform-Comment").attr("data-goto", goTo))
                .__breakLine()
                .__append(html)
                .__breakLine()
                ._DIV();
        return true;
    }

}
