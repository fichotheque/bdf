/* BdfServer_Html - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.forms;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.subsettree.SubsetTree;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.html.consumers.SelectOption;
import fr.exemole.bdfserver.html.consumers.SubsetTreeOptions;
import fr.exemole.bdfserver.html.consumers.attributes.Appelant;
import fr.exemole.bdfserver.html.consumers.attributes.Deploy;
import fr.exemole.bdfserver.tools.interaction.FicheQueryParser;
import fr.exemole.bdfserver.tools.subsettree.TreeFilterEngine;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.selection.CroisementCondition;
import net.fichotheque.selection.FicheCondition;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.FieldContentCondition;
import net.fichotheque.selection.MotcleCondition;
import net.fichotheque.selection.MotcleQuery;
import net.fichotheque.selection.PeriodCondition;
import net.fichotheque.selection.RangeCondition;
import net.fichotheque.selection.SubsetCondition;
import net.fichotheque.selection.UserCondition;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.conditions.ConditionsConstants;
import net.mapeadores.util.conditions.ConditionsUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.RangeUtils;


/**
 *
 * @author Vincent Calame
 */
public class SelectionFormPrinter {

    private final static SelectOption[] DISCARDFILTER_OPTIONS = {
        SelectOption.init(FicheQuery.DISCARDFILTER_ALL).textL10nObject("_ label.selection.discardfilter_all"),
        SelectOption.init(FicheQuery.DISCARDFILTER_NONE).textL10nObject("_ label.selection.discardfilter_none"),
        SelectOption.init(FicheQuery.DISCARDFILTER_ONLY).textL10nObject("_ label.selection.discardfilter_only")
    };
    private final HtmlPrinter htmlPrinter;
    private final Fichotheque fichotheque;
    private final BdfServer bdfServer;
    private final Lang workingLang;
    private final BdfUser bdfUser;
    private final PermissionSummary permissionSummary;
    private final FicheQuery ficheQuery;

    public SelectionFormPrinter(FicheQuery ficheQuery, BdfServer bdfServer, BdfUser bdfUser, PermissionSummary permissionSummary, HtmlPrinter htmlPrinter) {
        this.ficheQuery = ficheQuery;
        this.htmlPrinter = htmlPrinter;
        this.fichotheque = bdfServer.getFichotheque();
        this.bdfUser = bdfUser;
        this.permissionSummary = permissionSummary;
        this.workingLang = bdfUser.getWorkingLang();
        this.bdfServer = bdfServer;
    }

    public boolean printFieldsets() {
        htmlPrinter
                .DIV("selectform-Container")
                .DIV("selectform-Area selectform-Area_corpus")
                .__(printIdFieldset())
                .__(printCorpusFieldset())
                .__(printDiscardFilterFieldset())
                ._DIV()
                .DIV("selectform-Area selectform-Area_field")
                .__(printFieldConditionFieldset())
                .__(printUserFieldset())
                .__(printPeriodFieldSet())
                ._DIV()
                .DIV("selectform-Area selectform-Area_croisement")
                .__(printMotcleConditionFieldsets())
                .__(printFicheConditionFieldsets())
                ._DIV()
                ._DIV();
        return true;
    }

    public static SelectionFormPrinter init(FicheQuery ficheQuery, BdfServer bdfServer, BdfUser bdfUser, PermissionSummary permissionSummary, HtmlPrinter htmlPrinter) {
        return new SelectionFormPrinter(ficheQuery, bdfServer, bdfUser, permissionSummary, htmlPrinter);
    }

    private boolean printIdFieldset() {
        String idValue = "";
        RangeCondition idRangeCondition = ficheQuery.getIdRangeCondition();
        if (idRangeCondition != null) {
            idValue = RangeUtils.positiveRangesToString(idRangeCondition.getRanges());
        }
        htmlPrinter
                .FIELDSET("selectform-Id")
                .LEGEND()
                .__localize("_ label.selection.id")
                .__colon()
                ._LEGEND()
                .TEXTAREA(HA.name(FicheQueryParser.ID_PARAMNAME).classes("selectform-Input").rows(3))
                .__escape(idValue, true)
                ._TEXTAREA()
                ._FIELDSET();
        return true;
    }

    private boolean printFieldConditionFieldset() {
        String fieldValue = "";
        FieldContentCondition fieldContentCondition = ficheQuery.getFieldContentCondition();
        String currentOperator = ConditionsConstants.LOGICALOPERATOR_AND;
        String currentScope = FieldContentCondition.TITRE_SCOPE;
        if (fieldContentCondition != null) {
            fieldValue = ConditionsUtils.conditionToSimpleString(fieldContentCondition.getTextCondition(), false);
            currentScope = fieldContentCondition.getScope();
            currentOperator = fieldContentCondition.getTextCondition().getLogicalOperator();
        }
        htmlPrinter
                .FIELDSET("selectform-FieldCondition")
                .LEGEND()
                .__localize("_ label.selection.fieldcondition")
                .__colon()
                ._LEGEND()
                .INPUT_text(HA.name(FicheQueryParser.FIELDCONTENT_PARAMNAME).value(fieldValue).classes("selectform-Input"))
                .DIV("selectform-AndOr")
                .__(printAndOr(FicheQueryParser.FIELDCONTENT_OPERATOR_PARAMNAME, currentOperator))
                ._DIV()
                .__(addScopeRadio(FieldContentCondition.TITRE_SCOPE, "_ label.selection.scope_titre", currentScope))
                .__(addScopeRadio(FieldContentCondition.ENTETE_SCOPE, "_ label.selection.scope_entete", currentScope))
                .__(addScopeRadio(FieldContentCondition.FICHE_SCOPE, "_ label.selection.scope_fiche", currentScope))
                .__(addScopeRadio(FieldContentCondition.SELECTION_SCOPE, "_ label.selection.scope_selection", currentScope))
                ._FIELDSET();
        return true;
    }

    private boolean addScopeRadio(String scope, String key, String current) {
        String appelantFieldSelection = htmlPrinter.generateId();
        String appelantFieldSelectionAnchorId = htmlPrinter.generateId();
        String fieldSelectionAreaId = htmlPrinter.generateId();
        String fieldSelectionAreaClasses = "";
        boolean isSelectionScope = scope.equals(FieldContentCondition.SELECTION_SCOPE);
        if ((htmlPrinter.isWithJavascript()) && (!current.equals(scope))) {
            fieldSelectionAreaClasses = "hidden";
        }
        HtmlAttributes scopeAttr = HA.name(FicheQueryParser.FIELDCONTENT_SCOPE_PARAMNAME).value(scope).checked((scope.equals(current)));
        if (isSelectionScope) {
            scopeAttr.populate(Deploy.radio(fieldSelectionAreaId));
        }
        htmlPrinter
                .LABEL("selectform-Label")
                .INPUT_radio(scopeAttr)
                .__localize(key)
                ._LABEL();
        if (isSelectionScope) {
            String fieldSelection = "";
            FieldContentCondition fieldContentCondition = ficheQuery.getFieldContentCondition();
            if (fieldContentCondition != null) {
                fieldSelection = toString(fieldContentCondition.getFieldKeyList());
            }
            htmlPrinter
                    .DIV(HA.id(fieldSelectionAreaId).classes(fieldSelectionAreaClasses))
                    .INPUT_text(HA
                            .id(appelantFieldSelection)
                            .name(FicheQueryParser.FIELDCONTENT_FIELDSELECTION_PARAMNAME)
                            .value(fieldSelection)
                            .classes("selectform-Input")
                            .populate(Appelant.field().anchor(appelantFieldSelectionAnchorId)))
                    .DIV("selectform-AndOr")
                    .SPAN(HA.id(appelantFieldSelectionAnchorId))
                    .__space()
                    ._SPAN()
                    ._DIV()
                    ._DIV();
        }
        return true;
    }

    private boolean printCorpusFieldset() {
        String detailId = htmlPrinter.generateId();
        SubsetCondition corpusCondition = ficheQuery.getCorpusCondition();
        boolean all = corpusCondition.isAll();
        SubsetTree filteredSubsetTree = TreeFilterEngine.read(permissionSummary, bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_CORPUS));
        htmlPrinter
                .FIELDSET("selectform-fieldset-Corpus")
                .LEGEND()
                .__localize("_ label.selection.corpus")
                .__colon()
                ._LEGEND()
                .__(addRadios(FicheQueryParser.CORPUS_ALL_PARAMNAME, detailId, all, "_ label.selection.corpus_all"))
                .DIV(getDetailAttributes(detailId, all))
                .__(SubsetTreeOptions.init(filteredSubsetTree, bdfServer, workingLang)
                        .onlyNames(true)
                        .withKeys(false)
                        .selectedSet(corpusCondition.getSubsetKeySet())
                        .checkboxMode(HA.name(FicheQueryParser.CORPUS_SELECTION_PARAMNAME), HA.classes("selectform-Label"), HA.classes("selectform-GroupTitle"), HA.classes("selectform-GroupDiv"))
                )
                ._DIV()
                ._FIELDSET();
        return true;
    }

    private boolean printDiscardFilterFieldset() {
        String currentDiscardFilter = ficheQuery.getDiscardFilter();
        htmlPrinter
                .FIELDSET("selectform-fieldset-DiscardFilter")
                .LEGEND()
                .__localize("_ label.selection.discardfilter")
                .__colon()
                ._LEGEND()
                .SELECT(FicheQueryParser.DISCARDFILTER_PARAMNAME);
        for (SelectOption option : DISCARDFILTER_OPTIONS) {
            htmlPrinter
                    .__(option, currentDiscardFilter);
        }
        htmlPrinter
                ._SELECT()
                ._FIELDSET();
        return true;
    }

    private boolean printUserFieldset() {
        UserCondition userCondition = ficheQuery.getUserCondition();
        boolean all = (userCondition == null);
        String usersString = "";
        if (userCondition != null) {
            switch (userCondition.getFilter()) {
                case UserCondition.FILTER_ANY:
                    usersString = "*";
                    break;
                case UserCondition.FILTER_NONE:
                    usersString = "!*";
                    break;
                case UserCondition.FILTER_SOME:
                    usersString = toUsersString(userCondition);
                    break;
            }
        }
        String detailId = htmlPrinter.generateId();
        String appelantAnchorId = htmlPrinter.generateId();
        htmlPrinter
                .FIELDSET("selectform-fieldset-Users")
                .LEGEND()
                .__localize("_ label.selection.users")
                .__colon()
                ._LEGEND()
                .__(addRadios(FicheQueryParser.USERS_ALL_PARAMNAME, detailId, all, "_ label.selection.users_all"))
                .DIV(getDetailAttributes(detailId, all))
                .INPUT_text(htmlPrinter
                        .name(FicheQueryParser.USERS_SELECTION_PARAMNAME)
                        .value(usersString)
                        .classes("selectform-Input")
                        .populate(Appelant.user().sphere(bdfUser).anchor(appelantAnchorId)))
                .DIV("selectform-AndOr")
                .SPAN(HA.id(appelantAnchorId))
                .__space()
                ._SPAN()
                ._DIV()
                ._DIV()
                ._FIELDSET();
        return true;
    }

    private boolean printPeriodFieldSet() {
        String startDate = "";
        String endDate = "";
        boolean onCreationDate = true;
        boolean onModificationDate = false;
        boolean withFieldSelection = false;
        String fieldSelection = "";
        PeriodCondition periodCondition = ficheQuery.getPeriodCondition();
        if (periodCondition != null) {
            switch (periodCondition.getStartType()) {
                case PeriodCondition.ANY_TYPE:
                    startDate = PeriodCondition.ANY_CHAR;
                    break;
                case PeriodCondition.DATE_TYPE:
                    startDate = periodCondition.getStartDate().toString();
                    break;
            }
            switch (periodCondition.getEndType()) {
                case PeriodCondition.ANY_TYPE:
                    endDate = PeriodCondition.ANY_CHAR;
                    break;
                case PeriodCondition.DATE_TYPE:
                    endDate = periodCondition.getEndDate().toString();
                    break;
            }
            onCreationDate = periodCondition.isOnCreationDate();
            onModificationDate = periodCondition.isOnModificationDate();
            fieldSelection = toString(periodCondition.getFieldKeyList());
            withFieldSelection = !fieldSelection.isEmpty();
        }
        String fieldsAreaClasses = "";
        if ((htmlPrinter.isWithJavascript()) && (!withFieldSelection)) {
            fieldsAreaClasses = "hidden";
        }
        String fieldsAreaId = htmlPrinter.generateId();
        String appelantAnchorId = htmlPrinter.generateId();
        htmlPrinter
                .FIELDSET("selectform-Period")
                .LEGEND()
                .__localize("_ label.selection.period")
                .__colon()
                ._LEGEND()
                .LABEL("selectform-Label")
                .__localize("_ label.selection.period_start")
                .__colon()
                .INPUT_text(HA.name(FicheQueryParser.PERIOD_START_PARAMNAME).value(startDate).size("10").classes("selectform-Input").attr("data-selectform-role", "date"))
                ._LABEL()
                .LABEL("selectform-Label")
                .__localize("_ label.selection.period_end")
                .__colon()
                .INPUT_text(HA.name(FicheQueryParser.PERIOD_END_PARAMNAME).value(endDate).size("10").classes("selectform-Input").attr("data-selectform-role", "date"))
                ._LABEL()
                .LABEL("selectform-Label")
                .INPUT_checkbox(HA.name(FicheQueryParser.PERIOD_SCOPE_PARAMNAME).value(FicheQueryParser.CREATION_PARAMVALUE).checked(onCreationDate))
                .__space()
                .__localize("_ label.selection.period_onchrono_creation")
                ._LABEL()
                .LABEL("selectform-Label")
                .INPUT_checkbox(HA.name(FicheQueryParser.PERIOD_SCOPE_PARAMNAME).value(FicheQueryParser.MODIFICATION_PARAMVALUE).checked(onModificationDate))
                .__space()
                .__localize("_ label.selection.period_onchrono_modification")
                ._LABEL()
                .LABEL("selectform-Label")
                .INPUT_checkbox(HA.name(FicheQueryParser.PERIOD_SCOPE_PARAMNAME).value(FicheQueryParser.FIELDLIST_PARAMVALUE).checked(withFieldSelection).populate(Deploy.checkbox(fieldsAreaId)))
                .__space()
                .__localize("_ label.selection.period_fieldlist")
                ._LABEL()
                .DIV(HA.id(fieldsAreaId).classes(fieldsAreaClasses))
                .INPUT_text(htmlPrinter
                        .name(FicheQueryParser.PERIOD_FIELDLIST_PARAMNAME)
                        .value(fieldSelection)
                        .classes("selectform-Input")
                        .populate(Appelant.field().fieldtype_datation().anchor(appelantAnchorId)))
                .DIV("selectform-AndOr")
                .SPAN(HA.id(appelantAnchorId))
                .__space()
                ._SPAN()
                ._DIV()
                ._DIV()
                ._FIELDSET();
        return true;
    }

    private String toUsersString(UserCondition userCondition) {
        SubsetKey defaultSphereKey = bdfUser.getRedacteur().getSubsetKey();
        StringBuilder buf = new StringBuilder();
        for (UserCondition.Entry entry : userCondition.getEntryList()) {
            Sphere sphere = FichothequeUtils.getSphere(fichotheque, entry.getSphereName());
            if (sphere != null) {
                if (entry instanceof UserCondition.LoginEntry) {
                    Redacteur redacteur = sphere.getRedacteurByLogin(((UserCondition.LoginEntry) entry).getLogin());
                    if (redacteur != null) {
                        appendRedacteur(buf, redacteur, defaultSphereKey);
                    }
                } else if (entry instanceof UserCondition.IdEntry) {
                    Redacteur redacteur = sphere.getRedacteurById(((UserCondition.IdEntry) entry).getId());
                    if (redacteur != null) {
                        appendRedacteur(buf, redacteur, defaultSphereKey);
                    }
                } else {
                    buf.append('[');
                    buf.append(entry.getSphereName());
                    buf.append(']');
                    buf.append("; ");
                }
            }
        }
        return buf.toString();
    }

    private void appendRedacteur(StringBuilder buf, Redacteur redacteur, SubsetKey defaultSphereKey) {
        if (!defaultSphereKey.equals(redacteur.getSubsetKey())) {
            buf.append(redacteur.getBracketStyle());
        } else {
            buf.append(redacteur.getLogin());
        }
        buf.append("; ");
    }

    private String toString(List<FieldKey> fieldKeyList) {
        if (fieldKeyList.isEmpty()) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        for (FieldKey fieldKey : fieldKeyList) {
            if (buf.length() > 0) {
                buf.append(';');
            }
            buf.append(fieldKey.getKeyString());
        }
        return buf.toString();
    }

    private boolean printMotcleConditionFieldsets() {
        String logicalOperator = ConditionsConstants.LOGICALOPERATOR_AND;
        boolean hidden = true;
        MotcleCondition motcleCondition = ficheQuery.getMotcleCondition();
        HtmlPrinter hp = htmlPrinter;
        StringBuilder buf = new StringBuilder();
        PrintWriter old = hp.initPrinter(buf);
        printMotcleConditionFieldset(null, "nnn");
        hp.initPrinter(old);
        hp
                .DIV(HA.id("motcleFieldsets").attr("data-selectform-new-fieldset", buf.toString()));
        if (motcleCondition == null) {
            printMotcleConditionFieldset(null, "1");
        } else {
            int number = 1;
            for (MotcleCondition.Entry entry : motcleCondition.getEntryList()) {
                printMotcleConditionFieldset(entry.getMotcleQuery(), String.valueOf(number));
                number++;
            }
            logicalOperator = motcleCondition.getLogicalOperator();
            if (number > 2) {
                hidden = false;
            }
        }
        hp
                .__(printMotcleLogicalOperator(logicalOperator, hidden))
                ._DIV();
        return true;
    }

    private boolean printFicheConditionFieldsets() {
        String logicalOperator = ConditionsConstants.LOGICALOPERATOR_AND;
        FicheCondition ficheCondition = ficheQuery.getFicheCondition();
        String ficheConditionString = getFicheConditionString(ficheCondition);
        if (ficheCondition != null) {
            logicalOperator = ficheCondition.getLogicalOperator();
        }
        String appelantAnchorId = htmlPrinter.generateId();
        htmlPrinter
                .FIELDSET("selectform-fieldset-FicheCondition")
                .LEGEND()
                .__localize("_ label.selection.fichecondition")
                .__colon()
                ._LEGEND()
                .INPUT_text(htmlPrinter
                        .name(FicheQueryParser.FICHE_LIST_PARAMNAME)
                        .value(ficheConditionString)
                        .classes("selectform-Input")
                        .populate(Appelant.fiche().anchor(appelantAnchorId)))
                .DIV("selectform-AndOr")
                .__(printAndOr(FicheQueryParser.FICHE_LOGICALOPERATOR_PARAMNAME, logicalOperator))
                .SPAN(HA.id(appelantAnchorId))
                .__space()
                ._SPAN()
                ._DIV()
                ._FIELDSET();
        return true;
    }

    private boolean printMotcleLogicalOperator(String logicalOperator, boolean hidden) {
        String logicalOperatorClasses;
        if (hidden) {
            logicalOperatorClasses = "selectform-MotcleLogicalOperator hidden";
        } else {
            logicalOperatorClasses = "selectform-MotcleLogicalOperator";
        }
        htmlPrinter
                .DIV(HA.id("motcleLogicalOperator").classes(logicalOperatorClasses))
                .__localize("_ label.selection.motclelogicaloperator")
                .__colon()
                .BR()
                .__(printAndOr(FicheQueryParser.MOTCLE_LOGICALOPERATOR_PARAMNAME, logicalOperator))
                ._DIV();
        return true;
    }

    private boolean printMotcleConditionFieldset(MotcleQuery motcleQuery, String suffix) {
        String anchorId = "motcleInputAnchor_" + suffix;
        HtmlPrinter hp = htmlPrinter;
        String logicalOperator = ConditionsConstants.LOGICALOPERATOR_AND;
        MotcleQuery.ContentCondition contentCondition = null;
        boolean withSelection = false;
        List<Thesaurus> thesaurusList = null;
        if (motcleQuery != null) {
            SubsetCondition thesaurusCondition = motcleQuery.getThesaurusCondition();
            contentCondition = motcleQuery.getContentCondition();
            if (contentCondition != null) {
                logicalOperator = contentCondition.getTextCondition().getLogicalOperator();
            }
            if (!thesaurusCondition.isAll()) {
                withSelection = true;
                thesaurusList = new ArrayList<Thesaurus>();
                for (SubsetKey thesaurusKey : thesaurusCondition.getSubsetKeySet()) {
                    Thesaurus thesaurus = (Thesaurus) fichotheque.getSubset(thesaurusKey);
                    if (thesaurus != null) {
                        thesaurusList.add(thesaurus);
                    }
                }
                if (thesaurusList.isEmpty()) {
                    thesaurusList = null;
                    withSelection = false;
                }
            }
        }
        boolean withSatellite = true;
        String detailId = "thesaurusList_" + suffix;
        HtmlAttributes att = HA.id("parMotcle_" + suffix).classes("selectform-fieldset-MotcleCondition");
        hp
                .FIELDSET(att)
                .LEGEND()
                .__localize("_ label.selection.motclecondition", suffix)
                .__colon()
                ._LEGEND();
        hp
                .INPUT_hidden(FicheQueryParser.MOTCLE_SATELLITE_PREFIX + suffix, (withSatellite) ? "1" : "0");
        String value = (contentCondition != null) ? ConditionsUtils.conditionToString(contentCondition.getTextCondition()) : "";
        Appelant appelantData = Appelant.motcle().anchor(anchorId);
        if (thesaurusList != null) {
            for (Thesaurus thesaurus : thesaurusList) {
                appelantData.subsets(thesaurus.getSubsetKey());
            }
        }
        hp
                .INPUT_text(HA.id("motcleInput_" + suffix)
                        .name(FicheQueryParser.MOTCLE_INPUT_PREFIX + suffix)
                        .value(value)
                        .classes("selectform-Input")
                        .populate(appelantData))
                .DIV("selectform-AndOr")
                .__(printAndOr(FicheQueryParser.MOTCLE_CONTENTOPERATOR_PREFIX + suffix, logicalOperator))
                .SPAN(HA.id(anchorId))
                .__space()
                ._SPAN()
                ._DIV()
                .SPAN("selectform-Label")
                .__localize("_ label.selection.thesaurus")
                ._SPAN()
                .__(addRadios(FicheQueryParser.MOTCLE_ALL_PREFIX + suffix, detailId, !withSelection, "_ label.selection.motclecondition_all"))
                .DIV(getDetailAttributes(detailId, !withSelection));
        HashSet<SubsetKey> disabledSet = new HashSet<SubsetKey>();
        if (withSelection) {
            HtmlAttributes checkAttr = HA.name(FicheQueryParser.MOTCLE_SELECTION_PREFIX + suffix).checked(true).attr("data-selectform-role", "thesaurus-checkbox");
            for (Thesaurus thesaurus : thesaurusList) {
                SubsetKey thesaurusKey = thesaurus.getSubsetKey();
                checkAttr.value(thesaurusKey.getSubsetName());
                hp
                        .LABEL("selectform-Label")
                        .INPUT_checkbox(checkAttr)
                        .__escape(FichothequeUtils.getTitle(thesaurus, workingLang))
                        ._LABEL();
                disabledSet.add(thesaurusKey);
            }
        }
        HtmlAttributes selectAttributes = HA.id("thesaurusSelect_" + suffix).classes("selectform-ThesaurusSelect").name(FicheQueryParser.MOTCLE_SELECTION_PREFIX + suffix).multiple(true).size("10").attr("data-selectform-role", "thesaurus-select");
        hp
                .SELECT(selectAttributes)
                .__(SubsetTreeOptions.init(bdfServer.getTreeManager().getSubsetTree(SubsetKey.CATEGORY_THESAURUS), bdfServer, workingLang)
                        .multiple(true)
                        .onlyNames(true)
                        .withKeys(false)
                        .disabledSet(disabledSet)
                )
                ._SELECT()
                ._DIV()
                ._FIELDSET();
        return true;
    }

    private boolean addRadios(String name, String detailId, boolean all, String allLocKey) {
        htmlPrinter
                .LABEL("selectform-Label")
                .INPUT_radio(HA.name(name).value("1").checked(all))
                .__localize(allLocKey)
                ._LABEL()
                .LABEL("selectform-Label")
                .INPUT_radio(HA.name(name).value("0").checked(!all).populate(Deploy.radio(detailId)))
                .__localize("_ label.selection.selectlist")
                ._LABEL();
        return true;
    }

    private HtmlAttributes getDetailAttributes(String detailId, boolean empty) {
        boolean hidden = ((htmlPrinter.isWithJavascript()) && (empty));
        return HA.id(detailId).classes("selectform-Detail").addClass(hidden, "hidden");
    }

    private boolean printAndOr(String name, String logicalOperator) {
        boolean isAnd = logicalOperator.equals(ConditionsConstants.LOGICALOPERATOR_AND);
        htmlPrinter
                .LABEL()
                .INPUT_radio(HA.name(name).value("and").checked(isAnd))
                .__localize("_ label.global.and")
                ._LABEL()
                .__space()
                .LABEL()
                .INPUT_radio(HA.name(name).value("or").checked(!isAnd))
                .__localize("_ label.global.or")
                ._LABEL();
        return true;
    }

    private String getFicheConditionString(FicheCondition ficheCondition) {
        if (ficheCondition == null) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        for (FicheCondition.Entry entry : ficheCondition.getEntryList()) {
            FicheQuery entryFicheQuery = entry.getFicheQuery();
            RangeCondition idRangeCondition = entryFicheQuery.getIdRangeCondition();
            Set<SubsetKey> corpusKeySet = entryFicheQuery.getCorpusCondition().getSubsetKeySet();
            if ((!corpusKeySet.isEmpty()) && (idRangeCondition != null)) {
                buf.append(corpusKeySet.iterator().next().getSubsetName());
                buf.append('/');
                if (idRangeCondition.isExclude()) {
                    buf.append("!");
                }
                buf.append(RangeUtils.positiveRangesToString(idRangeCondition.getRanges(), ","));
                appendCroisementCondition(buf, entry.getCroisementCondition());
                buf.append("; ");
            }
        }
        return buf.toString();
    }

    private void appendCroisementCondition(StringBuilder buf, CroisementCondition croisementCondition) {
        if (croisementCondition == null) {
            return;
        }
        List<String> modeList = croisementCondition.getLienModeList();
        buf.append("|");
        boolean next = false;
        for (String mode : modeList) {
            if (next) {
                buf.append(',');
            } else {
                next = true;
            }
            if (mode.isEmpty()) {
                buf.append("_default");
            } else {
                buf.append(mode);
            }
        }
        RangeCondition rangeCondition = croisementCondition.getWeightRangeCondition();
        if (rangeCondition != null) {
            buf.append("|");
            if (rangeCondition.isExclude()) {
                buf.append("!");
            }
            buf.append(RangeUtils.positiveRangesToString(rangeCondition.getRanges(), ","));
        }
    }

}
