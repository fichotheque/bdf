/* BdfServer_Html - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import fr.exemole.bdfserver.tools.storage.StorageUtils;
import java.text.ParseException;
import net.mapeadores.util.jslib.JsLib;
import net.mapeadores.util.jslib.JsLibBuilder;
import net.mapeadores.util.jslib.TemplateFamily;
import net.mapeadores.util.jslib.TemplateFamilyBuilder;
import net.mapeadores.util.jslib.ThirdLibBuilder;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class BdfJsLibBuilder {

    private final static TemplateFamily BDF_TEMPLATEFAMILY = TemplateFamilyBuilder.init("bdf").toTemplateFamily();
    private final JsLibBuilder jsLibBuilder = new JsLibBuilder();
    private String currentExtensionName = null;
    private String currentAppName = null;
    private boolean templateAdded = false;


    public BdfJsLibBuilder() {

    }

    public BdfJsLibBuilder setCurrentExtensionName(String currentExtensionName) {
        this.currentExtensionName = currentExtensionName;
        return this;
    }

    public BdfJsLibBuilder setCurrentAppName(String currentAppName) {
        this.currentAppName = currentAppName;
        return this;
    }

    public BdfJsLibBuilder addDependency(JsLib dependency) {
        jsLibBuilder.addDependency(dependency);
        return this;
    }

    public BdfJsLibBuilder addThirdLib(String name, String... extensionNames) {
        ThirdLibBuilder builder = new ThirdLibBuilder(name);
        if (extensionNames != null) {
            for (String extensionName : extensionNames) {
                builder.addExtension(extensionName);
            }
        }
        jsLibBuilder.addThirdLib(builder.toThirdLib());
        return this;
    }

    public BdfJsLibBuilder addJs(String jsPath) {
        try {
            RelativePath relativePath = RelativePath.parse("js/" + jsPath);
            jsLibBuilder.addJsScript(relativePath);
        } catch (ParseException pe) {

        }
        return this;
    }

    public BdfJsLibBuilder addExtensionJs(String jsPath) {
        if (currentExtensionName == null) {
            throw new IllegalStateException("currentExtensionName is null");
        }
        try {
            RelativePath relativePath = StorageUtils.parseExtensionResourcePath(currentExtensionName, "js/" + jsPath);
            jsLibBuilder.addJsScript(relativePath);
        } catch (ParseException pe) {

        }
        return this;
    }

    public BdfJsLibBuilder addAppJs(String jsPath) {
        if (currentAppName == null) {
            throw new IllegalStateException("currentExtensionName is null");
        }
        try {
            RelativePath relativePath = RelativePath.parse("apps/" + currentAppName + "/js/" + jsPath);
            jsLibBuilder.addJsScript(relativePath);
        } catch (ParseException pe) {

        }
        return this;
    }


    public BdfJsLibBuilder addTemplateFamily(String family) {
        testTemplateAdd();
        jsLibBuilder.addTemplateFamily(TemplateFamilyBuilder.init(family).toTemplateFamily());
        return this;
    }

    public BdfJsLibBuilder addExtensionTemplateFamily(String family) {
        if (currentExtensionName == null) {
            throw new IllegalStateException("currentExtensionName is null");
        }
        testTemplateAdd();
        jsLibBuilder.addTemplateFamily(TemplateFamilyBuilder.init(family)
                .setOriginObject(new TemplateOrigin(TemplateOrigin.EXTENSION_TYPE, currentExtensionName))
                .toTemplateFamily());
        return this;
    }

    public BdfJsLibBuilder addAppTemplateFamily(String family) {
        if (currentAppName == null) {
            throw new IllegalStateException("currentAppName is null");
        }
        testTemplateAdd();
        jsLibBuilder.addTemplateFamily(TemplateFamilyBuilder.init(family)
                .setOriginObject(new TemplateOrigin(TemplateOrigin.APP_TYPE, currentAppName))
                .toTemplateFamily());
        return this;
    }

    private void testTemplateAdd() {
        if (!templateAdded) {
            templateAdded = true;
            jsLibBuilder.addTemplateFamily(BDF_TEMPLATEFAMILY);
        }
    }


    public JsLib toJsLib() {
        return jsLibBuilder.toJsLib();
    }

    public static BdfJsLibBuilder init() {
        return new BdfJsLibBuilder();
    }


}
