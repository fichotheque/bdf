/* BdfServer_Html - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class OpenDocumentJsLibs {

    public final static JsLib OPENDOCUMENT = BdfJsLibBuilder.init()
            .addDependency(UtilsJsLibs.CURRENCY)
            .addDependency(UtilsJsLibs.XMLWRITER)
            .addJs("opendocument/OpenDocument.js")
            .addJs("opendocument/OpenDocument.Elements.js")
            .addJs("opendocument/OpenDocument.CellCounter.js")
            .addJs("opendocument/OpenDocument.XmlWriter.js")
            .addJs("opendocument/OpenDocument.Style.js")
            .addJs("opendocument/OpenDocument.StyleManager.js")
            .addJs("opendocument/OpenDocument.OdsConverter.js")
            .toJsLib();

    private OpenDocumentJsLibs() {

    }

}
