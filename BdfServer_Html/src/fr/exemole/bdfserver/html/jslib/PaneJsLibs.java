/* BdfServer_Html - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class PaneJsLibs {

    public final static JsLib PANE = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.AJAX)
            .addDependency(BdfJsLibs.DEPLOY)
            .addJs("pane/Pane.js")
            .addThirdLib(JsLibCatalog.JQUERY, "form")
            .addTemplateFamily("pane")
            .toJsLib();

    private PaneJsLibs() {

    }

}
