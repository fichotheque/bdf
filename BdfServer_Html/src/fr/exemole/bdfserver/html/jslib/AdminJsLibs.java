/*  BdfServer_Html - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class AdminJsLibs {

    public final static JsLib ACCESS = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.COLLECTIONS)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("access/Access.js")
            .addJs("access/Access.List.js")
            .addJs("access/Access.Form.js")
            .addJs("access/Access.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "attributes")
            .addTemplateFamily("access")
            .toJsLib();
    public final static JsLib BALAYAGE = BdfJsLibBuilder.init()
            .addDependency(UtilsJsLibs.XMLWRITER)
            .addDependency(BdfJsLibs.COLLECTIONS)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("balayage/Balayage.js")
            .addJs("balayage/Balayage.List.js")
            .addJs("balayage/Balayage.MetadataForm.js")
            .addJs("balayage/Balayage.ContentForm.js")
            .addJs("balayage/Balayage.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "attributes")
            .addTemplateFamily("balayage")
            .toJsLib();
    public final static JsLib CENTRALLOGIN = BdfJsLibBuilder.init()
            .addJs("centrallogin/CentralLogin.js")
            .addThirdLib(JsLibCatalog.JQUERY, "form")
            .addTemplateFamily("centrallogin")
            .toJsLib();
    public final static JsLib DIAGNOSTIC = BdfJsLibBuilder.init()
            .addDependency(PaneJsLibs.PANE)
            .addJs("diagnostic/Diagnostic.js")
            .addJs("diagnostic/Diagnostic.List.js")
            .addJs("diagnostic/Diagnostic.UrlScan.js")
            .addTemplateFamily("diagnostic")
            .toJsLib();
    public final static JsLib RESOURCE = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("resource/Resource.js")
            .addJs("resource/Resource.Form.js")
            .addJs("resource/Resource.List.js")
            .addJs("resource/Resource.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "css", "xml", "javascript", "properties")
            .addTemplateFamily("resource")
            .toJsLib();
    public final static JsLib ROLE = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.SUBSETTREES)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("role/Role.js")
            .addJs("role/Role.List.js")
            .addJs("role/Role.Form.js")
            .addJs("role/Role.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "attributes")
            .addTemplateFamily("role")
            .toJsLib();
    public final static JsLib SCRUTARIEXPORT = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.SUBSETTREES)
            .addDependency(BdfJsLibs.COLLECTIONS)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("scrutariexport/ScrutariExport.js")
            .addJs("scrutariexport/ScrutariExport.List.js")
            .addJs("scrutariexport/ScrutariExport.Form.js")
            .addJs("scrutariexport/ScrutariExport.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "tableexport", "attributes", "xml")
            .addTemplateFamily("memento")
            .addTemplateFamily("scrutariexport")
            .toJsLib();
    public final static JsLib SELECTIONDEF = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.COLLECTIONS)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("selectiondef/SelectionDef.js")
            .addJs("selectiondef/SelectionDef.List.js")
            .addJs("selectiondef/SelectionDef.Form.js")
            .addJs("selectiondef/SelectionDef.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "attributes", "xml")
            .addTemplateFamily("selectiondef")
            .toJsLib();
    public final static JsLib SQLEXPORT = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.COLLECTIONS)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("sqlexport/SqlExport.js")
            .addJs("sqlexport/SqlExport.List.js")
            .addJs("sqlexport/SqlExport.Form.js")
            .addJs("sqlexport/SqlExport.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "attributes")
            .addTemplateFamily("sqlexport")
            .toJsLib();
    public final static JsLib TABLEEXPORT = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.COLLECTIONS)
            .addDependency(BdfJsLibs.SUBSETTREES)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("tableexport/TableExport.js")
            .addJs("tableexport/TableExport.List.js")
            .addJs("tableexport/TableExport.ContentForm.js")
            .addJs("tableexport/TableExport.MetadataForm.js")
            .addJs("tableexport/TableExport.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "tableexport", "attributes")
            .addTemplateFamily("memento")
            .addTemplateFamily("tableexport")
            .toJsLib();
    public final static JsLib TRANSFORMATION = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.SUBSETTREES)
            .addDependency(BdfJsLibs.COLLECTIONS)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addDependency(PaneJsLibs.PANE)
            .addJs("transformation/Transformation.js")
            .addJs("transformation/Transformation.List.js")
            .addJs("transformation/Transformation.ContentForm.js")
            .addJs("transformation/Transformation.MetadataForm.js")
            .addJs("transformation/Transformation.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "css", "xml", "javascript", "properties", "attributes")
            .addTemplateFamily("transformation")
            .toJsLib();


    private AdminJsLibs() {
    }

}
