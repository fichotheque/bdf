/* BdfServer_Html - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public class DocumentJsLibs {

    public final static JsLib CHANGE = BdfJsLibBuilder.init()
            .addJs("document/AddendaDoc.js")
            .addJs("document/AddendaDoc.Change.js")
            .addTemplateFamily("document")
            .toJsLib();
    public final static JsLib UPLOAD = BdfJsLibBuilder.init()
            .addJs("document/AddendaDoc.js")
            .addJs("document/AddendaDoc.Upload.js")
            .addTemplateFamily("document")
            .toJsLib();
    public final static JsLib UPLOADCONFIRM = BdfJsLibBuilder.init()
            .addJs("document/AddendaDoc.js")
            .addJs("document/AddendaDoc.UploadConfirm.js")
            .addTemplateFamily("document")
            .toJsLib();


    private DocumentJsLibs() {
    }

}
