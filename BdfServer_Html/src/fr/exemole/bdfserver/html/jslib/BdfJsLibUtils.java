/* BdfServer_Html - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.io.ResourceFolder;
import net.mapeadores.util.io.ResourceStorage;
import net.mapeadores.util.io.ResourceStorages;
import fr.exemole.bdfserver.tools.apps.AppConf;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.jslib.JsLib;
import net.mapeadores.util.jslib.TemplateFamily;
import net.mapeadores.util.jslib.TemplateFamilyBuilder;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public final class BdfJsLibUtils {

    private BdfJsLibUtils() {

    }

    public static JsLib getAppJsLib(ResourceStorages resourceStorages, AppConf appConf) {
        Set<String> pathSet = new HashSet<String>();
        Set<String> familySet = new HashSet<String>();
        String appName = appConf.getAppName();
        BdfJsLibBuilder jsLibBuilder = BdfJsLibBuilder.init()
                .setCurrentAppName(appName);
        for (String jsPath : appConf.getArray(AppConf.CORE_JSORDER)) {
            if (!pathSet.contains(jsPath)) {
                jsLibBuilder.addAppJs(jsPath);
                pathSet.add(jsPath);
            }
        }
        RelativePath jsFolderPath = StorageUtils.buildAppResourcePath(appName, "js");
        RelativePath templatesFolderPath = StorageUtils.buildAppResourcePath(appName, "templates");
        RelativePath libTemplatesFolderPath = StorageUtils.buildAppResourcePath(appName, "lib/templates");
        for (ResourceStorage resourceStorage : resourceStorages) {
            ResourceFolder jsFolder = resourceStorage.getResourceFolder(jsFolderPath);
            if (jsFolder != null) {
                addJsPath(jsLibBuilder, pathSet, jsFolder, "");
            }
            ResourceFolder templatesFolder = resourceStorage.getResourceFolder(templatesFolderPath);
            if (templatesFolder != null) {
                if (!familySet.contains("")) {
                    jsLibBuilder.addAppTemplateFamily("");
                    familySet.add("");
                }
            }
            ResourceFolder libTemplatesFolder = resourceStorage.getResourceFolder(libTemplatesFolderPath);
            if (libTemplatesFolder != null) {
                for (ResourceFolder subFolder : libTemplatesFolder.getSubfolderList()) {
                    String familyName = subFolder.getName();
                    if (!familySet.contains(familyName)) {
                        jsLibBuilder.addAppTemplateFamily(familyName);
                        familySet.add(familyName);
                    }
                }
            }
        }
        return jsLibBuilder.toJsLib();
    }

    public static TemplateFamily getAppTemplateFamily(String familyName, String appName) {
        return TemplateFamilyBuilder.init(familyName)
                .setOriginObject(new TemplateOrigin(TemplateOrigin.APP_TYPE, appName))
                .toTemplateFamily();
    }

    public static TemplateFamily getExtensionTemplateFamily(String familyName, String extensionName) {
        return TemplateFamilyBuilder.init(familyName)
                .setOriginObject(new TemplateOrigin(TemplateOrigin.EXTENSION_TYPE, extensionName))
                .toTemplateFamily();
    }

    private static void addJsPath(BdfJsLibBuilder jsLibBuilder, Set<String> pathSet, ResourceFolder folder, String folderPath) {
        for (ResourceFolder subfolder : folder.getSubfolderList()) {
            addJsPath(jsLibBuilder, pathSet, subfolder, folderPath + subfolder.getName() + "/");
        }
        for (String resourceName : folder.getResourceNameList()) {
            if (resourceName.endsWith(".js")) {
                String jsPath = folderPath + resourceName;
                if (!pathSet.contains(jsPath)) {
                    jsLibBuilder.addAppJs(folderPath + resourceName);
                }
            }
        }
    }

}
