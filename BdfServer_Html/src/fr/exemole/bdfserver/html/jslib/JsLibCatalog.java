/* BdfServer_Html - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public class JsLibCatalog {

    public final static String JQUERY = "jquery";
    public final static String CODEMIRROR = "codemirror";
    public final static String LEAFLET = "leaflet";
    public final static String MOUSETRAP = "mousetrap";
    public final static String MERGELY = "mergely";
    public final static String JSRENDER = "jsrender";
    public final static JsLibCatalog CORE;
    private final Map<String, JsLib> map;

    static {
        Map<String, JsLib> map = new HashMap<String, JsLib>();
        map.put("balayage", AdminJsLibs.BALAYAGE);
        map.put("bdf-ajax", BdfJsLibs.AJAX);
        map.put("bdf-api", BdfJsLibs.API);
        map.put("bdf-appelant", BdfJsLibs.APPELANT);
        map.put("bdf-codemirrormode", BdfJsLibs.CODEMIRRORMODE);
        map.put("bdf-commandtest", BdfJsLibs.COMMANDTEST);
        map.put("bdf-commandveil", BdfJsLibs.COMMANDVEIL);
        map.put("bdf-commandwait", BdfJsLibs.COMMANDWAIT);
        map.put("bdf-conditional", BdfJsLibs.CONDITIONAL);
        map.put("bdf-deploy", BdfJsLibs.DEPLOY);
        map.put("bdf-ficheblock", BdfJsLibs.FICHEBLOCK);
        map.put("bdf-formstorage", BdfJsLibs.FORMSTORAGE);
        map.put("bdf-multi", BdfJsLibs.MULTI);
        map.put("bdf-overlay", BdfJsLibs.OVERLAY);
        map.put("bdf-shortcut", BdfJsLibs.SHORTCUT);
        map.put("bdf-subsetchange", BdfJsLibs.SUBSETCHANGE);
        map.put("bdf-subsettrees", BdfJsLibs.SUBSETTREES);
        map.put("currency", UtilsJsLibs.CURRENCY);
        map.put("dashboard", MiscJsLibs.DASHBOARD);
        map.put("document-change", DocumentJsLibs.CHANGE);
        map.put("document-upload", DocumentJsLibs.UPLOAD);
        map.put("document-uploadconfirm", DocumentJsLibs.UPLOADCONFIRM);
        map.put("fapi", UtilsJsLibs.FAPI);
        map.put("fiche-form", FicheJsLibs.FORM);
        map.put("fiche-zoomedit", FicheJsLibs.ZOOMEDIT);
        map.put("ficheframe", FicheJsLibs.FICHEFRAME);
        map.put("ficheframe-init", FicheJsLibs.FICHEFRAME_INIT);
        map.put("fql", UtilsJsLibs.FQL);
        map.put("fqleditor", MiscJsLibs.FQLEDITOR);
        map.put("geo-pioche", GeoJsLibs.PIOCHE);
        map.put("geo-fiches", GeoJsLibs.FICHES);
        map.put("history", MiscJsLibs.HISTORY);
        map.put("illustration-change", IllustrationJsLibs.CHANGE);
        map.put("illustration-upload", IllustrationJsLibs.UPLOAD);
        map.put("illustration-uploadconfirm", IllustrationJsLibs.UPLOADCONFIRM);
        map.put("importation", MiscJsLibs.IMPORTATION);
        map.put("index", MultiJsLibs.INDEX);
        map.put("index-client", MultiJsLibs.INDEX_CLIENT);
        map.put("main", MiscJsLibs.MAIN);
        map.put("memento", MiscJsLibs.MEMENTO);
        map.put("menu", MiscJsLibs.MENU);
        map.put("multiadmin", MultiJsLibs.ADMIN);
        map.put("navigation", UtilsJsLibs.NAVIGATION);
        map.put("opendocument", OpenDocumentJsLibs.OPENDOCUMENT);
        map.put("pane", PaneJsLibs.PANE);
        map.put("pioche", MiscJsLibs.PIOCHE);
        map.put("plantuml", MiscJsLibs.PLANTUML);
        map.put("role", AdminJsLibs.ROLE);
        map.put("resource", AdminJsLibs.RESOURCE);
        map.put("restore", MiscJsLibs.RESTORE);
        map.put("scrutariexport", AdminJsLibs.SCRUTARIEXPORT);
        map.put("selectform", MiscJsLibs.SELECTFORM);
        map.put("selectionindexation", MiscJsLibs.SELECTIONINDEXATION);
        map.put("sqlexport", AdminJsLibs.SQLEXPORT);
        map.put("tabledisplay", MiscJsLibs.TABLEDISPLAY);
        map.put("tableexport", AdminJsLibs.TABLEEXPORT);
        map.put("transformation", AdminJsLibs.TRANSFORMATION);
        map.put("xmlwriter", UtilsJsLibs.XMLWRITER);
        CORE = new JsLibCatalog(map);
    }

    private JsLibCatalog(Map<String, JsLib> map) {
        this.map = map;
    }

    public JsLib getJsLib(String name) {
        name = checkAlias(name);
        return map.get(name);
    }

    private static String checkAlias(String name) {
        switch (name) {
            case "ficheql":
                return "fql";
            case "bdfapi":
                return "fapi";
            default:
                return name;
        }
    }

}
