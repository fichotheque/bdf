/* BdfServer_Html - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class IllustrationJsLibs {

    public final static JsLib CHANGE = BdfJsLibBuilder.init()
            .addJs("illustration/Illustration.js")
            .addJs("illustration/Illustration.Change.js")
            .addThirdLib(JsLibCatalog.JQUERY, "form", "imgareaselect")
            .addTemplateFamily("illustration")
            .toJsLib();
    public final static JsLib UPLOAD = BdfJsLibBuilder.init()
            .addJs("illustration/Illustration.js")
            .addJs("illustration/Illustration.Upload.js")
            .addTemplateFamily("illustration")
            .toJsLib();
    public final static JsLib UPLOADCONFIRM = BdfJsLibBuilder.init()
            .addJs("illustration/Illustration.js")
            .addJs("illustration/Illustration.UploadConfirm.js")
            .addTemplateFamily("illustration")
            .toJsLib();

    private IllustrationJsLibs() {
    }

}
