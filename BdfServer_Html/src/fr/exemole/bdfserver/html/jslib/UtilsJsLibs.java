/* BdfServer_Html - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class UtilsJsLibs {

    public final static JsLib FAPI;
    public final static JsLib FAPI_CORE;
    public final static JsLib CURRENCY;
    public final static JsLib FQL;
    public final static JsLib NAVIGATION;
    public final static JsLib XMLWRITER;

    static {
        FQL = BdfJsLibBuilder.init()
                .addJs("fql/Fql.js")
                .addJs("fql/Fql.Condition.js")
                .addJs("fql/Fql.Condition.Croisement.js")
                .addJs("fql/Fql.Condition.Fiche.js")
                .addJs("fql/Fql.Condition.FieldContent.js")
                .addJs("fql/Fql.Condition.Motcle.js")
                .addJs("fql/Fql.Condition.MotcleContent.js")
                .addJs("fql/Fql.Condition.Period.js")
                .addJs("fql/Fql.Condition.Range.js")
                .addJs("fql/Fql.Condition.Status.js")
                .addJs("fql/Fql.Condition.Subset.js")
                .addJs("fql/Fql.Condition.User.js")
                .addJs("fql/Fql.FicheQuery.js")
                .addJs("fql/Fql.MotcleQuery.js")
                .toJsLib();
        CURRENCY = BdfJsLibBuilder.init()
                .addJs("currency/Currency.js")
                .toJsLib();
        NAVIGATION = BdfJsLibBuilder.init()
                .addJs("navigation/Navigation.js")
                .toJsLib();
        XMLWRITER = BdfJsLibBuilder.init()
                .addJs("xmlwriter/XmlWriter.js")
                .toJsLib();
        FAPI = BdfJsLibBuilder.init()
                .addDependency(FQL)
                .addJs("fapi/Fapi.js")
                .addJs("fapi/Fapi.EndPoint.js")
                .addJs("fapi/Fapi.Ajax.js")
                .addJs("fapi/Fapi.Cache.js")
                .addJs("fapi/Fapi.Cache.Thesaurus.js")
                .addJs("fapi/Fapi.Cache.Fiches.js")
                .toJsLib();
        FAPI_CORE = BdfJsLibBuilder.init()
                .addDependency(FQL)
                .addJs("fapi/Fapi.js")
                .addJs("fapi/Fapi.EndPoint.js")
                .addJs("fapi/Fapi.Ajax.js")
                .toJsLib();
    }


    private UtilsJsLibs() {

    }

}
