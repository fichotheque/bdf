/* BdfServer_Html - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;


/**
 *
 * @author Vincent Calame
 */
public class TemplateOrigin {

    public final static short EXTENSION_TYPE = 1;
    public final static short APP_TYPE = 2;
    private final short type;
    private final String name;

    public TemplateOrigin(short type, String name) {
        this.type = type;
        this.name = name;
    }

    public short getType() {
        return type;
    }

    public String getName() {
        return name;
    }

}
