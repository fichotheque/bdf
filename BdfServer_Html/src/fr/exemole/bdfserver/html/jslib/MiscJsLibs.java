/* BdfServer_Html - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class MiscJsLibs {

    public final static JsLib DASHBOARD = BdfJsLibBuilder.init()
            .addDependency(UtilsJsLibs.FQL)
            .addDependency(UtilsJsLibs.FAPI)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(UtilsJsLibs.NAVIGATION)
            .addJs("dashboard/Dashboard.js")
            .addTemplateFamily("dashboard")
            .toJsLib();
    public final static JsLib HISTORY = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.AJAX)
            .addJs("history/History.js")
            .addJs("history/History.Fiche.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR)
            .addThirdLib(JsLibCatalog.MERGELY)
            .addTemplateFamily("history")
            .toJsLib();
    public final static JsLib IMPORTATION = BdfJsLibBuilder.init()
            .addJs("importation/Importation.js")
            .addJs("importation/Importation.Overlay.js")
            .addThirdLib(JsLibCatalog.JQUERY, "form")
            .addTemplateFamily("pane")
            .addTemplateFamily("importation")
            .toJsLib();
    public final static JsLib MAIN = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.API)
            .addDependency(FicheJsLibs.FICHEFRAME)
            .addJs("main/Main.js")
            .addTemplateFamily("main")
            .toJsLib();
    public final static JsLib MEMENTO = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.AJAX)
            .addJs("memento/Memento.js")
            .addTemplateFamily("memento")
            .toJsLib();
    public final static JsLib MENU = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.API)
            .addDependency(BdfJsLibs.AJAX)
            .addJs("menu/Menu.js")
            .addTemplateFamily("menu")
            .toJsLib();
    public final static JsLib PIOCHE = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.SHORTCUT)
            .addJs("pioche/Pioche.js")
            .addThirdLib(JsLibCatalog.JQUERY, "form")
            .addTemplateFamily("pioche")
            .toJsLib();
    public final static JsLib PLANTUML = BdfJsLibBuilder.init()
            .addJs("plantuml/PlantUml.js")
            .toJsLib();
    public final static JsLib FQLEDITOR = BdfJsLibBuilder.init()
            .addDependency(UtilsJsLibs.FQL)
            .addDependency(BdfJsLibs.SUBSETTREES)
            .addDependency(BdfJsLibs.DEPLOY)
            .addDependency(BdfJsLibs.APPELANT)
            .addJs("fqleditor/FqlEditor.js")
            .addJs("fqleditor/FqlEditor.Model.js")
            .addJs("fqleditor/FqlEditor.Overlay.js")
            .addTemplateFamily("fqleditor")
            .toJsLib();
    public final static JsLib RESTORE = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.AJAX)
            .addJs("restore/Restore.js")
            .addTemplateFamily("restore")
            .toJsLib();
    public final static JsLib SELECTFORM = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.DEPLOY)
            .addDependency(BdfJsLibs.APPELANT)
            .addJs("selectform/SelectForm.js")
            .addTemplateFamily("selectform")
            .toJsLib();
    public final static JsLib SELECTIONINDEXATION = BdfJsLibBuilder.init()
            .addJs("selectionindexation/SelectionIndexation.js")
            .addTemplateFamily("selectionindexation")
            .toJsLib();
    public final static JsLib TABLEDISPLAY = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.AJAX)
            .addJs("tabledisplay/TableDisplay.js")
            .addThirdLib(JsLibCatalog.JQUERY, "tablesorter-blue")
            .addTemplateFamily("tabledisplay")
            .toJsLib();


    private MiscJsLibs() {
    }

}
