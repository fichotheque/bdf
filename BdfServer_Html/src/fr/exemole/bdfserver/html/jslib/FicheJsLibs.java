/* BdfServer_Html - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class FicheJsLibs {

    public final static JsLib FORM = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.API)
            .addDependency(BdfJsLibs.DEPLOY)
            .addDependency(BdfJsLibs.AJAX)
            .addDependency(BdfJsLibs.FICHEBLOCK)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addJs("ficheform/Ficheform.js")
            .addJs("ficheform/Ficheform.ThesaurusInclude.js")
            .addJs("ficheform/Ficheform.CorpusInclude.js")
            .addJs("ficheform/Ficheform.CorpusInclude.Manager.js")
            .addJs("ficheform/Ficheform.Section.js")
            .addJs("ficheform/Ficheform.Section.Overlay.js")
            .addJs("ficheform/Ficheform.AlbumInclude.js")
            .addJs("ficheform/Ficheform.AddendaInclude.js")
            .addJs("ficheform/Ficheform.AddendaInclude.Manager.js")
            .addJs("ficheform/Ficheform.Toc.js")
            .addJs("ficheform/Ficheform.Init.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "ficheblock")
            .addTemplateFamily("ficheform")
            .toJsLib();
    public final static JsLib ZOOMEDIT = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.DEPLOY)
            .addDependency(BdfJsLibs.FICHEBLOCK)
            .addDependency(BdfJsLibs.SHORTCUT)
            .addJs("zoomedit/Zoomedit.js")
            .addJs("ficheform/Ficheform.js")
            .addJs("ficheform/Ficheform.Section.js")
            .addJs("ficheform/Ficheform.Section.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "ficheblock")
            .addTemplateFamily("zoomedit")
            .addTemplateFamily("ficheform")
            .toJsLib();
    public final static JsLib SECTIONPREVIEW = BdfJsLibBuilder.init()
            .addJs("extraction/Extraction.js")
            .addJs("extraction/Extraction.CodeMirror.js")
            .addJs("preview/Preview.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "css", "javascript", "properties", "clike", "htmlmixed", "tableexport", "ficheblock", "attributes", "uicomponents", "subsettree")
            .addTemplateFamily("preview")
            .toJsLib();
    public final static JsLib FICHEFRAME = BdfJsLibBuilder.init()
            .addDependency(UtilsJsLibs.FAPI_CORE)
            .addJs("ficheframe/Ficheframe.js")
            .addJs("ficheframe/Ficheframe.Info.js")
            .addJs("ficheframe/Ficheframe.Position.js")
            .addJs("ficheframe/Ficheframe.Positions.js")
            .addJs("ficheframe/Ficheframe.GotoManager.js")
            .addTemplateFamily("ficheframe")
            .toJsLib();
    public final static JsLib FICHEFRAME_INIT = BdfJsLibBuilder.init()
            .addDependency(FICHEFRAME)
            .addJs("ficheframe/Ficheframe.Init.js")
            .toJsLib();

    private FicheJsLibs() {
    }

}
