/* BdfServer_Html - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public class MultiJsLibs {

    public final static JsLib ADMIN = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.MULTI)
            .addDependency(PaneJsLibs.PANE)
            .addJs("multiadmin/MultiAdmin.js")
            .addJs("multiadmin/MultiAdmin.List.js")
            .addJs("multiadmin/MultiAdmin.Metadata.js")
            .addJs("multiadmin/MultiAdmin.Overlay.js")
            .addJs("multiadmin/MultiAdmin.PersonManager.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "attributes")
            .addTemplateFamily("multiadmin")
            .toJsLib();
    public final static JsLib CENTRALSPHERE = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.MULTI)
            .addDependency(PaneJsLibs.PANE)
            .addJs("centralsphere/CentralSphere.js")
            .addJs("centralsphere/CentralSphere.Ajax.js")
            .addJs("centralsphere/CentralSphere.List.js")
            .addJs("centralsphere/CentralSphere.Metadata.js")
            .addJs("centralsphere/CentralSphere.Stats.js")
            .addJs("centralsphere/CentralSphere.User.js")
            .addJs("centralsphere/CentralSphere.Overlay.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "attributes")
            .addThirdLib(JsLibCatalog.JQUERY, "tablesorter-blue")
            .addTemplateFamily("centralsphere")
            .toJsLib();
    public final static JsLib INDEX = BdfJsLibBuilder.init()
            .addDependency(BdfJsLibs.DEPLOY)
            .addDependency(BdfJsLibs.AJAX)
            .addDependency(BdfJsLibs.OVERLAY)
            .addDependency(FicheJsLibs.FICHEFRAME)
            .addJs("index/Index.js")
            .addJs("index/Index.Search.js")
            .addJs("index/Index.FicheInfo.js")
            .addTemplateFamily("index")
            .toJsLib();
    public final static JsLib INDEX_CLIENT = BdfJsLibBuilder.init()
            .addDependency(INDEX)
            .addJs("index/Index.Client.js")
            .toJsLib();

    private MultiJsLibs() {

    }

}
