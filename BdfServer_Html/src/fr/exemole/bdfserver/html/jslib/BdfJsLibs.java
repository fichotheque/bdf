/* BdfServer_Html - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class BdfJsLibs {


    public final static JsLib AJAX = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Ajax.js")
            .toJsLib();
    public final static JsLib API = BdfJsLibBuilder.init()
            .addDependency(UtilsJsLibs.FQL)
            .addDependency(UtilsJsLibs.FAPI_CORE)
            .addJs("bdf/Bdf.Api.js")
            .toJsLib();
    public final static JsLib APPELANT = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Appelant.js")
            .toJsLib();
    public final static JsLib CODEMIRRORMODE = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.CodeMirrorMode.js")
            .addThirdLib(JsLibCatalog.CODEMIRROR, "xml", "css", "javascript", "properties", "tableexport", "ficheblock", "attributes", "uicomponents", "subsettree")
            .toJsLib();
    public final static JsLib COLLECTIONS = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Ajax.js")
            .addJs("bdf/Bdf.Collections.js")
            .toJsLib();
    public final static JsLib COMMANDTEST = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.CommandWait.js")
            .addJs("bdf/Bdf.CommandTest.js")
            .addThirdLib(JsLibCatalog.JQUERY, "form")
            .toJsLib();
    public final static JsLib COMMANDVEIL = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.CommandVeil.js")
            .toJsLib();
    public final static JsLib COMMANDWAIT = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.CommandWait.js")
            .toJsLib();
    public final static JsLib CONDITIONAL = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Conditional.js")
            .toJsLib();
    public final static JsLib DEPLOY = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Deploy.js")
            .toJsLib();
    public final static JsLib FICHEBLOCK = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Ficheblock.js")
            .toJsLib();
    public final static JsLib FORMSTORAGE = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.FormStorage.js")
            .toJsLib();
    public final static JsLib MULTI = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Multi.js")
            .toJsLib();
    public final static JsLib OVERLAY = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Overlay.js")
            .toJsLib();
    public final static JsLib SHORTCUT = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Shortcut.js")
            .toJsLib();
    public final static JsLib SUBSETCHANGE = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Subsetchange.js")
            .toJsLib();
    public final static JsLib SUBSETTREES = BdfJsLibBuilder.init()
            .addJs("bdf/Bdf.Ajax.js")
            .addJs("bdf/Bdf.SubsetTrees.js")
            .toJsLib();

    private BdfJsLibs() {
    }

}
