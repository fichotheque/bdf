/* BdfServer_Html - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.jslib;

import net.mapeadores.util.jslib.JsLib;


/**
 *
 * @author Vincent Calame
 */
public final class GeoJsLibs {

    public final static JsLib PIOCHE = BdfJsLibBuilder.init()
            .addJs("geo/Geo.js")
            .addJs("geo/Geo.Position.js")
            .addJs("geo/Geo.Geocodage.js")
            .addThirdLib(JsLibCatalog.LEAFLET)
            .addTemplateFamily("geo")
            .toJsLib();
    public final static JsLib FICHES = BdfJsLibBuilder.init()
            .addJs("geo/Geo.js")
            .addJs("geo/Geo.Fiches.js")
            .addThirdLib(JsLibCatalog.LEAFLET, "markercluster")
            .addTemplateFamily("geo")
            .toJsLib();

    private GeoJsLibs() {
    }

}
