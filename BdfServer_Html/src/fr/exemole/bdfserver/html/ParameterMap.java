/* BdfServer_Html - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html;

import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import java.util.LinkedHashMap;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.request.RequestConstants;


/**
 *
 * @author Vincent Calame
 */
public class ParameterMap extends LinkedHashMap<String, String> {

    public ParameterMap() {

    }

    public ParameterMap command(String commandName) {
        put(RequestConstants.COMMAND_PARAMETER, commandName);
        return this;
    }

    public ParameterMap command(String domain, String commandName) {
        put(RequestConstants.COMMAND_PARAMETER, BdfInstructionUtils.getAbsoluteCommandName(domain, commandName));
        return this;
    }

    public ParameterMap borderFrameStyle() {
        put(InteractionConstants.BDF_FRAMESTYLE_PARAMNAME, "border");
        return this;
    }

    public ParameterMap param(String name, String value) {
        if (value == null) {
            remove(name);
        } else {
            put(name, value);
        }
        return this;
    }

    public ParameterMap page(String pageName) {
        put(RequestConstants.PAGE_PARAMETER, pageName);
        return this;
    }

    public ParameterMap page(String domain, String pageName) {
        put(RequestConstants.PAGE_PARAMETER, BdfInstructionUtils.getAbsolutePageName(domain, pageName));
        return this;
    }

    public ParameterMap errorPage(String pageName) {
        put(InteractionConstants.PAGE_ERROR_PARAMNAME, pageName);
        return this;
    }

    public ParameterMap subset(Subset subset) {
        return subset(subset.getSubsetKey());
    }

    public ParameterMap subset(SubsetKey subsetKey) {
        put(subsetKey.getCategoryString(), subsetKey.getSubsetName());
        return this;
    }

    public ParameterMap subsetItem(SubsetItem subsetItem) {
        put(InteractionConstants.ID_PARAMNAME, String.valueOf(subsetItem.getId()));
        return this;
    }

    public static ParameterMap init() {
        return new ParameterMap();
    }

}
