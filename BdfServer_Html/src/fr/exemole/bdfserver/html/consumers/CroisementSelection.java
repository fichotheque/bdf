/* BdfServer_Html - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.croisement.CroisementsBySubset;
import net.fichotheque.croisement.Lien;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.HtmlWrapper;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class CroisementSelection implements Consumer<HtmlPrinter> {

    public final static HtmlWrapper TREE = Tree.tree("global-CroisementTree");
    public final static HtmlWrapper OPEN_NODE = Tree.openNode("global-CroisementOpenContent");
    private final String paramName;
    private final List<CroisementsBySubset> croisementsBySubsetList;
    private final Lang workingLang;
    private final Locale formatLocale;

    public CroisementSelection(String paramName, List<CroisementsBySubset> croisementsBySubsetList, Lang workingLang, Locale formatLocale) {
        this.paramName = paramName;
        this.croisementsBySubsetList = croisementsBySubsetList;
        this.workingLang = workingLang;
        this.formatLocale = formatLocale;
    }

    @Override
    public void accept(HtmlPrinter hp) {
        hp
                .UL("global-CroisementList");
        for (CroisementsBySubset croisementsBySubset : croisementsBySubsetList) {
            Subset subset = croisementsBySubset.getSubset();
            SubsetKey subsetKey = subset.getSubsetKey();
            hp
                    .LI()
                    .P()
                    .__(SubsetTitle.init(subset, workingLang).subsetIcon(true))
                    ._P()
                    .__(TREE, () -> {
                        for (Croisements.Entry entry : croisementsBySubset.getCroisements().getEntryList()) {
                            SubsetItem subsetItem = entry.getSubsetItem();
                            for (Lien lien : entry.getCroisement().getLienList()) {
                                HtmlAttributes inputAttributes = hp.name(paramName).value(getValue(subsetKey, subsetItem, lien.getMode()));
                                hp
                                        .__(Tree.checkboxLeaf(inputAttributes, () -> {
                                            hp
                                                    .__(printMode(hp, lien.getMode(), lien.getPoids()))
                                                    .__(printTitle(hp, subsetItem));
                                        }));
                            }
                        }
                    })
                    ._LI();
        }
        hp
                ._UL();
    }

    private String getValue(SubsetKey subsetKey, SubsetItem subsetItem, String mode) {
        StringBuilder buf = new StringBuilder();
        buf.append(subsetKey);
        if (mode.length() > 0) {
            buf.append('_');
            buf.append(mode);
        }
        buf.append('/');
        buf.append(subsetItem.getId());
        return buf.toString();
    }

    private boolean printMode(HtmlPrinter hp, String mode, int poids) {
        if ((poids > 1) || (mode.length() > 0)) {
            hp
                    .__escape(" <");
            if (mode.length() > 0) {
                hp
                        .__escape(mode);
                if (poids > 1) {
                    hp
                            .__escape('_');
                }
            }
            if (poids > 1) {
                hp
                        .__append(poids);
            }
            hp
                    .__escape("> ");
            return true;
        } else {
            return false;
        }
    }

    private boolean printTitle(HtmlPrinter hp, SubsetItem subsetItem) {
        if (subsetItem instanceof FicheMeta) {
            hp
                    .__escape(CorpusMetadataUtils.getFicheTitle((FicheMeta) subsetItem, workingLang, formatLocale));
        }
        return true;
    }


}
