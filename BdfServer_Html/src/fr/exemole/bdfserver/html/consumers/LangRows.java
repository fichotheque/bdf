/* BdfServer_Html - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import java.util.function.Consumer;
import net.fichotheque.syntax.FormSyntax;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.Litteral;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public class LangRows implements Consumer<HtmlPrinter> {

    private final String namePrefix;
    private final Labels labels;
    private final Lang[] langArray;
    private final Langs langs;
    private final String[] textArray;
    private int cols = 50;
    private int rows = 1;
    private String codemirrorMode = null;
    private boolean displayOnly = false;
    private boolean preformatted = false;

    public LangRows(String namePrefix, Lang[] langArray, String[] textArray) {
        this.namePrefix = namePrefix;
        this.labels = null;
        this.langArray = langArray;
        this.langs = null;
        this.textArray = textArray;
    }

    public LangRows(String namePrefix, @Nullable Labels labels, Langs langs) {
        this.namePrefix = namePrefix;
        this.labels = labels;
        this.langArray = null;
        this.langs = langs;
        this.textArray = null;
    }

    public LangRows cols(int cols) {
        this.cols = cols;
        return this;
    }

    public LangRows rows(int rows) {
        this.rows = rows;
        return this;
    }

    public LangRows displayOnly(boolean displayOnly) {
        this.displayOnly = displayOnly;
        return this;
    }

    public LangRows preformatted(boolean preformatted) {
        this.preformatted = preformatted;
        return this;
    }

    public LangRows codemirrorMode(String codemirrorMode) {
        this.codemirrorMode = codemirrorMode;
        return this;
    }

    @Override
    public void accept(HtmlPrinter hp) {
        if (langArray != null) {
            if (textArray != null) {
                int length = langArray.length;
                for (int i = 0; i < length; i++) {
                    printLang(hp, langArray[i], textArray[i]);
                }
            } else {
                for (Lang lang : langArray) {
                    printLang(hp, lang);
                }
            }
        } else if (langs != null) {
            for (Lang lang : langs) {
                printLang(hp, lang);
            }
        }
    }

    private void printLang(HtmlPrinter hp, Lang lang) {
        String labelString = "";
        if (labels != null) {
            Label label = labels.getLabel(lang);
            if (label != null) {
                labelString = label.getLabelString();
            }
        }
        printLang(hp, lang, labelString);
    }

    private void printLang(HtmlPrinter hp, Lang lang, String value) {
        if (displayOnly) {
            printDisplayOnlyRow(hp, lang, value);
        } else if (rows == 1) {
            printSingleRow(hp, lang, value);
        } else {
            printTextAreaRow(hp, lang, value);
        }
    }

    private void printDisplayOnlyRow(HtmlPrinter hp, Lang lang, String value) {
        String inputClass;
        if (preformatted) {
            inputClass = "grid-InputCell global-Preformatted";
        } else {
            inputClass = "grid-InputCell";
        }
        hp
                .__(Grid.START_ROW)
                .__(Grid.labelCells(new Litteral(lang.toString())))
                .SPAN(inputClass)
                .__escape(value, preformatted)
                ._SPAN()
                .__(Grid.END_ROW);
    }

    private void printSingleRow(HtmlPrinter hp, Lang lang, String value) {
        if (codemirrorMode == null) {
            value = FormSyntax.escapeString(value);
        }
        hp
                .__(Grid.textInputRow(new Litteral(lang.toString()), hp.name(namePrefix + lang.toString()).value(value).size(String.valueOf(cols))));
    }

    private void printTextAreaRow(HtmlPrinter hp, Lang lang, String value) {
        String finalValue;
        if (codemirrorMode == null) {
            finalValue = FormSyntax.escapeString(value);
        } else {
            finalValue = value;
        }
        HtmlAttributes ha = hp.name(namePrefix + lang.toString()).rows(rows).cols(cols)
                .attr("data-codemirror-mode", codemirrorMode);
        hp
                .__(Grid.textAreaInputRow(new Litteral(lang.toString()), ha,
                        () -> {
                            hp
                                    .__escape(finalValue, true);
                        }));
    }

    public static LangRows init(String namePrefix, @Nullable Labels labels, Langs langs) {
        return new LangRows(namePrefix, labels, langs);
    }

    public static LangRows init(String namePrefix, Lang[] langArray, String[] textArray) {
        return new LangRows(namePrefix, langArray, textArray);
    }

}
