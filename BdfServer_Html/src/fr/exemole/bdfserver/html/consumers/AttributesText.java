/* BdfServer_Html - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import java.util.function.Consumer;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public class AttributesText implements Consumer<HtmlPrinter> {

    private final Attributes attributes;

    public AttributesText(Attributes attributes) {
        this.attributes = attributes;
    }

    @Override
    public void accept(HtmlPrinter hp) {
        hp
                .__escape(AttributeParser.toString(attributes), true);
    }

}
