/* BdfServer_Html - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers.attributes;

import java.util.function.Consumer;
import net.mapeadores.util.html.HtmlAttributes;


/**
 *
 * @author Vincent Calame
 */
public final class Deploy {

    public final static Consumer<HtmlAttributes> DETAILS = new Details();
    public final static Consumer<HtmlAttributes> REQUIRED = new Required();

    private Deploy() {

    }

    public static Consumer<HtmlAttributes> radio(String targetId) {
        return new Input("radio", targetId);
    }

    public static Consumer<HtmlAttributes> checkbox(String targetId) {
        return new Input("checkbox", targetId);
    }


    private static class Input implements Consumer<HtmlAttributes> {

        private final String role;
        private final String targetId;

        private Input(String role, String targetId) {
            this.role = role;
            this.targetId = targetId;
        }

        @Override
        public void accept(HtmlAttributes htmlAttributes) {
            htmlAttributes.attr("data-deploy-role", role).attr("data-deploy-target", "#" + targetId);
        }

    }


    private static class Details implements Consumer<HtmlAttributes> {

        private Details() {

        }

        @Override
        public void accept(HtmlAttributes htmlAttributes) {
            htmlAttributes.attr("data-deploy-role", "details");
        }

    }


    private static class Required implements Consumer<HtmlAttributes> {

        private Required() {

        }

        @Override
        public void accept(HtmlAttributes htmlAttributes) {
            htmlAttributes.attr("data-deploy-required", "visible");
        }

    }

}
