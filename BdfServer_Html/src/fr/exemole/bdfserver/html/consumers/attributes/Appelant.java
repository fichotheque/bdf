/* BdfServer_Html - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers.attributes;

import fr.exemole.bdfserver.api.interaction.domains.PiocheDomain;
import fr.exemole.bdfserver.api.users.BdfUser;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.html.HtmlAttributes;


/**
 *
 * @author Vincent Calame
 */
public class Appelant implements Consumer<HtmlAttributes> {

    private final String type;
    private final short category;
    private String sphere;
    private String wanted;
    private String anchor;
    private String fieldtype;
    private int limit;
    private List<String> subsetNameList;

    private Appelant(String type, short category) {
        this.type = type;
        this.category = category;
    }

    public static Appelant user() {
        return new Appelant("user", SubsetKey.CATEGORY_SPHERE);
    }

    public static Appelant motcle() {
        return new Appelant("motcle", SubsetKey.CATEGORY_THESAURUS);
    }

    public static Appelant field() {
        return new Appelant("field", SubsetKey.CATEGORY_CORPUS);
    }

    public static Appelant fiche() {
        return new Appelant("fiche", SubsetKey.CATEGORY_CORPUS);
    }

    public Appelant anchor(String anchor) {
        this.anchor = anchor;
        return this;
    }

    public Appelant fieldtype(String fieldtype) {
        this.fieldtype = fieldtype;
        return this;
    }

    public Appelant fieldtype_datation() {
        return fieldtype(PiocheDomain.DATATION_FIELDTYPE);
    }

    public Appelant limit(int limit) {
        this.limit = limit;
        return this;
    }

    public Appelant sphere(String sphere) {
        this.sphere = sphere;
        return this;
    }

    public Appelant sphere(BdfUser bdfUser) {
        return sphere(bdfUser.getRedacteur().getSubsetName());
    }

    public Appelant subsets(SubsetKey... subsetKeys) {
        if (subsetKeys == null) {
            return this;
        }
        if (subsetNameList == null) {
            subsetNameList = new ArrayList<String>();
        }
        for (SubsetKey item : subsetKeys) {
            if ((item != null) && (item.getCategory() == category)) {
                subsetNameList.add(item.getSubsetName());
            }
        }
        return this;
    }

    public Appelant subsets(String... subsetNames) {
        if (subsetNames == null) {
            return this;
        }
        if (subsetNameList == null) {
            subsetNameList = new ArrayList<String>();
        }
        for (String item : subsetNames) {
            if (item != null) {
                subsetNameList.add(item);
            }
        }
        return this;
    }

    public Appelant wanted(String wanted) {
        this.wanted = wanted;
        return this;
    }

    public Appelant wanted_code_id() {
        return wanted(PiocheDomain.CODE_ID_WANTED);
    }

    @Override
    public void accept(HtmlAttributes htmlAttributes) {
        String subsets = null;
        if (subsetNameList != null) {
            StringBuilder buf = new StringBuilder();
            for (String subsetName : subsetNameList) {
                if (buf.length() > 0) {
                    buf.append(',');
                }
                buf.append(subsetName);
            }
            subsets = buf.toString();
        }
        htmlAttributes
                .attr("data-appelant-role", "input")
                .attr("data-appelant-type", type)
                .attr("data-appelant-anchor", anchor)
                .attr((limit > 0), "data-appelant-limit", String.valueOf(limit))
                .attr("data-appelant-subsets", subsets)
                .attr("data-appelant-sphere", sphere)
                .attr("data-appelant-wanted", wanted)
                .attr("data-appelant-fieldtype", fieldtype);
    }

}
