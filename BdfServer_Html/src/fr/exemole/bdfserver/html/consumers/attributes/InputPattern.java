/* BdfServer_Html - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers.attributes;

import java.util.function.Consumer;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public final class InputPattern {

    public final static Consumer<HtmlAttributes> LANG = new InternalConsumer("[a-z]{2}[a-z]?(-[\\-a-zA-Z]+)?", "_ info.inputpattern.lang");
    public final static Consumer<HtmlAttributes> TECHNICAL_STRICT = new InternalConsumer("[a-z][a-z0-9]*", "_ info.inputpattern.technical_strict");
    public final static Consumer<HtmlAttributes> TECHNICAl_UNDERSCORE = new InternalConsumer("[a-z][_a-z0-9]*", "_ info.inputpattern.technical_underscore");
    public final static Consumer<HtmlAttributes> LOGIN = new InternalConsumer("[a-zA-Z][a-zA-Z0-9]*", "_ info.inputpattern.login");
    public final static Consumer<HtmlAttributes> AUTHORITY = new InternalConsumer("[\\-_\\.a-zA-Z0-9]+", "_ info.inputpattern.authority");
    public final static Consumer<HtmlAttributes> IDALPHA = new InternalConsumer("[\\-_\\.\\/@:~\\(\\)\\[\\]a-zA-Z0-9\\s]+", "_ info.inputpattern.idalpha");
    public final static Consumer<HtmlAttributes> SIZES = new InternalConsumer("([0-9]+[xX][0-9]+)(\\s[0-9]+[xX][0-9]+)*", "_ info.inputpattern.sizes");
    public final static Consumer<HtmlAttributes> DOCUMENTNAME = new InternalConsumer("[\\-_\\.a-z0-9]+", "_ info.inputpattern.documentname");
    public final static Consumer<HtmlAttributes> IDLIST = new InternalConsumer("[\\-;0-9\\s]*", "_ info.inputpattern.idlist");

    private InputPattern() {

    }


    private static class InternalConsumer implements Consumer<HtmlAttributes> {

        private final String pattern;
        private final String titleLocKey;
        private final Message titleMessage;

        private InternalConsumer(String pattern, String titleLocKey) {
            this.pattern = pattern;
            this.titleLocKey = titleLocKey;
            this.titleMessage = LocalisationUtils.toMessage(titleLocKey, pattern);
        }

        @Override
        public void accept(HtmlAttributes htmlAttributes) {
            htmlAttributes.pattern(pattern).titleMessage(titleMessage);
        }

    }


}
