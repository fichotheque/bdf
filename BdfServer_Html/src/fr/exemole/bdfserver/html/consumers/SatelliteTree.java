/* BdfServer_Html - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.interaction.domains.EditionDomain;
import fr.exemole.bdfserver.api.roles.SatelliteOpportunities;
import fr.exemole.bdfserver.html.BdfHtmlConstants;
import fr.exemole.bdfserver.tools.BH;
import fr.exemole.bdfserver.tools.instruction.BdfInstructionUtils;
import java.util.Locale;
import java.util.function.Consumer;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.HtmlWrapper;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class SatelliteTree implements Consumer<HtmlPrinter> {

    public final static HtmlWrapper TREE = Tree.tree("subset-satellite-Tree");
    public final static HtmlWrapper LEAF = Tree.leaf("subset-satellite-Leaf");
    private final Lang lang;
    private final Locale formatLocale;
    private String target = BdfHtmlConstants.EDITION_FRAME;
    private SubsetItem masterSubsetItem;
    private SatelliteOpportunities satelliteOpportunities;


    public SatelliteTree(Lang lang, Locale formatLocale) {
        this.lang = lang;
        this.formatLocale = formatLocale;
    }

    public SatelliteTree current(SubsetItem masterSubsetItem, SatelliteOpportunities satelliteOpportunities) {
        this.masterSubsetItem = masterSubsetItem;
        this.satelliteOpportunities = satelliteOpportunities;
        return this;
    }

    public SatelliteTree target(String target) {
        this.target = target;
        return this;
    }

    @Override
    public void accept(HtmlPrinter hp) {
        hp
                .__(TREE, () -> {
                    for (SatelliteOpportunities.Entry entry : satelliteOpportunities.getEntryList()) {
                        hp
                                .__(LEAF, () -> {
                                    short category = entry.getAvailableActionCategory();
                                    switch (category) {
                                        case SatelliteOpportunities.READ:
                                        case SatelliteOpportunities.WRITE:
                                            printSatelliteDisplay(hp, entry.getFicheMeta(), (category == SatelliteOpportunities.WRITE));
                                            break;
                                        case SatelliteOpportunities.CREATE:
                                            printSatelliteCreation(hp, entry.getSatelliteCorpus(), String.valueOf(masterSubsetItem.getId()));
                                            break;
                                        default:
                                            throw new SwitchException("wrong type : " + category);
                                    }
                                });
                    }
                });

    }

    public static SatelliteTree init(Lang lang, Locale formatLocale) {
        return new SatelliteTree(lang, formatLocale);
    }

    private boolean printSatelliteDisplay(HtmlPrinter hp, FicheMeta satelliteFiche, boolean canWrite) {
        String ficheTitle = CorpusMetadataUtils.getFicheTitle(satelliteFiche, lang, formatLocale);
        hp
                .SPAN("subset-satellite-CorpusTitle subset-satellite-Display")
                .__escape(CorpusMetadataUtils.getSatelliteLabel(satelliteFiche.getCorpus(), lang))
                .__colon()
                ._SPAN()
                .A(HA.href(BdfInstructionUtils.getFicheGetLink(satelliteFiche, "html")).classes("global-button-Transparent action-FicheDisplay").target(target).title(hp.getLocalization("_ link.fiches.fiche_long") + " = " + ficheTitle))
                .__(Button.ICON)
                ._A();
        if (canWrite) {
            hp
                    .A(HA.href(BH.domain(Domains.EDITION).page(EditionDomain.FICHE_CHANGE_PAGE).subsetItem(satelliteFiche)).target(target).classes("global-button-Transparent action-FicheEdit").title(hp.getLocalization("_ link.edition.fichechange_long") + " = " + ficheTitle))
                    .__(Button.ICON)
                    ._A();
        }
        return true;
    }

    private boolean printSatelliteCreation(HtmlPrinter hp, Corpus satelliteCorpus, String idValue) {
        hp
                .SPAN("subset-satellite-CorpusTitle subset-satellite-Creation")
                .__escape(FichothequeUtils.getTitle(satelliteCorpus, lang))
                .__colon()
                ._SPAN()
                .A(HA.href(BH.domain(Domains.EDITION).page(EditionDomain.FICHE_CHANGE_PAGE).subset(satelliteCorpus).param(InteractionConstants.ID_PARAMNAME, idValue).param(EditionDomain.FORCE_PARAMNAME, "1")).target(target).classes("global-button-Transparent action-FicheCreate").titleLocKey("_ label.edition.satellite_creation"))
                .__(Button.ICON)
                ._A();
        return true;
    }

}
