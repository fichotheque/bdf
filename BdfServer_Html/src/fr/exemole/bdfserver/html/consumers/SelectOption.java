/* BdfServer_Html - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import java.util.Collection;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Litteral;


/**
 *
 * @author Vincent Calame
 */
public class SelectOption implements BiConsumer<HtmlPrinter, Object> {

    private final String value;
    private Object textL10nObject;

    public SelectOption(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public SelectOption textL10nObject(Object textL10nObject) {
        this.textL10nObject = textL10nObject;
        return this;
    }

    public SelectOption text(String text) {
        if (text == null) {
            this.textL10nObject = null;
        } else {
            this.textL10nObject = new Litteral(text);
        }
        return this;
    }

    @Override
    public void accept(HtmlPrinter hp, Object object) {
        boolean selected = false;
        if (object != null) {
            if (object instanceof Boolean) {
                selected = (Boolean) object;
            } else {
                selected = (object.equals(value));
            }
        }
        hp
                .OPTION(value, selected);
        if (textL10nObject != null) {
            hp
                    .__localize(textL10nObject);
        }
        hp
                ._OPTION();
    }

    public static SelectOption init(String value) {
        return new SelectOption(value);
    }

    public static Consumer<HtmlPrinter> consumer(Collection<SelectOption> selectOptions, Object currentValue) {
        return new CollectionConsumer(selectOptions, currentValue);
    }

    public static Consumer<HtmlPrinter> consumer(SelectOption[] selectOptionArray, Object currentValue) {
        return new ArrayConsumer(selectOptionArray, currentValue);
    }


    private static class CollectionConsumer implements Consumer<HtmlPrinter> {

        private final Collection<SelectOption> selectOptions;
        private final Object currentValue;


        private CollectionConsumer(Collection<SelectOption> selectOptions, Object currentValue) {
            this.selectOptions = selectOptions;
            this.currentValue = currentValue;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            for (SelectOption option : selectOptions) {
                hp
                        .__(option, currentValue);
            }
        }

    }


    private static class ArrayConsumer implements Consumer<HtmlPrinter> {

        private final SelectOption[] selectOptions;
        private final Object currentValue;


        private ArrayConsumer(SelectOption[] selectOptions, Object currentValue) {
            this.selectOptions = selectOptions;
            this.currentValue = currentValue;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            for (SelectOption option : selectOptions) {
                hp
                        .__(option, currentValue);
            }
        }

    }

}
