/* BdfServer_Html - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import java.util.function.Consumer;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.html.ConsumerFactory;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class SubsetIcon {

    public final static Consumer<HtmlPrinter> ADDENDA = ConsumerFactory.emptySpan("subset-Icon subset-Icon_addenda");
    public final static Consumer<HtmlPrinter> ALBUM = ConsumerFactory.emptySpan("subset-Icon subset-Icon_album");
    public final static Consumer<HtmlPrinter> CORPUS = ConsumerFactory.emptySpan("subset-Icon subset-Icon_corpus");
    public final static Consumer<HtmlPrinter> COUNTRY = ConsumerFactory.emptySpan("subset-Icon subset-Icon_country");
    public final static Consumer<HtmlPrinter> ISO = ConsumerFactory.emptySpan("subset-Icon subset-Icon_iso");
    public final static Consumer<HtmlPrinter> LANG = ConsumerFactory.emptySpan("subset-Icon subset-Icon_lang");
    public final static Consumer<HtmlPrinter> SPHERE = ConsumerFactory.emptySpan("subset-Icon subset-Icon_sphere");
    public final static Consumer<HtmlPrinter> THESAURUS = ConsumerFactory.emptySpan("subset-Icon subset-Icon_thesaurus");


    private SubsetIcon() {

    }

    public static Consumer<HtmlPrinter> getIcon(short subsetCategory) {
        switch (subsetCategory) {
            case SubsetKey.CATEGORY_CORPUS:
                return CORPUS;
            case SubsetKey.CATEGORY_THESAURUS:
                return THESAURUS;
            case SubsetKey.CATEGORY_SPHERE:
                return SPHERE;
            case SubsetKey.CATEGORY_ADDENDA:
                return ADDENDA;
            case SubsetKey.CATEGORY_ALBUM:
                return ALBUM;
            default:
                throw new IllegalArgumentException("wrong croisementCategory value");
        }
    }

}
