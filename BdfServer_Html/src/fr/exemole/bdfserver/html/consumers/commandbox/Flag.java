/* BdfServer_Html - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers.commandbox;

import java.util.function.Consumer;
import net.mapeadores.util.html.CommandBox;


/**
 *
 * @author Vincent Calame
 */
public final class Flag {

    public final static Consumer<CommandBox> UPDATE_CORPUS_TREE = new InternalConsumer("updateCorpusTree");
    public final static Consumer<CommandBox> UPDATE_COLLECTIONS = new InternalConsumer("updateCollections");


    private static class InternalConsumer implements Consumer<CommandBox> {

        private final String flag;

        private InternalConsumer(String flag) {
            this.flag = flag;
        }

        @Override
        public void accept(CommandBox commandBox) {
            String action = commandBox.action();
            StringBuilder buf = new StringBuilder(action);
            if (action.contains("?")) {
                buf.append('&');
            } else {
                buf.append('?');
            }
            buf.append("bdf-flag=");
            buf.append(flag);
            commandBox.action(buf.toString());
        }

    }

}
