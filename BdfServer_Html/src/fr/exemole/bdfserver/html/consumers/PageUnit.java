/* BdfServer_Html - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import java.util.function.Consumer;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.HtmlWrapper;
import net.mapeadores.util.html.WrapperFactory;


/**
 *
 * @author Vincent Calame
 */
public class PageUnit implements Consumer<HtmlPrinter> {

    public final static HtmlWrapper SIMPLE = WrapperFactory.div("unit-SimpleUnit");
    public final static Consumer<HtmlPrinter> END = new End();
    public final static Consumer<HtmlPrinter> END_DETAILS = new DetailsEnd();
    private Object titleL10nObject;
    private String action;
    private String sectionCssClasses;
    private String family;
    private Consumer<HtmlPrinter> icon;
    private boolean isDetailsVariant = false;
    private boolean isDetailsOpen = false;

    public PageUnit() {

    }

    public PageUnit(Object l10nObject) {
        this.titleL10nObject = l10nObject;
    }

    public PageUnit action(String action) {
        this.action = action;
        return this;
    }

    public PageUnit title(Object titleL10nObject) {
        this.titleL10nObject = titleL10nObject;
        return this;
    }

    public PageUnit title(Runnable titleRunnable) {
        this.titleL10nObject = titleRunnable;
        return this;
    }

    public PageUnit sectionCss(String sectionCssClasses) {
        this.sectionCssClasses = sectionCssClasses;
        return this;
    }

    public PageUnit icon(Consumer<HtmlPrinter> icon) {
        this.icon = icon;
        return this;
    }

    public PageUnit family(String family) {
        this.family = family;
        return this;
    }

    public PageUnit detailsVariant(boolean isDetailsVariant) {
        this.isDetailsVariant = isDetailsVariant;
        return this;
    }

    public PageUnit detailsOpen(boolean isDetailsOpen) {
        this.isDetailsOpen = isDetailsOpen;
        return this;
    }

    @Override
    public void accept(HtmlPrinter hp) {
        boolean withFamily = (family != null);
        String finalSectionClasses;
        if (sectionCssClasses != null) {
            finalSectionClasses = sectionCssClasses;
        } else {
            finalSectionClasses = "unit-Unit";
        }
        if (withFamily) {
            finalSectionClasses = finalSectionClasses + " family-Colors " + family;
        }
        String finalHeaderClasses;
        if (withFamily) {
            finalHeaderClasses = "unit-Header family-Colors";
        } else {
            finalHeaderClasses = "unit-Header";
        }
        StringBuilder h1Buf = new StringBuilder();
        h1Buf.append("unit-Title");
        if (action != null) {
            h1Buf.append(" ");
            h1Buf.append(action);
        }
        if (withFamily) {
            h1Buf.append(" family-Colors");
        }
        if (isDetailsVariant) {
            hp
                    .SECTION(finalSectionClasses)
                    .DETAILS(HA.open(isDetailsOpen))
                    .SUMMARY(finalHeaderClasses + " unit-Title")
                    .__(printIcon(hp))
                    .__(printTitleSpan(hp))
                    ._SUMMARY()
                    .DIV("unit-Body");
        } else {
            hp
                    .SECTION(finalSectionClasses)
                    .HEADER(finalHeaderClasses)
                    .H1(h1Buf.toString())
                    .__(printIcon(hp))
                    .__(printTitleSpan(hp))
                    ._H1()
                    ._HEADER()
                    .DIV("unit-Body");
        }
    }

    private boolean printTitleSpan(HtmlPrinter hp) {
        if (titleL10nObject != null) {
            hp
                    .SPAN("unit-TitleSpan")
                    .__localize(titleL10nObject)
                    ._SPAN();
            return true;
        } else {
            return false;
        }
    }

    private boolean printIcon(HtmlPrinter hp) {
        if (icon != null) {
            hp
                    .__(icon);
            return true;
        } else if (!isDetailsVariant) {
            hp
                    .__(Button.ICON);
            return true;
        } else {
            return false;
        }
    }

    public static PageUnit start() {
        return new PageUnit();
    }

    public static PageUnit start(Object titleL10nObject) {
        return (new PageUnit(titleL10nObject));
    }

    public static PageUnit start(String action, Object titleL10nObject) {
        return (new PageUnit(titleL10nObject)).action(action);
    }

    public static PageUnit startDetails(Object titleL10nObject, boolean open) {
        return (new PageUnit(titleL10nObject).detailsVariant(true).detailsOpen(open));
    }


    private static class End implements Consumer<HtmlPrinter> {

        private End() {
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    ._DIV()
                    ._SECTION();
        }

    }


    private static class DetailsEnd implements Consumer<HtmlPrinter> {

        private DetailsEnd() {
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    ._DIV()
                    ._DETAILS()
                    ._SECTION();
        }

    }

}
