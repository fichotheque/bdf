/* BdfServer_Html - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import fr.exemole.bdfserver.api.BdfServerConstants;
import net.mapeadores.util.html.HtmlWrapper;
import net.mapeadores.util.html.WrapperFactory;


/**
 *
 * @author Vincent Calame
 */
public final class FicheForm {

    public final static HtmlWrapper LARGE_CELL = WrapperFactory.div("ficheform-standard-Cell ficheform-Large");
    public final static HtmlWrapper MEDIUM_CELL = WrapperFactory.div("ficheform-standard-Cell ficheform-Medium");
    public final static HtmlWrapper SMALL_CELL = WrapperFactory.div("ficheform-standard-Cell ficheform-Small");
    public final static HtmlWrapper XSMALL_CELL = WrapperFactory.div("ficheform-standard-Cell ficheform-XSmall");

    private FicheForm() {

    }

    public static HtmlWrapper cell(String widthType) {
        switch (widthType) {
            case BdfServerConstants.WIDTH_SMALL:
                return SMALL_CELL;
            case BdfServerConstants.WIDTH_MEDIUM:
                return MEDIUM_CELL;
            case BdfServerConstants.WIDTH_LARGE:
                return LARGE_CELL;
            default:
                return LARGE_CELL;
        }
    }


}
