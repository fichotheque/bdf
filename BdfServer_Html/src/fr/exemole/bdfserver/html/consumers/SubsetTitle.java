/* BdfServer_Html - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import static fr.exemole.bdfserver.html.BdfHtmlUtils.getSubsetCategoryLocKey;
import java.util.function.Consumer;
import net.fichotheque.Subset;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public class SubsetTitle implements Consumer<HtmlPrinter> {

    private final Subset subset;
    private final Lang lang;
    private boolean withSubsetIcon = false;
    private boolean withCategoryPrefix = false;
    private boolean withSubsetName = false;

    public SubsetTitle(Subset subset, Lang lang) {
        this.subset = subset;
        this.lang = lang;
    }

    public SubsetTitle subsetIcon(boolean withSubsetIcon) {
        this.withSubsetIcon = withSubsetIcon;
        return this;
    }

    public SubsetTitle categoryPrefix(boolean with) {
        this.withCategoryPrefix = with;
        return this;
    }

    public SubsetTitle subsetName(boolean withSubsetName) {
        this.withSubsetName = withSubsetName;
        return this;
    }

    @Override
    public void accept(HtmlPrinter hp) {
        short subsetCategory = subset.getSubsetKey().getCategory();
        if (withSubsetIcon) {
            hp
                    .__(SubsetIcon.getIcon(subsetCategory));
        }
        if (withCategoryPrefix) {
            hp
                    .SPAN("subset-Category")
                    .__localize(getSubsetCategoryLocKey(subsetCategory))
                    .__colon()
                    ._SPAN();
        }
        if (withSubsetName) {
            Label label = FichothequeUtils.getTitleLabel(subset, lang);
            if (label != null) {
                hp
                        .SPAN("subset-Title")
                        .__escape(label.getLabelString())
                        .__space()
                        .SPAN("subset-Key")
                        .__escape("[")
                        .__escape(subset.getSubsetName())
                        .__escape("]")
                        ._SPAN()
                        ._SPAN();
            } else {
                hp
                        .SPAN("subset-Title")
                        .__escape("[")
                        .__escape(subset.getSubsetName())
                        .__escape("]")
                        ._SPAN();

            }
        } else {
            hp
                    .SPAN("subset-Title")
                    .__escape(FichothequeUtils.getTitle(subset, lang))
                    ._SPAN();
        }
    }

    public static SubsetTitle init(Subset subset, Lang lang) {
        return new SubsetTitle(subset, lang);
    }

}
