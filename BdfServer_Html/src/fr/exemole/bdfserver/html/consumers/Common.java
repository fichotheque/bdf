/* BdfServer_Html - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html.consumers;

import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import java.util.function.Consumer;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.html.HtmlConstants;
import net.mapeadores.util.html.HtmlPrinter;


/**
 *
 * @author Vincent Calame
 */
public final class Common {

    public final static Consumer<HtmlPrinter> REMOVE_CONFIRM_CHECK = confirmationCheck("_ label.global.confirmationcheck_remove");
    public final static Consumer<HtmlPrinter> CLEANING_CONFIRM_CHECK = Common.confirmationCheck("_ label.global.confirmationcheck_cleaning");
    public final static Consumer<HtmlPrinter> MOVE_CONFIRM_CHECK = Common.confirmationCheck("_ label.global.confirmationcheck_move");
    public final static Consumer<HtmlPrinter> MERGE_CONFIRM_CHECK = Common.confirmationCheck("_ label.global.confirmationcheck_merge");
    public final static Consumer<HtmlPrinter> REPLACE_CONFIRM_CHECK = Common.confirmationCheck("_ label.global.confirmationcheck_replace");

    private Common() {

    }

    public static Consumer<HtmlPrinter> escape(String text) {
        return new Escape(text);
    }

    public static Consumer<HtmlPrinter> localize(String key) {
        return new Localize(key);
    }

    public static Consumer<HtmlPrinter> checkboxParagraph(String labelKey, HtmlAttributes choiceAttributes, boolean smaller) {
        return new ChoiceParagraph(labelKey, choiceAttributes.copy().type(HtmlConstants.CHECKBOX_TYPE), smaller);
    }

    public static Consumer<HtmlPrinter> radioParagraph(String labelKey, HtmlAttributes choiceAttributes, boolean smaller) {
        return new ChoiceParagraph(labelKey, choiceAttributes.copy().type(HtmlConstants.RADIO_TYPE), smaller);
    }


    public static Consumer<HtmlPrinter> confirmationCheck(String labelKey) {
        return checkboxParagraph(labelKey, HA.name(InteractionConstants.CONFIRMATIONCHECK_PARAMNAME).value(InteractionConstants.OK_CONFIRMATIONCHECK_PARAMVALUE), true);
    }


    private static class Escape implements Consumer<HtmlPrinter> {

        private final String text;

        private Escape(String text) {
            this.text = text;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .__escape(text);
        }

    }


    private static class Localize implements Consumer<HtmlPrinter> {

        private final String key;

        private Localize(String key) {
            this.key = key;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .__localize(key);
        }

    }


    private static class ChoiceParagraph implements Consumer<HtmlPrinter> {

        private final String labelKey;
        private final HtmlAttributes choiceAttributes;
        private final boolean smaller;

        private ChoiceParagraph(String labelKey, HtmlAttributes choiceAttributes, boolean smaller) {
            this.labelKey = labelKey;
            this.choiceAttributes = choiceAttributes;
            this.smaller = smaller;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            String id = hp.generateId();
            hp
                    .P((smaller) ? "command-FlexInput command-Smaller" : "command-FlexInput")
                    .INPUT(choiceAttributes.copy().id(id))
                    .LABEL_for(id)
                    .__localize(labelKey)
                    ._LABEL()
                    ._P();

        }

    }

}
