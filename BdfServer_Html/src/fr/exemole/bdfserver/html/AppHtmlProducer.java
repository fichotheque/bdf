/* BdfServer_Html - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.tools.apps.AppConf;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.html.TrustedHtml;
import net.mapeadores.util.html.jsoup.TrustedHtmlFactories;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class AppHtmlProducer extends BdfServerHtmlProducer {

    private final AppConf appConf;

    public AppHtmlProducer(BdfServer bdfServer, LangPreference langPreference, AppConf appConf) {
        super(bdfServer, langPreference);
        this.appConf = appConf;
        init();
    }

    public AppHtmlProducer(BdfParameters bdfParameters, AppConf appConf) {
        super(bdfParameters);
        this.appConf = appConf;
        init();
    }

    private void init() {
        scanIcons();
        AppInit.init(this, appConf, bdfServer.getResourceStorages(), bdfServer.getExtensionManager());
    }

    @Override
    public void printHtml() {
        startPhrase(appConf.getString(AppConf.CORE_TITLEPHRASENAME));
        RelativePath appHtmlPath = StorageUtils.buildAppResourcePath(appConf.getAppName(), "app.html");
        DocStream docStream = bdfServer.getResourceStorages().getResourceDocStream(appHtmlPath);
        if (docStream != null) {
            String appHtml = docStream.getContent();
            TrustedHtml trustedHtml = TrustedHtmlFactories.WELLFORMED.build(appHtml);
            this
                    .__append(trustedHtml);
        }
        JsObject jsObject = appConf.getArgsJsObject();
        if (jsObject != null) {
            this
                    .SCRIPT()
                    .__jsObject("ARGS", jsObject)
                    ._SCRIPT();
        }
        end();
    }


}
