/* BdfServer_HtmlProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.html;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfParameters;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.html.HtmlPrinterUtils;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.logging.CommandMessage;


/**
 *
 * @author Vincent Calame
 */
public class MessageHtmlProducer extends BdfServerHtmlProducer {

    private final List<CommandMessage> commandMessageList = new ArrayList<CommandMessage>();

    public MessageHtmlProducer(BdfParameters bdfParameters) {
        super(bdfParameters);
    }

    public MessageHtmlProducer(BdfServer bdfServer, LangPreference langPreference) {
        super(bdfServer, langPreference);
    }

    public MessageHtmlProducer addMessages(CommandMessage... messages) {
        if (messages != null) {
            for (CommandMessage message : messages) {
                commandMessageList.add(message);
            }
        }
        return this;
    }

    @Override
    public void printHtml() {
        startLoc("_ title.global.message", false);
        __(PageUnit.SIMPLE, () -> {
            printMainMessage();
            printMessageList();
        });
        end();
    }

    private boolean printMainMessage() {
        CommandMessage cm = getCommandMessage();
        if (cm != null) {
            this
                    .__(HtmlPrinterUtils.printCommandMessage(this, cm, "global-DoneMessage", "global-ErrorMessage"));
            return true;
        } else {
            return false;
        }
    }

    private boolean printMessageList() {
        if (commandMessageList.isEmpty()) {
            return false;
        }
        for (CommandMessage commandMessage : commandMessageList) {
            this
                    .__(HtmlPrinterUtils.printCommandMessage(this, commandMessage, "global-DoneMessage", "global-ErrorMessage"));
        }
        return true;
    }

    public static MessageHtmlProducer init(BdfParameters bdfParameters) {
        return new MessageHtmlProducer(bdfParameters);
    }

    public static MessageHtmlProducer init(BdfServer bdfServer, LangPreference langPreference) {
        return new MessageHtmlProducer(bdfServer, langPreference);
    }

}
