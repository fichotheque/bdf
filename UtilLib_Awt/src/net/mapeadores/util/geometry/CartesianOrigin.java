/* UtilLib - Copyright (c) 2006-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.geometry;

import java.awt.Point;


/**
 *
 * @author Vincent Calame
 */
public class CartesianOrigin {

    int graphicOx = 10;
    int graphicOy = 10;

    public CartesianOrigin() {
    }

    public CartesianOrigin(CartesianOrigin cartesianOrigin) {
        this.graphicOx = cartesianOrigin.graphicOx;
        this.graphicOy = cartesianOrigin.graphicOy;
    }

    public int getGraphicX() {
        return graphicOx;
    }

    public int getGraphicY() {
        return graphicOy;
    }

    public Point setGraphicCoordinates(int ox, int oy) {
        Point translation = new Point(ox - graphicOx, oy - graphicOy);
        graphicOx = ox;
        graphicOy = oy;
        return translation;
    }

    public CartesianPoint toCartesianPoint(Point graphicPoint) {
        int x = graphicPoint.x - graphicOx;
        int y = graphicOy - graphicPoint.y;
        return new CartesianPoint(x, y);
    }

    public Point toGraphicPoint(CartesianPoint cartesianPoint) {
        int x = graphicOx + cartesianPoint.x;
        int y = graphicOy - cartesianPoint.y;
        return new Point(x, y);
    }

}
