/* UtilLib_Email - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.email;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.smtp.SmtpParameters;
import net.mapeadores.util.smtp.SmtpUtils;


/**
 *
 * @author Vincent Calame
 */
public final class EmailUtils {

    private EmailUtils() {

    }

    public static InternetAddress convert(EmailCore emailCore) {
        InternetAddress result;
        String realName = emailCore.getRealName();
        try {
            if (realName.isEmpty()) {
                result = new InternetAddress(emailCore.getAddrSpec());
            } else {
                result = new InternetAddress(emailCore.getAddrSpec(), realName, "UTF-8");
            }
            return result;
        } catch (AddressException ae) {
            throw new ImplementationException(ae);
        } catch (UnsupportedEncodingException uee) {
            throw new ShouldNotOccurException(uee);
        }
    }

    public static InternetAddress[] convert(Collection<EmailCore> collection) {
        List<InternetAddress> list = new ArrayList<InternetAddress>();
        for (EmailCore emailCore : collection) {
            list.add(convert(emailCore));
        }
        return list.toArray(new InternetAddress[list.size()]);
    }

    public static Session createSession(SmtpParameters smtpParameters) {
        Properties properties = System.getProperties();
        properties.put("mail.transport.protocol", "smtp");
        properties.put("mail.smtp.host", smtpParameters.getHost());
        properties.put("mail.smtp.port", smtpParameters.getPort());
        properties.put("mail.smtp.starttls.enable", "true");
        String authentificationType = smtpParameters.getAuthentificationType();
        Authenticator authenticator = null;
        if (SmtpUtils.isUserMandatory(authentificationType)) {
            properties.setProperty("mail.smtp.auth", "true");
            authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(smtpParameters.getUsername(), smtpParameters.getPassword());
                }

            };
            switch (authentificationType) {
                case SmtpParameters.AUTHENTIFICATION_SSL:
                    properties.put("mail.smtp.ssl.enable", "true");
                    break;
                case SmtpParameters.AUTHENTIFICATION_STARTTLS:
                    properties.put("mail.smtp.starttls.required", "true");
                    break;

            }
        }
        if (authenticator != null) {
            return Session.getInstance(properties, authenticator);
        } else {
            return Session.getInstance(properties);
        }
    }

}
