/* OdLib_Io - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.transform;

import net.mapeadores.opendocument.io.Pictures;


/**
 *
 * @author Vincent Calame
 */
public abstract class PictureHandler {

    private final static String HREF_ATTRIBUTE = "xlink:href=";

    public PictureHandler() {

    }

    public abstract String checkHref(String href);

    public abstract Pictures toPictures();

    public String check(String text) {
        int idx = text.indexOf(HREF_ATTRIBUTE);
        if (idx == -1) {
            return text;
        }
        StringBuffer buf = new StringBuffer(text);
        check(buf, idx);
        return buf.toString();
    }

    public void check(StringBuffer buf) {
        check(buf, buf.indexOf(HREF_ATTRIBUTE));
    }

    private void check(StringBuffer buf, int idx) {
        int idx_debut;
        int idx_fin;
        while (idx != -1) {
            idx_debut = idx + HREF_ATTRIBUTE.length() + 1;
            String quoteChar = buf.substring(idx_debut - 1, idx_debut);
            idx_fin = buf.indexOf(quoteChar, idx_debut);
            String path = buf.substring(idx_debut, idx_fin);
            String checkedHref = checkHref(path);
            if (checkedHref != null) {
                buf.delete(idx_debut, idx_fin);
                buf.insert(idx_debut, checkedHref);
                idx_fin = idx_debut + checkedHref.length();
            }
            idx = buf.indexOf(HREF_ATTRIBUTE, idx_fin);
        }
    }

}
