/* OdLib_Io - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


/**
 *
 * @author Vincent Calame
 */
public class CharSequenceOdSource implements OdSource {

    private final CharSequence content;
    private final String encoding;

    public CharSequenceOdSource(CharSequence content, String encoding) {
        this.content = content;
        this.encoding = encoding;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, encoding));
        int length = content.length();
        for (int i = 0; i < length; i++) {
            writer.write(content.charAt(i));
        }
        writer.flush();
    }

}
