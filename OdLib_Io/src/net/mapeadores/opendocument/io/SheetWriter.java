/* OdLib_Io - Copyright (c) 2016-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io;

import java.io.IOException;
import java.util.List;
import net.mapeadores.opendocument.io.odtable.OdTableDef;
import net.mapeadores.opendocument.io.odtable.OdsXMLPart;


/**
 *
 * @author Vincent Calame
 */
public interface SheetWriter {

    public List<OdTableDef> getTableDefList();

    public void writeTable(OdsXMLPart odsXMLPart, String originalName, String checkedName) throws IOException;

}
