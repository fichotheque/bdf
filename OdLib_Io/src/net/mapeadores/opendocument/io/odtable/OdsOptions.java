/* OdLib_Io - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io.odtable;

import net.mapeadores.opendocument.elements.ElementMaps;
import net.mapeadores.opendocument.elements.OdLog;
import net.mapeadores.opendocument.io.SheetWriter;


/**
 *
 * @author Vincent Calame
 */
public class OdsOptions {

    private OdLog odLog;
    private ElementMaps elementMaps;
    private SheetWriter supplementarySheetWriter;

    public OdsOptions() {

    }

    public OdLog odLog() {
        return odLog;
    }

    public OdsOptions odLog(OdLog odLog) {
        this.odLog = odLog;
        return this;
    }

    public ElementMaps elementMaps() {
        return elementMaps;
    }

    public OdsOptions elementMaps(ElementMaps elementMaps) {
        this.elementMaps = elementMaps;
        return this;
    }

    public SheetWriter supplementarySheetWriter() {
        return supplementarySheetWriter;
    }

    public OdsOptions supplementarySheetWriter(SheetWriter supplementarySheetWriter) {
        this.supplementarySheetWriter = supplementarySheetWriter;
        return this;
    }

    public static OdsOptions init() {
        return new OdsOptions();
    }

}
