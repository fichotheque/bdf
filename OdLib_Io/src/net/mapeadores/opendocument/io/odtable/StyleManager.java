/* OdLib_Io - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io.odtable;

import java.io.IOException;
import java.util.List;
import net.mapeadores.util.money.ExtendedCurrency;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public interface StyleManager {

    /**
     * Retourne un nom de style de cellule correspondant aux arguments. Peut
     * retourner null, notamment si le style de la cellule correspond au style
     * par défaut de la colonne (indiquée par columnNumber)
     *
     */
    public String getCellStyleName(String tableName, int columnNumber, short styleFamily, String parentStyleName);

    public String getCurrencyStyleName(String tableName, int columnNumber, String parentStyleName, ExtendedCurrency currency);

    /**
     * Liste des styles à appliquer par défaut aux colonnes. Est censé être
     * traduit par des éléments <table:table-column> placés juste après
     * <table:table>
     *
     * @param tableName
     * @return
     */
    public List<OdColumn> getOdColumnList(String tableName);

    public void insertAutomaticStyles(XMLWriter xmlWriter) throws IOException;

}
