/* OdLib_Io - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io.odtable;


/**
 *
 * @author Vincent Calame
 */
public class OdColumn {

    private final String styleName;
    private final OdCellKey defaultOdCellKey;
    private final CellStyle defaultCellStyle;

    public OdColumn(String styleName, OdCellKey defaultOdCellKey, CellStyle defaultCellStyle) {
        this.styleName = styleName;
        this.defaultOdCellKey = defaultOdCellKey;
        this.defaultCellStyle = defaultCellStyle;
    }

    public String getStyleName() {
        return styleName;
    }

    public OdCellKey getDefaultOdCellKey() {
        return defaultOdCellKey;
    }

    public CellStyle getDefaultCellStyle() {
        return defaultCellStyle;
    }

}
