/* OdLib_Io - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io.odtable;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import net.mapeadores.opendocument.io.SheetNameChecker;
import net.mapeadores.opendocument.io.SheetWriter;


/**
 *
 * @author Vincent Calame
 */
public class SheetHandler {

    private final SheetWriter sheetWriter;
    private final Map<String, String> checkedNameMap = new LinkedHashMap<String, String>();

    public SheetHandler(SheetWriter sheetWriter) {
        this.sheetWriter = sheetWriter;
    }

    public void addSupplementary(StyleManagerBuilder styleManagerBuilder, SheetNameChecker sheetNameChecker) {
        for (OdTableDef odTableDef : sheetWriter.getTableDefList()) {
            String originalName = odTableDef.getTableName();
            String checkedName = sheetNameChecker.checkName(originalName);
            styleManagerBuilder.putTableDef(checkedName, odTableDef);
            checkedNameMap.put(checkedName, originalName);
        }
    }

    public void writeSupplementary(OdsXMLPart xmlPart) throws IOException {
        for (Map.Entry<String, String> entry : checkedNameMap.entrySet()) {
            sheetWriter.writeTable(xmlPart, entry.getValue(), entry.getKey());
        }
    }

}
