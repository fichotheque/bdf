/* OdLib_Io - Copyright (c) 2007-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io;

import java.io.IOException;
import java.io.OutputStream;


/**
 * Interface désignant une source susceptible d'écrire une entrée du fichier Zip
 * final.
 *
 * @author Vincent Calame
 */
public interface OdSource {

    /**
     * Écrit le contenu de OdSource dans outputStream.
     *
     * @param outputStream flux de destination
     * @throws IOException
     */
    public void writeStream(OutputStream outputStream) throws IOException;

}
