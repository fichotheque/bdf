/* OdLib_Io - Copyright (c) 2008-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io;


import java.io.IOException;
import java.io.OutputStream;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import net.mapeadores.util.exceptions.NestedTransformerException;


/**
 *
 * @author Vincent Calame
 */
public class TransformerOdSource implements OdSource {

    private final Transformer transformer;
    private final Source transformerSource;

    public TransformerOdSource(Transformer transformer, Source transformerSource) {
        this.transformer = transformer;
        this.transformerSource = transformerSource;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        try {
            transformer.transform(transformerSource, new StreamResult(outputStream));
        } catch (TransformerException te) {
            Throwable thr = te.getCause();
            if ((thr != null) && (thr instanceof IOException)) {
                throw (IOException) thr;
            } else {
                throw new NestedTransformerException(te);
            }
        }
    }

}
