/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.selection.CroisementCondition;
import net.fichotheque.selection.RangeCondition;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class CroisementConditionBuilder {

    private final Set<String> modeSet = new LinkedHashSet<String>();
    private RangeCondition weightRangeCondition = null;

    public CroisementConditionBuilder() {
    }

    /**
     * Accepte la valeur _default qui est convertie en chaine vide.
     */
    public CroisementConditionBuilder addLienMode(String mode) {
        if (mode.equals("_default")) {
            mode = "";
        }
        modeSet.add(mode);
        return this;
    }

    public CroisementConditionBuilder setWeightRangeCondition(RangeCondition weightRangeCondition) {
        this.weightRangeCondition = weightRangeCondition;
        return this;
    }

    public boolean isEmpty() {
        return ((weightRangeCondition == null) && (modeSet.isEmpty()));
    }

    public CroisementCondition toCroisementCondition() {
        List<String> lienModeList;
        int size = modeSet.size();
        if (size > 0) {
            lienModeList = StringUtils.wrap(modeSet.toArray(new String[size]));
        } else {
            lienModeList = StringUtils.EMPTY_STRINGLIST;
        }
        return new InternalCroisementCondition(lienModeList, weightRangeCondition);
    }

    public static CroisementConditionBuilder init() {
        return new CroisementConditionBuilder();
    }


    private static class InternalCroisementCondition implements CroisementCondition {

        private final List<String> lienModeList;
        private final RangeCondition weightRangeCondition;

        private InternalCroisementCondition(List<String> lienModeList, RangeCondition weightRangeCondition) {
            this.lienModeList = lienModeList;
            this.weightRangeCondition = weightRangeCondition;
        }

        @Override
        public List<String> getLienModeList() {
            return lienModeList;
        }

        @Override
        public RangeCondition getWeightRangeCondition() {
            return weightRangeCondition;
        }

    }

}
