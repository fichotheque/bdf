/* BdfServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Predicate;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.selection.DocumentSelector;
import net.fichotheque.selection.FicheSelector;
import net.fichotheque.selection.MotcleSelector;
import net.fichotheque.selection.RedacteurSelector;
import net.fichotheque.selection.SelectionContext;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.Comparators;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public final class SelectionEngines {

    private SelectionEngines() {
    }

    public static Collection<FicheMeta> run(SelectionContext selectionContext, FicheSelector ficheSelector, @Nullable Comparator<FicheMeta> comparator) {
        Predicate<Subset> subsetAccessPredicate = selectionContext.getSubsetAccessPredicate();
        if (comparator == null) {
            List<FicheMeta> list = new ArrayList();
            for (Corpus corpus : ficheSelector.getCorpusList()) {
                if (subsetAccessPredicate.test(corpus)) {
                    for (FicheMeta ficheMeta : corpus.getFicheMetaList()) {
                        if (ficheSelector.test(ficheMeta)) {
                            list.add(ficheMeta);
                        }
                    }
                }
            }
            return list;
        } else {
            TreeSet<FicheMeta> treeset = new TreeSet<FicheMeta>(comparator);
            for (Corpus corpus : ficheSelector.getCorpusList()) {
                if (subsetAccessPredicate.test(corpus)) {
                    for (FicheMeta ficheMeta : corpus.getFicheMetaList()) {
                        if (ficheSelector.test(ficheMeta)) {
                            treeset.add(ficheMeta);
                        }
                    }
                }
            }
            return treeset;
        }
    }

    public static Collection<Motcle> run(SelectionContext selectionContext, SubsetKey shouldNotCroisementSubsetKey, MotcleSelector motcleSelector, Comparator<Motcle> comparator) {
        Predicate<Subset> subsetAccessPredicate = selectionContext.getSubsetAccessPredicate();
        if (comparator != null) {
            TreeSet<Motcle> treeSet = new TreeSet<Motcle>(comparator);
            for (Thesaurus thesaurus : motcleSelector.getThesaurusList()) {
                if (subsetAccessPredicate.test(thesaurus)) {
                    for (Motcle motcle : thesaurus.getMotcleList()) {
                        if ((!shouldNotTest(motcle, shouldNotCroisementSubsetKey)) && (motcleSelector.test(motcle))) {
                            treeSet.add(motcle);
                        }
                    }
                }
            }
            return treeSet;
        } else {
            List<Motcle> result = new ArrayList<Motcle>();
            for (Thesaurus thesaurus : motcleSelector.getThesaurusList()) {
                if (subsetAccessPredicate.test(thesaurus)) {
                    scanMotcleList(thesaurus.getFirstLevelList(), result, shouldNotCroisementSubsetKey, motcleSelector);
                }
            }
            return result;
        }
    }

    private static void scanMotcleList(List<Motcle> list, List<Motcle> result, SubsetKey shouldNotCroisementSubsetKey, MotcleSelector motcleSelector) {
        for (Motcle motcle : list) {
            if ((!shouldNotTest(motcle, shouldNotCroisementSubsetKey)) && (motcleSelector.test(motcle))) {
                result.add(motcle);
            }
            scanMotcleList(motcle.getChildList(), result, shouldNotCroisementSubsetKey, motcleSelector);
        }
    }


    public static Collection<Document> run(SelectionContext selectionContext, DocumentSelector documentSelector) {
        Predicate<Subset> subsetAccessPredicate = selectionContext.getSubsetAccessPredicate();
        TreeMap<String, Document> documentMap = new TreeMap<String, Document>();
        for (Addenda addenda : documentSelector.getAddendaList()) {
            if (subsetAccessPredicate.test(addenda)) {
                for (Document document : addenda.getDocumentList()) {
                    if (documentSelector.test(document)) {
                        documentMap.put(document.getBasename(), document);
                    }
                }
            }
        }
        return documentMap.values();
    }

    public static Collection<Redacteur> run(SelectionContext selectionContext, RedacteurSelector redacteurSelector) {
        Predicate<Subset> subsetAccessPredicate = selectionContext.getSubsetAccessPredicate();
        TreeSet<Redacteur> redacSet = new TreeSet<Redacteur>(Comparators.REDACTEUR);
        for (Sphere sphere : redacteurSelector.getSphereList()) {
            if (subsetAccessPredicate.test(sphere)) {
                for (Redacteur redacteur : sphere.getRedacteurList()) {
                    if (redacteurSelector.test(redacteur)) {
                        redacSet.add(redacteur);
                    }
                }
            }
        }
        return redacSet;
    }

    private static boolean shouldNotTest(Motcle motcle, SubsetKey shouldNotCroisementSubsetKey) {
        if (shouldNotCroisementSubsetKey == null) {
            return false;
        }
        return motcle.shouldNotCroisement(shouldNotCroisementSubsetKey);
    }

}
