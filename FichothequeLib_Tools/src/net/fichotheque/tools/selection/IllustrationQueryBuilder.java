/* FichothequeLib_Tools - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.fichotheque.album.Album;
import net.fichotheque.selection.IllustrationQuery;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationQueryBuilder {

    private final Set<SubsetKey> albumKeySet = new LinkedHashSet();
    private final Set<String> dimNameSet = new LinkedHashSet();

    public IllustrationQueryBuilder() {
    }

    public IllustrationQueryBuilder addAlbum(Object object) {
        if (object == null) {
            return this;
        }
        if (object instanceof Album) {
            albumKeySet.add(((Album) object).getSubsetKey());
        } else if (object instanceof SubsetKey) {
            SubsetKey subsetKey = (SubsetKey) object;
            if (subsetKey.isAlbumSubset()) {
                albumKeySet.add(subsetKey);
            }
        } else if (object instanceof Collection) {
            for (Object obj : ((Collection) object)) {
                if (obj != null) {
                    if (obj instanceof Album) {
                        albumKeySet.add(((Album) obj).getSubsetKey());
                    } else if (obj instanceof SubsetKey) {
                        SubsetKey subsetKey = (SubsetKey) obj;
                        if (subsetKey.isAlbumSubset()) {
                            albumKeySet.add(subsetKey);
                        }
                    }
                }
            }
        } else if (object instanceof SubsetKey[]) {
            for (SubsetKey subsetKey : ((SubsetKey[]) object)) {
                if ((subsetKey != null) && (subsetKey.isAlbumSubset())) {
                    albumKeySet.add(subsetKey);
                }
            }
        }
        return this;
    }

    public IllustrationQueryBuilder addAlbumDimName(String albumDimName) {
        dimNameSet.add(albumDimName);
        return this;
    }

    public IllustrationQuery toIllustrationQuery() {
        List<SubsetKey> subsetKeyList = FichothequeUtils.wrap(albumKeySet.toArray(new SubsetKey[albumKeySet.size()]));
        List<String> dimNameList = StringUtils.toList(dimNameSet);
        return new InternalIllustrationQuery(subsetKeyList, dimNameList);
    }

    public static IllustrationQueryBuilder init() {
        return new IllustrationQueryBuilder();
    }


    private static class InternalIllustrationQuery implements IllustrationQuery {

        private final List<SubsetKey> albumKeyList;
        private final List<String> dimNameList;

        private InternalIllustrationQuery(List<SubsetKey> subsetKeyList, List<String> dimNameList) {
            this.albumKeyList = subsetKeyList;
            this.dimNameList = dimNameList;
        }

        @Override
        public List<SubsetKey> getAlbumKeyList() {
            return albumKeyList;
        }

        @Override
        public List<String> getAlbumDimNameList() {
            return dimNameList;
        }

    }

}
