/* FichothequeLib_Tools - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.selection.SelectionDef;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class SelectionDefBuilder extends DefBuilder {

    private final String name;
    private final FichothequeQueriesBuilder fichothequeQueriesBuilder = new FichothequeQueriesBuilder();

    public SelectionDefBuilder(String name) {
        this(name, null);
    }

    public SelectionDefBuilder(String name, Attributes initAttributes) {
        super(initAttributes);
        if (!StringUtils.isTechnicalName(name, true)) {
            throw new IllegalArgumentException("Wrong syntax");
        }
        this.name = name;
    }

    public FichothequeQueriesBuilder getFichothequeQueriesBuilder() {
        return fichothequeQueriesBuilder;
    }

    public SelectionDef toSelectionDef() {
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        return new InternalSelectionDef(name, titleLabels, attributes, fichothequeQueriesBuilder.toFichothequeQueries());
    }

    public static SelectionDefBuilder init(String name) {
        return new SelectionDefBuilder(name);
    }

    public static SelectionDefBuilder init(String name, Attributes initAttributes) {
        return new SelectionDefBuilder(name, initAttributes);
    }


    private static class InternalSelectionDef implements SelectionDef {

        private final String name;
        private final Labels titleLabels;
        private final Attributes attributes;
        private final FichothequeQueries fichothequeQueries;


        private InternalSelectionDef(String name, Labels titleLabels, Attributes attributes, FichothequeQueries fichothequeQueries) {
            this.name = name;
            this.fichothequeQueries = fichothequeQueries;
            this.titleLabels = titleLabels;
            this.attributes = attributes;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public FichothequeQueries getFichothequeQueries() {
            return fichothequeQueries;
        }

    }

}
