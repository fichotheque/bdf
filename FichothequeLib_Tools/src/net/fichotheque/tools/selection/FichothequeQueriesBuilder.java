/* FichothequeLib_Tools - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.selection.MotcleQuery;
import net.fichotheque.utils.SelectionUtils;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeQueriesBuilder {

    private final List<FicheQuery> ficheQueryList = new ArrayList<FicheQuery>();
    private final List<MotcleQuery> motcleQueryList = new ArrayList<MotcleQuery>();

    public FichothequeQueriesBuilder() {

    }

    public FichothequeQueriesBuilder addFichothequeQueries(FichothequeQueries fichothequeQueries) {
        for (FicheQuery ficheQuery : fichothequeQueries.getFicheQueryList()) {
            addFicheQuery(ficheQuery);
        }
        for (MotcleQuery motcleQuery : fichothequeQueries.getMotcleQueryList()) {
            addMotcleQuery(motcleQuery);
        }
        return this;
    }

    public FichothequeQueriesBuilder addFicheQuery(FicheQuery ficheQuery) {
        if (!ficheQuery.isEmpty()) {
            ficheQueryList.add(ficheQuery);
        }
        return this;
    }

    public FichothequeQueriesBuilder addMotcleQuery(MotcleQuery motcleQuery) {
        if (!motcleQuery.isEmpty()) {
            motcleQueryList.add(motcleQuery);
        }
        return this;
    }

    public FichothequeQueries toFichothequeQueries() {
        List<FicheQuery> immutableFicheQueryList = SelectionUtils.wrap(ficheQueryList.toArray(new FicheQuery[ficheQueryList.size()]));
        List<MotcleQuery> immutableMotcleQueryList = SelectionUtils.wrap(motcleQueryList.toArray(new MotcleQuery[motcleQueryList.size()]));
        return new InternalFichothequeQueries(immutableFicheQueryList, immutableMotcleQueryList);
    }

    public static FichothequeQueriesBuilder init() {
        return new FichothequeQueriesBuilder();
    }


    private static class InternalFichothequeQueries implements FichothequeQueries {

        private final List<FicheQuery> ficheQueryList;
        private final List<MotcleQuery> motcleQueryList;

        private InternalFichothequeQueries(List<FicheQuery> ficheQueryList, List<MotcleQuery> motcleQueryList) {
            this.ficheQueryList = ficheQueryList;
            this.motcleQueryList = motcleQueryList;
        }

        @Override
        public List<FicheQuery> getFicheQueryList() {
            return ficheQueryList;
        }

        @Override
        public List<MotcleQuery> getMotcleQueryList() {
            return motcleQueryList;
        }

    }

}
