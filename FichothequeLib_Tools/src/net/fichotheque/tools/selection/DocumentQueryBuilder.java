/* FichothequeLib_Tools - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.selection.DocumentQuery;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
public class DocumentQueryBuilder {

    private final Set<SubsetKey> addendaKeySet = new LinkedHashSet<SubsetKey>();
    private TextCondition nameCondition;

    public DocumentQueryBuilder() {
    }

    public DocumentQueryBuilder addAddenda(Object object) {
        if (object == null) {
            return this;
        }
        if (object instanceof Addenda) {
            addendaKeySet.add(((Addenda) object).getSubsetKey());
        } else if (object instanceof SubsetKey) {
            SubsetKey subsetKey = (SubsetKey) object;
            if (subsetKey.isAddendaSubset()) {
                addendaKeySet.add(subsetKey);
            }
        } else if (object instanceof Collection) {
            for (Object obj : ((Collection) object)) {
                if (obj != null) {
                    if (obj instanceof Addenda) {
                        addendaKeySet.add(((Addenda) obj).getSubsetKey());
                    } else if (obj instanceof SubsetKey) {
                        SubsetKey subsetKey = (SubsetKey) obj;
                        if (subsetKey.isAddendaSubset()) {
                            addendaKeySet.add(subsetKey);
                        }
                    }
                }
            }
        } else if (object instanceof SubsetKey[]) {
            for (SubsetKey subsetKey : ((SubsetKey[]) object)) {
                if ((subsetKey != null) && (subsetKey.isAddendaSubset())) {
                    addendaKeySet.add(subsetKey);
                }
            }
        }
        return this;
    }

    public DocumentQueryBuilder setNameCondition(TextCondition nameCondition) {
        this.nameCondition = nameCondition;
        return this;
    }

    public DocumentQuery toDocumentQuery() {
        List<SubsetKey> subsetKeyList = FichothequeUtils.toList(addendaKeySet);
        return new InternalDocumentQuery(subsetKeyList, nameCondition);
    }

    public static DocumentQueryBuilder init() {
        return new DocumentQueryBuilder();
    }


    private static class InternalDocumentQuery implements DocumentQuery {

        private final List<SubsetKey> addendaKeyList;
        private final TextCondition nameCondition;

        private InternalDocumentQuery(List<SubsetKey> subsetKeyList, TextCondition nameCondition) {
            this.addendaKeyList = subsetKeyList;
            this.nameCondition = nameCondition;
        }

        @Override
        public List<SubsetKey> getAddendaKeyList() {
            return addendaKeyList;
        }

        @Override
        public TextCondition getNameCondition() {
            return nameCondition;
        }

    }

}
