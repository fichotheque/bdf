/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import net.fichotheque.selection.FichothequeQueries;
import net.fichotheque.selection.SelectionOptions;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class SelectionOptionsBuilder {

    private final FichothequeQueriesBuilder customFichothequeQueriesBuilder = new FichothequeQueriesBuilder();
    private String selectionDefName = "";

    public SelectionOptionsBuilder() {

    }

    public FichothequeQueriesBuilder getCustomFichothequeQueriesBuilder() {
        return customFichothequeQueriesBuilder;
    }

    public SelectionOptionsBuilder setSelectionDefName(String selectionDefName) {
        this.selectionDefName = StringUtils.nullTrim(selectionDefName);
        return this;
    }

    public SelectionOptions toSelectionOptions() {
        return new InternalSelectionOptions(selectionDefName, customFichothequeQueriesBuilder.toFichothequeQueries());
    }


    private static class InternalSelectionOptions implements SelectionOptions {

        private final String selectionDefName;
        private final FichothequeQueries customFichothequeQueries;

        private InternalSelectionOptions(String selectionDefName, FichothequeQueries customFichothequeQueries) {
            this.selectionDefName = selectionDefName;
            this.customFichothequeQueries = customFichothequeQueries;
        }

        @Override
        public String getSelectionDefName() {
            return selectionDefName;
        }

        @Override
        public FichothequeQueries getCustomFichothequeQueries() {
            return customFichothequeQueries;
        }

    }

}
