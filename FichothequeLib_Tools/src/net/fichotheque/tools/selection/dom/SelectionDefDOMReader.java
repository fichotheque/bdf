/* FichothequeLib_Tools - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.tools.selection.SelectionDOMUtils;
import net.fichotheque.tools.selection.SelectionDefBuilder;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class SelectionDefDOMReader {

    private final Fichotheque fichotheque;
    private final SelectionDefBuilder selectionDefBuilder;
    private final MessageHandler messageHandler;

    public SelectionDefDOMReader(Fichotheque fichotheque, SelectionDefBuilder selectionDefBuilder, MessageHandler messageHandler) {
        this.fichotheque = fichotheque;
        this.selectionDefBuilder = selectionDefBuilder;
        this.messageHandler = messageHandler;
    }

    public SelectionDefDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static SelectionDefDOMReader init(Fichotheque fichotheque, SelectionDefBuilder selectionDefBuilder, MessageHandler messageHandler) {
        return new SelectionDefDOMReader(fichotheque, selectionDefBuilder, messageHandler);
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {

        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "label":
                    try {
                    LabelUtils.readLabel(element, selectionDefBuilder);
                } catch (ParseException ile) {
                    DomMessages.wrongLangAttribute(messageHandler, tagName, element.getAttribute("xml:lang"));
                }
                break;
                case "attr":
                    AttributeUtils.readAttrElement(selectionDefBuilder.getAttributesBuilder(), element);
                    break;
                default:
                    if (!SelectionDOMUtils.readQueryElement(selectionDefBuilder.getFichothequeQueriesBuilder(), fichotheque, element)) {
                        DomMessages.unknownTagWarning(messageHandler, tagName);
                    }
            }
        }

    }

}
