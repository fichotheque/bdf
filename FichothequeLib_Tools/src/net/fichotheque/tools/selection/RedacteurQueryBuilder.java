/* FichothequeLib_Tools - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.selection;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.fichotheque.selection.RedacteurQuery;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.conditions.TextCondition;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurQueryBuilder {

    private final Set<SubsetKey> sphereKeySet = new LinkedHashSet<SubsetKey>();
    private TextCondition nomcompletCondition;

    public RedacteurQueryBuilder() {

    }

    public RedacteurQueryBuilder addSphere(Object object) {
        if (object == null) {
            return this;
        }
        if (object instanceof Sphere) {
            sphereKeySet.add(((Sphere) object).getSubsetKey());
        } else if (object instanceof SubsetKey) {
            SubsetKey subsetKey = (SubsetKey) object;
            if (subsetKey.isSphereSubset()) {
                sphereKeySet.add(subsetKey);
            }
        } else if (object instanceof Collection) {
            for (Object obj : ((Collection) object)) {
                if (obj != null) {
                    if (obj instanceof Sphere) {
                        sphereKeySet.add(((Sphere) obj).getSubsetKey());
                    } else if (obj instanceof SubsetKey) {
                        SubsetKey subsetKey = (SubsetKey) obj;
                        if (subsetKey.isSphereSubset()) {
                            sphereKeySet.add(subsetKey);
                        }
                    }
                }
            }
        } else if (object instanceof SubsetKey[]) {
            for (SubsetKey subsetKey : ((SubsetKey[]) object)) {
                if ((subsetKey != null) && (subsetKey.isSphereSubset())) {
                    sphereKeySet.add(subsetKey);
                }
            }
        }
        return this;
    }

    public RedacteurQueryBuilder setNomcompletCondition(TextCondition nomcompletCondition) {
        this.nomcompletCondition = nomcompletCondition;
        return this;
    }

    public RedacteurQuery toRedacteurQuery() {
        List<SubsetKey> subsetKeyList = FichothequeUtils.toList(sphereKeySet);
        return new InternalRedacteurQuery(subsetKeyList, nomcompletCondition);
    }

    public static RedacteurQueryBuilder init() {
        return new RedacteurQueryBuilder();
    }


    private static class InternalRedacteurQuery implements RedacteurQuery {

        private final List<SubsetKey> sphereKeyList;
        private final TextCondition nomcompletCondition;

        private InternalRedacteurQuery(List<SubsetKey> sphereKeyList, TextCondition nomcompletCondition) {
            this.sphereKeyList = sphereKeyList;
            this.nomcompletCondition = nomcompletCondition;
        }

        @Override
        public List<SubsetKey> getSphereKeyList() {
            return sphereKeyList;
        }

        @Override
        public TextCondition getNomcompletCondition() {
            return nomcompletCondition;
        }

    }

}
