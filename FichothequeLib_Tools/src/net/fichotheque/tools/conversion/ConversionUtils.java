/* FichothequeLib_Tools - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.conversion;

import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.fiche.Link;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.fiche.Nombre;
import net.fichotheque.corpus.fiche.P;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Pays;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.corpus.fiche.S;
import net.mapeadores.util.models.EmailCore;


/**
 *
 * @author Vincent Calame
 */
public final class ConversionUtils {

    private ConversionUtils() {
    }

    public static FicheBlock toFicheBlock(FicheItem ficheItem) {
        P prg = new P();
        if (ficheItem instanceof Item) {
            prg.addText(((Item) ficheItem).getValue());
        } else if (ficheItem instanceof Para) {
            for (Object obj : (Para) ficheItem) {
                if (obj instanceof String) {
                    prg.addText((String) obj);
                } else if (obj instanceof S) {
                    prg.addS((S) obj);
                }
            }
        } else if (ficheItem instanceof Pays) {
            prg.addText(((Pays) ficheItem).getCountry().toString());
        } else if (ficheItem instanceof Langue) {
            prg.addText(((Langue) ficheItem).getLang().toString());
        } else if (ficheItem instanceof Datation) {
            prg.addText(((Datation) ficheItem).getDate().toISOString());
        } else if (ficheItem instanceof Geopoint) {
            Geopoint geopoint = (Geopoint) ficheItem;
            prg.addText(geopoint.getLatitude().toString() + " " + geopoint.getLongitude().toString());
        } else if (ficheItem instanceof Image) {
            Image image = (Image) ficheItem;
            S span = new S(S.IMAGE);
            span.setRef(image.getSrc());
            span.setValue(image.getAlt());
            prg.addS(span);
        } else if (ficheItem instanceof Link) {
            Link link = (Link) ficheItem;
            S span = new S(S.LINK);
            span.setRef(link.getHref());
            span.setValue(link.getTitle());
            prg.addS(span);
        } else if (ficheItem instanceof Courriel) {
            EmailCore emailCore = ((Courriel) ficheItem).getEmailCore();
            S span = new S(S.LINK);
            span.setRef("mailto:" + emailCore.getAddrSpec());
            span.setValue(emailCore.getRealName());
            prg.addS(span);
        } else if (ficheItem instanceof Montant) {
            Montant montant = (Montant) ficheItem;
            prg.addText(montant.getCurrency().getCurrencyCode() + " " + montant.getDecimal().toString());
        } else if (ficheItem instanceof Nombre) {
            prg.addText(((Nombre) ficheItem).getDecimal().toString());
        } else if (ficheItem instanceof Personne) {
            Personne personne = (Personne) ficheItem;
            String redacteurGlobalId = personne.getRedacteurGlobalId();
            if (redacteurGlobalId != null) {
                prg.addText(redacteurGlobalId);
            } else {
                prg.addText(personne.getPersonCore().toStandardStyle());
            }
        } else {
            prg.addText("Unknown : " + ficheItem.getClass().getName());
        }
        return prg;
    }

}
