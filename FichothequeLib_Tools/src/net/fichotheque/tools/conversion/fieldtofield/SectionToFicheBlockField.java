/* FichothequeLib_Tools - Copyright (c) 2008-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.conversion.fieldtofield;

import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.logging.MultiMessageHandler;


/**
 *
 * @author Vincent Calame
 */
class SectionToFicheBlockField extends FieldToFieldConverter {

    private final FieldKey sourceKey;
    private final FieldKey destinationKey;

    SectionToFicheBlockField(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        super(messageHandler);
        this.sourceKey = sourceKey;
        this.destinationKey = destinationKey;
    }

    @Override
    void convertContent(Fiche fiche, boolean removeOldField) {
        FicheBlocks ficheBlocks = fiche.getSection(sourceKey);
        if ((ficheBlocks == null) || (ficheBlocks.size() == 0)) {
            return;
        }
        fiche.appendSection(destinationKey, ficheBlocks);
        if (removeOldField) {
            fiche.setSection(sourceKey, null);
        }
    }

}
