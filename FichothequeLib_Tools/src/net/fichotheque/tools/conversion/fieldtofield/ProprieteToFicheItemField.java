/* FichothequeLib_Tools - Copyright (c) 2008-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.conversion.fieldtofield;

import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Propriete;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.logging.MultiMessageHandler;


/**
 *
 * @author Vincent Calame
 */
class ProprieteToFicheItemField extends FieldToFieldConverter {

    private final FieldKey sourceKey;
    private final FieldKey destinationKey;

    ProprieteToFicheItemField(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        super(messageHandler);
        this.sourceKey = sourceKey;
        this.destinationKey = destinationKey;
    }

    @Override
    void convertContent(Fiche fiche, boolean removeOldField) {
        Propriete source = fiche.getPropriete(sourceKey);
        if (source != null) {
            FicheItem ficheItem = source.getFicheItem();
            if (destinationKey.isPropriete()) {
                fiche.setPropriete(destinationKey, ficheItem);
            } else {
                fiche.appendInformation(destinationKey, FicheUtils.toFicheItems(ficheItem));
            }
            if (removeOldField) {
                fiche.setPropriete(sourceKey, null);
            }
        }

    }

}
