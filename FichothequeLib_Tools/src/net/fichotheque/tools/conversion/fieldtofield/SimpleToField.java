/* FichothequeLib_Tools - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.conversion.fieldtofield;

import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.tools.conversion.ConversionUtils;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MultiMessageHandler;


/**
 *
 * @author Vincent Calame
 */
class SimpleToField extends FieldToFieldConverter {

    private final FieldKey destinationKey;
    private final FieldKey sourceKey;

    SimpleToField(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        super(messageHandler);
        this.destinationKey = destinationKey;
        this.sourceKey = sourceKey;
    }

    @Override
    void convertContent(Fiche fiche, boolean removeOldField) {
        FicheItem ficheItem = getFicheItem(fiche);
        if (ficheItem == null) {
            return;
        }
        switch (destinationKey.getCategory()) {
            case FieldKey.PROPRIETE_CATEGORY:
                fiche.setPropriete(destinationKey, ficheItem);
                break;
            case FieldKey.INFORMATION_CATEGORY:
                fiche.appendInformation(destinationKey, FicheUtils.toFicheItems(ficheItem));
                break;
            case FieldKey.SECTION_CATEGORY:
                fiche.appendSection(destinationKey, FicheUtils.toFicheBlocks(ConversionUtils.toFicheBlock(ficheItem)));
                break;
        }
        if (removeOldField) {
            switch (sourceKey.getKeyString()) {
                case FieldKey.SPECIAL_TITRE:
                    fiche.setTitre("");
                    break;
                case FieldKey.SPECIAL_SOUSTITRE:
                    fiche.setSoustitre(null);
                    break;
            }
        }
    }

    private FicheItem getFicheItem(Fiche fiche) {
        switch (sourceKey.getKeyString()) {
            case FieldKey.SPECIAL_TITRE:
                return getItem(fiche.getTitre());
            case FieldKey.SPECIAL_SOUSTITRE:
                return fiche.getSoustitre();
            case FieldKey.SPECIAL_LANG:
                Lang lang = fiche.getLang();
                if (lang != null) {
                    return new Langue(lang);
                } else {
                    return null;
                }
            default:
                throw new SwitchException("sourceKey = " + sourceKey.getKeyString());
        }
    }

    private Item getItem(String s) {
        if (s.length() == 0) {
            return null;
        }
        return new Item(s);
    }

}
