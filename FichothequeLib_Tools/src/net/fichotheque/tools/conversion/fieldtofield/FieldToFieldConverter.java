/* FichothequeLib_Tools - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.conversion.fieldtofield;

import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.logging.MultiMessageHandler;


/**
 *
 * @author Vincent Calame
 */
public abstract class FieldToFieldConverter {

    protected final MultiMessageHandler messageHandler;

    protected FieldToFieldConverter(MultiMessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    protected void addWarning(String messageKey, Object messageValue) {
        messageHandler.addMessage("warning.conversion", LocalisationUtils.toMessage(messageKey, messageValue));
    }

    public static FieldToFieldConverter getInstance(CorpusField source, CorpusField destination, MultiMessageHandler messageHandler) {
        return getInstance(source.getFieldKey(), destination.getFieldKey(), messageHandler);
    }

    public static FieldToFieldConverter getInstance(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        switch (sourceKey.getCategory()) {
            case FieldKey.PROPRIETE_CATEGORY:
                return getProprieteInstance(sourceKey, destinationKey, messageHandler);
            case FieldKey.INFORMATION_CATEGORY:
                return getInformationInstance(sourceKey, destinationKey, messageHandler);
            case FieldKey.SECTION_CATEGORY:
                return getSectionInstance(sourceKey, destinationKey, messageHandler);
            case FieldKey.SPECIAL_CATEGORY:
                switch (sourceKey.getKeyString()) {
                    case FieldKey.SPECIAL_REDACTEURS:
                        return getInformationInstance(sourceKey, destinationKey, messageHandler);
                    default:
                        return new SimpleToField(sourceKey, destinationKey, messageHandler);
                }
            default:
                throw new SwitchException("Unknown category = " + sourceKey.getCategory());
        }
    }


    public void convert(FicheMeta ficheMeta, Fiche fiche) {
        convert(ficheMeta, fiche, false);
    }

    public void convert(FicheMeta ficheMeta, Fiche fiche, boolean removeOldField) {
        String uri = ficheMeta.getGlobalId();
        messageHandler.setCurrentSource(uri);
        convertContent(fiche, removeOldField);
    }

    abstract void convertContent(Fiche fiche, boolean removeOldField);

    private static boolean isFicheItemDestination(FieldKey destinationKey) {
        switch (destinationKey.getCategory()) {
            case FieldKey.PROPRIETE_CATEGORY:
            case FieldKey.INFORMATION_CATEGORY:
                return true;
            case FieldKey.SECTION_CATEGORY:
                return false;
            default:
                throw new IllegalArgumentException("wrong destination : " + destinationKey.getKeyString());
        }
    }

    private static FieldToFieldConverter getProprieteInstance(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        if (isFicheItemDestination(destinationKey)) {
            return new ProprieteToFicheItemField(sourceKey, destinationKey, messageHandler);
        } else {
            return new FicheItemFieldToFicheBlockField(sourceKey, destinationKey, messageHandler);
        }
    }

    private static FieldToFieldConverter getInformationInstance(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        if (isFicheItemDestination(destinationKey)) {
            return new InformationToFicheItemField(sourceKey, destinationKey, messageHandler);
        } else {
            return new FicheItemFieldToFicheBlockField(sourceKey, destinationKey, messageHandler);
        }
    }

    private static FieldToFieldConverter getSectionInstance(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        if (isFicheItemDestination(destinationKey)) {
            return new SectionToFicheItemField(sourceKey, destinationKey, messageHandler);
        } else {
            return new SectionToFicheBlockField(sourceKey, destinationKey, messageHandler);
        }
    }

}
