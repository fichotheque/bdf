/* FichothequeLib_Tools - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.conversion.fieldtofield;

import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Information;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.logging.MultiMessageHandler;


/**
 *
 * @author Vincent Calame
 */
class InformationToFicheItemField extends FieldToFieldConverter {

    private final FieldKey sourceKey;
    private final FieldKey destinationKey;

    InformationToFicheItemField(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        super(messageHandler);
        this.sourceKey = sourceKey;
        this.destinationKey = destinationKey;
    }

    @Override
    void convertContent(Fiche fiche, boolean removeOldField) {
        Information source = fiche.getInformation(sourceKey);
        if ((source == null) || (source.isEmpty())) {
            return;
        }
        if (destinationKey.isPropriete()) {
            FicheItem ficheItem = source.get(0);
            fiche.setPropriete(destinationKey, ficheItem);
            if (source.size() > 1) {
                addWarning("_ warning.conversion.manyficheitems", sourceKey.getKeyString());
            }
        } else {
            fiche.appendInformation(destinationKey, source);
        }
        if (removeOldField) {
            fiche.setInformation(sourceKey, null);
        }
    }

}
