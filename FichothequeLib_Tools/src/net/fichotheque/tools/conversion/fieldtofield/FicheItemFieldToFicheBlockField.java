/* FichothequeLib_Tools - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.conversion.fieldtofield;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Information;
import net.fichotheque.corpus.fiche.Propriete;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.tools.conversion.ConversionUtils;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.logging.MultiMessageHandler;


/**
 *
 * @author Vincent Calame
 */
class FicheItemFieldToFicheBlockField extends FieldToFieldConverter {

    private final FieldKey sourceKey;
    private final FieldKey destinationKey;

    FicheItemFieldToFicheBlockField(FieldKey sourceKey, FieldKey destinationKey, MultiMessageHandler messageHandler) {
        super(messageHandler);
        this.sourceKey = sourceKey;
        this.destinationKey = destinationKey;
    }

    @Override
    void convertContent(Fiche fiche, boolean removeOldField) {
        FicheBlocks list;
        if (sourceKey.isPropriete()) {
            list = getProprieteFicheBlocks(fiche);
        } else {
            list = getInformationFicheBlocks(fiche);
        }
        if (list.isEmpty()) {
            return;
        }
        fiche.appendSection(destinationKey, list);
        if (removeOldField) {
            if (sourceKey.isPropriete()) {
                fiche.setPropriete(sourceKey, null);
            } else {
                fiche.setInformation(sourceKey, null);
            }
        }
    }

    private FicheBlocks getProprieteFicheBlocks(Fiche fiche) {
        Propriete propriete = fiche.getPropriete(sourceKey);
        if (propriete == null) {
            return FicheUtils.EMPTY_FICHEBLOCKS;
        }
        FicheItem ficheItem = propriete.getFicheItem();
        if (ficheItem == null) {
            return FicheUtils.EMPTY_FICHEBLOCKS;
        }
        return FicheUtils.toFicheBlocks(ConversionUtils.toFicheBlock(ficheItem));
    }

    private FicheBlocks getInformationFicheBlocks(Fiche fiche) {
        Information information = fiche.getInformation(sourceKey);
        if (information == null) {
            return FicheUtils.EMPTY_FICHEBLOCKS;
        }
        List<FicheBlock> list = new ArrayList<FicheBlock>();
        for (FicheItem ficheItem : information) {
            FicheBlock ficheBlock = ConversionUtils.toFicheBlock(ficheItem);
            if (ficheBlock != null) {
                list.add(ficheBlock);
            }
        }
        return FicheUtils.toFicheBlocks(list);
    }

}
