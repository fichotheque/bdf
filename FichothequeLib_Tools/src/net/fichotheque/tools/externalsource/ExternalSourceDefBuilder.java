/* FichothequeLib_Tools - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.externalsource;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import net.fichotheque.externalsource.ExternalSourceDef;


/**
 *
 * @author Vincent Calame
 */
public class ExternalSourceDefBuilder {

    private final String type;
    private final Map<String, String> paramMap = new LinkedHashMap<String, String>();

    public ExternalSourceDefBuilder(String type) {
        this.type = type;
    }

    public ExternalSourceDefBuilder addParam(String name, String value) {
        if (value == null) {
            paramMap.remove(name);
        } else {
            paramMap.put(name, value);
        }
        return this;
    }

    public ExternalSourceDef toExternalSourceDef() {
        Map<String, String> finalParamMap = new LinkedHashMap<String, String>(paramMap);
        return new InternalExternalSourceDef(type, finalParamMap);
    }

    public static ExternalSourceDefBuilder init(String type) {
        return new ExternalSourceDefBuilder(type);
    }


    private static class InternalExternalSourceDef implements ExternalSourceDef {

        private final String type;
        private final Map<String, String> paramMap;

        private InternalExternalSourceDef(String type, Map<String, String> paramMap) {
            this.type = type;
            this.paramMap = paramMap;
        }

        @Override
        public String getType() {
            return type;
        }

        @Override
        public Set<String> getParamNameSet() {
            return Collections.unmodifiableSet(paramMap.keySet());
        }

        @Override
        public String getParam(String name) {
            return paramMap.get(name);
        }

    }

}
