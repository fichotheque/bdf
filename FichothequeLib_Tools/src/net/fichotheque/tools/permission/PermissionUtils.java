/* FichothequeLib_Tools - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.permission;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.permission.PermissionEntry;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.utils.EligibilityUtils;


/**
 *
 * @author Vincent Calame
 */
public final class PermissionUtils {

    public final static PermissionSummary FICHOTHEQUEADMIN_PERMISSIONSUMMARY = new FichothequeAdminPermissionSummary();

    private PermissionUtils() {

    }

    public static Predicate<FicheMeta> getFichePredicate(PermissionSummary summary) {
        if (summary.isFichothequeAdmin()) {
            return EligibilityUtils.ALL_FICHE_PREDICATE;
        }
        return new RedacteurFichePredicate(summary);
    }

    public static boolean croisementTest(SubsetItem subsetItem, PermissionSummary permissionSummary, boolean read) {
        List<PermissionEntry> permissionEntryList = ListFilterEngine.filter(subsetItem.getSubset(), Collections.singletonList(subsetItem), permissionSummary);
        boolean notEmpty = !permissionEntryList.isEmpty();
        if (read) {
            return (notEmpty);
        } else {
            return ((notEmpty) && (permissionEntryList.get(0).isEditable()));
        }
    }


    private static class RedacteurFichePredicate implements Predicate<FicheMeta> {

        private final PermissionSummary permissionSummary;

        private RedacteurFichePredicate(PermissionSummary permissionSummary) {
            this.permissionSummary = permissionSummary;
        }

        @Override
        public boolean test(FicheMeta ficheMeta) {
            return permissionSummary.canRead(ficheMeta);
        }

    }


    private static class FichothequeAdminPermissionSummary implements PermissionSummary {

        private FichothequeAdminPermissionSummary() {

        }

        @Override
        public int getReadLevel(SubsetKey subsetKey) {
            return PermissionSummary.SUMMARYLEVEL_5_ADMIN;
        }

        @Override
        public int getWriteLevel(SubsetKey subsetKey) {
            return PermissionSummary.SUMMARYLEVEL_5_ADMIN;
        }

        @Override
        public boolean canCreate(SubsetKey subsetKey) {
            return true;
        }

        @Override
        public boolean isSubsetAdmin(SubsetKey subsetKey) {
            return true;
        }

        @Override
        public boolean isFichothequeAdmin() {
            return true;
        }

        @Override
        public boolean hasRole(String roleName) {
            return true;
        }

        @Override
        public boolean canDo(String actionName) {
            return true;
        }

        @Override
        public Predicate<Subset> getSubsetAccessPredicate() {
            return EligibilityUtils.ALL_SUBSET_PREDICATE;
        }

        @Override
        public boolean canRead(FicheMeta ficheMeta) {
            return true;
        }

        @Override
        public boolean canWrite(FicheMeta ficheMeta) {
            return true;
        }

    }

}
