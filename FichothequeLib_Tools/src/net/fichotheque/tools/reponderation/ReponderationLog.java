/* FichothequeLib_Tools - Copyright (c) 2012-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.reponderation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public class ReponderationLog {

    public static final String ORIGIN_UNKNOWNSUBSET_ERROR = "origin_unknownsubset";
    public static final String CROISEMENT_UNKNOWNSUBSET_ERROR = "croisement_unknownsubset";
    public static final String MISSING_ORIGIN_ID_ERROR = "missing_origin_id_error";
    private final List<ReponderationError> errorList = new ArrayList<ReponderationError>();
    private final Map<String, ReponderationError> errorMap = new HashMap<String, ReponderationError>();
    private final List<ReponderationResult> resultList = new ArrayList<ReponderationResult>();
    private final ReponderationParameters reponderationParameters;

    ReponderationLog(ReponderationParameters reponderationParameters) {
        this.reponderationParameters = reponderationParameters;
    }

    public ReponderationParameters getReponderationParameters() {
        return reponderationParameters;
    }

    public int getErrorCount() {
        return errorList.size();
    }

    public ReponderationLog.ReponderationError getError(int i) {
        return errorList.get(i);
    }

    public int getResultCount() {
        return resultList.size();
    }

    public ReponderationLog.ReponderationResult getResult(int i) {
        return resultList.get(i);
    }

    boolean hasError() {
        return (!errorList.isEmpty());
    }

    void addErrorKey(String errorKey) {
        if (!errorMap.containsKey(errorKey)) {
            ReponderationError reponderationError = new ReponderationError(errorKey);
            errorList.add(reponderationError);
            errorMap.put(errorKey, reponderationError);
        }
    }

    void addIdError(String errorKey, int id) {
        ReponderationError reponderationError = errorMap.get(errorKey);
        if (reponderationError == null) {
            reponderationError = new ReponderationError(errorKey);
            errorList.add(reponderationError);
            errorMap.put(errorKey, reponderationError);
        }
        reponderationError.addId(id);
    }

    void addResult(int originId, int croisementId) {
        resultList.add(new ReponderationResult(originId, croisementId));
    }


    public static class ReponderationError {

        private final String errorKey;
        private final List<Integer> idList = new ArrayList<Integer>();

        private ReponderationError(String errorKey) {
            this.errorKey = errorKey;
        }

        public String getErrorKey() {
            return errorKey;
        }

        public int getIdCount() {
            return idList.size();
        }

        public int getId(int i) {
            return idList.get(i);
        }

        private void addId(int id) {
            idList.add(id);
        }

    }


    public static class ReponderationResult {

        private final int originId;
        private final int croisementId;

        private ReponderationResult(int originId, int croisementId) {
            this.originId = originId;
            this.croisementId = croisementId;
        }

        public int getOriginId() {
            return originId;
        }

        public int getCroisementId() {
            return croisementId;
        }

    }

}
