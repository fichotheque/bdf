/* FichothequeLib_Tools - Copyright (c) 2012-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.reponderation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class ReponderationParameters {

    private final Set<Integer> idSet = new HashSet<Integer>();
    private final List<Integer> idList = new ArrayList<Integer>();
    private final SubsetKey originSubsetKey;
    private final SubsetKey croisementSubsetKey;
    private String oldMode = "";
    private String newMode = "";
    private int oldPoids = -1;
    private int newPoids = -1;

    public ReponderationParameters(SubsetKey corpusKey, SubsetKey croisementSubsetKey) {
        this.originSubsetKey = corpusKey;
        this.croisementSubsetKey = croisementSubsetKey;
    }

    public SubsetKey getOriginSubsetKey() {
        return originSubsetKey;
    }

    public SubsetKey getCroisementSubsetKey() {
        return croisementSubsetKey;
    }

    public String getOldMode() {
        return oldMode;
    }

    public void setOldMode(String oldMode) throws ParseException {
        if (oldMode.length() > 0) {
            StringUtils.checkTechnicalName(oldMode, false);
        }
        this.oldMode = oldMode;
    }

    public String getNewMode() {
        return newMode;
    }

    public void setNewMode(String newMode) throws ParseException {
        if (newMode.length() > 0) {
            StringUtils.checkTechnicalName(newMode, false);
        }
        this.newMode = newMode;
    }

    public int getOldPoids() {
        return oldPoids;
    }

    public void setOldPoids(int oldPoids) {
        if (oldPoids < 1) {
            this.oldPoids = -1;
        } else {
            this.oldPoids = oldPoids;
        }
    }

    public int getNewPoids() {
        return newPoids;
    }

    public void setNewPoids(int newPoids) {
        if (newPoids < 1) {
            this.newPoids = -1;
        } else {
            this.newPoids = newPoids;
        }
    }

    public void addOriginId(int id) {
        if (id < 1) {
            throw new IllegalArgumentException("id < 1");
        }
        if (!idSet.contains(id)) {
            idSet.add(id);
            idList.add(id);
        }
    }

    public int getOriginIdCount() {
        return idList.size();
    }

    public int getOriginId(int i) {
        return idList.get(i);
    }

}
