/* FichothequeLib_Tools - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.syntaxes;

import java.io.IOException;
import net.fichotheque.utils.SyntaxUtils;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class PathResolver {

    public final static int FILE = 1;
    public final static int DIR = 2;

    private PathResolver() {

    }

    public static String resolve(String value, int type) {
        if (value.isEmpty()) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        try {
            resolve(buf, value, type);
        } catch (IOException ioe) {

        }
        return buf.toString();
    }

    public static void resolve(Appendable appendable, String value, int type) throws IOException {
        String[] tokens = StringUtils.getTokens(value, '/', StringUtils.NOTCLEAN);
        int last = tokens.length - 1;
        boolean dirEnd = value.endsWith("/");
        for (int i = 0; i < last; i++) {
            SyntaxUtils.appendSpan(appendable, "cm-path-dir", tokens[i]);
            SyntaxUtils.appendSpan(appendable, "cm-punctuation", "/");
        }
        if (type == - 1) {
            if (dirEnd) {
                type = DIR;
            } else {
                type = FILE;
            }
        }
        SyntaxUtils.appendSpan(appendable, getCssClass(type), tokens[last]);
        if (dirEnd) {
            SyntaxUtils.appendSpan(appendable, "cm-punctuation", "/");
        }
    }

    public static String getCssClass(int value) {
        switch (value) {
            case FILE:
                return "cm-path-file";
            case DIR:
                return "cm-path-dir";
            default:
                return "";
        }
    }

}
