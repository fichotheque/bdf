/* FichothequeLib_Tools - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.syntaxes;

import java.io.IOException;
import net.fichotheque.utils.SyntaxUtils;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class JavaResolver {

    public final static int INTERFACE = 1;
    public final static int CLASS = 2;

    private JavaResolver() {

    }

    public static String resolve(String value, int type) {
        if (value.isEmpty()) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        try {
            resolve(buf, value, type);
        } catch (IOException ioe) {

        }
        return buf.toString();
    }

    public static void resolve(Appendable appendable, String value, int type) throws IOException {
        String[] tokens = StringUtils.getTokens(value, '.', StringUtils.NOTCLEAN);
        int last = tokens.length - 1;
        for (int i = 0; i < last; i++) {
            SyntaxUtils.appendSpan(appendable, "cm-java-package", tokens[i]);
            SyntaxUtils.appendSpan(appendable, "cm-punctuation", ".");
        }
        SyntaxUtils.appendSpan(appendable, getCssClass(type), tokens[last]);
    }

    public static String getCssClass(int value) {
        switch (value) {
            case INTERFACE:
                return "cm-java-interface";
            case CLASS:
                return "cm-java-class";
            default:
                return "";
        }
    }

}
