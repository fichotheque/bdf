/* FichothequeLib_Tools - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.syntaxes;

import java.io.IOException;
import net.fichotheque.utils.SyntaxUtils;


/**
 *
 * @author Vincent Calame
 */
public final class PropertyResolver {

    private PropertyResolver() {

    }

    public static String resolve(String value) {
        if (value.isEmpty()) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        try {
            resolve(buf, value);
        } catch (IOException ioe) {

        }
        return buf.toString();
    }

    public static void resolve(Appendable appendable, String value) throws IOException {
        int idx = value.indexOf('=');
        if (idx == -1) {
            SyntaxUtils.appendSpan(appendable, "cm-def", value);
        } else {
            SyntaxUtils.appendSpan(appendable, "cm-def", value.substring(0, idx));
            SyntaxUtils.appendSpan(appendable, "cm-operator", "=");
            SyntaxUtils.appendSpan(appendable, "cm-quote", value.substring(idx + 1));
        }
    }

}
