/* FichothequeLib_Tools - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.syntaxes;

import java.io.IOException;
import net.fichotheque.utils.SyntaxUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractStepResolver {

    protected final String text;
    protected final StringBuilder finalBuf = new StringBuilder();
    protected StringBuilder currentBuf = new StringBuilder();
    protected int step;

    public AbstractStepResolver(String text) {
        this.text = text;
    }

    protected void appendSpaceToFinal() {
        finalBuf.append(' ');
    }

    protected void appendToFinal(String className, String textPart) {
        try {
            if (className != null) {
                SyntaxUtils.appendSpan(finalBuf, className, textPart);
            } else {
                SyntaxUtils.escape(finalBuf, textPart);
            }
        } catch (IOException ioe) {
            throw new ShouldNotOccurException();
        }
    }

    protected void appendOperator(String textPart) {
        try {
            SyntaxUtils.appendSpan(finalBuf, "cm-operator", textPart);
        } catch (IOException ioe) {
            throw new ShouldNotOccurException();
        }
    }

    protected void appendToCurrent(char carac) {
        currentBuf.append(carac);
    }

    protected void appendToCurrent(String part) {
        currentBuf.append(part);
    }

    protected void flush() {
        if (currentBuf.length() == 0) {
            return;
        }
        appendToFinal(getStepClassName(), currentBuf.toString());
        currentBuf = new StringBuilder();
    }

    protected String end() {
        flush();
        return finalBuf.toString();
    }

    public abstract String getStepClassName();

}
