/* FichothequeLib_Tools - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import net.fichotheque.Fichotheque;
import net.fichotheque.extraction.DataResolverProvider;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.extraction.LinkAnalyser;
import net.fichotheque.extraction.SyntaxResolver;
import net.fichotheque.extraction.run.ExtractorProvider;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.thesaurus.policies.PolicyProvider;
import net.fichotheque.tools.extraction.ExtractionContextUtils;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.MessageLocalisationProvider;
import net.mapeadores.util.mimetype.MimeTypeResolver;
import net.mapeadores.util.mimetype.MimeTypeUtils;


/**
 *
 * @author Vincent Calame
 */
public class ExtractionContextBuilder {

    private final Fichotheque fichotheque;
    private final MessageLocalisationProvider messageLocalisationProvider;
    private final LangContext langContext;
    private final PermissionSummary permissionSummary;
    private ExtractorProvider extractorProvider = ExtractionContextUtils.DEFAULT_EXTRACTORPROVIDER;
    private LinkAnalyser linkAnalyser = ExtractionContextUtils.DEFAULT_LINKANALYSER;
    private MimeTypeResolver mimeTypeResolver = MimeTypeUtils.DEFAULT_RESOLVER;
    private SyntaxResolver syntaxResolver = ExtractionContextUtils.DEFAULT_SYNTAXRESOLVER;
    private DataResolverProvider dataResolverProvider = ExtractionContextUtils.DEFAULT_DATARESOLVERPROVIDER;
    private PolicyProvider policyProvider = ExtractionContextUtils.DEFAULT_POLICYPROVIDER;

    public ExtractionContextBuilder(Fichotheque fichotheque, MessageLocalisationProvider messageLocalisationProvider, LangContext langContext, PermissionSummary permissionSummary) {
        this.fichotheque = fichotheque;
        this.messageLocalisationProvider = messageLocalisationProvider;
        this.langContext = langContext;
        this.permissionSummary = permissionSummary;
    }

    public ExtractionContextBuilder setExtractorProvider(ExtractorProvider extractorProvider) {
        if (extractorProvider == null) {
            this.extractorProvider = ExtractionContextUtils.DEFAULT_EXTRACTORPROVIDER;
        } else {
            this.extractorProvider = extractorProvider;
        }
        return this;
    }

    public ExtractionContextBuilder setLinkAnalyser(LinkAnalyser linkAnalyser) {
        if (linkAnalyser == null) {
            this.linkAnalyser = ExtractionContextUtils.DEFAULT_LINKANALYSER;
        } else {
            this.linkAnalyser = linkAnalyser;
        }
        return this;
    }

    public ExtractionContextBuilder setMimeTypeResolver(MimeTypeResolver mimeTypeResolver) {
        if (mimeTypeResolver == null) {
            this.mimeTypeResolver = MimeTypeUtils.DEFAULT_RESOLVER;
        } else {
            this.mimeTypeResolver = mimeTypeResolver;
        }
        return this;
    }

    public ExtractionContextBuilder setSyntaxResolver(SyntaxResolver syntaxResolver) {
        if (syntaxResolver == null) {
            this.syntaxResolver = ExtractionContextUtils.DEFAULT_SYNTAXRESOLVER;
        } else {
            this.syntaxResolver = syntaxResolver;
        }
        return this;
    }

    public ExtractionContextBuilder setDataResolverProvider(DataResolverProvider dataResolverProvider) {
        if (dataResolverProvider == null) {
            this.dataResolverProvider = ExtractionContextUtils.DEFAULT_DATARESOLVERPROVIDER;
        } else {
            this.dataResolverProvider = dataResolverProvider;
        }
        return this;
    }

    public ExtractionContextBuilder setPolicyProvider(PolicyProvider policyProvider) {
        if (policyProvider == null) {
            this.policyProvider = ExtractionContextUtils.DEFAULT_POLICYPROVIDER;
        } else {
            this.policyProvider = policyProvider;
        }
        return this;
    }

    public ExtractionContext toExtractionContext() {
        return new InternalExtractionContext(fichotheque, messageLocalisationProvider, langContext, permissionSummary, extractorProvider, linkAnalyser, mimeTypeResolver, syntaxResolver, dataResolverProvider, policyProvider);
    }

    public static ExtractionContextBuilder init(Fichotheque fichotheque, MessageLocalisationProvider messageLocalisationProvider, LangContext langContext, PermissionSummary permissionSummary) {
        return new ExtractionContextBuilder(fichotheque, messageLocalisationProvider, langContext, permissionSummary);
    }


    private static class InternalExtractionContext implements ExtractionContext {

        private final Fichotheque fichotheque;
        private final MessageLocalisationProvider messageLocalisationProvider;
        private final LangContext langContext;
        private final PermissionSummary permissionSummary;
        private final ExtractorProvider extractorProvider;
        private final LinkAnalyser linkAnalyser;
        private final MimeTypeResolver mimeTypeResolver;
        private final SyntaxResolver syntaxResolver;
        private final DataResolverProvider dataResolverProvider;
        private final PolicyProvider policyProvider;

        private InternalExtractionContext(Fichotheque fichotheque, MessageLocalisationProvider messageLocalisationProvider, LangContext langContext, PermissionSummary permissionSummary, ExtractorProvider extractorProvider, LinkAnalyser linkAnalyser, MimeTypeResolver mimeTypeResolver, SyntaxResolver syntaxResolver, DataResolverProvider dataResolverProvider, PolicyProvider policyProvider) {
            this.fichotheque = fichotheque;
            this.messageLocalisationProvider = messageLocalisationProvider;
            this.langContext = langContext;
            this.permissionSummary = permissionSummary;
            this.extractorProvider = extractorProvider;
            this.linkAnalyser = linkAnalyser;
            this.mimeTypeResolver = mimeTypeResolver;
            this.syntaxResolver = syntaxResolver;
            this.dataResolverProvider = dataResolverProvider;
            this.policyProvider = policyProvider;
        }

        @Override
        public Fichotheque getFichotheque() {
            return fichotheque;
        }

        @Override
        public MessageLocalisationProvider getMessageLocalisationProvider() {
            return messageLocalisationProvider;
        }

        @Override
        public LangContext getLangContext() {
            return langContext;
        }

        @Override
        public LinkAnalyser getLinkAnalyser() {
            return linkAnalyser;
        }

        @Override
        public ExtractorProvider getExtractorProvider() {
            return extractorProvider;
        }

        @Override
        public MimeTypeResolver getMimeTypeResolver() {
            return mimeTypeResolver;
        }

        @Override
        public SyntaxResolver getSyntaxResolver() {
            return syntaxResolver;
        }

        @Override
        public DataResolverProvider getDataResolverProvider() {
            return dataResolverProvider;
        }

        @Override
        public PolicyProvider getPolicyProvider() {
            return policyProvider;
        }

        @Override
        public PermissionSummary getPermissionSummary() {
            return permissionSummary;
        }

    }

}
