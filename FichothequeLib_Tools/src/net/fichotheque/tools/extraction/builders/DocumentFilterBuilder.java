/* FichothequeLib_Tools - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import net.fichotheque.extraction.def.DocumentFilter;
import net.fichotheque.extraction.def.TagNameInfo;


/**
 *
 * @author Vincent Calame
 */
public class DocumentFilterBuilder {

    private TagNameInfo tagNameInfo = TagNameInfo.DEFAULT;

    public DocumentFilterBuilder() {
    }

    public DocumentFilterBuilder setTagNameInfo(TagNameInfo tagNameInfo) {
        this.tagNameInfo = tagNameInfo;
        return this;
    }

    public DocumentFilter toDocumentFilter() {
        return new InternalDocumentFilter(tagNameInfo);
    }

    public static DocumentFilterBuilder init() {
        return new DocumentFilterBuilder();
    }


    private static class InternalDocumentFilter implements DocumentFilter {

        private final TagNameInfo tagNameInfo;

        private InternalDocumentFilter(TagNameInfo tagNameInfo) {
            this.tagNameInfo = tagNameInfo;
        }

        @Override
        public TagNameInfo getTagNameInfo() {
            return tagNameInfo;
        }

    }

}
