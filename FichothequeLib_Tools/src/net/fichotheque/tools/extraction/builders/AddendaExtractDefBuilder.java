/* FichothequeLib_Tools - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import java.util.Collections;
import java.util.List;
import net.fichotheque.extraction.def.AddendaExtractDef;
import net.fichotheque.extraction.def.DocumentFilter;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.selection.DocumentCondition;
import net.fichotheque.utils.SelectionUtils;


/**
 *
 * @author Vincent Calame
 */
public class AddendaExtractDefBuilder {

    private DocumentFilter documentFilter;
    private String name = null;
    private TagNameInfo tagNameInfo = TagNameInfo.DEFAULT;
    private List<DocumentCondition.Entry> entryList = SelectionUtils.EMPTY_DOCUMENTCONDITIONENTRYLIST;

    public AddendaExtractDefBuilder(DocumentFilter documentFilter) {
        if (documentFilter == null) {
            throw new IllegalArgumentException("documentFilter is null");
        }
        this.documentFilter = documentFilter;
    }

    public AddendaExtractDefBuilder setDocumentFilter(DocumentFilter DocumentFilter) {
        if (documentFilter == null) {
            throw new IllegalArgumentException("documentFilter is null");
        }
        this.documentFilter = DocumentFilter;
        return this;
    }

    public AddendaExtractDefBuilder setName(String name) {
        if ((name != null) && (name.length() == 0)) {
            name = null;
        }
        this.name = name;
        return this;
    }

    public AddendaExtractDefBuilder setTagNameInfo(TagNameInfo tagNameInfo) {
        this.tagNameInfo = tagNameInfo;
        return this;
    }

    public AddendaExtractDefBuilder setEntryList(List<DocumentCondition.Entry> list) {
        this.entryList = list;
        return this;
    }

    public AddendaExtractDefBuilder setEntryList(DocumentCondition.Entry entry) {
        this.entryList = Collections.singletonList(entry);
        return this;
    }

    public AddendaExtractDef toAddendaExtractDef() {
        return new InternalAddendaExtractDef(documentFilter, name, tagNameInfo, entryList);
    }

    public static AddendaExtractDefBuilder init(DocumentFilter documentFilter) {
        return new AddendaExtractDefBuilder(documentFilter);
    }


    private static class InternalAddendaExtractDef implements AddendaExtractDef {

        private final DocumentFilter documentFilter;
        private final String name;
        private final TagNameInfo tagNameInfo;
        private final List<DocumentCondition.Entry> entryList;

        private InternalAddendaExtractDef(DocumentFilter documentFilter, String name, TagNameInfo tagNameInfo, List<DocumentCondition.Entry> entryList) {
            this.tagNameInfo = tagNameInfo;
            this.name = name;
            this.documentFilter = documentFilter;
            this.entryList = entryList;
        }

        @Override
        public TagNameInfo getTagNameInfo() {
            return tagNameInfo;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public DocumentFilter getDocumentFilter() {
            return documentFilter;
        }

        @Override
        public List<DocumentCondition.Entry> getConditionEntryList() {
            return entryList;
        }

    }

}
