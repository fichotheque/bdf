/* FichothequeLib_Tools - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import java.util.Collections;
import java.util.List;
import net.fichotheque.extraction.def.AlbumExtractDef;
import net.fichotheque.extraction.def.IllustrationFilter;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.selection.IllustrationCondition;
import net.fichotheque.utils.SelectionUtils;


/**
 *
 * @author Vincent Calame
 */
public class AlbumExtractDefBuilder {

    private IllustrationFilter illustrationFilter;
    private String name = null;
    private TagNameInfo tagNameInfo = TagNameInfo.DEFAULT;
    private List<IllustrationCondition.Entry> entryList = SelectionUtils.EMPTY_ILLUSTRATIONCONDITIONENTRYLIST;

    public AlbumExtractDefBuilder(IllustrationFilter illustrationFilter) {
        if (illustrationFilter == null) {
            throw new IllegalArgumentException("illustrationFilter is null");
        }
        this.illustrationFilter = illustrationFilter;
    }


    public AlbumExtractDefBuilder setIllustrationFilter(IllustrationFilter illustrationFilter) {
        if (illustrationFilter == null) {
            throw new IllegalArgumentException("illustrationFilter is null");
        }
        this.illustrationFilter = illustrationFilter;
        return this;
    }

    public AlbumExtractDefBuilder setTagNameInfo(TagNameInfo tagNameInfo) {
        this.tagNameInfo = tagNameInfo;
        return this;
    }

    public AlbumExtractDefBuilder setName(String name) {
        if ((name != null) && (name.length() == 0)) {
            name = null;
        }
        this.name = name;
        return this;
    }

    public AlbumExtractDefBuilder setEntryList(List<IllustrationCondition.Entry> list) {
        this.entryList = list;
        return this;
    }

    public AlbumExtractDefBuilder setEntryList(IllustrationCondition.Entry entry) {
        this.entryList = Collections.singletonList(entry);
        return this;
    }

    public AlbumExtractDef toAlbumExtractDef() {
        return new InternalAlbumExtractDef(illustrationFilter, name, tagNameInfo, entryList);
    }

    public static AlbumExtractDefBuilder init(IllustrationFilter illustrationFilter) {
        return new AlbumExtractDefBuilder(illustrationFilter);
    }


    private static class InternalAlbumExtractDef implements AlbumExtractDef {

        private final IllustrationFilter illustrationFilter;
        private final String name;
        private final TagNameInfo tagNameInfo;
        private final List<IllustrationCondition.Entry> entryList;

        private InternalAlbumExtractDef(IllustrationFilter illustrationFilter, String name, TagNameInfo tagNameInfo, List<IllustrationCondition.Entry> entryList) {
            this.illustrationFilter = illustrationFilter;
            this.name = name;
            this.tagNameInfo = tagNameInfo;
            this.entryList = entryList;
        }

        @Override
        public TagNameInfo getTagNameInfo() {
            return tagNameInfo;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public IllustrationFilter getIllustrationFilter() {
            return illustrationFilter;
        }

        @Override
        public List<IllustrationCondition.Entry> getConditionEntryList() {
            return entryList;
        }

    }

}
