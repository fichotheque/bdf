/* FichothequeLib_Tools - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.fichotheque.extraction.FilterParameters;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class FilterParametersBuilder {


    private final Map<String, List<String>> map = new HashMap<String, List<String>>();

    public FilterParametersBuilder() {

    }

    public FilterParametersBuilder appendValue(String name, String value) {
        if (value == null) {
            return this;
        }
        value = value.trim();
        if (value.isEmpty()) {
            return this;
        }
        getList(name).add(value);
        return this;
    }

    public FilterParametersBuilder appendValues(String name, Collection<String> values) {
        List<String> list = getList(name);
        for (String value : values) {
            if (value != null) {
                value = value.trim();
                if (!value.isEmpty()) {
                    list.add(value);
                }
            }
        }
        return this;
    }

    private List<String> getList(String name) {
        List<String> list = map.get(name);
        if (list == null) {
            list = new ArrayList<String>();
            map.put(name, list);
        }
        return list;
    }

    public FilterParameters toFilterParameters() {
        Map<String, List<String>> finalMap = new HashMap<String, List<String>>();
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            List<String> finalList = StringUtils.toList(entry.getValue());
            if (!finalList.isEmpty()) {
                finalMap.put(entry.getKey(), finalList);
            }
        }
        return new InternalFilterParameters(finalMap);
    }

    public static FilterParametersBuilder init() {
        return new FilterParametersBuilder();
    }


    private static class InternalFilterParameters implements FilterParameters {

        private final Map<String, List<String>> map;

        private InternalFilterParameters(Map<String, List<String>> map) {
            this.map = map;
        }

        @Override
        public boolean hasParameters() {
            return !map.isEmpty();
        }

        @Override
        public Set<String> getParameterNameSet() {
            return Collections.unmodifiableSet(map.keySet());
        }


        @Override
        public List<String> getParameter(String name) {
            List<String> list = map.get(name);
            if (list != null) {
                return list;
            } else {
                return StringUtils.EMPTY_STRINGLIST;
            }
        }

    }

}
