/* FichothequeLib_Tools - Copyright (c) 2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import java.util.function.Predicate;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.extraction.ExtractParameters;
import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.ExtractionContext;
import net.fichotheque.extraction.IrefConverter;
import net.fichotheque.tools.extraction.IrefConverterFactory;


/**
 *
 * @author Vincent Calame
 */
public class ExtractParametersBuilder {

    private final ExtractionContext extractionContext;
    private int extractVersion = ExtractionConstants.PLURAL_VERSION;
    private boolean withEmpty = false;
    private boolean withPosition = false;
    private Predicate<FicheMeta> fichePredicate = null;
    private IrefConverterFactory irefConverterFactory = null;

    public ExtractParametersBuilder(ExtractionContext extractionContext) {
        this.extractionContext = extractionContext;
    }

    public ExtractParametersBuilder setExtractVersion(int extractVersion) {
        this.extractVersion = extractVersion;
        return this;
    }

    public ExtractParametersBuilder setWithEmpty(boolean withEmpty) {
        this.withEmpty = withEmpty;
        return this;
    }

    public ExtractParametersBuilder setWithPosition(boolean withPosition) {
        this.withPosition = withPosition;
        return this;
    }

    public ExtractParametersBuilder setFichePredicate(Predicate<FicheMeta> fichePredicate) {
        this.fichePredicate = fichePredicate;
        return this;
    }

    public ExtractParameters toExtractParameters() {
        if (irefConverterFactory == null) {
            irefConverterFactory = new IrefConverterFactory();
        }
        return new InternalExtractParameters(extractionContext, extractVersion, withEmpty, fichePredicate, irefConverterFactory, withPosition);
    }

    public static ExtractParametersBuilder init(ExtractionContext extractionContext) {
        return new ExtractParametersBuilder(extractionContext);
    }


    private static class InternalExtractParameters implements ExtractParameters {

        private final ExtractionContext extractionContext;
        private final int extractVersion;
        private final boolean withEmpty;
        private final Predicate<FicheMeta> fichePredicate;
        private final IrefConverterFactory irefConverterFactory;
        private final boolean withPosition;

        private InternalExtractParameters(ExtractionContext extractionContext, int extractVersion, boolean withEmpty, Predicate<FicheMeta> fichePredicate, IrefConverterFactory irefConverterFactory, boolean withPosition) {
            this.extractionContext = extractionContext;
            this.extractVersion = extractVersion;
            this.withEmpty = withEmpty;
            this.fichePredicate = fichePredicate;
            this.irefConverterFactory = irefConverterFactory;
            this.withPosition = withPosition;
        }

        @Override
        public ExtractionContext getExtractionContext() {
            return extractionContext;
        }

        @Override
        public int getExtractVersion() {
            return extractVersion;
        }

        @Override
        public boolean isWithEmpty() {
            return withEmpty;
        }

        @Override
        public Predicate<FicheMeta> getFichePredicate() {
            return fichePredicate;
        }

        @Override
        public IrefConverter newIrefConverter() {
            return irefConverterFactory.newInstance();
        }

        @Override
        public boolean isWithPosition() {
            return withPosition;
        }

    }

}
