/* FichothequeLib_Tools - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.extraction.def.CorpusExtractDef;
import net.fichotheque.extraction.def.ExtractionDef;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.extraction.def.ThesaurusExtractDef;


/**
 *
 * @author Vincent Calame
 */
public class ExtractionDefBuilder {

    private CorpusExtractDef dynamicCorpusExtractDef;
    private ThesaurusExtractDef dynamicThesaurusExtractDef;
    private TagNameInfo tagNameInfo = TagNameInfo.DEFAULT;
    private TagNameInfo staticTagNameInfo = TagNameInfo.DEFAULT;
    private final List<CorpusExtractDef> staticCorpusExtractDefList = new ArrayList<CorpusExtractDef>();
    private final List<ThesaurusExtractDef> staticThesaurusExtractDefList = new ArrayList<ThesaurusExtractDef>();

    public ExtractionDefBuilder() {
    }

    public ExtractionDefBuilder setDynamicCorpusExtractDef(CorpusExtractDef corpusExtractDef) {
        this.dynamicCorpusExtractDef = corpusExtractDef;
        return this;
    }

    public ExtractionDefBuilder setDynamicThesaurusExtractDef(ThesaurusExtractDef thesaurusExtractDef) {
        this.dynamicThesaurusExtractDef = thesaurusExtractDef;
        return this;
    }

    public ExtractionDefBuilder setTagNameInfo(TagNameInfo tagNameInfo) {
        this.tagNameInfo = tagNameInfo;
        return this;
    }

    public ExtractionDefBuilder setStaticTagNameInfo(TagNameInfo staticTagNameInfo) {
        this.staticTagNameInfo = staticTagNameInfo;
        return this;
    }

    public ExtractionDefBuilder addStatic(CorpusExtractDef corpusExtractDef) {
        this.staticCorpusExtractDefList.add(corpusExtractDef);
        return this;
    }

    public ExtractionDefBuilder addStatic(ThesaurusExtractDef thesaurusExtractDef) {
        this.staticThesaurusExtractDefList.add(thesaurusExtractDef);
        return this;
    }

    public ExtractionDef toExtractionDef() {
        return new InternalExtractionDef(dynamicCorpusExtractDef, dynamicThesaurusExtractDef, tagNameInfo, staticCorpusExtractDefList, staticThesaurusExtractDefList, staticTagNameInfo);
    }

    public static ExtractionDefBuilder init() {
        return new ExtractionDefBuilder();
    }


    private static class InternalExtractionDef implements ExtractionDef {

        private final CorpusExtractDef dynamicCorpusExtractDef;
        private final ThesaurusExtractDef dynamicThesaurusExtractDef;
        private final TagNameInfo tagNameInfo;
        private final TagNameInfo staticTagNameInfo;
        private final List<CorpusExtractDef> staticCorpusExtractDefList;
        private final List<ThesaurusExtractDef> staticThesaurusExtractDefList;

        private InternalExtractionDef(CorpusExtractDef dynamicCorpusExtractDef, ThesaurusExtractDef dynamicThesaurusExtractDef, TagNameInfo tagNameInfo, List<CorpusExtractDef> staticCorpusExtractDefList, List<ThesaurusExtractDef> staticThesaurusExtractDefList, TagNameInfo staticTagNameInfo) {
            this.dynamicCorpusExtractDef = dynamicCorpusExtractDef;
            this.dynamicThesaurusExtractDef = dynamicThesaurusExtractDef;
            this.tagNameInfo = tagNameInfo;
            this.staticCorpusExtractDefList = staticCorpusExtractDefList;
            this.staticThesaurusExtractDefList = staticThesaurusExtractDefList;
            this.staticTagNameInfo = staticTagNameInfo;
        }

        @Override
        public CorpusExtractDef getDynamicCorpusExtractDef() {
            return dynamicCorpusExtractDef;
        }

        @Override
        public ThesaurusExtractDef getDynamicThesaurusExtractDef() {
            return dynamicThesaurusExtractDef;
        }

        @Override
        public TagNameInfo getTagNameInfo() {
            return tagNameInfo;
        }

        @Override
        public List<CorpusExtractDef> getStaticCorpusExtractDefList() {
            return staticCorpusExtractDefList;
        }

        @Override
        public List<ThesaurusExtractDef> getStaticThesaurusExtractDefList() {
            return staticThesaurusExtractDefList;
        }

        @Override
        public TagNameInfo getStaticTagNameInfo() {
            return staticTagNameInfo;
        }

    }

}
