/* FichothequeLib_Tools - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import net.fichotheque.extraction.def.IllustrationFilter;
import net.fichotheque.extraction.def.TagNameInfo;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationFilterBuilder {

    private TagNameInfo tagNameInfo = TagNameInfo.DEFAULT;

    public IllustrationFilterBuilder() {
    }

    public IllustrationFilterBuilder setTagNameInfo(TagNameInfo tagNameInfo) {
        this.tagNameInfo = tagNameInfo;
        return this;
    }

    public IllustrationFilter toIllustrationFilter() {
        return new InternalIllustrationFilter(tagNameInfo);
    }

    public static IllustrationFilterBuilder init() {
        return new IllustrationFilterBuilder();
    }


    private static class InternalIllustrationFilter implements IllustrationFilter {

        private final TagNameInfo tagNameInfo;

        private InternalIllustrationFilter(TagNameInfo tagNameInfo) {
            this.tagNameInfo = tagNameInfo;
        }

        @Override
        public TagNameInfo getTagNameInfo() {
            return tagNameInfo;
        }

    }

}
