/* FichothequeLib_Tools - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.def.MotcleFilter;
import net.fichotheque.extraction.def.TagNameInfo;
import net.fichotheque.extraction.def.ThesaurusExtractDef;
import net.fichotheque.selection.MotcleCondition;
import net.fichotheque.utils.SelectionUtils;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusExtractDefBuilder implements ExtractionConstants {

    private final Map<String, Boolean> booleanMap = new HashMap<String, Boolean>();
    private MotcleFilter motcleFilter;
    private String name = null;
    private TagNameInfo tagNameInfo = TagNameInfo.DEFAULT;
    private List<MotcleCondition.Entry> entryList = SelectionUtils.EMPTY_MOTCLECONDITIONENTRYLIST;

    public ThesaurusExtractDefBuilder() {

    }

    public ThesaurusExtractDefBuilder(MotcleFilter motcleFilter) {
        if (motcleFilter == null) {
            throw new IllegalArgumentException("motcleFilter is null");
        }
        this.motcleFilter = motcleFilter;
    }


    public ThesaurusExtractDefBuilder setMotcleFilter(MotcleFilter motcleFilter) {
        if (motcleFilter == null) {
            throw new IllegalArgumentException("motcleFilter is null");
        }
        this.motcleFilter = motcleFilter;
        return this;
    }

    public ThesaurusExtractDefBuilder setName(String name) {
        if ((name != null) && (name.length() == 0)) {
            name = null;
        }
        this.name = name;
        return this;
    }

    public ThesaurusExtractDefBuilder setTagNameInfo(TagNameInfo tagNameInfo) {
        this.tagNameInfo = tagNameInfo;
        return this;
    }

    public ThesaurusExtractDefBuilder setEntryList(List<MotcleCondition.Entry> list) {
        this.entryList = list;
        return this;
    }

    public ThesaurusExtractDefBuilder setEntryList(MotcleCondition.Entry entry) {
        this.entryList = Collections.singletonList(entry);
        return this;
    }

    public ThesaurusExtractDefBuilder setBoolean(String name, boolean value) {
        if (value) {
            booleanMap.put(name, Boolean.TRUE);
        } else {
            booleanMap.remove(name);
        }
        return this;
    }

    public ThesaurusExtractDef toThesaurusExtractDef() {
        if (motcleFilter == null) {
            throw new IllegalStateException("motcleFilter is null");
        }
        Map<String, Boolean> finalBooleanMap = new HashMap<String, Boolean>(booleanMap);
        return new InternalThesaurusExtractDef(motcleFilter, name, tagNameInfo, entryList, finalBooleanMap);
    }

    public static ThesaurusExtractDefBuilder init() {
        return new ThesaurusExtractDefBuilder();
    }

    public static ThesaurusExtractDefBuilder init(MotcleFilter motcleFilter) {
        return new ThesaurusExtractDefBuilder(motcleFilter);
    }


    private static class InternalThesaurusExtractDef implements ThesaurusExtractDef {

        private final MotcleFilter motcleFilter;
        private final String name;
        private final TagNameInfo tagNameInfo;
        private final List<MotcleCondition.Entry> entryList;
        private final Map<String, Boolean> booleanMap;

        private InternalThesaurusExtractDef(MotcleFilter motcleFilter, String name, TagNameInfo tagNameInfo, List<MotcleCondition.Entry> entryList, Map<String, Boolean> booleanMap) {
            this.motcleFilter = motcleFilter;
            this.name = name;
            this.tagNameInfo = tagNameInfo;
            this.entryList = entryList;
            this.booleanMap = booleanMap;
        }

        @Override
        public TagNameInfo getTagNameInfo() {
            return tagNameInfo;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public MotcleFilter getMotcleFilter() {
            return motcleFilter;
        }

        @Override
        public List<MotcleCondition.Entry> getConditionEntryList() {
            return entryList;
        }

        @Override
        public boolean hasBooleanParameters() {
            return !booleanMap.isEmpty();
        }

        @Override
        public boolean getBooleanParameter(String name) {
            return booleanMap.containsKey(name);
        }

    }

}
