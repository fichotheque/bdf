/* FichothequeLib_Tools - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.def.PackClause;
import net.fichotheque.extraction.def.TagNameInfo;


/**
 *
 * @author Vincent Calame
 */
public class PackClauseBuilder implements ExtractionConstants {

    private int packSize;
    private TagNameInfo tagNameInfo = TagNameInfo.DEFAULT;
    private int modulo = 0;

    public PackClauseBuilder(int packSize) {
        if (packSize < 1) {
            packSize = 1;
        }
        this.packSize = packSize;
    }

    public PackClauseBuilder setTagNameInfo(TagNameInfo tagNameInfo) {
        this.tagNameInfo = tagNameInfo;
        return this;
    }

    public PackClauseBuilder setModulo(int modulo) {
        if (modulo < 2) {
            modulo = 0;
        }
        this.modulo = modulo;
        return this;
    }

    public PackClause toPackClause() {
        return new InternalPackClause(tagNameInfo, packSize, modulo);
    }

    public static PackClauseBuilder init(int packSize) {
        return new PackClauseBuilder(packSize);
    }


    private static class InternalPackClause implements PackClause {

        private final TagNameInfo tagNameInfo;
        private final int packSize;
        private final int modulo;


        private InternalPackClause(TagNameInfo tagNameInfo, int packSize, int modulo) {
            this.tagNameInfo = tagNameInfo;
            this.packSize = packSize;
            this.modulo = modulo;
        }

        @Override
        public TagNameInfo getTagNameInfo() {
            return tagNameInfo;
        }

        @Override
        public int getModulo() {
            return modulo;
        }

        @Override
        public int getPackSize() {
            return packSize;
        }

    }

}
