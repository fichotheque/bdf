/* FichothequeLib_Tools - Copyright (c) 2010-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.builders;

import net.fichotheque.extraction.def.TitleClause;


/**
 *
 * @author Vincent Calame
 */
public class TitleClauseBuilder {

    public static final TitleClause DEFAULT = new InternalTitleClause();


    private static class InternalTitleClause implements TitleClause {

        private InternalTitleClause() {

        }

        @Override
        public boolean withTitle() {
            return true;
        }

    }

}
