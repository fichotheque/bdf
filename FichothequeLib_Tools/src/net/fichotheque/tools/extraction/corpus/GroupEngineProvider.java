/* FichothequeLib_Tools - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.corpus;

import net.fichotheque.extraction.ExtractionConstants;
import net.fichotheque.extraction.def.GroupClause;
import net.mapeadores.util.exceptions.SwitchException;


/**
 *
 * @author Vincent Calame
 */
class GroupEngineProvider {

    private GroupClause groupClause;
    private GroupEngineProvider subProvider;

    GroupEngineProvider(GroupClause groupClause, GroupEngineProvider subProvider) {
        this.groupClause = groupClause;
        this.subProvider = subProvider;
    }

    GroupEngine newGroupEngine() {
        switch (groupClause.getGroupType()) {
            case ExtractionConstants.POIDS_TYPE:
                return new PoidsEngine(groupClause, subProvider);
            case ExtractionConstants.TITRE_TYPE:
                return new TitreEngine(groupClause, subProvider);
            case ExtractionConstants.ANNEE_TYPE:
                return new AnneeEngine(groupClause, subProvider);
            case ExtractionConstants.LANG_TYPE:
                return new LangueEngine(groupClause, subProvider);
            case ExtractionConstants.PAYS_TYPE:
                return new PaysEngine(groupClause, subProvider);
            case ExtractionConstants.FIELDS_TYPE:
                return new FieldsEngine(groupClause, subProvider);
            case ExtractionConstants.POSITION_TYPE:
                return new PositionEngine(groupClause, subProvider);
            default:
                throw new SwitchException("groupClause.getGroupeType() =  " + groupClause.getGroupType());
        }
    }

    static GroupEngineProvider newInstance(GroupClause groupClause) {
        GroupClause subGroupClause = groupClause.getSubGroupClause();
        GroupEngineProvider subProvider = null;
        if (subGroupClause != null) {
            subProvider = newInstance(subGroupClause);
        }
        return new GroupEngineProvider(groupClause, subProvider);
    }

}
