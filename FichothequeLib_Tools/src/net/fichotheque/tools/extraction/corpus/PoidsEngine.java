/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.corpus;

import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.def.FicheFilter;
import net.fichotheque.extraction.def.GroupClause;
import net.fichotheque.extraction.run.FicheGroup;
import static net.fichotheque.tools.extraction.corpus.GroupEngine.toGroupArray;


/**
 *
 * @author Vincent Calame
 */
class PoidsEngine extends GroupEngine {

    private final SortedMap<Integer, GroupBuilder> treeMap = new TreeMap<Integer, GroupBuilder>();
    private final GroupClause groupClause;
    private final GroupEngineProvider subGroupEngineProvider;

    PoidsEngine(GroupClause groupClause, GroupEngineProvider subGroupEngineProvider) {
        this.groupClause = groupClause;
        this.subGroupEngineProvider = subGroupEngineProvider;
    }

    @Override
    public void add(FicheMeta ficheMeta, FicheFilter ficheFilter, Croisement croisement) {
        int poids = 0;
        if (croisement != null) {
            poids = croisement.getLienList().get(0).getPoids();
        }
        Integer poidsInteger = poids;
        GroupBuilder groupBuilder = treeMap.get(poidsInteger);
        if (groupBuilder == null) {
            groupBuilder = GroupBuilder.newInstance(poidsInteger.toString(), groupClause.getTagNameInfo(), subGroupEngineProvider);
            treeMap.put(poidsInteger, groupBuilder);
        }
        groupBuilder.add(ficheMeta, ficheFilter, croisement);
    }

    @Override
    public FicheGroup[] toFicheGroupArray() {
        return toGroupArray(treeMap.values(), groupClause, null);
    }

}
