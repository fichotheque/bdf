/* FichothequeLib_Tools - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.corpus;

import java.util.Locale;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.def.FicheFilter;
import net.fichotheque.extraction.def.GroupClause;
import net.fichotheque.extraction.run.FicheExtractInfo;
import net.fichotheque.extraction.run.FicheGroup;
import net.fichotheque.extraction.run.FieldsGroupParams;
import net.mapeadores.util.text.alphabet.Alphabet;
import net.mapeadores.util.text.alphabet.AlphabeticEntry;
import net.mapeadores.util.text.collation.group.CollationGroupBuilder;
import net.mapeadores.util.text.collation.group.ObjectWrapper;


/**
 *
 * @author Vincent Calame
 */
class TitreEngine extends GroupEngine {

    private final GroupClause groupClause;
    private final GroupEngineProvider subGroupEngineProvider;
    private boolean alphabetMode = false;
    private final CollationGroupBuilder collationGroupBuilder;

    TitreEngine(GroupClause groupClause, GroupEngineProvider subGroupEngineProvider) {
        this.groupClause = groupClause;
        this.subGroupEngineProvider = subGroupEngineProvider;
        FieldsGroupParams groupParams = (FieldsGroupParams) groupClause.getGroupParams();
        if (groupParams != null) {
            alphabetMode = (groupParams.getMode() == FieldsGroupParams.ALPHABET_MODE);
        }
        collationGroupBuilder = new CollationGroupBuilder(Locale.getDefault());
    }

    @Override
    public void add(FicheMeta ficheMeta, FicheFilter ficheFilter, Croisement croisement) {
        String titre = ficheMeta.getTitre();
        Object groupClauseObject = null;
        if (alphabetMode) {
            AlphabeticEntry alphabeticEntry = Alphabet.newInstance(titre, ficheMeta.getLang());
            titre = alphabeticEntry.getEntryString();
            groupClauseObject = alphabeticEntry;
        }
        FicheExtractInfo info = GroupBuilder.toFicheExtractInfo(ficheMeta, ficheFilter, croisement, groupClauseObject);
        ObjectWrapper objectWrapper = ObjectWrapper.newCleanedInstance(titre, info);
        collationGroupBuilder.addObjectWrapper(objectWrapper);
    }

    @Override
    public FicheGroup[] toFicheGroupArray() {
        return toGroupArray(collationGroupBuilder, groupClause, subGroupEngineProvider);
    }

}
