/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction.corpus;

import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.extraction.def.FicheFilter;
import net.fichotheque.extraction.def.GroupClause;
import net.fichotheque.extraction.run.FicheGroup;
import net.fichotheque.extraction.run.FicheItemGroupParams;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
class LangueEngine extends GroupEngine {

    private final GroupClause groupClause;
    private final GroupEngineProvider subGroupEngineProvider;
    private FieldKey fieldKey;
    private GroupBuilder emptyGroupBuilder;
    private final SortedMap<String, GroupBuilder> treeMap = new TreeMap<String, GroupBuilder>();

    LangueEngine(GroupClause groupClause, GroupEngineProvider subGroupEngineProvider) {
        this.groupClause = groupClause;
        this.subGroupEngineProvider = subGroupEngineProvider;
        FicheItemGroupParams groupParams = (FicheItemGroupParams) groupClause.getGroupParams();
        if (groupParams != null) {
            fieldKey = groupParams.getFieldKey();
        }
    }

    @Override
    public void add(FicheMeta ficheMeta, FicheFilter ficheFilter, Croisement croisement) {
        Lang value = null;
        if (fieldKey == null) {
            value = ficheMeta.getLang();
        } else {
            Object obj = FicheUtils.getValue(ficheMeta, fieldKey);
            if (obj != null) {
                if (obj instanceof FicheItem) {
                    value = getValue((FicheItem) obj);
                } else if (obj instanceof FicheItems) {
                    value = getValue(((FicheItems) obj).get(0));
                }
            }
        }
        GroupBuilder groupBuilder = getGroupBuilder(value);
        groupBuilder.add(ficheMeta, ficheFilter, croisement);
    }

    private Lang getValue(FicheItem ficheItem) {
        if (ficheItem instanceof Langue) {
            return ((Langue) ficheItem).getLang();
        } else {
            return null;
        }
    }

    @Override
    public FicheGroup[] toFicheGroupArray() {
        return toGroupArray(treeMap.values(), groupClause, emptyGroupBuilder);
    }

    private GroupBuilder getGroupBuilder(Lang value) {
        if (value == null) {
            if (emptyGroupBuilder == null) {
                emptyGroupBuilder = GroupBuilder.newInstance("_", groupClause.getTagNameInfo(), subGroupEngineProvider);
            }
            return emptyGroupBuilder;
        }
        String langKey = value.toString();
        GroupBuilder groupBuilder = treeMap.get(langKey);
        if (groupBuilder == null) {
            groupBuilder = GroupBuilder.newInstance(langKey, groupClause.getTagNameInfo(), subGroupEngineProvider);
            treeMap.put(langKey, groupBuilder);
            Langue langue = new Langue(value);
            FicheItem[] matchingFicheItemArray = new FicheItem[1];
            matchingFicheItemArray[0] = langue;
            groupBuilder.setMatchingFicheItemArray(matchingFicheItemArray);
        }
        return groupBuilder;
    }

}
