/* FichothequeLib_Xml - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction;

import java.util.HashMap;
import java.util.Map;
import net.fichotheque.extraction.IrefConverter;


/**
 *
 * @author Vincent Calame
 */
public class IrefConverterFactory {

    private int step = 1;

    public IrefConverterFactory() {

    }

    private String getNewIref() {
        return String.valueOf(step++);
    }

    public IrefConverter newInstance() {
        return new InternalIrefConverter();
    }


    private class InternalIrefConverter implements IrefConverter {

        private final Map<String, String> matchingMap = new HashMap<String, String>();

        private InternalIrefConverter() {

        }

        @Override
        public String convert(String ref) {
            String existing = matchingMap.get(ref);
            if (existing != null) {
                return existing;
            }
            String newIref = getNewIref();
            matchingMap.put(ref, newIref);
            return newIref;
        }

    }

}
