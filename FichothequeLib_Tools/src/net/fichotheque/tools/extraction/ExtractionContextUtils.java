/* FichothequeLib_Tools - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.extraction;

import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.extraction.DataResolver;
import net.fichotheque.extraction.DataResolverProvider;
import net.fichotheque.extraction.LinkAnalyser;
import net.fichotheque.extraction.SyntaxResolver;
import net.fichotheque.extraction.def.AddendaExtractDef;
import net.fichotheque.extraction.def.AlbumExtractDef;
import net.fichotheque.extraction.def.CorpusExtractDef;
import net.fichotheque.extraction.def.ThesaurusExtractDef;
import net.fichotheque.extraction.run.AddendaExtractor;
import net.fichotheque.extraction.run.AlbumExtractor;
import net.fichotheque.extraction.run.CorpusExtractor;
import net.fichotheque.extraction.run.ExtractorProvider;
import net.fichotheque.extraction.run.ThesaurusExtractor;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.policies.DynamicEditPolicy;
import net.fichotheque.thesaurus.policies.PolicyProvider;
import net.fichotheque.tools.extraction.syntaxes.AttributeResolver;
import net.fichotheque.tools.extraction.syntaxes.CssPropertyResolver;
import net.fichotheque.tools.extraction.syntaxes.CssResolver;
import net.fichotheque.tools.extraction.syntaxes.FicheblockResolver;
import net.fichotheque.tools.extraction.syntaxes.JavaResolver;
import net.fichotheque.tools.extraction.syntaxes.PathResolver;
import net.fichotheque.tools.extraction.syntaxes.PropertyResolver;
import net.fichotheque.tools.extraction.syntaxes.XmlResolver;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public final class ExtractionContextUtils {

    public final static ExtractorProvider DEFAULT_EXTRACTORPROVIDER = new DefaultExtractorProvider();
    public final static LinkAnalyser DEFAULT_LINKANALYSER = new DefaultLinkAnalyser();
    public final static SyntaxResolver DEFAULT_SYNTAXRESOLVER = new DefaultSyntaxResolver();
    public final static DataResolver DEFAULT_DATARESOLVER = new DefaultDataResolver();
    public final static DataResolverProvider DEFAULT_DATARESOLVERPROVIDER = new DefaultDataResolverProvider();
    public final static PolicyProvider DEFAULT_POLICYPROVIDER = new DefaultPolicyProvider();
    public final static DataResolver.Writer EMPTY_DATARESOLVER_WRITER = new EmptyDataResolverWriter();

    private ExtractionContextUtils() {
    }


    private static class DefaultLinkAnalyser implements LinkAnalyser {

        private DefaultLinkAnalyser() {
        }

        @Override
        public boolean isExternalLink(String href) {
            if (href == null) {
                return false;
            }
            return (href.indexOf("://") > 0);
        }

    }


    private static class DefaultExtractorProvider implements ExtractorProvider {

        private DefaultExtractorProvider() {
        }

        @Override
        public AddendaExtractor getAddendaExtractor(AddendaExtractDef addendaExtractDef) {
            return new AddendaExtractorImpl(addendaExtractDef);
        }

        @Override
        public AlbumExtractor getAlbumExtractor(AlbumExtractDef albumExtractDef) {
            return new AlbumExtractorImpl(albumExtractDef);
        }

        @Override
        public CorpusExtractor getCorpusExtractor(CorpusExtractDef corpusExtractDef) {
            return new CorpusExtractorImpl(corpusExtractDef);
        }

        @Override
        public ThesaurusExtractor getThesaurusExtractor(ThesaurusExtractDef thesaurusExtractDef) {
            return new ThesaurusExtractorImpl(thesaurusExtractDef);
        }

    }


    private static class DefaultSyntaxResolver implements SyntaxResolver {


        private DefaultSyntaxResolver() {

        }

        @Override
        public String toCData(String syntaxName, String sValue) {
            switch (syntaxName) {
                case "attribute":
                    return AttributeResolver.resolve(sValue);
                case "css":
                    return CssResolver.resolve(sValue);
                case "cssproperty":
                    return CssPropertyResolver.resolve(sValue);
                case "ficheblock":
                    return FicheblockResolver.resolve(sValue);
                case "java-class":
                    return JavaResolver.resolve(sValue, JavaResolver.CLASS);
                case "java-interface":
                    return JavaResolver.resolve(sValue, JavaResolver.INTERFACE);
                case "path":
                    return PathResolver.resolve(sValue, -1);
                case "property":
                    return PropertyResolver.resolve(sValue);
                case "xml":
                    return XmlResolver.resolve(sValue);
                default:
                    return "";
            }
        }

    }


    private static class DefaultDataResolver implements DataResolver {

        private DefaultDataResolver() {

        }

        @Override
        public String getType() {
            return "unknown";
        }

        @Override
        public Writer getWriter(SubsetItemPointeur pointeur, LangContext langContext) {
            return EMPTY_DATARESOLVER_WRITER;
        }

    }


    private static class EmptyDataResolverWriter implements DataResolver.Writer {

        private EmptyDataResolverWriter() {

        }

        @Override
        public boolean isEmpty() {
            return true;
        }

        @Override
        public void write(XMLWriter xmlWriter) throws IOException {

        }

    }


    private static class DefaultDataResolverProvider implements DataResolverProvider {

        private DefaultDataResolverProvider() {

        }

        @Override
        public DataResolver getDataResolver(SubsetKey subsetKey, String name) {
            return DEFAULT_DATARESOLVER;
        }

    }


    private static class DefaultPolicyProvider implements PolicyProvider {

        private DefaultPolicyProvider() {

        }

        @Override
        public DynamicEditPolicy getDynamicEditPolicy(Thesaurus thesaurus) {
            return ThesaurusUtils.NONE_POLICY;
        }

    }


}
