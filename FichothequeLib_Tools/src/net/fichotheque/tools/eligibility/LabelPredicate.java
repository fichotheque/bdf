/* FichothequeLib_Tools - Copyright (c) 2007-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.eligibility;

import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public class LabelPredicate implements Predicate<SubsetItem> {

    private final Lang lang;
    private final boolean here;

    public LabelPredicate(Lang lang, boolean here) {
        this.lang = lang;
        this.here = here;
    }

    @Override
    public boolean test(SubsetItem subsetItem) {
        if (!(subsetItem instanceof Motcle)) {
            return false;
        }
        Label label = ((Motcle) subsetItem).getLabels().getLabel(lang);
        if (here) {
            return (label != null);
        } else {
            return (label == null);
        }
    }

}
