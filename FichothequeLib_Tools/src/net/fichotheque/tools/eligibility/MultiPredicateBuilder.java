/* FichothequeLib_Tools - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.eligibility;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;


/**
 *
 * @author Vincent Calame
 */
public class MultiPredicateBuilder {

    private List<Predicate<SubsetItem>> list = new ArrayList<Predicate<SubsetItem>>();

    public MultiPredicateBuilder() {

    }

    public MultiPredicateBuilder add(Predicate<SubsetItem> predicate) {
        list.add(predicate);
        return this;
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public Predicate<SubsetItem> toPredicate() {
        Predicate<SubsetItem>[] array = list.toArray(new Predicate[list.size()]);
        return new InternalPredicate(array);
    }

    public static MultiPredicateBuilder init() {
        return new MultiPredicateBuilder();
    }


    private static class InternalPredicate implements Predicate<SubsetItem> {

        private final Predicate<SubsetItem>[] array;

        private InternalPredicate(Predicate<SubsetItem>[] array) {
            this.array = array;
        }

        @Override
        public boolean test(SubsetItem subsetItem) {
            for (Predicate<SubsetItem> predicate : array) {
                if (!predicate.test(subsetItem)) {
                    return false;
                }
            }
            return true;
        }

    }

}
