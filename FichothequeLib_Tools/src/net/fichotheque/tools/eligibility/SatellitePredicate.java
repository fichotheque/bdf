/* FichothequeLib_Tools - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.eligibility;

import java.util.function.Predicate;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;


/**
 *
 * @author Vincent Calame
 */
public class SatellitePredicate implements Predicate<SubsetItem> {

    private final Subset subset;
    private final Corpus[] satelliteCorpusArray;
    private final boolean here;

    public SatellitePredicate(Subset subset, Corpus[] satelliteCorpusArray, boolean here) {
        this.subset = subset;
        this.satelliteCorpusArray = satelliteCorpusArray;
        this.here = here;
    }

    @Override
    public boolean test(SubsetItem subsetItem) {
        if (!subsetItem.getSubset().equals(subset)) {
            return false;
        }
        int id = subsetItem.getId();
        if (here) {
            for (Corpus satelliteCorpus : satelliteCorpusArray) {
                if (satelliteCorpus.getFicheMetaById(id) != null) {
                    return true;
                }
            }
            return false;
        } else {
            for (Corpus satelliteCorpus : satelliteCorpusArray) {
                if (satelliteCorpus.getFicheMetaById(id) != null) {
                    return false;
                }
            }
            return true;
        }
    }

}
