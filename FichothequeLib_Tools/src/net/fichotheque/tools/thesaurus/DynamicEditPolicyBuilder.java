/* FichothequeLib_Tools - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.thesaurus;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.fichotheque.externalsource.ExternalSourceDef;
import net.fichotheque.thesaurus.policies.DynamicEditPolicy;
import net.fichotheque.utils.FichothequeUtils;


/**
 *
 * @author Vincent Calame
 */
public class DynamicEditPolicyBuilder {

    public DynamicEditPolicyBuilder() {

    }

    public static DynamicEditPolicy.Transfer buildTransfer(SubsetKey transferSubsetKey) {
        if (transferSubsetKey == null) {
            throw new IllegalArgumentException("transferSubsetKey is null");
        }
        if (!transferSubsetKey.isThesaurusSubset()) {
            throw new IllegalArgumentException("transferSubsetKey is not a thesaurus subsetKey");
        }
        return new TransferDynamicEditPolicy(transferSubsetKey);
    }

    public static DynamicEditPolicy.Check buildCheck(Collection<SubsetKey> subsetKeyCollection) {
        Set<SubsetKey> set = new LinkedHashSet<SubsetKey>();
        for (SubsetKey subsetKey : subsetKeyCollection) {
            if (subsetKey.isThesaurusSubset()) {
                set.add(subsetKey);
            } else {
                throw new IllegalArgumentException("subsetKeyCollection contains no thesaurus subsetKey: " + subsetKey);
            }
        }
        if (set.isEmpty()) {
            throw new IllegalArgumentException("subsetKeyCollection is empty");
        }
        return new CheckDynamicEditPolicy(FichothequeUtils.toList(set));
    }

    public static DynamicEditPolicy.External buildExternal(ExternalSourceDef externalSourceDef) {
        return new ExternalDynamicEditPolicy(externalSourceDef);
    }


    private static class TransferDynamicEditPolicy implements DynamicEditPolicy.Transfer {

        private final SubsetKey transferSubsetKey;

        private TransferDynamicEditPolicy(SubsetKey transferSubsetKey) {
            this.transferSubsetKey = transferSubsetKey;
        }

        @Override
        public SubsetKey getTransferThesaurusKey() {
            return transferSubsetKey;
        }

    }


    private static class CheckDynamicEditPolicy implements DynamicEditPolicy.Check {

        private final List<SubsetKey> list;

        private CheckDynamicEditPolicy(List<SubsetKey> list) {
            this.list = list;
        }

        @Override
        public List<SubsetKey> getCheckSubseKeyList() {
            return list;
        }

    }


    private static class ExternalDynamicEditPolicy implements DynamicEditPolicy.External {

        private final ExternalSourceDef externalSourceDef;

        private ExternalDynamicEditPolicy(ExternalSourceDef externalSourceDef) {
            this.externalSourceDef = externalSourceDef;
        }

        @Override
        public ExternalSourceDef getExternalSourceDef() {
            return externalSourceDef;
        }

    }

}
