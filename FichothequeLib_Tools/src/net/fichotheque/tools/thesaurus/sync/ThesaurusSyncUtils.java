/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.thesaurus.sync;

import java.io.IOException;
import java.net.URL;
import net.fichotheque.namespaces.SyncSpace;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.sync.ThesaurusSync;
import net.fichotheque.tools.thesaurus.sync.dom.KeywordsDOMReader;
import net.mapeadores.util.exceptions.ResponseCodeException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusSyncUtils {

    private ThesaurusSyncUtils() {

    }

    public static String getSyncUrl(Thesaurus thesaurus) {
        return thesaurus.getMetadata().getAttributes().getFirstValue(SyncSpace.URL_KEY);
    }

    public static ThesaurusSync download(URL url, MessageHandler messageHandler) throws SAXException, ResponseCodeException, IOException {
        Document document = DOMUtils.parseDocument(url);
        ThesaurusSyncBuilder thesaurusSyncBuilder = new ThesaurusSyncBuilder();
        KeywordsDOMReader.init(thesaurusSyncBuilder, messageHandler)
                .read(document.getDocumentElement());
        return thesaurusSyncBuilder.toThesaurusSync();
    }

}
