/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.thesaurus.sync;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusSyncResult {

    private int changeCount;
    private int creationCount;
    private int orderCount;

    public ThesaurusSyncResult() {

    }

    public int getChangeCount() {
        return changeCount;
    }

    public ThesaurusSyncResult setChangeCount(int changeCount) {
        this.changeCount = changeCount;
        return this;
    }

    public int getCreationCount() {
        return creationCount;
    }

    public ThesaurusSyncResult setCreationCount(int creationCount) {
        this.creationCount = creationCount;
        return this;
    }

    public int getOrderCount() {
        return orderCount;
    }

    public ThesaurusSyncResult setOrderCount(int orderCount) {
        this.orderCount = orderCount;
        return this;
    }

    public boolean isEmpty() {
        return ((changeCount == 0) && (creationCount == 0) && (orderCount == 0));
    }

    public static ThesaurusSyncResult init() {
        return new ThesaurusSyncResult();
    }

}
