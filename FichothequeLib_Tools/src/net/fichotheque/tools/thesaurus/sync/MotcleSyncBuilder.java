/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.thesaurus.sync;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.thesaurus.sync.MotcleSync;
import net.fichotheque.utils.SyncUtils;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.LabelChangeBuilder;


/**
 *
 * @author Vincent Calame
 */
public class MotcleSyncBuilder {

    private final String idalpha;
    private final LabelChangeBuilder labelChangeBuilder = new LabelChangeBuilder();
    private final AttributeChangeBuilder attributeChangeBuilder = new AttributeChangeBuilder();
    private final List<MotcleSyncBuilder> childList = new ArrayList<MotcleSyncBuilder>();
    private String status;

    public MotcleSyncBuilder(String idalpha) {
        this.idalpha = idalpha;
    }

    public String getIdalpha() {
        return idalpha;
    }

    public MotcleSyncBuilder setStatus(String status) {
        this.status = status;
        return this;
    }

    public MotcleSyncBuilder addChild(MotcleSyncBuilder builder) {
        childList.add(builder);
        return this;
    }

    public LabelChangeBuilder getLabelChangeBuilder() {
        return labelChangeBuilder;
    }

    public AttributeChangeBuilder getAttributeChangeBuilder() {
        return attributeChangeBuilder;
    }


    public MotcleSync toMotcleSync() {
        int size = childList.size();
        MotcleSync[] array = new MotcleSync[size];
        int p = 0;
        for (MotcleSyncBuilder builder : childList) {
            array[p] = builder.toMotcleSync();
            p++;
        }
        return new InternalMotcleSync(idalpha, status, labelChangeBuilder.toLabelChange(), attributeChangeBuilder.toAttributeChange(), SyncUtils.wrap(array));
    }

    public static MotcleSyncBuilder init(String idalpha) {
        return new MotcleSyncBuilder(idalpha);
    }


    private static class InternalMotcleSync implements MotcleSync {

        private final String idalpha;
        private final String status;
        private final List<MotcleSync> childList;
        private final LabelChange labelChange;
        private final AttributeChange attributeChange;

        private InternalMotcleSync(String idalpha, String status, LabelChange labelChange, AttributeChange attributeChange, List<MotcleSync> childList) {
            this.idalpha = idalpha;
            this.status = status;
            this.labelChange = labelChange;
            this.attributeChange = attributeChange;
            this.childList = childList;
        }

        @Override
        public String getIdalpha() {
            return idalpha;
        }

        @Override
        public String getStatus() {
            return status;
        }

        @Override
        public List<MotcleSync> getChildList() {
            return childList;
        }

        @Override
        public LabelChange getLabelChange() {
            return labelChange;
        }

        @Override
        public AttributeChange getAttributeChange() {
            return attributeChange;
        }

    }

}
