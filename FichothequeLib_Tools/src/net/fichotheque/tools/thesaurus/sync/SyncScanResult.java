/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.thesaurus.sync;

import net.fichotheque.importation.ThesaurusImport;


/**
 *
 * @author Vincent Calame
 */
public class SyncScanResult {

    private ThesaurusImport changeImport;
    private ThesaurusImport creationImport;

    public SyncScanResult() {

    }

    public ThesaurusImport getChangeImport() {
        return changeImport;
    }

    public ThesaurusImport getCreationImport() {
        return creationImport;
    }

    public SyncScanResult setChangeImport(ThesaurusImport changeImport) {
        this.changeImport = changeImport;
        return this;
    }

    public SyncScanResult setCreationImport(ThesaurusImport creationImport) {
        this.creationImport = creationImport;
        return this;
    }

    public static SyncScanResult init() {
        return new SyncScanResult();
    }

}
