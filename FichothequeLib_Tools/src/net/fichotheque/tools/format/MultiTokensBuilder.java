/* FichothequeLib_Tools - Copyright (c) 2012-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.format.Tokens;


/**
 *
 * @author Vincent Calame
 */
public class MultiTokensBuilder {

    private final List<Tokens> tokensList = new ArrayList<Tokens>();

    public MultiTokensBuilder() {
    }

    public void add(Tokens sourceTokens) {
        if (sourceTokens.size() > 0) {
            tokensList.add(sourceTokens);
        }
    }

    public Tokens toTokens() {
        List<GlobalToken> globalTokenList = new ArrayList<GlobalToken>();
        int p = 0;
        for (Tokens sourceTokens : tokensList) {
            int size = sourceTokens.size();
            for (int i = 0; i < size; i++) {
                globalTokenList.add(new GlobalToken(p, sourceTokens, i));
            }
            p++;
        }
        return new MultiTokens(globalTokenList.toArray(new GlobalToken[globalTokenList.size()]));
    }


    private static class MultiTokens extends AbstractList<String> implements Tokens {

        private final GlobalToken[] array;

        private MultiTokens(GlobalToken[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public String get(int i) {
            return array[i].format();
        }

    }

}
