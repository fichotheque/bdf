/* FichothequeLib_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.posttransform;

import javax.xml.transform.TransformerException;
import net.mapeadores.util.xml.json.XmlToJson;


/**
 *
 * @author Vincent Calame
 */
public abstract class Posttransformer {

    private final static Posttransformer JSONSTRUCTURE = new JsonStructurePosttransformer();
    private final static Posttransformer JSONML = new JsonMLPosttransformer();

    public Posttransformer() {
    }

    public abstract String posttransform(String result);

    public static Posttransformer getPosttransformer(String posttransform) {
        if (posttransform == null) {
            return null;
        }
        if (posttransform.length() == 0) {
            return null;
        }
        if (posttransform.equals(XmlToJson.JSONSTRUCTURE_NAME)) {
            return JSONSTRUCTURE;
        }
        if (posttransform.equals(XmlToJson.JSONML_NAME)) {
            return JSONML;
        }
        return new UnknownErrorPosttransformer(posttransform);
    }


    private static class JsonMLPosttransformer extends Posttransformer {

        private JsonMLPosttransformer() {
        }

        @Override
        public String posttransform(String result) {
            if (result == null) {
                return null;
            }
            result = result.trim();
            if (result.length() == 0) {
                return null;
            }
            try {
                return XmlToJson.transform(XmlToJson.JSONML_NAME, result);
            } catch (TransformerException te) {
                Throwable th = te.getCause();
                String message;
                if (th != null) {
                    message = th.getMessage();
                } else {
                    message = te.getMessage();
                }
                return "#TRANSFORM ERROR: " + message;
            }
        }

    }


    private static class JsonStructurePosttransformer extends Posttransformer {

        private JsonStructurePosttransformer() {
        }

        @Override
        public String posttransform(String result) {
            if (result == null) {
                return null;
            }
            result = result.trim();
            if (result.length() == 0) {
                return null;
            }
            try {
                return XmlToJson.transform(XmlToJson.JSONSTRUCTURE_NAME, result);
            } catch (TransformerException te) {
                Throwable th = te.getCause();
                String message;
                if (th != null) {
                    message = th.getMessage();
                } else {
                    message = te.getMessage();
                }
                return "#TRANSFORM ERROR: " + message;
            }
        }

    }


    private static class UnknownErrorPosttransformer extends Posttransformer {

        private final String posttransform;

        private UnknownErrorPosttransformer(String posttransform) {
            this.posttransform = posttransform;
        }

        @Override
        public String posttransform(String result) {
            return "#POSTTRANSFORM UNKNOWN: " + posttransform;
        }

    }

}
