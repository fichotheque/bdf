/* FichothequeLib_Tools - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.patterndefs;

import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;
import net.fichotheque.format.PatternDef;


/**
 *
 * @author Vincent Calame
 */
public class PatternDefUtils {

    private PatternDefUtils() {

    }

    public static List<PatternDef> wrap(PatternDef[] array) {
        return new PatternDefList(array);
    }

    public static List<PatternDef.ParameterDef> wrap(PatternDef.ParameterDef[] array) {
        return new ParameterDefList(array);
    }


    private static class PatternDefList extends AbstractList<PatternDef> implements RandomAccess {

        private final PatternDef[] array;

        private PatternDefList(PatternDef[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public PatternDef get(int index) {
            return array[index];
        }

    }


    private static class ParameterDefList extends AbstractList<PatternDef.ParameterDef> implements RandomAccess {

        private final PatternDef.ParameterDef[] array;

        private ParameterDefList(PatternDef.ParameterDef[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public PatternDef.ParameterDef get(int index) {
            return array[index];
        }

    }

}
