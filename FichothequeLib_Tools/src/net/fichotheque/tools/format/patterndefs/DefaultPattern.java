/* FichothequeLib_Tools - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.patterndefs;

import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.mapeadores.util.exceptions.SwitchException;


/**
 *
 * @author Vincent Calame
 */
public final class DefaultPattern {

    private DefaultPattern() {

    }

    public static String attribute() {
        return "{value}";
    }

    public static String document() {
        return "{basename}{extensions}";
    }

    public static String extraction() {
        return "{xml}";
    }

    public static String ficheBlock() {
        return "{text}";
    }

    public static String ficheItem(short ficheItemType) {
        switch (ficheItemType) {
            case CorpusField.PERSONNE_FIELD:
                return "{normal}";
            case CorpusField.LANGUE_FIELD:
                return "{code}";
            case CorpusField.DATATION_FIELD:
                return "{code}";
            case CorpusField.PAYS_FIELD:
                return "{code}";
            case CorpusField.NOMBRE_FIELD:
                return "{code}";
            case CorpusField.MONTANT_FIELD:
                return "{code}";
            case CorpusField.COURRIEL_FIELD:
                return "{address}";
            case CorpusField.LINK_FIELD:
                return "{href}";
            case CorpusField.ITEM_FIELD:
                return "{value}";
            case CorpusField.GEOPOINT_FIELD:
                return "{lat} {lon}";
            case CorpusField.PARA_FIELD:
                return "{raw}";
            case CorpusField.IMAGE_FIELD:
                return "{src}";
            default:
                throw new SwitchException("ficheItemType = " + ficheItemType);
        }
    }

    public static String ficheMeta() {
        return "{titre}";
    }

    public static String illustration() {
        return "{album}-{id}.{format}";
    }

    public static String motcle(boolean withIdalpha) {
        if (withIdalpha) {
            return "{idalpha}";
        } else {
            return "{label}";
        }
    }

    public static String subfield(short subfieldType) {
        switch (subfieldType) {
            case SubfieldKey.SURNAME_SUBTYPE:
            case SubfieldKey.FORENAME_SUBTYPE:
            case SubfieldKey.NONLATIN_SUBTYPE:
            case SubfieldKey.SURNAMEFIRST_SUBTYPE:
            case SubfieldKey.SRC_SUBTYPE:
            case SubfieldKey.ALT_SUBTYPE:
            case SubfieldKey.TITLE_SUBTYPE:
            case SubfieldKey.LATITUDE_SUBTYPE:
            case SubfieldKey.LONGITUDE_SUBTYPE:
            case SubfieldKey.CURRENCY_SUBTYPE:
            case SubfieldKey.VALUE_SUBTYPE:
                return "{value}";
            case SubfieldKey.OTHERS_SUBTYPE:
                return "{code}";
            case SubfieldKey.AMOUNT_SUBTYPE:
                return "{value}";
            case SubfieldKey.LANG_SUBTYPE:
            default:
                throw new SwitchException("subfieldType = " + subfieldType);
        }
    }

    public static String value() {
        return "{value}";
    }

}
