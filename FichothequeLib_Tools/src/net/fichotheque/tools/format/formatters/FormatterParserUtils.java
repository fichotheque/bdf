/* FichothequeLib_Tools - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.formatters;

import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.format.FormatSource;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.instruction.Argument;
import net.mapeadores.util.instruction.Instruction;
import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
public final class FormatterParserUtils {

    private FormatterParserUtils() {

    }

    public static boolean isUppercase(Instruction instruction) {
        for (Argument argument : instruction) {
            String key = argument.getKey();
            if (key.equals("up")) {
                return true;
            }
        }
        return false;
    }

    public static String toUppercase(String value, FormatSource formatSource) {
        return value.toUpperCase(formatSource.getDefaultFormatLocale());
    }

    public static PersonCore toPersonCore(FicheItem ficheItem, FormatSource formatSource) {
        return SphereUtils.toPersonCore(formatSource.getFichotheque(), (Personne) ficheItem);
    }

    public static Cell[] resolve(SubsetItem subsetItem, JsonParameters jsonParameters, FormatSource formatSource) {
        Cell[] cellArray = null;
        String cellEngineName = jsonParameters.getCellEngineName();
        if (cellEngineName != null) {
            CellEngine cellEngine = formatSource.getCellEngineProvider().getCellEngine(cellEngineName);
            if (cellEngine != null) {
                cellArray = cellEngine.toCellArray(subsetItem, formatSource.getGlobalPredicate());
            }
        }
        return cellArray;
    }

}
