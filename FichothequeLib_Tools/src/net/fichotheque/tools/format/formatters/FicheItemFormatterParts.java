/* FichothequeLib_Tools  - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.formatters;

import java.io.IOException;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.fiche.Link;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.fiche.Nombre;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Pays;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.formatters.FicheBlockFormatter;
import net.fichotheque.format.formatters.FicheItemFormatter;
import net.fichotheque.json.FicheItemJson;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.syntax.FormSyntax;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.localisation.SpecialCodes;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.money.ExtendedCurrency;
import net.mapeadores.util.money.MoneyUtils;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.DegreSexagesimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.DateFormatBundle;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
final class FicheItemFormatterParts {

    public final static FicheItemFormatter PAYS_CODE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Pays) {
            return ((Pays) ficheItem).getCountry().toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter LANGUE_CODE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Langue) {
            return ((Langue) ficheItem).getLang().toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter ITEM_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Item) {
            return ((Item) ficheItem).getValue();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PARA_RAW_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Para) {
            return ((Para) ficheItem).contentToString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter GEO_LAT_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Geopoint) {
            return ((Geopoint) ficheItem).getLatitude().toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter GEO_LON_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Geopoint) {
            return ((Geopoint) ficheItem).getLongitude().toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter LINK_HREF_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Link) {
            return ((Link) ficheItem).getHref();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter LINK_TITLE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Link) {
            return ((Link) ficheItem).getTitle();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter LINK_COMMENT_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Link) {
            return ((Link) ficheItem).getComment();
        }
        return getDefaultFormat(ficheItem);
    };
    public final static FicheItemFormatter IMAGE_SRC_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Image) {
            return ((Image) ficheItem).getSrc();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter IMAGE_ALT_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Image) {
            return ((Image) ficheItem).getAlt();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter IMAGE_TITLE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Image) {
            return ((Image) ficheItem).getTitle();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter COURRIEL_COMPLETE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Courriel) {
            return ((Courriel) ficheItem).getEmailCore().toCompleteString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter COURRIEL_ADDRESS_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Courriel) {
            return ((Courriel) ficheItem).getEmailCore().getAddrSpec();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter COURRIEL_NAME_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Courriel) {
            return ((Courriel) ficheItem).getEmailCore().getRealName();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter NOMBRE_CODE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Nombre) {
            return ((Nombre) ficheItem).getDecimal().toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter MONTANT_CODE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Montant) {
            return ((Montant) ficheItem).toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter MONTANT_CURRENCY_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Montant) {
            return ((Montant) ficheItem).getCurrency().getCurrencyCode();
        }
        return getDefaultFormat(ficheItem);
    };
    public final static FicheItemFormatter MONTANT_DECIMAL_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Montant) {
            return ((Montant) ficheItem).getDecimal().toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter MONTANT_MONEYLONG_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Montant) {
            return String.valueOf(((Montant) ficheItem).toMoneyLong());
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter DATATION_CODE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Datation) {
            return ((Datation) ficheItem).getDate().toString();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter DATATION_ISO_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Datation) {
            return ((Datation) ficheItem).getDate().toISOString(false);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter DATATION_ISO_LAST_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Datation) {
            return ((Datation) ficheItem).getDate().toISOString(true);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter DATATION_YEAR_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Datation) {
            return String.valueOf(((Datation) ficheItem).getDate().getYear());
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter DATATION_ISOMONTH_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Datation) {
            return ((Datation) ficheItem).getDate().toMonthString(false);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter DATATION_ISOMONTH_LAST_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Datation) {
            return ((Datation) ficheItem).getDate().toMonthString(true);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_LOGIN_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            String redacteurGlobalId = ((Personne) ficheItem).getRedacteurGlobalId();
            if (redacteurGlobalId != null) {
                Redacteur redacteur = SphereUtils.getRedacteur(formatSource.getFichotheque(), redacteurGlobalId);
                if (redacteur != null) {
                    return redacteur.getLogin();
                } else {
                    return "";
                }
            } else {
                return "";
            }
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_SPHERE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            String redacteurGlobalId = ((Personne) ficheItem).getRedacteurGlobalId();
            if (redacteurGlobalId != null) {
                try {
                    SubsetKey sphereKey = SphereUtils.getSubsetKey(redacteurGlobalId);
                    return sphereKey.getSubsetName();
                } catch (ParseException pe) {
                    return "#ERR: wrong redacteurGlobaleId = " + redacteurGlobalId;
                }
            } else {
                return "";
            }
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_CODE_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            String redacteurGlobalId = ((Personne) ficheItem).getRedacteurGlobalId();
            if (redacteurGlobalId != null) {
                Redacteur redacteur = SphereUtils.getRedacteur(formatSource.getFichotheque(), redacteurGlobalId);
                if (redacteur != null) {
                    return redacteur.getBracketStyle();
                } else {
                    return redacteurGlobalId;
                }
            } else {
                return ((Personne) ficheItem).getPersonCore().toStandardStyle();
            }
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_STANDARD_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            PersonCore personCore = FormatterParserUtils.toPersonCore(ficheItem, formatSource);
            return personCore.toStandardStyle();
        }
        return getDefaultFormat(ficheItem);
    };
    public final static FicheItemFormatter PERSONNE_DIRECTORY_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            PersonCore personCore = FormatterParserUtils.toPersonCore(ficheItem, formatSource);
            return personCore.toDirectoryStyle(false);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_UPDIRECTORY_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            PersonCore personCore = FormatterParserUtils.toPersonCore(ficheItem, formatSource);
            return personCore.toDirectoryStyle(true);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_BIBLIO_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            PersonCore personCore = FormatterParserUtils.toPersonCore(ficheItem, formatSource);
            return personCore.toBiblioStyle(false);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_UPBIBLIO_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            PersonCore personCore = FormatterParserUtils.toPersonCore(ficheItem, formatSource);
            return personCore.toBiblioStyle(true);
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_SURNAME_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            return FormatterParserUtils.toPersonCore(ficheItem, formatSource).getSurname();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_UPSURNAME_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            return FormatterParserUtils.toPersonCore(ficheItem, formatSource).getSurname().toUpperCase();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_FORENAME_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            return FormatterParserUtils.toPersonCore(ficheItem, formatSource).getForename();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_NONLATIN_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            return FormatterParserUtils.toPersonCore(ficheItem, formatSource).getNonlatin();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_SURNAMEFIRST_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            PersonCore personCore = FormatterParserUtils.toPersonCore(ficheItem, formatSource);
            return (personCore.isSurnameFirst()) ? "1" : "0";
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter PERSONNE_ORGANISM_PART = (ficheItem, formatSource) -> {
        if (ficheItem instanceof Personne) {
            return ((Personne) ficheItem).getOrganism();
        } else {
            return getDefaultFormat(ficheItem);
        }
    };
    public final static FicheItemFormatter FORMSYNTAX_FORMATTER = new FormSyntaxPart();

    private FicheItemFormatterParts() {
    }

    public static FicheItemFormatter newLinkHrefPart(String base) {
        if (base == null) {
            return LINK_HREF_PART;
        }
        return new LinkHrefPart(base);
    }

    public static FicheItemFormatter newImageSrcPart(String base) {
        if (base == null) {
            return FicheItemFormatterParts.IMAGE_SRC_PART;
        }
        return new ImageSrcPart(base);
    }

    public static FicheItemFormatter newPaysLabelPart(LangParameters langParameters) {
        return new PaysLabelPart(langParameters);
    }

    public static FicheItemFormatter newLangueLabelPart(LangParameters langParameters) {
        return new LangueLabelPart(langParameters);
    }

    public static FicheItemFormatter newGeoLatLabelPart(LangParameters langParameters) {
        return new GeoLatLabelPart(langParameters);
    }

    public static FicheItemFormatter newGeoLonLabelPart(LangParameters langParameters) {
        return new GeoLonLabelPart(langParameters);
    }

    public static FicheItemFormatter newNombreLabelPart(LangParameters langParameters) {
        return new NombreLabelPart(langParameters);
    }

    public static FicheItemFormatter newMontantLabelPart(LangParameters langParameters) {
        return new MontantLabelPart(langParameters);
    }

    public static FicheItemFormatter newDatationLabelPart(LangParameters langParameters, short truncateType) {
        return new DatationLabelPart(langParameters, truncateType);
    }

    public static FicheItemFormatter newJsonPart(JsonParameters jsonParameters, LangParameters langParameters) {
        return new JsonPart(jsonParameters, langParameters.getCustomLangContext());
    }

    public static FicheItemFormatter newParaTransformationPart(FicheBlockFormatter ficheBlockFormatter) {
        return new ParaTransformationPart(ficheBlockFormatter);
    }

    public static FicheItemFormatter newDatationPatternPart(DateTimeFormatter dateTimeFormatter) {
        return new DatationPatternPart(dateTimeFormatter);
    }

    private static String getDefaultFormat(FicheItem ficheItem) {
        if (ficheItem instanceof Item) {
            return ((Item) ficheItem).getValue();
        }
        return "#ERROR: wrong ficheItem = " + ficheItem.getClass().getName();
    }


    public static class JsonPart implements FicheItemFormatter {

        private final JsonParameters jsonParameters;
        private final LangContext customLangContext;

        private JsonPart(JsonParameters jsonParameters, LangContext customLangContext) {
            this.jsonParameters = jsonParameters;
            this.customLangContext = customLangContext;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            StringBuilder buf = new StringBuilder();
            JSONWriter jsonWriter = new JSONWriter(buf);
            try {
                LangContext langContext;
                if (customLangContext != null) {
                    langContext = customLangContext;
                } else {
                    langContext = formatSource.getLangContext();
                }
                FicheItemJson.object(jsonWriter, ficheItem, jsonParameters, langContext, formatSource.getFichotheque(), formatSource.getFormatContext().getMessageLocalisationProvider());
            } catch (IOException ioe) {
                throw new ShouldNotOccurException(ioe);
            }
            return buf.toString();
        }

    }


    private static class PaysLabelPart implements FicheItemFormatter {

        private final LangParameters langParameters;

        private PaysLabelPart(LangParameters langParameters) {
            this.langParameters = langParameters;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Pays)) {
                return getDefaultFormat(ficheItem);
            }
            Pays pays = (Pays) ficheItem;
            String countryCode = pays.getCountry().toString();
            StringBuilder buf = new StringBuilder();
            Lang[] langArray = LangParameters.checkLangArray(langParameters, formatSource);
            for (Lang lang : langArray) {
                MessageLocalisation messageLocalisation = formatSource.getMessageLocalisation(lang);
                String message = messageLocalisation.toString(countryCode);
                if (message != null) {
                    message = StringUtils.getFirstPart(message);
                    if (buf.length() > 0) {
                        buf.append(langParameters.getSeparator());
                    }
                    buf.append(message);
                }
            }
            return buf.toString();
        }

    }


    private static class LangueLabelPart implements FicheItemFormatter {

        private final LangParameters langParameters;

        private LangueLabelPart(LangParameters langParameters) {
            this.langParameters = langParameters;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Langue)) {
                return getDefaultFormat(ficheItem);
            }
            Langue langue = (Langue) ficheItem;
            String langCode = langue.getLang().toString();
            StringBuilder buf = new StringBuilder();
            Lang[] langArray = LangParameters.checkLangArray(langParameters, formatSource);
            for (Lang lang : langArray) {
                MessageLocalisation messageLocalisation = formatSource.getMessageLocalisation(lang);
                String message = messageLocalisation.toString(langCode);
                if (message != null) {
                    message = StringUtils.getFirstPart(message);
                    if (buf.length() > 0) {
                        buf.append(langParameters.getSeparator());
                    }
                    buf.append(message);
                }
            }
            return buf.toString();
        }

    }


    private static class GeoLatLabelPart implements FicheItemFormatter {

        private final LangParameters langParameters;

        private GeoLatLabelPart(LangParameters langParameters) {
            this.langParameters = langParameters;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Geopoint)) {
                return getDefaultFormat(ficheItem);
            }
            StringBuilder buf = new StringBuilder();
            DegreSexagesimal sexa = DegreSexagesimal.fromDegreDecimal(((Geopoint) ficheItem).getLatitude());
            Lang[] langArray = LangParameters.checkLangArray(langParameters, formatSource);
            for (Lang lang : langArray) {
                if (buf.length() > 0) {
                    buf.append(langParameters.getSeparator());
                }
                MessageLocalisation messageLocalisation = formatSource.getMessageLocalisation(lang);
                String code = SpecialCodes.getLatitudeSpecialCode(sexa);
                buf.append(sexa.toString(false, " "));
                String message = messageLocalisation.toString(code);
                if (message != null) {
                    buf.append(message);
                } else {
                    buf.append(code);
                }
            }
            return buf.toString();
        }

    }


    private static class GeoLonLabelPart implements FicheItemFormatter {

        private final LangParameters langParameters;

        private GeoLonLabelPart(LangParameters langParameters) {
            this.langParameters = langParameters;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Geopoint)) {
                return getDefaultFormat(ficheItem);
            }
            StringBuilder buf = new StringBuilder();
            DegreSexagesimal sexa = DegreSexagesimal.fromDegreDecimal(((Geopoint) ficheItem).getLongitude());
            Lang[] langArray = LangParameters.checkLangArray(langParameters, formatSource);
            for (Lang lang : langArray) {
                if (buf.length() > 0) {
                    buf.append(langParameters.getSeparator());
                }
                MessageLocalisation messageLocalisation = formatSource.getMessageLocalisation(lang);
                String code = SpecialCodes.getLongitudeSpecialCode(sexa);
                buf.append(sexa.toString(false, " "));
                String message = messageLocalisation.toString(code);
                if (message != null) {
                    buf.append(message);
                } else {
                    buf.append(code);
                }
            }
            return buf.toString();
        }

    }


    private static class NombreLabelPart implements FicheItemFormatter {

        private final LangParameters langParameters;

        private NombreLabelPart(LangParameters langParameters) {
            this.langParameters = langParameters;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Nombre)) {
                return getDefaultFormat(ficheItem);
            }
            Decimal decimal = ((Nombre) ficheItem).getDecimal();
            StringBuilder buf = new StringBuilder();
            Locale[] array = LangParameters.checkLocaleArray(langParameters, formatSource);
            int length = array.length;
            for (int i = 0; i < length; i++) {
                if (i > 0) {
                    buf.append(langParameters.getSeparator());
                }
                appendLocale(buf, decimal, array[i]);
            }
            return buf.toString();
        }

        private void appendLocale(StringBuilder buf, Decimal decimal, Locale locale) {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            buf.append(decimal.toString(symbols));
        }

    }


    private static class MontantLabelPart implements FicheItemFormatter {

        private final LangParameters langParameters;

        private MontantLabelPart(LangParameters langParameters) {
            this.langParameters = langParameters;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Montant)) {
                return getDefaultFormat(ficheItem);
            }
            StringBuilder buf = new StringBuilder();
            Montant montant = (Montant) ficheItem;
            Decimal decimal = montant.getDecimal();
            ExtendedCurrency currency = montant.getCurrency();
            Locale[] array = LangParameters.checkLocaleArray(langParameters, formatSource);
            int length = array.length;
            for (int i = 0; i < length; i++) {
                if (i > 0) {
                    buf.append(langParameters.getSeparator());
                }
                appendLocale(buf, decimal, currency, array[i]);
            }
            return buf.toString();
        }

        private void appendLocale(StringBuilder buf, Decimal decimal, ExtendedCurrency currency, Locale locale) {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(locale);
            buf.append(MoneyUtils.toLitteralString(decimal, currency, symbols));
        }

    }


    private static class LinkHrefPart implements FicheItemFormatter {

        private final String base;

        private LinkHrefPart(String base) {
            this.base = base;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (ficheItem instanceof Link) {
                String href = ((Link) ficheItem).getHref();
                if (!StringUtils.isAbsoluteUrlString(href)) {
                    href = base + href;
                }
                return href;
            } else {
                return getDefaultFormat(ficheItem);
            }
        }


    }


    private static class ImageSrcPart implements FicheItemFormatter {

        private final String base;

        private ImageSrcPart(String base) {
            this.base = base;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (ficheItem instanceof Image) {
                String src = ((Image) ficheItem).getSrc();
                if (!StringUtils.isAbsoluteUrlString(src)) {
                    src = base + src;
                }
                return src;
            } else {
                return getDefaultFormat(ficheItem);
            }
        }


    }


    private static class DatationLabelPart implements FicheItemFormatter {

        private final LangParameters langParameters;
        private final short truncateType;

        private DatationLabelPart(LangParameters langParameters, short truncateType) {
            this.langParameters = langParameters;
            this.truncateType = truncateType;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Datation)) {
                return getDefaultFormat(ficheItem);
            }
            Datation datation = (Datation) ficheItem;
            StringBuilder buf = new StringBuilder();
            FuzzyDate date = datation.getDate();
            if (truncateType != FuzzyDate.DAY_TYPE) {
                date = date.truncate(truncateType);
            }
            Locale[] array = LangParameters.checkLocaleArray(langParameters, formatSource);
            int length = array.length;
            for (int i = 0; i < length; i++) {
                if (i > 0) {
                    buf.append(langParameters.getSeparator());
                }
                appendLocale(buf, date, array[i]);
            }
            return buf.toString();
        }

        private void appendLocale(StringBuilder buf, FuzzyDate date, Locale locale) {
            DateFormatBundle dateFormatBundle = DateFormatBundle.getDateFormatBundle(locale);
            buf.append(date.getDateLitteral(dateFormatBundle));
        }

    }


    private static class DatationPatternPart implements FicheItemFormatter {

        private final DateTimeFormatter dateTimeFormatter;

        private DatationPatternPart(DateTimeFormatter dateTimeFormatter) {
            this.dateTimeFormatter = dateTimeFormatter;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (!(ficheItem instanceof Datation)) {
                return getDefaultFormat(ficheItem);
            }
            Datation datation = (Datation) ficheItem;
            return datation.toLocalDate(false).format(dateTimeFormatter);
        }

    }


    private static class FormSyntaxPart implements FicheItemFormatter {

        private FormSyntaxPart() {
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            return FormSyntax.toString(ficheItem, formatSource.getFichotheque(), null);
        }

    }


    private static class ParaTransformationPart implements FicheItemFormatter {

        private final FicheBlockFormatter ficheBlockFormatter;

        private ParaTransformationPart(FicheBlockFormatter ficheBlockFormatter) {
            this.ficheBlockFormatter = ficheBlockFormatter;
        }

        @Override
        public String formatFicheItem(FicheItem ficheItem, FormatSource formatSource) {
            if (ficheItem instanceof Para) {
                SubsetKey corpusKey = null;
                Subset currentSubset = formatSource.getSubsetItemPointeur().getSubset();
                if (currentSubset instanceof Corpus) {
                    corpusKey = currentSubset.getSubsetKey();
                }
                return ficheBlockFormatter.formatFicheBlocks(((Para) ficheItem).toFicheBlocks(), formatSource, corpusKey);
            }
            return getDefaultFormat(ficheItem);
        }

    }

}
