/* FichothequeLib_Tools - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.formatters;

import java.io.IOException;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.format.FormatContext;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.formatters.ExtractionFormatter;
import net.fichotheque.format.formatters.FicheBlockFormatter;
import net.fichotheque.syntax.FicheblockSyntax;
import net.fichotheque.tools.format.patterndefs.DefaultPattern;
import net.fichotheque.utils.FormatterUtils;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.instruction.Argument;
import net.mapeadores.util.instruction.Instruction;
import net.mapeadores.util.instruction.InstructionResolver;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public final class FicheBlockFormatterParser {

    private final static FicheblockSyntax.Parameters DEFAULT_PARAMETERS = FicheblockSyntax.parameters().withSpecialParseChars(true);
    private final static FicheBlockFormatter FORMSYNTAX_PART = (ficheBlocks, formatSource, defaultCorpusKey) -> {
        return FicheblockSyntax.toString(ficheBlocks, DEFAULT_PARAMETERS);
    };
    private final static FicheBlockFormatter TEXT_PART = (ficheBlocks, formatSource, defaultCorpusKey) -> {
        StringBuilder appendable = new StringBuilder();
        try {
            FicheBlockFormat.textFormat(appendable, ficheBlocks, "\n");
        } catch (IOException ioe) {
        }
        return appendable.toString();
    };

    private FicheBlockFormatterParser() {
    }

    public static FicheBlockFormatter parse(String pattern, FormatContext formatContext, MessageHandler messageHandler) {
        if (pattern == null) {
            pattern = DefaultPattern.ficheBlock();
        }
        try {
            return parse(pattern, formatContext);
        } catch (ErrorMessageException fe) {
            messageHandler.addMessage(FormatConstants.SEVERE_PATTERN, fe.getErrorMessage());
            return null;
        }
    }


    public static FicheBlockFormatter parse(String pattern, FormatContext formatContext) throws ErrorMessageException {
        Object[] partArray = FormatterUtils.parsePattern(new InternalInstructionResolver(formatContext), pattern);
        return new InternalFicheBlockFormatter(partArray);
    }


    private static class InternalFicheBlockFormatter implements FicheBlockFormatter {

        private final Object[] partArray;

        private InternalFicheBlockFormatter(Object[] partArray) {
            this.partArray = partArray;
        }

        @Override
        public String formatFicheBlocks(FicheBlocks ficheBlocks, FormatSource formatSource, SubsetKey defaultCorpusKey) {
            StringBuilder buf = new StringBuilder();
            int length = partArray.length;
            for (int i = 0; i < length; i++) {
                Object obj = partArray[i];
                if (obj instanceof String) {
                    buf.append((String) obj);
                    continue;
                }
                FicheBlockFormatter formatter = (FicheBlockFormatter) obj;
                buf.append(formatter.formatFicheBlocks(ficheBlocks, formatSource, defaultCorpusKey));
            }
            return buf.toString();
        }

    }


    private static class InternalInstructionResolver implements InstructionResolver {

        private final FormatContext formatContext;

        private InternalInstructionResolver(FormatContext formatContext) {
            this.formatContext = formatContext;
        }

        @Override
        public Object resolve(Instruction instruction) throws ErrorMessageException {
            InstructionResolver resolver = formatContext.getInstructionResolverProvider().getInstructionResolver(ExtractionFormatter.class);
            if (resolver != null) {
                Object formatter = resolver.resolve(instruction);
                if (formatter != null) {
                    return formatter;
                }
            }
            Argument arg1 = instruction.get(0);
            String key = arg1.getKey();
            switch (key) {
                case "formsyntax":
                    return FORMSYNTAX_PART;
                case "text":
                    return TEXT_PART;
                case "values":
                    return new ValuesPart(ValuesParameters.fromInstruction(instruction));
                case "xml":
                    return formatContext.getFicheBlockFormatter(FormatContext.XML_SPECIAL_FORMATTER_NAME, instruction.toOptionMap(1));
                case "transformation":
                case "xslt":
                    String name = arg1.getValue();
                    FicheBlockFormatter ficheBlockFormatter = formatContext.getFicheBlockFormatter(name, instruction.toOptionMap(1));
                    if (ficheBlockFormatter == null) {
                        throw new ErrorMessageException("_ error.unknown.template", name);
                    }
                    return ficheBlockFormatter;
                default:
                    return null;
            }
        }

    }


    private static class ValuesPart implements FicheBlockFormatter {

        private final ValuesParameters valuesParameters;

        private ValuesPart(ValuesParameters valuesParameters) {
            this.valuesParameters = valuesParameters;
        }

        @Override
        public String formatFicheBlocks(FicheBlocks ficheBlocks, FormatSource formatSource, SubsetKey defaultCorpusKey) {
            int limit = valuesParameters.getLimit();
            int startIndex = valuesParameters.getStartIndex();
            String separator = valuesParameters.getSeparator();
            List<String> list = FicheBlockFormat.valuesFormat(ficheBlocks);
            int size = list.size();
            if (limit != -1) {
                size = Math.min(size, startIndex + limit);
            }
            StringBuilder buf = new StringBuilder();
            boolean first = true;
            for (int i = startIndex; i < size; i++) {
                if (first) {
                    first = false;
                } else {
                    buf.append(separator);
                }
                buf.append(list.get(i));
            }
            return buf.toString();
        }

    }

}
