/* FichothequeLib_Tools - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.formatters;

import java.io.IOException;
import net.fichotheque.album.Illustration;
import net.fichotheque.format.FormatContext;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.formatters.IllustrationFormatter;
import net.fichotheque.json.IllustrationJson;
import net.fichotheque.tools.format.patterndefs.DefaultPattern;
import net.fichotheque.utils.FormatterUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.instruction.Argument;
import net.mapeadores.util.instruction.Instruction;
import net.mapeadores.util.instruction.InstructionResolver;
import net.mapeadores.util.instruction.InstructionResolverProvider;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public final class IllustrationFormatterParser {

    private final static IllustrationFormatter ALBUM_PART = (illustration, poids, formatSource) -> {
        return illustration.getSubsetName();
    };
    private final static IllustrationFormatter FORMAT_PART = (illustration, poids, formatSource) -> {
        return illustration.getFormatTypeString();
    };
    private final static IllustrationFormatter ID_PART = (illustration, poids, formatSource) -> {
        return String.valueOf(illustration.getId());
    };
    private final static IllustrationFormatter POIDS_PART = (illustration, poids, formatSource) -> {
        return String.valueOf(poids);
    };
    private final static IllustrationFormatter WITHPOIDSFILTER_FORMSYNTAX_PART = new FormSyntaxPart(true);
    private final static IllustrationFormatter WITHOUTPOIDSFILTER_FORMSYNTAX_PART = new FormSyntaxPart(false);
    private final static JsonParameters DEFAULT_JSONPARAMETERS = JsonParameters.parse("format");

    private IllustrationFormatterParser() {
    }

    public static IllustrationFormatter parse(String pattern, FormatContext formatContext, boolean withPoidsFilter, MessageHandler messageHandler) {
        if (pattern == null) {
            pattern = DefaultPattern.illustration();
        }
        try {
            return parse(pattern, formatContext, withPoidsFilter);
        } catch (ErrorMessageException fe) {
            messageHandler.addMessage(FormatConstants.SEVERE_PATTERN, fe.getErrorMessage());
            return null;
        }
    }

    public static IllustrationFormatter parse(String pattern, FormatContext formatContext, boolean withPoidsFilter) throws ErrorMessageException {
        Object[] partArray = FormatterUtils.parsePattern(new InternalInstructionResolver(formatContext, withPoidsFilter), pattern);
        return new InternalIllustrationFormatter(partArray);
    }


    private static class InternalIllustrationFormatter implements IllustrationFormatter {

        private final Object[] partArray;

        private InternalIllustrationFormatter(Object[] partArray) {
            this.partArray = partArray;
        }

        @Override
        public String formatIllustration(Illustration illustration, int poids, FormatSource formatSource) {
            StringBuilder buf = new StringBuilder();
            int length = partArray.length;
            for (int i = 0; i < length; i++) {
                Object obj = partArray[i];
                if (obj instanceof String) {
                    buf.append((String) obj);
                    continue;
                }
                IllustrationFormatter formatter = (IllustrationFormatter) obj;
                buf.append(formatter.formatIllustration(illustration, poids, formatSource));
            }
            return buf.toString();
        }

    }


    private static class InternalInstructionResolver implements InstructionResolver {

        private final InstructionResolverProvider provider;
        private final boolean withPoidsFilter;

        private InternalInstructionResolver(FormatContext formatContext, boolean withPoidsFilter) {
            this.provider = formatContext.getInstructionResolverProvider();
            this.withPoidsFilter = withPoidsFilter;
        }

        @Override
        public Object resolve(Instruction instruction) throws ErrorMessageException {
            InstructionResolver resolver = provider.getInstructionResolver(IllustrationFormatter.class);
            if (resolver != null) {
                Object formatter = resolver.resolve(instruction);
                if (formatter != null) {
                    return formatter;
                }
            }
            Argument arg1 = instruction.get(0);
            String key = arg1.getKey();
            switch (key) {
                case "album":
                    return ALBUM_PART;
                case "format":
                    return FORMAT_PART;
                case "formsyntax":
                    if (withPoidsFilter) {
                        return WITHPOIDSFILTER_FORMSYNTAX_PART;
                    } else {
                        return WITHOUTPOIDSFILTER_FORMSYNTAX_PART;
                    }
                case "id":
                case "idalbum":
                    return ID_PART;
                case "json": {
                    JsonParameters jsonParameters = JsonParameters.fromInstruction(instruction);
                    if (jsonParameters == null) {
                        jsonParameters = DEFAULT_JSONPARAMETERS;
                    }
                    return new JsonPart(jsonParameters);
                }
                case "poids":
                    return POIDS_PART;
                default:
                    return null;
            }
        }

    }


    private static class JsonPart implements IllustrationFormatter {

        private final JsonParameters jsonParameters;

        private JsonPart(JsonParameters jsonParameters) {
            this.jsonParameters = jsonParameters;
        }

        @Override
        public String formatIllustration(Illustration illustration, int poids, FormatSource formatSource) {
            StringBuilder buf = new StringBuilder();
            JSONWriter jsonWriter = new JSONWriter(buf);
            try {
                jsonWriter.object();
                IllustrationJson.properties(jsonWriter, illustration, jsonParameters);
                jsonWriter.endObject();
            } catch (IOException ioe) {
                throw new ShouldNotOccurException(ioe);
            }
            return buf.toString();
        }

    }


    private static class FormSyntaxPart implements IllustrationFormatter {

        private final boolean withPoidsFilter;

        private FormSyntaxPart(boolean withPoidsFilter) {
            this.withPoidsFilter = withPoidsFilter;
        }

        @Override
        public String formatIllustration(Illustration illustration, int poids, FormatSource formatSource) {
            if ((withPoidsFilter) || (poids == 1)) {
                return String.valueOf(illustration.getId());
            } else {
                StringBuilder buf = new StringBuilder();
                buf.append(String.valueOf(illustration.getId()));
                buf.append(" <");
                buf.append(poids);
                buf.append(">");
                return buf.toString();
            }
        }

    }

}
