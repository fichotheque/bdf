/* FichothequeLib_Tools - Copyright (c) 2016-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.formatters;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.instruction.Argument;
import net.mapeadores.util.instruction.Instruction;
import net.mapeadores.util.json.PropertyEligibility;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class JsonParameters implements PropertyEligibility {

    private final static String PROPERTIES_PREFIX = "properties=";
    private final Set<String> propertyNameSet;
    private final String cellEngineName;

    public JsonParameters(Set<String> propertyNameSet, String cellEngineName) {
        this.propertyNameSet = propertyNameSet;
        this.cellEngineName = cellEngineName;
    }

    @Override
    public boolean includeProperty(String propertyName) {
        if (propertyName.equals("properties")) {
            return (cellEngineName != null);
        }
        return propertyNameSet.contains(propertyName);
    }

    @Nullable
    public String getCellEngineName() {
        return cellEngineName;
    }

    /**
     * Peut être nul.
     *
     * @param instruction
     * @return instance de JsonParameters ou null
     */
    public static JsonParameters fromInstruction(Instruction instruction) {
        Set<String> propertyNameSet = new HashSet<String>();
        boolean here = false;
        String cellEngineName = null;
        for (Argument argument : instruction) {
            String key = argument.getKey();
            String value = argument.getValue();
            switch (key) {
                case "include": {
                    here = true;
                    if (value != null) {
                        String[] tokens = StringUtils.getTechnicalTokens(value, true);
                        propertyNameSet.addAll(Arrays.asList(tokens));
                    }
                    break;
                }
                case "properties":
                case "cells": {
                    if (value != null) {
                        cellEngineName = value;
                        here = true;
                    }
                    break;
                }

            }
        }
        if (!here) {
            return null;
        }
        return new JsonParameters(propertyNameSet, cellEngineName);
    }

    public static JsonParameters parse(String propertyNames) {
        Set<String> propertyNameSet = new HashSet<String>();
        String[] tokens = StringUtils.getTechnicalTokens(propertyNames, true);
        String cellEngineName = null;
        for (String token : tokens) {
            if (token.startsWith(PROPERTIES_PREFIX)) {
                cellEngineName = token.substring(PROPERTIES_PREFIX.length()).trim();
                if (cellEngineName.length() == 0) {
                    cellEngineName = null;
                }
            } else {
                propertyNameSet.add(token);
            }
        }
        propertyNameSet.addAll(Arrays.asList(tokens));
        return new JsonParameters(propertyNameSet, cellEngineName);
    }

}
