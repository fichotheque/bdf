/* FichothequeLib_Tools  - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.formatters;

import java.io.IOException;
import java.util.List;
import net.fichotheque.addenda.Document;
import net.fichotheque.addenda.Version;
import net.fichotheque.format.FormatContext;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.formatters.DocumentFormatter;
import net.fichotheque.json.DocumentJson;
import net.fichotheque.tools.format.patterndefs.DefaultPattern;
import net.fichotheque.utils.FormatterUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.instruction.Argument;
import net.mapeadores.util.instruction.Instruction;
import net.mapeadores.util.instruction.InstructionResolver;
import net.mapeadores.util.instruction.InstructionResolverProvider;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.LangContext;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public final class DocumentFormatterParser {

    private final static DocumentFormatter ADDENDA_PART = (document, poids, formatSource) -> {
        return document.getSubsetName();
    };
    private final static DocumentFormatter BASENAME_PART = (document, poids, formatSource) -> {
        return document.getBasename();
    };
    private final static DocumentFormatter EXTENSIONS_PART = (document, poids, formatSource) -> {
        List<Version> versionList = document.getVersionList();
        if (versionList.size() == 1) {
            return '.' + versionList.get(0).getExtension();
        } else {
            StringBuilder buf = new StringBuilder();
            for (Version version : versionList) {
                buf.append('[');
                buf.append('.');
                buf.append(version.getExtension());
                buf.append(']');
            }
            return buf.toString();
        }
    };
    private final static DocumentFormatter ID_PART = (document, poids, formatSource) -> {
        return String.valueOf(document.getId());
    };
    private final static DocumentFormatter POIDS_PART = (document, poids, formatSource) -> {
        return String.valueOf(poids);
    };
    private final static DocumentFormatter WITHPOIDSFILTER_FORMSYNTAX_PART = new FormSyntaxPart(true);
    private final static DocumentFormatter WITHOUTPOIDSFILTER_FORMSYNTAX_PART = new FormSyntaxPart(false);
    private final static JsonParameters DEFAULT_JSONPARAMETERS = JsonParameters.parse("basename,versions,size");

    private DocumentFormatterParser() {
    }

    public static DocumentFormatter parse(String pattern, FormatContext formatContext, boolean withPoidsFilter, MessageHandler messageHandler) {
        if (pattern == null) {
            pattern = DefaultPattern.document();
        }
        try {
            return parse(pattern, formatContext, withPoidsFilter);
        } catch (ErrorMessageException fe) {
            messageHandler.addMessage(FormatConstants.SEVERE_PATTERN, fe.getErrorMessage());
            return null;
        }
    }

    public static DocumentFormatter parse(String pattern, FormatContext formatContext, boolean withPoidsFilter) throws ErrorMessageException {
        Object[] partArray = FormatterUtils.parsePattern(new InternalInstructionResolver(formatContext, withPoidsFilter), pattern);
        return new InternalDocumentFormatter(partArray);
    }


    private static class InternalDocumentFormatter implements DocumentFormatter {

        private final Object[] partArray;

        private InternalDocumentFormatter(Object[] partArray) {
            this.partArray = partArray;
        }

        @Override
        public String formatDocument(Document document, int poids, FormatSource formatSource) {
            StringBuilder buf = new StringBuilder();
            int length = partArray.length;
            for (int i = 0; i < length; i++) {
                Object obj = partArray[i];
                if (obj instanceof String) {
                    buf.append((String) obj);
                    continue;
                }
                DocumentFormatter formatter = (DocumentFormatter) obj;
                buf.append(formatter.formatDocument(document, poids, formatSource));
            }
            return buf.toString();
        }

    }


    private static class InternalInstructionResolver implements InstructionResolver {

        private final InstructionResolverProvider provider;
        private final boolean withPoidsFilter;

        private InternalInstructionResolver(FormatContext formatContext, boolean withPoidsFilter) {
            this.provider = formatContext.getInstructionResolverProvider();
            this.withPoidsFilter = withPoidsFilter;
        }

        @Override
        public Object resolve(Instruction instruction) throws ErrorMessageException {
            InstructionResolver resolver = provider.getInstructionResolver(DocumentFormatter.class);
            if (resolver != null) {
                Object formatter = resolver.resolve(instruction);
                if (formatter != null) {
                    return formatter;
                }
            }
            Argument arg1 = instruction.get(0);
            String key = arg1.getKey();
            switch (key) {
                case "addenda":
                    return ADDENDA_PART;
                case "basename":
                    return BASENAME_PART;
                case "extensions":
                    return EXTENSIONS_PART;
                case "formsyntax":
                    if (withPoidsFilter) {
                        return WITHPOIDSFILTER_FORMSYNTAX_PART;
                    } else {
                        return WITHOUTPOIDSFILTER_FORMSYNTAX_PART;
                    }
                case "id":
                    return ID_PART;
                case "json": {
                    JsonParameters jsonParameters = JsonParameters.fromInstruction(instruction);
                    if (jsonParameters == null) {
                        jsonParameters = DEFAULT_JSONPARAMETERS;
                    }
                    LangParameters langParameters = LangParameters.fromInstruction(instruction, "langs");
                    return new JsonPart(jsonParameters, langParameters.getCustomLangContext());
                }
                case "poids":
                    return POIDS_PART;
                default:
                    return null;

            }
        }

    }


    private static class JsonPart implements DocumentFormatter {

        private final JsonParameters jsonParameters;
        private final LangContext customLangContext;

        private JsonPart(JsonParameters jsonParameters, LangContext customLangContext) {
            this.jsonParameters = jsonParameters;
            this.customLangContext = customLangContext;
        }

        @Override
        public String formatDocument(Document document, int poids, FormatSource formatSource) {
            LangContext langContext;
            if (customLangContext != null) {
                langContext = customLangContext;
            } else {
                langContext = formatSource.getLangContext();
            }
            StringBuilder buf = new StringBuilder();
            JSONWriter jsonWriter = new JSONWriter(buf);
            try {
                jsonWriter.object();
                DocumentJson.properties(jsonWriter, document, langContext, jsonParameters);
                jsonWriter.endObject();
            } catch (IOException ioe) {
                throw new ShouldNotOccurException(ioe);
            }
            return buf.toString();
        }

    }


    private static class FormSyntaxPart implements DocumentFormatter {

        private final boolean withPoidsFilter;

        private FormSyntaxPart(boolean withPoidsFilter) {
            this.withPoidsFilter = withPoidsFilter;
        }

        @Override
        public String formatDocument(Document document, int poids, FormatSource formatSource) {
            if ((withPoidsFilter) || (poids == 1)) {
                return String.valueOf(document.getId());
            } else {
                StringBuilder buf = new StringBuilder();
                buf.append(String.valueOf(document.getId()));
                buf.append(" <");
                buf.append(poids);
                buf.append(">");
                return buf.toString();
            }
        }

    }


}
