/* FichothequeLib_Tools - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.formatters;

import net.mapeadores.util.instruction.Argument;
import net.mapeadores.util.instruction.Instruction;


/**
 *
 * @author Vincent Calame
 */
public class ValuesParameters {

    private final String separator;
    private final int startIndex;
    private final int limit;

    public ValuesParameters(String separator, int startIndex, int limit) {
        this.separator = separator;
        this.startIndex = startIndex;
        this.limit = limit;
    }

    public String getSeparator() {
        return separator;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getLimit() {
        return limit;
    }

    public static ValuesParameters fromInstruction(Instruction instruction) {
        String separator = "\n";
        int startIndex = 0;
        int limit = -1;
        for (Argument argument : instruction) {
            String key = argument.getKey();
            if (key.equals("sep")) {
                separator = argument.getValue();
            } else if (key.equals("start")) {
                try {
                    int val = Integer.parseInt(argument.getValue());
                    if (val > 0) {
                        startIndex = val - 1;
                    }
                } catch (NumberFormatException nfe) {
                }
            } else if (key.equals("limit")) {
                try {
                    limit = Integer.parseInt(argument.getValue());
                    if (limit < 1) {
                        limit = -1;
                    }
                } catch (NumberFormatException nfe) {
                }
            }
        }
        return new ValuesParameters(separator, startIndex, limit);
    }

}
