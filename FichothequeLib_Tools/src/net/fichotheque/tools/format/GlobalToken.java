/* FichothequeLib_Tools - Copyright (c) 2016-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format;

import net.fichotheque.format.Tokens;


/**
 *
 * @author Vincent Calame
 */
public class GlobalToken {

    private final int tokensIndex;
    private final Tokens tokens;
    private final int localIndex;


    public GlobalToken(int tokensIndex, Tokens tokens, int localIndex) {
        this.tokensIndex = tokensIndex;
        this.tokens = tokens;
        this.localIndex = localIndex;
    }

    public int getTokensIndex() {
        return tokensIndex;
    }

    public Tokens getTokens() {
        return tokens;
    }

    public int getLocalIndex() {
        return localIndex;
    }

    public String format() {
        return tokens.get(localIndex);
    }

}
