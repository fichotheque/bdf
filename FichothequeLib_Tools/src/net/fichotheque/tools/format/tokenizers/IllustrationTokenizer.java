/* FichothequeLib_Tools - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.IllustrationFormatter;
import net.fichotheque.utils.CroisementUtils;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationTokenizer implements Tokenizer {

    private final Album album;
    private final IllustrationFormatter illustrationFormatter;
    private final String mode;
    private final int poidsFilter;
    private final boolean globalSelect;

    public IllustrationTokenizer(Album album, IllustrationFormatter illustrationFormatter, String mode, int poidsFilter, boolean globalSelect) {
        this.album = album;
        this.illustrationFormatter = illustrationFormatter;
        this.mode = mode;
        this.poidsFilter = poidsFilter;
        this.globalSelect = globalSelect;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        if (!formatSource.getSubsetAccessPredicate().test(album)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Predicate<SubsetItem> predicate = null;
        if (globalSelect) {
            predicate = formatSource.getGlobalPredicate();
        }
        Croisements croisements = formatSource.getSubsetItemPointeur().getCroisements(album);
        List<Liaison> liaisons = new ArrayList<Liaison>(CroisementUtils.filter(croisements, mode, poidsFilter));
        if (liaisons.isEmpty()) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        return new InternalTokens(liaisons, illustrationFormatter, formatSource);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final List<Liaison> liaisonList;
        private final IllustrationFormatter illustrationFormatter;
        private final FormatSource formatSource;

        private InternalTokens(List<Liaison> liaisonList, IllustrationFormatter illustrationFormatter, FormatSource formatSource) {
            this.liaisonList = liaisonList;
            this.illustrationFormatter = illustrationFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return liaisonList.size();
        }

        @Override
        public String get(int i) {
            Liaison liaison = liaisonList.get(i);
            return illustrationFormatter.formatIllustration((Illustration) liaison.getSubsetItem(), liaison.getLien().getPoids(), formatSource);
        }

    }

}
