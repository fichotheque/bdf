/* FichothequeLib_Tools - Copyright (c) 2016-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.MotcleFormatter;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class MasterMotcleTokenizer implements Tokenizer {

    private final Thesaurus thesaurus;
    private final MotcleFormatter motcleFormatter;
    private final boolean globalSelect;

    public MasterMotcleTokenizer(Thesaurus thesaurus, MotcleFormatter motcleFormatter, boolean globalSelect) {
        this.thesaurus = thesaurus;
        this.motcleFormatter = motcleFormatter;
        this.globalSelect = globalSelect;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        if (!formatSource.getSubsetAccessPredicate().test(thesaurus)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Motcle motcle = thesaurus.getMotcleById(formatSource.getSubsetItemPointeur().getCurrentSubsetItem().getId());
        if ((motcle != null) && (globalSelect)) {
            Predicate<SubsetItem> predicate = formatSource.getGlobalPredicate();
            if (predicate != null) {
                if (!predicate.test(motcle)) {
                    motcle = null;
                }
            }
        }
        if (motcle == null) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        return new InternalTokens(motcle, motcleFormatter, formatSource);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final Motcle motcle;
        private final FormatSource formatSource;
        private final MotcleFormatter motcleFormatter;

        private InternalTokens(Motcle motcle, MotcleFormatter motcleFormatter, FormatSource formatSource) {
            this.motcle = motcle;
            this.motcleFormatter = motcleFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public String get(int i) {
            return motcleFormatter.formatMotcle(motcle, 1, formatSource);
        }

    }

}
