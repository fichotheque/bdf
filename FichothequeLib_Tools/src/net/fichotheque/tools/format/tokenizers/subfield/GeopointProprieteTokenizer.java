/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers.subfield;

import net.fichotheque.corpus.SubfieldValue;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.SubfieldFormatter;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.utils.FormatterUtils;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.primitives.DegreDecimal;


/**
 *
 * @author Vincent Calame
 */
class GeopointProprieteTokenizer implements Tokenizer {

    private final SubfieldKey subfieldKey;
    private final SubfieldFormatter subfieldFormatter;

    GeopointProprieteTokenizer(SubfieldKey subfieldKey, SubfieldFormatter subfieldFormatter) {
        this.subfieldKey = subfieldKey;
        this.subfieldFormatter = subfieldFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        FicheItem ficheItem = (FicheItem) ((FichePointeur) formatSource.getSubsetItemPointeur()).getValue(subfieldKey);
        if ((ficheItem == null) || (!(ficheItem instanceof Geopoint))) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Geopoint geopoint = (Geopoint) ficheItem;
        DegreDecimal degreDecimal = getDegreDecimal(geopoint, subfieldKey.getSubtype());
        SubfieldValue subfieldValue = FormatterUtils.toSubfieldValue(degreDecimal.toString());
        String value = subfieldFormatter.formatSubfield(subfieldValue, formatSource);
        return FormatterUtils.toTokens(value);
    }

    public DegreDecimal getDegreDecimal(Geopoint geopoint, short subfieldType) {
        switch (subfieldType) {
            case SubfieldKey.LATITUDE_SUBTYPE:
                return geopoint.getLatitude();
            case SubfieldKey.LONGITUDE_SUBTYPE:
                return geopoint.getLongitude();
            default:
                throw new SwitchException("subfieldType = " + subfieldType);
        }
    }

}
