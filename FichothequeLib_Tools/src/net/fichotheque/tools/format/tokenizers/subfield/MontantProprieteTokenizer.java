/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers.subfield;

import net.fichotheque.corpus.SubfieldValue;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.SubfieldFormatter;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.utils.FormatterUtils;
import net.mapeadores.util.exceptions.SwitchException;


/**
 *
 * @author Vincent Calame
 */
class MontantProprieteTokenizer implements Tokenizer {

    private final SubfieldKey subfieldKey;
    private final SubfieldFormatter subfieldFormatter;

    MontantProprieteTokenizer(SubfieldKey subfieldKey, SubfieldFormatter subfieldFormatter) {
        this.subfieldKey = subfieldKey;
        this.subfieldFormatter = subfieldFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        FicheItem ficheItem = (FicheItem) ((FichePointeur) formatSource.getSubsetItemPointeur()).getValue(subfieldKey);
        if ((ficheItem == null) || (!(ficheItem instanceof Montant))) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Montant montant = (Montant) ficheItem;
        String sourceValue;
        switch (subfieldKey.getSubtype()) {
            case SubfieldKey.CURRENCY_SUBTYPE:
                sourceValue = montant.getCurrency().getCurrencyCode();
                break;
            case SubfieldKey.VALUE_SUBTYPE:
                sourceValue = montant.getDecimal().toString();
                break;
            default:
                throw new SwitchException("subfieldType = " + subfieldKey.getSubtype());
        }
        SubfieldValue subfieldValue = FormatterUtils.toSubfieldValue(sourceValue);
        String value = subfieldFormatter.formatSubfield(subfieldValue, formatSource);
        return FormatterUtils.toTokens(value);
    }

}
