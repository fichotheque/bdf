/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers.subfield;

import net.fichotheque.corpus.SubfieldValue;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.SubfieldFormatter;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.utils.FormatterUtils;
import net.mapeadores.util.exceptions.SwitchException;


/**
 *
 * @author Vincent Calame
 */
public class ImageProprieteTokenizer implements Tokenizer {

    private final SubfieldKey subfieldKey;
    private final SubfieldFormatter subfieldFormatter;

    ImageProprieteTokenizer(SubfieldKey subfieldKey, SubfieldFormatter subfieldFormatter) {
        this.subfieldKey = subfieldKey;
        this.subfieldFormatter = subfieldFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        FicheItem ficheItem = (FicheItem) ((FichePointeur) formatSource.getSubsetItemPointeur()).getValue(subfieldKey);
        if (ficheItem == null) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        short subfieldType = subfieldKey.getSubtype();
        if (!(ficheItem instanceof Image)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Image image = (Image) ficheItem;
        String subfieldString = getSubfieldString(image, subfieldType);
        if (subfieldString != null) {
            SubfieldValue subfieldValue = FormatterUtils.toSubfieldValue(subfieldString);
            String value = subfieldFormatter.formatSubfield(subfieldValue, formatSource);
            return FormatterUtils.toTokens(value);
        } else {
            return FormatterUtils.EMPTY_TOKENS;
        }
    }

    private String getSubfieldString(Image image, short subfieldType) {
        switch (subfieldType) {
            case SubfieldKey.SRC_SUBTYPE:
                return image.getSrc();
            case SubfieldKey.ALT_SUBTYPE:
                return image.getAlt();
            case SubfieldKey.TITLE_SUBTYPE:
                return image.getTitle();
            default:
                throw new SwitchException("subfieldType = " + subfieldType);
        }
    }

}
