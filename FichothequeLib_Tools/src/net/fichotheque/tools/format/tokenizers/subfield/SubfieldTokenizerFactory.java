/* FichothequeLib_Tools - Copyright (c) 2009-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers.subfield;

import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.fichotheque.format.formatters.SubfieldFormatter;
import net.mapeadores.util.exceptions.SwitchException;
import net.fichotheque.format.Tokenizer;


/**
 *
 * @author Vincent Calame
 */
public class SubfieldTokenizerFactory {

    private SubfieldTokenizerFactory() {
    }

    public static Tokenizer newInstance(CorpusField corpusField, SubfieldKey subfieldKey, SubfieldFormatter subfieldFormatter, Fichotheque fichotheque) {
        short subfieldType = subfieldKey.getSubtype();
        switch (subfieldType) {
            case SubfieldKey.SURNAME_SUBTYPE:
            case SubfieldKey.FORENAME_SUBTYPE:
            case SubfieldKey.NONLATIN_SUBTYPE:
            case SubfieldKey.SURNAMEFIRST_SUBTYPE:
                return new PersonneProprieteTokenizer(subfieldKey, subfieldFormatter, fichotheque);
            case SubfieldKey.SRC_SUBTYPE:
            case SubfieldKey.ALT_SUBTYPE:
            case SubfieldKey.TITLE_SUBTYPE:
                return new ImageProprieteTokenizer(subfieldKey, subfieldFormatter);
            case SubfieldKey.CURRENCY_SUBTYPE:
            case SubfieldKey.VALUE_SUBTYPE:
                return new MontantProprieteTokenizer(subfieldKey, subfieldFormatter);
            case SubfieldKey.LATITUDE_SUBTYPE:
            case SubfieldKey.LONGITUDE_SUBTYPE:
                return new GeopointProprieteTokenizer(subfieldKey, subfieldFormatter);
            case SubfieldKey.OTHERS_SUBTYPE:
            case SubfieldKey.AMOUNT_SUBTYPE:
                return new MontantInformationTokenizer(corpusField, subfieldKey, subfieldFormatter);
            case SubfieldKey.LANG_SUBTYPE:
            default:
                throw new SwitchException("subfieldType = " + subfieldType);
        }
    }

}
