/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers.subfield;

import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.SubfieldValue;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.SubfieldFormatter;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.utils.FormatterUtils;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
class PersonneProprieteTokenizer implements Tokenizer {

    private final Fichotheque fichotheque;
    private final SubfieldKey subfieldKey;
    private final SubfieldFormatter subfieldFormatter;

    PersonneProprieteTokenizer(SubfieldKey subfieldKey, SubfieldFormatter subfieldFormatter, Fichotheque fichotheque) {
        this.subfieldKey = subfieldKey;
        this.fichotheque = fichotheque;
        this.subfieldFormatter = subfieldFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        FicheItem ficheItem = (FicheItem) ((FichePointeur) formatSource.getSubsetItemPointeur()).getValue(subfieldKey);
        if (ficheItem == null) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        if (!(ficheItem instanceof Personne)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        PersonCore personCore = SphereUtils.toPersonCore(fichotheque, (Personne) ficheItem);
        String subfieldString = getSubfieldString(personCore, subfieldKey.getSubtype());
        if (subfieldString != null) {
            SubfieldValue subfieldValue = FormatterUtils.toSubfieldValue(subfieldString);
            String value = subfieldFormatter.formatSubfield(subfieldValue, formatSource);
            return FormatterUtils.toTokens(value);
        } else {
            return FormatterUtils.EMPTY_TOKENS;
        }
    }

    private String getSubfieldString(PersonCore personCore, short subfieldType) {
        switch (subfieldType) {
            case SubfieldKey.SURNAME_SUBTYPE:
                return personCore.getSurname();
            case SubfieldKey.FORENAME_SUBTYPE:
                return personCore.getForename();
            case SubfieldKey.NONLATIN_SUBTYPE:
                return personCore.getNonlatin();
            case SubfieldKey.SURNAMEFIRST_SUBTYPE:
                boolean surnameFirst = personCore.isSurnameFirst();
                return (surnameFirst) ? "1" : "0";
            default:
                throw new SwitchException("subfieldType = " + subfieldType);
        }
    }

}
