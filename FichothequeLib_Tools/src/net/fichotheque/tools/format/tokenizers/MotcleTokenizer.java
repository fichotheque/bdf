/* FichothequeLib_Tools - Copyright (c) 2009-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.MotcleFormatter;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.CroisementUtils;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class MotcleTokenizer implements Tokenizer {

    private final Thesaurus thesaurus;
    private final MotcleFormatter motcleFormatter;
    private final String mode;
    private final int poidsFilter;
    private final boolean globalSelect;

    public MotcleTokenizer(Thesaurus thesaurus, MotcleFormatter motcleFormatter, String mode, int poidsFilter, boolean globalSelect) {
        this.thesaurus = thesaurus;
        this.motcleFormatter = motcleFormatter;
        this.mode = mode;
        this.poidsFilter = poidsFilter;
        this.globalSelect = globalSelect;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        if (!formatSource.getSubsetAccessPredicate().test(thesaurus)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Predicate<SubsetItem> predicate = null;
        if (globalSelect) {
            predicate = formatSource.getGlobalPredicate();
        }
        Croisements motcleCroisements = formatSource.getSubsetItemPointeur().getCroisements(thesaurus);
        List<Liaison> liaisons = new ArrayList<Liaison>(CroisementUtils.filter(motcleCroisements, mode, poidsFilter, predicate));
        if (liaisons.isEmpty()) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        return new InternalTokens(liaisons, motcleFormatter, formatSource);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final List<Liaison> liaisonList;
        private final FormatSource formatSource;
        private final MotcleFormatter motcleFormatter;

        private InternalTokens(List<Liaison> liaisonList, MotcleFormatter motcleFormatter, FormatSource formatSource) {
            this.liaisonList = liaisonList;
            this.motcleFormatter = motcleFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return liaisonList.size();
        }

        @Override
        public String get(int i) {
            Liaison liaison = liaisonList.get(i);
            return motcleFormatter.formatMotcle((Motcle) liaison.getSubsetItem(), liaison.getLien().getPoids(), formatSource);
        }

    }

}
