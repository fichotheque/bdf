/* FichothequeLib_Tools - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.ValueFormatter;
import net.fichotheque.pointeurs.MotclePointeur;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusFieldKey;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusFieldTokenizer implements Tokenizer {

    private final static short SPECIAL_NONE = 0;
    private final static short SPECIAL_LANG = 1;
    private final static short SPECIAL_LABEL = 2;
    private final Thesaurus thesaurus;
    private final ThesaurusFieldKey thesaurusFieldKey;
    private final ValueFormatter valueFormatter;
    private final short specialProcess;

    public ThesaurusFieldTokenizer(Thesaurus thesaurus, ThesaurusFieldKey thesaurusFieldKey, ValueFormatter valueFormatter) {
        this.thesaurus = thesaurus;
        this.thesaurusFieldKey = thesaurusFieldKey;
        this.valueFormatter = valueFormatter;
        if (!thesaurus.isBabelienType()) {
            if (thesaurusFieldKey.equals(ThesaurusFieldKey.BABELIENLABEL)) {
                this.specialProcess = SPECIAL_LABEL;
            } else if (thesaurusFieldKey.equals(ThesaurusFieldKey.BABELIENLANG)) {
                this.specialProcess = SPECIAL_LANG;
            } else {
                this.specialProcess = SPECIAL_NONE;
            }
        } else {
            this.specialProcess = SPECIAL_NONE;
        }

    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        String value = getString(formatSource);
        if (value == null) {
            return FormatterUtils.EMPTY_TOKENS;
        } else {
            return FormatterUtils.toTokens(valueFormatter.formatValue(value, formatSource));
        }

    }

    private String getString(FormatSource formatSource) {
        switch (specialProcess) {
            case SPECIAL_LABEL:
                return getSpecialLabel(formatSource);
            case SPECIAL_LANG:
                return formatSource.getDefaultLang().toString();
            default:
                MotclePointeur motclePointeur = (MotclePointeur) formatSource.getSubsetItemPointeur();
                Object obj = motclePointeur.getFieldValue(thesaurusFieldKey);
                if (obj == null) {
                    return null;
                }
                return obj.toString();
        }
    }

    private String getSpecialLabel(FormatSource formatSource) {
        ThesaurusFieldKey localisedFieldKey = ThesaurusFieldKey.toLabelThesaurusFieldKey(formatSource.getDefaultLang());
        MotclePointeur motclePointeur = (MotclePointeur) formatSource.getSubsetItemPointeur();
        Object obj = motclePointeur.getFieldValue(localisedFieldKey);
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

}
