/* FichothequeLib_Tools - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.ExtractionFormatter;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class ExtractionTokenizer implements Tokenizer {

    private final ExtractionFormatter extractionFormatter;

    public ExtractionTokenizer(ExtractionFormatter extractionFormatter) {
        super();
        this.extractionFormatter = extractionFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        if (formatSource.getExtractionInfo() == null) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        return new InternalTokens(extractionFormatter, formatSource);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final ExtractionFormatter extractionFormatter;
        private final FormatSource formatSource;

        private InternalTokens(ExtractionFormatter extractionFormatter, FormatSource formatSource) {
            this.extractionFormatter = extractionFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public String get(int i) {
            return extractionFormatter.formatExtraction(formatSource);
        }

    }

}
