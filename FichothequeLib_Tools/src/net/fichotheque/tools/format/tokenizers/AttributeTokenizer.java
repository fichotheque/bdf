/* FichothequeLib_Tools - Copyright (c) 2015-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import net.fichotheque.SubsetItem;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.AttributeFormatter;
import net.fichotheque.utils.FormatterUtils;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.fichotheque.format.Tokenizer;


/**
 *
 * @author Vincent Calame
 */
public class AttributeTokenizer implements Tokenizer {

    private final AttributeKey attributeKey;
    private final AttributeFormatter attributeFormatter;

    public AttributeTokenizer(AttributeKey attributeKey, AttributeFormatter attributeFormatter) {
        this.attributeKey = attributeKey;
        this.attributeFormatter = attributeFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        SubsetItem subsetItem = formatSource.getSubsetItemPointeur().getCurrentSubsetItem();
        Attribute attribute = subsetItem.getAttributes().getAttribute(attributeKey);
        if (attribute == null) {
            return FormatterUtils.EMPTY_TOKENS;
        } else {
            return new InternalTokens(attribute, attributeFormatter, formatSource);
        }
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final Attribute attribute;
        private final AttributeFormatter attributeFormatter;
        private final FormatSource formatSource;

        private InternalTokens(Attribute attribute, AttributeFormatter attributeFormatter, FormatSource formatSource) {
            this.attribute = attribute;
            this.attributeFormatter = attributeFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return attribute.size();
        }

        @Override
        public String get(int i) {
            return attributeFormatter.formatAttributeValue(attribute.get(i), i, formatSource);
        }

    }

}
