/* FichothequeLib_Tools - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.FicheBlockFormatter;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class FicheBlockTokenizer implements Tokenizer {

    private final CorpusField corpusField;
    private final FicheBlockFormatter ficheBlockFormatter;

    public FicheBlockTokenizer(CorpusField corpusField, FicheBlockFormatter ficheBlockFormatter) {
        super();
        this.corpusField = corpusField;
        this.ficheBlockFormatter = ficheBlockFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        FichePointeur fichePointeur = (FichePointeur) formatSource.getSubsetItemPointeur();
        SubsetKey corpusKey = fichePointeur.getSubsetKey();
        FicheBlocks ficheBlocks = (FicheBlocks) fichePointeur.getValue(corpusField);
        if (ficheBlocks == null) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        return new InternalTokens(ficheBlocks, ficheBlockFormatter, formatSource, corpusKey);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final FicheBlocks ficheBlocks;
        private final FicheBlockFormatter ficheBlockFormatter;
        private final FormatSource formatSource;
        private final SubsetKey corpusKey;

        private InternalTokens(FicheBlocks ficheBlocks, FicheBlockFormatter ficheBlockFormat, FormatSource formatSource, SubsetKey corpusKey) {
            this.ficheBlocks = ficheBlocks;
            this.ficheBlockFormatter = ficheBlockFormat;
            this.formatSource = formatSource;
            this.corpusKey = corpusKey;
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public String get(int i) {
            return ficheBlockFormatter.formatFicheBlocks(ficheBlocks, formatSource, corpusKey);
        }

    }

}
