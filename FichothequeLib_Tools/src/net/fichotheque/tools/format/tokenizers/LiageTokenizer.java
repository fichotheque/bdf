/* FichothequeLib_Tools - Copyright (c) 2012-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.FicheMetaFormatter;
import net.fichotheque.include.LiageTest;
import net.fichotheque.utils.CroisementUtils;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class LiageTokenizer implements Tokenizer {

    private final FicheMetaFormatter ficheMetaFormatter;
    private final boolean globalSelect;
    private final LiageTest liageTest;

    public LiageTokenizer(FicheMetaFormatter ficheMetaFormatter, boolean globalSelect, LiageTest liageTest) {
        this.ficheMetaFormatter = ficheMetaFormatter;
        this.globalSelect = globalSelect;
        this.liageTest = liageTest;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        List<FicheMetaWrapper> wrapperList = getList(formatSource);
        if ((wrapperList == null) || (wrapperList.isEmpty())) {
            return FormatterUtils.EMPTY_TOKENS;
        } else {
            return new InternalTokens(ficheMetaFormatter, wrapperList, formatSource);
        }
    }

    private List<FicheMetaWrapper> getList(FormatSource formatSource) {
        Predicate<SubsetItem> predicate = null;
        if (globalSelect) {
            predicate = formatSource.getGlobalPredicate();
        }
        List<Liaison> liaisons = CroisementUtils.getNoList(formatSource.getSubsetItemPointeur(), "", liageTest, formatSource.getExtractionContext().getSubsetAccessPredicate(), predicate);
        if (liaisons.isEmpty()) {
            return null;
        }
        List<FicheMetaWrapper> wrapperList = new ArrayList<FicheMetaWrapper>();
        for (Liaison liaison : liaisons) {
            wrapperList.add(new FicheMetaWrapper((FicheMeta) liaison.getSubsetItem(), liaison.getLien().getPoids()));
        }
        return wrapperList;
    }


    private static class FicheMetaWrapper {

        private final FicheMeta ficheMeta;
        private final int poids;

        private FicheMetaWrapper(FicheMeta ficheMeta, int poids) {
            this.ficheMeta = ficheMeta;
            this.poids = poids;
        }

        public FicheMeta getFicheMeta() {
            return ficheMeta;
        }

        public int getPoids() {
            return poids;
        }

    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final FicheMetaFormatter ficheMetaFormatter;
        private final List<FicheMetaWrapper> wrapperList;
        private final FormatSource formatSource;

        private InternalTokens(FicheMetaFormatter ficheMetaFormatter, List<FicheMetaWrapper> wrapperList, FormatSource formatSource) {
            this.wrapperList = wrapperList;
            this.ficheMetaFormatter = ficheMetaFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return wrapperList.size();
        }

        @Override
        public String get(int i) {
            FicheMetaWrapper wrapper = wrapperList.get(i);
            return ficheMetaFormatter.formatFicheMeta(wrapper.getFicheMeta(), wrapper.getPoids(), formatSource);
        }

    }

}
