/* FichothequeLib_Tools - Copyright (c) 2016-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.FicheMetaFormatter;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class FicheParentageTokenizer implements Tokenizer {

    private final Corpus corpus;
    private final FicheMetaFormatter ficheMetaFormatter;
    private final boolean globalSelect;

    public FicheParentageTokenizer(Corpus corpus, FicheMetaFormatter ficheMetaFormatter, boolean globalSelect) {
        this.corpus = corpus;
        this.ficheMetaFormatter = ficheMetaFormatter;
        this.globalSelect = globalSelect;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        if (!formatSource.getSubsetAccessPredicate().test(corpus)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        FicheMeta ficheMeta = corpus.getFicheMetaById(formatSource.getSubsetItemPointeur().getCurrentSubsetItem().getId());
        if ((ficheMeta != null) && (globalSelect)) {
            Predicate<SubsetItem> predicate = formatSource.getGlobalPredicate();
            if (predicate != null) {
                if (!predicate.test(ficheMeta)) {
                    ficheMeta = null;
                }
            }
        }
        if (ficheMeta == null) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        return new InternalTokens(ficheMeta, ficheMetaFormatter, formatSource);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final FicheMeta ficheMeta;
        private final FormatSource formatSource;
        private final FicheMetaFormatter ficheMetaFormatter;

        private InternalTokens(FicheMeta ficheMeta, FicheMetaFormatter ficheMetaFormatter, FormatSource formatSource) {
            this.ficheMeta = ficheMeta;
            this.ficheMetaFormatter = ficheMetaFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public String get(int i) {
            return ficheMetaFormatter.formatFicheMeta(ficheMeta, 1, formatSource);
        }

    }

}
