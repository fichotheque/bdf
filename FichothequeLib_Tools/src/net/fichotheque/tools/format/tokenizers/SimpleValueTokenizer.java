/* FichothequeLib_Tools - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.ValueFormatter;
import net.fichotheque.pointeurs.FichePointeur;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class SimpleValueTokenizer implements Tokenizer {

    private final CorpusField corpusField;
    private final ValueFormatter valueFormatter;

    public SimpleValueTokenizer(CorpusField corpusField, ValueFormatter valueFormatter) {
        super();
        this.corpusField = corpusField;
        this.valueFormatter = valueFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        Object obj = ((FichePointeur) formatSource.getSubsetItemPointeur()).getValue(corpusField);
        if (obj == null) {
            return FormatterUtils.EMPTY_TOKENS;
        } else {
            String value = obj.toString();
            return FormatterUtils.toTokens(valueFormatter.formatValue(value, formatSource));
        }
    }

}
