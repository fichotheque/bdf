/* FichothequeLib_Tools - Copyright (c) 2009-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.FicheMetaFormatter;
import net.fichotheque.utils.Comparators;
import net.fichotheque.utils.CroisementUtils;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class FicheMetaTokenizer implements Tokenizer {

    private final Corpus corpus;
    private final FicheMetaFormatter ficheMetaFormatter;
    private final String mode;
    private final int poidsFilter;
    private final boolean globalSelect;
    private final boolean idSort;

    public FicheMetaTokenizer(Corpus corpus, FicheMetaFormatter ficheMetaFormatter, String mode, int poidsFilter, boolean globalSelect, boolean idSort) {
        this.corpus = corpus;
        this.ficheMetaFormatter = ficheMetaFormatter;
        this.mode = mode;
        this.poidsFilter = poidsFilter;
        this.globalSelect = globalSelect;
        this.idSort = idSort;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        if (!formatSource.getSubsetAccessPredicate().test(corpus)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Predicate<SubsetItem> predicate = null;
        if (globalSelect) {
            predicate = formatSource.getGlobalPredicate();
        }
        Croisements croisements = formatSource.getSubsetItemPointeur().getCroisements(corpus);
        List<Liaison> liaisons = new ArrayList<Liaison>(CroisementUtils.filter(croisements, mode, poidsFilter, predicate));
        if (liaisons.isEmpty()) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        if (idSort) {
            Collections.sort(liaisons, Comparators.LIAISONID);
        }
        return new InternalTokens(liaisons, ficheMetaFormatter, formatSource);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final List<Liaison> liaisonList;
        private final FicheMetaFormatter ficheMetaFormatter;
        private final FormatSource formatSource;

        private InternalTokens(List<Liaison> liaisonList, FicheMetaFormatter ficheMetaFormatter, FormatSource formatSource) {
            this.liaisonList = liaisonList;
            this.ficheMetaFormatter = ficheMetaFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return liaisonList.size();
        }

        @Override
        public String get(int i) {
            Liaison liaison = liaisonList.get(i);
            return ficheMetaFormatter.formatFicheMeta((FicheMeta) liaison.getSubsetItem(), liaison.getLien().getPoids(), formatSource);
        }

    }

}
