/* FichothequeLib_Tools - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.FicheMetaFormatter;
import net.fichotheque.pointeurs.SubsetItemPointeur;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.FichothequeUtils;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class ParentageTokenizer implements Tokenizer {

    private final FicheMetaFormatter ficheMetaFormatter;
    private final boolean globalSelect;

    public ParentageTokenizer(FicheMetaFormatter ficheMetaFormatter, boolean globalSelect) {
        super();
        this.ficheMetaFormatter = ficheMetaFormatter;
        this.globalSelect = globalSelect;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        List<FicheMeta> list = getList(formatSource);
        if ((list == null) || (list.isEmpty())) {
            return FormatterUtils.EMPTY_TOKENS;
        } else {
            return new InternalTokens(ficheMetaFormatter, list, formatSource);
        }
    }

    private List<FicheMeta> getList(FormatSource formatSource) {
        Predicate<SubsetItem> predicate = null;
        if (globalSelect) {
            predicate = formatSource.getGlobalPredicate();
        }
        SubsetItemPointeur subsetItemPointeur = formatSource.getSubsetItemPointeur();
        SubsetItem currentSubsetItem = subsetItemPointeur.getCurrentSubsetItem();
        if (currentSubsetItem == null) {
            return null;
        }
        Subset subset = subsetItemPointeur.getSubset();
        List<Corpus> parentageCorpusList;
        if (subset instanceof Thesaurus) {
            parentageCorpusList = subset.getSatelliteCorpusList();
        } else {
            parentageCorpusList = FichothequeUtils.getParentageCorpusList((Corpus) subset);
        }
        if (parentageCorpusList.isEmpty()) {
            return null;
        }
        int id = currentSubsetItem.getId();
        List<FicheMeta> resultList = new ArrayList<FicheMeta>();
        for (Corpus corpus : parentageCorpusList) {
            if (!formatSource.getSubsetAccessPredicate().test(corpus)) {
                continue;
            }
            FicheMeta ficheMeta = corpus.getFicheMetaById(id);
            if (ficheMeta != null) {
                if ((predicate == null) || (predicate.test(ficheMeta))) {
                    resultList.add(ficheMeta);
                }
            }
        }
        return resultList;
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final FicheMetaFormatter ficheMetaFormatter;
        private final List<FicheMeta> ficheMetaList;
        private final FormatSource formatSource;

        private InternalTokens(FicheMetaFormatter ficheMetaFormatter, List<FicheMeta> ficheMetaList, FormatSource formatSource) {
            this.ficheMetaList = ficheMetaList;
            this.ficheMetaFormatter = ficheMetaFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return ficheMetaList.size();
        }

        @Override
        public String get(int i) {
            FicheMeta ficheMeta = ficheMetaList.get(i);
            return ficheMetaFormatter.formatFicheMeta(ficheMeta, 0, formatSource);
        }

    }

}
