/* FichothequeLib_Tools - Copyright (c) 2009-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.croisement.Liaison;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.DocumentFormatter;
import net.fichotheque.utils.CroisementUtils;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class DocumentTokenizer implements Tokenizer {

    private final Addenda addenda;
    private final DocumentFormatter documentFormatter;
    private final String mode;
    private final int poidsFilter;
    private final boolean globalSelect;

    public DocumentTokenizer(Addenda addenda, DocumentFormatter documentFormatter, String mode, int poidsFilter, boolean globalSelect) {
        this.documentFormatter = documentFormatter;
        this.addenda = addenda;
        this.mode = mode;
        this.poidsFilter = poidsFilter;
        this.globalSelect = globalSelect;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        if (!formatSource.getSubsetAccessPredicate().test(addenda)) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        Predicate<SubsetItem> predicate = null;
        if (globalSelect) {
            predicate = formatSource.getGlobalPredicate();
        }
        Croisements croisements = formatSource.getSubsetItemPointeur().getCroisements(addenda);
        List<Liaison> liaisons = new ArrayList<Liaison>(CroisementUtils.filter(croisements, mode, poidsFilter));
        if (liaisons.isEmpty()) {
            return FormatterUtils.EMPTY_TOKENS;
        }
        return new InternalTokens(liaisons, documentFormatter, formatSource);
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final List<Liaison> liaisonList;
        private final DocumentFormatter documentFormatter;
        private final FormatSource formatSource;

        private InternalTokens(List<Liaison> liaisonList, DocumentFormatter documentFormatter, FormatSource formatSource) {
            this.liaisonList = liaisonList;
            this.documentFormatter = documentFormatter;
            this.formatSource = formatSource;
        }

        @Override
        public int size() {
            return liaisonList.size();
        }

        @Override
        public String get(int i) {
            Liaison liaison = liaisonList.get(i);
            return documentFormatter.formatDocument((Document) liaison.getSubsetItem(), liaison.getLien().getPoids(), formatSource);
        }

    }

}
