/* FichothequeLib_Tools - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokenizer;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.MotcleFormatter;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.FormatterUtils;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusThisTokenizer implements Tokenizer {

    private final MotcleFormatter motcleFormatter;

    public ThesaurusThisTokenizer(Thesaurus thesaurus, MotcleFormatter motcleFormatter) {
        this.motcleFormatter = motcleFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        return FormatterUtils.toTokens(motcleFormatter.formatMotcle((Motcle) formatSource.getSubsetItemPointeur().getCurrentSubsetItem(), -1, formatSource));
    }

}
