/* FichothequeLib_Tools - Copyright (c) 2009-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.format.tokenizers;

import java.util.AbstractList;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.format.FormatSource;
import net.fichotheque.format.Tokens;
import net.fichotheque.format.formatters.FicheItemFormatter;
import net.fichotheque.utils.FichothequeUtils;
import net.fichotheque.utils.FormatterUtils;
import net.fichotheque.format.Tokenizer;


/**
 *
 * @author Vincent Calame
 */
public class ChronoTokenizer implements Tokenizer {

    private final String name;
    private final FicheItemFormatter ficheItemFormatter;

    public ChronoTokenizer(String name, FicheItemFormatter ficheItemFormatter) {
        this.name = name;
        this.ficheItemFormatter = ficheItemFormatter;
    }

    @Override
    public Tokens tokenize(FormatSource formatSource) {
        Datation datation = null;
        FicheMeta ficheMeta = (FicheMeta) formatSource.getSubsetItemPointeur().getCurrentSubsetItem();
        if (ficheMeta != null) {
            datation = FichothequeUtils.getDatation(ficheMeta, name);
        }
        if (datation == null) {
            return FormatterUtils.EMPTY_TOKENS;
        } else {
            return new InternalTokens(datation, ficheItemFormatter, formatSource);
        }
    }


    private static class InternalTokens extends AbstractList<String> implements Tokens {

        private final Datation datation;
        private final FicheItemFormatter ficheItemFormatter;
        private final FormatSource formatSource;

        private InternalTokens(Datation datation, FicheItemFormatter ficheItemFormatter, FormatSource formatSource) {
            this.datation = datation;
            this.formatSource = formatSource;
            this.ficheItemFormatter = ficheItemFormatter;
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public String get(int i) {
            return ficheItemFormatter.formatFicheItem(datation, formatSource);
        }

    }

}
