/* FichothequeLib_Tools - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers;

import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.include.IncludeKey;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.tools.parsers.croisement.PoidsMotcleToken;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
abstract class DynamicEdit {

    protected final ParseContext parseContext;
    protected final Parameters parameters;

    protected DynamicEdit(Parameters parameters) {
        this.parameters = parameters;
        this.parseContext = parameters.getParseContext();
    }

    public abstract void editToken(PoidsMotcleToken motcleToken);

    public Motcle createMotcle(CleanedString labelText) {
        return createMotcle(parameters.getMainThesaurus(), labelText);
    }

    public Motcle createMotcle(Thesaurus thesaurus, CleanedString labelText) {
        ThesaurusEditor thsedit = parseContext.getFichothequeEditor().getThesaurusEditor(thesaurus);
        try {
            Motcle motcle = thsedit.createMotcle(-1, null);
            if (labelText != null) {
                thsedit.putLabel(motcle, parameters.getLang(), labelText);
            }
            return motcle;
        } catch (ExistingIdException | ParseException e) {
            throw new ShouldNotOccurException(e);
        }
    }

    public void createLien(Motcle motcle, int poids) {
        parameters.croisementParseEngine.getMainChange().addLien(motcle, parameters.getMode(), poids);
    }

    public void appendLien(Motcle motcle, int poids) {
        parameters.croisementParseEngine.getAppendChange().addLien(motcle, parameters.getMode(), poids);
    }

    public Motcle getMotcle(Thesaurus otherThesaurus, CleanedString labelText) {
        if (labelText == null) {
            return null;
        }
        Lang lang = ThesaurusUtils.checkDisponibility(parseContext.getThesaurusLangChecker(), otherThesaurus, parameters.getLang());
        return otherThesaurus.seekMotcleByLabel(labelText.toString(), lang);
    }


    public static class Parameters {

        private final ParseContext parseContext;
        private final CroisementParseEngine croisementParseEngine;
        private final Thesaurus mainThesaurus;
        private final IncludeKey includeKey;
        private final Lang lang;


        public Parameters(ParseContext parseContext, CroisementParseEngine croisementParseEngine, Thesaurus mainThesaurus, IncludeKey includeKey, Lang lang) {
            this.parseContext = parseContext;
            this.croisementParseEngine = croisementParseEngine;
            this.mainThesaurus = mainThesaurus;
            this.includeKey = includeKey;
            this.lang = lang;
        }

        public ParseContext getParseContext() {
            return parseContext;
        }

        public CroisementParseEngine getCroisementParseEngine() {
            return croisementParseEngine;
        }

        public Thesaurus getMainThesaurus() {
            return mainThesaurus;
        }

        public IncludeKey getIncludeKey() {
            return includeKey;
        }

        public String getMode() {
            return includeKey.getMode();
        }

        public Lang getLang() {
            return lang;
        }

    }

}
