/* FichothequeLib_Tools - Copyright (c) 2015-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public class TokenKeys {

    public final static String DOCUMENT_REF = "document_ref";
    public final static String DOCUMENT_FILE = "document_file";
    public final static String ILLUSTRATION_REF = "illustration_ref";
    public final static String ILLUSTRATION_FILE = "illustration_file";
    private final List<TokenKey> documentRefList = new ArrayList<TokenKey>();
    private final List<TokenKey> documentFileList = new ArrayList<TokenKey>();
    private final List<TokenKey> illustrationRefList = new ArrayList<TokenKey>();
    private final List<TokenKey> illustrationFileList = new ArrayList<TokenKey>();


    private TokenKeys() {

    }

    public List<TokenKey> getTokenKeyList(String name) {
        switch (name) {
            case DOCUMENT_REF:
                return documentRefList;
            case DOCUMENT_FILE:
                return documentFileList;
            case ILLUSTRATION_REF:
                return illustrationRefList;
            case ILLUSTRATION_FILE:
                return illustrationFileList;
            default:
                return null;
        }
    }

    public boolean withIllustration() {
        return (!illustrationFileList.isEmpty()) || (!illustrationRefList.isEmpty());
    }

    public boolean withDocument() {
        return (!documentFileList.isEmpty()) || (!documentRefList.isEmpty());
    }

    public static TokenKeys build(Set<String> paramNameSet) {
        TokenKeys tokenKeys = new TokenKeys();
        for (String paramName : paramNameSet) {
            try {
                TokenKey tokenKey = TokenKey.parse(paramName);
                short category = tokenKey.getIncludeKey().getSubsetKey().getCategory();
                if (category == SubsetKey.CATEGORY_ALBUM) {
                    if (tokenKey.getSubname().equals("file")) {
                        tokenKeys.illustrationFileList.add(tokenKey);
                    } else {
                        tokenKeys.illustrationRefList.add(tokenKey);
                    }
                } else if (category == SubsetKey.CATEGORY_ADDENDA) {
                    if (tokenKey.getSubname().equals("file")) {
                        tokenKeys.documentFileList.add(tokenKey);
                    } else {
                        tokenKeys.documentRefList.add(tokenKey);
                    }
                }
            } catch (ParseException pe) {

            }
        }
        return tokenKeys;
    }

}
