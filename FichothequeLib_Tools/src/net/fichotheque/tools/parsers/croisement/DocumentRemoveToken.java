/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.include.IncludeKey;


/**
 *
 * @author Vincent Calame
 */
class DocumentRemoveToken {

    private final Document document;
    private final String mode;

    private DocumentRemoveToken(Document document, String mode) {
        this.document = document;
        this.mode = mode;
    }

    Document getDocument() {
        return document;
    }

    String getMode() {
        return mode;
    }

    static DocumentRemoveToken parse(String tokenString, Addenda addenda, IncludeKey includeKey) {
        Document document;
        try {
            int documentId = Integer.parseInt(tokenString);
            document = addenda.getDocumentById(documentId);
        } catch (NumberFormatException nfe) {
            document = addenda.getDocumentByBasename(tokenString);
        }
        if (document != null) {
            return new DocumentRemoveToken(document, includeKey.getMode());
        } else {
            return null;
        }
    }

}
