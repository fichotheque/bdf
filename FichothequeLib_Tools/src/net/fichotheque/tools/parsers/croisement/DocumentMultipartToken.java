/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.fichotheque.ExistingIdException;
import net.fichotheque.ExistingNameException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.AddendaEditor;
import net.fichotheque.addenda.Document;
import net.fichotheque.tools.croisement.LienBuffer;
import net.fichotheque.utils.AddendaUtils;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.request.FileValue;


/**
 *
 * @author Vincent Calame
 */
class DocumentMultipartToken {

    private final String basename;
    private final Addenda addenda;
    private final String mode;
    private final int poids;
    private final Map<String, FileValue> versionMap = new HashMap<String, FileValue>();

    DocumentMultipartToken(String basename, Addenda addenda, String mode, int poids) {
        this.basename = basename;
        this.addenda = addenda;
        this.mode = mode;
        if (poids > 1) {
            this.poids = poids;
        } else {
            this.poids = 1;
        }
    }

    void add(String extension, FileValue fileValue) {
        versionMap.put(extension, fileValue);
    }

    LienBuffer save(FichothequeEditor fichothequeEditor) {
        AddendaEditor addendaEditor = fichothequeEditor.getAddendaEditor(addenda);
        if (addendaEditor == null) {
            return null;
        }
        String checkedBasename = AddendaUtils.checkBasename(basename, addenda);
        Document document = null;
        for (Map.Entry<String, FileValue> entry : versionMap.entrySet()) {
            String extension = entry.getKey();
            FileValue fileValue = entry.getValue();
            if (document == null) {
                try {
                    document = addendaEditor.createDocument(-1, null);
                } catch (ExistingIdException eii) {
                    throw new ImplementationException(eii);
                }
                try {
                    addendaEditor.setBasename(document, checkedBasename);
                } catch (ExistingNameException | ParseException e) {
                    throw new ShouldNotOccurException("Test done before");
                }
            }
            try (InputStream is = fileValue.getInputStream()) {
                addendaEditor.saveVersion(document, extension, is);
            } catch (ParseException e) {
                throw new ShouldNotOccurException("Check done before");
            } catch (IOException ioe) {
                throw new NestedIOException(ioe);
            } finally {
                fileValue.free();
            }
        }
        return new LienBuffer(document, mode, poids);
    }

}
