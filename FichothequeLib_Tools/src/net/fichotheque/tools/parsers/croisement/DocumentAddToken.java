/* FichothequeLib_Tools - Copyright (c) 2021-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.include.IncludeKey;
import net.fichotheque.tools.croisement.LienBuffer;


/**
 *
 * @author Vincent Calame
 */
class DocumentAddToken {

    private final Document document;
    private final String mode;
    private final int poids;

    private DocumentAddToken(Document document, String mode, int poids) {
        this.document = document;
        this.mode = mode;
        this.poids = poids;
    }

    LienBuffer toLienBuffer() {
        return new LienBuffer(document, mode, poids);
    }

    static DocumentAddToken parse(String tokenString, Addenda addenda, IncludeKey includeKey) {
        Document document;
        try {
            int documentId = Integer.parseInt(tokenString);
            document = addenda.getDocumentById(documentId);
        } catch (NumberFormatException nfe) {
            document = addenda.getDocumentByBasename(tokenString);
        }
        if (document != null) {
            return new DocumentAddToken(document, includeKey.getMode(), includeKey.getPoidsFilter());
        } else {
            return null;
        }
    }

}
