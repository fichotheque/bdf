/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.include.IncludeKey;


/**
 *
 * @author Vincent Calame
 */
class IllustrationRemoveToken {

    private final Illustration illustration;
    private final String mode;

    private IllustrationRemoveToken(Illustration illustration, String mode) {
        this.illustration = illustration;
        this.mode = mode;
    }

    Illustration getIllustration() {
        return illustration;
    }

    String getMode() {
        return mode;
    }

    static IllustrationRemoveToken parse(String value, Album album, IncludeKey includeKey) {
        Illustration illustration = null;
        try {
            int id = Integer.parseInt(value);
            illustration = album.getIllustrationById(id);
        } catch (NumberFormatException nfe) {
        }
        if (illustration != null) {
            return new IllustrationRemoveToken(illustration, includeKey.getMode());
        } else {
            return null;
        }
    }

}
