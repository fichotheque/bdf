/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import java.io.File;
import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.ExistingNameException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.AddendaEditor;
import net.fichotheque.addenda.Document;
import net.fichotheque.include.IncludeKey;
import net.fichotheque.tools.croisement.LienBuffer;
import net.fichotheque.tools.parsers.DocumentChangeInfo;
import net.fichotheque.utils.AddendaUtils;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.text.FileName;


/**
 *
 * @author Vincent Calame
 */
class DocumentCreateToken {

    private final Addenda addenda;
    private final DocumentChangeInfo documentChangeInfo;
    private final IncludeKey includeKey;
    private final String mode;
    private final int poids;


    private DocumentCreateToken(Addenda addenda, DocumentChangeInfo documentChangeInfo, IncludeKey includeKey) {
        this.addenda = addenda;
        this.includeKey = includeKey;
        this.documentChangeInfo = documentChangeInfo;
        this.mode = includeKey.getMode();
        int poidsFilter = includeKey.getPoidsFilter();
        if (poidsFilter > 1) {
            this.poids = poidsFilter;
        } else {
            this.poids = 1;
        }
    }

    static DocumentCreateToken parse(String tokenString, Addenda addenda, IncludeKey includeKey) {
        DocumentChangeInfo documentChangeInfo = DocumentChangeInfo.parse(tokenString);
        if (documentChangeInfo.getNewBasename() == null) {
            return null;
        }
        if (documentChangeInfo.getTmpFileCount() == 0) {
            return null;
        }
        return new DocumentCreateToken(addenda, documentChangeInfo, includeKey);
    }

    LienBuffer save(FichothequeEditor fichothequeEditor, File tmpDirectory) {
        AddendaEditor addendaEditor = fichothequeEditor.getAddendaEditor(addenda);
        if (addendaEditor == null) {
            return null;
        }
        String basename = documentChangeInfo.getNewBasename();
        basename = AddendaUtils.checkBasename(basename, addendaEditor.getAddenda());
        Document document = null;
        int count = documentChangeInfo.getTmpFileCount();
        for (int i = 0; i < count; i++) {
            FileName fileName = documentChangeInfo.getTmpFileName(i);
            String extension = fileName.getExtension();
            File f = DocumentChangeToken.toTmpFile(tmpDirectory, fileName.toString());
            if (f == null) {
                continue;
            }
            if (document == null) {
                try {
                    document = addendaEditor.createDocument(-1, null);
                } catch (ExistingIdException eii) {
                    throw new ImplementationException(eii);
                }
                try {
                    addendaEditor.setBasename(document, basename);
                } catch (ExistingNameException | ParseException e) {
                    throw new ShouldNotOccurException("Test done before");
                }
            }
            DocumentChangeToken.updateVersion(addendaEditor, document, f, extension);
        }
        return new LienBuffer(document, mode, poids);
    }

    String getNewBasename() {
        return documentChangeInfo.getNewBasename();
    }

    IncludeKey getIncludeKey() {
        return includeKey;
    }

}
