/* FichothequeLib_Tools - Copyright (c) 2015-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import java.text.ParseException;
import net.fichotheque.include.IncludeKey;


/**
 *
 * @author Vincent Calame
 */
public final class TokenKey {

    private final String paramName;
    private final IncludeKey includeKey;
    private final String subname;

    public TokenKey(String paramName, IncludeKey includeKey, String subname) {
        this.paramName = paramName;
        this.includeKey = includeKey;
        this.subname = subname;
    }

    public String getParamName() {
        return paramName;
    }

    public IncludeKey getIncludeKey() {
        return includeKey;
    }

    public String getSubname() {
        return subname;
    }

    public static TokenKey parse(String paramName) throws ParseException {
        int idx = paramName.indexOf(':');
        if (idx == -1) {
            throw new ParseException("Missing :", 0);
        }
        IncludeKey includeKey = IncludeKey.parse(paramName.substring(0, idx));
        String subname = paramName.substring(idx + 1);
        if (subname.length() == 0) {
            throw new ParseException("subname is empty", idx);
        }
        return new TokenKey(paramName, includeKey, subname);
    }

}
