/* FichothequeLib_Tools - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import java.text.ParseException;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.croisement.LienBuffer;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public class LienBufferParser {

    private LienBufferParser() {
    }

    public static LienBuffer parse(Fichotheque fichotheque, String tokenString, short subsetCategory) throws ParseException {
        return parse(fichotheque, tokenString, subsetCategory, null, null);
    }

    public static LienBuffer parse(Fichotheque fichotheque, String tokenString, short subsetCategory, Subset defaultSubset, String mode) throws ParseException {
        LienToken lienToken = LienToken.parse(tokenString);
        int poids = lienToken.getPoids();
        if (mode == null) {
            mode = lienToken.getMode();
        }
        SubsetItem subsetItem = FichothequeUtils.parseGlobalId(lienToken.getText(), fichotheque, subsetCategory, defaultSubset);
        return new LienBuffer(subsetItem, mode, poids);
    }

    public static LienBuffer parseId(Subset subset, String tokenString, @Nullable String mode, int poidsFilter) throws ParseException {
        LienToken lienToken = LienToken.parse(tokenString);
        int poids;
        if (poidsFilter < 1) {
            poids = lienToken.getPoids();
        } else {
            poids = poidsFilter;
        }
        if (mode == null) {
            mode = lienToken.getMode();
        }
        int id;
        try {
            id = Integer.parseInt(lienToken.getText());
        } catch (NumberFormatException nfe) {
            throw new ParseException(tokenString, 0);
        }
        if (id < 0) {
            throw new ParseException(tokenString, 0);
        }
        SubsetItem subsetItem = subset.getSubsetItemById(id);
        if (subsetItem == null) {
            throw new ParseException(tokenString, 0);
        }
        return new LienBuffer(subsetItem, mode, poids);
    }

    public static LienBuffer parseIdalpha(Thesaurus thesaurus, String tokenString, String mode, int poidsFilter) throws ParseException {
        LienToken lienToken = LienToken.parse(tokenString);
        int poids;
        if (poidsFilter < 1) {
            poids = lienToken.getPoids();
        } else {
            poids = poidsFilter;
        }
        if (mode == null) {
            mode = lienToken.getMode();
        }
        Motcle motcle = thesaurus.getMotcleByIdalpha(lienToken.getText());
        if (motcle == null) {
            throw new ParseException(tokenString, 0);
        }
        return new LienBuffer(motcle, mode, poids);
    }

}
