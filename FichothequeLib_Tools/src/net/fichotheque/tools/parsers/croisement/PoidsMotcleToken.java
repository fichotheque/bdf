/* FichothequeLib_Tools - Copyright (c) 2007-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public class PoidsMotcleToken {

    private boolean idBundle = false;
    private int poids = 1;
    private int id;
    private CleanedString text;

    private PoidsMotcleToken(int id, int poids) {
        this.id = id;
        this.poids = poids;
        idBundle = true;
    }

    private PoidsMotcleToken(CleanedString text, int poids) {
        this.text = CleanedString.newInstance(text);
        this.poids = poids;
        idBundle = false;

    }

    public static PoidsMotcleToken parse(String s, boolean withIdalpha, int poidsFilter) {
        LienToken lienToken = LienToken.parse(s);
        String text = lienToken.getText();
        int poids;
        if (poidsFilter < 1) {
            poids = lienToken.getPoids();
        } else {
            poids = poidsFilter;
        }
        if (!withIdalpha) {
            try {
                int id = Integer.parseInt(text);
                return new PoidsMotcleToken(id, poids);
            } catch (NumberFormatException nfe) {
            }
        }
        if (text.startsWith("=")) {
            try {
                int id = Integer.parseInt(text.substring(1).trim());
                return new PoidsMotcleToken(id, poids);
            } catch (NumberFormatException nfe) {
            }
        }
        CleanedString cleanedString = CleanedString.newInstance(text);
        if (cleanedString == null) {
            return null;
        }
        return new PoidsMotcleToken(cleanedString, poids);
    }

    public boolean isIdBundle() {
        return idBundle;
    }

    public int getPoids() {
        return poids;
    }

    public int getId() {
        return id;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    /**
     * Null si et seulement si isIdBundle() = true
     */
    public CleanedString getText() {
        return text;
    }

    public static Motcle getMotcle(PoidsMotcleToken motcleToken, Thesaurus thesaurus, Lang lang) {
        if (motcleToken.isIdBundle()) {
            return thesaurus.getMotcleById(motcleToken.getId());
        }
        CleanedString cleanedString = motcleToken.getText();
        if (cleanedString == null) {
            return null;
        }
        String text = cleanedString.toString();
        if (thesaurus.isIdalphaType()) {
            return thesaurus.getMotcleByIdalpha(text);
        } else {
            return thesaurus.seekMotcleByLabel(text, lang);
        }
    }

}
