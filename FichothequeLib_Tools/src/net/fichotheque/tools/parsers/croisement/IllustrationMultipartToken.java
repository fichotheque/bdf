/* FichothequeLib_Tools - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.croisement;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumConstants;
import net.fichotheque.album.AlbumEditor;
import net.fichotheque.album.Illustration;
import net.fichotheque.tools.croisement.LienBuffer;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.images.ImagesUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.FileValue;


/**
 *
 * @author Vincent Calame
 */
class IllustrationMultipartToken {

    private final Album album;
    private final String extension;
    private final String mode;
    private final int poids;
    private final FileValue fileValue;

    IllustrationMultipartToken(Album album, String extension, String mode, int poids, FileValue fileValue) {
        this.album = album;
        this.extension = extension;
        this.mode = mode;
        if (poids > 1) {
            this.poids = poids;
        } else {
            this.poids = 1;
        }
        this.fileValue = fileValue;
    }

    LienBuffer createIllustration(FichothequeEditor fichothequeEditor) {
        AlbumEditor albumEditor = fichothequeEditor.getAlbumEditor(album);
        if (albumEditor == null) {
            return null;
        }
        short formatType;
        File tmpFile = null;
        if (AlbumUtils.isPngConvertible(extension)) {
            formatType = AlbumConstants.PNG_FORMATTYPE;
            try (InputStream is = fileValue.getInputStream()) {
                BufferedImage srcImg = ImagesUtils.read(is);
                tmpFile = File.createTempFile("upload", "png");
                ImagesUtils.write(srcImg, "png", tmpFile);
            } catch (IOException ioe) {
                throw new NestedIOException(ioe);
            } catch (ErrorMessageException eme) {
                tmpFile = null;
            } finally {
                fileValue.free();
            }
            if (tmpFile == null) {
                return null;
            }
        } else {
            formatType = AlbumUtils.formatTypeToShort(extension);
        }
        Illustration illustration;
        try {
            illustration = albumEditor.createIllustration(-1, formatType);
        } catch (ExistingIdException eie) {
            throw new ShouldNotOccurException(eie);
        }
        if (tmpFile != null) {
            try (InputStream is = new FileInputStream(tmpFile)) {
                albumEditor.updateIllustration(illustration, is, formatType);
            } catch (IOException ioe) {
                throw new NestedIOException(ioe);
            } catch (ErrorMessageException eme) {

            } finally {
                tmpFile.delete();
            }
        } else {
            try (InputStream is = fileValue.getInputStream()) {
                albumEditor.updateIllustration(illustration, is, formatType);
            } catch (IOException ioe) {
                throw new NestedIOException(ioe);
            } catch (ErrorMessageException eme) {
            } finally {
                fileValue.free();
            }
        }
        return new LienBuffer(illustration, mode, poids);
    }

}
