/* FichothequeLib_Tools - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.ficheblock;

import java.util.List;
import net.fichotheque.corpus.fiche.Atts;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.P;
import net.fichotheque.utils.FicheUtils;


/**
 *
 * @author Vincent Calame
 */
class ParagraphState {

    private boolean first = true;
    private boolean previousH = false;
    private int whiteLineCount = 0;
    private Atts previousAtts = null;

    ParagraphState() {
    }

    void reinit() {
        first = true;
        previousH = false;
        whiteLineCount = 0;
        previousAtts = null;
    }

    boolean isFirst() {
        return first;
    }

    void increaseWhiteLineCount() {
        if (!first) {
            whiteLineCount++;
        }
    }

    void checkH() {
        previousH = true;
        whiteLineCount = 0;
        first = false;
    }

    void flushWhiteLines(List<FicheBlock> ficheBlockList) {
        if (previousH) {
            whiteLineCount--;
        }
        previousH = false;
        if (whiteLineCount > 1) {
            for (int i = 1; i < whiteLineCount; i++) {
                ficheBlockList.add(new P());
            }
        }
        whiteLineCount = 0;
        first = false;
    }

    void flushPreviousAtts(List<FicheBlock> ficheBlockList) {
        if ((previousAtts != null) && (!previousAtts.hasOnlyAutomaticAtts())) {
            flushWhiteLines(ficheBlockList);
            P p = new P();
            FicheUtils.populate(p, previousAtts);
            previousAtts = null;
        }
    }

    Atts getPreviousAtts() {
        return previousAtts;
    }

    void setPreviousAtts(Atts previousAtts) {
        this.previousAtts = previousAtts;
    }

}
