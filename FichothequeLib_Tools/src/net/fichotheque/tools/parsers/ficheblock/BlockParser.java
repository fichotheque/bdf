/* FichothequeLib_Tools - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.ficheblock;

import net.fichotheque.corpus.fiche.Atts;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.ZoneBlock;
import net.fichotheque.tools.parsers.TextContentParser;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
abstract class BlockParser {

    private int endIndex;
    private FicheBlock ficheBlock;

    BlockParser() {
    }

    public int getEndIndex() {
        return endIndex;
    }

    protected void setParseResult(int endIndex, FicheBlock ficheBlock) {
        this.endIndex = endIndex;
        this.ficheBlock = ficheBlock;
    }

    public abstract void parse(String[] lineArray, int startIndex, Atts initAtts);

    public abstract boolean isStartLine(String line);

    public FicheBlock getFicheBlock() {
        return ficheBlock;
    }

    public static int parseZoneBlockElements(ZoneBlock zoneBlock, int beginIndex, String[] lineArray, TextContentParser textContentParser) {
        int length = lineArray.length;
        int blankLineCount = 0;
        while (beginIndex < length) {
            String line = StringUtils.cleanString(lineArray[beginIndex]);
            if (line.length() > 0) {
                if (line.startsWith("n=")) {
                    blankLineCount = 0;
                    textContentParser.parse(zoneBlock.getNumeroBuilder(), line.substring(2).trim());
                    beginIndex++;
                } else if (line.startsWith("l=")) {
                    blankLineCount = 0;
                    textContentParser.parse(zoneBlock.getLegendeBuilder(), line.substring(2).trim());
                    beginIndex++;
                } else {
                    break;
                }
            } else {
                blankLineCount++;
                beginIndex++;
            }
        }
        return beginIndex - blankLineCount;
    }

}
