/* FichothequeLib_Tools - Copyright (c) 2007-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.ficheblock;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.corpus.fiche.Atts;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.corpus.fiche.Div;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.tools.parsers.TextContentParser;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
class DivParser extends BlockParser {

    private final TextContentParser textContentParser;
    private final TypoOptions typoOptions;
    private final ContentChecker contentChecker;
    private final boolean withLineNumber;

    DivParser(TextContentParser textContentParser, TypoOptions typoOptions, ContentChecker contentChecker, boolean withLineNumber) {
        this.textContentParser = textContentParser;
        this.typoOptions = typoOptions;
        this.contentChecker = contentChecker;
        this.withLineNumber = withLineNumber;
    }

    @Override
    public boolean isStartLine(String line) {
        return line.startsWith("!!!");
    }

    @Override
    public void parse(String[] lineArray, int startIndex, Atts initAtts) {
        Div div = new Div();
        String firstLine = lineArray[startIndex];
        if (firstLine.length() > 3) {
            String params = firstLine.substring(3).trim();
            if (params.length() > 0) {
                try {
                    Lang lang = Lang.parse(params);
                    div.setLang(lang);
                } catch (ParseException pe) {
                }
            }
        }
        FicheUtils.populate(div, initAtts);
        int beginIndex = BlockParser.parseZoneBlockElements(div, startIndex + 1, lineArray, textContentParser);
        int stop = lineArray.length - 1;
        List<FicheBlock> list = new ArrayList<FicheBlock>();
        FicheBlockParser ficheBlockParser = new FicheBlockParser(contentChecker, typoOptions, withLineNumber);
        for (int i = beginIndex; i <= stop; i++) {
            String line = StringUtils.cleanString(lineArray[i]);
            if (line.startsWith("!!!")) {
                stop = i;
                break;
            }
            i = ficheBlockParser.parseLine(lineArray, i, list);
        }
        for (FicheBlock ficheBlock : list) {
            div.add(ficheBlock);
        }
        list.clear();
        setParseResult(stop, div);
    }

}
