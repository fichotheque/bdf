/* FichothequeLib_Tools - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.corpus.fiche.Courriel;
import net.fichotheque.corpus.fiche.Datation;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Geopoint;
import net.fichotheque.corpus.fiche.Image;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Langue;
import net.fichotheque.corpus.fiche.Link;
import net.fichotheque.corpus.fiche.Montant;
import net.fichotheque.corpus.fiche.Nombre;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Pays;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.corpus.metadata.SubfieldKey;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.tools.corpus.FicheChangeBuilder;
import net.fichotheque.tools.parsers.ficheblock.FicheBlockParser;
import net.fichotheque.utils.FicheUtils;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.conditions.ConditionsUtils;
import net.mapeadores.util.conditions.TextCondition;
import net.mapeadores.util.conditions.TextConditionBuilder;
import net.mapeadores.util.conditions.TextTest;
import net.mapeadores.util.localisation.CodeCatalog;
import net.mapeadores.util.localisation.Country;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.EmailCoreUtils;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.models.PersonCoreUtils;
import net.mapeadores.util.money.Currencies;
import net.mapeadores.util.money.ExtendedCurrency;
import net.mapeadores.util.money.MoneyUtils;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.primitives.DegreSexagesimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.DateFormatBundle;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
public class FicheParser {

    private final Parameters parameters;
    private final Fichotheque fichotheque;
    private final FicheChangeBuilder ficheChangeBuilder;
    private final FicheBlockParser ficheBlockParser;
    private final DateFormatBundle dateFormatBundle;
    private final TypoOptions typoOptions;
    private final TextContentParser textContentParser;
    private final Map<FieldKey, SubfieldBuffer> subfieldBufferMap = new HashMap<FieldKey, SubfieldBuffer>();
    private SubsetKey defaultSphereKey = null;

    public FicheParser(Parameters parameters, Corpus corpus, FicheChangeBuilder ficheChangeBuilder) {
        this.parameters = parameters;
        this.ficheChangeBuilder = ficheChangeBuilder;
        this.dateFormatBundle = DateFormatBundle.getDateFormatBundle(parameters.getUserLangContext().getFormatLocale());
        Redacteur userRedacteur = parameters.getUserRedacteur();
        if (userRedacteur != null) {
            this.defaultSphereKey = userRedacteur.getSubsetKey();
        }
        this.typoOptions = parameters.getTypoOptions();
        fichotheque = corpus.getFichotheque();
        textContentParser = new TextContentParser(typoOptions);
        ficheBlockParser = new FicheBlockParser(parameters.getContentChecker(), typoOptions, false);
    }

    public void flushParsedSubfields() {
        for (SubfieldBuffer subfieldBuffer : subfieldBufferMap.values()) {
            subfieldBuffer.flush();
        }
    }

    public void parseCorpusField(CorpusField corpusField, String value) {
        switch (corpusField.getCategory()) {
            case FieldKey.PROPRIETE_CATEGORY:
                parsePropriete(corpusField, cleanProprieteToken(value, parameters.getItemListSeparateur()));
                break;
            case FieldKey.INFORMATION_CATEGORY:
                String[] tokens = StringUtils.getTechnicalTokens(value, parameters.getItemListSeparateur(), false);
                parseInformation(corpusField, tokens);
                break;
            case FieldKey.SECTION_CATEGORY:
                parseSection(corpusField, value);
                break;
            case FieldKey.SPECIAL_CATEGORY:
                switch (corpusField.getFieldString()) {
                    case FieldKey.SPECIAL_TITRE:
                        parseTitre(value);
                        break;
                    case FieldKey.SPECIAL_SOUSTITRE:
                        parseSoustitre(value);
                        break;
                    case FieldKey.SPECIAL_LANG:
                        parseFicheLang(value);
                        break;
                    case FieldKey.SPECIAL_REDACTEURS: {
                        String[] redacteurTokens = StringUtils.getTechnicalTokens(value, parameters.getItemListSeparateur(), false);
                        parseRedacteurs(redacteurTokens);
                        break;
                    }
                }
                break;
        }
    }

    public void parseSubfield(CorpusField corpusField, SubfieldKey subfieldKey, String value) {
        if (SubfieldKey.isLegalSubfield(corpusField, subfieldKey.getSubtype())) {
            SubfieldBuffer fieldBuffer = subfieldBufferMap.get(corpusField.getFieldKey());
            if (fieldBuffer == null) {
                fieldBuffer = new SubfieldBuffer(corpusField);
                subfieldBufferMap.put(corpusField.getFieldKey(), fieldBuffer);
            }
            fieldBuffer.putSubfield(subfieldKey, value);
        }
    }

    public void parseFicheLang(String s) {
        if (s == null) {
            return;
        }
        Langue langue = parseLangue(s);
        if (langue != null) {
            ficheChangeBuilder.setLang(langue.getLang());
        } else {
            ficheChangeBuilder.setLang(null);
        }
    }

    public void parseTitre(String s) {
        ficheChangeBuilder.setTitre(CleanedString.newInstance(TypoParser.parseTypo(s, typoOptions)));
    }

    public void parseSoustitre(String s) {
        CleanedString cs = CleanedString.newInstance(s);
        if (cs != null) {
            ficheChangeBuilder.setSoustitre(parsePara(cs.toString()));
        } else {
            ficheChangeBuilder.setSoustitre(null);
        }
    }

    public void parseRedacteurs(String[] tokens) {
        FicheItems ficheItems = parseRedacteurItemList(tokens, defaultSphereKey);
        ficheChangeBuilder.putRedacteurs(ficheItems);
    }

    public void parsePropriete(CorpusField proprieteField, String s) {
        s = StringUtils.cleanString(s);
        FicheItem ficheItem = null;
        if (s.length() > 0) {
            ficheItem = parseFicheItem(s, proprieteField);
        }
        ficheChangeBuilder.setPropriete(proprieteField.getFieldKey(), ficheItem);
    }

    public void parsePersonnePropriete(CorpusField proprieteField, String surname, String forename, String nonlatin, String surnameFirstString) {
        surname = StringUtils.cleanString(surname);
        forename = StringUtils.cleanString(forename);
        nonlatin = StringUtils.cleanString(nonlatin);
        boolean surnameFirst = StringUtils.isTrue(surnameFirstString);
        FicheItem redacteurItem = parseRedacteurItem(surname, forename, nonlatin, surnameFirst);
        ficheChangeBuilder.setPropriete(proprieteField.getFieldKey(), redacteurItem);
    }

    public void parseMontantPropriete(CorpusField proprieteField, String value, String cur) {
        FicheItem ficheItem = null;
        boolean itemAllowed = true;
        value = StringUtils.cleanString(value);
        if (value.length() > 0) {
            ficheItem = parseMontant(value);
            if (ficheItem == null) {
                cur = StringUtils.cleanString(cur);
                if (cur.length() == 0) {
                    Currencies currencies = proprieteField.getCurrencies();
                    cur = currencies.get(0).getCurrencyCode();
                }
                ficheItem = parseMontant(value, cur);
            }
            if (ficheItem == null) {
                if (itemAllowed) {
                    if (!value.startsWith("?")) {
                        value = "?" + value;
                    }
                }
                ficheItem = parseItem(value, itemAllowed);
            }
        }
        ficheChangeBuilder.setPropriete(proprieteField.getFieldKey(), ficheItem);
    }

    public void parseImagePropriete(CorpusField proprieteField, String src, String alt, String title) {
        Image image = null;
        src = StringUtils.cleanString(src);
        if (src.length() > 0) {
            alt = StringUtils.cleanString(alt);
            title = StringUtils.cleanString(title);
            image = new Image(src, alt, title);
        }
        ficheChangeBuilder.setPropriete(proprieteField.getFieldKey(), image);
    }

    public void parseGeopointPropriete(CorpusField proprieteField, String latitude, String longitude) {
        latitude = StringUtils.cleanString(latitude);
        longitude = StringUtils.cleanString(longitude);
        DegreDecimal latitudeDecimal = parseDegreDecimal(latitude);
        DegreDecimal longitudeDecimal = parseDegreDecimal(longitude);
        FicheItem ficheItem = null;
        boolean itemAllowed = true;
        if ((latitudeDecimal != null) && (longitudeDecimal != null)) {
            ficheItem = new Geopoint(latitudeDecimal, longitudeDecimal);
        } else {
            if ((latitude.length() > 0) || (longitude.length() > 0)) {
                if (itemAllowed) {
                    if (!latitude.startsWith("?? ")) {
                        latitude = "?? " + latitude;
                    }
                }
                ficheItem = parseItem(latitude + " ?? " + longitude, itemAllowed);
            }
        }
        ficheChangeBuilder.setPropriete(proprieteField.getFieldKey(), ficheItem);
    }

    public void parseInformation(CorpusField informationField, String[] tokens) {
        List<FicheItem> ficheItemCollection = new ArrayList<FicheItem>();
        for (String token : tokens) {
            FicheItem ficheItem = parseFicheItem(token, informationField);
            if (ficheItem != null) {
                ficheItemCollection.add(ficheItem);
            }
        }
        addInInformation(informationField, FicheUtils.toFicheItems(ficheItemCollection));
    }

    public void addInInformation(CorpusField informationField, FicheItems ficheItems) {
        ficheChangeBuilder.putInformation(informationField.getFieldKey(), ficheItems);
    }

    public void parseSection(CorpusField sectionField, String s) {
        List<FicheBlock> list = new ArrayList<FicheBlock>();
        ficheBlockParser.parseFicheBlockList(s, list);
        FicheBlocks ficheBlocks = FicheUtils.toFicheBlocks(list);
        ficheChangeBuilder.putSection(sectionField.getFieldKey(), ficheBlocks);
    }

    public FicheItem parseFicheItem(String token, CorpusField corpusField) {
        int ficheItemType = corpusField.getFicheItemType();
        boolean itemAllowed = true;
        if (ficheItemType == CorpusField.LANGUE_FIELD) {
            Langue langue = parseLangue(token);
            if (langue != null) {
                return langue;
            } else {
                return parseItem(token, itemAllowed);
            }
        }
        if (ficheItemType == CorpusField.PAYS_FIELD) {
            Pays pays = parsePays(token);
            if (pays != null) {
                return pays;
            } else {
                return parseItem(token, itemAllowed);
            }
        }
        if (ficheItemType == CorpusField.DATATION_FIELD) {
            try {
                FuzzyDate date = FuzzyDate.parse(token, dateFormatBundle);
                return new Datation(date);
            } catch (ParseException pe) {
                return parseItem(token, itemAllowed);
            }
        }
        if (ficheItemType == CorpusField.LINK_FIELD) {
            Link link = parseLink(token);
            if (link != null) {
                return link;
            } else {
                return parseItem(token, itemAllowed);
            }
        }
        if (ficheItemType == CorpusField.NOMBRE_FIELD) {
            try {
                Decimal decimal = StringUtils.parseDecimal(token);
                return new Nombre(decimal);
            } catch (NumberFormatException nfe) {
                if (itemAllowed) {
                    if (!token.startsWith("?")) {
                        token = "?" + token;
                    }
                }
                return parseItem(token, itemAllowed);
            }
        }
        if (ficheItemType == CorpusField.MONTANT_FIELD) {
            Montant montant = parseMontant(token);
            if (montant != null) {
                return montant;
            }
            if (itemAllowed) {
                if (!token.startsWith("?")) {
                    token = "?" + token;
                }
            }
            return parseItem(token, itemAllowed);
        }
        if (ficheItemType == CorpusField.COURRIEL_FIELD) {
            try {
                EmailCore emailCore = EmailCoreUtils.parse(token);
                return new Courriel(emailCore);
            } catch (ParseException pe) {
                return parseItem(token, itemAllowed);
            }
        }
        if (ficheItemType == CorpusField.PERSONNE_FIELD) {
            return parseRedacteurItem(token, getDefaultSphereKey(corpusField));
        }
        if (ficheItemType == CorpusField.ITEM_FIELD) {
            return new Item(TypoParser.parseTypo(token, typoOptions));
        }
        if (ficheItemType == CorpusField.GEOPOINT_FIELD) {
            Geopoint geopoint = parseGeopoint(token);
            if (geopoint != null) {
                return geopoint;
            } else {
                return parseItem(token, itemAllowed);
            }
        }
        if (ficheItemType == CorpusField.PARA_FIELD) {
            return parsePara(token);
        }
        if (ficheItemType == CorpusField.IMAGE_FIELD) {
            Image image = parseImage(token);
            if (image != null) {
                return image;
            } else {
                return parseItem(token, itemAllowed);
            }
        }
        return null;
    }

    private Para parsePara(String token) {
        Para.Builder paraBuilder = new Para.Builder();
        textContentParser.parse(paraBuilder, token);
        return paraBuilder.toPara();
    }

    private FicheItems parseRedacteurItemList(String[] tokens, SubsetKey defaultSphereKey) {
        List<FicheItem> list = new ArrayList<FicheItem>();
        for (String token : tokens) {
            FicheItem redacteurItem = parseRedacteurItem(token, defaultSphereKey);
            if (redacteurItem != null) {
                list.add(redacteurItem);
            }
        }
        return FicheUtils.toFicheItems(list);
    }

    private FicheItem parseRedacteurItem(String token, SubsetKey defaultSphereKey) {
        try {
            Redacteur redacteur = SphereUtils.parse(fichotheque, token, defaultSphereKey);
            return new Personne(redacteur.getGlobalId());
        } catch (SphereUtils.RedacteurLoginException rle) {
            Personne personne = parsePersonne(token, typoOptions);
            if (personne != null) {
                return personne;
            }
            return new Item(TypoParser.parseTypo(token, typoOptions));
        }
    }

    private FicheItem parseRedacteurItem(String surname, String forename, String nonlatin, boolean surnameFirst) {
        if ((forename.length() == 0) && (nonlatin.length() == 0)) {
            try {
                Redacteur redacteur = SphereUtils.parse(fichotheque, surname);
                return new Personne(redacteur.getGlobalId());
            } catch (SphereUtils.RedacteurLoginException rle) {
            }
            if (surname.length() == 0) {
                return null;
            }
            if (!surnameFirst) {
                return new Item(TypoParser.parseTypo(surname, typoOptions));
            }
        }
        surname = TypoParser.parseTypo(surname, typoOptions);
        forename = TypoParser.parseTypo(forename, typoOptions);
        nonlatin = TypoParser.parseTypo(nonlatin, typoOptions);
        return new Personne(PersonCoreUtils.toPersonCore(surname, forename, nonlatin, surnameFirst), null);
    }

    public FicheItem parseMontantInformationSubfield(CorpusField corpusField, ExtendedCurrency currency, String value) {
        value = StringUtils.cleanString(value);
        boolean itemAllowed = true;
        if (value.isEmpty()) {
            return null;
        }
        try {
            Decimal decimal = StringUtils.parseDecimal(value);
            return new Montant(decimal, currency);
        } catch (NumberFormatException nfe) {
            if (itemAllowed) {
                if (!value.startsWith("?")) {
                    value = "?" + value;
                }
            }
            return parseItem(value, itemAllowed);
        }
    }

    private Item parseItem(String token, boolean itemAllowed) {
        if (itemAllowed) {
            return new Item(TypoParser.parseTypo(token, typoOptions));
        } else {
            return null;
        }
    }

    public Link parseLink(String s) {
        int separateurinterne = '*';
        int posseparateur = s.indexOf(separateurinterne);
        if (posseparateur == -1) {
            separateurinterne = '|';
            posseparateur = s.indexOf(separateurinterne);
        }
        String href;
        if (posseparateur == - 1) {
            href = s;
        } else {
            href = s.substring(0, posseparateur).trim();
        }
        if (href.startsWith("www.")) {
            href = "http://" + href;
        }
        try {
            URI uri = new URI(href);
            if (uri.isAbsolute()) {
                URL url = uri.toURL();
                href = url.toString();
            } else {
                href = uri.toString();
            }
            String title = "";
            String comment = "";
            if (posseparateur > -1) {
                String bout = s.substring(posseparateur + 1).trim();
                int pos2 = bout.indexOf(separateurinterne);
                if (pos2 > -1) {
                    title = bout.substring(0, pos2).trim();
                    comment = bout.substring(pos2 + 1).trim();
                } else {
                    title = bout;
                }
            }
            return new Link(href, TypoParser.parseTypo(title, typoOptions), TypoParser.parseTypo(comment, typoOptions));
        } catch (URISyntaxException | MalformedURLException e) {
            return null;
        }
    }

    public Image parseImage(String s) {
        int separateurinterne = '*';
        int posseparateur = s.indexOf(separateurinterne);
        String src;
        String alt = "";
        String title = "";
        if (posseparateur == - 1) {
            src = s;
        } else {
            src = s.substring(0, posseparateur).trim();
            int pos2 = s.indexOf(separateurinterne, posseparateur + 1);
            if (pos2 == -1) {
                alt = s.substring(posseparateur + 1).trim();
            } else {
                alt = s.substring(posseparateur + 1, pos2).trim();
                title = s.substring(pos2 + 1).trim();
            }
        }
        return new Image(src, TypoParser.parseTypo(alt, typoOptions), TypoParser.parseTypo(title, typoOptions));
    }

    private Geopoint parseGeopoint(String s) {
        DegreDecimal latitude = null;
        DegreDecimal longitude = null;
        int idxAster = s.indexOf('*');
        if (idxAster != -1) {
            latitude = parseDegreDecimal(s.substring(0, idxAster));
            longitude = parseDegreDecimal(s.substring(idxAster + 1));
        } else {
        }
        if ((latitude != null) && (longitude != null)) {
            return new Geopoint(latitude, longitude);
        } else {
            return null;
        }
    }

    private DegreDecimal parseDegreDecimal(String s) {
        try {
            Decimal decimal = StringUtils.parseDecimal(s);
            return DegreDecimal.newInstance(decimal);
        } catch (NumberFormatException nfe) {
            try {
                DegreSexagesimal ds = DegreSexagesimal.parse(s);
                return DegreDecimal.newInstance(ds);
            } catch (ParseException pe) {
                return null;
            }
        }
    }

    private Montant parseMontant(String s) {
        String[] result = MoneyUtils.splitMoney(s);
        if (result == null) {
            return null;
        }
        return parseMontant(result[0], result[1]);
    }


    private Montant parseMontant(String valString, String curString) {
        try {
            Decimal decimal = StringUtils.parseDecimal(valString);
            ExtendedCurrency currency = ExtendedCurrency.parse(curString);
            return new Montant(decimal, currency);
        } catch (ParseException | NumberFormatException e) {
            return null;
        }
    }

    private Langue parseLangue(String valString) {
        try {
            Lang lang = Lang.parse(valString);
            return new Langue(lang);
        } catch (ParseException mcle) {
            TextTest textTest = ConditionsUtils.parseTextTest(valString);
            if (textTest != null) {
                TextCondition condition = TextConditionBuilder.build(textTest);
                Set<String> result = parameters.getCodeCatalog().getLangCodeSet(condition, parameters.getUserLangContext().getLangPreference());
                if (!result.isEmpty()) {
                    for (String code : result) {
                        return new Langue(Lang.build(code));
                    }
                }
            }
            return null;
        }
    }

    private Pays parsePays(String valString) {
        try {
            Country country = Country.parse(valString);
            return new Pays(country);
        } catch (ParseException pe) {
            TextTest textTest = ConditionsUtils.parseTextTest(valString);
            if (textTest != null) {
                TextCondition condition = TextConditionBuilder.build(textTest);
                Set<String> result = parameters.getCodeCatalog().getCountryCodeSet(condition, parameters.getUserLangContext().getLangPreference());
                if (!result.isEmpty()) {
                    for (String code : result) {
                        return new Pays(Country.build(code));
                    }
                }
            }
            return null;
        }
    }


    private SubsetKey getDefaultSphereKey(CorpusField corpusField) {
        SubsetKey customSphereKey = corpusField.getDefaultSphereKey();
        if (customSphereKey == null) {
            customSphereKey = defaultSphereKey;
        }
        return customSphereKey;
    }

    private String cleanProprieteToken(String s, char itemListSeparateur) {
        int length = s.length();
        int p = length;
        for (int i = (length - 1); i >= 0; i--) {
            char carac = s.charAt(i);
            if ((carac == itemListSeparateur) || (Character.isWhitespace(carac))) {
                p--;
            } else {
                break;
            }
        }
        if (p == 0) {
            return "";
        } else if (p == length) {
            return s;
        } else {
            return s.substring(0, p);
        }
    }

    public static Personne parsePersonne(String s, TypoOptions typoOptions) {
        int separateurinterne = '*';
        int posvirg = s.indexOf(separateurinterne);
        if (posvirg == -1) {
            separateurinterne = '|';
            posvirg = s.indexOf(separateurinterne);
        }
        if (posvirg > -1) {
            String surname = TypoParser.parseTypo(s, 0, posvirg, typoOptions);
            String forename = "";
            String organism = null;
            if (posvirg < (s.length() - 1)) {
                String suite = s.substring(posvirg + 1).trim();
                int pos2 = suite.indexOf(separateurinterne);
                if (pos2 > -1) {
                    forename = suite.substring(0, pos2);
                    if (pos2 < (suite.length() - 1)) {
                        organism = suite.substring(pos2 + 1);
                    }
                } else {
                    forename = suite;
                }
            }
            boolean surnameFirst = false;
            if ((forename.length() > 0) && (forename.charAt(0) == '%')) {
                forename = forename.substring(1);
                surnameFirst = true;
            }
            forename = TypoParser.parseTypo(forename, typoOptions);
            PersonCore personCore = PersonCoreUtils.toPersonCore(surname, forename, "", surnameFirst);
            return new Personne(personCore, TypoParser.parseTypo(organism, typoOptions));
        } else {
            return null;
        }
    }


    private class SubfieldBuffer {

        private final CorpusField corpusField;
        private final Map<SubfieldKey, String> subfieldMap = new LinkedHashMap<SubfieldKey, String>();

        private SubfieldBuffer(CorpusField corpusField) {
            this.corpusField = corpusField;
        }

        private void putSubfield(SubfieldKey subfieldKey, String value) {
            subfieldMap.put(subfieldKey, value);
        }

        private void flush() {
            switch (corpusField.getFicheItemType()) {
                case CorpusField.MONTANT_FIELD:
                    if (corpusField.isPropriete()) {
                        flushMontantPropriete();
                    } else {
                        flushMontantInformation();
                    }
                    break;
                case CorpusField.PERSONNE_FIELD:
                    flushPersonne();
                    break;
                case CorpusField.GEOPOINT_FIELD:
                    flushGeopoint();
                    break;
                case CorpusField.IMAGE_FIELD:
                    flushImage();
                    break;
            }
        }

        private void flushMontantPropriete() {
            String num = "";
            String cur = "";
            for (Map.Entry<SubfieldKey, String> entry : subfieldMap.entrySet()) {
                String value = entry.getValue();
                short type = entry.getKey().getSubtype();
                switch (type) {
                    case SubfieldKey.CURRENCY_SUBTYPE:
                        cur = value;
                        break;
                    case SubfieldKey.VALUE_SUBTYPE:
                        num = value;
                        break;
                }
            }
            parseMontantPropriete(corpusField, num, cur);
        }

        private void flushMontantInformation() {
            Set<ExtendedCurrency> currencySet = new HashSet<ExtendedCurrency>();
            List<FicheItem> ficheItemCollection = new ArrayList<FicheItem>();
            String othersValue = null;
            for (Map.Entry<SubfieldKey, String> entry : subfieldMap.entrySet()) {
                String value = entry.getValue();
                SubfieldKey subfieldKey = entry.getKey();
                short type = entry.getKey().getSubtype();
                switch (type) {
                    case SubfieldKey.AMOUNT_SUBTYPE:
                        FicheItem ficheItem = parseMontantInformationSubfield(corpusField, subfieldKey.getCurrency(), value);
                        if (ficheItem != null) {
                            if (ficheItem instanceof Montant) {
                                Montant montant = (Montant) ficheItem;
                                if (!currencySet.contains(montant.getCurrency())) {
                                    ficheItemCollection.add(montant);
                                    currencySet.add(montant.getCurrency());
                                }
                            } else {
                                ficheItemCollection.add(ficheItem);
                            }
                        }
                        break;
                    case SubfieldKey.OTHERS_SUBTYPE:
                        othersValue = value;
                        break;

                }
            }
            if (othersValue != null) {
                String[] tokens = StringUtils.getTechnicalTokens(othersValue, parameters.getItemListSeparateur(), false);
                int length = tokens.length;
                for (int i = 0; i < length; i++) {
                    FicheItem ficheItem = parseFicheItem(tokens[i], corpusField);
                    if (ficheItem != null) {
                        if (ficheItem instanceof Montant) {
                            Montant montant = (Montant) ficheItem;
                            if (!currencySet.contains(montant.getCurrency())) {
                                ficheItemCollection.add(montant);
                                currencySet.add(montant.getCurrency());
                            }
                        } else {
                            ficheItemCollection.add(ficheItem);
                        }
                    }
                }
            }
            addInInformation(corpusField, FicheUtils.toFicheItems(ficheItemCollection));
        }

        private void flushGeopoint() {
            String latitude = "";
            String longitude = "";
            for (Map.Entry<SubfieldKey, String> entry : subfieldMap.entrySet()) {
                String value = entry.getValue();
                short type = entry.getKey().getSubtype();
                switch (type) {
                    case SubfieldKey.LATITUDE_SUBTYPE:
                        latitude = value;
                        break;
                    case SubfieldKey.LONGITUDE_SUBTYPE:
                        longitude = value;
                        break;
                }
            }
            parseGeopointPropriete(corpusField, latitude, longitude);
        }

        private void flushPersonne() {
            String surname = "";
            String forename = "";
            String nonlatin = "";
            String surnameFirstString = "";
            for (Map.Entry<SubfieldKey, String> entry : subfieldMap.entrySet()) {
                String value = entry.getValue();
                short type = entry.getKey().getSubtype();
                switch (type) {
                    case SubfieldKey.SURNAME_SUBTYPE:
                        surname = value;
                        break;
                    case SubfieldKey.FORENAME_SUBTYPE:
                        forename = value;
                        break;
                    case SubfieldKey.NONLATIN_SUBTYPE:
                        nonlatin = value;
                        break;
                    case SubfieldKey.SURNAMEFIRST_SUBTYPE:
                        surnameFirstString = value;
                        break;
                }
            }
            parsePersonnePropriete(corpusField, surname, forename, nonlatin, surnameFirstString);
        }

        private void flushImage() {
            String src = "";
            String alt = "";
            String title = "";
            for (Map.Entry<SubfieldKey, String> entry : subfieldMap.entrySet()) {
                String value = entry.getValue();
                short type = entry.getKey().getSubtype();
                switch (type) {
                    case SubfieldKey.SRC_SUBTYPE:
                        src = value;
                        break;
                    case SubfieldKey.ALT_SUBTYPE:
                        alt = value;
                        break;
                    case SubfieldKey.TITLE_SUBTYPE:
                        title = value;
                        break;
                }
            }
            parseImagePropriete(corpusField, src, alt, title);
        }

    }


    public static class Parameters {

        private final UserLangContext userLangContext;
        private final CodeCatalog codeCatalog;
        private final ContentChecker contentChecker;
        private final TypoOptions typoOptions;
        private char itemListSeparateur = ';';
        private Redacteur userRedacteur = null;

        public Parameters(UserLangContext userLangContext, CodeCatalog codeCatalog, ContentChecker contentChecker, TypoOptions typoOptions) {
            this.userLangContext = userLangContext;
            this.codeCatalog = codeCatalog;
            this.contentChecker = contentChecker;
            this.typoOptions = typoOptions;
        }

        public Lang getWorkingLang() {
            return userLangContext.getWorkingLang();
        }

        public UserLangContext getUserLangContext() {
            return userLangContext;
        }

        public CodeCatalog getCodeCatalog() {
            return codeCatalog;
        }

        public ContentChecker getContentChecker() {
            return contentChecker;
        }

        public TypoOptions getTypoOptions() {
            return typoOptions;
        }

        public char getItemListSeparateur() {
            return itemListSeparateur;
        }

        public void setItemListSeparateur(char itemListSeparateur) {
            this.itemListSeparateur = itemListSeparateur;
        }

        public Redacteur getUserRedacteur() {
            return userRedacteur;
        }

        public void setUserRedacteur(Redacteur userRedacteur) {
            this.userRedacteur = userRedacteur;
        }

    }

}
