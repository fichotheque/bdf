/* FichothequeLib_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.span;

import net.fichotheque.corpus.fiche.S;


/**
 *
 * @author Vincent Calame
 */
public class SpanParseResult {

    public final static short SUCCESS = 1;
    public final static short IGNORE = 2;
    public final static short SYNTAX_ERROR = 3;
    public final static SpanParseResult IGNORE_RESULT_1 = new SpanParseResult(IGNORE, 1);
    public final static SpanParseResult IGNORE_RESULT_2 = new SpanParseResult(IGNORE, 2);
    public final static SpanParseResult IGNORE_RESULT_3 = new SpanParseResult(IGNORE, 3);
    public final static SpanParseResult SYNTAX_EROR_RESULT_1 = new SpanParseResult(SYNTAX_ERROR, 1);
    private final S span;
    private final int endIndex;
    private final short state;
    private final int ignoreCharCount;

    SpanParseResult(short state, int ignoreCharCount) {
        this.span = null;
        this.endIndex = -1;
        this.state = state;
        this.ignoreCharCount = ignoreCharCount;
    }

    SpanParseResult(S span, int endIndex) {
        this.span = span;
        this.endIndex = endIndex;
        this.state = SUCCESS;
        this.ignoreCharCount = -1;
    }

    public short getState() {
        return state;
    }

    public S getSpan() {
        return span;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public int getIgnoreCharCount() {
        return ignoreCharCount;
    }

}
