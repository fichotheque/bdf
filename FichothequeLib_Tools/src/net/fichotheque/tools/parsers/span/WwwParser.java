/* FichothequeLib_Tools - Copyright (c) 2007-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.span;

import net.fichotheque.corpus.fiche.S;


/**
 *
 * @author Vincent Calame
 */
class WwwParser extends SpanParser {

    WwwParser() {
    }

    @Override
    public char getStartChar() {
        return 'w';
    }

    @Override
    public SpanParseResult parse(String bout, int startIndex) {
        int length = bout.length();
        if (startIndex > (length - 6)) {
            return SpanParseResult.IGNORE_RESULT_1;
        }
        if (bout.charAt(startIndex + 1) != 'w') {
            return SpanParseResult.IGNORE_RESULT_1;
        }
        if (bout.charAt(startIndex + 2) != 'w') {
            return SpanParseResult.IGNORE_RESULT_2;
        }
        if (bout.charAt(startIndex + 3) != '.') {
            return SpanParseResult.IGNORE_RESULT_3;
        }
        char prems = bout.charAt(startIndex + 4);
        if (!checkCharInUrl(prems)) {
            return SpanParseResult.IGNORE_RESULT_3;
        }
        StringBuilder refBuffer = new StringBuilder();
        refBuffer.append("http://www.");
        refBuffer.append(prems);
        int stopIndex = appendUrl(bout, refBuffer, startIndex + 5);
        S span = new S(S.LINK);
        String ref = refBuffer.toString();
        span.setRef(ref);
        span.setValue(ref.substring(7));
        return new SpanParseResult(span, stopIndex);
    }

}
