/* FichothequeLib_Tools - Copyright (c) 2007-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers.span;

import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
public abstract class SpanParser {

    SpanParser() {
    }

    public static SpanParser getEmphasisParser(TypoOptions typoOptions) {
        return new EmphasisParser(typoOptions);
    }

    public static SpanParser getBracketsParser(TypoOptions typoOptions) {
        return new BracketsParser(typoOptions);
    }

    public static SpanParser getHttpParser() {
        return new HttpParser();
    }

    public static SpanParser getWwwParser() {
        return new WwwParser();
    }

    public abstract SpanParseResult parse(String sourceString, int startIndex);

    public abstract char getStartChar();


    /*
     *Valide si un caractère peut-être dans un url ou non lorsqu'on fait une reconnaissance avec http. La règle est assez restrictive.
     * Il est à noter que rentrer un url avec [[][]] n'impose, lui aucune restriction.
     * le  point . et le point d'interrogation ? doivent être traités séparément
     */
    static boolean checkCharInUrl(char c) {
        if (c < '#' || c > '~') {
            return false;
        }
        if (c == '#') {
            return true;
        }
        if (c == '%') {
            return true;
        }
        if (c == '&') {
            return true;
        }
        if (c == '+') {
            return true;
        }
        if (c == '-') {
            return true;
        }
        if (c >= '/' && c <= ':') {
            return true;
        }
        if (c == '=') {
            return true;
        }
        if (c >= '@' && c <= 'Z') {
            return true;
        }
        if (c == '_') {
            return true;
        }
        if (c >= 'a' && c <= 'z') {
            return true;
        }
        if (c == '~') {
            return true;
        }
        return false;
    }

    static int appendUrl(String source, StringBuilder buftexte, int beginIndex) {
        int length = source.length();
        int stopIndex = beginIndex - 1;
        for (int i = beginIndex; i < length; i++) {
            char c = source.charAt(i);
            if (c == '?') {
                if (i == (length - 1)) {
                    break;
                }
                char suivant = source.charAt(i + 1);
                if (checkCharInUrl(suivant)) {
                    buftexte.append('?');
                } else {
                    break;
                }
            } else if (c == '.') {
                if (i == (length - 1)) {
                    break;
                }
                char suivant = source.charAt(i + 1);
                if (suivant == '.') {
                    buftexte.append("..");
                    i = i + 1;
                } else if (checkCharInUrl(suivant)) {
                    buftexte.append('.');
                } else {
                    break;
                }
            } else if (checkCharInUrl(c)) {
                buftexte.append(c);
            } else {
                break;
            }
            stopIndex = i;
        }
        return stopIndex;
    }

    static String getValue(String source, int beginIndex, int stopIndex, char stopChar) {
        StringBuilder buf = new StringBuilder();
        for (int i = beginIndex; i < stopIndex; i++) {
            char carac = source.charAt(i);
            if ((i == beginIndex) && (carac == ' ')) {
                continue;
            }
            if ((carac == '\\') && (i < (stopIndex - 1))) {
                char next = source.charAt(i + 1);
                if ((next == stopChar) || (next == '\\')) {
                    buf.append(next);
                    i++;
                    continue;
                }
            }
            buf.append(carac);
        }
        return buf.toString();
    }

    static int getStopIndex(String source, int beginIndex, char stopChar) {
        int length = source.length();
        for (int i = beginIndex; i < length; i++) {
            char carac = source.charAt(i);
            if (carac == stopChar) {
                return i;
            }
            if (carac == '\\') {
                i++;
            }
        }
        return -1;
    }

}
