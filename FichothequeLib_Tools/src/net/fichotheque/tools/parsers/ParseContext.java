/* FichothequeLib_Tools - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.parsers;

import java.util.function.Predicate;
import net.fichotheque.Fichotheque;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.Subset;
import net.fichotheque.externalsource.ExternalSourceProvider;
import net.fichotheque.thesaurus.metadata.ThesaurusLangChecker;
import net.fichotheque.thesaurus.policies.PolicyProvider;


/**
 *
 * @author Vincent Calame
 */
public class ParseContext {

    private final FichothequeEditor fichothequeEditor;
    private final Fichotheque fichotheque;
    private final ThesaurusLangChecker thesaurusLangChecker;
    private final PolicyProvider policyProvider;
    private final ExternalSourceProvider externalSourceProvider;
    private final Predicate<Subset> subsetAccessPredicate;


    public ParseContext(FichothequeEditor fichothequeEditor, PolicyProvider policyProvider, ThesaurusLangChecker thesaurusLangChecker, ExternalSourceProvider externalSourceProvider, Predicate<Subset> subsetAccessPredicate) {
        this.fichothequeEditor = fichothequeEditor;
        this.fichotheque = fichothequeEditor.getFichotheque();
        this.policyProvider = policyProvider;
        this.thesaurusLangChecker = thesaurusLangChecker;
        this.externalSourceProvider = externalSourceProvider;
        this.subsetAccessPredicate = subsetAccessPredicate;
    }

    public Fichotheque getFichotheque() {
        return fichotheque;
    }

    public FichothequeEditor getFichothequeEditor() {
        return fichothequeEditor;
    }

    public PolicyProvider getPolicyProvider() {
        return policyProvider;
    }

    public ThesaurusLangChecker getThesaurusLangChecker() {
        return thesaurusLangChecker;
    }

    public ExternalSourceProvider getExternalSourceProvider() {
        return externalSourceProvider;
    }

    public Predicate<Subset> getSubsetAccessPredicate() {
        return subsetAccessPredicate;
    }

}
