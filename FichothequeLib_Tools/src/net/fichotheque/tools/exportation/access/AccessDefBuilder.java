/* FichothequeLib_Tools - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.access;

import net.fichotheque.exportation.access.AccessDef;
import net.fichotheque.selection.SelectionOptions;
import net.fichotheque.tools.selection.SelectionOptionsBuilder;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class AccessDefBuilder extends DefBuilder {

    private final String name;
    private final SelectionOptionsBuilder selectionOptionsBuilder = new SelectionOptionsBuilder();
    private boolean publicAccess = false;
    private String tableExportName = "";

    public AccessDefBuilder(String name) {
        this(name, null);
    }

    public AccessDefBuilder(String name, Attributes initAttributes) {
        super(initAttributes);
        if (!StringUtils.isTechnicalName(name, true)) {
            throw new IllegalArgumentException("Wrong syntax");
        }
        this.name = name;
    }

    public SelectionOptionsBuilder getSelectionOptionsBuilder() {
        return selectionOptionsBuilder;
    }

    public AccessDefBuilder setPublic(boolean publicAccess) {
        this.publicAccess = publicAccess;
        return this;
    }

    public AccessDefBuilder setTableExportName(String tableExportName) {
        this.tableExportName = StringUtils.nullTrim(tableExportName);
        return this;
    }

    public AccessDef toAccessDef() {
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        return new InternalAccessDef(name, publicAccess, tableExportName, selectionOptionsBuilder.toSelectionOptions(), titleLabels, attributes);
    }

    public static AccessDefBuilder init(String name) {
        return new AccessDefBuilder(name);
    }

    public static AccessDefBuilder init(String name, Attributes initAttributes) {
        return new AccessDefBuilder(name, initAttributes);
    }


    private static class InternalAccessDef implements AccessDef {

        private final String name;
        private final boolean publicAccess;
        private final String tableExportName;
        private final SelectionOptions selectionOptions;
        private final Labels titleLabels;
        private final Attributes attributes;


        private InternalAccessDef(String name, boolean publicAccess, String tableExportName, SelectionOptions selectionOptions, Labels titleLabels, Attributes attributes) {
            this.name = name;
            this.publicAccess = publicAccess;
            this.tableExportName = tableExportName;
            this.selectionOptions = selectionOptions;
            this.titleLabels = titleLabels;
            this.attributes = attributes;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public boolean isPublic() {
            return publicAccess;
        }

        @Override
        public String getTableExportName() {
            return tableExportName;
        }

        @Override
        public SelectionOptions getSelectionOptions() {
            return selectionOptions;
        }

    }

}
