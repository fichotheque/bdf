/* FichothequeLib_Tools - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.access.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.tools.exportation.access.AccessDefBuilder;
import net.fichotheque.tools.selection.SelectionDOMUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class AccessDefDOMReader {

    private final Fichotheque fichotheque;
    private final AccessDefBuilder accessDefBuilder;
    private final MessageHandler messageHandler;

    public AccessDefDOMReader(Fichotheque fichotheque, AccessDefBuilder accessDefBuilder, MessageHandler messageHandler) {
        this.fichotheque = fichotheque;
        this.accessDefBuilder = accessDefBuilder;
        this.messageHandler = messageHandler;
    }

    public AccessDefDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static AccessDefDOMReader init(Fichotheque fichotheque, AccessDefBuilder accessDefBuilder, MessageHandler messageHandler) {
        return new AccessDefDOMReader(fichotheque, accessDefBuilder, messageHandler);
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {

        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "public":
                    accessDefBuilder.setPublic(true);
                    break;
                case "tableexport-name":
                    accessDefBuilder.setTableExportName(DOMUtils.readSimpleElement(element));
                    break;
                case "label":
                case "lib":
                    try {
                    LabelUtils.readLabel(element, accessDefBuilder);
                } catch (ParseException ile) {
                    DomMessages.wrongLangAttribute(messageHandler, tagName, element.getAttribute("xml:lang"));
                }
                break;
                default:
                    if (!SelectionDOMUtils.readSelectionOptions(accessDefBuilder.getSelectionOptionsBuilder(), fichotheque, element)) {
                        DomMessages.unknownTagWarning(messageHandler, tagName);
                    }
            }
        }


    }

}
