/* FichothequeLib_Tools - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.exportation.table.TableExportContentDescription;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.utils.TableExportUtils;


/**
 *
 * @author Vincent Calame
 */
public class TableExportDescriptionBuilder {

    private final TableExportDef tableExportDef;
    private final List<TableExportContentDescription> contentDescriptionList = new ArrayList<TableExportContentDescription>();
    private String state = TableExportDescription.DISABLED_STATE;


    public TableExportDescriptionBuilder(TableExportDef tableExportDef) {
        this.tableExportDef = tableExportDef;
    }

    public TableExportDescriptionBuilder setState(String state) {
        this.state = state;
        return this;
    }

    public TableExportDescriptionBuilder addTableExportContentDescription(TableExportContentDescription tableExportContentDescription) {
        contentDescriptionList.add(tableExportContentDescription);
        return this;
    }

    public TableExportDescription toTableExportDescription() {
        List<TableExportContentDescription> finalList = TableExportUtils.wrap(contentDescriptionList.toArray(new TableExportContentDescription[contentDescriptionList.size()]));
        return new InternalTableExportDescription(tableExportDef, state, finalList);
    }

    public static TableExportDescriptionBuilder init(TableExportDef tableExportDef) {
        return new TableExportDescriptionBuilder(tableExportDef);
    }


    private static class InternalTableExportDescription implements TableExportDescription {

        private final TableExportDef tableExportDef;
        private final String state;
        private final List<TableExportContentDescription> list;

        private InternalTableExportDescription(TableExportDef tableExportDef, String state, List<TableExportContentDescription> list) {
            this.tableExportDef = tableExportDef;
            this.state = state;
            this.list = list;
        }

        @Override
        public TableExportDef getTableExportDef() {
            return tableExportDef;
        }

        @Override
        public String getState() {
            return state;
        }

        @Override
        public List<TableExportContentDescription> getTableExportContentDescriptionList() {
            return list;
        }

        @Override
        public boolean isEditable() {
            return true;
        }

    }

}
