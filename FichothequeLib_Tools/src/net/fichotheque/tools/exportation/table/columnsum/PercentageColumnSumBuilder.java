/* FichothequeLib_Tools - Copyright (c) 2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table.columnsum;

import java.math.BigDecimal;
import net.fichotheque.exportation.table.TableExportResult;
import net.mapeadores.util.primitives.Decimal;


/**
 *
 * @author Vincent Calame
 */
public class PercentageColumnSumBuilder extends ColumnSumBuilder {

    private BigDecimal result = new BigDecimal(0);

    public PercentageColumnSumBuilder() {

    }

    public void sum(Decimal decimal) {
        result = result.add(decimal.toBigDecimal());
    }

    @Override
    public TableExportResult.ColumnSum toColumnSum() {
        return new InternalPercentageColumnSum(Decimal.fromBigDecimal(result));
    }


    private static class InternalPercentageColumnSum implements TableExportResult.PercentageColumnSum {

        private final Decimal result;

        private InternalPercentageColumnSum(Decimal result) {
            this.result = result;
        }

        @Override
        public Decimal getResult() {
            return result;
        }

    }

}
