/* FichothequeLib_Tools - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table.columnsum;

import net.fichotheque.exportation.table.TableExportResult;


/**
 *
 * @author Vincent Calame
 */
public class IntegerColumnSumBuilder extends ColumnSumBuilder {

    private long sum = 0;

    public IntegerColumnSumBuilder() {

    }

    public void sum(long l) {
        sum = sum + l;
    }

    @Override
    public TableExportResult.ColumnSum toColumnSum() {
        return new InternalIntegerColumnSum(sum);
    }


    private static class InternalIntegerColumnSum implements TableExportResult.IntegerColumnSum {

        private final long result;

        private InternalIntegerColumnSum(long result) {
            this.result = result;
        }

        @Override
        public long getResult() {
            return result;
        }

    }

}
