/* FichothequeLib_Tools - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table.columnsum;

import net.fichotheque.exportation.table.ColDef;
import net.fichotheque.exportation.table.TableExportResult;
import net.fichotheque.utils.TableDefUtils;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.money.Amount;
import net.mapeadores.util.primitives.Decimal;


/**
 *
 * @author Vincent Calame
 */
public class ColumnSumEngine {

    private final boolean withColumnSum;
    private final ColumnSumBuilder[] builderArray;

    public ColumnSumEngine(ColDef[] colDefArray) {
        int count = colDefArray.length;
        boolean with = false;
        this.builderArray = new ColumnSumBuilder[count];
        for (int i = 0; i < count; i++) {
            ColDef colDef = colDefArray[i];
            if (TableDefUtils.isWithColumnSum(colDef)) {
                ColumnSumBuilder builder = initBuilder(colDef.getCastType());
                builderArray[i] = builder;
                if (builder != null) {
                    with = true;
                }
            }
        }
        this.withColumnSum = with;
    }

    private ColumnSumBuilder initBuilder(short castType) {
        switch (castType) {
            case FormatConstants.INTEGER_CAST:
                return new IntegerColumnSumBuilder();
            case FormatConstants.DECIMAL_CAST:
                return new DecimalColumnSumBuilder();
            case FormatConstants.PERCENTAGE_CAST:
                return new PercentageColumnSumBuilder();
            case FormatConstants.MONEY_CAST:
                return new MoneyColumnSumBuilder();
            default:
                return null;
        }
    }

    public boolean hasColumnSum() {
        return withColumnSum;
    }

    public TableExportResult.ColumnSum[] getResult() {
        if (withColumnSum) {
            int count = builderArray.length;
            TableExportResult.ColumnSum[] result = new TableExportResult.ColumnSum[count];
            for (int i = 0; i < count; i++) {
                ColumnSumBuilder builder = builderArray[i];
                if (builder != null) {
                    result[i] = builder.toColumnSum();
                } else {
                    result[i] = null;
                }
            }
            return result;
        } else {
            return null;
        }
    }

    public void sumInteger(int colIndex, long lg) {
        ColumnSumBuilder builder = builderArray[colIndex];
        if (builder != null) {
            ((IntegerColumnSumBuilder) builder).sum(lg);
        }
    }

    public void sumDecimal(int colIndex, Decimal decimal) {
        ColumnSumBuilder builder = builderArray[colIndex];
        if (builder != null) {
            ((DecimalColumnSumBuilder) builder).sum(decimal);
        }
    }

    public void sumPercentage(int colIndex, Decimal decimal) {
        ColumnSumBuilder builder = builderArray[colIndex];
        if (builder != null) {
            ((PercentageColumnSumBuilder) builder).sum(decimal);
        }
    }

    public void sumMoney(int colIndex, Amount amount) {
        ColumnSumBuilder builder = builderArray[colIndex];
        if (builder != null) {
            ((MoneyColumnSumBuilder) builder).sum(amount);
        }
    }


}
