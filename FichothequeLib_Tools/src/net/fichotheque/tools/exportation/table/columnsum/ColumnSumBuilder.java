/* FichothequeLib_Tools - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table.columnsum;

import net.fichotheque.exportation.table.TableExportResult;


/**
 *
 * @author Vincent Calame
 */
public abstract class ColumnSumBuilder {

    public abstract TableExportResult.ColumnSum toColumnSum();

}
