/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.util.function.Predicate;
import net.fichotheque.SubsetItem;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.TableExportConstants;
import net.fichotheque.exportation.table.TableExportContext;
import net.fichotheque.utils.TableExportUtils;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class TableExportOdsParameters {

    private final Lang lang;
    private final TableExportContext tableExportContext;
    private CellEngine cellEngine = TableExportUtils.EMPTY_CELLENGINE;
    private String headerType = TableExportConstants.COLUMNKEY_HEADER;
    private boolean withThesaurusTable = false;
    private Predicate<SubsetItem> globalPredicate = null;


    public TableExportOdsParameters(TableExportContext tableExportContext, Lang lang) {
        this.tableExportContext = tableExportContext;
        this.lang = lang;
    }

    public Lang getWorkingLang() {
        return lang;
    }

    public CellEngine getCellEngine() {
        return cellEngine;
    }

    public TableExportContext getTableExportContext() {
        return tableExportContext;
    }

    public String getHeaderType() {
        return headerType;
    }

    public boolean isWithThesaurusTable() {
        return withThesaurusTable;
    }

    public Predicate<SubsetItem> getGlobalPredicate() {
        return globalPredicate;
    }


    public TableExportOdsParameters setWithThesaurusTable(boolean withThesaurusTable) {
        this.withThesaurusTable = withThesaurusTable;
        return this;
    }

    public TableExportOdsParameters setHeaderType(String headerType) {
        this.headerType = headerType;
        return this;
    }

    public TableExportOdsParameters setCellEngine(CellEngine cellEngine) {
        this.cellEngine = cellEngine;
        return this;
    }

    public TableExportOdsParameters setGlobalPredicate(Predicate<SubsetItem> globalPredicate) {
        this.globalPredicate = globalPredicate;
        return this;
    }

    public static TableExportOdsParameters init(TableExportContext tableExportContext, Lang lang) {
        return new TableExportOdsParameters(tableExportContext, lang);
    }

}
