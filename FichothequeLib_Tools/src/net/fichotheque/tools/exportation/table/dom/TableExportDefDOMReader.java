/* FichothequeLib_Tools - Copyright (c) 2016-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table.dom;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import net.fichotheque.tools.exportation.table.TableExportDefBuilder;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class TableExportDefDOMReader {

    private final TableExportDefBuilder tableExportDefBuilder;
    private final MessageHandler messageHandler;

    public TableExportDefDOMReader(TableExportDefBuilder tableExportDefBuilder, MessageHandler messageHandler) {
        this.tableExportDefBuilder = tableExportDefBuilder;
        this.messageHandler = messageHandler;
    }

    public TableExportDefDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static TableExportDefDOMReader init(TableExportDefBuilder tableExportDefBuilder, MessageHandler messageHandler) {
        return new TableExportDefDOMReader(tableExportDefBuilder, messageHandler);
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "label": {
                    try {
                        LabelUtils.readLabel(element, tableExportDefBuilder);
                    } catch (ParseException ile) {
                        DomMessages.wrongLangAttribute(messageHandler, tagName, element.getAttribute("xml:lang"));
                    }
                    break;
                }
                case "langs": {
                    String langMode = element.getAttribute("mode");
                    if (langMode.length() > 0) {
                        tableExportDefBuilder.setLangMode(langMode);
                    }
                    List<Lang> resultList = new ArrayList<Lang>();
                    LangsUtils.readLangElements(resultList, element, messageHandler, tagName);
                    for (Lang lang : resultList) {
                        tableExportDefBuilder.addLang(lang);
                    }
                    break;
                }
                case "attr":
                    AttributeUtils.readAttrElement(tableExportDefBuilder.getAttributesBuilder(), element);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }

}
