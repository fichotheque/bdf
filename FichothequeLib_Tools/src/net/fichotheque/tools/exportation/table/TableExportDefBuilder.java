/* FichothequeLib_Tools - Copyright (c) 2016-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.exportation.table.TableExportDef;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class TableExportDefBuilder extends DefBuilder {

    private final String name;
    private String langMode;
    private final Set<Lang> langSet = new LinkedHashSet();

    public TableExportDefBuilder(String name) {
        this(name, null);
    }

    public TableExportDefBuilder(String name, @Nullable Attributes initAttributes) {
        super(initAttributes);
        if (!StringUtils.isTechnicalName(name, true)) {
            throw new IllegalArgumentException("Wrong syntax");
        }
        this.name = name;
    }

    public TableExportDefBuilder setLangMode(@Nullable String langMode) {
        this.langMode = langMode;
        return this;
    }

    public TableExportDefBuilder addLang(Lang lang) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        langSet.add(lang);
        return this;
    }

    public TableExportDef toTableExportDef() {
        Langs langs;
        if (langSet.size() > 0) {
            langs = LangsUtils.fromCollection(langSet);
        } else {
            langs = LangsUtils.EMPTY_LANGS;
        }
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        return new InternalTableExportDef(name, langMode, langs, titleLabels, attributes);
    }

    public static TableExportDefBuilder init(String name, @Nullable Attributes initAttributes) {
        return new TableExportDefBuilder(name, initAttributes);
    }


    private static class InternalTableExportDef implements TableExportDef {

        private final String name;
        private final String langMode;
        private final Langs langs;
        private final Labels titleLabels;
        private final Attributes attributes;

        private InternalTableExportDef(String name, String langMode, Langs langs,
                Labels titleLabels, Attributes attributes) {
            this.name = name;
            this.langMode = langMode;
            this.langs = langs;
            this.titleLabels = titleLabels;
            this.attributes = attributes;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public String getLangMode() {
            return langMode;
        }

        @Override
        public Langs getLangs() {
            return langs;
        }

    }

}
