/* FichothequeLib_Tools - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.util.Collection;
import java.util.function.Predicate;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.exportation.table.CellEngine;
import net.fichotheque.exportation.table.ColDef;
import net.fichotheque.exportation.table.CroisementTable;
import net.fichotheque.exportation.table.CroisementTableWriter;
import net.fichotheque.exportation.table.SubsetTable;
import net.fichotheque.exportation.table.TableExportResult;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.money.Amount;
import net.mapeadores.util.table.TableWriter;


/**
 *
 * @author Vincent Calame
 */
public final class TableExportEngine {

    private TableExportEngine() {
    }

    public static TableExportResult exportSubset(SubsetTable subsetTable, TableWriter tableWriter, CellEngine cellEngine, Collection<SubsetItem> subsetItems) {
        return exportSubset(subsetTable, tableWriter, cellEngine, subsetItems, null);
    }

    public static TableExportResult exportSubset(SubsetTable subsetTable, TableWriter tableWriter, CellEngine cellEngine, Collection<SubsetItem> subsetItems, @Nullable Predicate<SubsetItem> predicate) {
        if (subsetItems == null) {
            subsetItems = subsetTable.getSubset().getSubsetItemList();
        }
        for (SubsetItem subsetItem : subsetItems) {
            Cell[] cellArray = cellEngine.toCellArray(subsetItem, predicate);
            if (cellArray != null) {
                tableWriter.startRow();
                for (Cell cell : cellArray) {
                    ColUtils.writeCell(tableWriter, cell);
                }
                tableWriter.endRow();
            }
        }
        return cellEngine.getTableExportResult(subsetTable.getSubset().getSubsetKey());
    }

    public static void exportCroisement(CroisementTable croisementTable, CroisementTableWriter croisementTableWriter) {
        Subset subset = croisementTable.getSubset1();
        Subset otherSubset = croisementTable.getSubset2();
        Fichotheque fichotheque = subset.getFichotheque();
        for (SubsetItem subsetItem : subset.getSubsetItemList()) {
            Croisements croisements = fichotheque.getCroisements(subsetItem, otherSubset);
            for (Croisements.Entry entry : croisements.getEntryList()) {
                SubsetItem otherItem = entry.getSubsetItem();
                croisementTableWriter.startRow();
                croisementTableWriter.addIntegerCell(subsetItem.getId());
                croisementTableWriter.addIntegerCell(otherItem.getId());
                croisementTableWriter.endRow();
            }
        }
    }

    public static int writeSumRows(TableWriter tableWriter, TableExportResult tableExportResult, int startRowNumber, NamedRangeHandler namedRangeHandler) {
        if (!tableExportResult.hasColumnSum()) {
            return 0;
        }
        int rowCount = 0;
        int colCount = tableExportResult.getColCount();
        short[] castTypeArray = new short[colCount];
        tableWriter.startRow();
        rowCount++;
        for (int i = 0; i < colCount; i++) {
            ColDef colDef = tableExportResult.getColDef(i);
            short castType = colDef.getCastType();
            ColUtils.writeNullCell(tableWriter, castType);
            castTypeArray[i] = castType;
        }
        tableWriter.endRow();
        startRowNumber++;
        int moneyMaxCount = 0;
        tableWriter.startRow();
        rowCount++;
        for (int i = 0; i < colCount; i++) {
            ColDef colDef = tableExportResult.getColDef(i);
            TableExportResult.ColumnSum columnSum = tableExportResult.getColumnSum(i);
            short castType = castTypeArray[i];
            if (columnSum == null) {
                ColUtils.writeNullCell(tableWriter, castType);
            } else {
                if (columnSum instanceof TableExportResult.IntegerColumnSum) {
                    tableWriter.addIntegerCell(((TableExportResult.IntegerColumnSum) columnSum).getResult());
                    namedRangeHandler.addColumnSum(startRowNumber, i, colDef);
                } else if (columnSum instanceof TableExportResult.DecimalColumnSum) {
                    tableWriter.addDecimalCell(((TableExportResult.DecimalColumnSum) columnSum).getResult());
                    namedRangeHandler.addColumnSum(startRowNumber, i, colDef);
                } else if (columnSum instanceof TableExportResult.PercentageColumnSum) {
                    tableWriter.addPercentageCell(((TableExportResult.PercentageColumnSum) columnSum).getResult());
                    namedRangeHandler.addColumnSum(startRowNumber, i, colDef);
                } else if (columnSum instanceof TableExportResult.MoneyColumnSum) {
                    TableExportResult.MoneyColumnSum moneyColumnSum = (TableExportResult.MoneyColumnSum) columnSum;
                    int count = moneyColumnSum.getResultCount();
                    moneyMaxCount = Math.max(count, moneyMaxCount);
                    Amount amount = moneyColumnSum.getResult(0);
                    tableWriter.addMoneyCell(amount);
                    if (count == 1) {
                        namedRangeHandler.addColumnSum(startRowNumber, i, colDef);
                    }
                } else {
                    ColUtils.writeNullCell(tableWriter, castType);
                }
            }
        }
        tableWriter.endRow();
        if (moneyMaxCount > 1) {
            tableWriter.startRow();
            rowCount++;
            for (int j = 1; j < moneyMaxCount; j++) {
                for (int i = 0; i < colCount; i++) {
                    TableExportResult.ColumnSum columnSum = tableExportResult.getColumnSum(i);
                    short castType = castTypeArray[i];
                    if ((columnSum != null) && (columnSum instanceof TableExportResult.MoneyColumnSum)) {
                        TableExportResult.MoneyColumnSum moneyColumnSum = (TableExportResult.MoneyColumnSum) columnSum;
                        int count = moneyColumnSum.getResultCount();
                        if (j < count) {
                            Amount result = moneyColumnSum.getResult(j);
                            tableWriter.addMoneyCell(result);
                        } else {
                            tableWriter.addMoneyCell(null);
                        }
                    } else {
                        ColUtils.writeNullCell(tableWriter, castType);
                    }
                }
            }
            tableWriter.endRow();
        }
        return rowCount;
    }

}
