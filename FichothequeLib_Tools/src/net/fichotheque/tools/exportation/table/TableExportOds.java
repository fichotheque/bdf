/* FichothequeLib_Tools - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.opendocument.io.OdSource;
import net.mapeadores.opendocument.io.OdUtils;
import net.mapeadores.opendocument.io.OdZip;
import net.mapeadores.opendocument.io.OdZipEngine;
import net.mapeadores.opendocument.io.odtable.OdTableDef;
import net.mapeadores.opendocument.io.odtable.OdsOptions;
import net.mapeadores.opendocument.io.odtable.TableSettings;


/**
 *
 * @author Vincent Calame
 */
public class TableExportOds {

    private final List<OdTableDef> tableDefList;
    private final OdSource contentOdSource;
    private final OdsOptions odsOptions;

    TableExportOds(List<OdTableDef> tableDefList, OdSource contentOdSource, OdsOptions odsOptions) {
        this.tableDefList = tableDefList;
        this.contentOdSource = contentOdSource;
        this.odsOptions = odsOptions;
    }

    public List<OdTableDef> getTableDefList() {
        return tableDefList;
    }

    public OdSource getContentOdSource() {
        return contentOdSource;
    }

    public void write(OutputStream outputStream, boolean fixFirstRow) throws IOException {
        OdSource settingsOdsSource;
        if (fixFirstRow) {
            List<TableSettings> settingsList = new ArrayList<TableSettings>();
            for (OdTableDef tableDef : tableDefList) {
                settingsList.add(TableSettings.init(tableDef.getTableName())
                        .fixedRows(1));
            }
            settingsOdsSource = OdUtils.getSettingsOdSource(settingsList);
        } else {
            settingsOdsSource = null;
        }
        OdZipEngine.run(outputStream, OdZip.spreadSheet()
                .stylesOdSource(OdUtils.toStyleOdSource(odsOptions.elementMaps(), true))
                .contentOdSource(contentOdSource)
                .settingsOdSource(settingsOdsSource)
        );
    }

}
