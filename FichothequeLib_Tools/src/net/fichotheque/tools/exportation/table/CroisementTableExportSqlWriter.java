/* FichothequeLib_Tools - Copyright (c) 2008-2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.io.IOException;
import java.io.Writer;
import net.fichotheque.exportation.table.CroisementTableWriter;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.sql.InsertWriter;


/**
 *
 * @author Vincent Calame
 */
public class CroisementTableExportSqlWriter implements CroisementTableWriter {

    protected InsertWriter insertWriter;
    protected Writer writer;
    protected String tableName;

    public CroisementTableExportSqlWriter(Writer writer, String tableName) {
        insertWriter = new InsertWriter(writer);
        this.tableName = tableName;
        this.writer = writer;
    }

    public void startRow() {
        try {
            insertWriter.startInsert(tableName);
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
    }

    public void addIntegerCell(int i) {
        try {
            insertWriter.addInteger(i);
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
    }

    public void endRow() {
        try {
            insertWriter.endInsertLine();
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
    }

}
