/* FichothequeLib_Tools - Copyright (c) 2017-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;
import net.fichotheque.exportation.table.Cell;
import net.fichotheque.exportation.table.Col;
import net.fichotheque.exportation.table.ColDef;
import net.fichotheque.exportation.table.TableExportResult;
import net.fichotheque.format.formatters.SourceFormatter;
import net.mapeadores.util.format.Calcul;
import net.mapeadores.util.format.FormatConstants;
import net.mapeadores.util.money.Amount;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.table.TableWriter;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class ColUtils {

    public final static Col[] EMPTY_COLARRAY = new Col[0];

    private ColUtils() {

    }

    public static Col toCol(ColDef colDef, SourceFormatter sourceFormatter) {
        return new InternalCol(colDef, sourceFormatter);
    }

    public static int writeNullCell(TableWriter tableWriter, short castType) {
        switch (castType) {
            case FormatConstants.INTEGER_CAST:
                return tableWriter.addIntegerCell(null);
            case FormatConstants.DECIMAL_CAST:
                return tableWriter.addDecimalCell(null);
            case FormatConstants.DATE_CAST:
                return tableWriter.addDateCell(null);
            case FormatConstants.MONEY_CAST:
                return tableWriter.addMoneyCell(null);
            case FormatConstants.PERCENTAGE_CAST:
                return tableWriter.addPercentageCell(null);
            default:
                return tableWriter.addStringCell(null);
        }
    }

    public static int writeCell(TableWriter tableWriter, short castType, Object obj) {
        if (obj == null) {
            return writeNullCell(tableWriter, castType);
        } else {
            switch (castType) {
                case FormatConstants.INTEGER_CAST:
                    return tableWriter.addIntegerCell((Long) obj);
                case FormatConstants.DECIMAL_CAST:
                    return tableWriter.addDecimalCell((Decimal) obj);
                case FormatConstants.DATE_CAST:
                    return tableWriter.addDateCell((FuzzyDate) obj);
                case FormatConstants.MONEY_CAST:
                    return tableWriter.addMoneyCell((Amount) obj);
                case FormatConstants.PERCENTAGE_CAST:
                    return tableWriter.addPercentageCell((Decimal) obj);
                default:
                    return tableWriter.addStringCell((String) obj);
            }
        }
    }

    public static int writeCell(TableWriter tableWriter, Cell cell) {
        return writeCell(tableWriter, cell.getFormatCast(), cell.getValue());
    }

    public static Decimal getDecimal(String value, Calcul calcul) {
        Decimal decimal = StringUtils.parseDecimal(value);
        if (calcul != null) {
            double d = decimal.toDouble();
            d = calcul.execute(d);
            decimal = new Decimal(d);
        }
        return decimal;
    }

    public static TableExportResult toTableExportResult(ColDef[] colDefArray, TableExportResult.ColumnSum[] colSumArray) {
        return new InternalTableExportResult(colDefArray, colSumArray);
    }


    private static class InternalTableExportResult implements TableExportResult {

        private final ColDef[] colDefArray;
        private final TableExportResult.ColumnSum[] colSumArray;

        private InternalTableExportResult(ColDef[] colDefArray, TableExportResult.ColumnSum[] colSumArray) {
            this.colDefArray = colDefArray;
            this.colSumArray = colSumArray;
        }

        @Override
        public boolean hasColumnSum() {
            return (colSumArray != null);
        }

        @Override
        public int getColCount() {
            return colDefArray.length;
        }

        @Override
        public ColDef getColDef(int i) {
            return colDefArray[i];
        }

        @Override
        public TableExportResult.ColumnSum getColumnSum(int i) {
            if (colSumArray == null) {
                return null;
            } else {
                return colSumArray[i];
            }
        }

    }


    private static class InternalCol implements Col {

        private final ColDef colDef;
        private final SourceFormatter sourceFormatter;

        private InternalCol(ColDef colDef, SourceFormatter sourceFormatter) {
            this.colDef = colDef;
            this.sourceFormatter = sourceFormatter;
        }

        @Override
        public ColDef getColDef() {
            return colDef;
        }

        @Override
        public SourceFormatter getSourceFormatter() {
            return sourceFormatter;
        }

    }

    public static List<Col> wrap(Col[] array) {
        return new ColList(array);
    }


    private static class ColList extends AbstractList<Col> implements RandomAccess {

        private final Col[] array;

        private ColList(Col[] colDefArray) {
            this.array = colDefArray;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public Col get(int i) {
            return array[i];
        }

    }

}
