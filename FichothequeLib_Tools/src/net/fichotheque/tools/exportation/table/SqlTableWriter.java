/* FichothequeLib_Tools - Copyright (c) 2007-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.io.IOException;
import java.io.Writer;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.money.Amount;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.sql.InsertWriter;
import net.mapeadores.util.table.TableWriter;


/**
 *
 * @author Vincent Calame
 */
public class SqlTableWriter implements TableWriter {

    protected InsertWriter insertWriter;
    protected Writer writer;
    protected String tableName;
    private boolean notNull;
    private int rowNumber;
    private int columnNumber;

    public SqlTableWriter(Writer writer, String tableName) {
        insertWriter = new InsertWriter(writer);
        this.tableName = tableName;
        this.writer = writer;
        this.rowNumber = 0;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    @Override
    public int startRow() {
        rowNumber++;
        try {
            insertWriter.startInsert(tableName);
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        columnNumber = 0;
        return rowNumber;
    }

    @Override
    public int addIntegerCell(Long lg) {
        columnNumber++;
        try {
            if (lg == null) {
                if (notNull) {
                    insertWriter.addInteger(0);
                } else {
                    insertWriter.addNull();
                }
            } else {
                insertWriter.addInteger(lg);
            }
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        return columnNumber;
    }

    @Override
    public int addDecimalCell(Decimal decimal) {
        columnNumber++;
        try {
            if (decimal == null) {
                if (notNull) {
                    insertWriter.addDecimal(new Decimal(0, (byte) 0, 0));
                } else {
                    insertWriter.addNull();
                }
            } else {
                insertWriter.addDecimal(decimal);
            }
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        return columnNumber;
    }

    @Override
    public int addStringCell(String s) {
        columnNumber++;
        try {
            if (s == null) {
                if (notNull) {
                    insertWriter.addText("");
                } else {
                    insertWriter.addNull();
                }
            } else {
                insertWriter.addText(s);
            }
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        return columnNumber;
    }

    @Override
    public int addDateCell(FuzzyDate date) {
        columnNumber++;
        try {
            if (date == null) {
                if (notNull) {
                    date = FuzzyDate.current();
                } else {
                    insertWriter.addNull();
                    return columnNumber;
                }
            }
            insertWriter.addText(date.toISOString());
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        return columnNumber;
    }

    @Override
    public int addMoneyCell(Amount amount) {
        columnNumber++;
        try {
            if (amount == null) {
                if (notNull) {
                    insertWriter.addText("");
                } else {
                    insertWriter.addNull();
                }
            } else {
                insertWriter.addText(amount.toString());
            }
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        return columnNumber;
    }

    @Override
    public int addPercentageCell(Decimal decimal) {
        return addDecimalCell(decimal);
    }

    @Override
    public int endRow() {
        try {
            insertWriter.endInsertLine();
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        return rowNumber;
    }

}
