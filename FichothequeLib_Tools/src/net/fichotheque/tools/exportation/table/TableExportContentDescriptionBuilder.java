/* FichothequeLib_Tools - Copyright (c) 2019-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.exportation.table.TableDef;
import net.fichotheque.exportation.table.TableExportContentDescription;
import net.mapeadores.util.logging.LineMessageHandler;
import net.mapeadores.util.logging.MessageByLine;
import net.mapeadores.util.logging.SimpleLineMessageHandler;


/**
 *
 * @author Vincent Calame
 */
public class TableExportContentDescriptionBuilder {

    private final SimpleLineMessageHandler lineMessageHandler = new SimpleLineMessageHandler();
    private final String path;
    private final String tableExportName;
    private String state = TableExportContentDescription.UNKNOWN_NAME_STATE;
    private SubsetKey subsetKey;
    private TableDef tableDef;
    private boolean editable = true;

    public TableExportContentDescriptionBuilder(String path, String tableExportName) {
        this.path = path;
        this.tableExportName = tableExportName;
    }

    public LineMessageHandler getLineMessageHandler() {
        return lineMessageHandler;
    }

    public boolean hasMessage() {
        return lineMessageHandler.hasMessage();
    }

    public TableExportContentDescriptionBuilder setState(String state) {
        this.state = state;
        return this;
    }

    public TableExportContentDescriptionBuilder setSubsetKey(SubsetKey subsetKey) {
        this.subsetKey = subsetKey;
        return this;
    }

    public TableExportContentDescriptionBuilder setTableDef(TableDef tableDef) {
        this.tableDef = tableDef;
        return this;
    }

    public TableExportContentDescriptionBuilder setEditable(boolean editable) {
        this.editable = editable;
        return this;
    }

    public TableExportContentDescription toTableExportContentDescription() {
        return new InternalTableExportContentDescription(path, tableExportName, editable, state, subsetKey, tableDef, lineMessageHandler.toMessageByLineList());
    }

    public static TableExportContentDescriptionBuilder init(String path, String tableExportName) {
        return new TableExportContentDescriptionBuilder(path, tableExportName);
    }


    private static class InternalTableExportContentDescription implements TableExportContentDescription {

        private final String path;
        private final String tableExportName;
        private final boolean editable;
        private final String state;
        private final TableDef tableDef;
        private final SubsetKey subsetKey;
        private final List<MessageByLine> messageByLineList;

        private InternalTableExportContentDescription(String path, String tableExportName, boolean editable, String state, SubsetKey subsetKey, TableDef tableDef, List<MessageByLine> messageByLineList) {
            this.path = path;
            this.tableExportName = tableExportName;
            this.editable = editable;
            this.state = state;
            this.tableDef = tableDef;
            this.subsetKey = subsetKey;
            this.messageByLineList = messageByLineList;
        }

        @Override
        public String getPath() {
            return path;
        }

        @Override
        public String getTableExportName() {
            return tableExportName;
        }

        @Override
        public String getState() {
            return state;
        }

        @Override
        public SubsetKey getSubsetKey() {
            return subsetKey;
        }

        @Override
        public TableDef getTableDef() {
            return tableDef;
        }

        @Override
        public List<MessageByLine> getMessageByLineList() {
            return messageByLineList;
        }

        @Override
        public boolean isEditable() {
            return editable;
        }

    }

}
