/* FichothequeLib_Tools - Copyright (c) 2012-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.table;

import net.fichotheque.Subset;
import net.fichotheque.exportation.table.CroisementTable;


/**
 *
 * @author Vincent Calame
 */
public class CroisementTableBuilder {

    private final Subset subset1;
    private final Subset subset2;
    private String tableName;

    public CroisementTableBuilder(Subset subset1, Subset subset2) {
        this.subset1 = subset1;
        this.subset2 = subset2;
    }

    public CroisementTableBuilder setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public CroisementTable toCroisementTable() {
        String finalTableName = tableName;
        if ((finalTableName == null) || (finalTableName.isEmpty())) {
            finalTableName = subset1.getSubsetKeyString() + "_" + subset2.getSubsetKeyString();
        }
        return new InternalCroisementTable(subset1, subset2, finalTableName);
    }

    public static CroisementTableBuilder init(Subset subset1, Subset subset2) {
        return new CroisementTableBuilder(subset1, subset2);
    }


    private static class InternalCroisementTable implements CroisementTable {

        private final Subset subset1;
        private final Subset subset2;
        private final String tableName;

        private InternalCroisementTable(Subset subset1, Subset subset2, String tableName) {
            this.subset1 = subset1;
            this.subset2 = subset2;
            this.tableName = tableName;
        }

        @Override
        public Subset getSubset1() {
            return subset1;
        }

        @Override
        public Subset getSubset2() {
            return subset2;
        }

        @Override
        public String getTableName() {
            return tableName;
        }

    }

}
