/* FichothequeLib_Tools - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.scrutari;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeUtils;


/**
 *
 * @author Vincent Calame
 */
class ExportInclude {

    private final boolean withAttributeFilter;
    private final Predicate<AttributeKey> attributePredicate;
    private final List<String> hookList;

    private ExportInclude(Predicate<AttributeKey> attributePredicate, List<String> hookList) {
        this.withAttributeFilter = (attributePredicate != null);
        this.attributePredicate = attributePredicate;
        this.hookList = hookList;
    }

    boolean isWithAttributeFilter() {
        return withAttributeFilter;
    }

    Predicate<AttributeKey> getAttributeFilter() {
        return attributePredicate;
    }

    List<String> getHookList() {
        return hookList;
    }

    static ExportInclude parse(List<String> tokenList) {
        List<String> filterList = new ArrayList<String>();
        List<String> hookList = new ArrayList<String>();
        for (String token : tokenList) {
            if (token.startsWith("%")) {
                token = token.substring(1);
                if (token.length() > 0) {
                    hookList.add(token);
                }
            } else {
                filterList.add(token);
            }
        }
        Predicate<AttributeKey> attributePredicate;
        if (!filterList.isEmpty()) {
            attributePredicate = AttributeUtils.toAttributeKeyPredicate(filterList);
        } else {
            attributePredicate = null;
        }
        return new ExportInclude(attributePredicate, hookList);
    }

}
