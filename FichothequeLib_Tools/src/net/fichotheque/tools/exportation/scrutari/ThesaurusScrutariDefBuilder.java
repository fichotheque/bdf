/* FichothequeLib_Tools - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.scrutari;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.fichotheque.exportation.scrutari.ThesaurusScrutariDef;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusScrutariDefBuilder {

    private final SubsetKey thesaurusKey;
    private final Set<String> includeTokenSet = new LinkedHashSet<String>();
    private String fieldGenerationSource = "";
    private boolean wholeThesaurus = false;


    public ThesaurusScrutariDefBuilder(SubsetKey thesaurusKey) {
        this.thesaurusKey = thesaurusKey;
    }

    public ThesaurusScrutariDefBuilder setFieldGenerationSource(String source) {
        if (source == null) {
            source = "";
        }
        this.fieldGenerationSource = source;
        return this;
    }

    public ThesaurusScrutariDefBuilder setWholeThesaurus(boolean wholeThesaurus) {
        this.wholeThesaurus = wholeThesaurus;
        return this;
    }

    public ThesaurusScrutariDefBuilder addIncludeToken(String includeToken) {
        if ((includeToken != null) && (includeToken.length() > 0)) {
            includeTokenSet.add(includeToken);
        }
        return this;
    }

    public ThesaurusScrutariDefBuilder parseIncludeTokens(String includeTokens) {
        if (includeTokens != null) {
            String[] tokens = StringUtils.getTechnicalTokens(includeTokens, true);
            for (String token : tokens) {
                includeTokenSet.add(token);
            }
        }
        return this;
    }

    public ThesaurusScrutariDef toThesaurusScrutariDef() {
        List<String> attributeFilterList = StringUtils.toList(includeTokenSet);
        return new InternalThesaurusScrutariDef(thesaurusKey, fieldGenerationSource, wholeThesaurus, attributeFilterList);
    }

    public static ThesaurusScrutariDefBuilder init(SubsetKey thesaurusKey) {
        return new ThesaurusScrutariDefBuilder(thesaurusKey);
    }


    private static class InternalThesaurusScrutariDef implements ThesaurusScrutariDef {

        private final SubsetKey thesaurusKey;
        private final String fieldGenerationSource;
        private final boolean wholeThesaurus;
        private final List<String> includeTokenList;

        public InternalThesaurusScrutariDef(SubsetKey thesaurusKey, String fieldGenerationSource, boolean wholeThesaurus, List<String> includeTokenList) {
            this.thesaurusKey = thesaurusKey;
            this.fieldGenerationSource = fieldGenerationSource;
            this.wholeThesaurus = wholeThesaurus;
            this.includeTokenList = includeTokenList;
        }

        @Override
        public SubsetKey getThesaurusKey() {
            return thesaurusKey;
        }

        @Override
        public String getFieldGenerationSource() {
            return fieldGenerationSource;
        }

        @Override
        public boolean isWholeThesaurus() {
            return wholeThesaurus;
        }

        @Override
        public List<String> getIncludeTokenList() {
            return includeTokenList;
        }

    }

}
