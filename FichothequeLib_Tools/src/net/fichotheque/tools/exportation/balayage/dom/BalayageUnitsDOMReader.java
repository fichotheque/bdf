/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.balayage.dom;

import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.tools.exportation.balayage.BalayageDefBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class BalayageUnitsDOMReader {

    private final Fichotheque fichotheque;
    private final BalayageDefBuilder balayageDefBuilder;
    private final MessageHandler messageHandler;

    public BalayageUnitsDOMReader(Fichotheque fichotheque, BalayageDefBuilder balayageDefBuilder, MessageHandler messageHandler) {
        this.fichotheque = fichotheque;
        this.balayageDefBuilder = balayageDefBuilder;
        this.messageHandler = messageHandler;
    }

    public BalayageUnitsDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static BalayageUnitsDOMReader init(Fichotheque fichotheque, BalayageDefBuilder balayageDefBuilder, MessageHandler messageHandler) {
        return new BalayageUnitsDOMReader(fichotheque, balayageDefBuilder, messageHandler);
    }


    private class RootConsumer implements Consumer<Element> {

        private final BalayageUnitDOMReader balayageUnitDOMReader;

        private RootConsumer() {
            this.balayageUnitDOMReader = new BalayageUnitDOMReader(fichotheque, balayageDefBuilder, messageHandler);
        }

        @Override
        public void accept(Element element) {
            balayageUnitDOMReader.read(element);
        }

    }

}
