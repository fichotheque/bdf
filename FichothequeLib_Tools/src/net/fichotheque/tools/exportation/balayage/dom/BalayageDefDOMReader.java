/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.balayage.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.tools.exportation.balayage.BalayageDefBuilder;
import net.fichotheque.tools.exportation.balayage.BalayagePostscriptumBuilder;
import net.fichotheque.tools.selection.SelectionDOMUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class BalayageDefDOMReader {

    private final Fichotheque fichotheque;
    private final BalayageDefBuilder balayageDefBuilder;
    private final MessageHandler messageHandler;

    public BalayageDefDOMReader(Fichotheque fichotheque, BalayageDefBuilder balayageDefBuilder, MessageHandler messageHandler) {
        this.fichotheque = fichotheque;
        this.balayageDefBuilder = balayageDefBuilder;
        this.messageHandler = messageHandler;
    }

    public BalayageDefDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static BalayageDefDOMReader init(Fichotheque fichotheque, BalayageDefBuilder balayageDefBuilder, MessageHandler messageHandler) {
        return new BalayageDefDOMReader(fichotheque, balayageDefBuilder, messageHandler);
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "label": {
                    try {
                        LabelUtils.readLabel(element, balayageDefBuilder);
                    } catch (ParseException ile) {
                        DomMessages.wrongLangAttribute(messageHandler, tagName, element.getAttribute("xml:lang"));
                    }
                    break;
                }
                case "langs": {
                    DOMUtils.readChildren(element, new LangsConsumer());
                    break;
                }
                case "target-name": {
                    balayageDefBuilder.setTargetName(DOMUtils.readSimpleElement(element));
                    break;
                }
                case "target-path": {
                    String targetPath = DOMUtils.readSimpleElement(element);
                    try {
                        RelativePath relativePath = RelativePath.parse(targetPath);
                        balayageDefBuilder.setTargetPath(relativePath);
                    } catch (ParseException pe) {
                        DomMessages.wrongElementValue(messageHandler, tagName, targetPath);
                    }
                    break;
                }
                case "default-lang-option": {
                    try {
                        balayageDefBuilder.setDefaultLangOption(DOMUtils.readSimpleElement(element));
                    } catch (IllegalArgumentException iae) {

                    }
                    break;
                }
                case "ignore-transformation": {
                    balayageDefBuilder.setIgnoreTransformation(true);
                    break;
                }
                case "postscriptum": {
                    DOMUtils.readChildren(element, new PostscriptumConsumer(balayageDefBuilder.getBalayagePostscriptumBuilder()));
                    break;
                }
                case "units": {
                    BalayageUnitsDOMReader.init(fichotheque, balayageDefBuilder, messageHandler)
                            .read(element);
                    break;
                }
                case "attr": {
                    AttributeUtils.readAttrElement(balayageDefBuilder.getAttributesBuilder(), element);
                    break;
                }

                default:
                    if (!SelectionDOMUtils.readSelectionOptions(balayageDefBuilder.getSelectionOptionsBuilder(), fichotheque, element)) {
                        DomMessages.unknownTagWarning(messageHandler, tagName);
                    }
            }
        }

    }


    private class LangsConsumer implements Consumer<Element> {

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lang")) {
                String langString = DOMUtils.readSimpleElement(element);
                try {
                    Lang lang = Lang.parse(langString);
                    balayageDefBuilder.addLang(lang);
                } catch (ParseException lie) {
                    DomMessages.wrongElementValue(messageHandler, tagName, langString);
                }
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }


    private class PostscriptumConsumer implements Consumer<Element> {

        private final BalayagePostscriptumBuilder balayagePostscriptumBuilder;

        private PostscriptumConsumer(BalayagePostscriptumBuilder balayagePostscriptumBuilder) {
            this.balayagePostscriptumBuilder = balayagePostscriptumBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "script":
                case "command":
                    String script = XMLUtils.getData(element);
                    if (script.length() > 0) {
                        balayagePostscriptumBuilder.addExternalScript(script);
                    }
                    break;
                case "scrutari":
                    String scrutari = XMLUtils.getData(element);
                    if (scrutari.length() > 0) {
                        balayagePostscriptumBuilder.addScrutariExport(scrutari);
                    }
                    break;
                case "sql":
                    String sql = XMLUtils.getData(element);
                    if (sql.length() > 0) {
                        balayagePostscriptumBuilder.addSqlExport(sql);
                    }
                    break;
            }
        }

    }

}
