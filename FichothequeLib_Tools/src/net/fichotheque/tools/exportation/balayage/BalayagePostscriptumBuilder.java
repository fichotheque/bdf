/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.balayage;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.exportation.balayage.BalayagePostscriptum;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class BalayagePostscriptumBuilder {

    private final List<String> externalScriptNameList = new ArrayList<String>();
    private final List<String> scrutariExportNameList = new ArrayList<String>();
    private final List<String> sqlExportNameList = new ArrayList<String>();

    public BalayagePostscriptumBuilder() {

    }

    public BalayagePostscriptumBuilder addExternalScript(String name) {
        externalScriptNameList.add(name);
        return this;
    }

    public BalayagePostscriptumBuilder addScrutariExport(String name) {
        scrutariExportNameList.add(name);
        return this;
    }

    public BalayagePostscriptumBuilder addSqlExport(String name) {
        sqlExportNameList.add(name);
        return this;
    }

    public BalayagePostscriptum toBalayagePostscriptum() {
        return new InternalBalayagePostscriptum(StringUtils.toList(scrutariExportNameList), StringUtils.toList(sqlExportNameList), StringUtils.toList(externalScriptNameList));
    }

    public static BalayagePostscriptumBuilder init() {
        return new BalayagePostscriptumBuilder();
    }


    private static class InternalBalayagePostscriptum implements BalayagePostscriptum {

        private final List<String> scrutariExportNameList;
        private final List<String> sqlExportNameList;
        private final List<String> externalScriptNameList;

        private InternalBalayagePostscriptum(List<String> scrutariExportNameList, List<String> sqlExportNameList, List<String> externalScriptNameList) {
            this.scrutariExportNameList = scrutariExportNameList;
            this.sqlExportNameList = sqlExportNameList;
            this.externalScriptNameList = externalScriptNameList;
        }

        @Override
        public List<String> getScrutariExportNameList() {
            return scrutariExportNameList;
        }

        @Override
        public List<String> getSqlExportNameList() {
            return sqlExportNameList;
        }

        @Override
        public List<String> getExternalScriptNameList() {
            return externalScriptNameList;
        }


    }

}
