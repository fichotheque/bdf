/* FichothequeLib_Tools - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.balayage;

import java.util.List;
import net.fichotheque.exportation.balayage.BalayageContentDescription;
import net.mapeadores.util.logging.LineMessageHandler;
import net.mapeadores.util.logging.MessageByLine;
import net.mapeadores.util.logging.SimpleLineMessageHandler;


/**
 *
 * @author Vincent Calame
 */
public class BalayageContentDescriptionBuilder {

    private final SimpleLineMessageHandler lineMessageHandler = new SimpleLineMessageHandler();
    private final String balayageName;
    private final String path;

    public BalayageContentDescriptionBuilder(String balayageName, String path) {
        this.balayageName = balayageName;
        this.path = path;
    }

    public LineMessageHandler getLineMessageHandler() {
        return lineMessageHandler;
    }

    public BalayageContentDescription toBalayageContentDescription() {
        List<MessageByLine> messageByLineList = lineMessageHandler.toMessageByLineList();
        short state = (messageByLineList.isEmpty()) ? BalayageContentDescription.OK_STATE : BalayageContentDescription.XML_ERROR_STATE;
        return new InternalBalayageContentDescription(balayageName, path, state, messageByLineList);
    }

    public static BalayageContentDescriptionBuilder init(String balayageName, String path) {
        return new BalayageContentDescriptionBuilder(balayageName, path);
    }


    private static class InternalBalayageContentDescription implements BalayageContentDescription {

        private final String balayageName;
        private final String path;
        private final short state;
        private final List<MessageByLine> messageByLineList;

        private InternalBalayageContentDescription(String balayageName, String path, short state, List<MessageByLine> messageByLineList) {
            this.balayageName = balayageName;
            this.path = path;
            this.state = state;
            this.messageByLineList = messageByLineList;
        }

        @Override
        public String getBalayageName() {
            return balayageName;
        }

        @Override
        public String getPath() {
            return path;
        }

        @Override
        public short getState() {
            return state;
        }

        @Override
        public List<MessageByLine> getMessageByLineList() {
            return messageByLineList;
        }

    }

}
