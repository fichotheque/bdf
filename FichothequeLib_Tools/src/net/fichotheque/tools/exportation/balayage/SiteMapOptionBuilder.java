/* BdfServer - Copyright (c) 2010-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.balayage;

import net.fichotheque.exportation.balayage.SiteMapOption;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class SiteMapOptionBuilder {

    private static final String DEFAULT_FILENAME = "sitemap.xml";
    private String path = "";
    private String fileName = DEFAULT_FILENAME;
    private String baseUrl = "";

    public SiteMapOptionBuilder() {
    }

    public SiteMapOptionBuilder setPath(String path) {
        this.path = StringUtils.nullTrim(path);
        return this;
    }

    public SiteMapOptionBuilder setFileName(String fileName) {
        this.fileName = StringUtils.nullTrim(fileName);
        return this;
    }

    public SiteMapOptionBuilder setBaseUrl(String baseUrl) {
        this.baseUrl = StringUtils.nullTrim(baseUrl);
        return this;
    }

    public SiteMapOption toSiteMapOption() {
        if (fileName.isEmpty()) {
            fileName = DEFAULT_FILENAME;
        }
        return new InternalSiteMapOption(path, fileName, baseUrl);
    }

    public static SiteMapOptionBuilder init() {
        return new SiteMapOptionBuilder();
    }


    private static class InternalSiteMapOption implements SiteMapOption {

        private final String path;
        private final String fileName;
        private final String baseUrl;

        private InternalSiteMapOption(String path, String fileName, String baseUrl) {
            this.path = path;
            this.fileName = fileName;
            this.baseUrl = baseUrl;
        }

        @Override
        public String getPath() {
            return path;
        }

        @Override
        public String getFileName() {
            return fileName;
        }

        @Override
        public String getBaseUrl() {
            return baseUrl;
        }

    }

}
