/* FichothequeLib_Tools - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.sql.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.tools.exportation.sql.SqlExportDefBuilder;
import net.fichotheque.tools.selection.SelectionDOMUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class SqlExportDefDOMReader {

    private final Fichotheque fichotheque;
    private final SqlExportDefBuilder sqlExportDefBuilder;
    private final MessageHandler messageHandler;

    public SqlExportDefDOMReader(Fichotheque fichotheque, SqlExportDefBuilder sqlExportDefBuilder, MessageHandler messageHandler) {
        this.fichotheque = fichotheque;
        this.sqlExportDefBuilder = sqlExportDefBuilder;
        this.messageHandler = messageHandler;
    }

    public SqlExportDefDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static SqlExportDefDOMReader init(Fichotheque fichotheque, SqlExportDefBuilder sqlExportDefBuilder, MessageHandler messageHandler) {
        return new SqlExportDefDOMReader(fichotheque, sqlExportDefBuilder, messageHandler);
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {

        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "param":
                    String name = element.getAttribute("name").trim();
                    if (name.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, "param", "name");
                        return;
                    }
                    String value = element.getAttribute("value").trim();
                    if (value.isEmpty()) {
                        value = DOMUtils.readSimpleElement(element);
                    }
                    sqlExportDefBuilder.addParam(name, value);
                    break;
                case "sqlexport-classname":
                    sqlExportDefBuilder.setSqlExportClassName(DOMUtils.readSimpleElement(element));
                    break;
                case "tableexport-name":
                    sqlExportDefBuilder.setTableExportName(DOMUtils.readSimpleElement(element));
                    break;
                case "post-command":
                    sqlExportDefBuilder.setPostCommand(DOMUtils.readSimpleElement(element));
                    break;
                case "target-name":
                    sqlExportDefBuilder.setTargetName(DOMUtils.readSimpleElement(element));
                    break;
                case "target-path":
                    String targetPath = DOMUtils.readSimpleElement(element);
                    try {
                        RelativePath relativePath = RelativePath.parse(targetPath);
                        sqlExportDefBuilder.setTargetPath(relativePath);
                    } catch (ParseException pe) {
                        DomMessages.wrongElementValue(messageHandler, tagName, targetPath);
                    }
                    break;
                case "file-name":
                    String fileName = DOMUtils.readSimpleElement(element);
                    try {
                        sqlExportDefBuilder.setFileName(fileName);
                    } catch (ParseException pe) {
                        DomMessages.wrongElementValue(messageHandler, tagName, fileName);
                    }
                    break;
                case "label":
                case "lib":
                    try {
                    LabelUtils.readLabel(element, sqlExportDefBuilder);
                } catch (ParseException ile) {
                    DomMessages.wrongLangAttribute(messageHandler, tagName, element.getAttribute("xml:lang"));
                }
                break;
                case "attr":
                    AttributeUtils.readAttrElement(sqlExportDefBuilder.getAttributesBuilder(), element);
                    break;
                case "path":
                    readOldPathElement(DOMUtils.readSimpleElement(element));
                    break;
                case "url":
                    break;
                default:
                    if (!SelectionDOMUtils.readSelectionOptions(sqlExportDefBuilder.getSelectionOptionsBuilder(), fichotheque, element)) {
                        DomMessages.unknownTagWarning(messageHandler, tagName);
                    }
            }
        }

        private void readOldPathElement(String path) {
            int idx = path.lastIndexOf("/");
            if (idx > 0) {
                try {
                    sqlExportDefBuilder.setFileName(path.substring(idx + 1));
                } catch (ParseException pe) {

                }
                path = path.substring(0, idx + 1);
                testOldTarget(path, "/home/tomcat/www/", "www");
                testOldTarget(path, "/home/tomcat/sync/", "sync");
            }
        }

        private void testOldTarget(String path, String suffix, String targetName) {
            if (path.startsWith(suffix)) {
                sqlExportDefBuilder.setTargetName(targetName);
                try {
                    RelativePath relativePath = RelativePath.parse(path.substring(suffix.length()));
                    sqlExportDefBuilder.setTargetPath(relativePath);
                } catch (ParseException pe) {

                }
            }
        }

    }


}
