/* FichothequeLib_Tools - Copyright (c) 2016-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.transformation;

import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;


/**
 *
 * @author Vincent Calame
 */
public class TemplateDefBuilder extends DefBuilder {

    private final TemplateKey templateKey;

    public TemplateDefBuilder(TemplateKey templateKey) {
        this(templateKey, null);
    }

    public TemplateDefBuilder(TemplateKey templateKey, @Nullable Attributes initAttributes) {
        super(initAttributes);
        if (templateKey == null) {
            throw new IllegalArgumentException("templateKey is null");
        }
        this.templateKey = templateKey;
    }

    public TemplateDef toTemplateDef() {
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        return new InternalTemplateDef(templateKey, titleLabels, attributes);
    }

    public static TemplateDefBuilder init(TemplateKey templateKey) {
        return new TemplateDefBuilder(templateKey, null);
    }

    public static TemplateDefBuilder init(TemplateKey templateKey, @Nullable Attributes initAttributes) {
        return new TemplateDefBuilder(templateKey, initAttributes);
    }


    private static class InternalTemplateDef implements TemplateDef {

        private final TemplateKey templateKey;
        private final Labels titleLabels;
        private final Attributes attributes;

        private InternalTemplateDef(TemplateKey templateKey,
                Labels titleLabels, Attributes attributes) {
            this.templateKey = templateKey;
            this.titleLabels = titleLabels;
            this.attributes = attributes;
        }

        @Override
        public TemplateKey getTemplateKey() {
            return templateKey;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

    }

}
