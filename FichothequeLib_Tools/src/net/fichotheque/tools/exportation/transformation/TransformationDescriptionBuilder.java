/* FichothequeLib_Tools - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.transformation;

import net.fichotheque.utils.TransformationUtils;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationDescription;
import net.fichotheque.exportation.transformation.TransformationKey;


/**
 *
 * @author Vincent Calame
 */
public class TransformationDescriptionBuilder {

    private final TransformationKey transformationKey;
    private final SortedMap<String, TemplateDescription> simpleMap = new TreeMap<String, TemplateDescription>();
    private final SortedMap<String, TemplateDescription> streamMap = new TreeMap<String, TemplateDescription>();

    public TransformationDescriptionBuilder(TransformationKey transformationKey) {
        this.transformationKey = transformationKey;
    }

    public TransformationDescriptionBuilder addTemplateDescription(TemplateDescription templateDescription) {
        TemplateKey templateKey = templateDescription.getTemplateKey();
        String extension = templateKey.getExtension();
        if (extension == null) {
            simpleMap.put(templateKey.getName(), templateDescription);
        } else {
            String key = extension + "/" + templateKey.getName();
            streamMap.put(key, templateDescription);
        }
        return this;
    }

    public TransformationDescriptionBuilder addTemplateDescriptions(Collection<TemplateDescription> templateDescriptions) {
        for (TemplateDescription templateDescription : templateDescriptions) {
            if (templateDescription != null) {
                addTemplateDescription(templateDescription);
            }
        }
        return this;
    }

    public TransformationDescription toTransformationDescription() {
        List<TemplateDescription> simpleArray = TransformationUtils.wrap(simpleMap.values().toArray(new TemplateDescription[simpleMap.size()]));
        List<TemplateDescription> streamArray = TransformationUtils.wrap(streamMap.values().toArray(new TemplateDescription[streamMap.size()]));
        return new InternalTransformationDescription(transformationKey, simpleArray, streamArray);
    }

    public static TransformationDescriptionBuilder init(TransformationKey transformationKey) {
        return new TransformationDescriptionBuilder(transformationKey);
    }


    private static class InternalTransformationDescription implements TransformationDescription {

        private final TransformationKey transformationKey;
        private final List<TemplateDescription> simpleList;
        private final List<TemplateDescription> streamList;

        private InternalTransformationDescription(TransformationKey transformationKey, List<TemplateDescription> simpleList, List<TemplateDescription> streamList) {
            this.transformationKey = transformationKey;
            this.simpleList = simpleList;
            this.streamList = streamList;
        }

        @Override
        public TransformationKey getTransformationKey() {
            return transformationKey;
        }

        @Override
        public List<TemplateDescription> getSimpleTemplateDescriptionList() {
            return simpleList;
        }

        @Override
        public List<TemplateDescription> getStreamTemplateDescriptionList() {
            return streamList;
        }

    }


}
