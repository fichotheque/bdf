/* BdfServer - Copyright (c) 2019-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.transformation;

import java.util.List;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.logging.LineMessage;
import net.mapeadores.util.logging.LineMessageHandler;
import net.mapeadores.util.logging.MessageByLine;
import net.mapeadores.util.logging.SimpleLineMessageHandler;


/**
 *
 * @author Vincent Calame
 */
public class TemplateContentDescriptionBuilder implements LineMessageHandler {

    private final SimpleLineMessageHandler simpleLineMessageHandler = new SimpleLineMessageHandler();
    private final TemplateKey templateKey;
    private final String path;
    private final String type;
    private boolean mandatory;
    private String state = TemplateContentDescription.OK_STATE;

    public TemplateContentDescriptionBuilder(TemplateKey templateKey, String path, String type) {
        this.templateKey = templateKey;
        this.path = path;
        this.type = type;
    }

    @Override
    public void addMessage(String category, LineMessage message) {
        state = TemplateContentDescription.XML_ERROR_STATE;
        simpleLineMessageHandler.addMessage(category, message);
    }

    public TemplateContentDescriptionBuilder setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
        return this;
    }

    public TemplateContentDescription toTemplateContentDescription() {
        return new InternalTemplateContentDescription(templateKey, path, type, mandatory, state, simpleLineMessageHandler.toMessageByLineList());
    }

    public static TemplateContentDescriptionBuilder init(TemplateKey templateKey, String path, String type) {
        return new TemplateContentDescriptionBuilder(templateKey, path, type);
    }


    private static class InternalTemplateContentDescription implements TemplateContentDescription {

        private final TemplateKey templateKey;
        private final String path;
        private final String type;
        private final boolean mandatory;
        private final String state;
        private final List<MessageByLine> messageByLineList;


        private InternalTemplateContentDescription(TemplateKey templateKey, String path, String type, boolean mandatory, String state, List<MessageByLine> messageByLineList) {
            this.templateKey = templateKey;
            this.path = path;
            this.type = type;
            this.mandatory = mandatory;
            this.state = state;
            this.messageByLineList = messageByLineList;
        }

        @Override
        public TemplateKey getTemplateKey() {
            return templateKey;
        }

        @Override
        public String getPath() {
            return path;
        }

        @Override
        public String getType() {
            return type;
        }

        @Override
        public boolean isMandatory() {
            return mandatory;
        }

        @Override
        public String getState() {
            return state;
        }

        @Override
        public List<MessageByLine> getMessageByLineList() {
            return messageByLineList;
        }

    }

}
