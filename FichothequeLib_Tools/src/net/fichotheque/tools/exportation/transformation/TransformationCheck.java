/* BdfServer - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.exportation.transformation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TransformationDescription;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.utils.TransformationUtils;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public class TransformationCheck {

    private final TransformationDescription transformationDescription;
    private TemplateDescription[] simpleTemplateDescriptionArray;
    private String[] extensionArray;
    private SortedMap<String, TemplateDescription[]> extensionMap;

    private TransformationCheck(TransformationDescription transformationDescription) {
        this.transformationDescription = transformationDescription;
    }

    public TransformationKey getTransformationKey() {
        return transformationDescription.getTransformationKey();
    }

    public TransformationDescription getTransformationDescription() {
        return transformationDescription;
    }

    public boolean withTemplate() {
        return (withSimpleTemplate() || withStreamTemplate());
    }

    public boolean withSimpleTemplate() {
        return (simpleTemplateDescriptionArray != null);
    }

    public boolean withStreamTemplate() {
        return (extensionArray.length != 0);
    }

    /**
     *
     * @return
     */
    @Nullable
    public TemplateDescription[] getSimpleTemplateDescriptionArray() {
        return simpleTemplateDescriptionArray;
    }

    public String[] getExtensionArray() {
        return extensionArray;
    }

    @Nullable
    public TemplateDescription[] getStreamTemplateDescriptionArray(String extension) {
        return extensionMap.get(extension);
    }

    private void init(boolean onlyUseableTemplate) {
        SortedMap<String, TemplateDescription> simpleMap = new TreeMap<String, TemplateDescription>();
        for (TemplateDescription templateDescription : transformationDescription.getSimpleTemplateDescriptionList()) {
            if ((!onlyUseableTemplate) || (TransformationUtils.isUseableTemplateState(templateDescription.getState()))) {
                simpleMap.put(templateDescription.getTemplateKey().getName(), templateDescription);
            }
        }
        int simpleSize = simpleMap.size();
        if (simpleSize == 0) {
            simpleTemplateDescriptionArray = null;
        } else {
            simpleTemplateDescriptionArray = simpleMap.values().toArray(new TemplateDescription[simpleSize]);
        }
        SortedMap<String, List<TemplateDescription>> tempMap = new TreeMap<String, List<TemplateDescription>>();
        for (TemplateDescription templateDescription : transformationDescription.getStreamTemplateDescriptionList()) {
            if ((!onlyUseableTemplate) || (TransformationUtils.isUseableTemplateState(templateDescription.getState()))) {
                String extension = templateDescription.getTemplateKey().getExtension();
                List<TemplateDescription> list = tempMap.get(extension);
                if (list == null) {
                    list = new ArrayList<TemplateDescription>();
                    tempMap.put(extension, list);
                }
                list.add(templateDescription);
            }
        }
        extensionArray = tempMap.keySet().toArray(new String[tempMap.size()]);
        extensionMap = new TreeMap<String, TemplateDescription[]>();
        for (Map.Entry<String, List<TemplateDescription>> entry : tempMap.entrySet()) {
            List<TemplateDescription> list = entry.getValue();
            TemplateDescription[] array = list.toArray(new TemplateDescription[list.size()]);
            extensionMap.put(entry.getKey(), array);
        }
    }

    public static TransformationCheck check(TransformationDescription transformationDescription, boolean onlyUseableTemplate) {
        TransformationCheck transformationCheck = new TransformationCheck(transformationDescription);
        transformationCheck.init(onlyUseableTemplate);
        return transformationCheck;
    }

}
