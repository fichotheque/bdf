/* FichothequeLib_Tools - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.corpus;

import java.text.ParseException;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public class GroupingKey {

    public final static GroupingKey CREATION_DATE = new GroupingKey(null);
    private final FieldKey fieldKey;

    private GroupingKey(FieldKey fieldKey) {
        this.fieldKey = fieldKey;
    }

    public boolean isCreationDate() {
        return (fieldKey == null);
    }

    public boolean isLang() {
        if (fieldKey == null) {
            return false;
        }
        switch (fieldKey.getKeyString()) {
            case FieldKey.SPECIAL_LANG:
                return true;
            default:
                return false;
        }
    }

    @Nullable
    public FieldKey getFieldKey() {
        return fieldKey;
    }

    public static GroupingKey parse(String text) throws ParseException {
        FieldKey fieldKey = FieldKey.parse(text);
        return new GroupingKey(fieldKey);
    }

}
