/* FichothequeLib_Tools - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.corpus;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.Information;
import net.fichotheque.corpus.fiche.Propriete;
import net.fichotheque.corpus.fiche.Section;
import net.fichotheque.corpus.metadata.FieldKey;


/**
 *
 * @author Vincent Calame
 */
public final class PurgeEngine {

    private final Predicate<FieldKey> fieldPredicate;
    private final boolean soustitrePurge;

    public PurgeEngine(Predicate<FieldKey> fieldPredicate) {
        this.fieldPredicate = fieldPredicate;
        this.soustitrePurge = !fieldPredicate.test(FieldKey.SOUSTITRE);
    }

    public void purge(Fiche fiche) {
        if (soustitrePurge) {
            fiche.setSoustitre(null);
        }
        purgeProprieteList(fiche);
        purgeInformationList(fiche);
        purgeSectionList(fiche);
    }

    private void purgeProprieteList(Fiche fiche) {
        List<FieldKey> purgeList = new ArrayList<FieldKey>();
        for (Propriete propriete : fiche.getProprieteList()) {
            FieldKey fieldKey = propriete.getFieldKey();
            if (!fieldPredicate.test(fieldKey)) {
                purgeList.add(fieldKey);
            }
        }
        if (!purgeList.isEmpty()) {
            for (FieldKey fieldKey : purgeList) {
                fiche.setPropriete(fieldKey, null);
            }
        }
    }

    private void purgeInformationList(Fiche fiche) {
        List<FieldKey> purgeList = new ArrayList<FieldKey>();
        for (Information information : fiche.getInformationList()) {
            FieldKey fieldKey = information.getFieldKey();
            if (!fieldPredicate.test(fieldKey)) {
                purgeList.add(fieldKey);
            }
        }
        if (!purgeList.isEmpty()) {
            for (FieldKey fieldKey : purgeList) {
                fiche.setInformation(fieldKey, null);
            }
        }
    }

    private void purgeSectionList(Fiche fiche) {
        List<FieldKey> purgeList = new ArrayList<FieldKey>();
        for (Section section : fiche.getSectionList()) {
            FieldKey fieldKey = section.getFieldKey();
            if (!fieldPredicate.test(fieldKey)) {
                purgeList.add(fieldKey);
            }
        }
        if (!purgeList.isEmpty()) {
            for (FieldKey fieldKey : purgeList) {
                fiche.setSection(fieldKey, null);
            }
        }
    }

}
