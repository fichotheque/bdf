/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.corpus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.corpus.FicheChange;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Information;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Propriete;
import net.fichotheque.corpus.fiche.Section;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public class FicheChangeBuilder {

    private final Map<FieldKey, Boolean> fieldKeyMap = new HashMap<FieldKey, Boolean>();
    private final Fiche fiche = new Fiche();

    public FicheChangeBuilder() {
    }

    public FicheChangeBuilder setTitre(CleanedString cs) {
        if (cs == null) {
            fieldKeyMap.put(FieldKey.TITRE, Boolean.FALSE);
            fiche.setTitre("");
        } else {
            fieldKeyMap.put(FieldKey.TITRE, Boolean.TRUE);
            fiche.setTitre(cs.toString());
        }
        return this;
    }

    public FicheChangeBuilder setLang(Lang lang) {
        if (lang == null) {
            fieldKeyMap.put(FieldKey.LANG, Boolean.FALSE);
            fiche.setLang(null);
        } else {
            fieldKeyMap.put(FieldKey.LANG, Boolean.TRUE);
            fiche.setLang(lang);
        }
        return this;
    }

    public FicheChangeBuilder setSoustitre(Para soustitrePara) {
        if ((soustitrePara == null) || (soustitrePara.isEmpty())) {
            fieldKeyMap.put(FieldKey.SOUSTITRE, Boolean.FALSE);
            fiche.setSoustitre(null);
        } else {
            fieldKeyMap.put(FieldKey.SOUSTITRE, Boolean.TRUE);
            fiche.setSoustitre(soustitrePara);
        }
        return this;
    }

    public FicheChangeBuilder putRedacteurs(FicheItems ficheItems) {
        if ((ficheItems == null) || (ficheItems.isEmpty())) {
            if (!fieldKeyMap.containsKey(FieldKey.REDACTEURS)) {
                fieldKeyMap.put(FieldKey.REDACTEURS, Boolean.FALSE);
            }
        } else {
            fieldKeyMap.put(FieldKey.REDACTEURS, Boolean.TRUE);
            fiche.appendRedacteurs(ficheItems);
        }
        return this;
    }

    public FicheChangeBuilder setPropriete(FieldKey fieldKey, FicheItem ficheItem) {
        fiche.setPropriete(fieldKey, ficheItem);
        if (ficheItem == null) {
            fieldKeyMap.put(fieldKey, Boolean.FALSE);
        } else {
            fieldKeyMap.put(fieldKey, Boolean.TRUE);
        }
        return this;
    }

    public FicheChangeBuilder putInformation(FieldKey fieldKey, FicheItems ficheItems) {
        if ((ficheItems == null) || (ficheItems.isEmpty())) {
            if (!fieldKeyMap.containsKey(fieldKey)) {
                fieldKeyMap.put(fieldKey, Boolean.FALSE);
            }
        } else {
            fieldKeyMap.put(fieldKey, Boolean.TRUE);
            fiche.appendInformation(fieldKey, ficheItems);
        }
        return this;
    }

    public FicheChangeBuilder putSection(FieldKey fieldKey, FicheBlocks ficheBlocks) {
        if ((ficheBlocks == null) || (ficheBlocks.isEmpty())) {
            if (!fieldKeyMap.containsKey(fieldKey)) {
                fieldKeyMap.put(fieldKey, Boolean.FALSE);
            }
        } else {
            fieldKeyMap.put(fieldKey, Boolean.TRUE);
            fiche.appendSection(fieldKey, ficheBlocks);
        }
        return this;
    }

    public FicheChangeBuilder removeField(FieldKey fieldKey) {
        switch (fieldKey.getCategory()) {
            case FieldKey.PROPRIETE_CATEGORY:
                setPropriete(fieldKey, null);
                break;
            case FieldKey.INFORMATION_CATEGORY:
                putInformation(fieldKey, null);
                break;
            case FieldKey.SECTION_CATEGORY:
                putSection(fieldKey, null);
                break;
            case FieldKey.SPECIAL_CATEGORY:
                switch (fieldKey.getKeyString()) {
                    case FieldKey.SPECIAL_TITRE:
                        setTitre(null);
                        break;
                    case FieldKey.SPECIAL_SOUSTITRE:
                        setSoustitre(null);
                        break;
                    case FieldKey.SPECIAL_LANG:
                        setLang(null);
                        break;
                    case FieldKey.SPECIAL_REDACTEURS:
                        putRedacteurs(null);
                        break;
                }
                break;
        }
        return this;
    }

    public FicheChangeBuilder appendFiche(Fiche fiche) {
        CleanedString titre = CleanedString.newInstance(fiche.getTitre());
        if (titre != null) {
            setTitre(titre);
        }
        Lang lang = fiche.getLang();
        if (lang != null) {
            setLang(lang);
        }
        Para soustitre = fiche.getSoustitre();
        if (soustitre != null) {
            setSoustitre(soustitre);
        }
        FicheItems redacteurs = fiche.getRedacteurs();
        if (redacteurs != null) {
            putRedacteurs(redacteurs);
        }
        for (Propriete propriete : fiche.getProprieteList()) {
            setPropriete(propriete.getFieldKey(), propriete.getFicheItem());
        }
        for (Information information : fiche.getInformationList()) {
            putInformation(information.getFieldKey(), information);
        }
        for (Section section : fiche.getSectionList()) {
            putSection(section.getFieldKey(), section);
        }
        return this;
    }

    public FicheChangeBuilder merge(FicheChangeBuilder otherFicheChangeBuilder) {
        Fiche otherFiche = otherFicheChangeBuilder.fiche;
        for (Information information : otherFiche.getInformationList()) {
            putInformation(information.getFieldKey(), information);
        }
        for (Section section : otherFiche.getSectionList()) {
            putSection(section.getFieldKey(), section);
        }
        FicheItems otherRedacteurs = otherFiche.getRedacteurs();
        if (otherRedacteurs != null) {
            putRedacteurs(otherRedacteurs);
        }
        for (Map.Entry<FieldKey, Boolean> entry : otherFicheChangeBuilder.fieldKeyMap.entrySet()) {
            boolean b = entry.getValue();
            if (!b) {
                FieldKey fieldKey = entry.getKey();
                if (!fieldKeyMap.containsKey(fieldKey)) {
                    fieldKeyMap.put(fieldKey, Boolean.FALSE);
                }
            }
        }
        return this;
    }

    public FicheChange toFicheChange() {
        boolean notEmpty = false;
        List<FieldKey> removedList = new ArrayList<FieldKey>();
        for (Map.Entry<FieldKey, Boolean> entry : fieldKeyMap.entrySet()) {
            Boolean b = entry.getValue();
            if (!b) {
                removedList.add(entry.getKey());
            } else {
                notEmpty = true;
            }
        }
        Fiche ficheAPI;
        if (notEmpty) {
            ficheAPI = fiche;
        } else {
            ficheAPI = null;
        }
        List<FieldKey> finalRemovedList = CorpusMetadataUtils.wrap(removedList.toArray(new FieldKey[removedList.size()]));
        return new InternalFicheChange(ficheAPI, finalRemovedList);
    }

    public static FicheChangeBuilder init() {
        return new FicheChangeBuilder();
    }


    private static class InternalFicheChange implements FicheChange {

        private final FicheAPI ficheAPI;
        private final List<FieldKey> removedList;

        private InternalFicheChange(FicheAPI ficheAPI, List<FieldKey> removedList) {
            this.ficheAPI = ficheAPI;
            this.removedList = removedList;
        }

        @Override
        public FicheAPI getFicheAPI() {
            return ficheAPI;
        }

        @Override
        public List<FieldKey> getRemovedList() {
            return removedList;
        }

    }

}
