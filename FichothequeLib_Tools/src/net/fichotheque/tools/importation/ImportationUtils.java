/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation;

import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.importation.LiensImport;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.include.IncludeKey;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public final class ImportationUtils {

    public final static LiensImport EMPTY_LIENSIMPORT = new EmptyLiensImport();
    public final static List<LiensImport.LienImport> EMPTY_LIENIMPORTLIST = Collections.emptyList();

    private ImportationUtils() {

    }

    public static ThesaurusImport.MotcleImport toMotcleImport(Motcle motcle) {
        return new InternalMotcleImport(motcle);
    }

    public static boolean isValidFicheImportFileName(String type, String name) {
        int typeLength = type.length();
        int wholeLength = typeLength + 1 + 8 + 4;
        if (name.length() != wholeLength) {
            return false;
        }
        if (name.charAt(typeLength) != '-') {
            return false;
        }
        if (!name.endsWith(".xml")) {
            return false;
        }
        if (!name.startsWith(type)) {
            return false;
        }
        for (int i = (typeLength + 1); i < (typeLength + 1 + 8); i++) {
            char carac = name.charAt(i);
            if ((carac < 'a') || (carac > 'z')) {
                return false;
            }
        }
        return true;
    }

    public static void updateIncrement(String type, List<String> fileList, Increment increment) {
        int size = fileList.size();
        if (size == 0) {
            return;
        }
        String lastName = fileList.get(size - 1);
        int start = type.length() + 1;
        for (int i = 0; i < 8; i++) {
            increment.setChar(i, lastName.charAt(i + start));
        }
    }

    public static boolean testCorpusImportType(String type) {
        if (type.equals(CorpusImport.CHANGE_TYPE)) {
            return true;
        }
        if (type.equals(CorpusImport.CREATION_TYPE)) {
            return true;
        }
        if (type.equals(CorpusImport.REMOVE_TYPE)) {
            return true;
        }
        return false;
    }

    public static LiensImport.LienImport toLienImport(IncludeKey originIncludeKey, SubsetItem otherSubsetItem, int poids) {
        return new InternalLienImport(originIncludeKey, otherSubsetItem, poids);
    }

    public static LiensImport.LienImport toLienImport(IncludeKey originIncludeKey, Subset otherSubset, Label label, int poids) {
        return new InternalLienImport(originIncludeKey, otherSubset, label, poids);
    }

    public static LiensImport.LienImport toLiageLienImport(SubsetItem otherSubsetItem, int poids) {
        return new InternalLienImport(null, otherSubsetItem, poids);
    }

    public static List<CorpusImport.FicheImport> wrap(CorpusImport.FicheImport[] array) {
        return new FicheImportList(array);
    }

    public static List<ThesaurusImport.MotcleImport> wrap(ThesaurusImport.MotcleImport[] array) {
        return new MotcleImportList(array);
    }


    private static class InternalMotcleImport implements ThesaurusImport.MotcleImport {

        private final Motcle motcle;

        InternalMotcleImport(Motcle motcle) {
            this.motcle = motcle;
        }

        @Override
        public Motcle getMotcle() {
            return motcle;
        }

    }


    private static class InternalLienImport implements LiensImport.LienImport {

        private final IncludeKey originIncludeKey;
        private final Subset otherSubset;
        private final SubsetItem otherSubsetItem;
        private final int poids;
        private final Label label;

        private InternalLienImport(IncludeKey originIncludeKey, SubsetItem otherSubsetItem, int poids) {
            this.originIncludeKey = originIncludeKey;
            this.otherSubset = otherSubsetItem.getSubset();
            this.otherSubsetItem = otherSubsetItem;
            this.poids = poids;
            this.label = null;
        }

        private InternalLienImport(IncludeKey originIncludeKey, Subset otherSubset, Label label, int poids) {
            this.originIncludeKey = originIncludeKey;
            this.otherSubset = otherSubset;
            this.otherSubsetItem = null;
            this.poids = poids;
            this.label = label;
        }

        @Override
        public IncludeKey getOriginIncludeKey() {
            return originIncludeKey;
        }

        @Override
        public Subset getOtherSubset() {
            return otherSubset;
        }

        @Override
        public int getPoids() {
            return poids;
        }

        @Override
        public SubsetItem getOtherSubsetItem() {
            return otherSubsetItem;
        }

        @Override
        public Label getLabel() {
            return label;
        }

    }


    private static class EmptyLiensImport implements LiensImport {

        private EmptyLiensImport() {

        }

        @Override
        public boolean isLiageRemoved() {
            return false;
        }

        @Override
        public List<IncludeKey> getRemovedIncludeKeyList() {
            return FichothequeUtils.EMPTY_INCLUDEKEYLIST;
        }

        @Override
        public List<LienImport> getReplaceLienImportList() {
            return EMPTY_LIENIMPORTLIST;
        }

        @Override
        public List<LienImport> getAppendLienImportList() {
            return EMPTY_LIENIMPORTLIST;
        }

        @Override
        public boolean isEmpty() {
            return true;
        }

    }


    private static class FicheImportList extends AbstractList<CorpusImport.FicheImport> implements RandomAccess {

        private final CorpusImport.FicheImport[] array;

        private FicheImportList(CorpusImport.FicheImport[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public CorpusImport.FicheImport get(int index) {
            return array[index];
        }

    }


    private static class MotcleImportList extends AbstractList<ThesaurusImport.MotcleImport> implements RandomAccess {

        private final ThesaurusImport.MotcleImport[] array;

        private MotcleImportList(ThesaurusImport.MotcleImport[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ThesaurusImport.MotcleImport get(int index) {
            return array[index];
        }

    }

}
