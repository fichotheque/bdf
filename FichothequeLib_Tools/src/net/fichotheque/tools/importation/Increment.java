/* FichothequeLib_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation;


/**
 *
 * @author Vincent Calame
 */
public class Increment {

    private final char[] array = new char[8];

    {
        for (int i = 0; i < 8; i++) {
            array[i] = 'a';
        }
    }

    public Increment() {
    }

    public void increase() {
        increase(7);
    }

    private void increase(int index) {
        char carac = array[index];
        if (carac != 'z') {
            array[index] = (char) (carac + 1);
        } else {
            if (index == 0) {
                throw new UnsupportedOperationException("zzzzzz increment");
            }
            array[index] = 'a';
            increase(index - 1);
        }
    }

    public void appendTo(StringBuilder buf) {
        for (int i = 0; i < 8; i++) {
            buf.append(array[i]);
        }
    }

    public void setChar(int index, char carac) {
        if ((carac < 'a') || (carac > 'z')) {
            throw new IllegalArgumentException();
        }
        array[index] = carac;
    }

}
