/* FichothequeLib_Tools - Copyright (c) 2013-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.importation.ParseResult;
import net.fichotheque.importation.ParseResult.ResultItem;


/**
 *
 * @author Vincent Calame
 */
public class ParseResultBuilder {
    
    private final InternalParseResult parseResult;
    
    public ParseResultBuilder(String type) {
        this.parseResult = new InternalParseResult(type);
    }
    
    public void addInitError(String key, String value) {
        parseResult.addInitError(key, value);
    }
    
    public void addInitWarning(String key, String value) {
        parseResult.addInitWarning(key, value);
    }
    
    public void addParseError(String parseError, int lineNumber) {
        parseResult.addParseError(parseError, lineNumber);
    }
    
    public void addBdfError(String key, String text, int lineNumber) {
        parseResult.addBdfError(key, text, lineNumber);
    }
    
    public boolean checkDoublon(String idString, int lineNumber) {
        ResultItem currentResultItem = parseResult.resultItemMap.get(idString);
        if (currentResultItem != null) {
            parseResult.addBdfError("DOUBLON", String.valueOf(currentResultItem.getLineNumber()), lineNumber);
            return true;
        } else {
            return false;
        }
    }
    
    public void addResult(String idString, Object key, Object value, int lineNumber) {
        InternalResultItem resultItem = new InternalResultItem(key, value, lineNumber);
        parseResult.resultItemMap.put(idString, resultItem);
    }
    
    public ParseResult toParseResult() {
        parseResult.flush();
        return parseResult;
    }
    
    public void setTmpFile(File tmpFile) {
        parseResult.setTmpFile(tmpFile);
    }
    
    public void setTmpDir(File tmpDir) {
        parseResult.setTmpDir(tmpDir);
    }
    
    
    private static class InternalParseResult implements ParseResult {
        
        private final String type;
        private final List<InternalInitError> initErrorList = new ArrayList<InternalInitError>();
        private final List<InternalInitWarning> initWarningList = new ArrayList<InternalInitWarning>();
        private final List<InternalParseError> parseErrorList = new ArrayList<InternalParseError>();
        private final List<InternalBdfError> bdfErrorList = new ArrayList<InternalBdfError>();
        private final Map<String, InternalResultItem> resultItemMap = new LinkedHashMap<String, InternalResultItem>();
        private List<InternalResultItem> resultItemList;
        private File tmpFile = null;
        private File tmpDir = null;
        
        private InternalParseResult(String type) {
            this.type = type;
        }
        
        @Override
        public String getType() {
            return type;
        }
        
        @Override
        public int getInitErrorCount() {
            return initErrorList.size();
        }
        
        @Override
        public InitError getInitError(int i) {
            return initErrorList.get(i);
        }
        
        @Override
        public int getInitWarningCount() {
            return initWarningList.size();
        }
        
        @Override
        public InitWarning getInitWarning(int i) {
            return initWarningList.get(i);
        }
        
        @Override
        public int getParseErrorCount() {
            return parseErrorList.size();
        }
        
        @Override
        public ParseError getParseError(int i) {
            return parseErrorList.get(i);
        }
        
        @Override
        public int getBdfErrorCount() {
            return bdfErrorList.size();
        }
        
        @Override
        public BdfError getBdfError(int i) {
            return bdfErrorList.get(i);
        }
        
        @Override
        public File getTmpFile() {
            return tmpFile;
        }
        
        @Override
        public File getTmpDir() {
            return tmpDir;
        }
        
        private void addInitError(String key, String text) {
            initErrorList.add(new InternalInitError(key, text));
        }
        
        private void addInitWarning(String key, String text) {
            initWarningList.add(new InternalInitWarning(key, text));
        }
        
        private void addParseError(String parseError, int lineNumber) {
            parseErrorList.add(new InternalParseError(parseError, lineNumber));
        }
        
        private void addBdfError(String key, String text, int lineNumber) {
            bdfErrorList.add(new InternalBdfError(key, text, lineNumber));
        }
        
        private void setTmpFile(File tmpFile) {
            this.tmpFile = tmpFile;
        }
        
        private void setTmpDir(File tmpDir) {
            this.tmpDir = tmpDir;
        }
        
        private void flush() {
            resultItemList = new ArrayList(resultItemMap.values());
        }
        
    }
    
    
    private static class InternalInitError implements ParseResult.InitError {
        
        private String key;
        private String text;
        
        private InternalInitError(String key, String text) {
            this.key = key;
            this.text = text;
        }
        
        @Override
        public String getKey() {
            return key;
        }
        
        @Override
        public String getText() {
            return text;
        }
        
    }
    
    
    private static class InternalInitWarning implements ParseResult.InitWarning {
        
        private String key;
        private String text;
        
        private InternalInitWarning(String key, String text) {
            this.key = key;
            this.text = text;
        }
        
        @Override
        public String getKey() {
            return key;
        }
        
        @Override
        public String getText() {
            return text;
        }
        
    }
    
    
    private static class InternalParseError implements ParseResult.ParseError {
        
        private String rawText;
        private int lineNumber;
        
        private InternalParseError(String rawText, int lineNumber) {
            this.rawText = rawText;
            this.lineNumber = lineNumber;
        }
        
        @Override
        public String getRawText() {
            return rawText;
        }
        
        @Override
        public int getLineNumber() {
            return lineNumber;
        }
        
    }
    
    
    private static class InternalBdfError implements ParseResult.BdfError {
        
        private String key;
        private String text;
        private int lineNumber;
        
        private InternalBdfError(String key, String text, int lineNumber) {
            this.key = key;
            this.text = text;
            this.lineNumber = lineNumber;
        }
        
        @Override
        public String getKey() {
            return key;
        }
        
        @Override
        public String getText() {
            return text;
        }
        
        @Override
        public int getLineNumber() {
            return lineNumber;
        }
        
    }
    
    
    private static class InternalResultItem implements ParseResult.ResultItem {
        
        private Object keyObject;
        private Object valueObject;
        private int lineNumber;
        
        private InternalResultItem(Object keyObject, Object valueObject, int lineNumber) {
            this.keyObject = keyObject;
            this.valueObject = valueObject;
            this.lineNumber = lineNumber;
        }
        
        @Override
        public Object getKeyObject() {
            return keyObject;
        }
        
        @Override
        public Object getValueObject() {
            return valueObject;
        }
        
        @Override
        public int getLineNumber() {
            return lineNumber;
        }
        
        private void setValueObject(Object obj) {
            this.valueObject = obj;
        }
        
    }
    
}
