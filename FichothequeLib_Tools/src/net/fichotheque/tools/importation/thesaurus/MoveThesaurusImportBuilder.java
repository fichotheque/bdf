/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.importation.ImportationUtils;


/**
 *
 * @author Vincent Calame
 */
public class MoveThesaurusImportBuilder extends ThesaurusImportBuilder {

    private final Set<Motcle> moveSet = new LinkedHashSet<Motcle>();

    public MoveThesaurusImportBuilder(Thesaurus thesaurus, Thesaurus destinationThesaurus) {
        super(thesaurus, ThesaurusImport.MOVE_TYPE, destinationThesaurus);
    }

    public MoveThesaurusImportBuilder add(Motcle motcle) {
        moveSet.add(motcle);
        return this;
    }

    @Override
    protected List<ThesaurusImport.MotcleImport> getMotcleImportList() {
        int p = 0;
        ThesaurusImport.MotcleImport[] array = new ThesaurusImport.MotcleImport[moveSet.size()];
        for (Motcle motcle : moveSet) {
            array[p] = ImportationUtils.toMotcleImport(motcle);
            p++;
        }
        return ImportationUtils.wrap(array);
    }

    public MoveThesaurusImportBuilder init(Thesaurus thesaurus, Thesaurus destinationThesaurus) {
        return new MoveThesaurusImportBuilder(thesaurus, destinationThesaurus);
    }

}
