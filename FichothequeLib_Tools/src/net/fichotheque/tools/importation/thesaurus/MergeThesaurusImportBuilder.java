/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.importation.ImportationUtils;


/**
 *
 * @author Vincent Calame
 */
public class MergeThesaurusImportBuilder extends ThesaurusImportBuilder {

    private final Map<Integer, InternalMergeMotcleImport> mergeMap = new LinkedHashMap<Integer, InternalMergeMotcleImport>();

    public MergeThesaurusImportBuilder(Thesaurus thesaurus, Thesaurus destinationThesaurus) {
        super(thesaurus, ThesaurusImport.MERGE_TYPE, destinationThesaurus);
    }

    public MergeThesaurusImportBuilder add(Motcle motcle, Motcle destinationMotcle) {
        if (!mergeMap.containsKey(motcle.getId())) {
            mergeMap.put(motcle.getId(), new InternalMergeMotcleImport(motcle, destinationMotcle));
        }
        return this;
    }

    @Override
    protected List<ThesaurusImport.MotcleImport> getMotcleImportList() {
        int p = 0;
        ThesaurusImport.MotcleImport[] array = new ThesaurusImport.MotcleImport[mergeMap.size()];
        for (ThesaurusImport.MergeMotcleImport mergeMotcleImport : mergeMap.values()) {
            array[p] = mergeMotcleImport;
            p++;
        }
        return ImportationUtils.wrap(array);
    }

    public MergeThesaurusImportBuilder init(Thesaurus thesaurus, Thesaurus destinationThesaurus) {
        return new MergeThesaurusImportBuilder(thesaurus, destinationThesaurus);
    }


    private static class InternalMergeMotcleImport implements ThesaurusImport.MergeMotcleImport {

        private final Motcle motcle;
        private final Motcle destinationMotcle;

        private InternalMergeMotcleImport(Motcle motcle, Motcle destinationMotcle) {
            this.motcle = motcle;
            this.destinationMotcle = destinationMotcle;
        }

        @Override
        public Motcle getMotcle() {
            return motcle;
        }

        @Override
        public Motcle getDestinationMotcle() {
            return destinationMotcle;
        }


    }

}
