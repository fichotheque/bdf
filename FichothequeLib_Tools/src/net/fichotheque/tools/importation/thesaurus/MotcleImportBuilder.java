/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import net.fichotheque.importation.LiensImport;
import net.fichotheque.tools.importation.ImportationUtils;
import net.fichotheque.tools.importation.LiensImportBuilder;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.text.LabelUtils;


/**
 *
 * @author Vincent Calame
 */
public abstract class MotcleImportBuilder {

    protected LabelChangeBuilder labelChangeBuilder;
    protected AttributeChangeBuilder attributeChangeBuilder;
    protected LiensImportBuilder liensImportBuilder;

    public MotcleImportBuilder() {

    }

    public LabelChangeBuilder getLabelChangeBuilder() {
        if (labelChangeBuilder == null) {
            labelChangeBuilder = new LabelChangeBuilder();
        }
        return labelChangeBuilder;
    }

    public AttributeChangeBuilder getAttributeChangeBuilder() {
        if (attributeChangeBuilder == null) {
            attributeChangeBuilder = new AttributeChangeBuilder();
        }
        return attributeChangeBuilder;
    }

    public LiensImportBuilder getLiensImportBuilder() {
        if (liensImportBuilder == null) {
            liensImportBuilder = new LiensImportBuilder();
        }
        return liensImportBuilder;
    }

    protected LabelChange getLabelChange() {
        if (labelChangeBuilder == null) {
            return LabelUtils.EMPTY_LABELCHANGE;
        } else {
            return labelChangeBuilder.toLabelChange();
        }
    }

    protected AttributeChange getAttributeChange() {
        if (attributeChangeBuilder == null) {
            return AttributeUtils.EMPTY_ATTRIBUTECHANGE;
        } else {
            return attributeChangeBuilder.toAttributeChange();
        }
    }

    protected LiensImport getLiensImport() {
        if (liensImportBuilder == null) {
            return ImportationUtils.EMPTY_LIENSIMPORT;
        } else {
            return liensImportBuilder.toLiensImport();
        }
    }

}
