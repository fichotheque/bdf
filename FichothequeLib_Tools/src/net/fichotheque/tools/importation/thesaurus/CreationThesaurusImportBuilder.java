/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.importation.ImportationUtils;


/**
 *
 * @author Vincent Calame
 */
public class CreationThesaurusImportBuilder extends ThesaurusImportBuilder {

    private final List<CreationMotcleImportBuilder> builderList = new ArrayList<CreationMotcleImportBuilder>();
    private final Map<Integer, CreationMotcleImportBuilder> builderMap = new HashMap<Integer, CreationMotcleImportBuilder>();
    private final Map<String, CreationMotcleImportBuilder> byIdalphaMap = new HashMap<String, CreationMotcleImportBuilder>();

    public CreationThesaurusImportBuilder(Thesaurus thesaurus) {
        super(thesaurus, ThesaurusImport.CREATION_TYPE);
    }

    public CreationMotcleImportBuilder getCreationMotcleImportBuilder(int newId, String newIdalpha) {
        CreationMotcleImportBuilder builder;
        if (newId > 0) {
            builder = builderMap.get(newId);
            if (builder == null) {
                newIdalpha = testIdalpha(newIdalpha);
                builder = CreationMotcleImportBuilder.init(newId);
                builderList.add(builder);
                builderMap.put(newId, builder);
                if (newIdalpha != null) {
                    builder.setNewIdalpha(newIdalpha);
                    byIdalphaMap.put(newIdalpha, builder);
                }
            }
        } else {
            newIdalpha = testIdalpha(newIdalpha);
            if (newIdalpha != null) {
                builder = byIdalphaMap.get(newIdalpha);
                if (builder == null) {
                    builder = CreationMotcleImportBuilder.init(-1).setNewIdalpha(newIdalpha);
                    byIdalphaMap.put(newIdalpha, builder);
                    builderList.add(builder);
                }
            } else {
                builder = CreationMotcleImportBuilder.init(-1);
                builderList.add(builder);
            }
        }
        return builder;
    }

    private String testIdalpha(String idalpha) {
        if (getThesaurus().isIdalphaType()) {
            if (idalpha == null) {
                throw new IllegalArgumentException("newIdalpha is null");
            }
            return idalpha;
        } else {
            return null;
        }
    }

    @Override
    protected List<ThesaurusImport.MotcleImport> getMotcleImportList() {
        int p = 0;
        ThesaurusImport.MotcleImport[] array = new ThesaurusImport.MotcleImport[builderList.size()];
        for (CreationMotcleImportBuilder builder : builderList) {
            array[p] = builder.toCreationMotcleImport();
            p++;
        }
        return ImportationUtils.wrap(array);
    }

}
