/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.importation.ImportationUtils;


/**
 *
 * @author Vincent Calame
 */
public class ChangeThesaurusImportBuilder extends ThesaurusImportBuilder {

    private final Map<Integer, ChangeMotcleImportBuilder> motcleMap = new LinkedHashMap<Integer, ChangeMotcleImportBuilder>();

    public ChangeThesaurusImportBuilder(Thesaurus thesaurus) {
        super(thesaurus, ThesaurusImport.CHANGE_TYPE);
    }

    public ChangeMotcleImportBuilder getChangeMotcleImportBuilder(Motcle motcle) {
        ChangeMotcleImportBuilder builder = motcleMap.get(motcle.getId());
        if (builder == null) {
            builder = new ChangeMotcleImportBuilder(motcle);
            motcleMap.put(motcle.getId(), builder);
        }
        return builder;
    }

    @Override
    protected List<ThesaurusImport.MotcleImport> getMotcleImportList() {
        int size = motcleMap.size();
        int p = 0;
        ThesaurusImport.MotcleImport[] array = new ThesaurusImport.MotcleImport[size];
        for (ChangeMotcleImportBuilder builder : motcleMap.values()) {
            ThesaurusImport.ChangeMotcleImport changeMotcleImport = builder.toChangeMotcleImport();
            if (!changeMotcleImport.isEmpty()) {
                array[p] = changeMotcleImport;
                p++;
            }
        }
        if (p < size) {
            ThesaurusImport.MotcleImport[] temp = new ThesaurusImport.MotcleImport[p];
            System.arraycopy(array, 0, temp, 0, p);
            array = temp;
        }
        return ImportationUtils.wrap(array);
    }

    public static ChangeThesaurusImportBuilder init(Thesaurus thesaurus) {
        return new ChangeThesaurusImportBuilder(thesaurus);
    }

}
