/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import java.util.List;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Thesaurus;


/**
 *
 * @author Vincent Calame
 */
public abstract class ThesaurusImportBuilder {

    private final Thesaurus thesaurus;
    private final Thesaurus destinationThesaurus;
    private final String type;

    protected ThesaurusImportBuilder(Thesaurus thesaurus, String type, Thesaurus destinationThesaurus) {
        this.thesaurus = thesaurus;
        this.destinationThesaurus = destinationThesaurus;
        this.type = type;
    }

    protected ThesaurusImportBuilder(Thesaurus thesaurus, String type) {
        this(thesaurus, type, null);
    }

    public Thesaurus getThesaurus() {
        return thesaurus;
    }

    public Thesaurus getDestinationThesaurus() {
        return destinationThesaurus;
    }

    public String getType() {
        return type;
    }

    protected abstract List<ThesaurusImport.MotcleImport> getMotcleImportList();

    public ThesaurusImport toThesaurusImport() {
        return new InternalThesaurusImport(thesaurus, type, destinationThesaurus, getMotcleImportList());
    }

    public static ThesaurusImportBuilder init(Thesaurus thesaurus, String type) {
        return init(thesaurus, type, null);
    }

    public static ThesaurusImportBuilder init(Thesaurus thesaurus, String type, Thesaurus destinationThesaurus) {
        type = ThesaurusImport.checkType(type);
        switch (type) {
            case ThesaurusImport.CHANGE_TYPE:
                return new ChangeThesaurusImportBuilder(thesaurus);
            case ThesaurusImport.CREATION_TYPE:
                return new CreationThesaurusImportBuilder(thesaurus);
            case ThesaurusImport.REMOVE_TYPE:
                return new RemoveThesaurusImportBuilder(thesaurus);
            case ThesaurusImport.MOVE_TYPE:
                return new MoveThesaurusImportBuilder(thesaurus, destinationThesaurus);
            case ThesaurusImport.MERGE_TYPE:
                return new MergeThesaurusImportBuilder(thesaurus, destinationThesaurus);
            default:
                throw new IllegalStateException("Test done before");
        }
    }


    private static class InternalThesaurusImport implements ThesaurusImport {

        private final Thesaurus thesaurus;
        private final Thesaurus destinationThesaurus;
        private final String type;
        private final List<MotcleImport> motcleImportList;

        private InternalThesaurusImport(Thesaurus thesaurus, String type, Thesaurus destinationThesaurus, List<MotcleImport> motcleImportList) {
            this.thesaurus = thesaurus;
            this.type = type;
            this.destinationThesaurus = destinationThesaurus;
            this.motcleImportList = motcleImportList;
        }

        @Override
        public Thesaurus getThesaurus() {
            return thesaurus;
        }

        @Override
        public Thesaurus getDestinationThesaurus() {
            return destinationThesaurus;
        }

        @Override
        public String getType() {
            return type;
        }

        @Override
        public List<MotcleImport> getMotcleImportList() {
            return motcleImportList;
        }

    }

}
