/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import net.fichotheque.importation.LiensImport;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public class ChangeMotcleImportBuilder extends MotcleImportBuilder {

    private final Motcle motcle;
    private String newIdalpha = null;
    private String newStatus = null;
    private Object parent = null;

    public ChangeMotcleImportBuilder(Motcle motcle) {
        this.motcle = motcle;
    }

    public ChangeMotcleImportBuilder setNewIdalpha(String newIdalpha) {
        this.newIdalpha = newIdalpha;
        return this;
    }

    public ChangeMotcleImportBuilder setNewStatus(String newStatus) {
        this.newStatus = newStatus;
        return this;
    }

    public ChangeMotcleImportBuilder setParent(Object parent) {
        this.parent = parent;
        return this;
    }

    public ThesaurusImport.ChangeMotcleImport toChangeMotcleImport() {
        return new InternalChangeMotcleImport(motcle, newIdalpha, newStatus, parent, getLabelChange(), getAttributeChange(), getLiensImport());
    }


    private static class InternalChangeMotcleImport implements ThesaurusImport.ChangeMotcleImport {

        private final Motcle motcle;
        private final String newIdalpha;
        private final String newStatus;
        private final Object parent;
        private final LabelChange labelChange;
        private final AttributeChange attributeChange;
        private final LiensImport liensImport;

        private InternalChangeMotcleImport(Motcle motcle, String newIdalpha, String newStatus, Object parent, LabelChange labelChange, AttributeChange attributeChange, LiensImport liensImport) {
            this.motcle = motcle;
            this.newIdalpha = newIdalpha;
            this.newStatus = newStatus;
            this.parent = parent;
            this.labelChange = labelChange;
            this.attributeChange = attributeChange;
            this.liensImport = liensImport;
        }

        @Override
        public Motcle getMotcle() {
            return motcle;
        }

        @Override
        public String getNewIdalpha() {
            return newIdalpha;
        }

        @Override
        public Object getParent() {
            return parent;
        }

        @Override
        public String getNewStatus() {
            return newStatus;
        }

        @Override
        public LabelChange getLabelChange() {
            return labelChange;
        }

        @Override
        public AttributeChange getAttributeChange() {
            return attributeChange;
        }

        @Override
        public LiensImport getLiensImport() {
            return liensImport;
        }


    }

}
