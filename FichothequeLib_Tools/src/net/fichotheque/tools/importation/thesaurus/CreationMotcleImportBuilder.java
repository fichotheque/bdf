/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.thesaurus;

import net.fichotheque.importation.LiensImport;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public class CreationMotcleImportBuilder extends MotcleImportBuilder {

    private final int newId;
    private String newIdalpha = null;
    private int parentId = -1;
    private String parentIdalpha = null;
    private String newStatus = null;

    public CreationMotcleImportBuilder(int newId) {
        this.newId = newId;
    }

    public CreationMotcleImportBuilder setNewIdalpha(String newIdalpha) {
        this.newIdalpha = newIdalpha;
        return this;
    }

    public CreationMotcleImportBuilder setNewStatus(String newStatus) {
        this.newStatus = newStatus;
        return this;
    }

    public CreationMotcleImportBuilder setParentId(int parentId) {
        this.parentId = parentId;
        return this;
    }

    public CreationMotcleImportBuilder setParentIdalpha(String parentIdalpha) {
        this.parentIdalpha = parentIdalpha;
        return this;
    }

    public ThesaurusImport.CreationMotcleImport toCreationMotcleImport() {
        return new InternalCreationMotcleImport(newId, newIdalpha, parentId, parentIdalpha, newStatus, getLabelChange(), getAttributeChange(), getLiensImport());
    }

    public static CreationMotcleImportBuilder init(int newId) {
        return new CreationMotcleImportBuilder(newId);
    }


    private static class InternalCreationMotcleImport implements ThesaurusImport.CreationMotcleImport {

        private final int newId;
        private final String newIdalpha;
        private final int newParentId;
        private final String newParentIdalpha;
        private final String newStatus;
        private final LabelChange labelChange;
        private final AttributeChange attributeChange;
        private final LiensImport liensImport;

        private InternalCreationMotcleImport(int newId, String newIdalpha, int newParent, String newParentIdalpha, String newStatus, LabelChange labelChange, AttributeChange attributeChange, LiensImport liensImport) {
            this.newId = newId;
            this.newIdalpha = newIdalpha;
            this.newParentId = newParent;
            this.newParentIdalpha = newParentIdalpha;
            this.newStatus = newStatus;
            this.labelChange = labelChange;
            this.attributeChange = attributeChange;
            this.liensImport = liensImport;
        }

        @Override
        public Motcle getMotcle() {
            return null;
        }

        @Override
        public int getNewId() {
            return newId;
        }

        @Override
        public String getNewIdalpha() {
            return newIdalpha;
        }

        @Override
        public int getParentId() {
            return newParentId;
        }

        @Override
        public String getParentIdalpha() {
            return newParentIdalpha;
        }

        @Override
        public String getNewStatus() {
            return newStatus;
        }

        @Override
        public LabelChange getLabelChange() {
            return labelChange;
        }

        @Override
        public AttributeChange getAttributeChange() {
            return attributeChange;
        }

        @Override
        public LiensImport getLiensImport() {
            return liensImport;
        }

    }

}
