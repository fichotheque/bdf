/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.corpus;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.tools.importation.ImportationUtils;


/**
 *
 * @author Vincent Calame
 */
public class ChangeCorpusImportBuilder extends CorpusImportBuilder {

    private final Map<Integer, ChangeFicheImportBuilder> ficheMap = new LinkedHashMap<Integer, ChangeFicheImportBuilder>();

    public ChangeCorpusImportBuilder(ContentChecker contentChecker, Corpus corpus) {
        super(contentChecker, corpus, CorpusImport.CHANGE_TYPE);
    }

    public ChangeFicheImportBuilder getChangeFicheImportBuilder(FicheMeta ficheMeta) {
        ChangeFicheImportBuilder builder = ficheMap.get(ficheMeta.getId());
        if (builder == null) {
            builder = new ChangeFicheImportBuilder(ficheMeta);
            ficheMap.put(ficheMeta.getId(), builder);
        }
        return builder;
    }

    @Override
    protected List<CorpusImport.FicheImport> getFicheImportList() {
        int p = 0;
        CorpusImport.FicheImport[] array = new CorpusImport.FicheImport[ficheMap.size()];
        for (ChangeFicheImportBuilder builder : ficheMap.values()) {
            array[p] = builder.toChangeFicheImport();
            p++;
        }
        return ImportationUtils.wrap(array);
    }

    public static ChangeCorpusImportBuilder init(ContentChecker contentChecker, Corpus corpus) {
        return new ChangeCorpusImportBuilder(contentChecker, corpus);
    }

}
