/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.corpus;

import net.fichotheque.corpus.FicheChange;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.importation.LiensImport;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class ChangeFicheImportBuilder extends FicheImportBuilder {

    private final FicheMeta ficheMeta;

    public ChangeFicheImportBuilder(FicheMeta ficheMeta) {
        this.ficheMeta = ficheMeta;
    }

    public CorpusImport.ChangeFicheImport toChangeFicheImport() {
        return new InternalChangeFicheImport(ficheMeta, getFicheChange(), getAttributeChange(), getLiensImport(), creationDate);
    }

    public static ChangeFicheImportBuilder init(FicheMeta ficheMeta) {
        return new ChangeFicheImportBuilder(ficheMeta);
    }


    private static class InternalChangeFicheImport implements CorpusImport.ChangeFicheImport {

        private final FicheMeta ficheMeta;
        private final FicheChange ficheChange;
        private final AttributeChange attributeChange;
        private final LiensImport liensImport;
        private final FuzzyDate creationDate;

        private InternalChangeFicheImport(FicheMeta ficheMeta, FicheChange ficheChange, AttributeChange attributeChange, LiensImport liensImport, FuzzyDate creationDate) {
            this.ficheMeta = ficheMeta;
            this.ficheChange = ficheChange;
            this.attributeChange = attributeChange;
            this.liensImport = liensImport;
            this.creationDate = creationDate;
        }

        @Override
        public FicheMeta getFicheMeta() {
            return ficheMeta;
        }

        @Override
        public FicheChange getFicheChange() {
            return ficheChange;
        }

        @Override
        public AttributeChange getAttributeChange() {
            return attributeChange;
        }

        @Override
        public LiensImport getLiensImport() {
            return liensImport;
        }

        @Override
        public FuzzyDate getCreationDate() {
            return creationDate;
        }

    }

}
