/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.corpus;

import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.importation.CorpusImport;


/**
 *
 * @author Vincent Calame
 */
public abstract class CorpusImportBuilder {

    private final ContentChecker contentChecker;
    private final Corpus corpus;
    private final String type;

    protected CorpusImportBuilder(ContentChecker contentChecker, Corpus corpus, String type) {
        this.contentChecker = contentChecker;
        this.corpus = corpus;
        this.type = type;
    }

    public ContentChecker getContentChecker() {
        return contentChecker;
    }

    public String getType() {
        return type;
    }

    public Corpus getCorpus() {
        return corpus;
    }

    public static CorpusImportBuilder init(ContentChecker contentChecker, Corpus corpus, String type) {
        type = CorpusImport.checkType(type);
        switch (type) {
            case CorpusImport.CHANGE_TYPE:
                return new ChangeCorpusImportBuilder(contentChecker, corpus);
            case CorpusImport.CREATION_TYPE:
                return new CreationCorpusImportBuilder(contentChecker, corpus);
            case CorpusImport.REMOVE_TYPE:
                return new RemoveCorpusImportBuilder(corpus);
            default:
                throw new IllegalArgumentException("Unknown type: " + type);
        }
    }

    public CorpusImport toCorpusImport() {
        return new InternalCorpusImport(corpus, type, getFicheImportList());
    }

    protected abstract List<CorpusImport.FicheImport> getFicheImportList();


    private static class InternalCorpusImport implements CorpusImport {

        private final Corpus corpus;
        private final String type;
        private final List<FicheImport> ficheImportList;

        private InternalCorpusImport(Corpus corpus, String type, List<FicheImport> ficheImportList) {
            this.corpus = corpus;
            this.type = type;
            this.ficheImportList = ficheImportList;
        }

        @Override
        public Corpus getCorpus() {
            return corpus;
        }

        @Override
        public String getType() {
            return type;
        }

        @Override
        public List<FicheImport> getFicheImportList() {
            return ficheImportList;
        }

    }

}
