/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.corpus;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.tools.importation.ImportationUtils;


/**
 *
 * @author Vincent Calame
 */
public class RemoveCorpusImportBuilder extends CorpusImportBuilder {

    private final Set<FicheMeta> removeSet = new LinkedHashSet<FicheMeta>();

    public RemoveCorpusImportBuilder(Corpus corpus) {
        super(null, corpus, CorpusImport.REMOVE_TYPE);
    }

    public RemoveCorpusImportBuilder add(FicheMeta ficheMeta) {
        removeSet.add(ficheMeta);
        return this;
    }

    @Override
    protected List<CorpusImport.FicheImport> getFicheImportList() {
        int p = 0;
        CorpusImport.FicheImport[] array = new CorpusImport.FicheImport[removeSet.size()];
        for (FicheMeta ficheMeta : removeSet) {
            array[p] = new InternalFicheImport(ficheMeta);
            p++;
        }
        return ImportationUtils.wrap(array);
    }

    public static RemoveCorpusImportBuilder init(Corpus corpus) {
        return new RemoveCorpusImportBuilder(corpus);
    }


    private static class InternalFicheImport implements CorpusImport.FicheImport {

        private final FicheMeta ficheMeta;

        private InternalFicheImport(FicheMeta ficheMeta) {
            this.ficheMeta = ficheMeta;
        }

        @Override
        public FicheMeta getFicheMeta() {
            return ficheMeta;
        }

    }

}
