/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.corpus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.tools.importation.ImportationUtils;


/**
 *
 * @author Vincent Calame
 */
public class CreationCorpusImportBuilder extends CorpusImportBuilder {

    private final List<CreationFicheImportBuilder> builderList = new ArrayList<CreationFicheImportBuilder>();
    private final Map<Integer, CreationFicheImportBuilder> builderMap = new HashMap<Integer, CreationFicheImportBuilder>();

    public CreationCorpusImportBuilder(ContentChecker contentChecker, Corpus corpus) {
        super(contentChecker, corpus, CorpusImport.CREATION_TYPE);
    }

    public CreationFicheImportBuilder getCreationFicheImportBuilder(int newId) {
        CreationFicheImportBuilder builder;
        if (newId > 0) {
            builder = builderMap.get(newId);
            if (builder == null) {
                builder = new CreationFicheImportBuilder(newId);
                builderList.add(builder);
                builderMap.put(newId, builder);
            }
        } else {
            builder = new CreationFicheImportBuilder(-1);
            builderList.add(builder);
        }
        return builder;
    }

    @Override
    protected List<CorpusImport.FicheImport> getFicheImportList() {
        int p = 0;
        CorpusImport.FicheImport[] array = new CorpusImport.FicheImport[builderList.size()];
        for (CreationFicheImportBuilder builder : builderList) {
            array[p] = builder.toCreationFicheImport();
            p++;
        }
        return ImportationUtils.wrap(array);
    }

    public static CreationCorpusImportBuilder init(ContentChecker contentChecker, Corpus corpus) {
        return new CreationCorpusImportBuilder(contentChecker, corpus);
    }

}
