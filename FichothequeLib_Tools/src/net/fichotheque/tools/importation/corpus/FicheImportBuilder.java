/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.corpus;

import net.fichotheque.corpus.FicheChange;
import net.fichotheque.importation.LiensImport;
import net.fichotheque.tools.corpus.FicheChangeBuilder;
import net.fichotheque.tools.importation.ImportationUtils;
import net.fichotheque.tools.importation.LiensImportBuilder;
import net.fichotheque.utils.CorpusUtils;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public abstract class FicheImportBuilder {

    protected FicheChangeBuilder ficheChangeBuilder;
    protected AttributeChangeBuilder attributeChangeBuilder;
    protected LiensImportBuilder liensImportBuilder;
    protected FuzzyDate creationDate;

    public FicheImportBuilder() {

    }

    public FicheImportBuilder setCreationDate(FuzzyDate creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public FicheChangeBuilder getFicheChangeBuilder() {
        if (ficheChangeBuilder == null) {
            ficheChangeBuilder = new FicheChangeBuilder();
        }
        return ficheChangeBuilder;
    }

    public AttributeChangeBuilder getAttributeChangeBuilder() {
        if (attributeChangeBuilder == null) {
            attributeChangeBuilder = new AttributeChangeBuilder();
        }
        return attributeChangeBuilder;
    }

    public LiensImportBuilder getLiensImportBuilder() {
        if (liensImportBuilder == null) {
            liensImportBuilder = new LiensImportBuilder();
        }
        return liensImportBuilder;
    }

    protected FicheChange getFicheChange() {
        if (ficheChangeBuilder == null) {
            return CorpusUtils.EMPTY_FICHECHANGE;
        } else {
            return ficheChangeBuilder.toFicheChange();
        }
    }

    protected AttributeChange getAttributeChange() {
        if (attributeChangeBuilder == null) {
            return AttributeUtils.EMPTY_ATTRIBUTECHANGE;
        } else {
            return attributeChangeBuilder.toAttributeChange();
        }
    }

    protected LiensImport getLiensImport() {
        if (liensImportBuilder == null) {
            return ImportationUtils.EMPTY_LIENSIMPORT;
        } else {
            return liensImportBuilder.toLiensImport();
        }
    }

}
