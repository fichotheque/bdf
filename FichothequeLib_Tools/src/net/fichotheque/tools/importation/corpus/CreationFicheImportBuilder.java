/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.corpus;

import net.fichotheque.corpus.FicheChange;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.importation.LiensImport;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class CreationFicheImportBuilder extends FicheImportBuilder {

    private final int newId;

    public CreationFicheImportBuilder(int newId) {
        this.newId = newId;
    }

    public CorpusImport.CreationFicheImport toCreationFicheImport() {
        return new InternalCreationFicheImport(newId, getFicheChange(), getAttributeChange(), getLiensImport(), creationDate);
    }

    public static CreationFicheImportBuilder init(int newId) {
        return new CreationFicheImportBuilder(newId);
    }


    private static class InternalCreationFicheImport implements CorpusImport.CreationFicheImport {

        private final int newId;
        private final FicheChange ficheChange;
        private final AttributeChange attributeChange;
        private final LiensImport liensImport;
        private final FuzzyDate creationDate;

        private InternalCreationFicheImport(int newId, FicheChange ficheChange, AttributeChange attributeChange, LiensImport liensImport, FuzzyDate creationDate) {
            this.newId = newId;
            this.ficheChange = ficheChange;
            this.attributeChange = attributeChange;
            this.liensImport = liensImport;
            this.creationDate = creationDate;
        }

        @Override
        public int getNewId() {
            return newId;
        }

        @Override
        public FicheMeta getFicheMeta() {
            return null;
        }

        @Override
        public FicheChange getFicheChange() {
            return ficheChange;
        }

        @Override
        public AttributeChange getAttributeChange() {
            return attributeChange;
        }

        @Override
        public LiensImport getLiensImport() {
            return liensImport;
        }

        @Override
        public FuzzyDate getCreationDate() {
            return creationDate;
        }

    }

}
