/* FichothequeLib_Tools - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.directory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;
import java.util.SortedSet;
import java.util.TreeSet;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.tools.importation.corpus.ChangeFicheImportBuilder;
import net.fichotheque.tools.importation.ImportationUtils;
import net.fichotheque.tools.importation.Increment;
import net.fichotheque.tools.importation.dom.FicheImportDOMReader;
import net.fichotheque.xml.importation.CorpusImportXMLPart;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
class ChangeDirectoryCorpusImport extends DirectoryCorpusImport {

    private final ContentChecker contentChecker;
    private final Corpus corpus;
    private final File dir;
    private final List<String> fileList = new ArrayList<String>();
    private final Increment increment = new Increment();
    private final List<CorpusImport.FicheImport> ficheImportList = new FicheImportList();

    ChangeDirectoryCorpusImport(ContentChecker contentChecker, Corpus corpus, File dir) {
        this.contentChecker = contentChecker;
        this.corpus = corpus;
        this.dir = dir;
        File[] files = dir.listFiles();
        int length = files.length;
        SortedSet<String> sortedSet = new TreeSet<String>();
        for (int i = 0; i < length; i++) {
            File f = files[i];
            if (f.isDirectory()) {
                continue;
            }
            String name = f.getName();
            if (ImportationUtils.isValidFicheImportFileName(CorpusImport.CHANGE_TYPE, name)) {
                sortedSet.add(name);
            }
        }
        fileList.addAll(sortedSet);
        ImportationUtils.updateIncrement(CorpusImport.CHANGE_TYPE, fileList, increment);
    }

    @Override
    public Corpus getCorpus() {
        return corpus;
    }

    @Override
    public String getType() {
        return CorpusImport.CHANGE_TYPE;
    }

    @Override
    public List<FicheImport> getFicheImportList() {
        return ficheImportList;
    }

    @Override
    public void saveFicheImport(CorpusImport.FicheImport ficheImport) {
        CorpusImport.ChangeFicheImport changeFicheImport = (CorpusImport.ChangeFicheImport) ficheImport;
        StringBuilder buf = new StringBuilder();
        buf.append(CorpusImport.CHANGE_TYPE);
        buf.append('-');
        increment.appendTo(buf);
        buf.append(".xml");
        increment.increase();
        File f = new File(dir, buf.toString());
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "UTF-8"))) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(writer);
            xmlWriter.appendXMLDeclaration();
            CorpusImportXMLPart corpusImportXMLPart = new CorpusImportXMLPart(xmlWriter);
            corpusImportXMLPart.addChangeFicheImport(changeFicheImport);
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
    }

    @Override
    public void endSave() {
    }


    private class FicheImportList extends AbstractList<CorpusImport.FicheImport> implements RandomAccess {

        private FicheImportList() {

        }

        @Override
        public int size() {
            return fileList.size();
        }

        @Override
        public CorpusImport.FicheImport get(int index) {
            String fileName = fileList.get(index);
            File f = new File(dir, fileName);
            Element rootElement = DOMUtils.readDocument(f).getDocumentElement();
            String idString = rootElement.getAttribute("id");
            try {
                int id = Integer.parseInt(idString);
                FicheMeta ficheMeta = corpus.getFicheMetaById(id);
                if (ficheMeta != null) {
                    ChangeFicheImportBuilder ficheImportBuilder = new ChangeFicheImportBuilder(ficheMeta);
                    FicheImportDOMReader.init(ficheImportBuilder, corpus.getFichotheque(), contentChecker).read(rootElement);
                    return ficheImportBuilder.toChangeFicheImport();
                } else {
                    return null;
                }
            } catch (NumberFormatException nfe) {
                return null;
            }
        }

    }

}
