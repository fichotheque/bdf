/* FichothequeLib_Tools - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.directory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.importation.CorpusImport;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.exceptions.SwitchException;


/**
 *
 * @author Vincent Calame
 */
public abstract class DirectoryCorpusImport implements CorpusImport {

    public DirectoryCorpusImport() {
    }

    public abstract void saveFicheImport(FicheImport ficheImport);

    public abstract void endSave();

    public static DirectoryCorpusImport save(ContentChecker contentChecker, CorpusImport corpusImport, File dir) {
        String type = corpusImport.getType();
        Corpus corpus = corpusImport.getCorpus();
        File initFile = new File(dir, "init.txt");
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(initFile), "UTF-8"))) {
            writer.write(corpus.getSubsetKeyString());
            writer.write("\n");
            writer.write(type);
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        DirectoryCorpusImport directoryCorpusImport;
        if (type.equals(CorpusImport.CREATION_TYPE)) {
            directoryCorpusImport = new CreationDirectoryCorpusImport(contentChecker, corpus, dir);
        } else if (type.equals(CorpusImport.CHANGE_TYPE)) {
            directoryCorpusImport = new ChangeDirectoryCorpusImport(contentChecker, corpus, dir);
        } else if (type.equals(CorpusImport.REMOVE_TYPE)) {
            directoryCorpusImport = new RemoveDirectoryCorpusImport(corpus, dir);
        } else {
            throw new SwitchException("Unknown type: " + type);
        }
        for (CorpusImport.FicheImport ficheImport : corpusImport.getFicheImportList()) {
            directoryCorpusImport.saveFicheImport(ficheImport);
        }
        directoryCorpusImport.endSave();
        return directoryCorpusImport;
    }

    public static DirectoryCorpusImport load(Fichotheque fichotheque, ContentChecker contentChecker, File dir) {
        File initFile = new File(dir, "init.txt");
        if (!initFile.exists()) {
            return null;
        }
        Corpus corpus;
        String type;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(initFile), "UTF-8"))) {
            String corpusString = reader.readLine();
            try {
                SubsetKey corpusKey = SubsetKey.parse(corpusString);
                corpus = (Corpus) fichotheque.getSubset(corpusKey);
            } catch (ParseException pe) {
                return null;
            }
            type = reader.readLine();
            try {
                type = CorpusImport.checkType(type);
            } catch (IllegalArgumentException iae) {
                return null;
            }
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        if (type.equals(CorpusImport.CHANGE_TYPE)) {
            return new ChangeDirectoryCorpusImport(contentChecker, corpus, dir);
        } else if (type.equals(CorpusImport.CREATION_TYPE)) {
            return new CreationDirectoryCorpusImport(contentChecker, corpus, dir);
        } else if (type.equals(CorpusImport.REMOVE_TYPE)) {
            return new RemoveDirectoryCorpusImport(corpus, dir);
        } else {
            return null;
        }
    }

}
