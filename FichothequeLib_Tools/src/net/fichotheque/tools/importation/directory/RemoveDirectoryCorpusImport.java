/* FichothequeLib_Tools - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.directory;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.importation.CorpusImport;
import net.mapeadores.util.exceptions.NestedIOException;


/**
 *
 * @author Vincent Calame
 */
class RemoveDirectoryCorpusImport extends DirectoryCorpusImport {

    private final Corpus corpus;
    private final File dir;
    private final List<InternalFicheImport> list = new ArrayList<InternalFicheImport>();
    private final List<CorpusImport.FicheImport> ficheImportList = new FicheImportList();

    RemoveDirectoryCorpusImport(Corpus corpus, File dir) {
        this.corpus = corpus;
        this.dir = dir;
        File listFile = new File(dir, "remove-list.txt");
        if (listFile.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(listFile), "UTF-8"));
                String line;
                while ((line = reader.readLine()) != null) {
                    int idx = line.indexOf('=');
                    if (idx != -1) {
                        try {
                            int id = Integer.parseInt(line.substring(0, idx));
                            FicheMeta ficheMeta = corpus.getFicheMetaById(id);
                            if (ficheMeta != null) {
                                list.add(new InternalFicheImport(ficheMeta));
                            }
                        } catch (NumberFormatException nfe) {

                        }
                    }
                }
            } catch (IOException ioe) {
                throw new NestedIOException(ioe);
            }
        }
    }

    @Override
    public Corpus getCorpus() {
        return corpus;
    }

    @Override
    public String getType() {
        return CorpusImport.REMOVE_TYPE;
    }

    @Override
    public List<FicheImport> getFicheImportList() {
        return ficheImportList;
    }

    @Override
    public void saveFicheImport(CorpusImport.FicheImport ficheImport) {
        FicheMeta ficheMeta = ficheImport.getFicheMeta();
        if (ficheMeta == null) {
            return;
        }
        list.add(new InternalFicheImport(ficheMeta));
    }

    @Override
    public void endSave() {
        File listFile = new File(dir, "remove-list.txt");
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(listFile), "UTF-8"))) {
            for (InternalFicheImport ficheImport : list) {
                FicheMeta ficheMeta = ficheImport.getFicheMeta();
                writer.write(String.valueOf(ficheMeta.getId()));
                writer.write("=");
                escape(writer, ficheMeta.getTitre());
                writer.write("\n");
            }
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
    }

    private static void escape(Writer writer, String s) throws IOException {
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char carac = s.charAt(i);
            if (Character.isWhitespace(carac)) {
                writer.write(' ');
            } else {
                writer.write(carac);
            }
        }
    }


    private static class InternalFicheImport implements CorpusImport.FicheImport {

        private final FicheMeta ficheMeta;

        private InternalFicheImport(FicheMeta ficheMeta) {
            this.ficheMeta = ficheMeta;
        }

        @Override
        public FicheMeta getFicheMeta() {
            return ficheMeta;
        }

    }


    private class FicheImportList extends AbstractList<CorpusImport.FicheImport> implements RandomAccess {

        private FicheImportList() {

        }

        @Override
        public int size() {
            return list.size();
        }

        @Override
        public CorpusImport.FicheImport get(int index) {
            return list.get(index);
        }

    }

}
