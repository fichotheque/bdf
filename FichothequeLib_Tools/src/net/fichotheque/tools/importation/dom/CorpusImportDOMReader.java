/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.dom;

import java.util.function.Consumer;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.tools.importation.corpus.ChangeCorpusImportBuilder;
import net.fichotheque.tools.importation.corpus.ChangeFicheImportBuilder;
import net.fichotheque.tools.importation.corpus.CorpusImportBuilder;
import net.fichotheque.tools.importation.corpus.CreationCorpusImportBuilder;
import net.fichotheque.tools.importation.corpus.CreationFicheImportBuilder;
import net.fichotheque.tools.importation.corpus.RemoveCorpusImportBuilder;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class CorpusImportDOMReader {

    private final Corpus corpus;
    private final ContentChecker contentChecker;
    private final CorpusImportBuilder corpusImportBuilder;

    public CorpusImportDOMReader(CorpusImportBuilder corpusImportBuilder) {
        this.corpusImportBuilder = corpusImportBuilder;
        this.corpus = corpusImportBuilder.getCorpus();
        this.contentChecker = corpusImportBuilder.getContentChecker();
    }

    public CorpusImportDOMReader read(Element element) {
        Consumer<Element> elementConsumer;
        String type = corpusImportBuilder.getType();
        switch (type) {
            case CorpusImport.CHANGE_TYPE:
                elementConsumer = new ChangeConsumer((ChangeCorpusImportBuilder) corpusImportBuilder);
                break;
            case CorpusImport.CREATION_TYPE:
                elementConsumer = new CreationConsumer((CreationCorpusImportBuilder) corpusImportBuilder);
                break;
            case CorpusImport.REMOVE_TYPE:
                elementConsumer = new RemoveConsumer((RemoveCorpusImportBuilder) corpusImportBuilder);
                break;
            default:
                throw new SwitchException("Unknown type: " + type);
        }
        DOMUtils.readChildren(element, elementConsumer);
        return this;
    }

    public static CorpusImportDOMReader init(CorpusImportBuilder corpusImportBuilder) {
        return new CorpusImportDOMReader(corpusImportBuilder);
    }


    private class ChangeConsumer implements Consumer<Element> {

        private final ChangeCorpusImportBuilder changeCorpusImportBuilder;

        private ChangeConsumer(ChangeCorpusImportBuilder changeCorpusImportBuilder) {
            this.changeCorpusImportBuilder = changeCorpusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "ficheimport":
                    String idString = element.getAttribute("id");
                    try {
                        int id = Integer.parseInt(idString);
                        FicheMeta ficheMeta = corpus.getFicheMetaById(id);
                        if (ficheMeta != null) {
                            ChangeFicheImportBuilder ficheImportBuilder = changeCorpusImportBuilder.getChangeFicheImportBuilder(ficheMeta);
                            FicheImportDOMReader.init(ficheImportBuilder, corpus.getFichotheque(), contentChecker).read(element);
                        }
                    } catch (NumberFormatException nfe) {
                    }
                    break;
            }
        }

    }


    private class CreationConsumer implements Consumer<Element> {

        private final CreationCorpusImportBuilder creationCorpusImportBuilder;

        private CreationConsumer(CreationCorpusImportBuilder creationCorpusImportBuilder) {
            this.creationCorpusImportBuilder = creationCorpusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "ficheimport":
                    String idString = element.getAttribute("id");
                    int newId = -1;
                    if (idString.length() > 0) {
                        try {
                            newId = Integer.parseInt(idString);
                        } catch (NumberFormatException nfe) {
                        }
                    }
                    CreationFicheImportBuilder ficheImportBuilder = creationCorpusImportBuilder.getCreationFicheImportBuilder(newId);
                    FicheImportDOMReader.init(ficheImportBuilder, corpus.getFichotheque(), contentChecker).read(element);
                    break;
            }
        }

    }


    private class RemoveConsumer implements Consumer<Element> {

        private final RemoveCorpusImportBuilder removeCorpusImportBuilder;

        private RemoveConsumer(RemoveCorpusImportBuilder removeCorpusImportBuilder) {
            this.removeCorpusImportBuilder = removeCorpusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "ficheimport":
                    String idString = element.getAttribute("id");
                    try {
                        int id = Integer.parseInt(idString);
                        FicheMeta ficheMeta = corpus.getFicheMetaById(id);
                        if (ficheMeta != null) {
                            removeCorpusImportBuilder.add(ficheMeta);
                        }
                    } catch (NumberFormatException nfe) {
                    }
                    break;
            }
        }

    }

}
