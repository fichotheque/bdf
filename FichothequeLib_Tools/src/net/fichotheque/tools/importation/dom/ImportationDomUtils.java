/* FichothequeLib_Xml - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.importation.corpus.CorpusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.ThesaurusImportBuilder;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public final class ImportationDomUtils {

    private ImportationDomUtils() {

    }

    public static CorpusImportBuilder buildCorpusImportBuilder(Fichotheque fichotheque, ContentChecker contentChecker, Element element) {
        String corpusName = element.getAttribute("corpus");
        SubsetKey corpusKey;
        try {
            corpusKey = SubsetKey.parse(SubsetKey.CATEGORY_CORPUS, corpusName);
        } catch (ParseException pe) {
            return null;
        }
        Corpus corpus = (Corpus) fichotheque.getSubset(corpusKey);
        if (corpus == null) {
            return null;
        }
        String type = element.getAttribute("type");
        try {
            type = CorpusImport.checkType(type);
        } catch (IllegalArgumentException iae) {
            return null;
        }
        return CorpusImportBuilder.init(contentChecker, corpus, type);
    }

    public static ThesaurusImportBuilder buildThesaurusImportBuilder(Fichotheque fichotheque, Element element) {
        String thesaurusName = element.getAttribute("thesaurus");
        SubsetKey thesaurusKey;
        try {
            thesaurusKey = SubsetKey.parse(SubsetKey.CATEGORY_THESAURUS, thesaurusName);
        } catch (ParseException pe) {
            return null;
        }
        Thesaurus thesaurus = (Thesaurus) fichotheque.getSubset(thesaurusKey);
        if (thesaurus == null) {
            return null;
        }
        String type = element.getAttribute("type");
        try {
            type = ThesaurusImport.checkType(type);
        } catch (IllegalArgumentException iae) {
            return null;
        }
        Thesaurus destinationThesaurus = null;
        if ((type.equals(ThesaurusImport.MERGE_TYPE)) || (type.equals(ThesaurusImport.MOVE_TYPE))) {
            String destinationThesaurusName = element.getAttribute("destination");
            try {
                SubsetKey destinationSubsetKey = SubsetKey.parse(SubsetKey.CATEGORY_THESAURUS, destinationThesaurusName);
                destinationThesaurus = (Thesaurus) fichotheque.getSubset(destinationSubsetKey);
                if (destinationThesaurus == null) {
                    return null;
                }
            } catch (ParseException pe) {
                return null;
            }
        }
        return ThesaurusImportBuilder.init(thesaurus, type, destinationThesaurus);
    }

    public static void readLabelElement(Element element, LabelChangeBuilder labelChangeBuilder) {
        try {
            Lang lang = Lang.parse(element.getAttribute("xml:lang"));
            CleanedString libString = DOMUtils.contentToCleanedString(element);
            if (libString == null) {
                labelChangeBuilder.putRemovedLang(lang);
            } else {
                labelChangeBuilder.putLabel(lang, libString);
            }
        } catch (ParseException pe) {
        }
    }

    public static void readAttrElement(Element element, AttributeChangeBuilder attributeChangeBuilder) {
        AttributeKey attributeKey = AttributeUtils.readAttributeKey(element);
        if (attributeKey == null) {
            return;
        }
        AttributeConsumer attributeConsumer = new AttributeConsumer(attributeKey, attributeChangeBuilder);
        DOMUtils.readChildren(element, attributeConsumer);
        if (!attributeConsumer.isDone()) {
            attributeChangeBuilder.putRemovedAttributeKey(attributeKey);
        }
    }


    private static class AttributeConsumer implements Consumer<Element> {

        private final AttributeKey attributeKey;
        private final AttributeChangeBuilder attributeChangeBuilder;
        private boolean done = false;

        private AttributeConsumer(AttributeKey attributeKey, AttributeChangeBuilder attributeChangeBuilder) {
            this.attributeKey = attributeKey;
            this.attributeChangeBuilder = attributeChangeBuilder;
        }

        private boolean isDone() {
            return done;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("val")) {
                CleanedString cs = XMLUtils.toCleanedString(element);
                if (cs != null) {
                    attributeChangeBuilder.appendValue(attributeKey, cs);
                    done = true;
                }
            }
        }

    }


}
