/* FichothequeLib_Tools - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.fiche.ContentChecker;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.tools.corpus.FicheChangeBuilder;
import net.fichotheque.tools.dom.FicheDOMReader;
import net.fichotheque.tools.importation.corpus.FicheImportBuilder;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class FicheImportDOMReader {

    private final FicheImportBuilder ficheImportBuilder;
    private final Fichotheque fichotheque;
    private final ContentChecker contentChecker;

    public FicheImportDOMReader(FicheImportBuilder ficheImportBuilder, Fichotheque fichotheque, ContentChecker contentChecker) {
        this.ficheImportBuilder = ficheImportBuilder;
        this.fichotheque = fichotheque;
        this.contentChecker = contentChecker;
    }

    public FicheImportDOMReader read(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
        return this;
    }

    public static FicheImportDOMReader init(FicheImportBuilder ficheImportBuilder, Fichotheque fichotheque, ContentChecker contentChecker) {
        return new FicheImportDOMReader(ficheImportBuilder, fichotheque, contentChecker);
    }


    private class RootConsumer implements Consumer<Element> {

        private final FicheDOMReader ficheDOMReader;

        private RootConsumer() {
            ficheDOMReader = new FicheDOMReader(contentChecker);
            ficheDOMReader.setAppend(true);
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "fiche":
                    ficheImportBuilder.getFicheChangeBuilder().appendFiche(ficheDOMReader.readFiche(element));
                    break;
                case "attr":
                    ImportationDomUtils.readAttrElement(element, ficheImportBuilder.getAttributeChangeBuilder());
                    break;
                case "chrono":
                    String creationString = element.getAttribute("creation");
                    if (creationString.length() > 0) {
                        try {
                            ficheImportBuilder.setCreationDate(FuzzyDate.parse(creationString));
                        } catch (ParseException die) {

                        }
                    }
                    break;
                case "removed":
                    DOMUtils.readChildren(element, new RemovedConsumer(ficheImportBuilder.getFicheChangeBuilder()));
                    break;
                case "liens":
                    String type = element.getAttribute("type");
                    LiensImportDOMReader.init(fichotheque, ficheImportBuilder.getLiensImportBuilder(), type)
                            .read(element);
                    break;
            }
        }

    }


    private static class RemovedConsumer implements Consumer<Element> {

        private final FicheChangeBuilder ficheChangeBuilder;

        private RemovedConsumer(FicheChangeBuilder ficheChangeBuilder) {
            this.ficheChangeBuilder = ficheChangeBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "key": {
                    try {
                        FieldKey fieldKey = FieldKey.parse(XMLUtils.getData(element));
                        ficheChangeBuilder.removeField(fieldKey);
                    } catch (ParseException pe) {

                    }
                    break;
                }
            }
        }

    }

}
