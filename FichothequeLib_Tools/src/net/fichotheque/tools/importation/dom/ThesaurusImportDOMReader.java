/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.dom;

import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.importation.thesaurus.ChangeMotcleImportBuilder;
import net.fichotheque.tools.importation.thesaurus.ChangeThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.CreationMotcleImportBuilder;
import net.fichotheque.tools.importation.thesaurus.CreationThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.MergeThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.MoveThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.RemoveThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.ThesaurusImportBuilder;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusImportDOMReader {

    private final ThesaurusImportBuilder thesaurusImportBuilder;
    private final Thesaurus thesaurus;
    private final Fichotheque fichotheque;


    public ThesaurusImportDOMReader(ThesaurusImportBuilder thesaurusImportBuilder) {
        this.thesaurusImportBuilder = thesaurusImportBuilder;
        this.thesaurus = thesaurusImportBuilder.getThesaurus();
        this.fichotheque = thesaurus.getFichotheque();
    }

    public ThesaurusImportDOMReader read(Element element) {
        Consumer<Element> elementConsumer;
        String type = thesaurusImportBuilder.getType();
        switch (type) {
            case ThesaurusImport.CHANGE_TYPE:
                elementConsumer = new ChangeConsumer((ChangeThesaurusImportBuilder) thesaurusImportBuilder);
                break;
            case ThesaurusImport.CREATION_TYPE:
                elementConsumer = new CreationConsumer((CreationThesaurusImportBuilder) thesaurusImportBuilder);
                break;
            case ThesaurusImport.MERGE_TYPE:
                elementConsumer = new MergeConsumer((MergeThesaurusImportBuilder) thesaurusImportBuilder);
                break;
            case ThesaurusImport.MOVE_TYPE:
                elementConsumer = new MoveConsumer((MoveThesaurusImportBuilder) thesaurusImportBuilder);
                break;
            case ThesaurusImport.REMOVE_TYPE:
                elementConsumer = new RemoveConsumer((RemoveThesaurusImportBuilder) thesaurusImportBuilder);
                break;
            default:
                throw new SwitchException("Unknown type: " + type);
        }
        DOMUtils.readChildren(element, elementConsumer);
        return this;
    }


    public static ThesaurusImportDOMReader init(ThesaurusImportBuilder thesaurusImportBuilder) {
        return new ThesaurusImportDOMReader(thesaurusImportBuilder);
    }


    private class ChangeConsumer implements Consumer<Element> {

        private final ChangeThesaurusImportBuilder changeThesaurusImportBuilder;

        private ChangeConsumer(ChangeThesaurusImportBuilder changeThesaurusImportBuilder) {
            this.changeThesaurusImportBuilder = changeThesaurusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "motcleimport":
                    String idString = element.getAttribute("id");
                    try {
                        int id = Integer.parseInt(idString);
                        Motcle motcle = thesaurus.getMotcleById(id);
                        if (motcle != null) {
                            ChangeMotcleImportBuilder builder = changeThesaurusImportBuilder.getChangeMotcleImportBuilder(motcle);
                            DOMUtils.readChildren(element, new ChangeMotcleConsumer(builder));
                        }
                    } catch (NumberFormatException nfe) {
                    }
                    break;
            }
        }

    }


    private class CreationConsumer implements Consumer<Element> {

        private final CreationThesaurusImportBuilder creationThesaurusImportBuilder;

        private CreationConsumer(CreationThesaurusImportBuilder creationThesaurusImportBuilder) {
            this.creationThesaurusImportBuilder = creationThesaurusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "motcleimport":
                    int newId = -1;
                    String idString = element.getAttribute("id");
                    try {
                        newId = Integer.parseInt(idString);
                    } catch (NumberFormatException nfe) {
                    }
                    IdalphaConsumer idalphaConsumer = new IdalphaConsumer();
                    DOMUtils.readChildren(element, idalphaConsumer);
                    CreationMotcleImportBuilder creationMotcleImportBuilder = creationThesaurusImportBuilder.getCreationMotcleImportBuilder(newId, idalphaConsumer.idalpha);
                    DOMUtils.readChildren(element, new CreationMotcleConsumer(creationMotcleImportBuilder));
                    break;
            }
        }

    }


    private class MergeConsumer implements Consumer<Element> {

        private final MergeThesaurusImportBuilder mergeThesaurusImportBuilder;

        private MergeConsumer(MergeThesaurusImportBuilder mergeThesaurusImportBuilder) {
            this.mergeThesaurusImportBuilder = mergeThesaurusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "motcleimport":
                    String idString = element.getAttribute("id");
                    String destinationString = element.getAttribute("destination");
                    try {
                        int id = Integer.parseInt(idString);
                        int destinationId = Integer.parseInt(destinationString);
                        Motcle motcle = thesaurus.getMotcleById(id);
                        Motcle destinationMotcle = thesaurusImportBuilder.getDestinationThesaurus().getMotcleById(destinationId);
                        if ((motcle != null) && (destinationMotcle != null)) {
                            mergeThesaurusImportBuilder.add(motcle, destinationMotcle);
                        }
                    } catch (NumberFormatException nfe) {
                    }
                    break;
            }
        }

    }


    private class MoveConsumer implements Consumer<Element> {

        private final MoveThesaurusImportBuilder moveThesaurusImportBuilder;

        private MoveConsumer(MoveThesaurusImportBuilder moveThesaurusImportBuilder) {
            this.moveThesaurusImportBuilder = moveThesaurusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "motcleimport":
                    String idString = element.getAttribute("id");
                    try {
                        int id = Integer.parseInt(idString);
                        Motcle motcle = thesaurus.getMotcleById(id);
                        if (motcle != null) {
                            moveThesaurusImportBuilder.add(motcle);
                        }
                    } catch (NumberFormatException nfe) {
                    }
                    break;
            }
        }

    }


    private class RemoveConsumer implements Consumer<Element> {

        private final RemoveThesaurusImportBuilder removeThesaurusImportBuilder;

        private RemoveConsumer(RemoveThesaurusImportBuilder removeThesaurusImportBuilder) {
            this.removeThesaurusImportBuilder = removeThesaurusImportBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "motcleimport":
                    String idString = element.getAttribute("id");
                    try {
                        int id = Integer.parseInt(idString);
                        Motcle motcle = thesaurus.getMotcleById(id);
                        if (motcle != null) {
                            removeThesaurusImportBuilder.add(motcle);
                        }
                    } catch (NumberFormatException nfe) {
                    }
                    break;
            }
        }

    }


    private class ChangeMotcleConsumer implements Consumer<Element> {

        private final ChangeMotcleImportBuilder builder;

        private ChangeMotcleConsumer(ChangeMotcleImportBuilder builder) {
            this.builder = builder;
        }


        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "attr":
                    ImportationDomUtils.readAttrElement(element, builder.getAttributeChangeBuilder());
                    break;
                case "label":
                case "lib":
                    ImportationDomUtils.readLabelElement(element, builder.getLabelChangeBuilder());
                    break;
                case "idalpha":
                    CleanedString idalphaString = DOMUtils.contentToCleanedString(element);
                    if (idalphaString != null) {
                        builder.setNewIdalpha(idalphaString.toString());
                    }
                    break;
                case "parent":
                    String idString = element.getAttribute("id");
                    if (idString.length() == 0) {
                        builder.setParent(thesaurus);
                    } else {
                        try {
                            int parentId = Integer.parseInt(idString);
                            Motcle parentMotcle = thesaurus.getMotcleById(parentId);
                            if (parentMotcle != null) {
                                builder.setParent(parentMotcle);
                            }
                        } catch (NumberFormatException nfe) {
                        }
                    }
                    break;
                case "status":
                    String statusValue = DOMUtils.readSimpleElement(element);
                    try {
                        builder.setNewStatus(FichothequeConstants.checkMotcleStatus(statusValue));
                    } catch (IllegalArgumentException iae) {

                    }
                    break;
                case "liens":
                    String type = element.getAttribute("type");
                    LiensImportDOMReader.init(fichotheque, builder.getLiensImportBuilder(), type)
                            .read(element);
                    break;
            }
        }

    }


    private class IdalphaConsumer implements Consumer<Element> {

        private String idalpha = null;

        private IdalphaConsumer() {

        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "idalpha":
                    CleanedString idalphaString = DOMUtils.contentToCleanedString(element);
                    if (idalphaString != null) {
                        idalpha = idalphaString.toString();
                    }
                    break;
            }
        }

    }


    private class CreationMotcleConsumer implements Consumer<Element> {

        private final CreationMotcleImportBuilder builder;

        private CreationMotcleConsumer(CreationMotcleImportBuilder builder) {
            this.builder = builder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "attr":
                    ImportationDomUtils.readAttrElement(element, builder.getAttributeChangeBuilder());
                    break;
                case "label":
                case "lib":
                    ImportationDomUtils.readLabelElement(element, builder.getLabelChangeBuilder());
                    break;
                case "idalpha":
                    break;
                case "parent":
                    String idString = element.getAttribute("id");
                    int newParentId = -1;
                    if (idString.length() > 0) {
                        try {
                            newParentId = Integer.parseInt(idString);
                            if (newParentId < 1) {
                                newParentId = -1;
                            } else {
                                builder.setParentId(newParentId);
                            }
                        } catch (NumberFormatException nfe) {
                        }
                    }
                    if (newParentId == -1) {
                        String idalphaParent = element.getAttribute("idalpha");
                        if (idalphaParent.length() > 0) {
                            builder.setParentIdalpha(idalphaParent);
                        }
                    }
                    break;
                case "status":
                    String statusValue = DOMUtils.readSimpleElement(element);
                    try {
                        builder.setNewStatus(FichothequeConstants.checkMotcleStatus(statusValue));
                    } catch (IllegalArgumentException iae) {

                    }
                    break;
                case "liens":
                    String type = element.getAttribute("type");
                    LiensImportDOMReader.init(fichotheque, builder.getLiensImportBuilder(), type)
                            .read(element);
                    break;
            }
        }

    }

}
