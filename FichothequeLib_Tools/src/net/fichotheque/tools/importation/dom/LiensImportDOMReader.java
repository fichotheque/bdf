/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.Fichotheque;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.SubsetKey;
import net.fichotheque.importation.LiensImport;
import net.fichotheque.include.IncludeKey;
import net.fichotheque.tools.importation.ImportationUtils;
import net.fichotheque.tools.importation.LiensImportBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class LiensImportDOMReader {

    private final Fichotheque fichotheque;
    private final LiensImportBuilder liensImportBuilder;
    private final String type;

    public LiensImportDOMReader(Fichotheque fichotheque, LiensImportBuilder liensImportBuilder, String type) {
        this.fichotheque = fichotheque;
        this.liensImportBuilder = liensImportBuilder;
        this.type = type;
    }

    public LiensImportDOMReader read(Element element) {
        switch (type) {
            case "replace":
                DOMUtils.readChildren(element, new LienConsumer(true));
                break;
            case "append":
                DOMUtils.readChildren(element, new LienConsumer(false));
                break;
            case "remove":
                DOMUtils.readChildren(element, new IncludeKeyConsumer());
                break;
        }
        return this;
    }

    public static LiensImportDOMReader init(Fichotheque fichotheque, LiensImportBuilder liensImportBuilder, String type) {
        return new LiensImportDOMReader(fichotheque, liensImportBuilder, type);
    }


    private class IncludeKeyConsumer implements Consumer<Element> {


        private IncludeKeyConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "include-key":
                    String value = DOMUtils.readSimpleElement(element);
                    if (value.equals(FichothequeConstants.LIAGE_NAME)) {
                        liensImportBuilder.setLiageRemoved();
                    } else {
                        try {
                            IncludeKey includeKey = IncludeKey.parse(value);
                            liensImportBuilder.addRemovedIncludeKey(includeKey);
                        } catch (ParseException pe) {

                        }
                    }
                    break;
            }
        }

    }


    private class LienConsumer implements Consumer<Element> {

        private final boolean replace;

        private LienConsumer(boolean replace) {
            this.replace = replace;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "lien":
                    LiensImport.LienImport lienImport = null;
                    String origin = element.getAttribute("origin");
                    IncludeKey originIncludeKey;
                    if (origin.equals(FichothequeConstants.LIAGE_NAME)) {
                        originIncludeKey = null;
                    } else {
                        try {
                            originIncludeKey = IncludeKey.parse(origin);
                        } catch (ParseException pe) {
                            return;
                        }
                    }
                    int poids = 1;
                    try {
                        poids = Integer.parseInt(element.getAttribute("poids"));
                        if (poids < 1) {
                            poids = 1;
                        }
                    } catch (NumberFormatException nfe) {
                    }
                    SubsetKey subsetKey;
                    try {
                        subsetKey = SubsetKey.parse(element.getAttribute("subset"));
                    } catch (ParseException pe) {
                        return;
                    }
                    Subset subset = fichotheque.getSubset(subsetKey);
                    if (subset == null) {
                        return;
                    }
                    String idString = element.getAttribute("id");
                    if (idString.length() > 0) {
                        try {
                            int id = Integer.parseInt(idString);
                            SubsetItem subsetItem = subset.getSubsetItemById(id);
                            if (subsetItem != null) {
                                if (originIncludeKey != null) {
                                    lienImport = ImportationUtils.toLienImport(originIncludeKey, subsetItem, poids);
                                } else {
                                    lienImport = ImportationUtils.toLiageLienImport(subsetItem, poids);
                                }
                            }
                        } catch (NumberFormatException nfe) {
                        }
                    } else if (originIncludeKey != null) {
                        try {
                            Lang lang = Lang.parse(element.getAttribute("lang"));
                            CleanedString labelString = CleanedString.newInstance(element.getAttribute("label"));
                            if (labelString != null) {
                                lienImport = ImportationUtils.toLienImport(originIncludeKey, subset, LabelUtils.toLabel(lang, labelString), poids);
                            }
                        } catch (ParseException lie) {
                        }
                    }
                    if (lienImport != null) {
                        if (replace) {
                            liensImportBuilder.addReplaceLienImport(lienImport);
                        } else {
                            liensImportBuilder.addAppendLienImport(lienImport);
                        }
                    }
                    break;
            }
        }

    }

}
