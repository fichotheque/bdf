/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers;

import java.text.ParseException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.importation.ParseErrorKeys;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.corpus.RemoveCorpusImportBuilder;


/**
 *
 * @author Vincent Calame
 */
class CorpusRemoveRowParser extends RowParser {

    private final RemoveCorpusImportBuilder removeCorpusImportBuilder;
    private final ParseResultBuilder parseResultBuilder;
    private final int idIndex;

    CorpusRemoveRowParser(RemoveCorpusImportBuilder removeCorpusImportBuilder, ParseResultBuilder parseResultBuilder, int idIndex) {
        this.removeCorpusImportBuilder = removeCorpusImportBuilder;
        this.parseResultBuilder = parseResultBuilder;
        this.idIndex = idIndex;
    }

    @Override
    public void parseRow(int rowNumber, Row row) {
        if (row.getColumnCount() <= idIndex) {
            parseResultBuilder.addParseError(ParseErrorKeys.MISSING_ID_COLUMN, rowNumber);
            return;
        }
        String idString = row.getColumnValue(idIndex).trim();
        Corpus corpus = removeCorpusImportBuilder.getCorpus();
        FicheMeta ficheMeta = null;
        try {
            int id = Integer.parseInt(idString);
            ficheMeta = corpus.getFicheMetaById(id);
            if (ficheMeta == null) {
                parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_ID, idString, rowNumber);
            }
        } catch (NumberFormatException nfe) {
            parseResultBuilder.addBdfError(ParseErrorKeys.NOT_INTEGER, idString, rowNumber);
        }
        if (ficheMeta != null) {
            removeCorpusImportBuilder.add(ficheMeta);
        }
    }

    static CorpusRemoveRowParser newInstance(String[] fieldArray, RemoveCorpusImportBuilder removeCorpusImportBuilder, ParseResultBuilder parseResultBuilder) {
        int fieldIndex = -1;
        int length = fieldArray.length;
        for (int i = 0; i < length; i++) {
            String field = fieldArray[i].trim();
            FieldKey fieldKey;
            try {
                fieldKey = FieldKey.parse(field);
            } catch (ParseException pe) {
                continue;
            }
            if (isIdField(fieldKey)) {
                if (fieldIndex == -1) {
                    fieldIndex = i;
                }
            }
        }
        if (fieldIndex == -1) {
            parseResultBuilder.addInitError(ParseErrorKeys.MISSING_FIELDS_INIT, FieldKey.SPECIAL_ID);
        }
        if (fieldIndex != -1) {
            return new CorpusRemoveRowParser(removeCorpusImportBuilder, parseResultBuilder, fieldIndex);
        } else {
            return null;
        }
    }

    private static boolean isIdField(FieldKey fieldKey) {
        switch (fieldKey.getKeyString()) {
            case FieldKey.SPECIAL_ID:
                return true;
            default:
                return false;
        }
    }

}
