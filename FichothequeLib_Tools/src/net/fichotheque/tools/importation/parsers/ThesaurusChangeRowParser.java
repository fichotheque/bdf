/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers;

import net.fichotheque.FichothequeConstants;
import net.fichotheque.importation.ParseErrorKeys;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusFieldKey;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.parsers.handlers.HandlerUtils;
import net.fichotheque.tools.importation.parsers.handlers.IdalphaHandler;
import net.fichotheque.tools.importation.thesaurus.ChangeMotcleImportBuilder;
import net.fichotheque.tools.importation.thesaurus.ChangeThesaurusImportBuilder;


/**
 *
 * @author Vincent Calame
 */
class ThesaurusChangeRowParser extends RowParser {

    private final ChangeThesaurusImportBuilder changeThesaurusImportBuilder;
    private final ParseResultBuilder parseResultBuilder;
    private final ThesaurusColumns thesaurusColumns;

    ThesaurusChangeRowParser(ChangeThesaurusImportBuilder changeThesaurusImportBuilder, ParseResultBuilder parseResultBuilder, ThesaurusColumns thesaurusColumns) {
        this.changeThesaurusImportBuilder = changeThesaurusImportBuilder;
        this.parseResultBuilder = parseResultBuilder;
        this.thesaurusColumns = thesaurusColumns;
    }

    @Override
    public void parseRow(int rowNumber, Row row) {
        Thesaurus thesaurus = changeThesaurusImportBuilder.getThesaurus();
        int columnCount = row.getColumnCount();
        int idIndex = thesaurusColumns.getIdIndex();
        IdalphaHandler idalphaHandler = thesaurusColumns.getIdalphaHandler();
        Motcle motcle = null;
        if (idIndex != -1) {
            if (columnCount <= idIndex) {
                parseResultBuilder.addParseError(ParseErrorKeys.MISSING_ID_COLUMN, rowNumber);
                return;
            }
            String idString = row.getColumnValue(idIndex).trim();
            try {
                int id = Integer.parseInt(idString);
                motcle = thesaurus.getMotcleById(id);
                if (motcle == null) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_ID, idString, rowNumber);
                }
            } catch (NumberFormatException nfe) {
                parseResultBuilder.addBdfError(ParseErrorKeys.NOT_INTEGER, idString, rowNumber);
            }
        } else {
            try {
                String idalpha = idalphaHandler.getIdalpha(row, true);
                motcle = thesaurus.getMotcleByIdalpha(idalpha);
                if (motcle == null) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_IDALPHA, idalpha, rowNumber);
                }
            } catch (BdfErrorException bee) {
                parseResultBuilder.addBdfError(bee.getKey(), bee.getKey(), rowNumber);
            }
        }
        if (motcle == null) {
            return;
        }
        ChangeMotcleImportBuilder motcleImportBuilder = changeThesaurusImportBuilder.getChangeMotcleImportBuilder(motcle);
        String newIdalpha = null;
        if ((idIndex != -1) && (idalphaHandler != null)) {
            try {
                newIdalpha = idalphaHandler.getIdalpha(row, false);
                if ((newIdalpha != null) && (thesaurus.getMotcleByIdalpha(newIdalpha) != null)) {
                    newIdalpha = null;
                    parseResultBuilder.addBdfError(ParseErrorKeys.EXISTING_IDALPHA, newIdalpha, rowNumber);
                }
            } catch (BdfErrorException bee) {
                parseResultBuilder.addBdfError(bee.getKey(), bee.getText(), rowNumber);
            }
        }
        Object parent = null;
        int parentIdIndex = thesaurusColumns.getParentIdIndex();
        IdalphaHandler parentIdalphaHandler = thesaurusColumns.getParentIdalphaHandler();
        if (parentIdIndex != -1) {
            if (parentIdIndex < columnCount) {
                String parentIdString = row.getColumnValue(parentIdIndex).trim();
                if (parentIdString.length() == 0) {
                    parent = thesaurus;
                } else {
                    try {
                        int parentId = Integer.parseInt(parentIdString);
                        if (parentId < 1) {
                            parent = thesaurus;
                        } else {
                            Motcle parentMotcle = thesaurus.getMotcleById(parentId);
                            if (parentMotcle == null) {
                                parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_ID, parentIdString, rowNumber);
                            } else {
                                parent = parentMotcle;
                            }
                        }
                    } catch (NumberFormatException nfe) {
                        parseResultBuilder.addBdfError(ParseErrorKeys.NOT_INTEGER, parentIdString, rowNumber);
                    }
                }
            } else {
                parent = thesaurus;
            }
        } else if (parentIdalphaHandler != null) {
            try {
                String parentIdalpha = parentIdalphaHandler.getIdalpha(row, false);
                if (parentIdalpha == null) {
                    parent = thesaurus;
                } else {
                    Motcle parentMotcle = thesaurus.getMotcleByIdalpha(parentIdalpha);
                    if (parentMotcle == null) {
                        parseResultBuilder.addBdfError(ParseErrorKeys.EXISTING_IDALPHA, newIdalpha, rowNumber);
                    } else {
                        parent = parentMotcle;
                    }
                }
            } catch (BdfErrorException bee) {
                String key = bee.getKey();
                if (key.equals(ParseErrorKeys.MALFORMED_IDALPHA)) {
                    parseResultBuilder.addBdfError(key, bee.getText(), rowNumber);
                } else {
                    parent = thesaurus;
                }
            }
        }
        int statusIndex = thesaurusColumns.getStatusIndex();
        if (statusIndex != -1) {
            String statusString = row.getColumnValue(statusIndex).trim();
            if (!statusString.isEmpty()) {
                try {
                    motcleImportBuilder.setNewStatus(FichothequeConstants.checkMotcleStatus(statusString));
                } catch (IllegalArgumentException iae) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_STATUS, statusString, rowNumber);
                }
            }
        }
        motcleImportBuilder.setNewIdalpha(newIdalpha).setParent(parent);
        if (thesaurusColumns.hasLabelHandler()) {
            HandlerUtils.populate(motcleImportBuilder.getLabelChangeBuilder(), thesaurusColumns.getLabelHandlerArray(), row, parseResultBuilder, rowNumber);
        }
        if (thesaurusColumns.hasAttributeHandler()) {
            HandlerUtils.populate(motcleImportBuilder.getAttributeChangeBuilder(), thesaurusColumns.getAttributeHandlerArray(), row);
        }
        if (thesaurusColumns.hasCroisementHandler()) {
            HandlerUtils.populate(motcleImportBuilder.getLiensImportBuilder(), thesaurusColumns.getCroisementHandlers(), row, thesaurus, null, null, null);
        }
    }

    static ThesaurusChangeRowParser newInstance(String[] fieldArray, ChangeThesaurusImportBuilder changeThesaurusImportBuilder, ParseResultBuilder parseResultBuilder) {
        ThesaurusColumns thesaurusColumns = ThesaurusColumns.parse(fieldArray, changeThesaurusImportBuilder, parseResultBuilder);
        if ((thesaurusColumns.getIdIndex() == -1) && (thesaurusColumns.getIdalphaHandler() == null)) {
            parseResultBuilder.addInitError(ParseErrorKeys.MISSING_FIELDS_INIT, ThesaurusFieldKey.ID.toString());
            return null;
        }
        return new ThesaurusChangeRowParser(changeThesaurusImportBuilder, parseResultBuilder, thesaurusColumns);
    }

}
