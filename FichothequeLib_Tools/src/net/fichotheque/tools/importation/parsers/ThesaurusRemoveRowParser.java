/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers;

import java.text.ParseException;
import net.fichotheque.importation.ParseErrorKeys;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusFieldKey;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.thesaurus.RemoveThesaurusImportBuilder;


/**
 *
 * @author Vincent Calame
 */
class ThesaurusRemoveRowParser extends RowParser {

    private final int fieldIndex;
    private final boolean isIdalpha;
    private final RemoveThesaurusImportBuilder removeThesaurusImportBuilder;
    private final ParseResultBuilder parseResultBuilder;

    private ThesaurusRemoveRowParser(RemoveThesaurusImportBuilder removeThesaurusImportBuilder, ParseResultBuilder parseResultBuilder, int fieldIndex, boolean isIdalpha) {
        this.removeThesaurusImportBuilder = removeThesaurusImportBuilder;
        this.parseResultBuilder = parseResultBuilder;
        this.fieldIndex = fieldIndex;
        this.isIdalpha = isIdalpha;
    }

    @Override
    public void parseRow(int rowNumber, Row row) {
        if (row.getColumnCount() <= fieldIndex) {
            parseResultBuilder.addParseError(ParseErrorKeys.MISSING_ID_COLUMN, rowNumber);
            return;
        }
        String idString = row.getColumnValue(fieldIndex).trim();
        Thesaurus thesaurus = removeThesaurusImportBuilder.getThesaurus();
        Motcle motcle = null;
        if (isIdalpha) {
            motcle = thesaurus.getMotcleByIdalpha(idString);
            if (motcle == null) {
                parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_IDALPHA, idString, rowNumber);
            }
        } else {
            try {
                int id = Integer.parseInt(idString);
                motcle = thesaurus.getMotcleById(id);
                if (motcle == null) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_ID, idString, rowNumber);
                }
            } catch (NumberFormatException nfe) {
                parseResultBuilder.addBdfError(ParseErrorKeys.NOT_INTEGER, idString, rowNumber);
            }
        }
        if (motcle != null) {
            removeThesaurusImportBuilder.add(motcle);
        }
    }

    static ThesaurusRemoveRowParser newInstance(String[] fieldArray, RemoveThesaurusImportBuilder removeThesaurusImportBuilder, ParseResultBuilder parseResultBuilder) {
        int fieldIndex = -1;
        int idalphaIndex = -1;
        int length = fieldArray.length;
        for (int i = 0; i < length; i++) {
            String field = fieldArray[i].trim();
            ThesaurusFieldKey thesaurusFieldKey;
            try {
                thesaurusFieldKey = ThesaurusFieldKey.parse(field);
            } catch (ParseException pe) {
                continue;
            }
            if (thesaurusFieldKey.equals(ThesaurusFieldKey.ID)) {
                if (fieldIndex == -1) {
                    fieldIndex = i;
                }
            } else if (thesaurusFieldKey.equals(ThesaurusFieldKey.IDALPHA)) {
                if (idalphaIndex == -1) {
                    idalphaIndex = i;
                }
            }
        }
        boolean isIdalpha = false;
        if (fieldIndex == -1) {
            if (removeThesaurusImportBuilder.getThesaurus().isIdalphaType()) {
                if (idalphaIndex != -1) {
                    fieldIndex = idalphaIndex;
                    isIdalpha = true;
                } else {
                    parseResultBuilder.addInitError(ParseErrorKeys.MISSING_FIELDS_INIT, "(" + ThesaurusFieldKey.ID.toString() + " || " + ThesaurusFieldKey.IDALPHA.toString() + ")");
                }
            } else {
                parseResultBuilder.addInitError(ParseErrorKeys.MISSING_FIELDS_INIT, ThesaurusFieldKey.ID.toString());
            }
        }
        if (fieldIndex != -1) {
            return new ThesaurusRemoveRowParser(removeThesaurusImportBuilder, parseResultBuilder, fieldIndex, isIdalpha);
        } else {
            return null;
        }
    }

}
