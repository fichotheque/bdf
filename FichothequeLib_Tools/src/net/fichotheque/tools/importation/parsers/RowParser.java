/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers;

import net.fichotheque.importation.CorpusImport;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.metadata.ThesaurusLangChecker;
import net.fichotheque.thesaurus.policies.PolicyProvider;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.corpus.ChangeCorpusImportBuilder;
import net.fichotheque.tools.importation.corpus.CorpusImportBuilder;
import net.fichotheque.tools.importation.corpus.CreationCorpusImportBuilder;
import net.fichotheque.tools.importation.corpus.RemoveCorpusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.ChangeThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.CreationThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.MergeThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.MoveThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.RemoveThesaurusImportBuilder;
import net.fichotheque.tools.importation.thesaurus.ThesaurusImportBuilder;
import net.fichotheque.tools.parsers.FicheParser;
import net.mapeadores.util.exceptions.SwitchException;


/**
 *
 * @author Vincent Calame
 */
public abstract class RowParser {

    public abstract void parseRow(int rowNumber, Row row);

    public static RowParser newThesaurusRowParser(String[] fieldArray, ThesaurusImportBuilder thesaurusImportBuilder, ParseResultBuilder parseResultBuilder) {
        String type = thesaurusImportBuilder.getType();
        if (type.equals(ThesaurusImport.CREATION_TYPE)) {
            return ThesaurusCreationRowParser.newInstance(fieldArray, (CreationThesaurusImportBuilder) thesaurusImportBuilder, parseResultBuilder);
        } else if (type.equals(ThesaurusImport.CHANGE_TYPE)) {
            return ThesaurusChangeRowParser.newInstance(fieldArray, (ChangeThesaurusImportBuilder) thesaurusImportBuilder, parseResultBuilder);
        } else if (type.equals(ThesaurusImport.REMOVE_TYPE)) {
            return ThesaurusRemoveRowParser.newInstance(fieldArray, (RemoveThesaurusImportBuilder) thesaurusImportBuilder, parseResultBuilder);
        } else if (type.equals(ThesaurusImport.MERGE_TYPE)) {
            return ThesaurusMergeRowParser.newInstance(fieldArray, (MergeThesaurusImportBuilder) thesaurusImportBuilder, parseResultBuilder);
        } else if (type.equals(ThesaurusImport.MOVE_TYPE)) {
            return ThesaurusMoveRowParser.newInstance(fieldArray, (MoveThesaurusImportBuilder) thesaurusImportBuilder, parseResultBuilder);
        } else {
            throw new SwitchException("Unknown type: " + type);
        }
    }

    public static RowParser newCorpusRowParser(String[] fieldArray, CorpusImportBuilder corpusImportBuilder, ParseResultBuilder parseResultBuilder, FicheParser.Parameters ficheParserParameters, PolicyProvider policyProvider, ThesaurusLangChecker thesaurusLangChecker) {
        String type = corpusImportBuilder.getType();
        if (type.equals(CorpusImport.CREATION_TYPE)) {
            return CorpusCreationRowParser.newInstance(fieldArray, (CreationCorpusImportBuilder) corpusImportBuilder, parseResultBuilder, ficheParserParameters, policyProvider, thesaurusLangChecker);
        } else if (type.equals(CorpusImport.CHANGE_TYPE)) {
            return CorpusChangeRowParser.newInstance(fieldArray, (ChangeCorpusImportBuilder) corpusImportBuilder, parseResultBuilder, ficheParserParameters, policyProvider, thesaurusLangChecker);
        } else if (type.equals(CorpusImport.REMOVE_TYPE)) {
            return CorpusRemoveRowParser.newInstance(fieldArray, (RemoveCorpusImportBuilder) corpusImportBuilder, parseResultBuilder);
        } else {
            throw new SwitchException("Unknown type: " + type);
        }
    }

}
