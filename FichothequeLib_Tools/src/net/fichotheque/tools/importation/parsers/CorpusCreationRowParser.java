/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers;

import java.text.ParseException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.importation.ParseErrorKeys;
import net.fichotheque.thesaurus.metadata.ThesaurusLangChecker;
import net.fichotheque.thesaurus.policies.PolicyProvider;
import net.fichotheque.tools.importation.corpus.CreationCorpusImportBuilder;
import net.fichotheque.tools.importation.corpus.CreationFicheImportBuilder;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.parsers.handlers.HandlerUtils;
import net.fichotheque.tools.parsers.FicheParser;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class CorpusCreationRowParser extends RowParser {

    private final CreationCorpusImportBuilder creationCorpusImportBuilder;
    private final ParseResultBuilder parseResultBuilder;
    private final FicheParser.Parameters ficheParserParameters;
    private final CorpusColumns corpusColumns;
    private final PolicyProvider policyProvider;
    private final ThesaurusLangChecker thesaurusLangChecker;

    CorpusCreationRowParser(CreationCorpusImportBuilder creationCorpusImportBuilder, ParseResultBuilder parseResultBuilder, FicheParser.Parameters ficheParserParameters, CorpusColumns corpusColumns, PolicyProvider policyProvider, ThesaurusLangChecker thesaurusLangChecker) {
        this.creationCorpusImportBuilder = creationCorpusImportBuilder;
        this.parseResultBuilder = parseResultBuilder;
        this.ficheParserParameters = ficheParserParameters;
        this.corpusColumns = corpusColumns;
        this.policyProvider = policyProvider;
        this.thesaurusLangChecker = thesaurusLangChecker;
    }

    @Override
    public void parseRow(int rowNumber, Row row) {
        int idIndex = corpusColumns.getIdIndex();
        int columnCount = row.getColumnCount();
        Corpus corpus = creationCorpusImportBuilder.getCorpus();
        int newId = -1;
        if ((idIndex != -1) && (columnCount > idIndex)) {
            String id = row.getColumnValue(idIndex).trim();
            try {
                newId = Integer.parseInt(id);
                FicheMeta ficheMeta = corpus.getFicheMetaById(newId);
                if (ficheMeta != null) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.EXISTING_ID, id, rowNumber);
                    return;
                }
            } catch (NumberFormatException nfe) {
                parseResultBuilder.addBdfError(ParseErrorKeys.NOT_INTEGER, id, rowNumber);
                return;
            }
        }
        CreationFicheImportBuilder ficheImportBuilder = creationCorpusImportBuilder.getCreationFicheImportBuilder(newId);
        if (corpusColumns.hasFicheHandler()) {
            HandlerUtils.populate(ficheImportBuilder.getFicheChangeBuilder(), corpusColumns.getFicheHandlerArray(), row, corpus, ficheParserParameters);
        }
        if (corpusColumns.hasAttributeHandler()) {
            HandlerUtils.populate(ficheImportBuilder.getAttributeChangeBuilder(), corpusColumns.getAttributeHandlerArray(), row);
        }
        if (corpusColumns.hasCroisementHandler()) {
            HandlerUtils.populate(ficheImportBuilder.getLiensImportBuilder(), corpusColumns.getCroisementHandlers(), row, corpus, ficheParserParameters.getWorkingLang(), policyProvider, thesaurusLangChecker);
        }
        int creationDateIndex = corpusColumns.getCreationDateIndex();
        if ((creationDateIndex != -1) && (columnCount > creationDateIndex)) {
            String creationDateString = row.getColumnValue(creationDateIndex).trim();
            try {
                FuzzyDate creationDate = FuzzyDate.parse(creationDateString);
                ficheImportBuilder.setCreationDate(creationDate);
            } catch (ParseException die) {
                parseResultBuilder.addBdfError(ParseErrorKeys.NOT_DATE, creationDateString, rowNumber);
            }
        }
    }

    static CorpusCreationRowParser newInstance(String[] fieldArray, CreationCorpusImportBuilder corpusImportBuilder, ParseResultBuilder parseResultBuilder, FicheParser.Parameters ficheParserParameters, PolicyProvider policyProvider, ThesaurusLangChecker thesaurusLangChecker) {
        CorpusColumns corpusColumns = CorpusColumns.parse(fieldArray, corpusImportBuilder, parseResultBuilder);
        return new CorpusCreationRowParser(corpusImportBuilder, parseResultBuilder, ficheParserParameters, corpusColumns, policyProvider, thesaurusLangChecker);
    }

}
