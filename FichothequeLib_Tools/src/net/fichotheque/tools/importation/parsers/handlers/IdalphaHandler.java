/* BdfServer - Copyright (c) 2015-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers.handlers;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.importation.ParseErrorKeys;
import net.fichotheque.tools.importation.parsers.BdfErrorException;
import net.fichotheque.tools.importation.parsers.Row;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Idalpha;


/**
 *
 * @author Vincent Calame
 */
public class IdalphaHandler {

    private int uniqueIndex = -1;
    private List<Integer> indexList;

    public IdalphaHandler() {

    }

    public boolean addIndex(int index) {
        if (index == - 1) {
            this.uniqueIndex = index;
        } else {
            this.uniqueIndex = -2;
            if (indexList == null) {
                indexList = new ArrayList<Integer>();
            }
            indexList.add(index);
        }
        return true;
    }

    public String getIdalpha(Row row, boolean errorIfEmpty) throws BdfErrorException {
        int columnCount = row.getColumnCount();
        CleanedString cs = null;
        if (uniqueIndex > -1) {
            if (uniqueIndex < columnCount) {
                cs = CleanedString.newInstance(row.getColumnValue(uniqueIndex));
            }
        } else {
            StringBuilder buf = new StringBuilder();
            int indexCount = indexList.size();
            for (int i = 0; i < indexCount; i++) {
                int index = indexList.get(i);
                if (index < columnCount) {
                    String val = row.getColumnValue(index);
                    if (val != null) {
                        buf.append(val);
                    }
                }
            }
            cs = CleanedString.newInstance(buf.toString());
        }
        if (cs == null) {
            if (errorIfEmpty) {
                throw new BdfErrorException(ParseErrorKeys.EMPTY_IDALPHA, "");
            } else {
                return null;
            }
        }
        String idalpha = cs.toString();
        if (!Idalpha.isValid(idalpha)) {
            throw new BdfErrorException(ParseErrorKeys.MALFORMED_IDALPHA, idalpha);
        }
        return idalpha;
    }

}
