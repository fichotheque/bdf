/* BdfServer - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers.handlers;

import net.fichotheque.Subset;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.thesaurus.metadata.ThesaurusLangChecker;
import net.fichotheque.thesaurus.policies.PolicyProvider;
import net.fichotheque.tools.corpus.FicheChangeBuilder;
import net.fichotheque.tools.importation.LiensImportBuilder;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.parsers.BdfErrorException;
import net.fichotheque.tools.importation.parsers.LiensParser;
import net.fichotheque.tools.importation.parsers.Row;
import net.fichotheque.tools.parsers.FicheParser;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelChangeBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class HandlerUtils {

    private HandlerUtils() {

    }

    public static void populate(LabelChangeBuilder labelChangeBuilder, LabelHandler[] labelHandlerArray, Row row, ParseResultBuilder parseResultBuilder, int rowNumber) {
        for (LabelHandler labelHandler : labelHandlerArray) {
            try {
                labelHandler.handle(row, labelChangeBuilder);
            } catch (BdfErrorException bee) {
                parseResultBuilder.addBdfError(bee.getKey(), bee.getText(), rowNumber);
            }
        }
    }

    public static void populate(AttributeChangeBuilder attributeChangeBuilder, AttributeHandler[] attributeHandlerArray, Row row) {
        for (AttributeHandler attributeHandler : attributeHandlerArray) {
            attributeHandler.handle(row, attributeChangeBuilder);
        }
    }

    public static void populate(LiensImportBuilder liensImportBuilder, CroisementHandlers croisementHandlers, Row row, Subset subset, Lang lang, PolicyProvider policyProvider, ThesaurusLangChecker thesaurusLangChecker) {
        LiensParser liensParser = new LiensParser(subset, liensImportBuilder, lang, policyProvider, thesaurusLangChecker);
        croisementHandlers.handle(row, liensParser);
    }

    public static void populate(FicheChangeBuilder ficheChangeBuilder, FicheHandler[] ficheHandlerArray, Row row, Corpus corpus, FicheParser.Parameters ficheParserParameters) {
        FicheParser ficheParser = new FicheParser(ficheParserParameters, corpus, ficheChangeBuilder);
        if (ficheHandlerArray != null) {
            for (FicheHandler ficheHandler : ficheHandlerArray) {
                ficheHandler.handle(row, ficheParser);
            }
        }
        ficheParser.flushParsedSubfields();
    }

}
