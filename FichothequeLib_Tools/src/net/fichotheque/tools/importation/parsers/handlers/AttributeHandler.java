/* BdfServer - Copyright (c) 2015-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers.handlers;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.tools.importation.parsers.Row;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.attr.AttributeChangeBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public class AttributeHandler {

    private final AttributeKey attributeKey;
    private int uniqueIndex = -1;
    private List<Integer> indexList;

    public AttributeHandler(AttributeKey attributeKey) {
        this.attributeKey = attributeKey;
    }

    public boolean addIndex(int index) {
        if (index == - 1) {
            this.uniqueIndex = index;
        } else {
            this.uniqueIndex = -2;
            if (indexList == null) {
                indexList = new ArrayList<Integer>();
            }
            indexList.add(index);
        }
        return true;
    }

    public void handle(Row row, AttributeChangeBuilder attributeChangeBuilder) {
        int columnCount = row.getColumnCount();
        List<CleanedString> valueList = new ArrayList<CleanedString>();
        if (uniqueIndex > -1) {
            if (uniqueIndex < columnCount) {
                AttributeParser.parseValues(valueList, row.getColumnValue(uniqueIndex));
            }
        } else {
            int indexCount = indexList.size();
            for (int i = 0; i < indexCount; i++) {
                int index = indexList.get(i);
                if (index < columnCount) {
                    AttributeParser.parseValues(valueList, row.getColumnValue(index));
                }
            }
        }
        if (valueList.isEmpty()) {
            attributeChangeBuilder.putRemovedAttributeKey(attributeKey);
        } else {
            attributeChangeBuilder.appendValues(attributeKey, valueList);
        }
    }

}
