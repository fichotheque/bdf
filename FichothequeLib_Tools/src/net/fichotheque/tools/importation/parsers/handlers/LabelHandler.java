/* BdfServer - Copyright (c) 2015-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers.handlers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.importation.ParseErrorKeys;
import net.fichotheque.tools.importation.parsers.BdfErrorException;
import net.fichotheque.tools.importation.parsers.Row;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public abstract class LabelHandler {

    protected int uniqueIndex = -1;
    protected List<Integer> indexList;

    protected LabelHandler() {

    }

    public abstract void handle(Row row, LabelChangeBuilder changeLabelHolderBuilder) throws BdfErrorException;

    public boolean addIndex(int index) {
        if (index == - 1) {
            this.uniqueIndex = index;
        } else {
            this.uniqueIndex = -2;
            if (indexList == null) {
                indexList = new ArrayList<Integer>();
            }
            indexList.add(index);
        }
        return true;
    }


    public static class Babelien extends LabelHandler {

        private int langIndex = -1;

        public Babelien() {

        }

        public boolean hasLangIndex() {
            return (langIndex != -1);
        }

        public boolean hasLibIndices() {
            return (uniqueIndex != -1);
        }

        public void setLangIndex(int langIndex) {
            this.langIndex = langIndex;
        }

        @Override
        public void handle(Row row, LabelChangeBuilder changeLabelHolderBuilder) throws BdfErrorException {
            int columnCount = row.getColumnCount();
            if (langIndex < columnCount) {
                String langString = row.getColumnValue(langIndex).trim();
                try {
                    Lang lang = Lang.parse(langString);
                    handleLabel(this, lang, row, changeLabelHolderBuilder);
                } catch (ParseException pe) {
                    throw new BdfErrorException(ParseErrorKeys.NOT_LANG, langString);
                }
            }
        }

    }


    public static class ByLang extends LabelHandler {

        private final Lang lang;

        public ByLang(Lang lang) {
            this.lang = lang;
        }

        @Override
        public void handle(Row row, LabelChangeBuilder changeLabelHolderBuilder) throws BdfErrorException {
            handleLabel(this, lang, row, changeLabelHolderBuilder);
        }

    }

    private static void handleLabel(LabelHandler handler, Lang lang, Row row, LabelChangeBuilder changeLabelHolderBuilder) {
        int columnCount = row.getColumnCount();
        CleanedString cs = null;
        if (handler.uniqueIndex > -1) {
            if (handler.uniqueIndex < columnCount) {
                cs = CleanedString.newInstance(row.getColumnValue(handler.uniqueIndex));
            }
        } else {
            StringBuilder buf = new StringBuilder();
            int indexCount = handler.indexList.size();
            for (int i = 0; i < indexCount; i++) {
                int index = handler.indexList.get(i);
                if (index < columnCount) {
                    String val = row.getColumnValue(index);
                    if (val != null) {
                        buf.append(val);
                    }
                }
            }
            cs = CleanedString.newInstance(buf.toString());
        }
        changeLabelHolderBuilder.putLabel(lang, cs);
    }

}
