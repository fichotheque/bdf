/* FichothequeLib_Tools - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.importation.parsers;

import java.util.HashSet;
import java.util.Set;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.importation.ParseErrorKeys;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusFieldKey;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.parsers.handlers.HandlerUtils;
import net.fichotheque.tools.importation.parsers.handlers.IdalphaHandler;
import net.fichotheque.tools.importation.thesaurus.CreationMotcleImportBuilder;
import net.fichotheque.tools.importation.thesaurus.CreationThesaurusImportBuilder;


/**
 *
 * @author Vincent Calame
 */
class ThesaurusCreationRowParser extends RowParser {

    private final CreationThesaurusImportBuilder creationThesaurusImportBuilder;
    private final ParseResultBuilder parseResultBuilder;
    private final ThesaurusColumns thesaurusColumns;
    private final Set<String> idalphaSet;
    private final Set<Integer> idSet;

    private ThesaurusCreationRowParser(CreationThesaurusImportBuilder creationThesaurusImportBuilder, ParseResultBuilder parseResultBuilder, ThesaurusColumns thesaurusColumns) {
        this.creationThesaurusImportBuilder = creationThesaurusImportBuilder;
        this.parseResultBuilder = parseResultBuilder;
        this.thesaurusColumns = thesaurusColumns;
        if (thesaurusColumns.getIdIndex() != -1) {
            idSet = new HashSet<Integer>();
        } else {
            idSet = null;
        }
        if (thesaurusColumns.getIdalphaHandler() != null) {
            idalphaSet = new HashSet<String>();
        } else {
            idalphaSet = null;
        }
    }

    @Override
    public void parseRow(int rowNumber, Row row) {
        Thesaurus thesaurus = creationThesaurusImportBuilder.getThesaurus();
        int columnCount = row.getColumnCount();
        int idIndex = thesaurusColumns.getIdIndex();
        int newId = -1;
        boolean withFatalError = false;
        if (idIndex != -1) {
            if (idIndex < columnCount) {
                String idString = row.getColumnValue(idIndex).trim();
                try {
                    newId = Integer.parseInt(idString);
                    if (newId < 1) {
                        newId = -1;
                    } else {
                        if (thesaurus.getMotcleById(newId) != null) {
                            parseResultBuilder.addBdfError(ParseErrorKeys.EXISTING_ID, idString, rowNumber);
                            newId = -1;
                        } else {
                            idSet.add(newId);
                        }
                    }
                } catch (NumberFormatException nfe) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.NOT_INTEGER, idString, rowNumber);
                }
            }
        }
        IdalphaHandler idalphaHandler = thesaurusColumns.getIdalphaHandler();
        String newIdalpha = null;
        if (idalphaHandler != null) {
            try {
                newIdalpha = idalphaHandler.getIdalpha(row, true);
                if (thesaurus.getMotcleByIdalpha(newIdalpha) != null) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.EXISTING_IDALPHA, newIdalpha, rowNumber);
                    withFatalError = true;
                } else {
                    idalphaSet.add(newIdalpha);
                }
            } catch (BdfErrorException bee) {
                parseResultBuilder.addBdfError(bee.getKey(), bee.getKey(), rowNumber);
                withFatalError = true;
            }

        }
        int parentId = -1;
        String parentIdalpha = null;
        int parentIdIndex = thesaurusColumns.getParentIdIndex();
        IdalphaHandler parentIdalphaHandler = thesaurusColumns.getParentIdalphaHandler();
        if (parentIdIndex != -1) {
            if (parentIdIndex < columnCount) {
                String parentIdString = row.getColumnValue(parentIdIndex).trim();
                if (parentIdString.length() > 0) {
                    try {
                        parentId = Integer.parseInt(parentIdString);
                        if (parentId < 1) {
                            parentId = -1;
                        } else {
                            if ((thesaurus.getMotcleById(parentId) == null) && (!idSet.contains(parentId))) {
                                parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_ID, parentIdString, rowNumber);
                                parentId = -1;
                            } else if (parentId == newId) {
                                parseResultBuilder.addBdfError(ParseErrorKeys.SELF_PARENT, parentIdString, rowNumber);
                                parentId = -1;
                            }
                        }
                    } catch (NumberFormatException nfe) {
                        parseResultBuilder.addBdfError(ParseErrorKeys.NOT_INTEGER, parentIdString, rowNumber);
                    }
                }
            }
        } else if (parentIdalphaHandler != null) {
            try {
                parentIdalpha = parentIdalphaHandler.getIdalpha(row, false);
                if (parentIdalpha != null) {
                    if ((thesaurus.getMotcleByIdalpha(parentIdalpha) == null) && (!idalphaSet.contains(parentIdalpha))) {
                        parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_IDALPHA, parentIdalpha, rowNumber);
                        parentIdalpha = null;
                    } else if (parentIdalpha.equals(newIdalpha)) {
                        parseResultBuilder.addBdfError(ParseErrorKeys.SELF_PARENT, parentIdalpha, rowNumber);
                        parentIdalpha = null;
                    }
                }
            } catch (BdfErrorException bee) {
                parseResultBuilder.addBdfError(bee.getKey(), bee.getKey(), rowNumber);
            }
        }
        if (withFatalError) {
            return;
        }
        CreationMotcleImportBuilder motcleImportBuilder = creationThesaurusImportBuilder.getCreationMotcleImportBuilder(newId, newIdalpha);
        motcleImportBuilder.setParentId(parentId).setParentIdalpha(parentIdalpha);
        int statusIndex = thesaurusColumns.getStatusIndex();
        String newStatus = null;
        if (statusIndex != -1) {
            String statusString = row.getColumnValue(statusIndex).trim();
            if (!statusString.isEmpty()) {
                try {
                    motcleImportBuilder.setNewStatus(FichothequeConstants.checkMotcleStatus(statusString));
                } catch (IllegalArgumentException iae) {
                    parseResultBuilder.addBdfError(ParseErrorKeys.UNKNOWN_STATUS, statusString, rowNumber);
                }
            }
        }
        if (thesaurusColumns.hasLabelHandler()) {
            HandlerUtils.populate(motcleImportBuilder.getLabelChangeBuilder(), thesaurusColumns.getLabelHandlerArray(), row, parseResultBuilder, rowNumber);
        }
        if (thesaurusColumns.hasAttributeHandler()) {
            HandlerUtils.populate(motcleImportBuilder.getAttributeChangeBuilder(), thesaurusColumns.getAttributeHandlerArray(), row);
        }
        if (thesaurusColumns.hasCroisementHandler()) {
            HandlerUtils.populate(motcleImportBuilder.getLiensImportBuilder(), thesaurusColumns.getCroisementHandlers(), row, thesaurus, null, null, null);
        }
    }

    static ThesaurusCreationRowParser newInstance(String[] fieldArray, CreationThesaurusImportBuilder creationThesaurusImportBuilder, ParseResultBuilder parseResultBuilder) {
        ThesaurusColumns thesaurusColumns = ThesaurusColumns.parse(fieldArray, creationThesaurusImportBuilder, parseResultBuilder);
        if ((creationThesaurusImportBuilder.getThesaurus().isIdalphaType()) && (thesaurusColumns.getIdalphaHandler() == null)) {
            parseResultBuilder.addInitError(ParseErrorKeys.MISSING_FIELDS_INIT, ThesaurusFieldKey.IDALPHA.toString());
            return null;
        } else {
            return new ThesaurusCreationRowParser(creationThesaurusImportBuilder, parseResultBuilder, thesaurusColumns);
        }
    }

}
