/* FichothequeLib_Tools - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.duplication;

import java.util.function.Predicate;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.eligibility.SubsetEligibility;


/**
 *
 * @author Vincent Calame
 */
public class DuplicationFilter {

    private final SubsetEligibility subsetEligibility;
    private final Predicate<FieldKey> fieldPredicate;

    public DuplicationFilter(SubsetEligibility subsetEligibility, Predicate<FieldKey> fieldPredicate) {
        this.subsetEligibility = subsetEligibility;
        this.fieldPredicate = fieldPredicate;
    }

    public SubsetEligibility getSubsetEligibility() {
        return subsetEligibility;
    }

    public Predicate<FieldKey> getFieldPredicate() {
        return fieldPredicate;
    }

}
