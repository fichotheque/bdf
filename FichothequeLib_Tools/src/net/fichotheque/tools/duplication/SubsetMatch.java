/* FichothequeLib_Tools - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.duplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;


/**
 *
 * @author Vincent Calame
 */
public class SubsetMatch {

    private final Subset originSubset;
    private final Subset destinationSubset;
    private final List<Entry> entryList = new ArrayList<Entry>();
    private final Map<Integer, Entry> originEntryMap = new HashMap<Integer, Entry>();
    private final Map<Integer, Entry> destinationEntryMap = new HashMap<Integer, Entry>();

    public SubsetMatch(Subset originSubset, Subset destinationSubset) {
        this.originSubset = originSubset;
        this.destinationSubset = destinationSubset;
    }

    public Subset getOriginSubset() {
        return originSubset;
    }

    public Subset getDestinationSubset() {
        return destinationSubset;
    }

    public int size() {
        return entryList.size();
    }

    public List<Entry> getEntryList() {
        return entryList;
    }

    public void add(SubsetItem origin, SubsetItem destination) {
        if (!origin.getSubset().equals(originSubset)) {
            throw new IllegalArgumentException("origin not from origin subset");
        }
        if (!destination.getSubset().equals(destinationSubset)) {
            throw new IllegalArgumentException("destination not from destination subset");
        }
        if (originEntryMap.containsKey(origin.getId())) {
            throw new IllegalArgumentException("origin already included");
        }
        if (destinationEntryMap.containsKey(destination.getId())) {
            throw new IllegalArgumentException("destination already included");
        }
        Entry entry = new Entry(origin, destination);
        originEntryMap.put(origin.getId(), entry);
        destinationEntryMap.put(destination.getId(), entry);
        entryList.add(entry);
    }

    public Entry getEntryByOrigin(int originId) {
        return originEntryMap.get(originId);
    }

    public Entry getEntryByDestination(int destinationId) {
        return destinationEntryMap.get(destinationId);
    }


    public static class Entry {

        private final SubsetItem origin;
        private final SubsetItem destination;

        private Entry(SubsetItem origin, SubsetItem destination) {
            this.origin = origin;
            this.destination = destination;
        }

        public SubsetItem getOrigin() {
            return origin;
        }

        public SubsetItem getDestination() {
            return destination;
        }


    }

}
