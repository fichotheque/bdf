/* FichothequeLib_Tools - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.croisement;

import java.util.ArrayList;
import java.util.List;
import net.fichotheque.SubsetItem;
import net.fichotheque.croisement.CroisementChange;
import net.fichotheque.croisement.CroisementChanges;
import net.fichotheque.utils.CroisementUtils;
import net.fichotheque.utils.FichothequeUtils;


/**
 *
 * @author Vincent Calame
 */
public class CroisementChangesBuilder {

    private final List<SubsetItem> removedList = new ArrayList<SubsetItem>();
    private final List<CroisementChanges.Entry> entryList = new ArrayList<CroisementChanges.Entry>();


    public CroisementChangesBuilder() {
    }

    public CroisementChangesBuilder addRemoved(SubsetItem subsetItem) {
        removedList.add(subsetItem);
        return this;
    }

    public CroisementChangesBuilder addEntry(SubsetItem subsetItem, CroisementChange croisementChange) {
        entryList.add(CroisementUtils.toEntry(subsetItem, croisementChange));
        return this;
    }

    public CroisementChanges toCroisementChanges() {
        List<SubsetItem> finalRemovedList = FichothequeUtils.wrap(removedList.toArray(new SubsetItem[removedList.size()]));
        List<CroisementChanges.Entry> finalEntryList = CroisementUtils.wrap(entryList.toArray(new CroisementChanges.Entry[entryList.size()]));
        return new InternalCroisementChanges(finalRemovedList, finalEntryList);
    }

    public static CroisementChangesBuilder init() {
        return new CroisementChangesBuilder();
    }


    private static class InternalCroisementChanges implements CroisementChanges {

        private final List<SubsetItem> removedList;
        private final List<CroisementChanges.Entry> entryList;

        private InternalCroisementChanges(List<SubsetItem> removedList, List<CroisementChanges.Entry> entryList) {
            this.removedList = removedList;
            this.entryList = entryList;
        }

        @Override
        public List<SubsetItem> getRemovedList() {
            return removedList;
        }

        @Override
        public List<Entry> getEntryList() {
            return entryList;
        }

    }

}
