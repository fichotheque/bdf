/* FichothequeLib_API - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.croisement;

import net.fichotheque.SubsetItem;


/**
 *
 * @author Vincent Calame
 */
public class LienBuffer {

    private final SubsetItem subsetItem;
    private final String mode;
    private final int poids;

    public LienBuffer(SubsetItem subsetItem, String mode, int poids) {
        this.subsetItem = subsetItem;
        this.mode = mode;
        this.poids = poids;
    }

    public SubsetItem getSubsetItem() {
        return subsetItem;
    }

    public String getMode() {
        return mode;
    }

    public int getPoids() {
        return poids;
    }

}
