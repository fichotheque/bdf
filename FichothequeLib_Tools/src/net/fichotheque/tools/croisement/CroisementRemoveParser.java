/* FichothequeLib_Tools - Copyright (c) 2015-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.croisement;

import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Predicate;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.croisement.CroisementChanges;
import net.fichotheque.include.IncludeKey;


/**
 *
 * @author Vincent Calame
 */
public final class CroisementRemoveParser {

    private CroisementRemoveParser() {

    }

    public static CroisementChanges parseRemove(String[] values, Fichotheque fichotheque, Predicate<SubsetItem> predicate) {
        Map<SubsetItem, CroisementChangeBuilder> map = new LinkedHashMap<SubsetItem, CroisementChangeBuilder>();
        for (String value : values) {
            int idx = value.indexOf('/');
            if (idx > 0) {
                try {
                    IncludeKey includeKey = IncludeKey.parse(value.substring(0, idx));
                    int id = Integer.parseInt(value.substring(idx + 1));
                    Subset subset = fichotheque.getSubset(includeKey.getSubsetKey());
                    if (subset != null) {
                        SubsetItem subsetItem = subset.getSubsetItemById(id);
                        if (subsetItem != null) {
                            if ((predicate != null) && (!predicate.test(subsetItem))) {
                                continue;
                            }
                            CroisementChangeBuilder builder = map.get(subsetItem);
                            if (builder == null) {
                                builder = new CroisementChangeBuilder();
                                map.put(subsetItem, builder);
                            }
                            builder.addRemovedMode(includeKey.getMode());
                        }
                    }
                } catch (ParseException | NumberFormatException e) {

                }
            }
        }
        CroisementChangesBuilder croisementChangesBuilder = new CroisementChangesBuilder();
        for (Map.Entry<SubsetItem, CroisementChangeBuilder> entry : map.entrySet()) {
            croisementChangesBuilder.addEntry(entry.getKey(), entry.getValue().toCroisementChange());
        }
        return croisementChangesBuilder.toCroisementChanges();
    }

}
