/* FichothequeLib_Tools - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.croisement;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.fichotheque.croisement.CroisementChange;
import net.fichotheque.croisement.Lien;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class CroisementChangeBuilder {

    private final Set<String> removedModeSet = new LinkedHashSet<String>();
    private final Map<String, Lien> lienMap = new LinkedHashMap<String, Lien>();

    public CroisementChangeBuilder() {
    }

    public CroisementChangeBuilder addRemovedMode(String mode) {
        removedModeSet.add(mode);
        return this;
    }

    public CroisementChangeBuilder addLien(Lien lien) {
        lienMap.put(lien.getMode(), lien);
        return this;
    }

    public CroisementChange toCroisementChange() {
        return toCroisementChange(removedModeSet, lienMap.values());
    }

    public static CroisementChangeBuilder init() {
        return new CroisementChangeBuilder();
    }

    static CroisementChange toCroisementChange(Collection<String> removedModes, Collection<Lien> changedLiens) {
        List<String> removedModeArray = StringUtils.toList(removedModes);
        List<Lien> changedLienArray = CroisementUtils.wrap(changedLiens.toArray(new Lien[changedLiens.size()]));
        return new InternalCroisementChange(removedModeArray, changedLienArray);
    }


    private static class InternalCroisementChange implements CroisementChange {

        private final List<String> removedModeArray;
        private final List<Lien> changedLienArray;

        private InternalCroisementChange(List<String> removedModeArray, List<Lien> changedLienArray) {
            this.removedModeArray = removedModeArray;
            this.changedLienArray = changedLienArray;
        }

        @Override
        public List<String> getRemovedModeList() {
            return removedModeArray;
        }

        @Override
        public List<Lien> getChangedLienList() {
            return changedLienArray;
        }

    }

}
