/* FichothequeLib_Tools - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.alias;

import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public class AliasUtils {

    public final static String SEVERE_ALIAS = "severe.alias";

    private AliasUtils() {
    }

    public static boolean testDatationPropriete(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isPropriete()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "propriete");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.DATATION_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.DATATION_FIELD);
            return false;
        }
        return true;
    }

    public static boolean testMontantPropriete(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isPropriete()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "propriete");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.MONTANT_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.MONTANT_FIELD);
            return false;
        }
        return true;
    }

    public static boolean testNombrePropriete(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isPropriete()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "propriete");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.NOMBRE_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.NOMBRE_FIELD);
            return false;
        }
        return true;
    }

    public static boolean testPersonnePropriete(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isPropriete()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "propriete");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.PERSONNE_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.PERSONNE_FIELD);
            return false;
        }
        return true;
    }

    public static boolean testItemPropriete(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isPropriete()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "propriete");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.ITEM_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.ITEM_FIELD);
            return false;
        }
        return true;
    }

    public static boolean testDatationInformation(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isInformation()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "information");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.DATATION_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.DATATION_FIELD);
            return false;
        }
        return true;
    }

    public static boolean testMontantInformation(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isInformation()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "information");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.MONTANT_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.MONTANT_FIELD);
            return false;
        }
        return true;
    }

    public static boolean testLangueInformation(String alias, CorpusField corpusField, MessageHandler messageHandler) {
        if (!corpusField.isInformation()) {
            wrongFieldCategory(messageHandler, alias, corpusField, "information");
            return false;
        }
        if (corpusField.getFicheItemType() != CorpusField.LANGUE_FIELD) {
            wrongFicheItemType(messageHandler, alias, corpusField, CorpusField.LANGUE_FIELD);
            return false;
        }
        return true;
    }

    public static void unknownAlias(MessageHandler messageHandler, String aliasType, String alias) {
        messageHandler.addMessage(SEVERE_ALIAS, "_ error.unknown.alias", aliasType, alias);
    }

    public static void missingSubset(MessageHandler messageHandler, String alias, SubsetKey subsetKey) {
        messageHandler.addMessage(SEVERE_ALIAS, "_ error.unknown.alias.subset", alias, subsetKey);
    }

    public static void missingField(MessageHandler messageHandler, String alias, FieldKey fieldKey) {
        messageHandler.addMessage(SEVERE_ALIAS, "_ error.unknown.alias.field", alias, fieldKey);
    }

    public static void wrongFieldCategory(MessageHandler messageHandler, String alias, CorpusField corpusField, String expectedCategories) {
        messageHandler.addMessage(SEVERE_ALIAS, "_ error.wrong.alias.fieldcategory", alias, corpusField.getFieldString(), expectedCategories);
    }

    public static void wrongFicheItemType(MessageHandler messageHandler, String alias, CorpusField corpusField, short expectedFicheItemType) {
        messageHandler.addMessage(SEVERE_ALIAS, "_ error.wrong.alias.itemtype", alias, corpusField.getFieldString(), CorpusField.ficheItemTypeToString(expectedFicheItemType));
    }


    public static class ErrorHandler implements MessageHandler {

        private final MessageHandler wrappedHandler;
        private boolean withError = false;

        public ErrorHandler(MessageHandler wrappedHandler) {
            this.wrappedHandler = wrappedHandler;
        }

        public boolean isWithError() {
            return withError;
        }

        @Override
        public void addMessage(String category, Message message) {
            if (category.equals(SEVERE_ALIAS)) {
                withError = true;
            }
            wrappedHandler.addMessage(category, message);
        }

    }

}
