/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.alias;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.FieldKey;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class AliasDOMReader {

    private final AliasHolderBuilder builder;
    private final Map<String, SubsetKey> corpusKeyMap = new HashMap<String, SubsetKey>();
    private final Map<String, SubsetKey> thesaurusKeyMap = new HashMap<String, SubsetKey>();
    private final Map<String, FieldKey> fieldKeyMap = new HashMap<String, FieldKey>();

    /**
     * Builder peut être nul
     */
    public AliasDOMReader(AliasHolderBuilder builder) {
        this.builder = builder;
    }

    public void readAlias(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
    }

    public SubsetKey getCorpusKey(String alias) {
        return corpusKeyMap.get(alias);
    }

    public SubsetKey getThesaurusKey(String alias) {
        return thesaurusKeyMap.get(alias);
    }

    public FieldKey getFieldKey(String alias) {
        return fieldKeyMap.get(alias);
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {

        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            String alias = element.getAttribute("alias");
            if (alias.length() == 0) {
                return;
            }
            String name = element.getAttribute("name");
            if (tagName.equals("corpus")) {
                if (name.length() == 0) {
                    return;
                }
                SubsetKey subsetKey;
                try {
                    subsetKey = SubsetKey.parse(SubsetKey.CATEGORY_CORPUS, name);
                } catch (ParseException pe) {
                    return;
                }
                corpusKeyMap.put(alias, subsetKey);
                if (builder != null) {
                    Corpus corpus = builder.initCorpus(alias, subsetKey);
                    if (corpus != null) {
                        DOMUtils.readChildren(element, new CorpusConsumer(corpus, alias));
                    }
                } else {
                    DOMUtils.readChildren(element, new CorpusConsumer(null, alias));
                }
            } else if (tagName.equals("thesaurus")) {
                if (name.length() == 0) {
                    return;
                }
                SubsetKey subsetKey;
                try {
                    subsetKey = SubsetKey.parse(SubsetKey.CATEGORY_THESAURUS, name);
                } catch (ParseException pe) {
                    return;
                }
                thesaurusKeyMap.put(alias, subsetKey);
                if (builder != null) {
                    builder.initThesaurus(alias, subsetKey);
                }
            } else if (tagName.equals("core-corpus")) {
                if (builder != null) {
                    Corpus coreCorpus = builder.getCoreCorpus(alias);
                    if (coreCorpus != null) {
                        DOMUtils.readChildren(element, new CorpusConsumer(coreCorpus, alias));
                    }
                } else {
                    DOMUtils.readChildren(element, new CorpusConsumer(null, alias));
                }
            }
        }

    }


    private class CorpusConsumer implements Consumer<Element> {

        private final Corpus corpus;
        private final String corpusAlias;

        private CorpusConsumer(Corpus corpus, String corpusAlias) {
            this.corpus = corpus;
            this.corpusAlias = corpusAlias;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            String alias = element.getAttribute("alias");
            if (alias.length() == 0) {
                return;
            }
            if (tagName.equals("field")) {
                String key = element.getAttribute("key");
                if (key.length() == 0) {
                    return;
                }
                FieldKey fieldKey;
                try {
                    fieldKey = FieldKey.parse(key);
                } catch (ParseException pe) {
                    return;
                }
                fieldKeyMap.put(alias, fieldKey);
                if (builder != null) {
                    builder.initField(corpus, corpusAlias, alias, fieldKey);
                }
            }
        }

    }

}
