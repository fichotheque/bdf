/* FichothequeLib_Tools - Copyright (c) 2006-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.valueresolvers;

import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.text.AccoladeArgument;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.ValueResolver;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusValueResolver implements ValueResolver {

    public final static String THESAURUS = "thesaurus";
    private final Thesaurus thesaurus;

    public ThesaurusValueResolver(Thesaurus thesaurus) {
        this.thesaurus = thesaurus;
    }

    @Override
    public String getValue(AccoladeArgument accoladeArgument) {
        if (accoladeArgument.getName().equals(THESAURUS)) {
            return thesaurus.getSubsetName();
        }
        return AccoladePattern.toString(accoladeArgument);
    }

}
