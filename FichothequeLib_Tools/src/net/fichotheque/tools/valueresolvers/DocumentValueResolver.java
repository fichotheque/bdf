/* FichothequeLib_Tools - Copyright (c) 2007-2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.valueresolvers;

import net.fichotheque.addenda.Document;
import net.mapeadores.util.text.AccoladeArgument;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.ValueResolver;


/**
 *
 * @author Vincent Calame
 */
public class DocumentValueResolver implements ValueResolver {

    public final static String BASENAME = "basename";
    private Document document;

    public DocumentValueResolver(Document document) {
        this.document = document;
    }

    public String getValue(AccoladeArgument accoladeArgument) {
        String name = accoladeArgument.getName();
        if (name.equals(BASENAME)) {
            return document.getBasename();
        }
        return AccoladePattern.toString(accoladeArgument);
    }

}
