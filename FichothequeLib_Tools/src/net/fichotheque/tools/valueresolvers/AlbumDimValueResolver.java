/* FichothequeLib_Tools - Copyright (c) 2010-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.valueresolvers;

import net.fichotheque.album.metadata.AlbumDim;
import net.mapeadores.util.text.AccoladeArgument;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.ValueResolver;


/**
 *
 * @author Vincent Calame
 */
public class AlbumDimValueResolver implements ValueResolver {

    public final static String ALBUM = "album";
    public final static String ALBUMDIM = "albumdim";
    private AlbumDim albumDim;

    public AlbumDimValueResolver(AlbumDim albumDim) {
        this.albumDim = albumDim;
    }

    public String getValue(AccoladeArgument patternArgument) {
        if (patternArgument.getName().equals(ALBUM)) {
            return albumDim.getAlbumMetadata().getAlbum().getSubsetName();
        }
        if (patternArgument.getName().equals(ALBUMDIM)) {
            return albumDim.getName();
        }
        return AccoladePattern.toString(patternArgument);
    }

}
