/* FichothequeLib_Tools - Copyright (c) 2010-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.valueresolvers;

import net.fichotheque.album.Album;
import net.mapeadores.util.text.AccoladeArgument;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.ValueResolver;


/**
 *
 * @author Vincent Calame
 */
public class AlbumValueResolver implements ValueResolver {

    public final static String ALBUM = "album";
    private final Album album;

    public AlbumValueResolver(Album album) {
        this.album = album;
    }

    @Override
    public String getValue(AccoladeArgument accoladeArgument) {
        if (accoladeArgument.getName().equals(ALBUM)) {
            return album.getSubsetName();
        }
        return AccoladePattern.toString(accoladeArgument);
    }

}
