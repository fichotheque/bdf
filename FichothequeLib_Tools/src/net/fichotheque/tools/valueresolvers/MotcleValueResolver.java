/* FichothequeLib_Tools - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.valueresolvers;

import net.fichotheque.thesaurus.Motcle;
import net.mapeadores.util.text.AccoladeArgument;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.ValueResolver;


/**
 *
 * @author Vincent Calame
 */
public class MotcleValueResolver implements ValueResolver {

    private final Motcle motcle;

    public MotcleValueResolver(Motcle motcle) {
        this.motcle = motcle;
    }

    @Override
    public String getValue(AccoladeArgument accoladeArgument) {
        String name = accoladeArgument.getName();
        if (name.equals("thesaurus")) {
            return motcle.getSubsetName();
        }
        if ((name.equals("id")) || (name.equals("idths"))) {
            return String.valueOf(motcle.getId());
        }
        if (name.equals("idalpha")) {
            return motcle.getIdalpha();
        }
        return AccoladePattern.toString(accoladeArgument);
    }

}
