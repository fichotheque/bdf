/* BdfServer - Copyright (c) 2017-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.history;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import net.fichotheque.EditOrigin;
import net.fichotheque.history.HistoryUnit;
import net.fichotheque.history.HistoryUnit.Revision;


/**
 *
 * @author Vincent Calame
 */
public class HistoryUnitBuilder {

    private final List<HistoryUnit.Revision> revisionList = new ArrayList<HistoryUnit.Revision>();
    private boolean deleted = false;

    public HistoryUnitBuilder() {

    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void addRevision(String name, EditOrigin editOrigin) {
        EditOrigin[] array = {editOrigin};
        addRevision(name, array);
    }

    public void addRevision(String name, Collection<EditOrigin> editOrigins) {
        EditOrigin[] array = editOrigins.toArray(new EditOrigin[editOrigins.size()]);
        addRevision(name, array);
    }

    private void addRevision(String name, EditOrigin[] array) {
        revisionList.add(new InternalRevision(name, new EditOriginList(array)));
    }

    public HistoryUnit toHistoryUnit() {
        List<HistoryUnit.Revision> finalRevisionList = new RevisionList(revisionList.toArray(new HistoryUnit.Revision[revisionList.size()]));
        return new InternalHistoryUnit(deleted, finalRevisionList);
    }


    private static class InternalHistoryUnit implements HistoryUnit {

        private final boolean deleted;
        private final List<HistoryUnit.Revision> revisionList;

        private InternalHistoryUnit(boolean deleted, List<HistoryUnit.Revision> revisionList) {
            this.deleted = deleted;
            this.revisionList = revisionList;
        }

        @Override
        public boolean isDeleted() {
            return deleted;
        }

        @Override
        public List<Revision> getRevisionList() {
            return revisionList;
        }

    }


    private static class InternalRevision implements HistoryUnit.Revision {

        private final String name;
        private final List<EditOrigin> editOriginList;

        private InternalRevision(String name, List<EditOrigin> editOriginList) {
            this.name = name;
            this.editOriginList = editOriginList;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<EditOrigin> getEditOriginList() {
            return editOriginList;
        }

    }


    private static class RevisionList extends AbstractList<HistoryUnit.Revision> implements RandomAccess {

        private final HistoryUnit.Revision[] array;

        private RevisionList(HistoryUnit.Revision[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public Revision get(int index) {
            return array[index];
        }

    }


    private static class EditOriginList extends AbstractList<EditOrigin> implements RandomAccess {

        private final EditOrigin[] array;

        private EditOriginList(EditOrigin[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public EditOrigin get(int index) {
            return array[index];
        }

    }

}
