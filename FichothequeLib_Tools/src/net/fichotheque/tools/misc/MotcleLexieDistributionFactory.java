/* FichothequeLib_Tools - Copyright (c) 2007-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.misc;

import java.util.List;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.lexie.LexieDistribution;
import net.mapeadores.util.text.lexie.LexieDistributionBuilder;
import net.mapeadores.util.text.lexie.LexieSource;


/**
 *
 * @author Vincent Calame
 */
public final class MotcleLexieDistributionFactory {

    private MotcleLexieDistributionFactory() {
    }

    public static LexieDistribution getLexieDistribution(Thesaurus thesaurus, Lang thesaurusLang) {
        LexieDistributionBuilder lexieDistributionBuilder = new LexieDistributionBuilder(thesaurusLang);
        add(lexieDistributionBuilder, thesaurus.getFirstLevelList(), thesaurusLang, true);
        return lexieDistributionBuilder.toLexieDistribution();
    }

    public static LexieDistribution getLexieDistribution(List<Motcle> motcleList, Lang thesaurusLang, boolean recursive) {
        LexieDistributionBuilder lexieDistributionBuilder = new LexieDistributionBuilder(thesaurusLang);
        add(lexieDistributionBuilder, motcleList, thesaurusLang, recursive);
        return lexieDistributionBuilder.toLexieDistribution();
    }

    private static void add(LexieDistributionBuilder lexieDistributionBuilder, List<Motcle> level, Lang thesaurusLang, boolean recursive) {
        for (Motcle motcle : level) {
            Label label = motcle.getLabels().getLabel(thesaurusLang);
            if (label != null) {
                LexieSource lexieSource = new LexieSource(label.getLabelString(), motcle, String.valueOf(motcle.getId()));
                lexieDistributionBuilder.addLexieSource(lexieSource);
            }
            if (recursive) {
                add(lexieDistributionBuilder, motcle.getChildList(), thesaurusLang, true);
            }
        }
    }

}
