/* FichothequeLib_Tools - Copyright (c) 2010-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.misc;

import java.util.Locale;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusLangChecker;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusMessageLocalisation implements MessageLocalisation {

    private final Thesaurus thesaurus;
    private final ThesaurusLangChecker thesaurusLangChecker;
    private final Lang lang;
    private final Locale formatLocale;

    public ThesaurusMessageLocalisation(Thesaurus thesaurus, Lang lang, ThesaurusLangChecker thesaurusLangChecker, Locale formatLocale) {
        this.thesaurus = thesaurus;
        this.lang = lang;
        this.thesaurusLangChecker = thesaurusLangChecker;
        this.formatLocale = formatLocale;
    }

    @Override
    public String toString(String messageKey) {
        if (messageKey.isEmpty()) {
            return "";
        }
        messageKey = LocalisationUtils.cleanMessageKey(messageKey);
        Motcle motcle = thesaurus.getMotcleByIdalpha(messageKey);
        if (motcle != null) {
            return checkLabelString(motcle, lang, messageKey);
        } else {
            return null;
        }
    }

    @Override
    public String toString(Message message) {
        String messageKey = message.getMessageKey();
        if (messageKey.isEmpty()) {
            return LocalisationUtils.joinValues(message);
        }
        messageKey = LocalisationUtils.cleanMessageKey(messageKey);
        Motcle motcle = thesaurus.getMotcleByIdalpha(messageKey);
        if (motcle != null) {
            try {
                return StringUtils.formatMessage(checkLabelString(motcle, lang, messageKey), message.getMessageValues(), formatLocale);
            } catch (StringUtils.FormatException fe) {
                return "[" + messageKey + "] " + fe.getMessage();
            }
        } else {
            return null;
        }
    }

    @Override
    public boolean containsKey(String messageKey) {
        messageKey = LocalisationUtils.cleanMessageKey(messageKey);
        Motcle motcle = thesaurus.getMotcleByIdalpha(messageKey);
        return (motcle != null);
    }

    private String checkLabelString(Motcle motcle, Lang lang, String messageKey) {
        Label label = motcle.getLabels().getLangPartCheckedLabel(lang);
        if (label == null) {
            label = LabelUtils.getFirstLabel(motcle.getLabels(), thesaurusLangChecker.getAuthorizedLangs(motcle.getThesaurus()));
        }
        if (label != null) {
            return label.getLabelString();
        } else {
            return messageKey;
        }
    }

}
