/* FichothequeLib_Tools - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.MetadataEditor;
import net.fichotheque.namespaces.ExtractionSpace;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractMetadataDOMReader {

    protected final MetadataEditor metadataEditor;
    protected final MessageHandler messageHandler;
    private final String oldIntituleName;

    public AbstractMetadataDOMReader(MetadataEditor metadataEditor, MessageHandler messageHandler) {
        this.metadataEditor = metadataEditor;
        this.messageHandler = messageHandler;
        this.oldIntituleName = "";
    }

    public AbstractMetadataDOMReader(MetadataEditor metadataEditor, MessageHandler messageHandler, String oldIntituleName) {
        this.metadataEditor = metadataEditor;
        this.messageHandler = messageHandler;
        this.oldIntituleName = oldIntituleName;
    }

    protected boolean readCommonElement(Element element, AttributesBuilder attributesBuilder, String parentXpath) {
        String tagName = element.getTagName();
        String xpath = parentXpath + "/" + tagName;
        boolean common = true;
        if (tagName.equals("label")) {
            try {
                Label label = LabelUtils.readLabel(element);
                if (label != null) {
                    metadataEditor.putLabel(null, label);
                }
            } catch (ParseException pe) {
                DomMessages.wrongLangAttribute(messageHandler, xpath, element.getAttribute("xml:lang"));
            }
        } else if (tagName.equals("phrase")) {
            String name = element.getAttribute("name");
            if (name.length() == 0) {
                DomMessages.emptyAttribute(messageHandler, xpath, "name");
            } else {
                xpath = xpath + "[@name='" + name + "']";
                DOMUtils.readChildren(element, new PhraseLabelConsumer(name, xpath));
            }
        } else if (tagName.equals("attr")) {
            AttributeUtils.readAttrElement(attributesBuilder, element, messageHandler, xpath, ExtractionSpace.EXTRACTION_ALIAS);
        } else if (tagName.equals("intitule")) {
            FichothequeDOMUtils.readIntitule(element, metadataEditor, messageHandler, oldIntituleName);
        } else {
            common = false;
        }
        return common;
    }

    protected void flush(AttributesBuilder attributesBuilder) {
        Attributes attributes = attributesBuilder.toAttributes();
        for (Attribute attribute : attributes) {
            metadataEditor.putAttribute(attribute);
        }
    }


    private class PhraseLabelConsumer implements Consumer<Element> {

        private final String phraseName;
        private final String parentXpath;

        PhraseLabelConsumer(String phraseName, String parentXpath) {
            this.phraseName = phraseName;
            this.parentXpath = parentXpath;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            String xpath = parentXpath + "/" + tagName;
            if (tagName.equals("label")) {
                try {
                    Label label = LabelUtils.readLabel(element);
                    if (label != null) {
                        metadataEditor.putLabel(phraseName, label);
                    }
                } catch (ParseException pe) {
                    DomMessages.wrongLangAttribute(messageHandler, xpath, element.getAttribute("xml:lang"));
                }
            } else {
                DomMessages.unknownTagWarning(messageHandler, xpath);
            }
        }

    }

}
