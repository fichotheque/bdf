/* FichothequeLib_Tools - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class SphereListDOMReader {

    private final Handler handler;
    private final MessageHandler messageHandler;

    public SphereListDOMReader(SphereEditor sphereEditor, MessageHandler messageHandler) {
        this.handler = new SphereEditorHandler(sphereEditor);
        this.messageHandler = messageHandler;
    }

    public SphereListDOMReader(Handler handler, MessageHandler messageHandler) {
        this.handler = handler;
        this.messageHandler = messageHandler;
    }

    public void fillSphere(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("redacteur")) {
                String idString = element.getAttribute("id");
                if (idString.length() == 0) {
                    idString = element.getAttribute("idsph");
                }
                if (idString.length() == 0) {
                    DomMessages.emptyAttribute(messageHandler, tagName, "id");
                    return;
                }
                int id;
                try {
                    id = Integer.parseInt(idString);
                } catch (NumberFormatException nfe) {
                    DomMessages.wrongIntegerAttributeValue(messageHandler, tagName, "id", idString);
                    return;
                }
                String login = element.getAttribute("login");
                if (login.length() == 0) {
                    DomMessages.emptyAttribute(messageHandler, tagName, "login");
                    return;
                }
                try {
                    handler.checkRedacteur(id, login, getStatus(element));
                } catch (ExistingIdException eie) {
                    messageHandler.addMessage(FichothequeConstants.SEVERE_FICHOTHEQUE, "_ error.existing.id", login);
                } catch (ParseException pe) {
                    DomMessages.wrongAttributeValue(messageHandler, tagName, "login", login);
                }
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

        private String getStatus(Element element) {
            String status = FichothequeConstants.ACTIVE_STATUS;
            String statusAttr = element.getAttribute("status");
            if (statusAttr.isEmpty()) {
                String activeString = element.getAttribute("active");
                if (activeString.isEmpty()) {
                    activeString = element.getAttribute("actif");
                }
                if (activeString.equals("0")) {
                    status = FichothequeConstants.INACTIVE_STATUS;
                }
            } else {
                try {
                    status = FichothequeConstants.checkRedacteurStatus(statusAttr);
                } catch (IllegalArgumentException iae) {
                }
            }
            return status;
        }

    }


    public static abstract class Handler {

        public Handler() {

        }

        public abstract void checkRedacteur(int id, String login, String status) throws ExistingIdException, ParseException;


    }


    private static class SphereEditorHandler extends Handler {

        private final SphereEditor sphereEditor;

        private SphereEditorHandler(SphereEditor sphereEditor) {
            this.sphereEditor = sphereEditor;
        }

        @Override
        public void checkRedacteur(int id, String login, String status) throws ExistingIdException, ParseException {
            Redacteur redacteur = sphereEditor.createRedacteur(id, login);
            sphereEditor.setStatus(redacteur, status);
        }

    }

}
