/* FichothequeLib_Tools - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.util.function.Consumer;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheMeta;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class FicheAttributesDOMReader {

    private final MessageHandler messageHandler;
    private final CorpusEditor corpusEditor;
    private final RootConsumer rootConsumer = new RootConsumer();

    public FicheAttributesDOMReader(CorpusEditor corpusEditor, MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.corpusEditor = corpusEditor;
    }

    public void fillFicheMeta(FicheMeta ficheMeta, Element element) {
        AttributesBuilder attributesBuilder = new AttributesBuilder();
        rootConsumer.setFicheMeta(ficheMeta, attributesBuilder);
        DOMUtils.readChildren(element, rootConsumer);
        corpusEditor.getFichothequeEditor().putAttributes(ficheMeta, attributesBuilder.toAttributes());
    }


    private class RootConsumer implements Consumer<Element> {

        private FicheMeta ficheMeta;
        private AttributesBuilder attributesBuilder;

        private RootConsumer() {
        }

        private void setFicheMeta(FicheMeta ficheMeta, AttributesBuilder attributesBuilder) {
            this.ficheMeta = ficheMeta;
            this.attributesBuilder = attributesBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("attr")) {
                AttributeUtils.readAttrElement(attributesBuilder, element, messageHandler, tagName);
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }

}
