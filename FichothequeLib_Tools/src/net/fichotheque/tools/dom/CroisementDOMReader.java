/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import net.fichotheque.SubsetItem;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.croisement.CroisementChange;
import net.fichotheque.croisement.CroisementEditor;
import net.fichotheque.croisement.Lien;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class CroisementDOMReader {

    private final CroisementEditor croisementEditor;
    private final MessageHandler messageHandler;
    private final RootConsumer rootConsumer = new RootConsumer();

    public CroisementDOMReader(CroisementEditor croisementEditor, MessageHandler messageHandler) {
        this.croisementEditor = croisementEditor;
        this.messageHandler = messageHandler;
    }

    public void readCroisement(Element element, SubsetItem subsetItem1, SubsetItem subsetItem2) {
        rootConsumer.clear();
        DOMUtils.readChildren(element, rootConsumer);
        rootConsumer.flush(subsetItem1, subsetItem2);
    }


    private class RootConsumer implements Consumer<Element> {

        private final Map<String, Lien> lienMap = new LinkedHashMap<String, Lien>();
        private AttributesBuilder attributesBuilder;

        private RootConsumer() {
        }

        private void clear() {
            lienMap.clear();
            attributesBuilder = null;
        }

        private void flush(SubsetItem subsetItem1, SubsetItem subsetItem2) {
            List<Lien> lienList = CroisementUtils.wrap(lienMap.values().toArray(new Lien[lienMap.size()]));
            CroisementChange croisementChange = new InternalCroisementChange(lienList);
            Croisement croisement = croisementEditor.updateCroisement(subsetItem1, subsetItem2, croisementChange);
            if ((croisement != null) && (attributesBuilder != null)) {
                croisementEditor.getFichothequeEditor().putAttributes(croisement, attributesBuilder.toAttributes());
            }
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lien")) {
                String mode = element.getAttribute("mode");
                int poids = getPoids(element);
                int position1 = getPosition(element, "position1");
                int position2 = getPosition(element, "position2");
                Lien lien = CroisementUtils.toLien(mode, poids, position1, position2);
                lienMap.put(mode, lien);
            } else if (tagName.equals("attr")) {
                if (attributesBuilder == null) {
                    attributesBuilder = new AttributesBuilder();
                }
                AttributeUtils.readAttrElement(attributesBuilder, element, messageHandler, tagName);
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

        private int getPoids(Element element) {
            int poids = 1;
            String poidsString = element.getAttribute("poids");
            if (poidsString.length() > 0) {
                try {
                    poids = Integer.parseInt(poidsString);
                    if (poids < 1) {
                        poids = 1;
                    }
                } catch (NumberFormatException nfe) {
                }
            }
            return poids;
        }

        private int getPosition(Element element, String attributeName) {
            int position = 0;
            String posString = element.getAttribute(attributeName);
            if (posString.length() > 0) {
                try {
                    position = Integer.parseInt(posString);
                    if (position < 0) {
                        position = 0;
                    }
                } catch (NumberFormatException nfe) {
                }
            }
            return position;
        }

    }


    private static class InternalCroisementChange implements CroisementChange {

        private final List<Lien> changedLienList;

        private InternalCroisementChange(List<Lien> changedLienList) {
            this.changedLienList = changedLienList;
        }

        @Override
        public List<String> getRemovedModeList() {
            return CroisementUtils.EMPTY_REMOVEDMODELIST;
        }

        @Override
        public List<Lien> getChangedLienList() {
            return changedLienList;
        }

    }

}
