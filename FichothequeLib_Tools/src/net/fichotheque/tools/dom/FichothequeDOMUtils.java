/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.text.ParseException;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.MetadataEditor;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
public final class FichothequeDOMUtils {

    private FichothequeDOMUtils() {
    }

    public static void readIntitule(Element element, MetadataEditor metadataEditor, MessageHandler messageHandler, String titleName) {
        String name = element.getAttribute("name");
        if (name.length() == 0) {
            return;
        }
        if (name.equals(titleName)) {
            name = null;
        } else if (name.equals("fiche.existing")) {
            name = FichothequeConstants.FICHE_PHRASE;
        } else if (name.equals("fiche.new")) {
            name = FichothequeConstants.NEWFICHE_PHRASE;
        }
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nd = nodeList.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) nd;
                String tagname = child.getTagName();
                if ((tagname.equals("label")) || (tagname.equals("lib"))) {
                    try {
                        Label label = LabelUtils.readLabel(child);
                        if (label != null) {
                            metadataEditor.putLabel(name, label);
                        }
                    } catch (ParseException e) {
                        DomMessages.wrongLangAttribute(messageHandler, "intitule[@name=\"" + name + "\"]", child.getAttribute("xml:lang"));
                    }
                } else {
                    DomMessages.unknownTagWarning(messageHandler, tagname);
                }
            }
        }
    }

}
