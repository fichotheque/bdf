/* FichothequeLib_Tools - Copyright (c) 2021-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom.revisions;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import net.fichotheque.croisement.CroisementRevision;
import net.fichotheque.croisement.Lien;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class CroisementRevisionDOMReader {

    private final RootConsumer rootConsumer = new RootConsumer();

    public CroisementRevision readCroisementRevision(Element element) {
        CroisementRevision croisementRevision = new CroisementRevision();
        DOMUtils.readChildren(element, rootConsumer);
        rootConsumer.flush(croisementRevision);
        return croisementRevision;
    }


    private class RootConsumer implements Consumer<Element> {

        private final Map<String, Lien> lienMap = new LinkedHashMap<String, Lien>();
        private final List<Element> attrTempList = new ArrayList<Element>();

        private RootConsumer() {
        }

        private void clear() {
            lienMap.clear();
            attrTempList.clear();
        }

        private void flush(CroisementRevision croisementRevision) {
            for (Lien lien : lienMap.values()) {
                croisementRevision.addLien(lien);
            }
            clear();
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lien")) {
                String mode = element.getAttribute("mode");
                int poids = getPoids(element);
                int position1 = getPosition(element, "position1");
                int position2 = getPosition(element, "position2");
                Lien lien = CroisementUtils.toLien(mode, poids, position1, position2);
                lienMap.put(mode, lien);
            } else if (tagName.equals("attr")) {
                attrTempList.add(element);
            } else {
            }
        }

        private int getPoids(Element element) {
            int poids = 1;
            String poidsString = element.getAttribute("poids");
            if (poidsString.length() > 0) {
                try {
                    poids = Integer.parseInt(poidsString);
                    if (poids < 1) {
                        poids = 1;
                    }
                } catch (NumberFormatException nfe) {
                }
            }
            return poids;
        }

        private int getPosition(Element element, String attributeName) {
            int position = 0;
            String posString = element.getAttribute(attributeName);
            if (posString.length() > 0) {
                try {
                    position = Integer.parseInt(posString);
                    if (position < 0) {
                        position = 0;
                    }
                } catch (NumberFormatException nfe) {
                }
            }
            return position;
        }

    }

}
