/* FichothequeLib_Tools - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.ParentRecursivityException;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusTreeDOMReader {

    private final ThesaurusEditor thesaurusEditor;
    private final MessageHandler messageHandler;

    public ThesaurusTreeDOMReader(ThesaurusEditor thesaurusEditor, MessageHandler messageHandler) {
        this.thesaurusEditor = thesaurusEditor;
        this.messageHandler = messageHandler;
    }

    public void fillThesaurus(Element element) {
        MotcleConsumer motcleConsumer = new MotcleConsumer(null, "/" + element.getTagName());
        DOMUtils.readChildren(element, motcleConsumer);
    }

    private void addMotcle(Element element, Motcle parent, String xpath) {
        String idString = element.getAttribute("id");
        if (idString.length() == 0) {
            idString = element.getAttribute("idths");
        }
        if (idString.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "id");
            return;
        }
        xpath = xpath + "[@id='" + idString + "']";
        int motcleid;
        try {
            motcleid = Integer.parseInt(idString);
        } catch (NumberFormatException nfe) {
            DomMessages.wrongIntegerAttributeValue(messageHandler, xpath, "id", idString);
            return;
        }
        String idalpha = null;
        if (thesaurusEditor.getThesaurus().isIdalphaType()) {
            idalpha = element.getAttribute("idalpha");
            if (idalpha.length() == 0) {
                DomMessages.emptyAttribute(messageHandler, xpath, "idalpha");
                return;
            }
        }
        Motcle motcle;
        try {
            motcle = thesaurusEditor.createMotcle(motcleid, idalpha);
        } catch (ExistingIdException eie) {
            messageHandler.addMessage(FichothequeConstants.SEVERE_FICHOTHEQUE, "_ error.existing.idalpha", idalpha);
            return;
        } catch (ParseException pe) {
            DomMessages.wrongAttributeValue(messageHandler, xpath, "idalpha", idalpha);
            return;
        }
        thesaurusEditor.setStatus(motcle, getStatus(element));
        if (parent != null) {
            try {
                thesaurusEditor.setParent(motcle, parent);
            } catch (ParentRecursivityException pre) {
                throw new ImplementationException(pre);
            }
        }
        DOMUtils.readChildren(element, new MotcleConsumer(motcle, xpath));
    }

    private String getStatus(Element element) {
        String status = FichothequeConstants.ACTIVE_STATUS;
        String statusAttr = element.getAttribute("status");
        if (!statusAttr.isEmpty()) {
            try {
                status = FichothequeConstants.checkMotcleStatus(statusAttr);
            } catch (IllegalArgumentException iae) {
            }
        }
        return status;
    }


    private class MotcleConsumer implements Consumer<Element> {

        private final Motcle parent;
        private final String parentXpath;

        private MotcleConsumer(Motcle parent, String parentXpath) {
            this.parent = parent;
            this.parentXpath = parentXpath;
        }

        @Override
        public void accept(Element element) {
            String tagname = element.getTagName();
            String xpath = parentXpath + "/" + tagname;
            if (tagname.equals("motcle")) {
                addMotcle(element, parent, xpath);
            } else {
                DomMessages.unknownTagWarning(messageHandler, xpath);
            }

        }

    }

}
