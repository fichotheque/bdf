/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Consumer;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadataEditor;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusMetadataDOMReader extends AbstractMetadataDOMReader {

    private final ThesaurusMetadataEditor thesaurusMetadataEditor;

    public ThesaurusMetadataDOMReader(ThesaurusMetadataEditor thesaurusMetadataEditor, MessageHandler messageHandler) {
        super(thesaurusMetadataEditor, messageHandler, "thesaurus");
        this.thesaurusMetadataEditor = thesaurusMetadataEditor;
    }

    public static short getThesaurusType(Element element) {
        String typeString = element.getAttribute("thesaurus-type");
        return ThesaurusUtils.thesaurusTypeToShort(typeString);
    }

    public void fillMetadata(Element element) {
        if (((ThesaurusMetadata) thesaurusMetadataEditor.getMetadata()).getThesaurusType() != ThesaurusMetadata.BABELIEN_TYPE) {
            String langsString = element.getAttribute("thesaurus-langs");
            if (langsString.length() > 0) {
                Langs langs = LangsUtils.toCleanLangs(langsString);
                if (langs.size() > 0) {
                    thesaurusMetadataEditor.setAuthorizedLangs(langs);
                } else {
                    DomMessages.wrongAttributeValue(messageHandler, element.getTagName(), "thesaurus-langs", langsString);
                }
            }
        }
        AttributesBuilder attributesBuilder = new AttributesBuilder();
        Set<Lang> langSet = new LinkedHashSet();
        DOMUtils.readChildren(element, new RootConsumer(attributesBuilder, langSet, "/" + element.getTagName()));
        if (!langSet.isEmpty()) {
            thesaurusMetadataEditor.setAuthorizedLangs(LangsUtils.fromCollection(langSet));
        }
        flush(attributesBuilder);
    }


    private class RootConsumer implements Consumer<Element> {

        private final AttributesBuilder attributesBuilder;
        private final Set<Lang> langSet;
        private final String parentXpath;

        private RootConsumer(AttributesBuilder attributesBuilder, Set<Lang> langSet, String parentXpath) {
            this.attributesBuilder = attributesBuilder;
            this.langSet = langSet;
            this.parentXpath = parentXpath;
        }

        @Override
        public void accept(Element element) {
            if (readCommonElement(element, attributesBuilder, parentXpath)) {
                return;
            }
            String tagName = element.getTagName();
            String xpath = parentXpath + "/" + tagName;
            if (tagName.equals("langs")) {
                LangsUtils.readLangElements(langSet, element, messageHandler, xpath);
            } else {
                DomMessages.unknownTagWarning(messageHandler, xpath);
            }
        }

    }

}
