/* FichothequeLib_Tools - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.EmailCoreUtils;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.models.PersonCoreUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurDOMReader {

    private final SphereEditor sphereEditor;
    private final MessageHandler messageHandler;
    private final RootConsumer rootConsumer = new RootConsumer();

    public RedacteurDOMReader(SphereEditor sphereEditor, MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.sphereEditor = sphereEditor;
    }

    public void fillRedacteur(Redacteur redacteur, Element element) {
        AttributesBuilder attributesBuilder = new AttributesBuilder();
        rootConsumer.setRedacteur(redacteur, attributesBuilder);
        DOMUtils.readChildren(element, rootConsumer);
        sphereEditor.getFichothequeEditor().putAttributes(redacteur, attributesBuilder.toAttributes());
    }


    private class RootConsumer implements Consumer<Element> {

        private Redacteur redacteur;
        private AttributesBuilder attributesBuilder;

        private RootConsumer() {
        }

        private void setRedacteur(Redacteur redacteur, AttributesBuilder attributesBuilder) {
            this.redacteur = redacteur;
            this.attributesBuilder = attributesBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if ((tagName.equals("person-core")) || (tagName.equals("personne-core")) || tagName.equals("personne-bean")) {
                PersonCore personCore = toPersonCore(element);
                sphereEditor.setPerson(redacteur, personCore);
            } else if ((tagName.equals("email-core")) || (tagName.equals("courriel-core")) || (tagName.equals("courriel-bean"))) {
                EmailCore emailCore = null;
                try {
                    emailCore = toEmailCore(element);
                } catch (ParseException iae) {
                    DomMessages.wrongAttributeValue(messageHandler, tagName, "addr-spec", element.getAttribute("addr-spec"));
                }
                sphereEditor.setEmail(redacteur, emailCore);
            } else if (tagName.equals("attr")) {
                AttributeUtils.readAttrElement(attributesBuilder, element, messageHandler, tagName);
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }

    private static PersonCore toPersonCore(Element element) {
        String surname = element.getAttribute("surname");
        if (surname.length() == 0) {
            surname = element.getAttribute("nom");
        }
        String forename = element.getAttribute("forename");
        if (forename.length() == 0) {
            forename = element.getAttribute("prenom");
        }
        String nonlatin = element.getAttribute("nonlatin");
        if (nonlatin.length() == 0) {
            nonlatin = element.getAttribute("original");
        }
        boolean surnameFirst = false;
        String surnameFirstString = element.getAttribute("surname-first");
        if (surnameFirstString.length() == 0) {
            surnameFirstString = element.getAttribute("nom-avant");
        }
        if (surnameFirstString.equals("true")) {
            surnameFirst = true;
        }
        return PersonCoreUtils.toPersonCore(surname, forename, nonlatin, surnameFirst);
    }

    private static EmailCore toEmailCore(Element element) throws ParseException {
        String addrSpec = element.getAttribute("addr-spec");
        String realName = element.getAttribute("real-name");
        if (realName.length() == 0) {
            realName = element.getAttribute("nom-complet");
        }
        return EmailCoreUtils.parse(addrSpec, realName);
    }

}
