/* FichothequeLib_Tools - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.fichotheque.tools.sphere;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.Fichotheque;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.fichotheque.utils.SphereUtils;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurStats {

    private final Redacteur redacteur;
    private final List<ByCorpusStats> byCorpusStatsList = new ArrayList<ByCorpusStats>();
    private int count = 0;

    public RedacteurStats(Redacteur redacteur) {
        this.redacteur = redacteur;
        init();
    }

    public boolean isEmpty() {
        return byCorpusStatsList.isEmpty();
    }

    public int getFicheCount() {
        return count;
    }

    private void init() {
        Fichotheque fichotheque = redacteur.getFichotheque();
        for (Corpus corpus : fichotheque.getCorpusList()) {
            List<CorpusField> fieldList = CorpusMetadataUtils.getCorpusFieldListByFicheItemType(corpus.getCorpusMetadata(), CorpusField.PERSONNE_FIELD, false);
            ByCorpusStats byCorpusStats;
            if (fieldList.isEmpty()) {
                byCorpusStats = initRedacteur(corpus);
            } else {
                FieldKey[] fieldArray = CorpusMetadataUtils.toFieldKeyArray(fieldList);
                byCorpusStats = initRedacteurAndFields(corpus, fieldArray);
            }
            if (byCorpusStats != null) {
                byCorpusStatsList.add(byCorpusStats);
                count = count + byCorpusStats.getFicheCount();
            }
        }
    }

    private ByCorpusStats initRedacteur(Corpus corpus) {
        ByCorpusStats byCorpusStats = null;
        String globalId = redacteur.getGlobalId();
        for (FicheMeta ficheMeta : corpus.getFicheMetaList()) {
            if (SphereUtils.ownsToRedacteur(ficheMeta, globalId)) {
                if (byCorpusStats == null) {
                    byCorpusStats = new ByCorpusStats(corpus);
                }
                byCorpusStats.add(ficheMeta.getId(), 1);
            }
        }
        return byCorpusStats;
    }

    private ByCorpusStats initRedacteurAndFields(Corpus corpus, FieldKey[] fieldArray) {
        String globalId = redacteur.getGlobalId();
        ByCorpusStats byCorpusStats = null;
        for (FicheMeta ficheMeta : corpus.getFicheMetaList()) {
            int poids = 0;
            if (SphereUtils.ownsToRedacteur(ficheMeta, globalId)) {
                poids = 1;
            }
            FicheAPI fiche = corpus.getFicheAPI(ficheMeta, false);
            for (FieldKey fieldKey : fieldArray) {
                Object value = fiche.getValue(fieldKey);
                if (value != null) {
                    switch (fieldKey.getCategory()) {
                        case FieldKey.PROPRIETE_CATEGORY: {
                            boolean here = SphereUtils.testFicheItem(redacteur, (FicheItem) value);
                            if (here) {
                                poids = poids | 2;
                            }
                            break;
                        }
                        case FieldKey.INFORMATION_CATEGORY: {
                            for (FicheItem ficheItem : (FicheItems) value) {
                                boolean here = SphereUtils.testFicheItem(redacteur, ficheItem);
                                if (here) {
                                    poids = poids | 4;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
            if (poids > 0) {
                if (byCorpusStats == null) {
                    byCorpusStats = new ByCorpusStats(corpus);
                }
                byCorpusStats.add(ficheMeta.getId(), poids);
            }
        }
        return byCorpusStats;
    }


    public static class ByCorpusStats {

        private final Corpus corpus;
        private final SortedMap<Integer, ByFicheStats> treeMap = new TreeMap<Integer, ByFicheStats>();

        ByCorpusStats(Corpus corpus) {
            this.corpus = corpus;
        }

        public int getFicheCount() {
            return treeMap.size();
        }

        public Corpus getCorpus() {
            return corpus;
        }

        private void add(int id, int poids) {
            ByFicheStats byFicheStats = new ByFicheStats(id, poids);
            treeMap.put(id, byFicheStats);
        }

        public List<ByFicheStats> getByFicheStatsList() {
            return new ArrayList<ByFicheStats>(treeMap.values());
        }

    }


    public static class ByFicheStats {

        private final int id;
        private final int poids;

        ByFicheStats(int id, int poids) {
            this.id = id;
            this.poids = poids;
        }

        public int getId() {
            return id;
        }

        public int getPoids() {
            return poids;
        }

    }

    public List<ByCorpusStats> getByCorpusStatsList() {
        return byCorpusStatsList;
    }

}
