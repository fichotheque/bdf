/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2018 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport;

import java.util.Date;
import net.scrutari.dataexport.api.ScrutariDataExport;
import net.scrutari.dataexport.xml.XmlScrutariDataExport;
import net.scrutari.dataexport.xml.XmlWriter;


/**
 *
 * @author Vincent Calame
 */
public final class ScrutariDataExportFactory {

    private ScrutariDataExportFactory() {
    }

    public static ScrutariDataExport newInstance(Appendable appendable, int indentLength, boolean includeXMLDeclaration) {
        XmlWriter xmlWriter = new XmlWriter(appendable, indentLength, includeXMLDeclaration);
        return new XmlScrutariDataExport(xmlWriter);
    }

    public static ScrutariDataExport newInstance(Appendable appendable, int indentLength, boolean includeXMLDeclaration, Date date) {
        XmlWriter xmlWriter = new XmlWriter(appendable, indentLength, includeXMLDeclaration);
        return new XmlScrutariDataExport(xmlWriter, date);
    }

}
