/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2018 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
class AttributeBuffer {

    private final static String[] ATTRIBUTES = {"ns", "key"};
    private final String[] array = new String[2];
    private final List<String> valueList = new ArrayList<String>();

    AttributeBuffer(String nameSpace, String localKey) {
        this.array[0] = nameSpace;
        this.array[1] = localKey;
    }

    void addValue(String value) {
        valueList.add(value);
    }

    void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTagWithAttributes("attr", ATTRIBUTES, array);
        for (String value : valueList) {
            xmlWriter.addSimpleElement("val", value);
        }
        xmlWriter.closeTag("attr");
    }

}
