/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.LinkedHashMap;
import java.util.Map;
import net.scrutari.dataexport.api.MotcleExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlMotcleExport extends XmlAttributeExport implements MotcleExport {


    private final Map<String, String> libelleMap = new LinkedHashMap<String, String>();
    private String motcleId;

    public XmlMotcleExport() {

    }

    public void reinit(String motcleId) {
        this.motcleId = motcleId;
        this.libelleMap.clear();
        super.clear();
    }

    @Override
    public void setLibelle(String lang, String libelleValue) {
        if (libelleValue == null) {
            libelleValue = "";
        } else {
            libelleValue = libelleValue.trim();
        }
        if (libelleValue.isEmpty()) {
            libelleMap.remove(lang);
        } else {
            libelleMap.put(lang, libelleValue);
        }
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTagWithAttribute("motcle", "motcle-id", motcleId);
        for (Map.Entry<String, String> entry : libelleMap.entrySet()) {
            xmlWriter.addLibElement(entry.getKey(), entry.getValue());
        }
        writeAttributes(xmlWriter);
        xmlWriter.closeTag("motcle");
    }

}
