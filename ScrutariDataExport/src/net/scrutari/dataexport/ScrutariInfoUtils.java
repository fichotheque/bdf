/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2018 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport;

import java.text.SimpleDateFormat;
import java.util.Date;
import net.scrutari.dataexport.xml.XmlWriter;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariInfoUtils {

    private final static SimpleDateFormat ISO_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private ScrutariInfoUtils() {
    }

    public static void writeScrutariInfo(Appendable appendable, int indentLength, boolean includeXMLDeclaration, Date date, String[] urlArray) {
        XmlWriter xmlWriter = new XmlWriter(appendable, indentLength, includeXMLDeclaration);
        xmlWriter.openTag("info");
        xmlWriter.addSimpleElement("date", ISO_FORMAT.format(date));
        int length = urlArray.length;
        for (int i = 0; i < length; i++) {
            xmlWriter.addSimpleElement("scrutaridata-url", urlArray[i]);
        }
        xmlWriter.closeTag("info");
    }

}
