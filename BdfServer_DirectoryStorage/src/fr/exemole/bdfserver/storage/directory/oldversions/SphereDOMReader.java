/* BdfServer_DirectoryStorage - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.oldversions;

import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.Metadata;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.fichotheque.sphere.metadata.SphereMetadataEditor;
import net.fichotheque.tools.dom.FichothequeDOMUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.EmailCoreUtils;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.models.PersonCoreUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Correspond à un format XML supprimé à la révision 1590.
 *
 * @author Vincent Calame
 */
public class SphereDOMReader {

    private final SphereEditor sphereEditor;
    private final SphereMetadataEditor sphereMetadataEditor;
    private final MessageHandler messageHandler;

    public SphereDOMReader(SphereEditor sphereEditor, MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.sphereEditor = sphereEditor;
        this.sphereMetadataEditor = sphereEditor.getSphereMetadataEditor();
    }

    public void fillSphere(Element element_xml) {
        boolean metadone = false;
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            if (liste.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                String tagname = element.getTagName();
                if ((!metadone) && (tagname.equals("metadata"))) {
                    metadone = true;
                    fillMetadata(element);
                } else if (tagname.equals("redacteur")) {
                    String login = element.getAttribute("idsphere");
                    try {
                        Redacteur redacteur = sphereEditor.createRedacteur(-1, login);
                        String actifString = element.getAttribute("actif");
                        if (actifString.equals("0")) {
                            sphereEditor.setStatus(redacteur, FichothequeConstants.INACTIVE_STATUS);
                        }
                        NodeList listered = element.getChildNodes();
                        for (int j = 0; j < listered.getLength(); j++) {
                            if (listered.item(j).getNodeType() == Node.ELEMENT_NODE) {
                                Element elementred = (Element) listered.item(j);
                                String tagnamered = elementred.getTagName();
                                if ((tagnamered.equals("personne-core")) || tagnamered.equals("personne-bean")) {
                                    PersonCore personCore = toPersonCore(elementred);
                                    sphereEditor.setPerson(redacteur, personCore);
                                } else if ((tagnamered.equals("courriel-core")) || (tagnamered.equals("courriel-bean"))) {
                                    EmailCore emailCore = null;
                                    try {
                                        emailCore = toEmailCore(elementred);
                                    } catch (ParseException iae) {
                                        DomMessages.wrongAttributeValue(messageHandler, tagnamered, "addr-spec", elementred.getAttribute("addr-spec"));
                                    }
                                    sphereEditor.setEmail(redacteur, emailCore);
                                } else {
                                    DomMessages.unknownTagWarning(messageHandler, tagname);
                                }
                            }
                        }
                    } catch (ParseException pe) {
                        DomMessages.wrongAttributeValue(messageHandler, tagname, "idsphere", login);

                    } catch (ExistingIdException eie) {
                        messageHandler.addMessage(FichothequeConstants.SEVERE_FICHOTHEQUE, "_ error.existing.login", login);
                    }
                } else {
                    DomMessages.unknownTagWarning(messageHandler, tagname);
                }
            }
        }

    }

    private void fillMetadata(Element element_xml) {
        Metadata metadata = sphereMetadataEditor.getMetadata();
        AttributesBuilder attributesBuilder = new AttributesBuilder();
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) nd;
                String tagname = el.getTagName();
                if (tagname.equals("intitule")) {
                    FichothequeDOMUtils.readIntitule(el, sphereMetadataEditor, messageHandler, "sphere");
                } else if (tagname.equals("attr")) {
                    AttributeUtils.readAttrElement(attributesBuilder, el, messageHandler, tagname);
                } else {
                    DomMessages.unknownTagWarning(messageHandler, tagname);
                }
            }
        }
        sphereEditor.getFichothequeEditor().putAttributes(metadata, attributesBuilder.toAttributes());
    }

    private static PersonCore toPersonCore(Element element) {
        String surname = element.getAttribute("nom");
        String forename = element.getAttribute("prenom");
        String nonlatin = element.getAttribute("original");
        boolean surnameFirst = false;
        String surnameFirstString = element.getAttribute("nom-avant");
        if (surnameFirstString.equals("true")) {
            surnameFirst = true;
        }
        //deprécié
        String strg_style = element.getAttribute("style");
        if (strg_style.length() > 0) {
            surnameFirst = true;
        }
        return PersonCoreUtils.toPersonCore(surname, forename, nonlatin, surnameFirst);
    }

    private static EmailCore toEmailCore(Element element) throws ParseException {
        String addrSpec = element.getAttribute("addr-spec");
        String realName = element.getAttribute("real-name");
        if (realName.length() == 0) {
            realName = element.getAttribute("nom-complet");
        }
        return EmailCoreUtils.parse(addrSpec, realName);
    }

}
