/* BdfServer_DirectoryStorage - Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.oldversions;

import fr.exemole.bdfserver.tools.BdfServerUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import net.mapeadores.util.primitives.FuzzyDate;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
class ChronoDOMReader {

    ChronoDOMReader() {
    }

    void migrateChrono(File currentCorpusDir, Element element_xml) throws IOException {
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) nd;
                String tagname = el.getTagName();
                if (tagname.equals("ch")) {
                    Object[] array = getArray(el);
                    if (array != null) {
                        int ficheid = (Integer) array[0];
                        File millierDir = new File(currentCorpusDir, "f." + BdfServerUtils.getMillier(ficheid));
                        if (millierDir.exists()) {
                            String idString = String.valueOf(ficheid);
                            File ficheDir = new File(millierDir, idString);
                            File ficheFile = new File(millierDir, idString + ".xml");
                            if ((ficheDir.exists()) || (ficheFile.exists())) {
                                ficheDir.mkdir();
                                File chrono = new File(ficheDir, "chrono.txt");
                                FuzzyDate creationDate = (FuzzyDate) array[1];
                                FuzzyDate modificationDate = (FuzzyDate) array[2];
                                try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(chrono), "UTF-8"))) {
                                    if (creationDate != null) {
                                        writer.write(creationDate.toString());
                                        if (FuzzyDate.compare(modificationDate, creationDate) > 0) {
                                            writer.write("\n");
                                            writer.write(modificationDate.toString());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private Object[] getArray(Element element_xml) {
        String idString = element_xml.getAttribute("id");
        if (idString.length() == 0) {
            return null;
        }
        int id;
        try {
            id = Integer.parseInt(idString);
        } catch (NumberFormatException nfe) {
            return null;
        }
        String creationDateString = element_xml.getAttribute("c");
        if (creationDateString.length() == 0) {
            return null;
        }
        FuzzyDate creationDate;
        try {
            creationDate = FuzzyDate.parse(creationDateString);
        } catch (ParseException ide) {
            return null;
        }
        FuzzyDate modificationDate = null;
        String modificationDateString = element_xml.getAttribute("m");
        if (modificationDateString.length() > 0) {
            try {
                modificationDate = FuzzyDate.parse(modificationDateString);

            } catch (ParseException e) {
                return null;
            }
        }
        Object[] result = new Object[3];
        result[0] = id;
        result[1] = creationDate;
        result[2] = modificationDate;
        return result;
    }

}
