/* BdfServer - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.oldversions;

import fr.exemole.bdfserver.api.BdfServerEditor;
import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import fr.exemole.bdfserver.tools.configuration.LangConfigurationBuilder;
import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class ConfigurationManagerDOMReader {

    private final MessageHandler messageHandler;
    private BdfServerEditor bdfServerEditor;

    public ConfigurationManagerDOMReader(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public void readConfigurationManager(BdfServerEditor bdfServerEditor, Element element) {
        this.bdfServerEditor = bdfServerEditor;
        DOMUtils.readChildren(element, new RootConsumer());
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lang-config")) {
                LangConfigConsumer langConfigConsumer = new LangConfigConsumer();
                DOMUtils.readChildren(element, langConfigConsumer);
                bdfServerEditor.setLangConfiguration(langConfigConsumer.toLangConfiguration());
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }


    private class LangConfigConsumer implements Consumer<Element> {

        private final LangConfigurationBuilder langConfigurationBuilder = new LangConfigurationBuilder();

        private LangConfigConsumer() {
            langConfigurationBuilder.setAllLanguages(false);
        }

        public LangConfiguration toLangConfiguration() {
            return langConfigurationBuilder.toLangConfiguration();
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("all-languages")) {
                langConfigurationBuilder.setAllLanguages(true);
            } else if (tagName.equals("with-original-name")) {
                langConfigurationBuilder.setWithNonlatin(true);
            } else if (tagName.equals("working-lang")) {
                try {
                    Lang lang = Lang.parse(element.getAttribute("lang"));
                    langConfigurationBuilder.addWorkingLang(lang);
                } catch (ParseException pe) {
                }
            } else if (tagName.equals("supplementary-lang")) {
                try {
                    Lang langInteger = Lang.parse(element.getAttribute("lang"));
                    langConfigurationBuilder.addSupplementaryLang(langInteger);
                } catch (ParseException pe) {
                }
            } else if (tagName.equals("without-nom-avant")) {
                langConfigurationBuilder.setWithoutSurnameFirst(true);
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        }

    }

}
