/* BdfServer_DirectoryStorage - Copyright (c) 2008-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.oldversions;

import java.util.Collections;
import java.util.List;
import net.fichotheque.Subset;
import net.fichotheque.SubsetItem;
import net.fichotheque.croisement.CroisementChange;
import net.fichotheque.croisement.CroisementEditor;
import net.fichotheque.croisement.Lien;
import net.mapeadores.util.xml.DomMessages;
import net.fichotheque.utils.CroisementUtils;
import net.mapeadores.util.logging.MessageHandler;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
public class OldCroisementDOMReader {

    private final MessageHandler messageHandler;

    public OldCroisementDOMReader(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public void readCroisement(CroisementEditor croisementEditor, Subset subset1, Subset subset2, Element element_xml) {
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) nd;
                String tagname = el.getTagName();
                if (tagname.equals("ci")) {
                    try {
                        InternalCroisementChange croisementChange = getCroisementChange(subset1, subset2, el);
                        if (croisementChange != null) {
                            croisementEditor.updateCroisement(croisementChange.subsetItem1, croisementChange.subsetItem2, croisementChange);
                        }
                    } catch (IllegalArgumentException iae) {
                    }
                } else {
                    DomMessages.unknownTagWarning(messageHandler, tagname);
                }
            }
        }
    }

    private InternalCroisementChange getCroisementChange(Subset subset1, Subset subset2, Element element) {
        int id1 = getAttribute(element, "id1", false);
        SubsetItem subsetItem1 = subset1.getSubsetItemById(id1);
        if (subsetItem1 == null) {
            return null;
        }
        int id2 = getAttribute(element, "id2", false);
        SubsetItem subsetItem2 = subset2.getSubsetItemById(id2);
        if (subsetItem2 == null) {
            return null;
        }
        int poids = getAttribute(element, "poids", true);
        if (poids == -1) {
            poids = 1;
        }
        int position1 = getAttribute(element, "pos1", true) + 1;
        int position2 = getAttribute(element, "pos2", true) + 1;
        Lien lien = CroisementUtils.toLien("", poids, position1, position2);
        return new InternalCroisementChange(subsetItem1, subsetItem2, lien);
    }

    private int getAttribute(Element element, String name, boolean emptyallowed) {
        String s = element.getAttribute(name);
        if (s.length() == 0) {
            if (emptyallowed) {
                return -1;
            }
            DomMessages.emptyAttribute(messageHandler, element.getTagName(), name);
            throw new IllegalArgumentException();
        }
        try {
            int entier = Integer.parseInt(s);
            if (entier < 0) {
                entier = -1;
            }
            return entier;
        } catch (NumberFormatException nfe) {
            DomMessages.wrongIntegerAttributeValue(messageHandler, element.getTagName(), name, s);
            throw new IllegalArgumentException();
        }
    }


    private static class InternalCroisementChange implements CroisementChange {

        private final List<Lien> lienList;
        private final SubsetItem subsetItem1;
        private final SubsetItem subsetItem2;

        private InternalCroisementChange(SubsetItem subsetItem1, SubsetItem subsetItem2, Lien lien) {
            this.lienList = Collections.singletonList(lien);
            this.subsetItem1 = subsetItem1;
            this.subsetItem2 = subsetItem2;
        }

        @Override
        public List<String> getRemovedModeList() {
            return CroisementUtils.EMPTY_REMOVEDMODELIST;
        }

        @Override
        public List<Lien> getChangedLienList() {
            return lienList;
        }

    }

}
