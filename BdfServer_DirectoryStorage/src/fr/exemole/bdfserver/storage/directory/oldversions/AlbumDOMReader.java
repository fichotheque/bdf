/* BdfServer_DirectoryStorage - Copyright (c)2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.oldversions;

import java.text.ParseException;
import net.fichotheque.ExistingNameException;
import net.fichotheque.album.AlbumEditor;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.album.metadata.AlbumMetadata;
import net.fichotheque.album.metadata.AlbumMetadataEditor;
import net.fichotheque.tools.dom.FichothequeDOMUtils;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Correspond à un format XML supprimé à la révision 1590.
 *
 * @author Vincent Calame
 */
public class AlbumDOMReader {

    private final AlbumEditor albumEditor;
    private final AlbumMetadataEditor albumMetadataEditor;
    private final MessageHandler messageHandler;

    public AlbumDOMReader(AlbumEditor albumEditor, MessageHandler messageHandler) {
        this.albumEditor = albumEditor;
        this.albumMetadataEditor = albumEditor.getAlbumMetadataEditor();
        this.messageHandler = messageHandler;
    }

    public void fillAlbum(Element element_xml) {
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            if (liste.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                String tagname = element.getTagName();
                if (tagname.equals("metadata")) {
                    fillMetadata(element);
                } else {
                    DomMessages.unknownTagWarning(messageHandler, tagname);
                }
            }
        }
    }

    private void fillMetadata(Element element_xml) {
        AlbumMetadata metadata = (AlbumMetadata) albumMetadataEditor.getMetadata();
        AttributesBuilder attributesBuilder = new AttributesBuilder();
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) nd;
                String tagname = el.getTagName();
                if (tagname.equals("intitule")) {
                    FichothequeDOMUtils.readIntitule(el, albumMetadataEditor, messageHandler, "album");
                } else if (tagname.equals("album-dim")) {
                    addAlbumDim(el);
                } else if (tagname.equals("dim-usage")) {

                } else if (tagname.equals("attr")) {
                    AttributeUtils.readAttrElement(attributesBuilder, el, messageHandler, tagname);
                } else {
                    DomMessages.unknownTagWarning(messageHandler, tagname);
                }
            }
        }
        albumEditor.getFichothequeEditor().putAttributes(metadata, attributesBuilder.toAttributes());
    }

    private void addAlbumDim(Element element_xml) {
        String tagName = "album-dim";
        String name = element_xml.getAttribute("name");
        if (name.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, tagName, "name");
            return;
        }
        tagName = tagName + " name=\"" + name + "\"";
        String type = element_xml.getAttribute("type");
        if (type.isEmpty()) {
            DomMessages.emptyAttribute(messageHandler, tagName, "type");
            return;
        }
        try {
            type = AlbumUtils.checkDimType(type);
        } catch (IllegalArgumentException iae) {
            DomMessages.wrongAttributeValue(messageHandler, tagName, "type", type);
            return;
        }
        int width = 0;
        int height = 0;
        if (AlbumUtils.needWidth(type)) {
            width = getInt(element_xml, tagName, "width");
            if (width == -1) {
                return;
            }
        }
        if (AlbumUtils.needHeight(type)) {
            height = getInt(element_xml, tagName, "height");
            if (height == -1) {
                return;
            }
        }
        AlbumDim albumDim;
        try {
            albumDim = albumMetadataEditor.createAlbumDim(name, type);
        } catch (ExistingNameException | ParseException e) {
            return;
        }
        albumMetadataEditor.setDim(albumDim, width, height);
    }

    private int getInt(Element element_xml, String tagName, String attributeName) {
        String intString = element_xml.getAttribute(attributeName);
        int result;
        if (intString.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, tagName, attributeName);
            return -1;
        }
        try {
            result = Integer.parseInt(intString);
        } catch (NumberFormatException nfe) {
            DomMessages.wrongIntegerAttributeValue(messageHandler, tagName, attributeName, intString);
            return -1;
        }
        if (result < 1) {
            DomMessages.wrongIntegerAttributeValue(messageHandler, tagName, attributeName, intString);
            return -1;
        }
        return result;
    }

}
