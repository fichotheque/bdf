/* BdfServer_DirectoryStorage - Copyright (c) 2010-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.oldversions;

import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Correspond à un format XML supprimé à la révision 1590.
 *
 * @author Vincent Calame
 */
public class AddendaDOMReader {


    private AddendaDOMReader() {

    }

    public static Map<Integer, String> getNameMap(Element element_xml) {
        Map<Integer, String> nameMap = new HashMap<Integer, String>();
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) nd;
                String tagname = el.getTagName();
                if (tagname.equals("document")) {
                    checkDocument(nameMap, el);
                }
            }
        }
        return nameMap;
    }

    private static void checkDocument(Map<Integer, String> nameMap, Element element_xml) {
        String idString = element_xml.getAttribute("id");
        if (idString.length() == 0) {
            idString = element_xml.getAttribute("idaddenda");
        }
        if (idString.length() == 0) {
            idString = element_xml.getAttribute("idsubset");
        }
        try {
            int id = Integer.parseInt(idString);
            String basename = element_xml.getAttribute("basename");
            if (basename.length() > 0) {
                nameMap.put(id, basename);
            }
        } catch (NumberFormatException nfe) {

        }
    }

}
