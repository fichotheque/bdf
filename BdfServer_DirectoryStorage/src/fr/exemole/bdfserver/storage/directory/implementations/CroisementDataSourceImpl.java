/* BdfServer_DirectoryStorage - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.storage.directory.bdfdata.BdfStorageUtils;
import fr.exemole.bdfserver.storage.directory.bdfdata.Remove;
import fr.exemole.bdfserver.storage.directory.bdfdata.Save;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageFile;
import net.fichotheque.EditOrigin;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.croisement.CroisementKey;
import net.fichotheque.croisement.CroisementRevision;
import net.fichotheque.impl.CroisementDataSource;
import net.fichotheque.tools.dom.revisions.CroisementRevisionDOMReader;
import org.w3c.dom.Document;


/**
 *
 * @author Vincent Calame
 */
public class CroisementDataSourceImpl implements CroisementDataSource {

    private final StorageDirectories storageDirectories;

    public CroisementDataSourceImpl(StorageDirectories storageDirectories) {
        this.storageDirectories = storageDirectories;
    }

    @Override
    public void saveCroisement(Croisement croisement, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveCroisement(storageDirectories, croisement, editOrigin);
    }

    @Override
    public void removeCroisement(CroisementKey croisementKey, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeCroisement(storageDirectories, croisementKey, editOrigin);
    }

    @Override
    public CroisementRevision getCroisementRevision(CroisementKey croisementKey, String revisionName) {
        StorageFile storageFile = BdfStorageUtils.getCroisementStorageFile(storageDirectories, croisementKey);
        Document document = storageFile.readDocument(revisionName);
        if (document == null) {
            return null;
        }
        CroisementRevisionDOMReader croisementRevisionDOMReader = new CroisementRevisionDOMReader();
        return croisementRevisionDOMReader.readCroisementRevision(document.getDocumentElement());
    }

}
