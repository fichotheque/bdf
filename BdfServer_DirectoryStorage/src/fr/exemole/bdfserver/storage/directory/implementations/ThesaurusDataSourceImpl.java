/* BdfServer_DirectoryStorage - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.storage.directory.bdfdata.Remove;
import fr.exemole.bdfserver.storage.directory.bdfdata.Save;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import net.fichotheque.EditOrigin;
import net.fichotheque.impl.ThesaurusDataSource;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusDataSourceImpl implements ThesaurusDataSource {

    private StorageDirectories storageDirectories;

    public ThesaurusDataSourceImpl(StorageDirectories storageDirectories) {
        this.storageDirectories = storageDirectories;
    }

    @Override
    public void saveMetadata(Thesaurus thesaurus, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveThesaurusMetadata(storageDirectories, thesaurus, editOrigin);
    }

    @Override
    public void saveTree(Thesaurus thesaurus, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveThesaurusTree(storageDirectories, thesaurus, editOrigin);
    }

    @Override
    public void saveMotcle(Motcle motcle, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveMotcle(storageDirectories, motcle, editOrigin);
    }

    @Override
    public void removeMotcle(Thesaurus thesaurus, int id, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeMotcle(storageDirectories, thesaurus, id, editOrigin);
    }

}
