/* BdfServer_DirectoryStorage - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.api.configuration.LangConfiguration;
import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.api.storage.ConfigurationStorage;
import fr.exemole.bdfserver.storage.directory.bdfdata.BdfDirectoryConstants;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageFile;
import fr.exemole.bdfserver.tools.configuration.LangConfigurationBuilder;
import fr.exemole.bdfserver.tools.configuration.dom.LangConfigurationDOMReader;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import fr.exemole.bdfserver.xml.configuration.LangConfigurationXMLPart;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.logging.MultiMessageHandler;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Document;


/**
 *
 * @author Vincent Calame
 */
public class ConfigurationStorageImpl implements ConfigurationStorage {

    private final StorageDirectories storageDirectories;

    public ConfigurationStorageImpl(StorageDirectories storageDirectories) {
        this.storageDirectories = storageDirectories;
    }

    public LangConfiguration checkLangConfiguration(MultiMessageHandler messageHandler) {
        LangConfigurationBuilder langConfigurationBuilder = new LangConfigurationBuilder();
        StorageFile storageFile = StorageFile.build(storageDirectories, BdfDirectoryConstants.CONF + File.separator + "lang-configuration.xml");
        if (storageFile.exists()) {
            messageHandler.setCurrentSource(storageFile.toURI());
            Document doc = storageFile.readDocument();
            LangConfigurationDOMReader langConfigurationDOMReader = new LangConfigurationDOMReader(langConfigurationBuilder, messageHandler);
            langConfigurationDOMReader.readLangConfiguration(doc.getDocumentElement());
        }
        return langConfigurationBuilder.toLangConfiguration();
    }

    public List<String> checkActiveExtensionList(MultiMessageHandler messageHandler) {
        List<String> activeExtensionList = new ArrayList<String>();
        StorageFile storageFile = StorageFile.build(storageDirectories, BdfDirectoryConstants.CONF + File.separator + "extensions");
        List<String> lines = storageFile.readLines();
        if (lines != null) {
            for (String line : lines) {
                line = line.trim();
                if (line.length() > 0) {
                    activeExtensionList.add(line);
                }
            }
        }
        return activeExtensionList;
    }

    @Override
    public void saveLangConfiguration(LangConfiguration langConfiguration) {
        StorageFile storageFile = StorageFile.build(storageDirectories, BdfDirectoryConstants.CONF + File.separator + "lang-configuration.xml");
        try (BufferedWriter buf = storageFile.getWriter()) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            LangConfigurationXMLPart xmlPart = new LangConfigurationXMLPart(xmlWriter);
            xmlPart.addLangConfiguration(langConfiguration);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    @Override
    public void saveActiveExtensionList(List<String> activeExtensionList) {
        StorageFile storageFile = StorageFile.build(storageDirectories, BdfDirectoryConstants.CONF + File.separator + "extensions");
        try (BufferedWriter buf = storageFile.getWriter()) {
            StorageUtils.writeExtensionList(buf, activeExtensionList);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

}
