/* BdfServer_DirectoryStorage - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.api.storage.BdfExtensionStorage;
import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import fr.exemole.bdfserver.conf.ConfConstants;
import java.io.File;


/**
 *
 * @author Vincent Calame
 */
public class BdfExtensionStorageImpl implements BdfExtensionStorage {

    private final static String SUBDIR = "extdata";
    private final File varDirectory;
    private final File backupDirectory;

    public BdfExtensionStorageImpl(BdfServerDirs dirs) {
        this.varDirectory = dirs.getSubPath(ConfConstants.VAR_DATA, SUBDIR);
        this.backupDirectory = dirs.getSubPath(ConfConstants.VAR_BACKUP, SUBDIR);
    }

    @Override
    public File getDataDirectory(String extensionName) {
        File dir = new File(varDirectory, extensionName);
        if ((dir.exists()) && (!dir.isDirectory())) {
            throw new BdfStorageException(dir.getAbsolutePath() + " is not a directory");
        }
        return dir;
    }

    @Override
    public File getBackupDirectory(String extensionName) {
        if (backupDirectory == null) {
            return null;
        }
        File dir = new File(backupDirectory, extensionName);
        if ((dir.exists()) && (!dir.isDirectory())) {
            throw new BdfStorageException(dir.getAbsolutePath() + " is not a directory");
        }
        return dir;
    }

}
