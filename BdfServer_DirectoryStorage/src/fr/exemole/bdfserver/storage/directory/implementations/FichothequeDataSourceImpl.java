/* BdfServer_DirectoryStorage - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.storage.directory.bdfdata.Remove;
import fr.exemole.bdfserver.storage.directory.bdfdata.Save;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import fr.exemole.bdfserver.storage.directory.jdbm.FichothequeJdbm;
import java.io.File;
import net.fichotheque.EditOrigin;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.impl.AddendaDataSource;
import net.fichotheque.impl.AlbumDataSource;
import net.fichotheque.impl.CorpusDataSource;
import net.fichotheque.impl.CroisementDataSource;
import net.fichotheque.impl.FichothequeDataSource;
import net.fichotheque.impl.SphereDataSource;
import net.fichotheque.impl.ThesaurusDataSource;
import net.fichotheque.metadata.FichothequeMetadata;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeDataSourceImpl implements FichothequeDataSource {

    private final StorageDirectories storageDirectories;
    private final AddendaDataSourceImpl addendaDataSourceImpl;
    private final AlbumDataSourceImpl albumDataSourceImpl;
    private final CorpusDataSourceImpl corpusDataSourceImpl;
    private final SphereDataSourceImpl sphereDataSourceImpl;
    private final ThesaurusDataSourceImpl thesaurusDataSourceImpl;
    private final CroisementDataSourceImpl croisementDataSourceImpl;

    public FichothequeDataSourceImpl(StorageDirectories storageDirectories, File cacheDir, FichothequeJdbm fichothequeJdbm) {
        this.storageDirectories = storageDirectories;
        this.addendaDataSourceImpl = new AddendaDataSourceImpl(storageDirectories);
        this.albumDataSourceImpl = new AlbumDataSourceImpl(storageDirectories, cacheDir);
        this.corpusDataSourceImpl = new CorpusDataSourceImpl(storageDirectories, fichothequeJdbm);
        this.sphereDataSourceImpl = new SphereDataSourceImpl(storageDirectories);
        this.thesaurusDataSourceImpl = new ThesaurusDataSourceImpl(storageDirectories);
        this.croisementDataSourceImpl = new CroisementDataSourceImpl(storageDirectories);
    }

    @Override
    public AddendaDataSource getAddendaDataSource() {
        return addendaDataSourceImpl;
    }

    @Override
    public AlbumDataSource getAlbumDataSource() {
        return albumDataSourceImpl;
    }

    @Override
    public CorpusDataSource getCorpusDataSource() {
        return corpusDataSourceImpl;
    }

    @Override
    public SphereDataSource getSphereDataSource() {
        return sphereDataSourceImpl;
    }

    @Override
    public ThesaurusDataSource getThesaurusDataSource() {
        return thesaurusDataSourceImpl;
    }

    @Override
    public CroisementDataSource getCroisementDataSource() {
        return croisementDataSourceImpl;
    }

    @Override
    public void saveFichothequeMetadata(FichothequeMetadata fichothequeMetadata, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveFichothequeMetadata(storageDirectories, fichothequeMetadata, editOrigin);
    }

    @Override
    public void removeAddenda(SubsetKey addendaKey, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeAddenda(storageDirectories, addendaKey, editOrigin);
    }

    @Override
    public void removeAlbum(SubsetKey albumKey, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeAlbum(storageDirectories, albumKey, editOrigin);
    }

    @Override
    public void removeCorpus(SubsetKey corpusKey, Subset masterSubset, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeCorpus(storageDirectories, corpusKey, masterSubset, editOrigin);
    }

    @Override
    public void removeSphere(SubsetKey sphereKey, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeSphere(storageDirectories, sphereKey, editOrigin);
    }

    @Override
    public void removeThesaurus(SubsetKey thesaurusKey, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeThesaurus(storageDirectories, thesaurusKey, editOrigin);
    }

}
