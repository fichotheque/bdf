/* BdfServer_DirectoryStorage - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.storage.directory.bdfdata.DocumentDirectory;
import fr.exemole.bdfserver.storage.directory.bdfdata.Remove;
import fr.exemole.bdfserver.storage.directory.bdfdata.Save;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import net.fichotheque.EditOrigin;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.addenda.VersionInfo;
import net.fichotheque.impl.AddendaDataSource;


/**
 *
 * @author Vincent Calame
 */
public class AddendaDataSourceImpl implements AddendaDataSource {

    private final StorageDirectories storageDirectories;

    public AddendaDataSourceImpl(StorageDirectories storageDirectories) {
        this.storageDirectories = storageDirectories;
    }

    @Override
    public void saveMetadata(Addenda addenda, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveAddendaMetadata(storageDirectories, addenda, editOrigin);
    }

    @Override
    public void saveDocument(Document document, EditOrigin editOrigin) {
        Save.saveDocument(storageDirectories, document, editOrigin);
    }

    @Override
    public void removeDocument(Addenda addenda, int id, EditOrigin editOrign) {
        Remove.removeDocument(storageDirectories, addenda, id, editOrign);
    }

    @Override
    public InputStream getInputStream(Document document, String extension) {
        DocumentDirectory documentDirectory = getDocumentDirectory(document);
        return documentDirectory.getInputStream(extension);
    }

    @Override
    public boolean deleteVersion(Document document, String extension) {
        DocumentDirectory documentDirectory = getDocumentDirectory(document);
        return documentDirectory.delete(extension);
    }

    @Override
    public VersionInfo saveVersion(Document document, String extension, InputStream inputStream) {
        DocumentDirectory documentDirectory = getDocumentDirectory(document);
        return documentDirectory.saveInputStream(extension, inputStream);
    }

    @Override
    public boolean linkTo(Document document, String extension, Path destination, boolean symbolicLink) throws IOException {
        DocumentDirectory documentDirectory = getDocumentDirectory(document);
        return documentDirectory.linkTo(extension, destination, symbolicLink);
    }

    private DocumentDirectory getDocumentDirectory(Document document) {
        return DocumentDirectory.getDocumentDirectory(storageDirectories, document.getSubsetKey(), document.getId());
    }

}
