/* BdfServer_DirectoryStorage - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.storage.directory.bdfdata.Remove;
import fr.exemole.bdfserver.storage.directory.bdfdata.Save;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import net.fichotheque.EditOrigin;
import net.fichotheque.impl.SphereDataSource;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;


/**
 *
 * @author Vincent Calame
 */
public class SphereDataSourceImpl implements SphereDataSource {

    private StorageDirectories storageDirectories;

    public SphereDataSourceImpl(StorageDirectories storageDirectories) {
        this.storageDirectories = storageDirectories;
    }

    @Override
    public void saveMetadata(Sphere sphere, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveSphereMetadata(storageDirectories, sphere, editOrigin);
    }

    @Override
    public void saveList(Sphere sphere, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveSphereList(storageDirectories, sphere, editOrigin);
    }

    @Override
    public void saveRedacteur(Redacteur redacteur, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Save.saveRedacteur(storageDirectories, redacteur, editOrigin);
    }

    @Override
    public void removeRedacteur(Sphere sphere, int id, EditOrigin editOrigin) {
        if (editOrigin == null) {
            throw new IllegalArgumentException("editOrigin is null");
        }
        Remove.removeRedacteur(storageDirectories, sphere, id, editOrigin);
    }

}
