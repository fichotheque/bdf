/* BdfServer_DirectoryStorage - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.api.storage.RedacteurListStorage;
import fr.exemole.bdfserver.storage.directory.bdfdata.BdfDirectoryConstants;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageFile;
import fr.exemole.bdfserver.tools.roles.dom.RedacteurListDOMReader;
import fr.exemole.bdfserver.xml.roles.RedacteurListXMLPart;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.fichotheque.Fichotheque;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.logging.MultiMessageHandler;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Document;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurListStorageImpl implements RedacteurListStorage {

    private final StorageDirectories storageDirectories;
    private final String rootPath = BdfDirectoryConstants.CONF + File.separator + "redacteurlists";

    public RedacteurListStorageImpl(StorageDirectories storageDirectories) {
        this.storageDirectories = storageDirectories;
    }

    @Override
    public void saveRedacteurList(String name, List<Redacteur> redacteurList) {
        StorageFile storageFile = getRedacteurListStorageFile(name);
        try (BufferedWriter buf = storageFile.getWriter()) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            RedacteurListXMLPart xmlPart = new RedacteurListXMLPart(xmlWriter);
            xmlPart.addRedacteurList(redacteurList);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    @Override
    public void removeRedacteurList(String name) {
        StorageFile storageFile = getRedacteurListStorageFile(name);
        if (storageFile.exists()) {
            storageFile.delete();
        }
    }

    public Map<String, List<Redacteur>> checkRedacteurListMap(MultiMessageHandler messageHandler, Fichotheque fichotheque) {
        Map<String, List<Redacteur>> result = new HashMap<String, List<Redacteur>>();
        File redacteurlistsDirectory = new File(storageDirectories.getDataDir(), rootPath);
        if (redacteurlistsDirectory.exists()) {
            if (!redacteurlistsDirectory.isDirectory()) {
                throw new BdfStorageException(redacteurlistsDirectory.getPath() + " is not a directory");
            }
        } else {
            return result;
        }
        RedacteurListDOMReader redacteurListDOMReader = new RedacteurListDOMReader(messageHandler, fichotheque);
        for (File f : redacteurlistsDirectory.listFiles()) {
            if (f.isDirectory()) {
                continue;
            }
            String fileName = f.getName();
            if (fileName.endsWith(".xml")) {
                String name = fileName.substring(0, fileName.length() - 4);
                StorageFile storageFile = StorageFile.build(storageDirectories, rootPath + File.separator + fileName);
                messageHandler.setCurrentSource(storageFile.toURI());
                Document doc = storageFile.readDocument();
                List<Redacteur> redacteurList = redacteurListDOMReader.readRedacteurList(doc.getDocumentElement());
                result.put(name, redacteurList);
            }
        }
        return result;
    }

    private StorageFile getRedacteurListStorageFile(String name) {
        return StorageFile.build(storageDirectories, rootPath + File.separator + name + ".xml");
    }

}
