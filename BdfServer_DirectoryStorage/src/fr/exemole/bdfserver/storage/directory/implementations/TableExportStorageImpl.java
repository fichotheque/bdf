/* BdfServer_DirectoryStorage - Copyright (c) 2007-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.implementations;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.api.storage.StorageContent;
import fr.exemole.bdfserver.api.storage.TableExportStorage;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import fr.exemole.bdfserver.conf.ConfConstants;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageFile;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import fr.exemole.bdfserver.tools.storage.TableExportStorageUnitBuilder;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.tools.exportation.table.TableExportDefBuilder;
import net.fichotheque.tools.exportation.table.dom.TableExportDefDOMReader;
import net.fichotheque.xml.defs.TableExportDefXMLPart;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Document;


/**
 *
 * @author Vincent Calame
 */
public class TableExportStorageImpl implements TableExportStorage {

    private final static String SUBDIR = "tableexportdata";
    private final File varTableExportDirectory;
    private final File backupTableExportDirectory;

    public TableExportStorageImpl(BdfServerDirs dirs) {
        this.varTableExportDirectory = dirs.getSubPath(ConfConstants.VAR_DATA, SUBDIR);
        this.backupTableExportDirectory = dirs.getSubPath(ConfConstants.VAR_BACKUP, SUBDIR);
    }

    @Override
    public Unit[] checkStorage() {
        SortedMap<String, TableExportStorage.Unit> map = new TreeMap<String, TableExportStorage.Unit>();
        scanTableExportDirectories(map);
        return map.values().toArray(new Unit[map.size()]);
    }

    @Override
    public Unit getTableExportStorageUnit(String tableExportName) {
        if (!testName(tableExportName)) {
            return null;
        }
        File varDir = new File(varTableExportDirectory, tableExportName);
        if (varDir.exists()) {
            if (!varDir.isDirectory()) {
                return null;
            }
            return buildUnit(tableExportName, varDir);
        }
        return null;
    }

    @Override
    public boolean saveTableExportDef(TableExportDef tableExportDef, EditOrigin editOrigin) {
        String tableExportName = tableExportDef.getName();
        if (!testName(tableExportName)) {
            return false;
        }
        StorageFile storageFile = getStorageFile(tableExportName, "_def.xml");
        try (BufferedWriter writer = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(writer);
            xmlWriter.appendXMLDeclaration();
            TableExportDefXMLPart.init(xmlWriter)
                    .addTableExportDef(tableExportDef);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
        return true;
    }

    @Override
    public boolean removeTableExport(String tableExportName, EditOrigin editOrigin) {
        if (!testName(tableExportName)) {
            return false;
        }
        File rootDirectory = new File(varTableExportDirectory, tableExportName);
        if (!rootDirectory.exists()) {
            return false;
        }
        File rootBackupDirectory = null;
        if (backupTableExportDirectory != null) {
            rootBackupDirectory = new File(backupTableExportDirectory, tableExportName);
        }
        StorageFile.archiveAndDeleteDirectory(rootDirectory, rootBackupDirectory, editOrigin);
        return true;
    }

    @Override
    public boolean copyTableExport(String tableExportName, String newTableExportName, EditOrigin editOrigin) {
        if (!testName(tableExportName)) {
            return false;
        }
        if (!testName(newTableExportName)) {
            throw new IllegalArgumentException(newTableExportName + " is not a technical name");
        }
        File sourceDir = new File(varTableExportDirectory, tableExportName);
        File destinationDir = new File(varTableExportDirectory, newTableExportName);
        if (destinationDir.exists()) {
            return false;
        }
        File destinationBackupDirectory = null;
        if (backupTableExportDirectory != null) {
            destinationBackupDirectory = new File(backupTableExportDirectory, newTableExportName);
        }
        StorageFile.archiveAndCopyDirectory(sourceDir, destinationDir, destinationBackupDirectory, editOrigin);
        return true;
    }

    @Override
    public StorageContent getStorageContent(String tableExportName, String contentPath) {
        if (!testName(tableExportName)) {
            return null;
        }
        contentPath = checkPath(contentPath);
        if (contentPath == null) {
            return null;
        }
        StorageFile storageFile = getStorageFile(tableExportName, contentPath);
        if (!storageFile.exists()) {
            return null;
        }
        return StorageUtils.toStorageContent(contentPath, storageFile.getFile());
    }

    @Override
    public void saveStorageContent(String tableExportName, String contentPath, InputStream inputStream, EditOrigin editOrigin) throws IOException {
        if (!testName(tableExportName)) {
            throw new IllegalArgumentException(tableExportName + " is not a technical name");
        }
        contentPath = checkPath(contentPath);
        if (contentPath == null) {
            throw new IllegalArgumentException(contentPath + " is not a relative path");
        }
        StorageFile storageFile = getStorageFile(tableExportName, contentPath);
        try (OutputStream os = storageFile.archiveAndGetOutputStream(editOrigin)) {
            IOUtils.copy(inputStream, os);
        }
    }

    @Override
    public boolean removeStorageContent(String tableExportName, String contentPath, EditOrigin editOrigin) {
        if (!testName(tableExportName)) {
            return false;
        }
        contentPath = checkPath(contentPath);
        if (contentPath == null) {
            return false;
        }
        StorageFile storageFile = getStorageFile(tableExportName, contentPath);
        if (storageFile.exists()) {
            storageFile.archiveAndDelete(editOrigin);
            return true;
        } else {
            return false;
        }
    }


    private void scanTableExportDirectories(Map<String, TableExportStorage.Unit> map) {
        if (!varTableExportDirectory.exists()) {
            return;
        }
        if (!varTableExportDirectory.isDirectory()) {
            return;
        }
        for (File file : varTableExportDirectory.listFiles()) {
            if (file.isDirectory()) {
                String name = file.getName();
                if (testName(name)) {
                    if (map.containsKey(name)) {
                        continue;
                    }
                    TableExportStorage.Unit unit = buildUnit(name, file);
                    map.put(name, unit);
                }
            }
        }
    }

    private TableExportStorage.Unit buildUnit(String name, File directory) {
        File backupDirectory = null;
        if (backupTableExportDirectory != null) {
            backupDirectory = new File(backupTableExportDirectory, name);
        }
        TableExportStorageUnitBuilder builder = new TableExportStorageUnitBuilder();
        TableExportDefBuilder tableExportDefBuilder = new TableExportDefBuilder(name);
        File[] files = directory.listFiles();
        int count = files.length;
        for (int i = 0; i < count; i++) {
            File f = files[i];
            if (!f.isDirectory()) {
                String fileName = f.getName();
                if ((fileName.endsWith(".txt")) && (fileName.length() > 4)) {
                    if (testName(fileName.substring(0, fileName.length() - 4))) {
                        builder.addStorageContent(StorageUtils.toStorageContent(fileName, f));
                    }
                } else if (fileName.equals("_def.xml")) {
                    StorageFile storageFile = StorageFile.build(directory, backupDirectory, fileName);
                    Document doc = storageFile.readDocument();
                    TableExportDefDOMReader.init(tableExportDefBuilder, LogUtils.NULL_MULTIMESSAGEHANDLER)
                            .read(doc.getDocumentElement());
                }
            }
        }
        builder.setTableExportDef(tableExportDefBuilder.toTableExportDef());
        return builder.toTableExportStorageUnit();
    }

    private StorageFile getStorageFile(String tableExportName, String fileName) {
        String path = tableExportName + File.separator + fileName;
        return StorageFile.build(varTableExportDirectory, backupTableExportDirectory, path);
    }

    private static boolean testName(String s) {
        return StringUtils.isTechnicalName(s, true);
    }

    private static String checkPath(String path) {
        path = StringUtils.normalizeRelativePath(path);
        if (path == null) {
            return null;
        }
        switch (path) {
            case "_def.xml":
                return null;
            default:
                return path;
        }
    }


}
