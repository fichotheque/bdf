/* BdfServer_DirectoryStorage - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.fichotheque.EditOrigin;
import net.fichotheque.history.HistoryUnit;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.io.FileUtils;
import net.mapeadores.util.io.IOUtils;
import org.w3c.dom.Document;


/**
 * Désigne un fichier destiné à stocker des informations de la base.
 *
 * @author Vincent Calame
 */
public class StorageFile {

    public final static String ENCODING = "UTF-8";
    private final File file;
    private final File backupFile;

    public StorageFile(File file, File backupFile) {
        if (file == null) {
            throw new IllegalArgumentException("file is null");
        }
        this.file = file;
        this.backupFile = backupFile;
    }

    public boolean exists() {
        return file.exists();
    }

    public boolean backupExist() {
        return (backupFile != null) && (backupFile.exists());
    }

    public File getFile() {
        return file;
    }

    public String toURI() {
        return file.getPath();
    }

    public void delete() {
        backup();
        try {
            if (file.exists()) {
                FileUtils.forceDelete(file);
            }
        } catch (IOException ioe) {
            throw new BdfStorageException(file, ioe);
        }
    }

    public HistoryUnit getHistoryUnit() {
        return StorageUtils.getHistoryUnit(file, backupFile);
    }

    public List<String> readLines() {
        if (!file.exists()) {
            return null;
        }
        List<String> result;
        try (InputStream is = new FileInputStream(file)) {
            result = IOUtils.readLines(is, "UTF-8");
        } catch (IOException ioe) {
            if (!backupExist()) {
                throw new BdfStorageException(file, ioe);
            }
            try (InputStream is2 = new FileInputStream(backupFile)) {
                result = IOUtils.readLines(is2, "UTF-8");
            } catch (IOException ioe2) {
                throw new BdfStorageException(backupFile, ioe2);
            }
            restore();
        }
        return result;
    }

    public Document readDocument() {
        if (!file.exists()) {
            return null;
        }
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docbuilder = null;
        Document document = null;
        try {
            docbuilder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException pce) {
            throw new BdfStorageException(pce);
        }
        try (InputStream is = new FileInputStream(file)) {
            document = docbuilder.parse(is);
        } catch (Exception e) {
            if (!backupExist()) {
                throw new BdfStorageException(file, e);
            }
            try (InputStream is2 = new FileInputStream(backupFile)) {
                document = docbuilder.parse(is2);
            } catch (Exception e2) {
                throw new BdfStorageException(backupFile, e);
            }
            restore();
        }
        return document;
    }

    public Document readDocument(String versionName) {
        return StorageUtils.readDocument(file, backupFile, versionName);
    }

    public boolean restore() {
        if (backupExist()) {
            try {
                FileUtils.copyFile(backupFile, file);
            } catch (IOException ioe) {
                throw new BdfStorageException(file, ioe);
            }
            return true;
        }
        return false;
    }

    public BufferedWriter getWriter() throws IOException {
        return getWriter(true);
    }

    public BufferedWriter getWriter(boolean withBackup) throws IOException {
        testPath();
        if (withBackup) {
            backup();
        }
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        BufferedWriter bufWriter = new BufferedWriter(writer);
        return bufWriter;
    }

    public BufferedWriter archiveAndGetWriter(@Nullable EditOrigin editOrigin) throws IOException {
        testPath();
        if (editOrigin != null) {
            StorageUtils.archive(file, backupFile, editOrigin);
        } else {
            backup();
        }
        OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        BufferedWriter bufWriter = new BufferedWriter(writer);
        return bufWriter;
    }

    public BufferedOutputStream getOutputStream() throws IOException {
        return getOutputStream(true);
    }

    public BufferedOutputStream getOutputStream(boolean withBackup) throws IOException {
        testPath();
        if (withBackup) {
            backup();
        }
        OutputStream outputStream = new FileOutputStream(file);
        BufferedOutputStream bufOutputStream = new BufferedOutputStream(outputStream);
        return bufOutputStream;
    }

    public BufferedOutputStream archiveAndGetOutputStream(EditOrigin editOrigin) throws IOException {
        testPath();
        if (editOrigin != null) {
            StorageUtils.archive(file, backupFile, editOrigin);
        }
        OutputStream outputStream = new FileOutputStream(file);
        BufferedOutputStream bufOutputStream = new BufferedOutputStream(outputStream);
        return bufOutputStream;
    }

    public void archiveAndDelete(EditOrigin editOrigin) {
        try {
            if (editOrigin != null) {
                StorageUtils.archiveBeforeDelete(file, backupFile, editOrigin);
            }
            if (file.exists()) {
                FileUtils.forceDelete(file);
            }
        } catch (IOException ioe) {
            throw new BdfStorageException(file, ioe);
        }
    }


    private void testPath() {
        file.getParentFile().mkdirs();
        if (file.isDirectory()) {
            file.delete();
        }
    }

    private void backup() {
        if ((file.exists()) && (backupFile != null) && (!file.isDirectory())) {
            try {
                FileUtils.copyFile(file, backupFile);
            } catch (IOException ioe) {
                throw new BdfStorageException(backupFile, ioe);
            }
        }
    }

    public static StorageFile build(StorageDirectories storageDirectories, String path) {
        File file = new File(storageDirectories.getDataDir(), path);
        File backupFile = null;
        if (storageDirectories.isWithBackup()) {
            backupFile = new File(storageDirectories.getBackupDir(), path);
        }
        return new StorageFile(file, backupFile);
    }

    public static StorageFile build(File rootDir, File rootBackupDir, String path) {
        File file = new File(rootDir, path);
        File backupFile = null;
        if (rootBackupDir != null) {
            backupFile = new File(rootBackupDir, path);
        }
        return new StorageFile(file, backupFile);
    }

    public static void archiveAndDeleteDirectory(File rootDirectory, File rootBackupDirectory, EditOrigin editOrigin) {
        for (File f : rootDirectory.listFiles()) {
            if (f.isDirectory()) {
                if (rootBackupDirectory == null) {
                    archiveAndDeleteDirectory(f, null, editOrigin);
                } else {
                    archiveAndDeleteDirectory(f, new File(rootBackupDirectory, f.getName()), editOrigin);
                }
            } else {
                File backupFile = null;
                if (rootBackupDirectory != null) {
                    backupFile = new File(rootBackupDirectory, f.getName());
                }
                StorageFile storageFile = new StorageFile(f, backupFile);
                storageFile.archiveAndDelete(editOrigin);
            }
        }
        try {
            FileUtils.forceDelete(rootDirectory);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void archiveAndCopyDirectory(File sourceDirectory, File destinationDirectory, File destinationBackupDirectory, EditOrigin editOrigin) {
        for (File f : sourceDirectory.listFiles()) {
            if (f.isDirectory()) {
                File subdestination = new File(destinationDirectory, f.getName());
                if (destinationBackupDirectory == null) {
                    archiveAndCopyDirectory(f, subdestination, null, editOrigin);
                } else {
                    archiveAndCopyDirectory(f, subdestination, new File(destinationBackupDirectory, f.getName()), editOrigin);
                }
            } else {
                File backupFile = null;
                if (destinationBackupDirectory != null) {
                    backupFile = new File(destinationBackupDirectory, f.getName());
                }
                StorageFile storageFile = new StorageFile(new File(destinationDirectory, f.getName()), backupFile);
                try (InputStream is = new FileInputStream(f); OutputStream os = storageFile.archiveAndGetOutputStream(editOrigin)) {
                    IOUtils.copy(is, os);
                } catch (IOException ioe) {
                    throw new BdfStorageException(ioe);
                }
            }
        }
    }

}
