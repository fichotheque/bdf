/* BdfServer_DirectoryStorage - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;


/**
 *
 * @author Vincent Calame
 */
public interface SubsetInfoList {

    public int getStandardSubsetCount();

    public SubsetInfo getStandardSubsetInfo(int i);

    public int getSatelliteSubsetCount();

    public SubsetInfo getSatelliteSubsetInfo(int i);

}
