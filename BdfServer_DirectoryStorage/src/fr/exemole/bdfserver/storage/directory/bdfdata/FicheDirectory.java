/* BdfServer_DirectoryStorage - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import java.io.File;
import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.io.FileUtils;


/**
 *
 * @author Vincent Calame
 */
public class FicheDirectory {

    private final int id;
    private final File dir;

    private FicheDirectory(File dir, int id) {
        this.dir = dir;
        this.id = id;
    }

    public boolean delete() {
        try {
            if (dir.exists()) {
                FileUtils.forceDelete(dir);
                return true;
            } else {
                return false;
            }
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static FicheDirectory getFicheDirectory(StorageDirectories storageDirectories, SubsetKey subsetKey, int id) {
        File docDir = new File(storageDirectories.getDataDir(), getPath(subsetKey, id));
        FicheDirectory ficheDirectory = new FicheDirectory(docDir, id);
        return ficheDirectory;
    }

    public static StorageFile getFicheStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey, int id) {
        String path = getPath(subsetKey, id) + File.separatorChar + "fiche.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getChronoStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey, int id) {
        String path = getPath(subsetKey, id) + File.separatorChar + "chrono.txt";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getAttributesStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey, int id) {
        String path = getPath(subsetKey, id) + File.separatorChar + "attributes.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static String getPath(SubsetKey subsetKey, int id) {
        if (!subsetKey.isCorpusSubset()) {
            throw new IllegalArgumentException("!subsetKey.isCorpusSubset()");
        }
        if (id < 0) {
            throw new IllegalArgumentException("id < 0");
        }
        StringBuilder buf = new StringBuilder();
        buf.append(BdfDirectoryConstants.FICHOTHEQUE);
        buf.append(File.separatorChar);
        buf.append(SubsetKey.CATEGORY_CORPUS_STRING);
        buf.append(File.separatorChar);
        buf.append(subsetKey.getSubsetName());
        buf.append(File.separatorChar);
        buf.append("f.");
        buf.append(BdfServerUtils.getMillier(id));
        buf.append(File.separatorChar);
        buf.append(id);
        return buf.toString();
    }

}
