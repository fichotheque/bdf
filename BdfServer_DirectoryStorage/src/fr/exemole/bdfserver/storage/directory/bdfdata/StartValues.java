/* BdfServer_DirectoryStorage - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.text.AccoladeArgument;
import net.mapeadores.util.text.AccoladePattern;
import net.mapeadores.util.text.ValueResolver;


/**
 *
 * @author Vincent Calame
 */
public class StartValues {

    public final static String AUTHORITY = "authority";
    public final static String BASENAME = "basename";
    public final static String WORKINGLANGS = "workinglangs";
    public final static String SPHERENAME = "spherename";
    public final static String FIRSTUSER = "firstuser";
    public final static String FIRSTPASSWORD = "firstpassword";
    private final Map<String, String> valueMap = new HashMap<String, String>();
    private final InternalResolver resolver = new InternalResolver();
    private Langs workingLangs = LangsUtils.toCleanLangs("fr");

    {
        valueMap.put(AUTHORITY, "localhost");
        valueMap.put(BASENAME, "base");
        valueMap.put(SPHERENAME, "admin");
        valueMap.put(FIRSTUSER, "ADMIN");
        valueMap.put(FIRSTPASSWORD, "admin");
    }

    public StartValues() {

    }

    public String authority() {
        return valueMap.get(AUTHORITY);
    }

    public StartValues authority(String authority) {
        if (authority == null) {
            authority = "localhost";
        }
        put(AUTHORITY, authority);
        return this;
    }

    public String basename() {
        return valueMap.get(BASENAME);
    }

    public StartValues basename(String basename) {
        if (basename == null) {
            basename = "base";
        }
        put(BASENAME, basename);
        return this;
    }

    public Langs workingLangs() {
        return workingLangs;
    }

    public StartValues workingLangs(Langs langs) {
        if (!langs.isEmpty()) {
            this.workingLangs = langs;
        }
        return this;
    }

    public String spherename() {
        return valueMap.get(SPHERENAME);
    }

    public StartValues spherename(String spherename) {
        if (spherename == null) {
            spherename = "admin";
        }
        put(SPHERENAME, spherename);
        return this;
    }

    public String firstuser() {
        return valueMap.get(FIRSTUSER);
    }

    public StartValues firstuser(String firstuser) {
        if (firstuser == null) {
            firstuser = "firstuser";
        }
        put(FIRSTUSER, firstuser);
        return this;
    }

    public String firstpassword() {
        return valueMap.get(FIRSTPASSWORD);
    }

    public StartValues firstpassword(String firstpassword) {
        if (firstpassword == null) {
            firstpassword = "admin";
        }
        put(FIRSTPASSWORD, firstpassword);
        return this;
    }

    private void put(String key, String value) {
        valueMap.put(key, value);
    }

    public String replaceInText(String text) throws ParseException {
        AccoladePattern pattern = new AccoladePattern(text);
        return pattern.format(resolver);
    }

    public static StartValues init() {
        return new StartValues();
    }


    private class InternalResolver implements ValueResolver {

        private InternalResolver() {

        }

        @Override
        public String getValue(AccoladeArgument accoladeArgument) {
            String argumentName = accoladeArgument.getName();
            if (argumentName.equals(WORKINGLANGS)) {
                StringBuilder buf = new StringBuilder();
                for (Lang lang : workingLangs) {
                    buf.append("<working-lang lang=\"");
                    buf.append(lang.toString());
                    buf.append("\"/>\n");
                }
                return buf.toString();
            } else {
                String value = valueMap.get(argumentName);
                if (value != null) {
                    return value;
                } else {
                    return AccoladePattern.toString(accoladeArgument);
                }
            }
        }

    }

}
