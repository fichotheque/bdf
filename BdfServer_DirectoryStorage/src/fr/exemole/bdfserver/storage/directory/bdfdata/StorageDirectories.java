/* BdfServer_DirectoryStorage - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import java.io.File;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public interface StorageDirectories {

    public File getDataDir();

    @Nullable
    public File getBackupDir();

    public default boolean isWithBackup() {
        return (getBackupDir() != null);
    }

}
