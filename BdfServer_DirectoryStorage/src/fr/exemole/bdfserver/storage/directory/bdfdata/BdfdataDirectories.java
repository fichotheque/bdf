/* BdfServer_DirectoryStorage - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import fr.exemole.bdfserver.conf.ConfConstants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.exceptions.InternalResourceException;
import net.mapeadores.util.io.FileUtils;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.security.PasswordChecker;


/**
 *
 * @author Vincent Calame
 */
public class BdfdataDirectories implements StorageDirectories {

    private final static FileInfo[] FILES = new FileInfo[2];
    private final static FileInfo[] FILES_SPHERE = new FileInfo[3];
    private final File bdfdataDir;
    private final File backupDir;
    private final File cacheDir;

    static {
        FILES[0] = new FileInfo("fichotheque-metadata.xml", BdfDirectoryConstants.FICHOTHEQUE, "", "metadata.xml");
        FILES[1] = new FileInfo("lang-configuration.xml", BdfDirectoryConstants.CONF, "", "lang-configuration.xml");
        FILES_SPHERE[0] = new FileInfo("admin.xml", BdfDirectoryConstants.CONF, "redacteurlists", "admin.xml");
        FILES_SPHERE[1] = new FileInfo("sphere-metadata.xml", BdfDirectoryConstants.FICHOTHEQUE, "sphere", "{spherename}.xml");
        FILES_SPHERE[2] = new FileInfo("sphere-list.xml", BdfDirectoryConstants.FICHOTHEQUE, "sphere/{spherename}", "list.xml");
    }

    public BdfdataDirectories(File bdfdataDir, File cacheDir, File backupDir) {
        BdfStorageUtils.testDirectory(bdfdataDir);
        this.bdfdataDir = bdfdataDir;
        if (cacheDir != null) {
            BdfStorageUtils.testDirectory(cacheDir);
            this.cacheDir = cacheDir;
        } else {
            this.cacheDir = null;
        }
        if (backupDir != null) {
            BdfStorageUtils.testDirectory(backupDir);
            this.backupDir = backupDir;
        } else {
            this.backupDir = null;
        }
        testRelativePath(BdfDirectoryConstants.CONF);
        testRelativePath(BdfDirectoryConstants.USERS);
        testRelativePath(BdfDirectoryConstants.FICHOTHEQUE);
    }

    /**
     * Les répertoires par catégorie sont sous la forme fichotheque/{categorie}.
     * Cette méthode crèe le répertoire pour la catégorie si elle n'existe pas.
     */
    public void testCategoryDirectory(short category) {
        String categoryString = SubsetKey.categoryToString(category);
        File directory = new File(getDataDir(), BdfDirectoryConstants.FICHOTHEQUE + File.separator + categoryString);
        directory.mkdirs();
    }

    private void testRelativePath(String path) {
        File testDirectory = new File(bdfdataDir, path);
        BdfStorageUtils.testDirectory(testDirectory);
    }

    @Override
    public File getDataDir() {
        return bdfdataDir;
    }

    @Override
    public File getBackupDir() {
        return backupDir;
    }

    public File getCacheDir() {
        return cacheDir;
    }

    public static BdfdataDirectories build(BdfServerDirs dirs) {
        String subdirName = "bdfdata";
        File bdfdataDir = dirs.getSubPath(ConfConstants.VAR_DATA, subdirName);
        File backupDir = dirs.getSubPath(ConfConstants.VAR_BACKUP, subdirName);
        File cacheDir = dirs.getDir(ConfConstants.VAR_CACHE);
        if (!bdfdataDir.exists()) {
            StartValues startValues = StartValues.init().basename(dirs.getName());
            writeStartFiles(bdfdataDir, startValues, true);
        }
        return new BdfdataDirectories(bdfdataDir, cacheDir, backupDir);
    }

    public static void writeStartFiles(BdfServerDirs dirs, StartValues startValues, boolean withFirstSphere) {
        File bdfdataDir = dirs.getSubPath(ConfConstants.VAR_DATA, "bdfdata");
        bdfdataDir.mkdirs();
        writeStartFiles(bdfdataDir, startValues, withFirstSphere);
    }

    private static void writeStartFiles(File bdfdataDir, StartValues startValues, boolean withFirstSphere) {
        for (FileInfo fileInfo : FILES) {
            writeStartFile(bdfdataDir, fileInfo, startValues);
        }
        if (withFirstSphere) {
            for (FileInfo fileInfo : FILES_SPHERE) {
                writeStartFile(bdfdataDir, fileInfo, startValues);
            }
            writePassword(bdfdataDir, startValues);
        }
    }

    private static void writeStartFile(File bdfdataDir, FileInfo fileInfo, StartValues startValues) {
        String fileContent;
        try (InputStream is = BdfdataDirectories.class.getResourceAsStream("defaultfiles/" + fileInfo.getOriginName())) {
            fileContent = IOUtils.toString(is, "UTF-8");
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
        File destFile;
        try {
            fileContent = startValues.replaceInText(fileContent);
            File destDir = new File(bdfdataDir, fileInfo.getRoot());
            String path = fileInfo.getPath();
            if (path.length() > 0) {
                path = startValues.replaceInText(path);
                destDir = new File(destDir, path);
            }
            destDir.mkdirs();
            destFile = new File(destDir, startValues.replaceInText(fileInfo.getDestinationName()));
        } catch (ParseException pe) {
            throw new InternalResourceException(pe);
        }
        try {
            FileUtils.writeStringToFile(destFile, fileContent, "UTF-8");
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    private static void writePassword(File bdfdataDir, StartValues startValues) {
        File confDir = new File(bdfdataDir, BdfDirectoryConstants.CONF);
        File passwdFile = new File(confDir, "passwd");
        try (BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(passwdFile), "UTF-8"))) {
            buf.append(startValues.spherename());
            buf.append("/1:");
            buf.append(PasswordChecker.getHash(PasswordChecker.PBKDF2, startValues.firstpassword()));
            buf.append('\n');
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }


    private static class FileInfo {

        private final String name;
        private final String root;
        private final String path;
        private final String destinationName;

        private FileInfo(String name, String root, String path, String destinationName) {
            this.name = name;
            this.root = root;
            this.path = path;
            this.destinationName = destinationName;
        }

        private String getOriginName() {
            return name;
        }

        private String getRoot() {
            return root;
        }

        private String getPath() {
            return path;
        }

        private String getDestinationName() {
            return destinationName;
        }

    }

}
