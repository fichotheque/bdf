/* BdfServer_DirectoryStorage - Copyright (c) 2008-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;


/**
 *
 * @author Vincent Calame
 */
public interface BdfDirectoryConstants {

    public final static String CONF = "conf";
    public final static String USERS = "users";
    public final static String FICHOTHEQUE = "fichotheque";
}
