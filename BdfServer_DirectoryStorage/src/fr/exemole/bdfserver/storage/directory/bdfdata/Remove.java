/* BdfServer_DirectoryStorage - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import net.fichotheque.EditOrigin;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.album.Album;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.croisement.CroisementKey;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.thesaurus.Thesaurus;


/**
 *
 * @author Vincent Calame
 */
public final class Remove {

    private Remove() {
    }

    public static void removeAddenda(StorageDirectories storageDirectories, SubsetKey addendaKey, EditOrigin editOrigin) {
        StorageFile metadataStorageFile = BdfStorageUtils.getAddendaMetadataStorageFile(storageDirectories, addendaKey);
        metadataStorageFile.archiveAndDelete(editOrigin);
        StorageFile directoryStorageFile = BdfStorageUtils.getAddendaDirectory(storageDirectories, addendaKey);
        directoryStorageFile.delete();
    }

    public static void removeAlbum(StorageDirectories storageDirectories, SubsetKey albumKey, EditOrigin editOrigin) {
        StorageFile metadataStorageFile = BdfStorageUtils.getAlbumMetadataStorageFile(storageDirectories, albumKey);
        metadataStorageFile.archiveAndDelete(editOrigin);
        StorageFile directoryStorageFile = BdfStorageUtils.getAlbumDirectory(storageDirectories, albumKey);
        directoryStorageFile.delete();
    }

    public static void removeCorpus(StorageDirectories storageDirectories, SubsetKey corpusKey, Subset masterSubset, EditOrigin editOrigin) {
        SubsetKey masterSubsetKey = null;
        if (masterSubset != null) {
            masterSubsetKey = masterSubset.getSubsetKey();
        }
        StorageFile metadataStorageFile = BdfStorageUtils.getCorpusMetadataStorageFile(storageDirectories, corpusKey, masterSubsetKey);
        metadataStorageFile.archiveAndDelete(editOrigin);
        StorageFile directoryStorageFile = BdfStorageUtils.getCorpusDirectory(storageDirectories, corpusKey);
        directoryStorageFile.delete();
    }

    public static void removeSphere(StorageDirectories storageDirectories, SubsetKey sphereKey, EditOrigin editOrigin) {
        StorageFile listStorageFile = BdfStorageUtils.getSphereListStorageFile(storageDirectories, sphereKey);
        listStorageFile.archiveAndDelete(editOrigin);
        StorageFile metadataStorageFile = BdfStorageUtils.getSphereMetadataStorageFile(storageDirectories, sphereKey);
        metadataStorageFile.archiveAndDelete(editOrigin);
        StorageFile directoryStorageFile = BdfStorageUtils.getSphereDirectory(storageDirectories, sphereKey);
        directoryStorageFile.delete();
    }

    public static void removeThesaurus(StorageDirectories storageDirectories, SubsetKey thesaurusKey, EditOrigin editOrigin) {
        StorageFile treeStorageFile = BdfStorageUtils.getThesaurusTreeStorageFile(storageDirectories, thesaurusKey);
        treeStorageFile.archiveAndDelete(editOrigin);
        StorageFile metadataStorageFile = BdfStorageUtils.getThesaurusMetadataStorageFile(storageDirectories, thesaurusKey);
        metadataStorageFile.archiveAndDelete(editOrigin);
        StorageFile directoryStorageFile = BdfStorageUtils.getThesaurusDirectory(storageDirectories, thesaurusKey);
        directoryStorageFile.delete();
    }

    public static void removeDocument(StorageDirectories storageDirectories, Addenda addenda, int id, EditOrigin editOrigin) {
        SubsetKey addendaKey = addenda.getSubsetKey();
        StorageFile storageFile = DocumentDirectory.getDocumentStorageFile(storageDirectories, addendaKey, id);
        storageFile.archiveAndDelete(editOrigin);
        DocumentDirectory documentDirectory = DocumentDirectory.getDocumentDirectory(storageDirectories, addendaKey, id);
        documentDirectory.delete();
    }

    public static void removeIllustration(StorageDirectories storageDirectories, Album album, int id, EditOrigin editOrigin) {
        SubsetKey albumKey = album.getSubsetKey();
        StorageFile storageFile = IllustrationDirectory.getIllustrationStorageFile(storageDirectories, albumKey, id);
        storageFile.archiveAndDelete(editOrigin);
        IllustrationDirectory illustrationDirectory = IllustrationDirectory.getIllustrationDirectory(storageDirectories, albumKey, id);
        illustrationDirectory.delete();
    }

    public static void removeFiche(StorageDirectories storageDirectories, Corpus corpus, int id, EditOrigin editOrigin) {
        SubsetKey subsetKey = corpus.getSubsetKey();
        StorageFile ficheStorageFile = FicheDirectory.getFicheStorageFile(storageDirectories, subsetKey, id);
        ficheStorageFile.archiveAndDelete(editOrigin);
        StorageFile attributesStorageFile = FicheDirectory.getAttributesStorageFile(storageDirectories, subsetKey, id);
        attributesStorageFile.archiveAndDelete(editOrigin);
        StorageFile chronoStorageFile = FicheDirectory.getChronoStorageFile(storageDirectories, subsetKey, id);
        chronoStorageFile.delete();
        FicheDirectory ficheDirectory = FicheDirectory.getFicheDirectory(storageDirectories, subsetKey, id);
        ficheDirectory.delete();
    }

    public static void removeMotcle(StorageDirectories storageDirectories, Thesaurus thesaurus, int id, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getMotcleStorageFile(storageDirectories, thesaurus.getSubsetKey(), id);
        storageFile.archiveAndDelete(editOrigin);
    }

    public static void removeRedacteur(StorageDirectories storageDirectories, Sphere sphere, int id, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getRedacteurStorageFile(storageDirectories, sphere.getSubsetKey(), id);
        storageFile.archiveAndDelete(editOrigin);
    }

    public static void removeCroisement(StorageDirectories storageDirectories, CroisementKey croisementKey, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getCroisementStorageFile(storageDirectories, croisementKey);
        storageFile.archiveAndDelete(editOrigin);
    }

}
