/* BdfServer_DirectoryStorage - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import fr.exemole.bdfserver.conf.ConfConstants;
import fr.exemole.bdfserver.conf.ConfDirs;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import net.fichotheque.SubsetKey;
import net.fichotheque.croisement.CroisementKey;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 *
 * @author Vincent Calame
 */
public final class BdfStorageUtils {

    private BdfStorageUtils() {
    }

    public static StorageDirectories toStorageDirectories(ConfDirs confDirs, boolean createIfMissing) {
        return toStorageDirectories(confDirs.getDir(ConfConstants.VAR_DATA), confDirs.getDir(ConfConstants.VAR_BACKUP), createIfMissing);
    }

    public static StorageDirectories toStorageDirectories(BdfServerDirs bdfServerDirs, boolean createIfMissing) {
        String subdirName = "bdfdata";
        File bdfdataDir = bdfServerDirs.getSubPath(ConfConstants.VAR_DATA, subdirName);
        File backupDir = bdfServerDirs.getSubPath(ConfConstants.VAR_BACKUP, subdirName);
        return toStorageDirectories(bdfdataDir, backupDir, createIfMissing);
    }

    public static StorageDirectories toStorageDirectories(File dataDir, @Nullable File backupDir, boolean createIfMissing) {
        if (createIfMissing) {
            BdfStorageUtils.testDirectory(dataDir);
            if (backupDir != null) {
                BdfStorageUtils.testDirectory(backupDir);
            }
        }
        return new InternalStorageDirectories(dataDir, backupDir);
    }


    public static StorageFile getChronoStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + "chrono" + File.separator + subsetKey + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getFichothequeMetadataFile(StorageDirectories storageDirectories) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + "metadata.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static void testDirectory(File directory) {
        if (!directory.exists()) {
            directory.mkdirs();
        } else if (!directory.isDirectory()) {
            throw new BdfStorageException(directory.getPath() + " is not a directory");
        }
    }

    private static String getPrefix(SubsetKey masterSubsetKey) {
        if (masterSubsetKey == null) {
            return "";
        }
        String preprefix = "";
        switch (masterSubsetKey.getCategory()) {
            case SubsetKey.CATEGORY_CORPUS:
                preprefix = "_mc_";
                break;
            case SubsetKey.CATEGORY_THESAURUS:
                preprefix = "_mt_";
                break;
            default:
                throw new UnsupportedOperationException("Unkwnown Master Subset category : " + masterSubsetKey.getCategoryString());
        }
        return preprefix + masterSubsetKey.getSubsetName() + "_";
    }

    public static StorageFile getAddendaMetadataStorageFile(StorageDirectories storageDirectories, SubsetKey addendaKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_ADDENDA_STRING + File.separator + addendaKey.getSubsetName() + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getAddendaDirectory(StorageDirectories storageDirectories, SubsetKey addendaKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_ADDENDA_STRING + File.separator + addendaKey.getSubsetName();
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getAlbumMetadataStorageFile(StorageDirectories storageDirectories, SubsetKey albumKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_ALBUM_STRING + File.separator + albumKey.getSubsetName() + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getAlbumDirectory(StorageDirectories storageDirectories, SubsetKey albumKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_ALBUM_STRING + File.separator + albumKey.getSubsetName();
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getCorpusMetadataStorageFile(StorageDirectories storageDirectories, SubsetKey corpusKey, SubsetKey masterSubsetKey) {
        String prefix = getPrefix(masterSubsetKey);
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_CORPUS_STRING + File.separator + prefix + corpusKey.getSubsetName() + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getCorpusDirectory(StorageDirectories storageDirectories, SubsetKey corpusKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_CORPUS_STRING + File.separator + corpusKey.getSubsetName();
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getSphereMetadataStorageFile(StorageDirectories storageDirectories, SubsetKey sphereKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_SPHERE_STRING + File.separator + sphereKey.getSubsetName() + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getSphereListStorageFile(StorageDirectories storageDirectories, SubsetKey sphereKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_SPHERE_STRING + File.separator + sphereKey.getSubsetName() + File.separator + "list.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getSphereDirectory(StorageDirectories storageDirectories, SubsetKey sphereKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_SPHERE_STRING + File.separator + sphereKey.getSubsetName();
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getThesaurusMetadataStorageFile(StorageDirectories storageDirectories, SubsetKey thesaurusKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_THESAURUS_STRING + File.separator + thesaurusKey.getSubsetName() + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getThesaurusTreeStorageFile(StorageDirectories storageDirectories, SubsetKey thesaurusKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_THESAURUS_STRING + File.separator + thesaurusKey.getSubsetName() + File.separator + "tree.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getThesaurusDirectory(StorageDirectories storageDirectories, SubsetKey thesaurusKey) {
        String path = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.CATEGORY_THESAURUS_STRING + File.separator + thesaurusKey.getSubsetName();
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getMotcleStorageFile(StorageDirectories storageDirectories, SubsetKey thesaurusKey, int id) {
        if (!thesaurusKey.isThesaurusSubset()) {
            throw new IllegalArgumentException("!thesaurusKey.isThesaurusSubset()");
        }
        if (id < 0) {
            throw new IllegalArgumentException("id < 0");
        }
        StringBuilder buf = new StringBuilder();
        buf.append(BdfDirectoryConstants.FICHOTHEQUE);
        buf.append(File.separatorChar);
        buf.append(SubsetKey.CATEGORY_THESAURUS_STRING);
        buf.append(File.separatorChar);
        buf.append(thesaurusKey.getSubsetName());
        buf.append(File.separatorChar);
        buf.append("m.");
        buf.append(BdfServerUtils.getMillier(id));
        buf.append(File.separatorChar);
        buf.append(id);
        buf.append(".xml");
        return StorageFile.build(storageDirectories, buf.toString());
    }

    public static StorageFile getRedacteurStorageFile(StorageDirectories storageDirectories, SubsetKey sphereKey, int id) {
        if (!sphereKey.isSphereSubset()) {
            throw new IllegalArgumentException("!sphereKey.isSphereSubset()");
        }
        if (id < 0) {
            throw new IllegalArgumentException("id < 0");
        }
        StringBuilder buf = new StringBuilder();
        buf.append(BdfDirectoryConstants.FICHOTHEQUE);
        buf.append(File.separatorChar);
        buf.append(SubsetKey.CATEGORY_SPHERE_STRING);
        buf.append(File.separatorChar);
        buf.append(sphereKey.getSubsetName());
        buf.append(File.separatorChar);
        buf.append("r.");
        buf.append(BdfServerUtils.getMillier(id));
        buf.append(File.separatorChar);
        buf.append(id);
        buf.append(".xml");
        return StorageFile.build(storageDirectories, buf.toString());
    }

    public static StorageFile getCroisementStorageFile(StorageDirectories storageDirectories, CroisementKey croisementKey) {
        SubsetKey subsetKey1 = croisementKey.getSubsetKey1();
        SubsetKey subsetKey2 = croisementKey.getSubsetKey2();
        int id1 = croisementKey.getId1();
        int id2 = croisementKey.getId2();
        StringBuilder buf = new StringBuilder();
        buf.append(BdfDirectoryConstants.FICHOTHEQUE);
        buf.append(File.separatorChar);
        buf.append(subsetKey1.getCategoryString());
        buf.append("_");
        buf.append(subsetKey2.getCategoryString());
        buf.append(File.separatorChar);
        buf.append(subsetKey1.getSubsetName());
        buf.append("_");
        buf.append(subsetKey2.getSubsetName());
        buf.append(File.separatorChar);
        buf.append("c.");
        buf.append(BdfServerUtils.getMillier(id1));
        buf.append("_");
        buf.append(BdfServerUtils.getMillier(id2));
        buf.append(File.separatorChar);
        buf.append(id1);
        buf.append("_");
        buf.append(id2);
        buf.append(".xml");
        return StorageFile.build(storageDirectories, buf.toString());
    }

    public static String getCroisementDirPath(SubsetKey subsetKey1, SubsetKey subsetKey2) {
        if (subsetKey1.compareTo(subsetKey2) > 0) {
            SubsetKey temp = subsetKey1;
            subsetKey1 = subsetKey2;
            subsetKey2 = temp;
        }
        StringBuilder buf = new StringBuilder();
        buf.append(BdfDirectoryConstants.FICHOTHEQUE);
        buf.append(File.separatorChar);
        buf.append(subsetKey1.getCategoryString());
        buf.append("_");
        buf.append(subsetKey2.getCategoryString());
        buf.append(File.separatorChar);
        buf.append(subsetKey1.getSubsetName());
        buf.append("_");
        buf.append(subsetKey2.getSubsetName());
        return buf.toString();
    }

    public static StorageFile getPasswordStorageFile(StorageDirectories storageDirectories) {
        String path = BdfDirectoryConstants.CONF + File.separator + "passwd";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getUiStorageFile(StorageDirectories storageDirectories, String filebasename) {
        String path = BdfDirectoryConstants.CONF + File.separator + "ui" + File.separator + filebasename + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getAlterUiStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey, String name) {
        String path = BdfDirectoryConstants.CONF + File.separator + "ui" + File.separator + name + File.separator + subsetKey + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static SubsetInfoList getSubsetInfoList(StorageDirectories storageDirectories, short subsetCategory) {
        String rootPath = BdfDirectoryConstants.FICHOTHEQUE + File.separator + SubsetKey.categoryToString(subsetCategory);
        File subsetDirectory = new File(storageDirectories.getDataDir(), rootPath);
        InternalSubsetInfoList result = new InternalSubsetInfoList();
        if (!subsetDirectory.exists()) {
            subsetDirectory.mkdirs();
            return result;
        } else if (!subsetDirectory.isDirectory()) {
            throw new BdfStorageException(subsetDirectory.getPath() + " is not a directory");
        }
        for (File f : subsetDirectory.listFiles()) {
            String name = f.getName();
            if (name.endsWith(".xml")) {
                String basename = name.substring(0, name.length() - 4);
                try {
                    InternalSubsetInfo subsetInfo = new InternalSubsetInfo();
                    subsetInfo.parse(subsetCategory, basename);
                    StorageFile storageFile = StorageFile.build(storageDirectories, rootPath + File.separator + name);
                    subsetInfo.setStorageFile(storageFile);
                    result.add(subsetInfo);
                } catch (IllegalArgumentException iae) {
                } catch (ParseException pe) {
                }
            }
        }
        return result;
    }

    public static StorageFile getOldCroisementStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey1, SubsetKey subsetKey2) {
        if (subsetKey2.compareTo(subsetKey1) < 0) {
            SubsetKey temp = subsetKey1;
            subsetKey1 = subsetKey2;
            subsetKey2 = temp;
        }
        StringBuilder buf = new StringBuilder();
        buf.append(BdfDirectoryConstants.FICHOTHEQUE);
        buf.append(File.separatorChar);
        buf.append(subsetKey1.getCategoryString());
        buf.append("_");
        buf.append(subsetKey2.getCategoryString());
        buf.append(File.separatorChar);
        buf.append(subsetKey1.getSubsetName());
        buf.append("_");
        buf.append(subsetKey2.getSubsetName());
        buf.append(".xml");
        return StorageFile.build(storageDirectories, buf.toString());

    }


    private static class InternalSubsetInfoList implements SubsetInfoList {

        private final List<InternalSubsetInfo> subsetList = new ArrayList<InternalSubsetInfo>();
        private final List<InternalSubsetInfo> satelliteSubsetList = new ArrayList<InternalSubsetInfo>();

        private InternalSubsetInfoList() {
        }

        @Override
        public int getStandardSubsetCount() {
            return subsetList.size();
        }

        @Override
        public SubsetInfo getStandardSubsetInfo(int i) {
            return subsetList.get(i);
        }

        @Override
        public int getSatelliteSubsetCount() {
            return satelliteSubsetList.size();
        }

        @Override
        public SubsetInfo getSatelliteSubsetInfo(int i) {
            return satelliteSubsetList.get(i);
        }

        void add(InternalSubsetInfo subsetInfo) {
            if (subsetInfo.isSatelliteSubset()) {
                satelliteSubsetList.add(subsetInfo);
            } else {
                subsetList.add(subsetInfo);
            }
        }

        void sort() {
            Collections.sort(subsetList);
            Collections.sort(satelliteSubsetList);
        }

    }


    private static class InternalSubsetInfo implements SubsetInfo, Comparable<InternalSubsetInfo> {

        private SubsetKey subsetKey;
        private StorageFile storageFile;
        private SubsetKey masterSubsetKey;

        private InternalSubsetInfo() {
        }

        @Override
        public SubsetKey getSubsetKey() {
            return subsetKey;
        }

        @Override
        public int compareTo(InternalSubsetInfo other) {
            int result = this.subsetKey.compareTo(other.subsetKey);
            if (result == 0) {
                result = this.masterSubsetKey.compareTo(other.masterSubsetKey);
            }
            return result;
        }

        @Override
        public StorageFile getStorageFile() {
            return storageFile;
        }

        void setStorageFile(StorageFile storageFile) {
            this.storageFile = storageFile;
        }

        @Override
        public SubsetKey getMasterSubsetKey() {
            return masterSubsetKey;
        }

        @Override
        public boolean isSatelliteSubset() {
            return (masterSubsetKey != null);
        }

        void parse(short subsetCategory, String baseName) throws ParseException {
            if (baseName.startsWith("_mc_")) {
                parserSatelliteSubsetId(subsetCategory, baseName.substring(4), SubsetKey.CATEGORY_CORPUS);
            } else if (baseName.startsWith("_mt_")) {
                parserSatelliteSubsetId(subsetCategory, baseName.substring(4), SubsetKey.CATEGORY_THESAURUS);
            } else {
                subsetKey = SubsetKey.parse(subsetCategory, baseName);
                masterSubsetKey = null;
            }
        }

        private void parserSatelliteSubsetId(short subsetCategory, String baseName, short masterSubsetCategory) throws ParseException {
            int idx = baseName.indexOf("_");
            if (idx == -1) {
                throw new ParseException("second _ is missing", 4);
            }
            masterSubsetKey = SubsetKey.parse(masterSubsetCategory, baseName.substring(0, idx));
            subsetKey = SubsetKey.parse(subsetCategory, baseName.substring(idx + 1));
        }

    }


    private static class InternalStorageDirectories implements StorageDirectories {

        private final File dataDir;
        private final File backupDir;

        private InternalStorageDirectories(File dataDir, File backupDir) {
            this.dataDir = dataDir;
            this.backupDir = backupDir;
        }

        @Override
        public File getDataDir() {
            return dataDir;
        }

        @Override
        public File getBackupDir() {
            return backupDir;
        }

    }

}
