/* BdfServer_DirectoryStorage - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import fr.exemole.bdfserver.xml.users.BdfUserPrefsXMLPart;
import java.io.BufferedWriter;
import java.io.IOException;
import net.fichotheque.EditOrigin;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.Document;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.croisement.Croisement;
import net.fichotheque.metadata.FichothequeMetadata;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.xml.storage.AddendaMetadataXMLPart;
import net.fichotheque.xml.storage.AlbumMetadataXMLPart;
import net.fichotheque.xml.storage.AttributesStorageXMLPart;
import net.fichotheque.xml.storage.CorpusMetadataXMLPart;
import net.fichotheque.xml.storage.CroisementStorageXMLPart;
import net.fichotheque.xml.storage.DocumentStorageXMLPart;
import net.fichotheque.xml.storage.FicheStorageXMLPart;
import net.fichotheque.xml.storage.FichothequeMetadataXMLPart;
import net.fichotheque.xml.storage.IllustrationStorageXMLPart;
import net.fichotheque.xml.storage.MotcleStorageXMLPart;
import net.fichotheque.xml.storage.RedacteurStorageXMLPart;
import net.fichotheque.xml.storage.SphereListXMLPart;
import net.fichotheque.xml.storage.SphereMetadataXMLPart;
import net.fichotheque.xml.storage.ThesaurusMetadataXMLPart;
import net.fichotheque.xml.storage.ThesaurusTreeXMLPart;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;


/**
 * Dans les différentes méthodes, EditOrigin peut être nul, cela doit
 * correspondre aux cas de mise à jour au démarrage suite à un changement de
 * version du XML.
 *
 * @author Vincent Calame
 */
public final class Save {

    private Save() {
    }

    public static void saveAddendaMetadata(StorageDirectories storageDirectories, Addenda addenda, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getAddendaMetadataStorageFile(storageDirectories, addenda.getSubsetKey());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            AddendaMetadataXMLPart addendaMetadataXMLPart = new AddendaMetadataXMLPart(xmlWriter);
            addendaMetadataXMLPart.appendAddendaMetadata(addenda.getAddendaMetadata());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveAlbumMetadata(StorageDirectories storageDirectories, Album album, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getAlbumMetadataStorageFile(storageDirectories, album.getSubsetKey());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            AlbumMetadataXMLPart albumMetadataXMLPart = new AlbumMetadataXMLPart(xmlWriter);
            albumMetadataXMLPart.appendAlbumMetadata(album.getAlbumMetadata());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveFichothequeMetadata(StorageDirectories storageDirectories, FichothequeMetadata fichothequeMetadata, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getFichothequeMetadataFile(storageDirectories);
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            FichothequeMetadataXMLPart fichothequeMetadataXMLPart = new FichothequeMetadataXMLPart(xmlWriter);
            fichothequeMetadataXMLPart.appendFichothequeMetadata(fichothequeMetadata);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveCorpusMetadata(StorageDirectories storageDirectories, Corpus corpus, EditOrigin editOrigin) {
        SubsetKey masterSubsetKey = null;
        Subset masterSubset = corpus.getMasterSubset();
        if (masterSubset != null) {
            masterSubsetKey = masterSubset.getSubsetKey();
        }
        StorageFile storageFile = BdfStorageUtils.getCorpusMetadataStorageFile(storageDirectories, corpus.getSubsetKey(), masterSubsetKey);
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            CorpusMetadataXMLPart corpusMetadataXMLPart = new CorpusMetadataXMLPart(xmlWriter);
            corpusMetadataXMLPart.appendCorpusMetadata(corpus.getCorpusMetadata());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveSphereMetadata(StorageDirectories storageDirectories, Sphere sphere, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getSphereMetadataStorageFile(storageDirectories, sphere.getSubsetKey());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            SphereMetadataXMLPart sphereMetadataXMLPart = new SphereMetadataXMLPart(xmlWriter);
            sphereMetadataXMLPart.appendSphereMetadata(sphere.getSphereMetadata());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveSphereList(StorageDirectories storageDirectories, Sphere sphere, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getSphereListStorageFile(storageDirectories, sphere.getSubsetKey());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            SphereListXMLPart sphereMetadataXMLPart = new SphereListXMLPart(xmlWriter);
            sphereMetadataXMLPart.appendSphereList(sphere, null);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveThesaurusMetadata(StorageDirectories storageDirectories, Thesaurus thesaurus, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getThesaurusMetadataStorageFile(storageDirectories, thesaurus.getSubsetKey());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            ThesaurusMetadataXMLPart thesaurusMetadataXMLPart = new ThesaurusMetadataXMLPart(xmlWriter);
            thesaurusMetadataXMLPart.appendThesaurusMetadata(thesaurus.getThesaurusMetadata());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveThesaurusTree(StorageDirectories storageDirectories, Thesaurus thesaurus, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getThesaurusTreeStorageFile(storageDirectories, thesaurus.getSubsetKey());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            ThesaurusTreeXMLPart thesaurusTreeXMLPart = new ThesaurusTreeXMLPart(xmlWriter);
            thesaurusTreeXMLPart.appendThesaurus(thesaurus, null);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveMotcle(StorageDirectories storageDirectories, Motcle motcle, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getMotcleStorageFile(storageDirectories, motcle.getSubsetKey(), motcle.getId());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            MotcleStorageXMLPart motcleStorageXMLPart = new MotcleStorageXMLPart(xmlWriter);
            motcleStorageXMLPart.appendMotcle(motcle);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveFiche(StorageDirectories storageDirectories, FicheMeta ficheMeta, FicheAPI fiche, EditOrigin editOrigin) {
        StorageFile storageFile = FicheDirectory.getFicheStorageFile(storageDirectories, ficheMeta.getSubsetKey(), ficheMeta.getId());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            FicheStorageXMLPart ficheStorageXMLPart = new FicheStorageXMLPart(xmlWriter);
            ficheStorageXMLPart.appendFiche(fiche);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveChrono(StorageDirectories storageDirectories, FicheMeta ficheMeta, EditOrigin editOrigin) {
        StorageFile storageFile = FicheDirectory.getChronoStorageFile(storageDirectories, ficheMeta.getSubsetKey(), ficheMeta.getId());
        try (BufferedWriter buf = storageFile.getWriter()) {
            StorageUtils.writeChrono(buf, ficheMeta);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveAttributes(StorageDirectories storageDirectories, FicheMeta ficheMeta, EditOrigin editOrigin) {
        StorageFile storageFile = FicheDirectory.getAttributesStorageFile(storageDirectories, ficheMeta.getSubsetKey(), ficheMeta.getId());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            AttributesStorageXMLPart attributesStorageXMLPart = new AttributesStorageXMLPart(xmlWriter);
            attributesStorageXMLPart.appendAttributes(ficheMeta.getAttributes());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveDocument(StorageDirectories storageDirectories, Document document, EditOrigin editOrigin) {
        StorageFile storageFile = DocumentDirectory.getDocumentStorageFile(storageDirectories, document.getSubsetKey(), document.getId());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            DocumentStorageXMLPart documentStorageXMLPart = new DocumentStorageXMLPart(xmlWriter);
            documentStorageXMLPart.appendDocument(document);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveIllustration(StorageDirectories storageDirectories, Illustration illustration, EditOrigin editOrigin) {
        StorageFile storageFile = IllustrationDirectory.getIllustrationStorageFile(storageDirectories, illustration.getSubsetKey(), illustration.getId());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            IllustrationStorageXMLPart documentStorageXMLPart = new IllustrationStorageXMLPart(xmlWriter);
            documentStorageXMLPart.appendIllustration(illustration);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveRedacteur(StorageDirectories storageDirectories, Redacteur redacteur, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getRedacteurStorageFile(storageDirectories, redacteur.getSubsetKey(), redacteur.getId());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            RedacteurStorageXMLPart redacteurStorageXMLPart = new RedacteurStorageXMLPart(xmlWriter);
            redacteurStorageXMLPart.appendRedacteur(redacteur);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveBdfUserPrefs(StorageDirectories storageDirectories, Redacteur redacteur, BdfUserPrefs bdfUserPrefs) {
        StorageFile storageFile = BdfUserDirectory.getPrefsStorageFile(storageDirectories, redacteur.getSubsetKey(), redacteur.getId());
        try (BufferedWriter buf = storageFile.getWriter()) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            BdfUserPrefsXMLPart bdfUserPrefsXMLPart = new BdfUserPrefsXMLPart(xmlWriter);
            bdfUserPrefsXMLPart.addPrefs(bdfUserPrefs);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void saveCroisement(StorageDirectories storageDirectories, Croisement croisement, EditOrigin editOrigin) {
        StorageFile storageFile = BdfStorageUtils.getCroisementStorageFile(storageDirectories, croisement.getCroisementKey());
        try (BufferedWriter buf = storageFile.archiveAndGetWriter(editOrigin)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            CroisementStorageXMLPart croisementStorageXMLPart = new CroisementStorageXMLPart(xmlWriter);
            croisementStorageXMLPart.appendCroisement(croisement);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

}
