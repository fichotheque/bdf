/* BdfServer_DirectoryStorage - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.bdfdata;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import java.io.File;
import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.io.FileUtils;


/**
 *
 * @author Vincent Calame
 */
public class BdfUserDirectory {

    private final File dir;

    private BdfUserDirectory(File dir) {
        this.dir = dir;
    }

    public boolean delete() {
        try {
            if (dir.exists()) {
                FileUtils.forceDelete(dir);
                return true;
            } else {
                return false;
            }
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static BdfUserDirectory getBdfUserDirectory(StorageDirectories storageDirectories, SubsetKey subsetKey, int id) {
        File dir = new File(storageDirectories.getDataDir(), getPath(subsetKey, id));
        return new BdfUserDirectory(dir);
    }

    public static StorageFile getPrefsStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey, int id) {
        String path = getPath(subsetKey, id) + File.separatorChar + "prefs.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile getIniStorageFile(StorageDirectories storageDirectories, SubsetKey subsetKey, int id, String name) {
        String path = getPath(subsetKey, id) + File.separatorChar + "store_" + name + ".ini";
        return StorageFile.build(storageDirectories, path);
    }

    public static void deleteSphereDir(StorageDirectories storageDirectories, SubsetKey subsetKey) {
        File dir = new File(storageDirectories.getDataDir(), BdfDirectoryConstants.USERS + File.separatorChar + subsetKey.getSubsetName());
        if (dir.exists()) {
            try {
                FileUtils.forceDelete(dir);
            } catch (IOException ioe) {
                throw new BdfStorageException(ioe);
            }
        }
    }

    public static String getPath(SubsetKey subsetKey, int id) {
        if (!subsetKey.isSphereSubset()) {
            throw new IllegalArgumentException("!subsetKey.isSphereSubset()");
        }
        if (id < 0) {
            throw new IllegalArgumentException("id < 0");
        }
        StringBuilder buf = new StringBuilder();
        buf.append(BdfDirectoryConstants.USERS);
        buf.append(File.separatorChar);
        buf.append(subsetKey.getSubsetName());
        buf.append(File.separatorChar);
        buf.append("r.");
        if (id < 1000) {
            buf.append("000");
        } else {
            if (id < 10000) {
                buf.append("00");
            } else if (id < 100000) {
                buf.append("0");
            }
            buf.append(id / 1000);
        }
        buf.append(File.separatorChar);
        buf.append(id);
        return buf.toString();
    }

}
