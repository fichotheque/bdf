/* BdfServer_DirectoryStorage - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.tools;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.storage.directory.bdfdata.BdfDirectoryConstants;
import fr.exemole.bdfserver.storage.directory.bdfdata.BdfStorageUtils;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageFile;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import fr.exemole.bdfserver.xml.roles.RedacteurListXMLPart;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.xml.storage.RedacteurStorageXMLPart;
import net.fichotheque.xml.storage.SphereListXMLPart;
import net.fichotheque.xml.storage.SphereMetadataXMLPart;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;


/**
 *
 * @author Vincent Calame
 */
public final class SphereCopyEngine {

    private final StorageDirectories storageDirectories;
    private final BdfServer sourceBdfServer;


    private SphereCopyEngine(StorageDirectories storageDirectories, BdfServer sourceBdfServer) {
        this.storageDirectories = storageDirectories;
        this.sourceBdfServer = sourceBdfServer;
    }

    private void run() {
        for (Sphere sphere : sourceBdfServer.getFichotheque().getSphereList()) {
            copySphereMetadata(sphere);
            copySphereList(sphere);
            copyRedacteurList(sphere);

        }
        copyAdmin();
        copyPasswords();
    }

    private void copySphereList(Sphere sphere) {
        StorageFile listStorageFile = BdfStorageUtils.getSphereListStorageFile(storageDirectories, sphere.getSubsetKey());
        try (BufferedWriter buf = listStorageFile.getWriter(false)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            SphereListXMLPart sphereMetadataXMLPart = new SphereListXMLPart(xmlWriter);
            sphereMetadataXMLPart.appendSphereList(sphere, null);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    private void copySphereMetadata(Sphere sphere) {
        StorageFile storageFile = BdfStorageUtils.getSphereMetadataStorageFile(storageDirectories, sphere.getSubsetKey());
        try (BufferedWriter buf = storageFile.getWriter(false)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            SphereMetadataXMLPart sphereMetadataXMLPart = new SphereMetadataXMLPart(xmlWriter);
            sphereMetadataXMLPart.appendSphereMetadata(sphere.getSphereMetadata());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    private void copyRedacteurList(Sphere sphere) {
        SubsetKey sphereKey = sphere.getSubsetKey();
        for (Redacteur redacteur : sphere.getRedacteurList()) {
            StorageFile storageFile = BdfStorageUtils.getRedacteurStorageFile(storageDirectories, sphereKey, redacteur.getId());
            try (BufferedWriter buf = storageFile.getWriter(false)) {
                AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
                xmlWriter.appendXMLDeclaration();
                RedacteurStorageXMLPart redacteurStorageXMLPart = new RedacteurStorageXMLPart(xmlWriter);
                redacteurStorageXMLPart.appendRedacteur(redacteur);
            } catch (IOException ioe) {
                throw new BdfStorageException(ioe);
            }
        }
    }

    private void copyAdmin() {
        StorageFile adminFile = StorageFile.build(storageDirectories, BdfDirectoryConstants.CONF + File.separator + "redacteurlists" + File.separator + "admin.xml");
        try (BufferedWriter buf = adminFile.getWriter(false)) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            RedacteurListXMLPart xmlPart = new RedacteurListXMLPart(xmlWriter);
            xmlPart.addRedacteurList(sourceBdfServer.getPermissionManager().getAdminRedacteurList());
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    private void copyPasswords() {
        String[] passwordArray = sourceBdfServer.getPasswordManager().toPasswordArray();
        StorageFile passwordStorageFile = BdfStorageUtils.getPasswordStorageFile(storageDirectories);
        try (BufferedWriter writer = passwordStorageFile.getWriter(false)) {
            StorageUtils.writePasswordArray(writer, passwordArray);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }

    public static void run(StorageDirectories storageDirectories, BdfServer sourceBdfServer) {
        SphereCopyEngine engine = new SphereCopyEngine(storageDirectories, sourceBdfServer);
        engine.run();
    }


}
