/* BdfServer_DirectoryStorage - Copyright (c) 2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.jdbm;

import java.io.IOException;
import java.util.Properties;
import jdbm.RecordManager;
import jdbm.recman.BaseRecordManager;
import jdbm.RecordManagerProvider;
import jdbm.RecordManagerOptions;


/**
 *
 * @author Vincent Calame
 */
public class Provider implements RecordManagerProvider {

    public Provider() {
    }

    public RecordManager createRecordManager(String name, Properties options) throws IOException {
        BaseRecordManager recordManager = new BaseRecordManager(name);
        String value = options.getProperty(RecordManagerOptions.DISABLE_TRANSACTIONS, "false" );
        if ( value.equalsIgnoreCase("true") ) {
            recordManager.disableTransactions();
        }
        return recordManager;
    }

}
