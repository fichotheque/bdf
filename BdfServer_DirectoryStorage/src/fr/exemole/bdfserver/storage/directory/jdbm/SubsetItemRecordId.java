/* BdfServer_DirectoryStorage - Copyright (c) 2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.jdbm;


/**
 *
 * @author Vincent Calame
 */
public class SubsetItemRecordId {

    public final static byte NORMAL_TYPE = 1;
    public final static byte DOUBLE_TYPE = 2;
    private long recid1 = 0;
    private long recid2 = 0;
    private byte type;

    public SubsetItemRecordId(long l) {
        this.recid1 = l;
        this.type = NORMAL_TYPE;
    }

    public SubsetItemRecordId(long l1, long l2) {
        this.recid1 = l1;
        this.recid2 = l2;
        this.type = DOUBLE_TYPE;
    }

    public long getRecid1() {
        return recid1;
    }

    public long getRecid2() {
        return recid2;
    }

    public byte getType() {
        return type;
    }

}
