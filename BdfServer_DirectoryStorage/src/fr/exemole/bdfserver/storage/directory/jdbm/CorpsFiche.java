/* BdfServer_DirectoryStorage - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.jdbm;

import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.Section;


/**
 *
 * @author Vincent Calame
 */
public class CorpsFiche extends CorpsFicheSerializer.Input {

    private final Fiche fiche;

    public CorpsFiche(Fiche fiche) {
        super(fiche);
        this.fiche = fiche;
    }

    public Fiche getFiche() {
        return fiche;
    }

    public void appendCorpsTo(Fiche otherFiche) {
        for (Section section : fiche.getSectionList()) {
            otherFiche.appendSection(section.getFieldKey(), section);
        }
    }

    public void setCorpsTo(Fiche otherFiche) {
        for (Section section : fiche.getSectionList()) {
            otherFiche.setSection(section.getFieldKey(), section);
        }
    }

}
