/* BdfServer_DirectoryStorage - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.jdbm;

import net.fichotheque.corpus.fiche.Fiche;


/**
 *
 * @author Vincent Calame
 */
public class EnteteFiche extends EnteteFicheSerializer.Input {

    private final Fiche fiche;

    public EnteteFiche(int id, Fiche fiche) {
        super(id, fiche);
        this.fiche = fiche;
    }

    public Fiche getFiche() {
        return fiche;
    }

}
