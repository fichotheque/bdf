/* BdfServer_DirectoryStorage - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.storage.directory.jdbm;

import fr.exemole.bdfserver.api.storage.BdfStorageException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import jdbm.helper.Serializer;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheAPI;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.FicheItems;
import net.fichotheque.corpus.fiche.Information;
import net.fichotheque.corpus.fiche.Para;
import net.fichotheque.corpus.fiche.Propriete;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.srz.FichePrimitives;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;


/**
 *
 * @author Vincent Calame
 */
public class EnteteFicheSerializer implements Serializer {

    private final FieldKeyCoder fieldKeyCoder;

    public EnteteFicheSerializer(FieldKeyCoder fieldKeyCoder) {
        this.fieldKeyCoder = fieldKeyCoder;
    }

    @Override
    public byte[] serialize(Object obj) throws IOException {
        Input input = (Input) obj;
        FicheAPI fiche = input.getFicheAPI();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        PrimitivesWriter primWriter = PrimitivesIOFactory.newWriter(output);
        primWriter.writeInt(input.getId());
        Lang lang = fiche.getLang();
        if (lang != null) {
            primWriter.writeString(lang.toString());
        } else {
            primWriter.writeString("");
        }
        primWriter.writeString(fiche.getTitre());
        FichePrimitives.writeFicheItem(primWriter, fiche.getSoustitre());
        FichePrimitives.writeFicheItems(primWriter, fiche.getRedacteurs());
        List<Propriete> proprieteList = fiche.getProprieteList();
        int proprieteSize = proprieteList.size();
        primWriter.writeInt(proprieteSize);
        for (int i = 0; i < proprieteSize; i++) {
            Propriete propriete = proprieteList.get(i);
            int fieldKeyCode = fieldKeyCoder.askForCode(propriete);
            primWriter.writeInt(fieldKeyCode);
            FichePrimitives.writeProprieteFicheItem(primWriter, propriete);
        }
        List<Information> informationList = fiche.getInformationList();
        int informationSize = informationList.size();
        primWriter.writeInt(informationSize);
        for (int i = 0; i < informationSize; i++) {
            Information information = informationList.get(i);
            int fieldKeyCode = fieldKeyCoder.askForCode(information);
            primWriter.writeInt(fieldKeyCode);
            FichePrimitives.writeFicheItems(primWriter, information);
        }
        return output.toByteArray();
    }

    @Override
    public Object deserialize(byte[] serialized) {
        PrimitivesReader primReader = PrimitivesIOFactory.newReader(serialized);
        try {
            int ficheid = primReader.readInt();
            Fiche fiche = new Fiche();
            EnteteFiche enteteFiche = new EnteteFiche(ficheid, fiche);
            String langString = primReader.readString();
            if (!langString.isEmpty()) {
                fiche.setLang(Lang.build(langString));
            }
            String titre = primReader.readString();
            fiche.setTitre(titre);
            Para soustitrePara = (Para) FichePrimitives.readFicheItem(primReader);
            fiche.setSoustitre(soustitrePara);
            FicheItems redacteurList = FichePrimitives.readFicheItems(primReader);
            fiche.setRedacteurs(redacteurList);
            int proprieteCount = primReader.readInt();
            for (int i = 0; i < proprieteCount; i++) {
                int fieldKeyCode = primReader.readInt();
                FicheItem ficheItem = FichePrimitives.readFicheItem(primReader);
                if (ficheItem != null) {
                    FieldKey fieldKey = fieldKeyCoder.getFieldKey(fieldKeyCode);
                    if ((fieldKey != null) && (fieldKey.isPropriete())) {
                        fiche.setPropriete(fieldKey, ficheItem);
                    }
                }
            }
            int informationCount = primReader.readInt();
            for (int i = 0; i < informationCount; i++) {
                int fieldKeyCode = primReader.readInt();
                FicheItems ficheItems = FichePrimitives.readFicheItems(primReader);
                if (ficheItems != null) {
                    FieldKey fieldKey = fieldKeyCoder.getFieldKey(fieldKeyCode);
                    if ((fieldKey != null) && (fieldKey.isInformation())) {
                        fiche.setInformation(fieldKey, ficheItems);
                    }
                }
            }
            return enteteFiche;
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
    }


    public static class Input {

        private final int id;
        private final FicheAPI ficheAPI;

        public Input(int id, FicheAPI ficheAPI) {
            this.id = id;
            this.ficheAPI = ficheAPI;
        }

        public int getId() {
            return id;
        }

        public FicheAPI getFicheAPI() {
            return ficheAPI;
        }

    }

}
