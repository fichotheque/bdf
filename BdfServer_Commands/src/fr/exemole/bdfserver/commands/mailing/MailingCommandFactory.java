/* BdfServer_Commands - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.mailing;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class MailingCommandFactory {

    private MailingCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case SendCommand.COMMANDNAME:
                return new SendCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
