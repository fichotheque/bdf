/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.ui.UiUtils;
import net.fichotheque.ExistingSubsetException;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.tools.corpus.CorpusTools;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CloneCommand extends AbstractCorpusCreationCommand {

    public final static String COMMANDNAME = "Clone";
    public final static String COMMANDKEY = "_ CRP-21";
    public static String NEWCORPUS_PARAMNAME = "newcorpus";
    private Corpus corpus;
    private SubsetKey newCorpusKey;
    private CorpusEditor newCorpusEditor;

    public CloneCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            try {
                newCorpusEditor = session.getFichothequeEditor().createCorpus(newCorpusKey, masterSubset);
            } catch (ExistingSubsetException ese) {
                throw new ShouldNotOccurException("test before ExistingSubsetException");
            }
            CorpusTools.copy(corpus.getCorpusMetadata(), newCorpusEditor.getCorpusMetadataEditor());
            UiUtils.copyUi(session.getBdfServerEditor(), corpus, newCorpusEditor.getCorpus());
        }
        putResultObject(CORPUS_OBJ, newCorpusEditor.getCorpus());
        setDone("_ done.corpus.clone", corpus.getSubsetKeyString(), newCorpusKey.getKeyString());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        corpus = requestHandler.getMandatoryCorpus();
        String newCorpusName = getMandatory(NEWCORPUS_PARAMNAME).trim();
        initSubsetMaster();
        if (newCorpusName.isEmpty()) {
            throw BdfErrors.error("_ error.empty.corpusname");
        }
        try {
            newCorpusKey = SubsetKey.parse(SubsetKey.CATEGORY_CORPUS, newCorpusName);
            if (fichotheque.containsSubset(newCorpusKey)) {
                throw BdfErrors.error("_ error.existing.corpus", newCorpusKey.getKeyString());
            }
        } catch (java.text.ParseException pe) {
            throw BdfErrors.error("_ error.wrong.corpusname", newCorpusName);
        }
    }

}
