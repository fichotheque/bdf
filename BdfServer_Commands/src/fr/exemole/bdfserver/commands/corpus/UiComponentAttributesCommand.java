/* BdfServer_Commands - Copyright (c) 2021-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerEditor;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.ui.CommentUi;
import fr.exemole.bdfserver.api.ui.DataUi;
import fr.exemole.bdfserver.api.ui.FieldUi;
import fr.exemole.bdfserver.api.ui.IncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.ui.components.CommentUiBuilder;
import fr.exemole.bdfserver.tools.ui.components.DataUiBuilder;
import fr.exemole.bdfserver.tools.ui.components.FieldUiBuilder;
import fr.exemole.bdfserver.tools.ui.components.IncludeUiBuilder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class UiComponentAttributesCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "UiComponentAttributes";
    public final static String COMMANDKEY = "_ CRP-30";
    public final static String ATTRIBUTES_PARAMSUFFIX = "|attributes";
    private Corpus corpus;
    private UiComponents uiComponents;
    private List<UiComponent> uiComponentList;

    public UiComponentAttributesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        boolean done = false;
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            BdfServerEditor bdfServerEditor = session.getBdfServerEditor();
            for (UiComponent componentUi : uiComponentList) {
                boolean stepDone = bdfServerEditor.putComponentUi(uiComponents, componentUi);
                if (stepDone) {
                    done = true;
                }
            }
        }
        putResultObject(CORPUS_OBJ, corpus);
        if (done) {
            setDone("_ done.global.attributechange");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        checkSubsetAdmin(corpus);
        uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        uiComponentList = new ArrayList<UiComponent>();
        Set<String> existingSet = new HashSet<String>();
        for (UiComponent uiComponent : uiComponents.getUiComponentList()) {
            String name = uiComponent.getName();
            if (!existingSet.contains(name)) {
                existingSet.add(name);
                String attributes = requestHandler.getTrimedParameter(name + ATTRIBUTES_PARAMSUFFIX);
                if (!attributes.isEmpty()) {
                    UiComponent newUiComponent = getNewComponent(uiComponent, attributes);
                    if (newUiComponent != null) {
                        uiComponentList.add(newUiComponent);
                    }
                }
            }
        }
    }

    private UiComponent getNewComponent(UiComponent uiComponent, String text) {
        if (uiComponent instanceof FieldUi) {
            FieldUiBuilder fieldUiBuilder = FieldUiBuilder.derive((FieldUi) uiComponent);
            AttributeParser.parse(fieldUiBuilder.getAttributeChangeBuilder(), text);
            return fieldUiBuilder.toFieldUi();
        } else if (uiComponent instanceof IncludeUi) {
            IncludeUiBuilder includeUiBuilder = IncludeUiBuilder.init((IncludeUi) uiComponent);
            AttributeParser.parse(includeUiBuilder.getAttributeChangeBuilder(), text);
            return includeUiBuilder.toIncludeUi();
        } else if (uiComponent instanceof CommentUi) {
            CommentUiBuilder commentUiBuilder = CommentUiBuilder.init(bdfServer.getUiManager().getTrustedHtmlFactory(), (CommentUi) uiComponent);
            AttributeParser.parse(commentUiBuilder.getAttributeChangeBuilder(), text);
            return commentUiBuilder.toCommentUi();
        } else if (uiComponent instanceof DataUi) {
            DataUiBuilder dataUiBuilder = DataUiBuilder.init((DataUi) uiComponent);
            AttributeParser.parse(dataUiBuilder.getAttributeChangeBuilder(), text);
            return dataUiBuilder.toDataUi();
        } else {
            return null;
        }
    }

}
