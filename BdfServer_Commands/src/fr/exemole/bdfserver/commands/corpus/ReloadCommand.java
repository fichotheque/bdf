/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.tools.corpus.CorpusTools;
import net.fichotheque.tools.corpus.FieldGenerationEngine;
import net.fichotheque.tools.corpus.PurgeEngine;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ReloadCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "Reload";
    public final static String COMMANDKEY = "_ CRP-20";
    public final static String PURGE_PARAMNAME = "purge";
    private Corpus corpus;
    private boolean purge;

    public ReloadCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            CorpusEditor corpusEditor = session.getFichothequeEditor().getCorpusEditor(corpus.getSubsetKey());
            FieldGenerationEngine engine = BdfCommandUtils.buildEngine(this, corpus);
            PurgeEngine purgeEngine = null;
            if (purge) {
                purgeEngine = new PurgeEngine(CorpusMetadataUtils.getFieldKeyPredicate(corpus.getCorpusMetadata()));
            }
            for (FicheMeta ficheMeta : corpus.getFicheMetaList()) {
                Fiche fiche = corpus.getFiche(ficheMeta);
                if (purgeEngine != null) {
                    purgeEngine.purge(fiche);
                }
                CorpusTools.saveFiche(corpusEditor, ficheMeta, fiche, engine, false);
            }
        }
        putResultObject(CORPUS_OBJ, corpus);
        setDone("_ done.corpus.reload", corpus.getFicheMetaList().size());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        checkSubsetAdmin(corpus);
        purge = requestHandler.isTrue(PURGE_PARAMNAME);
    }


}
