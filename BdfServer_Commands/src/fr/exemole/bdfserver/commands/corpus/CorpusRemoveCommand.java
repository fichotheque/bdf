/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.NoRemoveableSubsetException;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CorpusRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "CorpusRemove";
    public final static String COMMANDKEY = "_ CRP-09";
    private Corpus corpus;

    public CorpusRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        SubsetKey corpusKey = corpus.getSubsetKey();
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            try {
                session.getFichothequeEditor().removeCorpus(corpus);
            } catch (NoRemoveableSubsetException nrse) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        setDone("_ done.corpus.corpusremove", corpusKey.getSubsetName());
        putResultObject(SUBSETKEY_OBJ, corpusKey);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        corpus = requestHandler.getMandatoryCorpus();
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        if (!fichotheque.isRemoveable(corpus)) {
            throw BdfErrors.error("_ error.notremoveable.corpus");
        }
    }

}
