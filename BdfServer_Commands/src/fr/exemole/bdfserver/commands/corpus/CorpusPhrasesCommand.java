/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.commands.AbstractPhrasesCommand;
import java.text.ParseException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadataEditor;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.tools.parsers.TypoParser;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
public class CorpusPhrasesCommand extends AbstractPhrasesCommand {

    public final static String COMMANDNAME = "CorpusPhrases";
    public final static String COMMANDKEY = "_ CRP-10";
    private Corpus corpus;

    public CorpusPhrasesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done;
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            CorpusMetadataEditor corpusMetadataEditor = session.getFichothequeEditor().getCorpusEditor(corpus).getCorpusMetadataEditor();
            done = update(corpusMetadataEditor);
            for (String paramName : requestHandler.getParameterNameSet()) {
                int idx = paramName.lastIndexOf('/');
                if (idx == -1) {
                    continue;
                }
                String fieldName = paramName.substring(0, idx);
                try {
                    Lang currentLang = Lang.parse(paramName.substring(idx + 1));
                    FieldKey fieldKey = FieldKey.parse(fieldName);
                    TypoOptions typoOptions = TypoOptions.getTypoOptions(currentLang.toLocale());
                    String newLabelString = TypoParser.parseTypo(requestHandler.getMandatoryParameter(paramName), typoOptions);
                    CleanedString cleanedString = CleanedString.newInstance(newLabelString);
                    CorpusField corpusField = corpus.getCorpusMetadata().getCorpusField(fieldKey);
                    if (corpusField != null) {
                        boolean stepDone;
                        if (cleanedString == null) {
                            stepDone = corpusMetadataEditor.removeFieldLabel(corpusField, currentLang);
                        } else {
                            stepDone = corpusMetadataEditor.putFieldLabel(corpusField, LabelUtils.toLabel(currentLang, cleanedString));
                        }
                        if (stepDone) {
                            done = true;
                        }
                    }
                } catch (ParseException pe) {
                }
            }
        }
        putResultObject(CORPUS_OBJ, corpus);
        if (done) {
            setDone("_ done.corpus.corpusphrases");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        checkSubsetAdmin(corpus);
        checkPhrasesParameters();
    }

}
