/* BdfServer_Commands - Copyright (c) 2022-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.corpus.fiche.Section;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.tools.corpus.CorpusTools;
import net.fichotheque.tools.corpus.FieldGenerationEngine;
import net.fichotheque.tools.parsers.ficheblock.FicheBlockParser;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
public class SectionMergeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "SectionMerge";
    public final static String COMMANDKEY = "_ CRP-32";
    public final static String ORIGIN_PARAMNAME = "origin";
    public final static String TRANSITIONTEXT_PARAMNAME = "transitiontext";
    public final static String DESTINATION_PARAMNAME = "destination";
    private Corpus corpus;
    private CorpusField originField;
    private CorpusField destinationField;
    private FicheBlocks transitionBlocks;

    public SectionMergeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            CorpusEditor corpusEditor = session.getFichothequeEditor().getCorpusEditor(corpus);
            FieldGenerationEngine engine = BdfCommandUtils.buildEngine(this, corpus);
            FieldKey destinationKey = destinationField.getFieldKey();
            for (FicheMeta ficheMeta : corpus.getFicheMetaList()) {
                Fiche fiche = corpus.getFiche(ficheMeta);
                Section section = fiche.getSection(originField.getFieldKey());
                if ((section != null) && (!section.isEmpty())) {
                    fiche.appendSection(destinationKey, transitionBlocks);
                    fiche.appendSection(destinationKey, section);
                    CorpusTools.saveFiche(corpusEditor, ficheMeta, fiche, engine, false);
                }
            }
        }
        putResultObject(CORPUS_OBJ, corpus);
        setDone("_ done.corpus.sectionmerge", originField.getFieldKey().getKeyString(), destinationField.getFieldKey().getKeyString());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        checkSubsetAdmin(corpus);
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_merge");
        }
        originField = getCorpusField(corpus, ORIGIN_PARAMNAME);
        destinationField = getCorpusField(corpus, DESTINATION_PARAMNAME);
        if (originField.getFieldKey().equals(destinationField.getFieldKey())) {
            throw BdfErrors.error("_ error.unsupported.samefield");
        }
        transitionBlocks = getTransitionFicheBlocks();
    }

    private CorpusField getCorpusField(Corpus corpus, String parameter) throws ErrorMessageException {
        String fieldName = getMandatory(parameter);
        try {
            FieldKey fieldKey = FieldKey.parse(fieldName);
            if (!fieldKey.isSection()) {
                throw BdfErrors.unsupportedParameterValue(parameter, fieldName);
            }
            CorpusField field = corpus.getCorpusMetadata().getCorpusField(fieldKey);
            if (field == null) {
                throw BdfErrors.unknownParameterValue(parameter, fieldName);
            }
            return field;
        } catch (ParseException pe) {
            throw BdfErrors.wrongParameterValue(parameter, fieldName);
        }
    }

    private FicheBlocks getTransitionFicheBlocks() throws ErrorMessageException {
        if (!requestHandler.hasParameter(TRANSITIONTEXT_PARAMNAME)) {
            return FicheUtils.EMPTY_FICHEBLOCKS;
        }
        String text = requestHandler.getMandatoryParameter(TRANSITIONTEXT_PARAMNAME);
        if (text.isEmpty()) {
            return FicheUtils.EMPTY_FICHEBLOCKS;
        }
        TypoOptions typoOptions = BdfUserUtils.getTypoOptions(bdfUser);
        FicheBlockParser ficheBlockParser = new FicheBlockParser(getContentChecker(), typoOptions, false);
        List<FicheBlock> list = new ArrayList<FicheBlock>();
        ficheBlockParser.parseFicheBlockList(text, list);
        return FicheUtils.toFicheBlocks(list);
    }

}
