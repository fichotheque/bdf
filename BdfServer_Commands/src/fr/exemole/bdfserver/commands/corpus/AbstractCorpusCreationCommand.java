/* BdfServer_Commands - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.Subset;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractCorpusCreationCommand extends AbstractBdfCommand {

    public final static String CORPUSTYPE_PARAMNAME = "corpustype";
    public final static String STANDARD_PARAMVALUE = "standard";
    public final static String CORPUSSATELLITE_PARAMVALUE = "corpussatellite";
    public final static String THESAURUSSATELLITE_PARAMVALUE = "thesaurussatellite";
    public final static String MASTERTHESAURUS_PARAMNAME = "masterthesaurus";
    public final static String MASTERCORPUS_PARAMNAME = "mastercorpus";
    protected Subset masterSubset;

    public AbstractCorpusCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    protected void initSubsetMaster() throws ErrorMessageException {
        String corpusType = getMandatory(CORPUSTYPE_PARAMNAME);
        if (corpusType.equals(STANDARD_PARAMVALUE)) {
            masterSubset = null;
        } else if (corpusType.equals(THESAURUSSATELLITE_PARAMVALUE)) {
            String masterthesaurusId = getMandatory(MASTERTHESAURUS_PARAMNAME);
            masterSubset = FichothequeUtils.getThesaurus(bdfServer.getFichotheque(), masterthesaurusId);
            if (masterSubset == null) {
                throw BdfErrors.unknownParameterValue(MASTERTHESAURUS_PARAMNAME, masterthesaurusId);
            }
        } else if (corpusType.equals(CORPUSSATELLITE_PARAMVALUE)) {
            String mastercorpusId = getMandatory(MASTERCORPUS_PARAMNAME);
            masterSubset = FichothequeUtils.getCorpus(bdfServer.getFichotheque(), mastercorpusId);
            if (masterSubset == null) {
                throw BdfErrors.unknownParameterValue(MASTERCORPUS_PARAMNAME, mastercorpusId);
            }
            if (((Corpus) masterSubset).getMasterSubset() != null) {
                throw BdfErrors.unsupportedParameterValue(MASTERCORPUS_PARAMNAME, mastercorpusId);
            }
        } else {
            throw BdfErrors.unknownParameterValue(CORPUSTYPE_PARAMNAME, corpusType);
        }
    }

}
