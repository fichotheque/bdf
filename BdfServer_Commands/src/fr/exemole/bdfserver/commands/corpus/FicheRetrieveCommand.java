/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.namespaces.BdfSpace;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FicheRetrieveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "FicheRetrieve";
    public final static String COMMANDKEY = "_ CRP-28";
    private FicheMeta ficheMeta;

    public FicheRetrieveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            FichothequeEditor fichothequeEditor = session.getFichothequeEditor();
            fichothequeEditor.removeAttribute(ficheMeta, BdfSpace.DISCARDTEXT_KEY);
            int ficheid = ficheMeta.getId();
            for (Corpus corpus : ficheMeta.getCorpus().getSatelliteCorpusList()) {
                FicheMeta satelliteFicheMeta = corpus.getFicheMetaById(ficheid);
                if (satelliteFicheMeta != null) {
                    fichothequeEditor.removeAttribute(satelliteFicheMeta, BdfSpace.DISCARDTEXT_KEY);
                }
            }
        }
        putResultObject(CORPUS_OBJ, ficheMeta.getCorpus());
        putResultObject(FICHEMETA_OBJ, ficheMeta);
        setDone("_ done.corpus.ficheretrieve");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        ficheMeta = requestHandler.getMandatoryFicheMeta();
        getPermissionChecker()
                .checkWrite(ficheMeta);
    }

}
