/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.metadata.CorpusField;
import net.fichotheque.corpus.metadata.CorpusMetadataEditor;
import net.fichotheque.corpus.metadata.FieldKey;
import net.fichotheque.corpus.metadata.MandatoryFieldException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FieldRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "FieldRemove";
    public final static String COMMANDKEY = "_ CRP-08";
    public final static String FIELDKEY_PARAMNAME = "fieldkey";
    private Corpus corpus;
    private CorpusField corpusField;

    public FieldRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        String key;
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            CorpusMetadataEditor corpusMetadataEditor = session.getFichothequeEditor().getCorpusEditor(corpus).getCorpusMetadataEditor();
            try {
                key = corpusField.getFieldString();
                corpusMetadataEditor.removeCorpusField(corpusField);
            } catch (MandatoryFieldException mfe) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        putResultObject(CORPUS_OBJ, corpus);
        setDone("_ done.corpus.fieldremove", key);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        checkSubsetAdmin(corpus);
        String fieldKeyValue = getMandatory(FIELDKEY_PARAMNAME);
        fieldKeyValue = fieldKeyValue.trim();
        if (fieldKeyValue.length() == 0) {
            throw BdfErrors.error("_ error.empty.fieldkey");
        }
        FieldKey fieldKey = null;
        try {
            fieldKey = FieldKey.parse(fieldKeyValue);
        } catch (ParseException pe) {
            throw BdfErrors.wrongParameterValue(FIELDKEY_PARAMNAME, fieldKeyValue);
        }
        corpusField = corpus.getCorpusMetadata().getCorpusField(fieldKey);
        if ((corpusField == null) || (fieldKey.isMandatoryField())) {
            throw BdfErrors.wrongParameterValue(FIELDKEY_PARAMNAME, fieldKeyValue);
        }
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
    }

}
