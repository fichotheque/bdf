/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerEditor;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.ui.CommentUi;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.instruction.RequestHandler;
import fr.exemole.bdfserver.tools.ui.components.CommentUiBuilder;
import java.text.ParseException;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CommentChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "CommentChange";
    public final static String COMMANDKEY = "_ CRP-16";
    public final static String NAME_PARAMNAME = "name";
    public final static String LOCATION_PARAMNAME = "location";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    public final static String COMMENT_PREFIX = "comment/";
    public final static String LOCATION_FORM_PARAMVALUE = "form";
    public final static String LOCATION_TEMPLATE_PARAMVALUE = "template";
    public final static String LOCATION_BOTH_PARAMVALUE = "both";
    private Corpus corpus;
    private UiComponents uiComponents;
    private CommentUi commentUi;
    private int location;
    private String attributes;

    public CommentChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done;
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            done = updateCommentUi(session.getBdfServerEditor(), commentUi);
        }
        putResultObject(CORPUS_OBJ, corpus);
        if (done) {
            setDone("_ done.corpus.commentchange", commentUi.getCommentName());
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        checkSubsetAdmin(corpus);
        String name = getMandatory(NAME_PARAMNAME);
        uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        commentUi = (CommentUi) uiComponents.getUiComponent(CommentUi.toComponentName(name));
        if (commentUi == null) {
            throw BdfErrors.unknownParameterValue(NAME_PARAMNAME, name);
        }
        location = getLocation(requestHandler, LOCATION_PARAMNAME);
        this.attributes = getMandatory(ATTRIBUTES_PARAMNAME);
    }

    private boolean updateCommentUi(BdfServerEditor bdfServerEditor, CommentUi commentUi) throws ErrorMessageException {
        CommentUiBuilder commentUiBuilder = CommentUiBuilder.init(bdfServer.getUiManager().getTrustedHtmlFactory(), commentUi)
                .setLocation(location);
        AttributeParser.parse(commentUiBuilder.getAttributeChangeBuilder(), attributes);
        putHtmlString(commentUiBuilder, requestHandler, COMMENT_PREFIX);
        return bdfServerEditor.putComponentUi(uiComponents, commentUiBuilder.toCommentUi());
    }

    public static int getLocation(RequestHandler requestHandler, String paramName) throws ErrorMessageException {
        String positionString = requestHandler.getMandatoryParameter(paramName);
        if (positionString.equals(LOCATION_FORM_PARAMVALUE)) {
            return CommentUi.FORM_BITVALUE;
        } else if (positionString.equals(LOCATION_TEMPLATE_PARAMVALUE)) {
            return CommentUi.TEMPLATE_BITVALUE;
        } else if (positionString.equals(LOCATION_BOTH_PARAMVALUE)) {
            return CommentUi.ALL_MASK;
        } else {
            throw BdfErrors.unknownParameterValue(paramName, positionString);
        }
    }

    public static void putHtmlString(CommentUiBuilder commentUiBuilder, RequestHandler requestHandler, String paramPrefix) throws ErrorMessageException {
        int prefixLength = paramPrefix.length();
        for (String paramName : requestHandler.getParameterNameSet()) {
            if (!paramName.startsWith(paramPrefix)) {
                continue;
            }
            try {
                Lang currentLang = Lang.parse(paramName.substring(prefixLength));
                String value = requestHandler.getMandatoryParameter(paramName);
                commentUiBuilder.putHtml(currentLang, value);
            } catch (ParseException pe) {
            }
        }
    }

}
