/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.namespaces.BdfSpace;
import net.fichotheque.tools.FichothequeTools;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class FicheRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "FicheRemove";
    public final static String COMMANDKEY = "_ CRP-11";
    public final static String DISCARDTEXT_PARAMNAME = "discardtext";
    private FicheMeta ficheMeta;
    private Attribute discardAttribute;

    public FicheRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Corpus corpus = ficheMeta.getCorpus();
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            FichothequeEditor fichothequeEditor = session.getFichothequeEditor();
            fichothequeEditor.putAttribute(ficheMeta, discardAttribute);
            int ficheid = ficheMeta.getId();
            for (Corpus satelliteCorpus : ficheMeta.getCorpus().getSatelliteCorpusList()) {
                FicheMeta satelliteFicheMeta = satelliteCorpus.getFicheMetaById(ficheid);
                if (satelliteFicheMeta != null) {
                    fichothequeEditor.putAttribute(satelliteFicheMeta, discardAttribute);
                }
            }
            fichothequeEditor.saveChanges();
            FichothequeTools.remove(fichothequeEditor, ficheMeta);
        }
        putResultObject(CORPUS_OBJ, corpus);
        setDone("_ done.corpus.ficheremove");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        ficheMeta = requestHandler.getMandatoryFicheMeta();
        checkSubsetAdmin(ficheMeta.getCorpus());
        String discardText = getMandatory(DISCARDTEXT_PARAMNAME);
        String[] tokens = StringUtils.getLineTokens(discardText, StringUtils.EMPTY_EXCLUDE);
        if (tokens.length == 0) {
            throw BdfErrors.error("_ error.empty.discardtext_remove");
        }
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        AttributeBuilder builder = new AttributeBuilder(BdfSpace.DISCARDTEXT_KEY);
        for (String token : tokens) {
            builder.addValue(token);
        }
        this.discardAttribute = builder.toAttribute();
    }


}
