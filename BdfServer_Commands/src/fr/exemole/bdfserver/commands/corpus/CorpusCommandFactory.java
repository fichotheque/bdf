/* BdfServer_Commands - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusCommandFactory {

    private CorpusCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case CorpusCreationCommand.COMMANDNAME:
                return new CorpusCreationCommand(bdfServer, requestMap);
            case SoustitreActivationCommand.COMMANDNAME:
                return new SoustitreActivationCommand(bdfServer, requestMap);
            case FieldCreationCommand.COMMANDNAME:
                return new FieldCreationCommand(bdfServer, requestMap);
            case CorpusRemoveCommand.COMMANDNAME:
                return new CorpusRemoveCommand(bdfServer, requestMap);
            case FieldOptionsCommand.COMMANDNAME:
                return new FieldOptionsCommand(bdfServer, requestMap);
            case FieldRemoveCommand.COMMANDNAME:
                return new FieldRemoveCommand(bdfServer, requestMap);
            case ConfChangeCommand.COMMANDNAME:
                return new ConfChangeCommand(bdfServer, requestMap);
            case CorpusPhrasesCommand.COMMANDNAME:
                return new CorpusPhrasesCommand(bdfServer, requestMap);
            case SubsetIncludeCreationCommand.COMMANDNAME:
                return new SubsetIncludeCreationCommand(bdfServer, requestMap);
            case SpecialIncludeAddCommand.COMMANDNAME:
                return new SpecialIncludeAddCommand(bdfServer, requestMap);
            case IncludeChangeCommand.COMMANDNAME:
                return new IncludeChangeCommand(bdfServer, requestMap);
            case IncludeRemoveCommand.COMMANDNAME:
                return new IncludeRemoveCommand(bdfServer, requestMap);
            case CommentCreationCommand.COMMANDNAME:
                return new CommentCreationCommand(bdfServer, requestMap);
            case CommentChangeCommand.COMMANDNAME:
                return new CommentChangeCommand(bdfServer, requestMap);
            case CommentRemoveCommand.COMMANDNAME:
                return new CommentRemoveCommand(bdfServer, requestMap);
            case CloneCommand.COMMANDNAME:
                return new CloneCommand(bdfServer, requestMap);
            case ConversionCommand.COMMANDNAME:
                return new ConversionCommand(bdfServer, requestMap);
            case DuplicationCommand.COMMANDNAME:
                return new DuplicationCommand(bdfServer, requestMap);
            case ReponderationCommand.COMMANDNAME:
                return new ReponderationCommand(bdfServer, requestMap);
            case CorpusAttributeChangeCommand.COMMANDNAME:
                return new CorpusAttributeChangeCommand(bdfServer, requestMap);
            case FicheAttributeChangeCommand.COMMANDNAME:
                return new FicheAttributeChangeCommand(bdfServer, requestMap);
            case ImageToIllustrationCommand.COMMANDNAME:
                return new ImageToIllustrationCommand(bdfServer, requestMap);
            case FicheRemoveCommand.COMMANDNAME:
                return new FicheRemoveCommand(bdfServer, requestMap);
            case FicheDiscardCommand.COMMANDNAME:
                return new FicheDiscardCommand(bdfServer, requestMap);
            case FicheRetrieveCommand.COMMANDNAME:
                return new FicheRetrieveCommand(bdfServer, requestMap);
            case CroisementCopyCommand.COMMANDNAME:
                return new CroisementCopyCommand(bdfServer, requestMap);
            case FicheRestoreCommand.COMMANDNAME:
                return new FicheRestoreCommand(bdfServer, requestMap);
            case ReloadCommand.COMMANDNAME:
                return new ReloadCommand(bdfServer, requestMap);
            case SectionMergeCommand.COMMANDNAME:
                return new SectionMergeCommand(bdfServer, requestMap);
            case UiComponentAttributesCommand.COMMANDNAME:
                return new UiComponentAttributesCommand(bdfServer, requestMap);
            case UiComponentOptionsCommand.COMMANDNAME:
                return new UiComponentOptionsCommand(bdfServer, requestMap);
            case UiComponentPositionCommand.COMMANDNAME:
                return new UiComponentPositionCommand(bdfServer, requestMap);
            case DataCreationCommand.COMMANDNAME:
                return new DataCreationCommand(bdfServer, requestMap);
            case DataChangeCommand.COMMANDNAME:
                return new DataChangeCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
