/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.corpus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.ui.IncludeUi;
import fr.exemole.bdfserver.api.ui.UiComponent;
import fr.exemole.bdfserver.api.ui.UiComponents;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.corpus.Corpus;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class IncludeRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "IncludeRemove";
    public final static String COMMANDKEY = "_ CRP-14";
    public final static String NAME_PARAMNAME = "name";
    private Corpus corpus;
    private String name;
    private UiComponents uiComponents;

    public IncludeRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        boolean done;
        try (EditSession session = startEditSession(Domains.CORPUS, COMMANDNAME)) {
            done = session.getBdfServerEditor().removeComponentUi(uiComponents, name);
        }
        putResultObject(CORPUS_OBJ, corpus);
        if (done) {
            setDone("_ done.corpus.includeremove", name);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        checkSubsetAdmin(corpus);
        name = getMandatory(NAME_PARAMNAME);
        uiComponents = bdfServer.getUiManager().getMainUiComponents(corpus);
        UiComponent current = uiComponents.getUiComponent(name);
        if (current == null) {
            throw BdfErrors.unknownParameterValue(NAME_PARAMNAME, name);
        } else if (!(current instanceof IncludeUi)) {
            throw BdfErrors.wrongParameterValue(NAME_PARAMNAME, name);
        }
    }

}
