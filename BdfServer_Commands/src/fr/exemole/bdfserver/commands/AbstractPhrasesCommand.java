/* BdfServer_Commands - Copyright (c) 2018-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.util.HashMap;
import java.util.Map;
import net.fichotheque.MetadataEditor;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractPhrasesCommand extends AbstractBdfCommand {

    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String NEWPHRASE_PARAMNAME = "newphrase";
    public final static String NEWPHRASE_PARAMPREFIX = "newphrase/";
    private final Map<String, LabelChange> phraseChangeMap = new HashMap<String, LabelChange>();
    private LabelChange titleLabelChange;
    private String newPhraseName;
    private LabelChange newPhraseLabelChange;

    public AbstractPhrasesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    protected boolean update(MetadataEditor metadataEditor) {
        boolean done = false;
        if (metadataEditor.changeLabels(null, titleLabelChange)) {
            done = true;
        }
        for (Map.Entry<String, LabelChange> entry : phraseChangeMap.entrySet()) {
            if (metadataEditor.changeLabels(entry.getKey(), entry.getValue())) {
                done = true;
            }
        }
        if (newPhraseName != null) {
            if (metadataEditor.changeLabels(newPhraseName, newPhraseLabelChange)) {
                done = true;
            }
        }
        return done;
    }

    protected void checkPhrasesParameters() throws ErrorMessageException {
        newPhraseName = requestHandler.getTrimedParameter(NEWPHRASE_PARAMNAME);
        if (!newPhraseName.isEmpty()) {
            if (FichothequeUtils.isValidPhraseName(newPhraseName)) {
                newPhraseLabelChange = requestHandler.getLabelChange(NEWPHRASE_PARAMPREFIX);
            } else {
                newPhraseName = null;
                throw BdfErrors.error("_ error.wrong.phrasename", newPhraseName);
            }
        } else {
            newPhraseName = null;
        }
        titleLabelChange = requestHandler.getLabelChange(TITLE_PARAMPREFIX);
        BdfCommandUtils.populatePhraseLabelChange(requestHandler.getRequestMap(), phraseChangeMap);
    }

}
