/* BdfServer_Commands - Copyright (c) 2019-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.SortConstants;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.RangeCondition;
import net.fichotheque.tools.selection.FicheQueryBuilder;
import net.fichotheque.utils.selection.RangeConditionBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.primitives.Range;
import net.mapeadores.util.primitives.Ranges;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class UserSelectionByFicheCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "UserSelectionByFiche";
    private FicheMeta ficheMeta;

    public UserSelectionByFicheCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        FicheQuery ficheQuery = toFicheQuery();
        bdfServer.getSelectionManager().setFicheSelection(bdfUser, ficheQuery, SortConstants.ID_ASC);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        ficheMeta = requestHandler.getMandatoryFicheMeta(true);
    }

    private FicheQuery toFicheQuery() {
        Ranges ranges = new Ranges();
        ranges.addRange(new Range(ficheMeta.getId(), ficheMeta.getId()));
        RangeCondition rangeCondition = RangeConditionBuilder.init(ranges).toRangeCondition();
        return FicheQueryBuilder.init()
                .addCorpus(ficheMeta.getCorpus())
                .setIdRangeCondition(rangeCondition)
                .toFicheQuery();
    }

}
