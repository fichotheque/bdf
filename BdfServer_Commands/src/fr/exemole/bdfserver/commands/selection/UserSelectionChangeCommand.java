/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.SelectionManager;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.interaction.FicheQueryParser;
import net.fichotheque.corpus.SortConstants;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.SelectionDef;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class UserSelectionChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "UserSelectionChange";
    public final static String SORTTYPE_PARAMNAME = "sorttype";
    public final static String SELECTIONTYPE_PARAMNAME = "selectiontype";
    public final static String SELECTIONDEF_TYPE_PARAMVALUE = "selectiondef";
    public final static String SIMPLE_TYPE_PARAMVALUE = "simple";
    private String sortType;
    private SelectionDef selectionDef;
    private FicheQuery ficheQuery;

    public UserSelectionChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        SelectionManager selectionManager = bdfServer.getSelectionManager();
        if (selectionDef != null) {
            selectionManager.setFicheSelection(bdfUser, selectionDef.getName(), sortType);
        } else {
            selectionManager.setFicheSelection(bdfUser, ficheQuery, sortType);
        }
        bdfServer.store(bdfUser, "selection", "sort", sortType);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        String sorttypeParamValue = requestHandler.getTrimedParameter(SORTTYPE_PARAMNAME);
        if (!sorttypeParamValue.isEmpty()) {
            try {
                sortType = SortConstants.checkSortType(sorttypeParamValue);
            } catch (IllegalArgumentException iae) {
                throw BdfErrors.unknownParameterValue(SORTTYPE_PARAMNAME, sorttypeParamValue);
            }
        } else {
            sortType = SortConstants.ID_ASC;
        }
        String selectionType = requestHandler.getTrimedParameter(SELECTIONTYPE_PARAMNAME);
        switch (selectionType) {
            case SELECTIONDEF_TYPE_PARAMVALUE:
                selectionDef = requestHandler.getMandatorySelectionDef();
                break;
            default:
                ficheQuery = FicheQueryParser.parse(requestHandler.getRequestMap(), bdfServer, bdfUser);
        }
    }

}
