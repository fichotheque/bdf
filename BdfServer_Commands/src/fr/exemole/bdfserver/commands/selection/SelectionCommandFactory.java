/* BdfServer_Commands - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class SelectionCommandFactory {

    private SelectionCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case SelectionDefCreationCommand.COMMANDNAME:
                return new SelectionDefCreationCommand(bdfServer, requestMap);
            case SelectionDefRemoveCommand.COMMANDNAME:
                return new SelectionDefRemoveCommand(bdfServer, requestMap);
            case SelectionDefChangeCommand.COMMANDNAME:
                return new SelectionDefChangeCommand(bdfServer, requestMap);
            case UserSelectionChangeCommand.COMMANDNAME:
                return new UserSelectionChangeCommand(bdfServer, requestMap);
            case UserDefaultSelectionCommand.COMMANDNAME:
                return new UserDefaultSelectionCommand(bdfServer, requestMap);
            case UserSelectionByFicheCommand.COMMANDNAME:
                return new UserSelectionByFicheCommand(bdfServer, requestMap);
            case UserSelectionByMotcleCommand.COMMANDNAME:
                return new UserSelectionByMotcleCommand(bdfServer, requestMap);
            case FqlParserCommand.COMMANDNAME:
                return new FqlParserCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
