/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.corpus.SortConstants;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.MotcleQuery;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.tools.selection.FicheQueryBuilder;
import net.fichotheque.tools.selection.MotcleQueryBuilder;
import net.fichotheque.utils.SelectionUtils;
import net.fichotheque.utils.selection.RangeConditionBuilder;
import net.mapeadores.util.conditions.ConditionsConstants;
import net.mapeadores.util.conditions.ConditionsUtils;
import net.mapeadores.util.conditions.TextCondition;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.primitives.Range;
import net.mapeadores.util.primitives.Ranges;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class UserSelectionByMotcleCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "UserSelectionByMotcle";
    private Motcle motcle;

    public UserSelectionByMotcleCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        FicheQuery ficheQuery = toFicheQuery();
        bdfServer.getSelectionManager().setFicheSelection(bdfUser, ficheQuery, SortConstants.ID_ASC);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        motcle = requestHandler.getMandatoryMotcle();
    }

    private FicheQuery toFicheQuery() {
        MotcleQueryBuilder motcleQueryBuilder = new MotcleQueryBuilder();
        if (motcle.getThesaurus().isIdalphaType()) {
            String idalpha = motcle.getIdalpha();
            TextCondition condition = ConditionsUtils.parseCondition(idalpha, ConditionsConstants.LOGICALOPERATOR_AND);
            motcleQueryBuilder.setContentCondition(condition, MotcleQuery.SCOPE_IDALPHA_ONLY);
        } else {
            Ranges ranges = new Ranges();
            ranges.addRange(new Range(motcle.getId(), motcle.getId()));
            motcleQueryBuilder.setIdRangeCondition(RangeConditionBuilder.init(ranges).toRangeCondition());
        }
        motcleQueryBuilder
                .addThesaurus(motcle.getThesaurus());
        return FicheQueryBuilder.init()
                .addMotcleConditionEntry(SelectionUtils.toMotcleConditionEntry(motcleQueryBuilder.toMotcleQuery(), null, true))
                .toFicheQuery();
    }

}
