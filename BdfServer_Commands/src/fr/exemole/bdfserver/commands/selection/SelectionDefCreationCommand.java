/* BdfServer_Commands - Copyright (c) 2023-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.SelectionManager;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.selection.FicheQuery;
import net.fichotheque.selection.SelectionDef;
import net.fichotheque.tools.selection.SelectionDefBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class SelectionDefCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "SelectionDefCreation";
    public final static String COMMANDKEY = "_ EXP-01";
    public final static String NEWSELECTION_PARAMNAME = "newselection";
    public final static String FILL_PARAMNAME = "fill";
    public final static String CURRENT_PARAMVALUE = "current";
    private String newSelectionName;
    private boolean fillWithCurrent;

    public SelectionDefCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        SelectionManager selectionManager = bdfServer.getSelectionManager();
        SelectionDefBuilder selectionDefBuilder = SelectionDefBuilder.init(newSelectionName);
        if (fillWithCurrent) {
            FicheQuery ficheQuery = bdfUser.getFicheQuery();
            selectionDefBuilder.getFichothequeQueriesBuilder().addFicheQuery(ficheQuery);
        }
        SelectionDef selectionDef = selectionDefBuilder.toSelectionDef();
        selectionManager.putSelectionDef(selectionDef);
        putResultObject(SELECTIONDEF_OBJ, selectionDef);
        setDone("_ done.selection.selectiondefcreation", newSelectionName);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        newSelectionName = getMandatory(NEWSELECTION_PARAMNAME).trim();
        if (newSelectionName.isEmpty()) {
            throw BdfErrors.error("_ error.empty.selectionname");
        }
        SelectionDef selectionDef = bdfServer.getSelectionManager().getSelectionDef(newSelectionName);
        if (selectionDef != null) {
            throw BdfErrors.error("_ error.existing.selection", newSelectionName);
        }
        if (!StringUtils.isTechnicalName(newSelectionName, true)) {
            throw BdfErrors.error("_ error.wrong.selectionname", newSelectionName);
        }
        String fillParam = requestHandler.getTrimedParameter(FILL_PARAMNAME);
        if (fillParam.equals(CURRENT_PARAMVALUE)) {
            fillWithCurrent = true;
        }
    }

}
