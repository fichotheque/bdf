/* BdfServer_Commands - Copyright (c) 2023-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.selection.SelectionDef;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class SelectionDefRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "SelectionDefRemove";
    public final static String COMMANDKEY = "_ EXP-02";
    private String name;

    public SelectionDefRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        bdfServer.getSelectionManager().removeSelectionDef(name);
        setDone("_ done.selection.selectiondefremove", name);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        SelectionDef selectionDef = requestHandler.getMandatorySelectionDef();
        this.name = selectionDef.getName();
    }

}
