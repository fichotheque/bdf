/* BdfServer_Commands - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.selection;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.tools.selection.FichothequeQueriesBuilder;
import net.fichotheque.tools.selection.SelectionDOMUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public class FqlParserCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "FqlParser";
    public final static String XML_PARAMNAME = "xml";
    private String xml;

    public FqlParserCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        String xmlContent = checkXML();
        FichothequeQueriesBuilder fichothequeQueriesBuilder = new FichothequeQueriesBuilder();
        try {
            Document document = DOMUtils.parseDocument(xmlContent);
            DOMUtils.readChildren(document.getDocumentElement(), SelectionDOMUtils.getQueryElementConsumer(fichotheque, fichothequeQueriesBuilder));
        } catch (SAXException saxException) {
            throw BdfErrors.error("_ error.exception.xml.sax", saxException.getMessage());
        }
        putResultObject(FICHOTHEQUEQUERIES_OBJ, fichothequeQueriesBuilder.toFichothequeQueries());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        xml = getMandatory(XML_PARAMNAME);
    }

    private String checkXML() {
        int idx = xml.indexOf("<fql");
        if (idx == -1) {
            idx = xml.indexOf("<queries");
        }
        if (idx == -1) {
            return "<fql>" + xml + "</fql>";
        } else {
            return xml;
        }
    }

}
