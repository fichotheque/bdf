/* BdfServer_Commands - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ConfigurationCommandFactory {

    private ConfigurationCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case GroupChangeCommand.COMMANDNAME:
                return new GroupChangeCommand(bdfServer, requestMap);
            case GroupCreationCommand.COMMANDNAME:
                return new GroupCreationCommand(bdfServer, requestMap);
            case GroupRemoveCommand.COMMANDNAME:
                return new GroupRemoveCommand(bdfServer, requestMap);
            case IdentificationCommand.COMMANDNAME:
                return new IdentificationCommand(bdfServer, requestMap);
            case FichothequePhrasesCommand.COMMANDNAME:
                return new FichothequePhrasesCommand(bdfServer, requestMap);
            case LangConfigCommand.COMMANDNAME:
                return new LangConfigCommand(bdfServer, requestMap);
            case SubsetTreeCommand.COMMANDNAME:
                return new SubsetTreeCommand(bdfServer, requestMap);
            case UserAllowChangeCommand.COMMANDNAME:
                return new UserAllowChangeCommand(bdfServer, requestMap);
            case FichothequeAttributeChangeCommand.COMMANDNAME:
                return new FichothequeAttributeChangeCommand(bdfServer, requestMap);
            case ExtensionActivationCommand.COMMANDNAME:
                return new ExtensionActivationCommand(bdfServer, requestMap);
            case ExternalScriptRunCommand.COMMANDNAME:
                return new ExternalScriptRunCommand(bdfServer, requestMap);
            case CustomIconUploadCommand.COMMANDNAME:
                return new CustomIconUploadCommand(bdfServer, requestMap);
            case OdtLogoChangeCommand.COMMANDNAME:
                return new OdtLogoChangeCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
