/* BdfServer_Commands - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.commands.AbstractPhrasesCommand;
import net.fichotheque.metadata.FichothequeMetadataEditor;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FichothequePhrasesCommand extends AbstractPhrasesCommand {

    public final static String COMMANDNAME = "FichothequePhrases";
    public final static String COMMANDKEY = "_ CNF-04";

    public FichothequePhrasesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done = false;
        try (EditSession session = startEditSession(Domains.CONFIGURATION, COMMANDNAME)) {
            FichothequeMetadataEditor fichothequeMetadataEditor = session.getFichothequeEditor().getFichothequeMetadataEditor();
            if (update(fichothequeMetadataEditor)) {
                done = true;
            }
        }
        if (done) {
            setDone("_ done.configuration.fichothequephrases");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        checkPhrasesParameters();
    }

}
