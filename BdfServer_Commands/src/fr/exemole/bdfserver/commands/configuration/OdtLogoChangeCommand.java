/* BdfServer_Commands - Copyright (c) 2024-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import net.fichotheque.EditOrigin;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.ini.IniWriter;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.mimetype.MimeTypeResolver;
import net.mapeadores.util.mimetype.MimeTypeUtils;
import net.mapeadores.util.request.FileValue;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class OdtLogoChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "OdtLogoChange";
    public final static String COMMANDKEY = "_ CNF-19";
    public final static String FILE_PARAMNAME = "file";
    public final static String HEIGHT_PARAMNAME = "height";
    public final static String WIDTH_PARAMNAME = "width";
    private String width;
    private String height;

    public OdtLogoChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.CONFIGURATION, COMMANDNAME);
        Map<String, String> logoMap = ConfigurationUtils.getOdtLogoParameters(bdfServer);
        logoMap.put("width", width);
        logoMap.put("height", height);
        FileValue fileValue = requestHandler.getRequestMap().getFileValue(FILE_PARAMNAME);
        if (fileValue != null) {
            if (fileValue.length() > 0) {
                String name = fileValue.getName();
                MimeTypeResolver mimeTypeResolver = bdfServer.getMimeTypeResolver();
                String mimeType = MimeTypeUtils.getMimeType(mimeTypeResolver, fileValue.getName());
                String extension = mimeTypeResolver.getPreferredExtension(mimeType);
                if (extension != null) {
                    String path = "images/logo-odt." + extension;
                    RelativePath relativePath = RelativePath.build(path);
                    logoMap.put("path", path);
                    try (InputStream is = fileValue.getInputStream()) {
                        StorageUtils.saveResource(bdfServer, relativePath, is, editOrigin);
                    } catch (IOException ioe) {
                        throw new NestedIOException(ioe);
                    }
                }
            }
            fileValue.free();
        }
        try (InputStream is = IOUtils.toInputStream(IniWriter.mapToString(logoMap), "UTF-8")) {
            StorageUtils.saveResource(bdfServer, StorageUtils.LOGO_ODT_INI, is, editOrigin);
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.streamtext_io", ioe.getMessage());
        }
        setDone("_ done.configuration.odtlogochange");

    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        width = getMandatory(WIDTH_PARAMNAME);
        height = getMandatory(HEIGHT_PARAMNAME);
    }

}
