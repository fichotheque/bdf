/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerEditor;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.groups.GroupDef;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class GroupCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "GroupCreation";
    public final static String COMMANDKEY = "_ CNF-10";
    public final static String NAME_PARAMNAME = "name";
    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    private String name;
    private LabelChange groupLabelChange;
    private AttributeChange attributeChange;

    public GroupCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.CONFIGURATION, COMMANDNAME)) {
            BdfServerEditor bdfServerEditor = session.getBdfServerEditor();
            GroupDef groupDef = bdfServerEditor.createGroupDef(name);
            BdfServerUtils.changeLabels(bdfServerEditor, groupDef, groupLabelChange);
            if (attributeChange != null) {
                BdfServerUtils.changeAttributes(bdfServerEditor, groupDef, attributeChange);
            }
        }
        setDone("_ done.configuration.groupcreation", name);

    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        name = getMandatory(NAME_PARAMNAME);
        if (!StringUtils.isTechnicalName(name, true)) {
            throw BdfErrors.error("_ error.wrong.groupname", name);
        }
        if (bdfServer.getGroupManager().getGroupDef(name) != null) {
            throw BdfErrors.error("_ error.existing.group", name);
        }
        groupLabelChange = requestHandler.getLabelChange(TITLE_PARAMPREFIX);
        String attributes = requestHandler.getTrimedParameter(ATTRIBUTES_PARAMNAME);
        if (!attributes.isEmpty()) {
            attributeChange = AttributeParser.parse(attributes);
        }
    }

}
