/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ExtensionActivationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ExtensionActivation";
    public final static String COMMANDKEY = "_ CNF-18";
    public final static String ACTIVATION_PARAMNAME = "activation";
    private List<String> activeExtensionList;

    public ExtensionActivationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.CONFIGURATION, COMMANDNAME)) {
            session.getBdfServerEditor().setActiveExtensionList(activeExtensionList);
        }
        setDone("_ done.configuration.extensionactivation");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        String[] values = requestHandler.getTokens(ACTIVATION_PARAMNAME);
        activeExtensionList = new ArrayList<String>();
        for (String value : values) {
            value = value.trim();
            if (value.length() > 0) {
                activeExtensionList.add(value);
            }
        }
    }

}
