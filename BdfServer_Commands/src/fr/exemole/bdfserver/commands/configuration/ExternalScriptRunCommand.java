/* BdfServer_Command - Copyright (c) 2019-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.externalscript.ExternalScript;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ExternalScriptRunCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ExternalScriptRunCommand";
    public final static String NAME_PARAMNAME = "name";
    private ExternalScript externalScript;

    public ExternalScriptRunCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try {
            externalScript.exec();
            setDone("_ done.configuration.externalscriptrun", externalScript.getName());
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.script_io", externalScript.getName(), ioe.getLocalizedMessage());
        }

    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        String name = getMandatory(NAME_PARAMNAME);
        this.externalScript = bdfServer.getExternalScriptManager().getExternalScript(name);
        if (externalScript == null) {
            throw BdfErrors.error("_ error.unknown.externalscript", name);
        }
    }

}
