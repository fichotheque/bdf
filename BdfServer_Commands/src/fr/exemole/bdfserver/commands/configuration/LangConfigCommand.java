/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.configuration.LangConfigurationBuilder;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class LangConfigCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "LangConfig";
    public final static String COMMANDKEY = "_ CNF-03";
    public final static String WORKINGLANG_PARAMNAME = "workinglang";
    public final static String SUPPLEMENTARYLANG_PARAMNAME = "supplementarylang";
    public final static String ALLLANG_PARAMNAME = "alllang";
    public final static String WITHNONLATIN_PARAMNAME = "withnonlatin";
    public final static String WITHOUTSURNAMEFIRST = "withoutsurnamefirst";
    private Lang[] workingArray;
    private Lang[] supplementaryArray;
    private boolean allLanguage;
    private boolean withNonlatin;
    private boolean withoutSurnameFirst;

    public LangConfigCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        LangConfigurationBuilder langConfigurationBuilder = LangConfigurationBuilder.init()
                .setWithNonlatin(withNonlatin)
                .setWithoutSurnameFirst(withoutSurnameFirst)
                .setAllLanguages(allLanguage);
        for (Lang lang : workingArray) {
            langConfigurationBuilder.addWorkingLang(lang);
        }
        if (!allLanguage) {
            for (Lang lang : supplementaryArray) {
                langConfigurationBuilder.addSupplementaryLang(lang);
            }
        }
        try (EditSession session = startEditSession(Domains.CONFIGURATION, COMMANDNAME)) {
            session.getBdfServerEditor().setLangConfiguration(langConfigurationBuilder.toLangConfiguration());
        }
        setDone("_ done.configuration.langconfig");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        String workingLangParam = getMandatory(WORKINGLANG_PARAMNAME);
        workingArray = LangsUtils.toCleanLangArray(workingLangParam);
        if (workingArray.length == 0) {
            throw BdfErrors.error("_ error.empty.workinglang");
        }
        allLanguage = requestHandler.isTrue(ALLLANG_PARAMNAME);
        if (!allLanguage) {
            String supplementaryLangParam = getMandatory(SUPPLEMENTARYLANG_PARAMNAME);
            supplementaryArray = LangsUtils.toCleanLangArray(supplementaryLangParam);
        }
        withNonlatin = requestHandler.isTrue(WITHNONLATIN_PARAMNAME);
        withoutSurnameFirst = requestHandler.isTrue(WITHOUTSURNAMEFIRST);
    }

}
