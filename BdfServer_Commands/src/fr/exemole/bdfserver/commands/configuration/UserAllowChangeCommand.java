/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.policies.UserAllow;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.policies.UserAllowBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class UserAllowChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "UserAllowChange";
    public final static String COMMANDKEY = "_ CNF-05";
    public final static String DATA_PARAMNAME = "data";
    public final static String PASSWORD_PARAMNAME = "password";
    private boolean allowData = false;
    private boolean allowPassword = false;

    public UserAllowChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        UserAllow userAllow = UserAllowBuilder.init()
                .setDataChangeAllowed(allowData)
                .setPasswordChangeAllowed(allowPassword)
                .toUserAllow();
        boolean done;
        try (EditSession session = startEditSession(Domains.CONFIGURATION, COMMANDNAME)) {
            done = session.getBdfServerEditor().setUserAllow(userAllow);
        }
        if (done) {
            setDone("_ done.configuration.userallowchange");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        allowData = requestHandler.isTrue(DATA_PARAMNAME);
        allowPassword = requestHandler.isTrue(PASSWORD_PARAMNAME);
    }

}
