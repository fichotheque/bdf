/* BdfServer_Commands - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.configuration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.groups.GroupDef;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class GroupRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "GroupRemove";
    public final static String COMMANDKEY = "_ CNF-12";
    public final static String NAME_PARAMNAME = "name";
    private GroupDef groupDef;

    public GroupRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        String name = groupDef.getName();
        try (EditSession session = startEditSession(Domains.CONFIGURATION, COMMANDNAME)) {
            session.getBdfServerEditor().removeGroupDef(groupDef);
        }
        setDone("_ done.configuration.groupremove", name);

    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        String name = getMandatory(NAME_PARAMNAME);
        groupDef = bdfServer.getGroupManager().getGroupDef(name);
        if (groupDef == null) {
            throw BdfErrors.unknownParameterValue(NAME_PARAMNAME, name);
        }
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
    }

}
