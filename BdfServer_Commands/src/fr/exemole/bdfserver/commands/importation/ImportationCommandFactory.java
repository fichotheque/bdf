/* BdfServer_Commands - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.importation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ImportationCommandFactory {

    private ImportationCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case LabelImportParseCommand.COMMANDNAME:
                return new LabelImportParseCommand(bdfServer, requestMap);
            case ThesaurusImportParseCommand.COMMANDNAME:
                return new ThesaurusImportParseCommand(bdfServer, requestMap);
            case CorpusImportParseCommand.COMMANDNAME:
                return new CorpusImportParseCommand(bdfServer, requestMap);
            case ImportationEngineCommand.COMMANDNAME:
                return new ImportationEngineCommand(bdfServer, requestMap);
            case HtmlConversionCommand.COMMANDNAME:
                return new HtmlConversionCommand(bdfServer, requestMap);
            case MetaReportCommand.COMMANDNAME:
                return new MetaReportCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
