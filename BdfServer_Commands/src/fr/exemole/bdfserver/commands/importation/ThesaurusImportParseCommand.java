/* BdfServer_Commands - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.importation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.importation.ImportationEngine;
import fr.exemole.bdfserver.tools.importation.ImportationFileUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.File;
import java.io.IOException;
import net.fichotheque.SubsetKey;
import net.fichotheque.importation.ThesaurusImport;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.parsers.Row;
import net.fichotheque.tools.importation.parsers.RowParser;
import net.fichotheque.tools.importation.thesaurus.ThesaurusImportBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusImportParseCommand extends AbstractImportParseCommand {

    public final static String COMMANDNAME = "ThesaurusImportParse";
    public final static String COMMANDKEY = "_ IMP-02";
    public final static String TYPE_PARAMNAME = "type";
    public final static String DESTINATIONTHESAURUS_PARAMNAME = "destinationthesaurus";
    private String type;
    private Thesaurus thesaurus;
    private Thesaurus destinationThesaurus;

    public ThesaurusImportParseCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        ThesaurusImportBuilder thesaurusImportBuilder = ThesaurusImportBuilder.init(thesaurus, type, destinationThesaurus);
        ParseResultBuilder parseResultBuilder = new ParseResultBuilder(ImportationEngine.THESAURUS_IMPORT);
        RowParser rowParser = RowParser.newThesaurusRowParser(fieldsArray, thesaurusImportBuilder, parseResultBuilder);
        if (rowParser != null) {
            for (int i = 0; i < lines.length; i++) {
                String[] line = lines[i];
                if (line.length != 0) {
                    rowParser.parseRow(i + 1, new Row(line));
                }
            }
            try {
                File tmpFile = ImportationFileUtils.saveTmpXml(bdfServer, thesaurusImportBuilder.toThesaurusImport());
                parseResultBuilder.setTmpFile(tmpFile);
            } catch (IOException ioe) {
                throw BdfErrors.ioException(ioe);
            }
        }
        putResultObject(PARSERESULT_OBJ, parseResultBuilder.toParseResult());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        requestHandler.enableStore();
        String typeParam = getMandatory(TYPE_PARAMNAME);
        try {
            typeParam = ThesaurusImport.checkType(typeParam);
        } catch (IllegalArgumentException iae) {
            throw BdfErrors.wrongParameterValue(TYPE_PARAMNAME, typeParam);
        }
        this.type = typeParam;
        this.thesaurus = requestHandler.getMandatoryThesaurus();
        switch (typeParam) {
            case ThesaurusImport.MOVE_TYPE:
            case ThesaurusImport.MERGE_TYPE:
                this.destinationThesaurus = (Thesaurus) requestHandler.getMandatorySubset(SubsetKey.CATEGORY_THESAURUS, DESTINATIONTHESAURUS_PARAMNAME);
        }
        checkLines();
        requestHandler.store("form_import_thesaurus");
    }

}
