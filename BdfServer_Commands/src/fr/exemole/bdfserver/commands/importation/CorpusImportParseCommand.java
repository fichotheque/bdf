/* BdfServer_Commands - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.importation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.importation.ImportationEngine;
import fr.exemole.bdfserver.tools.importation.ImportationFileUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.io.File;
import java.io.IOException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.importation.CorpusImport;
import net.fichotheque.tools.importation.ParseResultBuilder;
import net.fichotheque.tools.importation.corpus.CorpusImportBuilder;
import net.fichotheque.tools.importation.directory.DirectoryCorpusImport;
import net.fichotheque.tools.importation.parsers.Row;
import net.fichotheque.tools.importation.parsers.RowParser;
import net.fichotheque.tools.parsers.FicheParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CorpusImportParseCommand extends AbstractImportParseCommand {

    public final static String COMMANDNAME = "CorpusImportParse";
    public final static String COMMANDKEY = "_ IMP-01";
    public final static String TYPE_PARAMNAME = "type";
    private String type;
    private Corpus corpus;


    public CorpusImportParseCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        FicheParser.Parameters ficheParserParameters = new FicheParser.Parameters(bdfUser, bdfServer.getL10nManager().getCodeCatalog(), getContentChecker(), BdfUserUtils.getTypoOptions(bdfUser));
        ficheParserParameters.setUserRedacteur(bdfUser.getRedacteur());
        CorpusImportBuilder corpusImportBuilder = CorpusImportBuilder.init(getContentChecker(), corpus, type);
        ParseResultBuilder parseResultBuilder = new ParseResultBuilder(ImportationEngine.CORPUS_IMPORT);
        RowParser rowParser = RowParser.newCorpusRowParser(fieldsArray, corpusImportBuilder, parseResultBuilder, ficheParserParameters, bdfServer.getPolicyManager().getPolicyProvider(), bdfServer.getThesaurusLangChecker());
        if (rowParser != null) {
            for (int i = 0; i < lines.length; i++) {
                String[] line = lines[i];
                if (line.length != 0) {
                    rowParser.parseRow(i + 1, new Row(line));
                }
            }
            try {
                File tmpDir = ImportationFileUtils.createTmpDir(bdfServer, "corpusimport-" + type);
                DirectoryCorpusImport.save(getContentChecker(), corpusImportBuilder.toCorpusImport(), tmpDir);
                parseResultBuilder.setTmpDir(tmpDir);
            } catch (IOException ioe) {
                throw BdfErrors.ioException(ioe);
            }
        }
        putResultObject(PARSERESULT_OBJ, parseResultBuilder.toParseResult());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        requestHandler.enableStore();
        String typeParam = getMandatory(TYPE_PARAMNAME);
        try {
            typeParam = CorpusImport.checkType(typeParam);
        } catch (IllegalArgumentException iae) {
            throw BdfErrors.wrongParameterValue(TYPE_PARAMNAME, typeParam);
        }
        this.type = typeParam;
        this.corpus = requestHandler.getMandatoryCorpus();
        checkLines();
        requestHandler.store("form_import_corpus");
    }


}
