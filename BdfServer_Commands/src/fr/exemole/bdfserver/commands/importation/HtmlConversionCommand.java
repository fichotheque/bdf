/* BdfServer_Commands - Copyright (c) 2019-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.importation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.tools.from.html.ConversionEngine;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class HtmlConversionCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "HtmlConversion";
    public final static String SOURCE_PARAMNAME = "source";
    private String source;

    public HtmlConversionCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        FicheBlocks ficheBlocks = ConversionEngine.convertHtmlFragment(source);
        putResultObject(FICHEBLOCKS_OBJ, ficheBlocks);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        this.source = getMandatory(SOURCE_PARAMNAME);
    }

}
