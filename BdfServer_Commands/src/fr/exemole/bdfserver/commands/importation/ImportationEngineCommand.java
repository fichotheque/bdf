/* BdfServer_Commands - Copyright (c) 2014-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.importation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.importation.ImportationEngine;
import fr.exemole.bdfserver.tools.importation.ImportationFileUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.File;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ImportationEngineCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ImportationEngine";
    public final static String TYPE_PARAMNAME = "type";
    public final static String TMPFILE_PARAMNAME = "tmpfile";
    public final static String TMPDIR_PARAMNAME = "tmpdir";
    private File tmpFile;
    private String engineType;

    public ImportationEngineCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.IMPORTATION, COMMANDNAME + "/" + engineType)) {
            ImportationEngine.run(session, this, engineType, tmpFile);
        }
        setDone("_ done.importation.engine");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        this.engineType = getMandatory(TYPE_PARAMNAME);
        if (!ImportationEngine.isValidEngineType(engineType)) {
            throw BdfErrors.wrongParameterValue(TYPE_PARAMNAME, engineType);
        }
        String tmpFileName = requestHandler.getTrimedParameter(TMPFILE_PARAMNAME);
        if (!tmpFileName.isEmpty()) {
            tmpFile = ImportationFileUtils.getTmpFile(bdfServer, tmpFileName);
            if (tmpFile == null) {
                throw BdfErrors.unknownParameterValue(TMPFILE_PARAMNAME, tmpFileName);
            }
        } else {
            String tmpDirName = getMandatory(TMPDIR_PARAMNAME);
            tmpFile = ImportationFileUtils.getTmpFile(bdfServer, tmpDirName);
            if (tmpFile == null) {
                throw BdfErrors.unknownParameterValue(TMPDIR_PARAMNAME, tmpDirName);
            }
        }
    }

}
