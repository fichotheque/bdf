/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.edition;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.EditionEngine;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.ExistingIdException;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.NoMasterIdException;
import net.fichotheque.utils.CorpusMetadataUtils;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FicheCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "FicheCreation";
    private Corpus corpus;

    public FicheCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        FicheMeta ficheMeta;
        try (EditSession session = startEditSession(Domains.EDITION, COMMANDNAME)) {
            CorpusEditor corpusEditor = session.getFichothequeEditor().getCorpusEditor(corpus);
            try {
                ficheMeta = corpusEditor.createFiche(-1);
            } catch (ExistingIdException eii) {
                throw new ImplementationException(eii);
            } catch (NoMasterIdException nmie) {
                throw new ShouldNotOccurException(nmie);
            }
            corpusEditor.setDate(ficheMeta, FuzzyDate.current(), false);
            EditionEngine.replace(session, this, requestHandler.getRequestMap(), ficheMeta);
        }
        putResultObject(CORPUS_OBJ, corpus);
        putResultObject(FICHEMETA_OBJ, ficheMeta);
        setDone("_ done.edition.fichecreation", CorpusMetadataUtils.getFicheTitle(ficheMeta, bdfUser.getWorkingLang(), bdfUser.getFormatLocale()));
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        if (corpus.getMasterSubset() != null) {
            throw BdfErrors.internalError("corpus.getMasterSubset() != null");
        }
        getPermissionChecker()
                .checkFicheCreate(corpus);
    }

}
