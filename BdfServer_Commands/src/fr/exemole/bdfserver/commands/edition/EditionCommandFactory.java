/* BdfServer_Commands - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.edition;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class EditionCommandFactory {

    private EditionCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case FicheCreationCommand.COMMANDNAME:
                return new FicheCreationCommand(bdfServer, requestMap);
            case FicheChangeCommand.COMMANDNAME:
                return new FicheChangeCommand(bdfServer, requestMap);
            case FicheIndexationChangeCommand.COMMANDNAME:
                return new FicheIndexationChangeCommand(bdfServer, requestMap);
            case FicheAddendaChangeCommand.COMMANDNAME:
                return new FicheAddendaChangeCommand(bdfServer, requestMap);
            case FicheAlbumChangeCommand.COMMANDNAME:
                return new FicheAlbumChangeCommand(bdfServer, requestMap);
            case SectionParserCommand.COMMANDNAME:
                return new SectionParserCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
