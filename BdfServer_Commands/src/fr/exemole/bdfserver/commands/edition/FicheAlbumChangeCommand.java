/* BdfServer_Commands - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.edition;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import static fr.exemole.bdfserver.api.instruction.BdfInstructionConstants.CORPUS_OBJ;
import static fr.exemole.bdfserver.api.instruction.BdfInstructionConstants.FICHEMETA_OBJ;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.EditionEngine;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.corpus.FicheMeta;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FicheAlbumChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "FicheAlbumChange";
    private FicheMeta ficheMeta;

    public FicheAlbumChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.EDITION, COMMANDNAME)) {
            EditionEngine.croisementOnly(session, this, requestHandler.getRequestMap(), ficheMeta);
        }
        putResultObject(CORPUS_OBJ, ficheMeta.getCorpus());
        putResultObject(FICHEMETA_OBJ, ficheMeta);
        setDone("_ done.edition.fichealbumupdate");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        ficheMeta = requestHandler.getMandatoryFicheMeta();
        getPermissionChecker()
                .checkWrite(ficheMeta);
    }

}
