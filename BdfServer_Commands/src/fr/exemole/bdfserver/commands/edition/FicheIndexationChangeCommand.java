/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.edition;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.EditionEngine;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.corpus.FicheMeta;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FicheIndexationChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "FicheIndexationChange";
    private FicheMeta ficheMeta;

    public FicheIndexationChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.EDITION, COMMANDNAME)) {
            EditionEngine.croisementOnly(session, this, requestHandler.getRequestMap(), ficheMeta);
        }
        putResultObject(CORPUS_OBJ, ficheMeta.getCorpus());
        putResultObject(FICHEMETA_OBJ, ficheMeta);
        setDone("_ done.edition.ficheindexationchange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        ficheMeta = requestHandler.getMandatoryFicheMeta();
        checkSubsetAdmin(ficheMeta.getCorpus());
    }

}
