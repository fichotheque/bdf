/* BdfServer_Commands - Copyright (c) 2022-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.edition;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.fiche.FicheBlock;
import net.fichotheque.corpus.fiche.FicheBlocks;
import net.fichotheque.tools.parsers.ficheblock.FicheBlockParser;
import net.fichotheque.utils.FicheUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
public class SectionParserCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "SectionParser";
    public final static String TEXT_PARAMNAME = "text";
    private Corpus corpus;
    private String text;

    public SectionParserCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        FicheBlocks ficheBlocks = toFicheBlocks();
        putResultObject(CORPUS_OBJ, corpus);
        putResultObject(FICHEBLOCKS_OBJ, ficheBlocks);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        corpus = requestHandler.getMandatoryCorpus();
        text = getMandatory(TEXT_PARAMNAME);
    }

    private FicheBlocks toFicheBlocks() {
        if (text.isEmpty()) {
            return FicheUtils.EMPTY_FICHEBLOCKS;
        }
        TypoOptions typoOptions = BdfUserUtils.getTypoOptions(bdfUser);
        FicheBlockParser ficheBlockParser = new FicheBlockParser(getContentChecker(), typoOptions, true);
        List<FicheBlock> list = new ArrayList<FicheBlock>();
        ficheBlockParser.parseFicheBlockList(text, list);
        return FicheUtils.toFicheBlocks(list);
    }

}
