/* BdfServer_Commands - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.commands.AbstractPhrasesCommand;
import net.fichotheque.album.Album;
import net.fichotheque.album.metadata.AlbumMetadataEditor;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AlbumPhrasesCommand extends AbstractPhrasesCommand {

    public final static String COMMANDNAME = "AlbumIntitule";
    public final static String COMMANDKEY = "_ ALB-02";
    private Album album;

    public AlbumPhrasesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done = false;
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            AlbumMetadataEditor albumMetadataEditor = session.getFichothequeEditor().getAlbumEditor(album).getAlbumMetadataEditor();
            if (update(albumMetadataEditor)) {
                done = true;
            }
        }
        putResultObject(ALBUM_OBJ, album);
        if (done) {
            setDone("_ done.album.albumphrases");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        album = requestHandler.getMandatoryAlbum();
        checkSubsetAdmin(album);
        checkPhrasesParameters();
    }

}
