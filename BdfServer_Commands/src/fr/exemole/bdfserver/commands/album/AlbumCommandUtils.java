/* BdfServer_Commands - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import java.io.File;
import java.io.IOException;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumConstants;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.awt.ResizeInfo;
import net.mapeadores.util.images.ImageResizing;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public class AlbumCommandUtils {

    public static final String ORIGINAL_TMPFILENAME = "_ORIGINAL";

    private AlbumCommandUtils() {
    }

    public static void minimizeTmpFile(File tmpFile, Album album) throws IOException, ErrorMessageException {
        ResizeInfo resizeInfo = album.getAlbumMetadata().getResizeInfo(AlbumConstants.MINI_SPECIALDIM);
        String fileName = tmpFile.getName();
        String extension = AlbumUtils.getExtension(fileName);
        File miniFile = new File(tmpFile.getParentFile(), "mini-" + fileName);
        ImageResizing.resize(tmpFile, miniFile, resizeInfo, extension);
    }

    public static File getTmpFile(BdfServer bdfServer, String extension) {
        synchronized (bdfServer) {
            File tmpDirectory = ConfigurationUtils.getTmpDirectory(bdfServer, true);
            String number = Long.toHexString(System.currentTimeMillis());
            File f = new File(tmpDirectory, "img-" + number + "." + extension);
            if (!f.exists()) {
                return f;
            } else {
                int p = 1;
                while (true) {
                    f = new File(tmpDirectory, "img-" + number + "_" + p + "." + extension);
                    if (!f.exists()) {
                        return f;
                    } else {
                        p++;
                    }
                }
            }
        }
    }

}
