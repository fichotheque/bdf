/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.fichotheque.album.Illustration;
import net.fichotheque.croisement.CroisementChanges;
import net.fichotheque.tools.croisement.CroisementChangeEngine;
import net.fichotheque.tools.croisement.LienBuffer;
import net.fichotheque.tools.parsers.croisement.LienBufferParser;
import net.fichotheque.tools.permission.PermissionPredicate;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class CroisementAddCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "CroisementAdd";
    public final static String COMMANDKEY = "_ ALB-14";
    public final static String ADD_PARAMNAME = "add";
    private Illustration illustration;
    private CroisementChanges croisementChanges;

    public CroisementAddCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            session.getFichothequeEditor().getCroisementEditor().updateCroisements(illustration, croisementChanges);
        }
        putResultObject(ALBUM_OBJ, illustration.getAlbum());
        putResultObject(ILLUSTRATION_OBJ, illustration);
        setDone("_ done.album.croisementadd");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        illustration = requestHandler.getMandatoryIllustration();
        getPermissionChecker()
                .checkWrite(illustration);
        PermissionPredicate predicate = PermissionPredicate.read(getPermissionSummary());
        String addString = getMandatory(ADD_PARAMNAME);
        String[] tokens = StringUtils.getTechnicalTokens(addString, false);
        CroisementChangeEngine croisementChangeEngine = CroisementChangeEngine.appendEngine(illustration);
        boolean done = false;
        for (String token : tokens) {
            try {
                LienBuffer lienBuffer = LienBufferParser.parse(fichotheque, token, SubsetKey.CATEGORY_CORPUS);
                if (predicate.test(lienBuffer.getSubsetItem())) {
                    croisementChangeEngine.addLien(lienBuffer);
                    done = true;
                }
            } catch (ParseException e) {

            }
        }
        if (!done) {
            throw BdfErrors.error("_ error.wrong.fichekeys");
        }
        croisementChanges = croisementChangeEngine.toCroisementChanges();
    }

}
