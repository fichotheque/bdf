/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.NoRemoveableSubsetException;
import net.fichotheque.SubsetKey;
import net.fichotheque.album.Album;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AlbumRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "AlbumRemove";
    public final static String COMMANDKEY = "_ ALB-10";
    private Album album;

    public AlbumRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        SubsetKey albumKey = album.getSubsetKey();
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            try {
                session.getFichothequeEditor().removeAlbum(album);
            } catch (NoRemoveableSubsetException nrse) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        putResultObject(SUBSETKEY_OBJ, albumKey);
        setDone("_ done.album.albumremove", albumKey.getSubsetName());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        album = requestHandler.getMandatoryAlbum();
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        if (!album.isRemoveable()) {
            throw BdfErrors.error("_ error.notremoveable.album");
        }
    }

}
