/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.album.Album;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.album.metadata.AlbumMetadataEditor;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AlbumDimChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "AlbumDimChange";
    public final static String COMMANDKEY = "_ ALB-04";
    public final static String ALBUMDIM_WIDTH_PARAMNAME = "albumdimwidth";
    public final static String ALBUMDIM_HEIGHT_PARAMNAME = "albumdimheight";
    private AlbumDim albumDim;
    private int width = 0;
    private int height = 0;

    public AlbumDimChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Album album = albumDim.getAlbumMetadata().getAlbum();
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            AlbumMetadataEditor albumMetadataEditor = session.getFichothequeEditor().getAlbumEditor(album).getAlbumMetadataEditor();
            albumMetadataEditor.setDim(albumDim, width, height);
        }
        putResultObject(ALBUM_OBJ, album);
        putResultObject(ALBUMDIM_OBJ, albumDim);
        setDone("_ done.album.albumdimchange", albumDim.getName());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        albumDim = requestHandler.getMandatoryAlbumDim();
        checkSubsetAdmin(albumDim.getAlbumMetadata().getAlbum());
        String dimType = albumDim.getDimType();
        if (AlbumUtils.needWidth(dimType)) {
            String widthString = getMandatory(ALBUMDIM_WIDTH_PARAMNAME).trim();
            if (widthString.isEmpty()) {
                throw BdfErrors.error("_ error.empty.width");
            }
            try {
                width = Integer.parseInt(widthString);
                if (width < 1) {
                    throw BdfErrors.error("_ error.wrong.width", widthString);
                }
            } catch (NumberFormatException nfe) {
                throw BdfErrors.error("_ error.wrong.width", widthString);
            }
        }
        if (AlbumUtils.needHeight(dimType)) {
            String heightString = getMandatory(ALBUMDIM_HEIGHT_PARAMNAME).trim();
            if (heightString.isEmpty()) {
                throw BdfErrors.error("_ error.empty.height");
            }
            try {
                height = Integer.parseInt(heightString);
                if (height < 1) {
                    throw BdfErrors.error("_ error.wrong.height", heightString);
                }
            } catch (NumberFormatException nfe) {
                throw BdfErrors.error("_ error.wrong.height", heightString);
            }
        }
    }

}
