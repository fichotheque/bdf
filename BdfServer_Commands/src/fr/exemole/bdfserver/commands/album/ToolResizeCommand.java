/* BdfServer_Commands - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumConstants;
import net.fichotheque.album.Illustration;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.awt.ResizeInfo;
import net.mapeadores.util.images.ImageResizing;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ToolResizeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ToolResize";
    public final static String TMPFILE_PARAMNAME = "tmpfile";
    public final static String WIDTH_PARAMNAME = "width";
    public final static String HEIGHT_PARAMNAME = "height";
    private Album album;
    private File tmpFile;
    private Illustration illustration;
    private int width, height;
    private String extension;

    public ToolResizeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        File newTmpFile = AlbumCommandUtils.getTmpFile(bdfServer, extension);
        ResizeInfo resizeInfo = ResizeInfo.newFixedDimInstance(width, height);
        try (InputStream is = getInputStream()) {
            ImageResizing.resize(is, newTmpFile, resizeInfo, extension);
            AlbumCommandUtils.minimizeTmpFile(newTmpFile, album);
        } catch (IOException ioe) {
            throw BdfErrors.ioException(ioe);
        }
        putResultObject(TMPFILE_OBJ, newTmpFile);
        putResultObject(ALBUM_OBJ, album);
        setDone("_ done.album.toolresize", newTmpFile.getName());
    }

    private InputStream getInputStream() throws IOException {
        if (tmpFile != null) {
            return new FileInputStream(tmpFile);
        } else {
            return illustration.getInputStream(AlbumConstants.ORIGINAL_SPECIALDIM);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        album = requestHandler.getMandatoryAlbum();
        width = requestHandler.getMandatoryIntegerParameter(WIDTH_PARAMNAME);
        height = requestHandler.getMandatoryIntegerParameter(HEIGHT_PARAMNAME);
        String tmpFileName = requestHandler.getTrimedParameter(TMPFILE_PARAMNAME);
        if ((!tmpFileName.isEmpty()) && (!tmpFileName.equals(AlbumCommandUtils.ORIGINAL_TMPFILENAME))) {
            File tmpDirectory = ConfigurationUtils.getTmpDirectory(bdfServer);
            tmpFile = new File(tmpDirectory, tmpFileName);
            if (!tmpFile.exists()) {
                throw BdfErrors.error("_ error.unknown.tmpfile", tmpFileName);
            }
            extension = AlbumUtils.getExtension(tmpFile);
        } else {
            illustration = (Illustration) requestHandler.getMandatorySubsetItem(album);
            extension = illustration.getFormatTypeString();
        }
    }

}
