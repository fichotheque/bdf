/* BdfServer_Commands - Copyright (c) 2011-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumConstants;
import net.fichotheque.album.Illustration;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.images.ImagesUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ToolCropCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ToolCrop";
    public final static String TMPFILE_PARAMNAME = "tmpfile";
    public final static String X_PARAMNAME = "x";
    public final static String Y_PARAMNAME = "y";
    public final static String WIDTH_PARAMNAME = "width";
    public final static String HEIGHT_PARAMNAME = "height";
    private Album album;
    private File tmpFile;
    private Illustration illustration;
    private int x, y, width, height;
    private String extension;

    public ToolCropCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        File newTmpFile = AlbumCommandUtils.getTmpFile(bdfServer, extension);
        try (InputStream is = getInputStream(); OutputStream os = new FileOutputStream(newTmpFile)) {
            ImagesUtils.cropImage(is, extension, os, x, y, width, height);
            AlbumCommandUtils.minimizeTmpFile(newTmpFile, album);
        } catch (IOException ioe) {
            throw BdfErrors.ioException(ioe);
        }
        putResultObject(TMPFILE_OBJ, newTmpFile);
        putResultObject(ALBUM_OBJ, album);
        setDone("_ done.album.toolcrop", newTmpFile.getName());
    }

    private InputStream getInputStream() throws IOException {
        if (tmpFile != null) {
            return new FileInputStream(tmpFile);
        } else {
            return illustration.getInputStream(AlbumConstants.ORIGINAL_SPECIALDIM);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        album = requestHandler.getMandatoryAlbum();
        x = requestHandler.getMandatoryIntegerParameter(X_PARAMNAME);
        y = requestHandler.getMandatoryIntegerParameter(Y_PARAMNAME);
        width = requestHandler.getMandatoryIntegerParameter(WIDTH_PARAMNAME);
        height = requestHandler.getMandatoryIntegerParameter(HEIGHT_PARAMNAME);
        String tmpFileName = requestHandler.getTrimedParameter(TMPFILE_PARAMNAME);
        if ((!tmpFileName.isEmpty()) && (!tmpFileName.equals(AlbumCommandUtils.ORIGINAL_TMPFILENAME))) {
            File tmpDirectory = ConfigurationUtils.getTmpDirectory(bdfServer);
            tmpFile = new File(tmpDirectory, tmpFileName);
            if (!tmpFile.exists()) {
                throw BdfErrors.error("_ error.unknown.tmpfile", tmpFileName);
            }
            extension = AlbumUtils.getExtension(tmpFile);
        } else {
            illustration = (Illustration) requestHandler.getMandatorySubsetItem(album);
            extension = illustration.getFormatTypeString();
        }
    }

}
