/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.album.Illustration;
import net.fichotheque.croisement.CroisementChanges;
import net.fichotheque.tools.croisement.CroisementRemoveParser;
import net.fichotheque.tools.permission.PermissionPredicate;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CroisementRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "CroisementRemove";
    public final static String COMMANDKEY = "_ ALB-13";
    public final static String REMOVE_PARAMNAME = "remove";
    private Illustration illustration;
    private CroisementChanges croisementChanges;

    public CroisementRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            FichothequeEditor fichothequeEditor = session.getFichothequeEditor();
            fichothequeEditor.getCroisementEditor().updateCroisements(illustration, croisementChanges);
        }
        putResultObject(ALBUM_OBJ, illustration.getAlbum());
        putResultObject(ILLUSTRATION_OBJ, illustration);
        setDone("_ done.album.croisementremove");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        illustration = requestHandler.getMandatoryIllustration();
        getPermissionChecker()
                .checkWrite(illustration);
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        String[] values = requestHandler.getTokens(REMOVE_PARAMNAME);
        if (values.length == 0) {
            throw BdfErrors.error("_ error.empty.ficheselection");
        }
        croisementChanges = CroisementRemoveParser.parseRemove(values, fichotheque, PermissionPredicate.read(getPermissionSummary()));
    }

}
