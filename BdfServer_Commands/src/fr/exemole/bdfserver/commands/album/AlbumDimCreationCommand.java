/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.ExistingNameException;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumEditor;
import net.fichotheque.album.metadata.AlbumDim;
import net.fichotheque.utils.AlbumUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AlbumDimCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "AlbumDimCreation";
    public final static String COMMANDKEY = "_ ALB-03";
    public final static String NEWALBUMDIM_PARAMNAME = "newalbumdim";
    public final static String ALBUMDIMTYPE_PARAMNAME = "albumdimtype";
    private Album album;
    private String newAlbumDimName;
    private String albumDimType;

    public AlbumDimCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        AlbumDim albumDim;
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            AlbumEditor albumEditor = session.getFichothequeEditor().getAlbumEditor(album);
            try {
                albumDim = albumEditor.getAlbumMetadataEditor().createAlbumDim(newAlbumDimName, albumDimType);
            } catch (ParseException | ExistingNameException e) {
                throw new ShouldNotOccurException("test done before", e);
            }
        }
        putResultObject(ALBUM_OBJ, album);
        putResultObject(ALBUMDIM_OBJ, albumDim);
        setDone("_ done.album.albumdimcreation", newAlbumDimName);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        album = requestHandler.getMandatoryAlbum();
        checkSubsetAdmin(album);
        newAlbumDimName = getMandatory(NEWALBUMDIM_PARAMNAME).trim();
        if (newAlbumDimName.isEmpty()) {
            throw BdfErrors.error("_ error.empty.albumdimname");
        }
        if (album.getAlbumMetadata().getAlbumDimByName(newAlbumDimName) != null) {
            throw BdfErrors.error("_ error.existing.albumdim", newAlbumDimName);
        }
        if (!AlbumUtils.isValidAlbumDimName(newAlbumDimName)) {
            throw BdfErrors.error("_ error.wrong.albumdimname", newAlbumDimName);
        }
        String albumDimTypeValue = getMandatory(ALBUMDIMTYPE_PARAMNAME);
        try {
            albumDimType = AlbumUtils.checkDimType(albumDimTypeValue);
        } catch (IllegalArgumentException iae) {
            throw BdfErrors.unknownParameterValue(ALBUMDIMTYPE_PARAMNAME, albumDimTypeValue);
        }
    }

}
