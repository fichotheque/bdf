/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.album.Album;
import net.fichotheque.album.AlbumEditor;
import net.fichotheque.album.metadata.AlbumDim;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AlbumDimRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "AlbumDimRemove";
    public final static String COMMANDKEY = "_ ALB-05";
    private AlbumDim albumDim;

    public AlbumDimRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        String name = albumDim.getName();
        Album album = albumDim.getAlbumMetadata().getAlbum();
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            AlbumEditor albumEditor = session.getFichothequeEditor().getAlbumEditor(album);
            albumEditor.getAlbumMetadataEditor().removeAlbumDim(albumDim);
        }
        setDone("_ done.album.albumdimremove", name);
        putResultObject(ALBUM_OBJ, album);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        albumDim = requestHandler.getMandatoryAlbumDim();
        checkSubsetAdmin(albumDim.getAlbumMetadata().getAlbum());
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
    }

}
