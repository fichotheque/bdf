/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.album.Illustration;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationAttributeChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "IllustrationAttributeChange";
    public final static String COMMANDKEY = "_ ALB-12";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    private Illustration illustration;
    private AttributeChange attributeChange;

    public IllustrationAttributeChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            session.getFichothequeEditor().changeAttributes(illustration, attributeChange);
        }
        putResultObject(ALBUM_OBJ, illustration.getAlbum());
        putResultObject(ILLUSTRATION_OBJ, illustration);
        setDone("_ done.global.attributechange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        illustration = requestHandler.getMandatoryIllustration();
        checkSubsetAdmin(illustration.getAlbum());
        String attributes = getMandatory(ATTRIBUTES_PARAMNAME);
        this.attributeChange = AttributeParser.parse(attributes);
    }

}
