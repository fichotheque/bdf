/* BdfServer_Commands - Copyright (c) 2010-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AlbumCommandFactory {

    private AlbumCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case AlbumCreationCommand.COMMANDNAME:
                return new AlbumCreationCommand(bdfServer, requestMap);
            case AlbumDimCreationCommand.COMMANDNAME:
                return new AlbumDimCreationCommand(bdfServer, requestMap);
            case AlbumDimChangeCommand.COMMANDNAME:
                return new AlbumDimChangeCommand(bdfServer, requestMap);
            case AlbumDimRemoveCommand.COMMANDNAME:
                return new AlbumDimRemoveCommand(bdfServer, requestMap);
            case IllustrationFileUploadCommand.COMMANDNAME:
                return new IllustrationFileUploadCommand(bdfServer, requestMap);
            case IllustrationFileDownloadCommand.COMMANDNAME:
                return new IllustrationFileDownloadCommand(bdfServer, requestMap);
            case ToolCropCommand.COMMANDNAME:
                return new ToolCropCommand(bdfServer, requestMap);
            case ToolResizeCommand.COMMANDNAME:
                return new ToolResizeCommand(bdfServer, requestMap);
            case AlbumAttributeChangeCommand.COMMANDNAME:
                return new AlbumAttributeChangeCommand(bdfServer, requestMap);
            case AlbumPhrasesCommand.COMMANDNAME:
                return new AlbumPhrasesCommand(bdfServer, requestMap);
            case AlbumRemoveCommand.COMMANDNAME:
                return new AlbumRemoveCommand(bdfServer, requestMap);
            case IllustrationAttributeChangeCommand.COMMANDNAME:
                return new IllustrationAttributeChangeCommand(bdfServer, requestMap);
            case IllustrationRemoveCommand.COMMANDNAME:
                return new IllustrationRemoveCommand(bdfServer, requestMap);
            case CroisementRemoveCommand.COMMANDNAME:
                return new CroisementRemoveCommand(bdfServer, requestMap);
            case CroisementAddCommand.COMMANDNAME:
                return new CroisementAddCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
