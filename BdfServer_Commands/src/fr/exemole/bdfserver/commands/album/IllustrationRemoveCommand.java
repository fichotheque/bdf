/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.album;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.FichothequeEditor;
import net.fichotheque.album.Album;
import net.fichotheque.album.Illustration;
import net.fichotheque.tools.FichothequeTools;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class IllustrationRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "IllustrationRemove";
    public final static String COMMANDKEY = "_ ALB-11";
    private Illustration illustration;

    public IllustrationRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Album album = illustration.getAlbum();
        try (EditSession session = startEditSession(Domains.ALBUM, COMMANDNAME)) {
            FichothequeEditor fichothequeEditor = session.getFichothequeEditor();
            FichothequeTools.remove(fichothequeEditor, illustration);
        }
        putResultObject(ALBUM_OBJ, album);
        setDone("_ done.album.illustrationremove");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        illustration = requestHandler.getMandatoryIllustration();
        getPermissionChecker()
                .checkWrite(illustration);
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
    }

}
