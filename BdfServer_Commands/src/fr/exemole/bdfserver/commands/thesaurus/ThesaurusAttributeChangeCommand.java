/* BdfServer_Commands - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusAttributeChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ThesaurusAttributeChange";
    public final static String COMMANDKEY = "_ THS-19";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    private Thesaurus thesaurus;
    private AttributeChange attributeChange;

    public ThesaurusAttributeChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            session.getFichothequeEditor().changeAttributes(thesaurus.getThesaurusMetadata(), attributeChange);
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        setDone("_ done.global.attributechange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        thesaurus = requestHandler.getMandatoryThesaurus();
        checkSubsetAdmin(thesaurus);
        String attributes = getMandatory(ATTRIBUTES_PARAMNAME);
        this.attributeChange = AttributeParser.parse(attributes);
    }

}
