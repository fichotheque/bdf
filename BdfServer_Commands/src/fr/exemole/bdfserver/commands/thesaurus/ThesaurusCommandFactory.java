/* BdfServer_HtmlProducers - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusCommandFactory {

    private ThesaurusCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case ThesaurusCreationCommand.COMMANDNAME:
                return new ThesaurusCreationCommand(bdfServer, requestMap);
            case ThesaurusPhrasesCommand.COMMANDNAME:
                return new ThesaurusPhrasesCommand(bdfServer, requestMap);
            case ThesaurusLangListCommand.COMMANDNAME:
                return new ThesaurusLangListCommand(bdfServer, requestMap);
            case ThesaurusRemoveCommand.COMMANDNAME:
                return new ThesaurusRemoveCommand(bdfServer, requestMap);
            case ThesaurusPolicyCommand.COMMANDNAME:
                return new ThesaurusPolicyCommand(bdfServer, requestMap);
            case ThesaurusCleaningCommand.COMMANDNAME:
                return new ThesaurusCleaningCommand(bdfServer, requestMap);
            case ThesaurusCloneCommand.COMMANDNAME:
                return new ThesaurusCloneCommand(bdfServer, requestMap);
            case IdalphaSortCommand.COMMANDNAME:
                return new IdalphaSortCommand(bdfServer, requestMap);
            case MotcleCreationCommand.COMMANDNAME:
                return new MotcleCreationCommand(bdfServer, requestMap);
            case IdalphaChangeCommand.COMMANDNAME:
                return new IdalphaChangeCommand(bdfServer, requestMap);
            case ParentChangeCommand.COMMANDNAME:
                return new ParentChangeCommand(bdfServer, requestMap);
            case LabelChangeCommand.COMMANDNAME:
                return new LabelChangeCommand(bdfServer, requestMap);
            case MotcleRemoveCommand.COMMANDNAME:
                return new MotcleRemoveCommand(bdfServer, requestMap);
            case MotcleIndexationCommand.COMMANDNAME:
                return new MotcleIndexationCommand(bdfServer, requestMap);
            case MotclePonderationCommand.COMMANDNAME:
                return new MotclePonderationCommand(bdfServer, requestMap);
            case MotcleMoveCommand.COMMANDNAME:
                return new MotcleMoveCommand(bdfServer, requestMap);
            case MotcleMergeCommand.COMMANDNAME:
                return new MotcleMergeCommand(bdfServer, requestMap);
            case MotcleStatusCommand.COMMANDNAME:
                return new MotcleStatusCommand(bdfServer, requestMap);
            case ThesaurusAttributeChangeCommand.COMMANDNAME:
                return new ThesaurusAttributeChangeCommand(bdfServer, requestMap);
            case MotcleAttributeChangeCommand.COMMANDNAME:
                return new MotcleAttributeChangeCommand(bdfServer, requestMap);
            case ChildrenReorderCommand.COMMANDNAME:
                return new ChildrenReorderCommand(bdfServer, requestMap);
            case MotcleTestCommand.COMMANDNAME:
                return new MotcleTestCommand(bdfServer, requestMap);
            case SelectionIndexationCommand.COMMANDNAME:
                return new SelectionIndexationCommand(bdfServer, requestMap);
            case MotcleCheckCommand.COMMANDNAME:
                return new MotcleCheckCommand(bdfServer, requestMap);
            case ThesaurusSyncCommand.COMMANDNAME:
                return new ThesaurusSyncCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
