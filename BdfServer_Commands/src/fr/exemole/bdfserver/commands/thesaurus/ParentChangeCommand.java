/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.ParentRecursivityException;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ParentChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ParentChange";
    public final static String COMMANDKEY = "_ THS-10";
    public final static String NEWPARENT_PARAMNAME = "newparent";
    private Motcle motcle;
    private Motcle parentMotcle;

    public ParentChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Thesaurus thesaurus = motcle.getThesaurus();
        boolean done = false;
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            ThesaurusEditor thesaurusEditor = session.getFichothequeEditor().getThesaurusEditor(thesaurus);
            try {
                done = thesaurusEditor.setParent(motcle, parentMotcle);
            } catch (ParentRecursivityException pre) {
            }
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        putResultObject(MOTCLE_OBJ, motcle);
        if (done) {
            setDone("_ done.thesaurus.parentchange");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        motcle = requestHandler.getMandatoryMotcle();
        checkSubsetAdmin(motcle.getThesaurus());
        Thesaurus thesaurus = motcle.getThesaurus();
        String idParentString = getMandatory(NEWPARENT_PARAMNAME).trim();
        if (idParentString.length() > 0) {
            try {
                parentMotcle = ThesaurusUtils.getMotcle(thesaurus, idParentString);
            } catch (NumberFormatException nfe) {
            }
            if (parentMotcle == null) {
                throw BdfErrors.error("_ error.unknown.parentmotcle", idParentString);
            }
        }
        if ((parentMotcle != null) && (parentMotcle.equals(motcle))) {
            throw BdfErrors.error("_ error.unsupported.selfparent");
        }
        if (parentMotcle != null) {
            if (ThesaurusUtils.isDescendant(parentMotcle, motcle)) {
                Object[] objserr = new Object[2];
                if (thesaurus.isIdalphaType()) {
                    objserr[0] = motcle.getIdalpha();
                    objserr[1] = parentMotcle.getIdalpha();
                } else {
                    objserr[0] = motcle.getId();
                    objserr[1] = parentMotcle.getId();
                }
                throw BdfErrors.error("_ error.unsupported.recursiveparent", objserr);
            }
        }
    }

}
