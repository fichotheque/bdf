/* BdfServer_HtmlProducers - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.ExistingSubsetException;
import net.fichotheque.SubsetKey;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadata;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ThesaurusCreation";
    public final static String COMMANDKEY = "_ THS-01";
    public final static String NEWTHESAURUS_PARAMNAME = "newthesaurus";
    public final static String THESAURUSTYPE_PARAMNAME = "thesaurustype";
    private SubsetKey newThesaurusKey;
    private short thesaurusType;

    public ThesaurusCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Thesaurus thesaurus;
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            try {
                ThesaurusEditor thesaurusEditor = session.getFichothequeEditor().createThesaurus(newThesaurusKey, thesaurusType);
                if (thesaurusType != ThesaurusMetadata.BABELIEN_TYPE) {
                    thesaurusEditor.getThesaurusMetadataEditor().setAuthorizedLangs(bdfServer.getLangConfiguration().getWorkingLangs());
                }
                thesaurus = thesaurusEditor.getThesaurus();
            } catch (ExistingSubsetException ese) {
                throw new ShouldNotOccurException("test before ExistingSubsetException");
            }
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        setDone("_ done.thesaurus.thesauruscreation", newThesaurusKey.getKeyString());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        String newThesaurusName = getMandatory(NEWTHESAURUS_PARAMNAME);
        newThesaurusName = newThesaurusName.trim();
        if (newThesaurusName.length() == 0) {
            throw BdfErrors.error("_ error.empty.thesaurusname", newThesaurusName);
        }
        try {
            newThesaurusKey = SubsetKey.parse(SubsetKey.CATEGORY_THESAURUS, newThesaurusName);
            if (fichotheque.containsSubset(newThesaurusKey)) {
                throw BdfErrors.error("_ error.existing.thesaurus", newThesaurusKey.getKeyString());
            }
        } catch (ParseException pe) {
            throw BdfErrors.error("_ error.wrong.thesaurusname", newThesaurusName);
        }
        String thesaurusTypeString = getMandatory(THESAURUSTYPE_PARAMNAME);
        try {
            thesaurusType = ThesaurusUtils.thesaurusTypeToShort(thesaurusTypeString);
        } catch (IllegalArgumentException iae) {
            throw BdfErrors.unknownParameterValue(THESAURUSTYPE_PARAMNAME, thesaurusTypeString);
        }
    }

}
