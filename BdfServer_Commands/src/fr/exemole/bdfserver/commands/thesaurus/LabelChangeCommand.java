/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.tools.parsers.TypoParser;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
public class LabelChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "LabelChange";
    public final static String COMMANDKEY = "_ THS-11";
    public final static String LABEL_PARAMPREFIX = "label/";
    public final static String BABELIEN_LANG_PARAMNAME = "babelienlang";
    public final static String BABELIEN_LABEL_PARAMNAME = "babelienlabel";
    private Motcle motcle;
    private LabelChange motcleLabelChange;
    private Lang babelienLang;
    private String babelienLabel;

    public LabelChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Thesaurus thesaurus = motcle.getThesaurus();
        boolean done = false;
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            ThesaurusEditor thesaurusEditor = session.getFichothequeEditor().getThesaurusEditor(thesaurus);
            if (motcleLabelChange != null) {
                done = ThesaurusUtils.changeMotcleLabels(thesaurusEditor, motcle, motcleLabelChange);
            } else if (babelienLabel != null) {
                TypoOptions typoOptions = TypoOptions.getTypoOptions(babelienLang.toLocale());
                done = thesaurusEditor.putLabel(motcle, babelienLang, TypoParser.parseTypo(babelienLabel, typoOptions));
            }
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        putResultObject(MOTCLE_OBJ, motcle);
        if (done) {
            setDone("_ done.thesaurus.labelchange");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        motcle = requestHandler.getMandatoryMotcle();
        checkSubsetAdmin(motcle.getThesaurus());
        if (motcle.getThesaurus().isBabelienType()) {
            babelienLabel = getMandatory(BABELIEN_LABEL_PARAMNAME);
            String langString = getMandatory(BABELIEN_LANG_PARAMNAME).trim();
            if (!langString.isEmpty()) {
                try {
                    babelienLang = Lang.parse(langString);
                } catch (ParseException lie) {
                    throw BdfErrors.error("_ error.wrong.lang", langString);
                }
            }
        } else {
            motcleLabelChange = requestHandler.getLabelChange(LABEL_PARAMPREFIX);
        }
    }

}
