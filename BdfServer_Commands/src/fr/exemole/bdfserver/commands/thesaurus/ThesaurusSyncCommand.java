/* BdfServer_Commands - Copyright (c) 2024-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.sync.SyncEngine;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import net.fichotheque.namespaces.SyncSpace;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.sync.ThesaurusSync;
import net.fichotheque.tools.thesaurus.sync.ThesaurusSyncResult;
import net.fichotheque.tools.thesaurus.sync.ThesaurusSyncUtils;
import net.mapeadores.util.exceptions.ResponseCodeException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.request.RequestMap;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusSyncCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ThesaurusSync";
    private Thesaurus thesaurus;
    private URL url;

    public ThesaurusSyncCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        ThesaurusSync thesaurusSync;
        try {
            thesaurusSync = ThesaurusSyncUtils.download(url, LogUtils.NULL_MULTIMESSAGEHANDLER);
        } catch (ResponseCodeException rce) {
            throw BdfErrors.error("_ error.exception.url_httpcode", rce.getHttpStatusCode());
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.url_io", url);
        } catch (SAXException saxe) {
            throw BdfErrors.error("_ error.exception.xml.sax", saxe.getLocalizedMessage());
        }
        ThesaurusSyncResult syncResult;
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            syncResult = SyncEngine.runThesaurusSync(session, this, thesaurusSync, thesaurus);
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        if (syncResult.isEmpty()) {
            setDone("_ done.thesaurus.sync_nochange");
        } else {
            setDone("_ done.thesaurus.sync", syncResult.getChangeCount(), syncResult.getCreationCount(), syncResult.getOrderCount());
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        thesaurus = requestHandler.getMandatoryThesaurus();
        checkSubsetAdmin(thesaurus);
        String urlString = ThesaurusSyncUtils.getSyncUrl(thesaurus);
        if (urlString == null) {
            throw BdfErrors.error("_ error.empty.attribute", SyncSpace.URL_KEY);
        }
        try {
            url = new URL(urlString);
        } catch (MalformedURLException mue) {
            throw BdfErrors.error("_ error.wrong.url", urlString);
        }
    }

}
