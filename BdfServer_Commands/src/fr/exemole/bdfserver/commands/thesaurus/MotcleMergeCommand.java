/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.users.BdfUserConstants;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.ParentRecursivityException;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.thesaurus.ThesaurusTools;
import net.fichotheque.utils.FichothequeUtils;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class MotcleMergeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "MotcleMerge";
    public final static String COMMANDKEY = "_ THS-18";
    public static String MERGETHESAURUS_PARAMNAME = "mergethesaurus";
    public static String MERGEMOTCLE_PARAMNAME = "mergemotcle";
    private Motcle motcle;
    private Motcle mergeMotcle;

    public MotcleMergeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Thesaurus mergeThesaurus = mergeMotcle.getThesaurus();
        Lang workingLang = bdfUser.getWorkingLang();
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            try {
                Langs langs = bdfServer.getThesaurusLangChecker().getAuthorizedLangs(mergeThesaurus);
                ThesaurusTools.merge(session.getFichothequeEditor(), motcle, mergeMotcle, langs);
            } catch (ParentRecursivityException pre) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        bdfUser.putParameter(BdfUserConstants.THESAURUSMERGELAST_SUBSETKEY_OBJ, mergeThesaurus.getSubsetKey());
        putResultObject(THESAURUS_OBJ, mergeThesaurus);
        putResultObject(MOTCLE_OBJ, mergeMotcle);
        String messageKey = (mergeThesaurus.equals(motcle.getThesaurus())) ? "_ done.thesaurus.motclemerge_samethesaurus" : "_ done.thesaurus.motclemerge_otherthesaurus";
        setDone(messageKey, ThesaurusUtils.getMotcleTitle(motcle, workingLang, true, false), ThesaurusUtils.getMotcleTitle(mergeMotcle, workingLang, true, false));
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        motcle = requestHandler.getMandatoryMotcle();
        String mergeThesaurusName = getMandatory(MERGETHESAURUS_PARAMNAME);
        Thesaurus mergeThesaurus = FichothequeUtils.getThesaurus(fichotheque, mergeThesaurusName);
        if (mergeThesaurus == null) {
            throw BdfErrors.error("_ error.unknown.thesaurus", mergeThesaurusName);
        }
        checkSubsetAdmin(motcle.getThesaurus());
        checkSubsetAccess(mergeThesaurus);
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_merge");
        }
        String idMergeString = getMandatory(MERGEMOTCLE_PARAMNAME);
        try {
            mergeMotcle = ThesaurusUtils.getMotcle(mergeThesaurus, idMergeString);
        } catch (NumberFormatException nfe) {
        }
        if (mergeMotcle == null) {
            throw BdfErrors.error("_ error.unknown.mergemotcle", idMergeString);
        }
        if (ThesaurusUtils.isDescendant(mergeMotcle, motcle)) {
            throw BdfErrors.error("_ error.unsupported.recursivemerge", idMergeString);
        }
    }

}
