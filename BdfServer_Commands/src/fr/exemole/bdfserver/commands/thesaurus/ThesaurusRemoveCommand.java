/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.NoRemoveableSubsetException;
import net.fichotheque.SubsetKey;
import net.fichotheque.thesaurus.Thesaurus;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ThesaurusRemove";
    public final static String COMMANDKEY = "_ THS-04";
    private Thesaurus thesaurus;

    public ThesaurusRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        SubsetKey thesaurusKey = thesaurus.getSubsetKey();
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            try {
                session.getFichothequeEditor().removeThesaurus(thesaurus);
            } catch (NoRemoveableSubsetException nrse) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        putResultObject(SUBSETKEY_OBJ, thesaurusKey);
        setDone("_ done.thesaurus.thesaurusremove", thesaurusKey.getSubsetName());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        thesaurus = requestHandler.getMandatoryThesaurus();
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        if (!thesaurus.isRemoveable()) {
            throw BdfErrors.error("_ error.notremoveable.thesaurus");
        }
    }

}
