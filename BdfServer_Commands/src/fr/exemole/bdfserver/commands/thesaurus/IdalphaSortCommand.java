/* BdfServer_Commands - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.tools.thesaurus.IdalphaSortEngine;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class IdalphaSortCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "IdalphaSort";
    public final static String COMMANDKEY = "_ THS-07";
    public final static String RECURSIVE_PARAMNAME = "recursive";
    public final static String DESCENDING_PARAMNAME = "descending";
    public final static String IGNORECASE_PARAMNAME = "ignorecase";
    public final static String IGNOREPUNCTUATION_PARAMNAME = "ignorepunctuation";
    private Thesaurus thesaurus;
    private Motcle motcle;
    private boolean recursive = false;
    private boolean descending = false;
    private boolean ignoreCase = false;
    private boolean ignorePunctuation = false;

    public IdalphaSortCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            ThesaurusEditor thesaurusEditor = session.getFichothequeEditor().getThesaurusEditor(thesaurus);
            IdalphaSortEngine.init(thesaurusEditor)
                    .setRecursive(recursive)
                    .setDescending(descending)
                    .setIgnoreCase(ignoreCase)
                    .setIgnorePunctuation(ignorePunctuation)
                    .sort(motcle);
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        if (motcle != null) {
            putResultObject(MOTCLE_OBJ, motcle);
        }
        setDone("_ done.thesaurus.idalphasort");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        thesaurus = requestHandler.getMandatoryThesaurus();
        checkSubsetAdmin(thesaurus);
        motcle = requestHandler.getOptionnalMotcle(thesaurus);
        recursive = requestHandler.isTrue(RECURSIVE_PARAMNAME);
        descending = requestHandler.isTrue(DESCENDING_PARAMNAME);
        ignoreCase = requestHandler.isTrue(IGNORECASE_PARAMNAME);
        ignorePunctuation = requestHandler.isTrue(IGNOREPUNCTUATION_PARAMNAME);
    }

}
