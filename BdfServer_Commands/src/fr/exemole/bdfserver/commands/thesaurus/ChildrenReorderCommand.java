/* BdfServer_Commands - Copyright (c) 2014-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.fichotheque.tools.thesaurus.ChildrenReorderEngine;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class ChildrenReorderCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ChildrenReorder";
    public final static String COMMANDKEY = "_ THS-21";
    public final static String CHILDREN_PARAMNAME = "children";
    private Thesaurus thesaurus;
    private Motcle motcle;
    private final Set<Motcle> newOrderSet = new LinkedHashSet<Motcle>();

    public ChildrenReorderCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            ThesaurusEditor thesaurusEditor = session.getFichothequeEditor().getThesaurusEditor(thesaurus);
            ChildrenReorderEngine engine = new ChildrenReorderEngine(thesaurusEditor);
            engine.reorder(motcle, newOrderSet);
        }
        putResultObject(BdfInstructionConstants.THESAURUS_OBJ, thesaurus);
        if (motcle != null) {
            putResultObject(BdfInstructionConstants.MOTCLE_OBJ, motcle);
        }
        setDone("_ done.thesaurus.childrenreorder");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        thesaurus = requestHandler.getMandatoryThesaurus();
        checkSubsetAdmin(thesaurus);
        motcle = requestHandler.getOptionnalMotcle(thesaurus);
        String list = getMandatory(CHILDREN_PARAMNAME);
        String[] tokens = StringUtils.getLineTokens(list, StringUtils.EMPTY_EXCLUDE);
        boolean idalphaTest = thesaurus.isIdalphaType();
        int length = tokens.length;
        for (int i = 0; i < length; i++) {
            String token = tokens[i];
            Motcle child = null;
            if (idalphaTest) {
                child = thesaurus.getMotcleByIdalpha(token);
            } else {
                try {
                    int id = Integer.parseInt(token);
                    child = thesaurus.getMotcleById(id);
                } catch (NumberFormatException nfe) {
                }
            }
            if ((child != null) && (isParent(motcle, child))) {
                newOrderSet.add(child);
            }
        }
    }

    private static boolean isParent(Motcle parent, Motcle child) {
        if (parent == null) {
            return (child.getParent() == null);
        } else if (child.getParent() == null) {
            return false;
        } else {
            return child.getParent().equals(parent);
        }

    }

}
