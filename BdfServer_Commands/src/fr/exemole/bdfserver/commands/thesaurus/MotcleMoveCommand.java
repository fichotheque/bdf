/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.users.BdfUserConstants;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.tools.thesaurus.ThesaurusTools;
import net.fichotheque.utils.FichothequeUtils;
import net.fichotheque.utils.ThesaurusUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class MotcleMoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "MotcleMove";
    public final static String COMMANDKEY = "_ THS-17";
    public static String DESTINATIONTHESAURUS_PARAMNAME = "destinationthesaurus";
    private Motcle motcle;
    private Thesaurus destinationThesaurus;

    public MotcleMoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Lang lang = bdfUser.getWorkingLang();
        Langs langs = bdfServer.getThesaurusLangChecker().getAuthorizedLangs(destinationThesaurus);
        Motcle newMotcle;
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            newMotcle = ThesaurusTools.move(session.getFichothequeEditor(), motcle, destinationThesaurus, langs);
        }
        bdfUser.putParameter(BdfUserConstants.THESAURUSMOVELAST_SUBSETKEY_OBJ, destinationThesaurus.getSubsetKey());
        putResultObject(THESAURUS_OBJ, destinationThesaurus);
        putResultObject(MOTCLE_OBJ, newMotcle);
        setDone("_ done.thesaurus.motclemove", ThesaurusUtils.getMotcleTitle(motcle, lang, true, false), FichothequeUtils.getTitle(destinationThesaurus, lang), newMotcle.getId(), true);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        motcle = requestHandler.getMandatoryMotcle();
        checkSubsetAdmin(motcle.getThesaurus());
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_move");
        }
        String destinationThesaurusName = getMandatory(DESTINATIONTHESAURUS_PARAMNAME);
        destinationThesaurus = FichothequeUtils.getThesaurus(fichotheque, destinationThesaurusName);
        if (destinationThesaurus == null) {
            throw BdfErrors.error("_ error.unknown.thesaurus", destinationThesaurusName);
        }
        checkSubsetAdmin(destinationThesaurus);
        if (destinationThesaurus.equals(motcle.getThesaurus())) {
            throw BdfErrors.error("_ error.unsupported.samethesaurus");
        }
    }

}
