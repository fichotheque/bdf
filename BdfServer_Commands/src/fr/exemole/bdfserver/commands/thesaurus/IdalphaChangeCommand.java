/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.SubsetKey;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.ThesaurusEditor;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.Idalpha;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class IdalphaChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "IdalphaChange";
    public final static String COMMANDKEY = "_ THS-09";
    public final static String NEWIDALPHA_PARAMNAME = "newidalpha";
    private Motcle motcle;
    private String newIdalpha;

    public IdalphaChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Thesaurus thesaurus = motcle.getThesaurus();
        boolean done;
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            ThesaurusEditor thesaurusEditor = session.getFichothequeEditor().getThesaurusEditor(thesaurus);
            try {
                done = thesaurusEditor.setIdalpha(motcle, newIdalpha);
            } catch (ExistingIdException | ParseException eia) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        putResultObject(MOTCLE_OBJ, motcle);
        if (done) {
            setDone("_ done.thesaurus.idalphachange");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        motcle = requestHandler.getMandatoryMotcle();
        Thesaurus thesaurus = motcle.getThesaurus();
        checkSubsetAdmin(thesaurus);
        if (!thesaurus.isIdalphaType()) {
            throw BdfErrors.unsupportedParameterValue(SubsetKey.CATEGORY_THESAURUS_STRING, thesaurus.getSubsetName());
        }
        newIdalpha = StringUtils.cleanString(getMandatory(NEWIDALPHA_PARAMNAME));
        if (newIdalpha.isEmpty()) {
            throw BdfErrors.error("_ error.empty.idalpha");
        }
        if (!Idalpha.isValid(newIdalpha)) {
            throw BdfErrors.error("_ error.wrong.idalpha", newIdalpha);
        }
        Motcle other = thesaurus.getMotcleByIdalpha(newIdalpha);
        if (other != null) {
            throw BdfErrors.error("_ error.existing.idalpha", newIdalpha);
        }
    }

}
