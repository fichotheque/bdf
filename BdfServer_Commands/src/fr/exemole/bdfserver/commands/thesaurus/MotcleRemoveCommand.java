/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.Fichotheque;
import net.fichotheque.Subset;
import net.fichotheque.croisement.Croisements;
import net.fichotheque.thesaurus.Motcle;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.policies.DynamicEditPolicy;
import net.fichotheque.tools.FichothequeTools;
import net.fichotheque.utils.FichothequeUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class MotcleRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "MotcleRemove";
    public final static String COMMANDKEY = "_ THS-12";
    private Motcle motcle;

    public MotcleRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Thesaurus thesaurus = motcle.getThesaurus();
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            FichothequeTools.remove(session.getFichothequeEditor(), motcle);
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        setDone("_ done.thesaurus.motcleremove");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        motcle = requestHandler.getMandatoryMotcle();
        checkSubsetAdmin(motcle.getThesaurus());
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        if (!isRemoveCandidate(bdfServer, motcle)) {
            throw BdfErrors.error("_ error.notremoveable.motcle");
        }
    }

    public static boolean isRemoveCandidate(BdfServer bdfServer, Motcle motcle) {
        DynamicEditPolicy dynamicEditPolicy = bdfServer.getPolicyManager().getPolicyProvider().getDynamicEditPolicy(motcle.getThesaurus());
        if (dynamicEditPolicy.isLax()) {
            return true;
        }
        Thesaurus thesaurus = motcle.getThesaurus();
        Fichotheque fichotheque = thesaurus.getFichotheque();
        if (motcle.isRemoveable()) {
            return true;
        }
        if (motcle.getChildList().size() > 0) {
            return false;
        }
        Subset[] subsetArray = FichothequeUtils.toSubsetArray(fichotheque);
        for (Subset subset : subsetArray) {
            Croisements croisements = motcle.getCroisements(subset);
            if (!croisements.isEmpty()) {
                return false;
            }
        }
        return true;
    }

}
