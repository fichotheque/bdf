/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.thesaurus;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.thesaurus.metadata.ThesaurusMetadataEditor;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusLangListCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ThesaurusLangList";
    public final static String COMMANDKEY = "_ THS-03";
    public final static String LANGLIST_PARAMNAME = "langlist";
    private Thesaurus thesaurus;
    private Langs langs;

    public ThesaurusLangListCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done;
        try (EditSession session = startEditSession(Domains.THESAURUS, COMMANDNAME)) {
            ThesaurusMetadataEditor thesaurusMetadataEditor = session.getFichothequeEditor().getThesaurusEditor(thesaurus).getThesaurusMetadataEditor();
            done = thesaurusMetadataEditor.setAuthorizedLangs(langs);
        }
        putResultObject(THESAURUS_OBJ, thesaurus);
        if (done) {
            setDone("_ done.thesaurus.langlist");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        thesaurus = requestHandler.getMandatoryThesaurus();
        checkSubsetAdmin(thesaurus);
        String langListString = getMandatory(LANGLIST_PARAMNAME);
        langs = LangsUtils.toCleanLangs(langListString);
        if (langs.isEmpty()) {
            throw BdfErrors.error("_ error.empty.langlist");
        }
    }

}
