/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.zip.BdfServerZip;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipOutputStream;
import net.fichotheque.eligibility.SubsetEligibility;
import net.fichotheque.tools.eligibility.SelectionSubsetEligibility;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class BackupCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "Backup";
    public final static String COMMANDKEY = "_ ADM-01";
    public final static String SELECTION_PARAMNAME = "selection";
    private boolean selection;

    public BackupCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        File backupFile = getBackupFile();
        SubsetEligibility subsetEligibility = null;
        if (selection) {
            subsetEligibility = SelectionSubsetEligibility.build(fichotheque, bdfUser.getSelectedFiches());
        }
        BdfServerZip bdfServerZip = new BdfServerZip(bdfServer, subsetEligibility);
        try (ZipOutputStream os = new ZipOutputStream(new FileOutputStream(backupFile))) {
            bdfServerZip.zip(os);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        }
        putResultObject(BACKUPFILENAME_OBJ, backupFile.getName());
        setDone("_ done.administration.backup");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        selection = requestHandler.isTrue(SELECTION_PARAMNAME);
    }

    public File getBackupFile() {
        File backupDirectory = ConfigurationUtils.getBackupDirectory(bdfServer);
        StringBuilder buf = new StringBuilder();
        buf.append("bdf-");
        buf.append(bdfServer.getFichotheque().getFichothequeMetadata().getBaseName());
        if (selection) {
            buf.append("-selection");
        }
        buf.append(".zip");
        File backupFile = new File(backupDirectory, buf.toString());
        return backupFile;
    }

}
