/* BdfServer_Commands - Copyright (c) 2021-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.storage.BdfStorageException;
import fr.exemole.bdfserver.tools.apps.AppConfUtils;
import fr.exemole.bdfserver.tools.apps.StarterEngine;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import java.io.IOException;
import java.text.ParseException;
import net.fichotheque.EditOrigin;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class NewAppCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "NewApp";
    public final static String COMMANDKEY = "_ ADM-15";
    public final static String NEWAPPNAME_PARAMNAME = "newappname";
    public final static String STARTER_PARAMNAME = "starter";
    private String newAppName;
    private String starterName;

    public NewAppCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.ADMINISTRATION, COMMANDNAME);
        try {
            StarterEngine.copy(bdfServer, editOrigin, newAppName, starterName);
            setDone("_ done.administration.newapp", newAppName);
        } catch (IOException ioe) {
            throw new BdfStorageException(ioe);
        } catch (ParseException pe) {
            throw new ShouldNotOccurException("test done before", pe);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        this.newAppName = getMandatory(NEWAPPNAME_PARAMNAME);
        this.starterName = requestHandler.getTrimedParameter(STARTER_PARAMNAME);
        if (starterName.isEmpty()) {
            starterName = "minimal";
        }
        try {
            StringUtils.checkTechnicalName(newAppName, true);
        } catch (ParseException pe) {
            throw BdfErrors.error("_ error.wrong.appname", newAppName);
        }
        RelativePath appIniPath = StorageUtils.buildAppResourcePath(newAppName, "app.ini");
        if (bdfServer.getResourceStorages().containsResource(appIniPath)) {
            throw BdfErrors.error("_ error.existing.app", newAppName);
        }
        if (!AppConfUtils.isAvailableStarter(starterName)) {
            throw BdfErrors.error("_ error.unknwon.starter", starterName);
        }
    }

}
