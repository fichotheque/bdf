/* BdfServer_Commands - Copyright (c) 2014-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import net.fichotheque.EditOrigin;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class ResourceRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ResourceRemove";
    public final static String COMMANDKEY = "_ ADM-13";
    public final static String RESTORE_COMMANDKEY = "_ ADM-14";
    private RelativePath relativePath;
    private boolean restore;

    public ResourceRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.ADMINISTRATION, COMMANDNAME);
        boolean done = StorageUtils.removeResource(bdfServer, relativePath, editOrigin);
        if (done) {
            setDone((restore) ? "_ done.administration.resourceremove_restore" : "_ done.administration.resourceremove", relativePath.toString());
            BdfServerUtils.checkResourceChange(bdfServer, relativePath);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        relativePath = requestHandler.getMandatoryRelativePath();
        restore = StorageUtils.containsDefaultResource(bdfServer, relativePath);
        if (!checkConfirmation()) {
            throw BdfErrors.error((restore) ? "_ error.empty.confirmationcheck_restore" : "_ error.empty.confirmationcheck_remove");
        }
        if (!StorageUtils.containsVarResource(bdfServer, relativePath)) {
            throw BdfErrors.error("_ error.notremoveable.resource", relativePath.toString());
        }
    }

}
