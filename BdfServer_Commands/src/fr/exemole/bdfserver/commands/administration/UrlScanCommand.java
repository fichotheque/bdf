/* BdfServer_Commands - Copyright (c) 2022-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.diagnostic.urlscan.UrlInfo;
import fr.exemole.bdfserver.tools.diagnostic.urlscan.UrlScanEngine;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import java.util.Collection;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class UrlScanCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "UrlScan";
    public final static String FICHES_PARAMNAME = "fiches";
    public final static String ALL_FICHES_PARAMVALUE = "all";
    public final static String SELECTION_FICHES_PARAMVALUE = "selection";
    private boolean all = false;

    public UrlScanCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Collection<UrlInfo> infos;
        if (all) {
            infos = UrlScanEngine.run(bdfServer).values();
        } else {
            infos = UrlScanEngine.run(bdfServer, bdfUser.getSelectedFiches()).values();
        }
        putResultObject(URLINFOCOLLECTION_OBJ, infos);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        String fiches = requestHandler.getTrimedParameter(FICHES_PARAMNAME);
        if (fiches.equals(ALL_FICHES_PARAMVALUE)) {
            all = true;
        }
    }

}
