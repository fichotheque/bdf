/* BdfServer_Commands - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class AdministrationCommandFactory {

    private AdministrationCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case BackupCommand.COMMANDNAME:
                return new BackupCommand(bdfServer, requestMap);
            case BackupDeleteCommand.COMMANDNAME:
                return new BackupDeleteCommand(bdfServer, requestMap);
            case RoleChangeCommand.COMMANDNAME:
                return new RoleChangeCommand(bdfServer, requestMap);
            case RoleCreationCommand.COMMANDNAME:
                return new RoleCreationCommand(bdfServer, requestMap);
            case RoleRemoveCommand.COMMANDNAME:
                return new RoleRemoveCommand(bdfServer, requestMap);
            case StreamTextChangeCommand.COMMANDNAME:
                return new StreamTextChangeCommand(bdfServer, requestMap);
            case ResourceCreationCommand.COMMANDNAME:
                return new ResourceCreationCommand(bdfServer, requestMap);
            case ResourceRemoveCommand.COMMANDNAME:
                return new ResourceRemoveCommand(bdfServer, requestMap);
            case ResourceUploadCommand.COMMANDNAME:
                return new ResourceUploadCommand(bdfServer, requestMap);
            case NewAppCommand.COMMANDNAME:
                return new NewAppCommand(bdfServer, requestMap);
            case UrlScanCommand.COMMANDNAME:
                return new UrlScanCommand(bdfServer, requestMap);
            case UrlTestCommand.COMMANDNAME:
                return new UrlTestCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
