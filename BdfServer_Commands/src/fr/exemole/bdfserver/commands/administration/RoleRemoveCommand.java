/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.administration;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.roles.Role;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.roles.RoleUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RoleRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RoleRemove";
    public final static String COMMANDKEY = "_ ROL-03";
    private Role role;

    public RoleRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        String roleName = role.getName();
        try (EditSession session = startEditSession(Domains.ADMINISTRATION, COMMANDNAME)) {
            session.getBdfServerEditor().getRoleEditor().removeRole(role);
        }
        setDone("_ done.administration.roleremove", roleName);
        putResultObject(STRING_OBJ, roleName);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        role = requestHandler.getMandatoryRole();
        if (RoleUtils.isDefaultRole(role)) {
            throw BdfErrors.wrongParameterValue("role", "default role");
        }
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
    }

}
