/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.addenda;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.NoRemoveableSubsetException;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Addenda;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AddendaRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "AddendaRemove";
    public final static String COMMANDKEY = "_ ADD-04";
    private Addenda addenda;

    public AddendaRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        SubsetKey addendaKey = addenda.getSubsetKey();
        try (EditSession session = startEditSession(Domains.ADDENDA, COMMANDNAME)) {
            try {
                session.getFichothequeEditor().removeAddenda(addenda);
            } catch (NoRemoveableSubsetException nrse) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        putResultObject(SUBSETKEY_OBJ, addendaKey);
        setDone("_ done.addenda.addendaremove", addendaKey.getSubsetName());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        addenda = requestHandler.getMandatoryAddenda();
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        if (!addenda.isRemoveable()) {
            throw BdfErrors.error("_ error.notremoveable.addenda");
        }
    }

}
