/* BdfServer_Commands - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.addenda;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class AddendaCommandFactory {

    private AddendaCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case AddendaCreationCommand.COMMANDNAME:
                return new AddendaCreationCommand(bdfServer, requestMap);
            case AddendaPhrasesCommand.COMMANDNAME:
                return new AddendaPhrasesCommand(bdfServer, requestMap);
            case AddendaRemoveCommand.COMMANDNAME:
                return new AddendaRemoveCommand(bdfServer, requestMap);
            case AddendaAttributeChangeCommand.COMMANDNAME:
                return new AddendaAttributeChangeCommand(bdfServer, requestMap);
            case DocumentFileUploadCommand.COMMANDNAME:
                return new DocumentFileUploadCommand(bdfServer, requestMap);
            case DocumentFileDownloadCommand.COMMANDNAME:
                return new DocumentFileDownloadCommand(bdfServer, requestMap);
            case DocumentNameChangeCommand.COMMANDNAME:
                return new DocumentNameChangeCommand(bdfServer, requestMap);
            case DocumentAttributeChangeCommand.COMMANDNAME:
                return new DocumentAttributeChangeCommand(bdfServer, requestMap);
            case DocumentRemoveCommand.COMMANDNAME:
                return new DocumentRemoveCommand(bdfServer, requestMap);
            case CroisementRemoveCommand.COMMANDNAME:
                return new CroisementRemoveCommand(bdfServer, requestMap);
            case CroisementAddCommand.COMMANDNAME:
                return new CroisementAddCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
