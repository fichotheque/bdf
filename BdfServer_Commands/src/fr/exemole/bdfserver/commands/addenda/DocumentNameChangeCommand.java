/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.addenda;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.ExistingNameException;
import net.fichotheque.addenda.AddendaEditor;
import net.fichotheque.addenda.Document;
import net.fichotheque.utils.AddendaUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class DocumentNameChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "DocumentNameChange";
    public final static String COMMANDKEY = "_ ADD-05";
    public final static String NAME_PARAMNAME = "documentname";
    private Document document;
    private String newDocumentName;

    public DocumentNameChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.ADDENDA, COMMANDNAME)) {
            AddendaEditor addendaEditor = session.getFichothequeEditor().getAddendaEditor(document.getSubsetKey());
            try {
                addendaEditor.setBasename(document, newDocumentName);
            } catch (ExistingNameException | ParseException pe) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        putResultObject(ADDENDA_OBJ, document.getAddenda());
        putResultObject(DOCUMENT_OBJ, document);
        setDone("_ done.addenda.documentnamechange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        document = requestHandler.getMandatoryDocument();
        getPermissionChecker()
                .checkWrite(document);
        newDocumentName = getMandatory(NAME_PARAMNAME);
        newDocumentName = StringUtils.cleanString(newDocumentName);
        if (newDocumentName.isEmpty()) {
            throw BdfErrors.error("_ error.empty.documentname", newDocumentName);
        }
        if (!AddendaUtils.testBasename(newDocumentName)) {
            throw BdfErrors.error("_ error.wrong.documentname", newDocumentName);
        }
        if (document.getAddenda().getDocumentByBasename(newDocumentName) != null) {
            throw BdfErrors.error("_ error.existing.documentname", newDocumentName);
        }
    }

}
