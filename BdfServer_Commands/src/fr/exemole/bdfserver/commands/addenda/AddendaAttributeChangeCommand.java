/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.addenda;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.addenda.Addenda;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AddendaAttributeChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "AddendaAttributeChange";
    public final static String COMMANDKEY = "_ ADD-03";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    private Addenda addenda;
    private AttributeChange attributeChange;

    public AddendaAttributeChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.ADDENDA, COMMANDNAME)) {
            session.getFichothequeEditor().changeAttributes(addenda.getAddendaMetadata(), attributeChange);
        }
        putResultObject(ADDENDA_OBJ, addenda);
        setDone("_ done.global.attributechange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        addenda = requestHandler.getMandatoryAddenda();
        checkSubsetAdmin(addenda);
        String attributes = getMandatory(ATTRIBUTES_PARAMNAME);
        this.attributeChange = AttributeParser.parse(attributes);
    }

}
