/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.addenda;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import static fr.exemole.bdfserver.api.instruction.BdfInstructionConstants.ADDENDA_OBJ;
import static fr.exemole.bdfserver.api.instruction.BdfInstructionConstants.DOCUMENT_OBJ;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.fichotheque.addenda.Document;
import net.fichotheque.croisement.CroisementChanges;
import net.fichotheque.tools.croisement.CroisementChangeEngine;
import net.fichotheque.tools.croisement.LienBuffer;
import net.fichotheque.tools.parsers.croisement.LienBufferParser;
import net.fichotheque.tools.permission.PermissionPredicate;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class CroisementAddCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "CroisementAdd";
    public final static String COMMANDKEY = "_ ADD-09";
    public final static String ADD_PARAMNAME = "add";
    private Document document;
    private CroisementChanges croisementChanges;

    public CroisementAddCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.ADDENDA, COMMANDNAME)) {
            session.getFichothequeEditor().getCroisementEditor().updateCroisements(document, croisementChanges);
        }
        putResultObject(ADDENDA_OBJ, document.getAddenda());
        putResultObject(DOCUMENT_OBJ, document);
        setDone("_ done.addenda.croisementadd");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        document = requestHandler.getMandatoryDocument();
        getPermissionChecker()
                .checkWrite(document);
        PermissionPredicate predicate = PermissionPredicate.read(getPermissionSummary());
        String addString = getMandatory(ADD_PARAMNAME);
        String[] tokens = StringUtils.getTechnicalTokens(addString, false);
        CroisementChangeEngine croisementChangeEngine = CroisementChangeEngine.appendEngine(document);
        boolean done = false;
        for (String token : tokens) {
            try {
                LienBuffer lienBuffer = LienBufferParser.parse(fichotheque, token, SubsetKey.CATEGORY_CORPUS);
                if (predicate.test(lienBuffer.getSubsetItem())) {
                    croisementChangeEngine.addLien(lienBuffer);
                    done = true;
                }
            } catch (ParseException e) {

            }
        }
        if (!done) {
            throw BdfErrors.error("_ error.wrong.fichekeys");
        }
        croisementChanges = croisementChangeEngine.toCroisementChanges();
    }

}
