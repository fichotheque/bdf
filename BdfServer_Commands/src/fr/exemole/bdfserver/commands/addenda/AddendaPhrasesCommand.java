/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.addenda;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.commands.AbstractPhrasesCommand;
import net.fichotheque.addenda.Addenda;
import net.fichotheque.addenda.metadata.AddendaMetadataEditor;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AddendaPhrasesCommand extends AbstractPhrasesCommand {

    public final static String COMMANDNAME = "AddendaPhrases";
    public final static String COMMANDKEY = "_ ADD-02";
    private Addenda addenda;

    public AddendaPhrasesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done = false;
        try (EditSession session = startEditSession(Domains.ADDENDA, COMMANDNAME)) {
            AddendaMetadataEditor addendaMetadataEditor = session.getFichothequeEditor().getAddendaEditor(addenda).getAddendaMetadataEditor();
            if (update(addendaMetadataEditor)) {
                done = true;
            }
        }
        putResultObject(ADDENDA_OBJ, addenda);
        if (done) {
            setDone("_ done.addenda.addendaphrases");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        addenda = requestHandler.getMandatoryAddenda();
        checkSubsetAdmin(addenda);
        checkPhrasesParameters();
    }

}
