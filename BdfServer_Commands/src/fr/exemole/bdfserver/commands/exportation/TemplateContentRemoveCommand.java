/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class TemplateContentRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TemplateContentRemove";
    public final static String PATH_PARAMNAME = "path";
    public final static String COMMANDKEY = "_ EXP-34";
    private TemplateKey templateKey;
    private String path;

    public TemplateContentRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        TemplateDescription templateDescription = bdfServer.getTransformationManager().removeTemplateContent(templateKey, path, editOrigin);
        if (templateDescription != null) {
            putResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ, templateDescription);
            setDone("_ done.exportation.templatecontentremove", templateKey.getKeyString(), path);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TemplateDescription templateDescription = requestHandler.getMandatoryTemplateDescription();
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        this.templateKey = templateDescription.getTemplateKey();
        this.path = getMandatory(PATH_PARAMNAME);
        TemplateContentDescription templateContentDescription = templateDescription.getTemplateContentDescription(path);
        if (templateContentDescription == null) {
            throw BdfErrors.unknownParameterValue(PATH_PARAMNAME, path);
        } else if (templateContentDescription.isMandatory()) {
            throw BdfErrors.unsupportedNotEditableParameterValue(PATH_PARAMNAME, path);
        }
    }

}
