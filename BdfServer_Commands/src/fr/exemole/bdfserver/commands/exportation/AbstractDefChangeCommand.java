/* BdfServer_Commands - Copyright (c) 2024-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import static fr.exemole.bdfserver.commands.exportation.AbstractDefChangeCommand.SELECTIONDEFNAME_PARAMNAME;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.tools.selection.SelectionOptionsBuilder;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractDefChangeCommand extends AbstractBdfCommand {

    public final static String TARGETNAME_PARAMNAME = "targetname";
    public final static String TARGETPATH_PARAMNAME = "targetpath";
    public final static String SELECTIONDEFNAME_PARAMNAME = "selectiondefname";
    public final static String QUERY_XML_PARAMNAME = "query_xml";
    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";

    public AbstractDefChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    public void checkSelectionOptions(SelectionOptionsBuilder selectionOptionsBuilder) throws ErrorMessageException {
        String selectionDefName = requestHandler.getTrimedParameter(SELECTIONDEFNAME_PARAMNAME);
        selectionOptionsBuilder.setSelectionDefName(selectionDefName);
        BdfCommandUtils.parseQueries(fichotheque, requestHandler.getTrimedParameter(QUERY_XML_PARAMNAME), selectionOptionsBuilder.getCustomFichothequeQueriesBuilder());
    }

    public void checkDefBuilder(DefBuilder defBuilder) throws ErrorMessageException {
        LabelChange labelChange = requestHandler.getLabelChange(TITLE_PARAMPREFIX);
        for (Label label : labelChange.getChangedLabels()) {
            defBuilder.putLabel(label);
        }
        String attributes = requestHandler.getTrimedParameter(ATTRIBUTES_PARAMNAME);
        if (!attributes.isEmpty()) {
            defBuilder.getAttributesBuilder().changeAttributes(AttributeParser.parse(attributes));
        }
    }

    public String getTargetName() throws ErrorMessageException {
        return requestHandler.getTrimedParameter(TARGETNAME_PARAMNAME);
    }

    public RelativePath getTargetPath() throws ErrorMessageException {
        String targetPathString = requestHandler.getTrimedParameter(TARGETPATH_PARAMNAME);
        if (targetPathString.isEmpty()) {
            return RelativePath.EMPTY;
        }
        try {
            return RelativePath.parse(targetPathString);
        } catch (ParseException pe) {
            throw BdfErrors.error("_ error.wrong.relativepath", targetPathString);
        }
    }

}
