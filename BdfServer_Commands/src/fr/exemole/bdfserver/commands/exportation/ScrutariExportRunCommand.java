/* BdfServer_Command - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.configuration.PathConfiguration;
import fr.exemole.bdfserver.tools.configuration.PathConfigurationBuilder;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.runners.ScrutariExportRunner;
import java.io.IOException;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariExportRunCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ScrutariExportRun";
    private ScrutariExportDef scrutariExportDef;

    public ScrutariExportRunCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return false;
    }

    @Override
    protected void doCommand() {
        PathConfiguration pathConfiguration = PathConfigurationBuilder.build(bdfServer);
        try {
            ScrutariExportRunner.run(scrutariExportDef, bdfServer, pathConfiguration);
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
        putResultObject(SCRUTARIEXPORTDEF_OBJ, scrutariExportDef);
        putResultObject(PATHCONFIGURATION_OBJ, pathConfiguration);
        setDone("_ done.exportation.scrutariexportrun");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        scrutariExportDef = requestHandler.getMandatoryScrutariExportDef();
    }

}
