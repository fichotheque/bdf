/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.table.TableExportDescription;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class TableExportRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TableExportRemove";
    public final static String COMMANDKEY = "_ EXP-12";
    private String tableExportName;

    public TableExportRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        bdfServer.getTableExportManager().removeTableExport(tableExportName, editOrigin);
        setDone("_ done.exportation.tableexportremove", tableExportName);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TableExportDescription tableExportDescription = requestHandler.getMandatoryTableExportDescription();
        if (!tableExportDescription.isEditable()) {
            throw BdfErrors.unsupportedNotEditableParameterValue(InteractionConstants.TABLEEXPORT_PARAMNAME, tableExportDescription.getName());
        }
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        this.tableExportName = tableExportDescription.getName();
    }

}
