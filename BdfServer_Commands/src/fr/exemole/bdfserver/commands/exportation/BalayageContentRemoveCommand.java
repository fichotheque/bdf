/* BdfServer_Commands - Copyright (c) 2019-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class BalayageContentRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "BalayageContentRemove";
    public final static String COMMANDKEY = "_ EXP-55";
    private String balayageName;
    private String contentPath;

    public BalayageContentRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        BalayageDescription balayageDescription = bdfServer.getBalayageManager().removeBalayageContent(balayageName, contentPath, editOrigin);
        if (balayageDescription != null) {
            putResultObject(BdfInstructionConstants.BALAYAGEDESCRIPTION_OBJ, balayageDescription);
            setDone("_ done.exportation.balayagecontentremove", balayageName, contentPath);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        BalayageDescription balayageDescription = requestHandler.getMandatoryBalayageDescription();
        this.balayageName = balayageDescription.getName();
        this.contentPath = requestHandler.getMandatoryPath();
    }

}
