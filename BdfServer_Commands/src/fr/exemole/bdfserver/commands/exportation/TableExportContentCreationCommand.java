/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.exportation.table.FicheTableParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.api.managers.TableExportManager;
import fr.exemole.bdfserver.tools.exportation.table.DefaultTableDefFactory;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.EditOrigin;
import net.fichotheque.Subset;
import net.fichotheque.SubsetKey;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.exportation.table.TableDef;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.thesaurus.Thesaurus;
import net.fichotheque.utils.TableExportUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 * Note : cette commande crée à partir d'un corpus ou d'un thésaurus. On peut
 * créer un contenu directement avec du texte avec
 * TableExportContentChangeCommand
 *
 * @author Vincent Calame
 */
public class TableExportContentCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TableExportContentCreation";
    public final static String CORPUS_COMMANDKEY = "_ EXP-14";
    public final static String THESAURUS_COMMANDKEY = "_ EXP-15";
    private String tableExportName;
    private FicheTableParameters ficheTableParameters;
    private Subset subset;

    public TableExportContentCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        TableExportManager tableExportManager = bdfServer.getTableExportManager();
        SubsetKey subsetKey = subset.getSubsetKey();
        TableDef tableDef;
        if (subset instanceof Corpus) {
            tableDef = DefaultTableDefFactory.fromUi(bdfServer, (Corpus) subset, ficheTableParameters, getPermissionSummary());
        } else {
            tableDef = DefaultTableDefFactory.fromThesaurusMetadata((Thesaurus) subset, bdfServer.getThesaurusLangChecker());
        }
        TableExportDescription tableExportDescription = tableExportManager.putTableDef(tableExportName, subsetKey, tableDef, editOrigin);
        putResultObject(TABLEEXPORTDESCRIPTION_OBJ, tableExportDescription);
        setDone("_ done.exportation.tableexportcontentcreation", tableExportName, subsetKey.getKeyString());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TableExportDescription tableExportDescription = requestHandler.getMandatoryTableExportDescription();
        if (!tableExportDescription.isEditable()) {
            throw BdfErrors.unsupportedNotEditableParameterValue(InteractionConstants.TABLEEXPORT_PARAMNAME, tableExportDescription.getName());
        }
        this.tableExportName = tableExportDescription.getName();
        subset = requestHandler.getMandatorySubset();
        SubsetKey subsetKey = subset.getSubsetKey();
        String subsetString = subsetKey.toString();
        if ((!subsetKey.isCorpusSubset()) && (!subsetKey.isThesaurusSubset())) {
            throw BdfErrors.unsupportedParameterValue(InteractionConstants.SUBSET_PARAMNAME, subsetString);
        }
        if (TableExportUtils.getTableExportContentDescription(tableExportDescription, subsetString) != null) {
            throw BdfErrors.unsupportedNotEditableParameterValue(InteractionConstants.SUBSET_PARAMNAME, subsetString);
        }
        if (subset instanceof Corpus) {
            ficheTableParameters = requestHandler.getFicheTableParameters();
        }
    }

}
