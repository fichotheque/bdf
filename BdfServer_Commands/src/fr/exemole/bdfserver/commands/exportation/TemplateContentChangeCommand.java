/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class TemplateContentChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TemplateContentChange";
    public final static String COMMANDKEY = "_ EXP-35";
    public final static String CONTENT_PARAMNAME = "content";
    private TemplateKey templateKey;
    private String contentPath;
    private String content;

    public TemplateContentChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        TemplateDescription templateDescription;
        try (InputStream is = IOUtils.toInputStream(content, "UTF-8")) {
            templateDescription = bdfServer.getTransformationManager().putTemplateContent(templateKey, contentPath, is, editOrigin);

        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.templatecontent_io", ioe.getMessage());
        }
        if (templateDescription != null) {
            putResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ, templateDescription);
            setDone("_ done.exportation.templatecontentchange", templateKey.getKeyString(), contentPath);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TemplateDescription templateDescription = requestHandler.getMandatoryTemplateDescription();
        this.templateKey = templateDescription.getTemplateKey();
        this.contentPath = requestHandler.getMandatoryPath();
        this.content = getMandatory(CONTENT_PARAMNAME);
    }

}
