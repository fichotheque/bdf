/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.utils.TableExportUtils;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 * Crée le contenu au besoin
 *
 * @author Vincent Calame
 */
public class TableExportContentChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TableExportContentChange";
    public final static String COMMANDKEY = "_ EXP-17";
    public final static String CONTENT_PARAMNAME = "content";
    private String tableExportName;
    private String contentPath;
    private String content;

    public TableExportContentChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        TableExportDescription tableExportDescription;
        try (InputStream is = IOUtils.toInputStream(content, "UTF-8")) {
            tableExportDescription = bdfServer.getTableExportManager().putTableExportContent(tableExportName, contentPath, is, editOrigin);
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.tableexportcontent_io", ioe.getMessage());
        }
        if (tableExportDescription != null) {
            putResultObject(TABLEEXPORTDESCRIPTION_OBJ, tableExportDescription);
            setDone("_ done.exportation.tableexportcontentchange", tableExportName, contentPath);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TableExportDescription tableExportDescription = requestHandler.getMandatoryTableExportDescription();
        if (!tableExportDescription.isEditable()) {
            throw BdfErrors.unsupportedNotEditableParameterValue(InteractionConstants.TABLEEXPORT_PARAMNAME, tableExportDescription.getName());
        }
        this.tableExportName = tableExportDescription.getName();
        this.contentPath = requestHandler.getMandatoryPath();
        if (!TableExportUtils.isContentEditable(tableExportDescription, contentPath)) {
            throw BdfErrors.unsupportedNotEditableParameterValue(InteractionConstants.PATH_PARAMNAME, contentPath);
        }
        this.content = getMandatory(CONTENT_PARAMNAME);
    }

}
