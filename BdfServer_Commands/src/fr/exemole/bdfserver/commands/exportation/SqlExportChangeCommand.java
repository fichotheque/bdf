/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.SqlExportManager;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.fichotheque.tools.exportation.sql.SqlExportDefBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class SqlExportChangeCommand extends AbstractDefChangeCommand {

    public final static String COMMANDNAME = "SqlExportChange";
    public final static String COMMANDKEY = "_ EXP-43";
    public final static String CUSTOMCLASSNAME_PARAMNAME = "customclassname";
    public final static String PARAMS_PARAMNAME = "params";
    public final static String FILENAME_PARAMNAME = "filename";
    public final static String TABLEEXPORTNAME_PARAMNAME = "tableexportname";
    public final static String POSTCOMMAND_PARAMNAME = "postcommand";

    private SqlExportDef sqlExportDef;

    public SqlExportChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        SqlExportManager sqlExportManager = bdfServer.getSqlExportManager();
        sqlExportManager.putSqlExportDef(sqlExportDef);
        putResultObject(SQLEXPORTDEF_OBJ, sqlExportDef);
        setDone("_ done.exportation.sqlexportchange", sqlExportDef.getName());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        SqlExportDef currentSqlExportDef = requestHandler.getMandatorySqlExportDef();
        String sqlExportClassName = getMandatory(CUSTOMCLASSNAME_PARAMNAME);
        String tableExportName = getMandatory(TABLEEXPORTNAME_PARAMNAME);
        String postCommand = getMandatory(POSTCOMMAND_PARAMNAME);
        String fileName = getMandatory(FILENAME_PARAMNAME);
        String params = getMandatory(PARAMS_PARAMNAME);
        SqlExportDefBuilder sqlExportDefBuilder = SqlExportDefBuilder.init(currentSqlExportDef.getName(), currentSqlExportDef.getAttributes())
                .setSqlExportClassName(sqlExportClassName)
                .setPostCommand(postCommand)
                .setTableExportName(tableExportName)
                .setTargetName(getTargetName())
                .setTargetPath(getTargetPath());
        try {
            sqlExportDefBuilder.setFileName(fileName);
        } catch (ParseException pe) {
            throw BdfErrors.error("_ error.wrong.filename", fileName);
        }
        String[] tokens = StringUtils.getLineTokens(params, StringUtils.EMPTY_EXCLUDE);
        for (String token : tokens) {
            int idx = token.indexOf('=');
            if (idx == -1) {
                continue;
            }
            String name = token.substring(0, idx).trim();
            if (name.length() > 0) {
                sqlExportDefBuilder.addParam(name, token.substring(idx + 1));
            }
        }
        checkSelectionOptions(sqlExportDefBuilder.getSelectionOptionsBuilder());
        checkDefBuilder(sqlExportDefBuilder);
        this.sqlExportDef = sqlExportDefBuilder.toSqlExportDef();
    }

}
