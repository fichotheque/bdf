/* BdfServer_Commands - Copyright (c) 2021-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.TemplateContentDescription;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.FileValue;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class TemplateBinaryUpdateCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TemplateBinaryUpdateCommand";
    public final static String COMMANDKEY = "_ EXP-36";
    public final static String FILE_PARAMNAME = "file";
    private TemplateKey templateKey;
    private String path;

    public TemplateBinaryUpdateCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        FileValue fileValue = requestHandler.getRequestMap().getFileValue(FILE_PARAMNAME);
        if (fileValue == null) {
            throw BdfErrors.emptyMandatoryParameter(FILE_PARAMNAME);
        }
        if (fileValue.length() < 2) {
            fileValue.free();
            throw BdfErrors.error("_ error.empty.file");
        }
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        try (InputStream is = fileValue.getInputStream()) {
            TemplateDescription templateDescription = bdfServer.getTransformationManager().putTemplateContent(templateKey, path, is, editOrigin);
            if (templateDescription != null) {
                putResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ, templateDescription);
                putResultObject(BdfInstructionConstants.TEMPLATECONTENTDESCRIPTION_OBJ, templateDescription.getTemplateContentDescription(path));
                setDone("_ done.exportation.templatebinaryupdate");
            }
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.templatecontent_io", ioe.getMessage());
        } finally {
            fileValue.free();
        }

    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TemplateDescription templateDescription = requestHandler.getMandatoryTemplateDescription();
        TemplateContentDescription templateContentDescription = requestHandler.getMandatoryTemplateContentDescription(templateDescription);
        this.templateKey = templateContentDescription.getTemplateKey();
        this.path = templateContentDescription.getPath();
    }

}
