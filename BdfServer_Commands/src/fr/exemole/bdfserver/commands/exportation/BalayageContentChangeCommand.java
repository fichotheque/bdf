/* BdfServer_Commands - Copyright (c) 2019-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class BalayageContentChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "BalayageContentChange";
    public final static String COMMANDKEY = "_ EXP-56";
    public final static String CONTENT_PARAMNAME = "content";
    private String balayageName;
    private String contentPath;
    private String content;

    public BalayageContentChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        BalayageDescription balayageDescription;
        try (InputStream is = IOUtils.toInputStream(content, "UTF-8");) {
            balayageDescription = bdfServer.getBalayageManager().putBalayageContent(balayageName, contentPath, is, editOrigin);

        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.balayagecontent_io", ioe.getMessage());
        }
        if (balayageDescription != null) {
            putResultObject(BdfInstructionConstants.BALAYAGEDESCRIPTION_OBJ, balayageDescription);
            setDone("_ done.exportation.balayagecontentchange", balayageName, contentPath);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        BalayageDescription balayageDescription = requestHandler.getMandatoryBalayageDescription();
        this.balayageName = balayageDescription.getName();
        this.contentPath = requestHandler.getMandatoryPath();
        this.content = getMandatory(CONTENT_PARAMNAME);
    }

}
