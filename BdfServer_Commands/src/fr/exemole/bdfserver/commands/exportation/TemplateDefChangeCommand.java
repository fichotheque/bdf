/* BdfServer_Commands - Copyright (c) 2016-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.managers.TransformationManager;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.TemplateDef;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.tools.exportation.transformation.TemplateDefBuilder;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public class TemplateDefChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TemplateDefChange";
    public final static String COMMANDKEY = "_ EXP-33";
    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    private TemplateDef templateDef;

    public TemplateDefChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        TransformationManager transformationManager = bdfServer.getTransformationManager();
        transformationManager.putTemplateDef(templateDef, editOrigin);
        TemplateKey templateKey = templateDef.getTemplateKey();
        TemplateDescription templateDescription = transformationManager.updateTemplateDescription(templateKey);
        putResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ, templateDescription);
        setDone("_ done.exportation.templatedefchange", templateKey.getKeyString());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TemplateDescription templateDescription = requestHandler.getMandatoryTemplateDescription();
        TemplateDefBuilder templateDefBuilder = new TemplateDefBuilder(templateDescription.getTemplateKey(), templateDescription.getTemplateDef().getAttributes());
        LabelChange labelChange = requestHandler.getLabelChange(TITLE_PARAMPREFIX);
        for (Label label : labelChange.getChangedLabels()) {
            templateDefBuilder.putLabel(label);
        }
        String attributes = requestHandler.getTrimedParameter(ATTRIBUTES_PARAMNAME);
        if (!attributes.isEmpty()) {
            templateDefBuilder.getAttributesBuilder().changeAttributes(AttributeParser.parse(attributes));
        }
        this.templateDef = templateDefBuilder.toTemplateDef();
    }

}
