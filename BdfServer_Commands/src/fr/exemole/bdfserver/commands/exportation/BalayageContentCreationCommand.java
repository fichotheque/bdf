/* BdfServer_Commands - Copyright (c) 2019-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.fichotheque.utils.BalayageUtils;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class BalayageContentCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "BalayageContentCreation";
    public final static String COMMANDKEY = "_ EXP-54";
    public final static String SUBDIR_PARAMNAME = "subdir";
    public final static String NEWCONTENT_PARAMNAME = "newcontent";
    private String balayageName;
    private String subdir;
    private String newContentName;


    public BalayageContentCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        String contentPath;
        if (subdir.length() > 0) {
            contentPath = subdir + "/" + newContentName;
        } else {
            contentPath = newContentName;
        }
        BalayageDescription balayageDescription;
        try (InputStream is = IOUtils.toInputStream(BalayageUtils.getDefaultContent(newContentName, subdir), "UTF-8");) {
            balayageDescription = bdfServer.getBalayageManager().putBalayageContent(balayageName, contentPath, is, editOrigin);
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.balayagecontent_io", ioe.getMessage());
        }
        if (balayageDescription != null) {
            putResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ, balayageDescription);
            setDone("_ done.exportation.balayagecontentcreation", balayageName, contentPath);
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        BalayageDescription balayageDescription = requestHandler.getMandatoryBalayageDescription();
        this.balayageName = balayageDescription.getName();
        this.subdir = getMandatory(SUBDIR_PARAMNAME);
        if (!isValidSubdir(subdir)) {
            throw BdfErrors.unknownParameterValue(SUBDIR_PARAMNAME, subdir);
        }
        this.newContentName = getMandatory(NEWCONTENT_PARAMNAME).trim();
        if (this.newContentName.isEmpty()) {
            throw BdfErrors.error("_ errror.empty.balayage.contentname");
        } else if (!BalayageUtils.isValidContentName(newContentName, subdir)) {
            throw BdfErrors.error("_ error.wrong.balayage.contentname");
        }
    }

    private static boolean isValidSubdir(String subdir) {
        switch (subdir) {
            case "":
            case "extraction":
            case "xslt":
                return true;
            default:
                return false;
        }
    }

}
