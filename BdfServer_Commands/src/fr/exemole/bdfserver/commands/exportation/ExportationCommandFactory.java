/* BdfServer_Commands - Copyright (c) 2012-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class ExportationCommandFactory {

    private ExportationCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case AccessCreationCommand.COMMANDNAME:
                return new AccessCreationCommand(bdfServer, requestMap);
            case AccessChangeCommand.COMMANDNAME:
                return new AccessChangeCommand(bdfServer, requestMap);
            case AccessRemoveCommand.COMMANDNAME:
                return new AccessRemoveCommand(bdfServer, requestMap);
            case TableExportCreationCommand.COMMANDNAME:
                return new TableExportCreationCommand(bdfServer, requestMap);
            case TemplateCreationCommand.COMMANDNAME:
                return new TemplateCreationCommand(bdfServer, requestMap);
            case ScrutariExportCreationCommand.COMMANDNAME:
                return new ScrutariExportCreationCommand(bdfServer, requestMap);
            case ScrutariExportChangeCommand.COMMANDNAME:
                return new ScrutariExportChangeCommand(bdfServer, requestMap);
            case ScrutariExportRemoveCommand.COMMANDNAME:
                return new ScrutariExportRemoveCommand(bdfServer, requestMap);
            case ScrutariExportRunCommand.COMMANDNAME:
                return new ScrutariExportRunCommand(bdfServer, requestMap);
            case SqlExportCreationCommand.COMMANDNAME:
                return new SqlExportCreationCommand(bdfServer, requestMap);
            case SqlExportChangeCommand.COMMANDNAME:
                return new SqlExportChangeCommand(bdfServer, requestMap);
            case SqlExportRemoveCommand.COMMANDNAME:
                return new SqlExportRemoveCommand(bdfServer, requestMap);
            case SqlExportRunCommand.COMMANDNAME:
                return new SqlExportRunCommand(bdfServer, requestMap);
            case BalayageCreationCommand.COMMANDNAME:
                return new BalayageCreationCommand(bdfServer, requestMap);
            case BalayageRemoveCommand.COMMANDNAME:
                return new BalayageRemoveCommand(bdfServer, requestMap);
            case BalayageDefChangeCommand.COMMANDNAME:
                return new BalayageDefChangeCommand(bdfServer, requestMap);
            case BalayageContentCreationCommand.COMMANDNAME:
                return new BalayageContentCreationCommand(bdfServer, requestMap);
            case BalayageContentChangeCommand.COMMANDNAME:
                return new BalayageContentChangeCommand(bdfServer, requestMap);
            case BalayageContentRemoveCommand.COMMANDNAME:
                return new BalayageContentRemoveCommand(bdfServer, requestMap);
            case BalayageRunCommand.COMMANDNAME:
                return new BalayageRunCommand(bdfServer, requestMap);
            case TableExportDefChangeCommand.COMMANDNAME:
                return new TableExportDefChangeCommand(bdfServer, requestMap);
            case TableExportContentChangeCommand.COMMANDNAME:
                return new TableExportContentChangeCommand(bdfServer, requestMap);
            case TableExportContentRemoveCommand.COMMANDNAME:
                return new TableExportContentRemoveCommand(bdfServer, requestMap);
            case TableExportContentCreationCommand.COMMANDNAME:
                return new TableExportContentCreationCommand(bdfServer, requestMap);
            case TableExportRemoveCommand.COMMANDNAME:
                return new TableExportRemoveCommand(bdfServer, requestMap);
            case TableExportCopyCommand.COMMANDNAME:
                return new TableExportCopyCommand(bdfServer, requestMap);
            case TemplateContentChangeCommand.COMMANDNAME:
                return new TemplateContentChangeCommand(bdfServer, requestMap);
            case TemplateRemoveCommand.COMMANDNAME:
                return new TemplateRemoveCommand(bdfServer, requestMap);
            case TemplateContentRemoveCommand.COMMANDNAME:
                return new TemplateContentRemoveCommand(bdfServer, requestMap);
            case TemplateExtractionReinitCommand.COMMANDNAME:
                return new TemplateExtractionReinitCommand(bdfServer, requestMap);
            case TemplateDefChangeCommand.COMMANDNAME:
                return new TemplateDefChangeCommand(bdfServer, requestMap);
            case TemplateBinaryUpdateCommand.COMMANDNAME:
                return new TemplateBinaryUpdateCommand(bdfServer, requestMap);
            case TemplateDuplicateCommand.COMMANDNAME:
                return new TemplateDuplicateCommand(bdfServer, requestMap);
            case TemplateConvertToOdtCommand.COMMANDNAME:
                return new TemplateConvertToOdtCommand(bdfServer, requestMap);
        }
        return null;
    }

}
