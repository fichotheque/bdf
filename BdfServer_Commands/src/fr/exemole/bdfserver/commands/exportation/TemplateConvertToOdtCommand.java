/* BdfServer_Commands - Copyright (c) 2023-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import static fr.exemole.bdfserver.api.instruction.BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.exportation.transformation.ConvertToOdtEngine;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class TemplateConvertToOdtCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TemplateConvertToOdt";
    public final static String COMMANDKEY = "_ EXP-38";
    private ConvertToOdtEngine engine;


    public TemplateConvertToOdtCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        try {
            TemplateDescription templateDescription = engine.run(editOrigin);
            putResultObject(TEMPLATEDESCRIPTION_OBJ, templateDescription);
            setDone("_ done.exportation.templateconverttoodt", templateDescription.getTemplateKey().getKeyString());
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.templatecontent_io", ioe.getMessage());
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TemplateDescription originTemplateDescription = requestHandler.getMandatoryTemplateDescription();
        this.engine = ConvertToOdtEngine.build(bdfServer, originTemplateDescription);
    }

}
