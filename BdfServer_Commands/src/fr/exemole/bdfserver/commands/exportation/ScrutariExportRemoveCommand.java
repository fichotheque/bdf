/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.ScrutariExportManager;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.exportation.scrutari.ScrutariExportDef;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariExportRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "ScrutariExportRemove";
    public final static String COMMANDKEY = "_ EXP-22";
    private String name;

    public ScrutariExportRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        ScrutariExportManager scrutariExportManager = bdfServer.getScrutariExportManager();
        scrutariExportManager.removeScrutariExportDef(name);
        setDone("_ done.exportation.scrutariexportremove", name);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        ScrutariExportDef scrutariExportDef = requestHandler.getMandatoryScrutariExportDef();
        name = scrutariExportDef.getName();
    }

}
