/* BdfServer_Commands - Copyright (c) 2019-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.managers.BalayageManager;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.balayage.BalayageDescription;
import net.fichotheque.utils.BalayageUtils;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class BalayageCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "BalayageCreation";
    public final static String COMMANDKEY = "_ EXP-51";
    public final static String NEWBALAYAGE_PARAMNAME = "newbalayage";
    private String newBalayageName;

    public BalayageCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        String defaultName = "balayage.xml";
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        BalayageDescription balayageDescription = null;
        try (InputStream is = IOUtils.toInputStream(BalayageUtils.getDefaultContent(defaultName, ""), "UTF-8");) {
            balayageDescription = bdfServer.getBalayageManager().putBalayageContent(newBalayageName, defaultName, is, editOrigin);
        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.balayagecontent_io", ioe.getMessage());
        }
        putResultObject(BdfInstructionConstants.BALAYAGEDESCRIPTION_OBJ, balayageDescription);
        setDone("_ done.exportation.balayagecreation", newBalayageName);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        newBalayageName = getMandatory(NEWBALAYAGE_PARAMNAME).trim();
        if (newBalayageName.length() == 0) {
            throw BdfErrors.error("_ error.empty.balayagename");
        }
        if (!StringUtils.isTechnicalName(newBalayageName, true)) {
            throw BdfErrors.error("_ error.wrong.balayagename", newBalayageName);
        }
        if (containsBalayageDescription(bdfServer.getBalayageManager(), newBalayageName)) {
            throw BdfErrors.error("_ error.existing.balayage", newBalayageName);
        }
    }

    private static boolean containsBalayageDescription(BalayageManager manager, String balayageName) {
        for (BalayageDescription balayageDescription : manager.getBalayageDescriptionList()) {
            if (balayageDescription.getName().equals(balayageName)) {
                return true;
            }
        }
        return false;
    }

}
