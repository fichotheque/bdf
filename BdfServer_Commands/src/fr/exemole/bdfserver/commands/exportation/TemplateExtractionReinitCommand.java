/* BdfServer_Commands - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfInstructionConstants;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.tools.exportation.transformation.DefaultExtractionDefFactory;
import fr.exemole.bdfserver.tools.exportation.transformation.defaulttemplates.DefaultTemplateUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.io.IOException;
import java.io.InputStream;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.transformation.TemplateDescription;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.extraction.def.ExtractionDef;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class TemplateExtractionReinitCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TemplateExtractionReinit";
    public final static String COMPACTSTYLE_PARAMNAME = "compactstyle";
    private TemplateKey templateKey;
    private ExtractionDef extractionDef;
    private boolean compactStyle;

    public TemplateExtractionReinitCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        String extractionContent = DefaultTemplateUtils.getExtractionDefXML(extractionDef, compactStyle);
        TemplateDescription templateDescription;
        try (InputStream is = IOUtils.toInputStream(extractionContent, "UTF-8")) {
            templateDescription = bdfServer.getTransformationManager().putTemplateContent(templateKey, "extraction.xml", is, editOrigin);

        } catch (IOException ioe) {
            throw BdfErrors.error("_ error.exception.templatecontent_io", ioe.getMessage());
        }
        if (templateDescription != null) {
            putResultObject(BdfInstructionConstants.TEMPLATEDESCRIPTION_OBJ, templateDescription);
            setDone("_ done.exportation.templatecontentchange", templateKey.getKeyString(), "extraction.xml");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TemplateDescription templateDescription = requestHandler.getMandatoryTemplateDescription();
        this.templateKey = templateDescription.getTemplateKey();
        this.extractionDef = DefaultExtractionDefFactory.newInstance(bdfServer, templateKey.getTransformationKey(), templateDescription.getTemplateDef().getAttributes());
        if (extractionDef == null) {
            throw BdfErrors.unsupportedNotEditableParameterValue(InteractionConstants.TEMPLATE_PARAMNAME, templateKey + "/extraction.xml");
        }
        this.compactStyle = requestHandler.isTrue(COMPACTSTYLE_PARAMNAME);
    }

}
