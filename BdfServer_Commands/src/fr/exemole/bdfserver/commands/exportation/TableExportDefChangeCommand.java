/* BdfServer_Commands - Copyright (c) 2016-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.EditOrigin;
import net.fichotheque.exportation.table.TableExportDef;
import net.fichotheque.exportation.table.TableExportDescription;
import net.fichotheque.tools.exportation.table.TableExportDefBuilder;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class TableExportDefChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "TableExportDefChange";
    public final static String COMMANDKEY = "_ EXP-13";
    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    public final static String LANGMODE_PARAMNAME = "langmode";
    public final static String LANGLIST_PARAMNAME = "langlist";
    private TableExportDef tableExportDef;

    public TableExportDefChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        EditOrigin editOrigin = bdfUser.newEditOrigin(Domains.EXPORTATION, COMMANDNAME);
        TableExportDescription tableExportDescription = bdfServer.getTableExportManager().putTableExportDef(tableExportDef, editOrigin);
        putResultObject(TABLEEXPORTDESCRIPTION_OBJ, tableExportDescription);
        setDone("_ done.exportation.tableexportdefchange", tableExportDef.getName());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        TableExportDescription tableExportDescription = requestHandler.getMandatoryTableExportDescription();
        if (!tableExportDescription.isEditable()) {
            throw BdfErrors.unsupportedNotEditableParameterValue(InteractionConstants.TABLEEXPORT_PARAMNAME, tableExportDescription.getName());
        }
        TableExportDefBuilder tableExportDefBuilder = new TableExportDefBuilder(tableExportDescription.getName(), tableExportDescription.getTableExportDef().getAttributes());
        LabelChange labelChange = requestHandler.getLabelChange(TITLE_PARAMPREFIX);
        for (Label label : labelChange.getChangedLabels()) {
            tableExportDefBuilder.putLabel(label);
        }
        String attributes = requestHandler.getTrimedParameter(ATTRIBUTES_PARAMNAME);
        if (!attributes.isEmpty()) {
            tableExportDefBuilder.getAttributesBuilder().changeAttributes(AttributeParser.parse(attributes));
        }
        String langMode = checkLangMode(requestHandler.getTrimedParameter(LANGMODE_PARAMNAME));
        if ((langMode != null) && (langMode.equals(TableExportDescription.CUSTOMLIST_LANGMODE))) {
            String langListString = getMandatory(LANGLIST_PARAMNAME);
            String[] tokens = StringUtils.getTechnicalTokens(langListString, true);
            boolean done = false;
            for (String token : tokens) {
                try {
                    Lang lang = Lang.parse(token);
                    done = true;
                    tableExportDefBuilder.addLang(lang);
                } catch (ParseException mce) {
                }
            }
            if (!done) {
                langMode = null;
            }
        }
        tableExportDefBuilder.setLangMode(langMode);
        this.tableExportDef = tableExportDefBuilder.toTableExportDef();
    }

    private static String checkLangMode(String langMode) {
        switch (langMode) {
            case TableExportDescription.WORKING_LANGMODE:
            case TableExportDescription.SUPPLEMENTARY_LANGMODE:
            case TableExportDescription.CUSTOMLIST_LANGMODE:
                return langMode;
            default:
                return null;
        }
    }

}
