/* BdfServer_Commands - Copyright (c) 2012-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.exportation;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.exportation.sql.SqlExportDef;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class SqlExportRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "SqlExportRemove";
    public final static String COMMANDKEY = "_ EXP-42";
    private String name;

    public SqlExportRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() {
        bdfServer.getSqlExportManager().removeSqlExportDef(name);
        setDone("_ done.exportation.sqlexportremove", name);

    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        SqlExportDef sqlExportDef = requestHandler.getMandatorySqlExportDef();
        this.name = sqlExportDef.getName();
    }

}
