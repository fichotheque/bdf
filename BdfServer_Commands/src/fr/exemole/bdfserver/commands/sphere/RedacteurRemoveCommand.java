/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.BdfServerConstants;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.managers.PermissionManager;
import fr.exemole.bdfserver.api.session.SessionObserver;
import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurRemove";
    public final static String COMMANDKEY = "_ SPH-06";
    private Redacteur redacteur;

    public RedacteurRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        String redacteurString = redacteur.getBracketStyle();
        Sphere sphere = redacteur.getSphere();
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            session.getFichothequeEditor().getSphereEditor(sphere.getSubsetKey()).removeRedacteur(redacteur);
        }
        putResultObject(SPHERE_OBJ, sphere);
        setDone("_ done.sphere.redacteurremove", redacteurString);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        redacteur = requestHandler.getMandatoryRedacteur();
        checkSubsetAdmin(redacteur.getSphere());
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        if (!isRemoveable(bdfServer.getPermissionManager(), redacteur)) {
            throw BdfErrors.error("_ error.notremoveable.redacteur", redacteur.getBracketStyle());
        }
        if (isRedacteurInSession(redacteur)) {
            throw BdfErrors.error("_ error.unsupported.redacteurinsesssion", redacteur.getBracketStyle());
        }
    }

    private boolean isRemoveable(PermissionManager permissionManager, Redacteur redacteur) {
        if (!bdfServer.getFichotheque().isRemoveable(redacteur)) {
            return false;
        }
        if ((permissionManager.isAdmin(redacteur))) {
            if (permissionManager.getAdminRedacteurList().size() == 1) {
                return false;
            }
        }
        return true;
    }

    private boolean isRedacteurInSession(Redacteur redacteur) {
        SessionObserver sessionObserver = (SessionObserver) bdfServer.getContextObject(BdfServerConstants.SESSIONOBSERVER_CONTEXTOBJECT);
        BdfUser[] array = sessionObserver.getCurrentBdfUserArray();
        int length = array.length;
        for (int i = 0; i < length; i++) {
            if (BdfUserUtils.isSame(bdfUser, redacteur)) {
                return true;
            }
        }
        return false;
    }

}
