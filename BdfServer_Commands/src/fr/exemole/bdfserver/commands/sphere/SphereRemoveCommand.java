/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.NoRemoveableSubsetException;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class SphereRemoveCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "SphereRemove";
    public final static String COMMANDKEY = "_ SPH-03";
    private Sphere sphere;

    public SphereRemoveCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        SubsetKey sphereKey = sphere.getSubsetKey();
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            try {
                session.getFichothequeEditor().removeSphere(sphere);
            } catch (NoRemoveableSubsetException nrse) {
                throw new ShouldNotOccurException("test done before");
            }
        }
        putResultObject(SUBSETKEY_OBJ, sphereKey);
        setDone("_ done.sphere.sphereremove", sphereKey.getSubsetName());
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        sphere = requestHandler.getMandatorySphere();
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        if (!sphere.isRemoveable()) {
            throw BdfErrors.error("_ error.notremoveable.sphere");
        }
    }

}
