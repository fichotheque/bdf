/* BdfServer_Commands - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public final class SphereCommandFactory {

    private SphereCommandFactory() {
    }

    public static BdfCommand getBdfCommand(BdfServer bdfServer, RequestMap requestMap, String commandName) {
        switch (commandName) {
            case RedacteurStatusCommand.COMMANDNAME:
                return new RedacteurStatusCommand(bdfServer, requestMap);
            case RedacteurChangeCommand.COMMANDNAME:
                return new RedacteurChangeCommand(bdfServer, requestMap);
            case RedacteurCreationCommand.COMMANDNAME:
                return new RedacteurCreationCommand(bdfServer, requestMap);
            case RedacteurRemoveCommand.COMMANDNAME:
                return new RedacteurRemoveCommand(bdfServer, requestMap);
            case RedacteurReplaceCommand.COMMANDNAME:
                return new RedacteurReplaceCommand(bdfServer, requestMap);
            case SphereCreationCommand.COMMANDNAME:
                return new SphereCreationCommand(bdfServer, requestMap);
            case SphereRemoveCommand.COMMANDNAME:
                return new SphereRemoveCommand(bdfServer, requestMap);
            case SpherePhrasesCommand.COMMANDNAME:
                return new SpherePhrasesCommand(bdfServer, requestMap);
            case RedacteurRoleCommand.COMMANDNAME:
                return new RedacteurRoleCommand(bdfServer, requestMap);
            case RedacteurLangCommand.COMMANDNAME:
                return new RedacteurLangCommand(bdfServer, requestMap);
            case RedacteurTemplatesCommand.COMMANDNAME:
                return new RedacteurTemplatesCommand(bdfServer, requestMap);
            case RedacteurPasswordCommand.COMMANDNAME:
                return new RedacteurPasswordCommand(bdfServer, requestMap);
            case SphereAttributeChangeCommand.COMMANDNAME:
                return new SphereAttributeChangeCommand(bdfServer, requestMap);
            case RedacteurAttributeChangeCommand.COMMANDNAME:
                return new RedacteurAttributeChangeCommand(bdfServer, requestMap);
            case LoginChangeCommand.COMMANDNAME:
                return new LoginChangeCommand(bdfServer, requestMap);
            case RedacteurDefaultTableExportParametersCommand.COMMANDNAME:
                return new RedacteurDefaultTableExportParametersCommand(bdfServer, requestMap);
            case RedacteurCustomizeUICommand.COMMANDNAME:
                return new RedacteurCustomizeUICommand(bdfServer, requestMap);
            case UserPrefChangeCommand.COMMANDNAME:
                return new UserPrefChangeCommand(bdfServer, requestMap);
            case UserStoreCommand.COMMANDNAME:
                return new UserStoreCommand(bdfServer, requestMap);
            default:
                return null;
        }
    }

}
