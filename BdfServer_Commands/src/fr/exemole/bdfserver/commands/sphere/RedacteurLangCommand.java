/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import fr.exemole.bdfserver.api.users.BdfUserPrefsEditor;
import fr.exemole.bdfserver.tools.configuration.ConfigurationUtils;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.text.ParseException;
import java.util.Locale;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.LangPreferenceBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurLangCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurLang";
    public final static String COMMANDKEY = "_ USR-02";
    public final static String WORKINGLANG_PARAMNAME = "workinglang";
    public final static String CUSTOMFORMATLOCALE_PARAMNAME = "customformatlocale";
    public final static String CUSTOMLANGPREFERENCE_PARAMNAME = "customlangpreference";
    private Lang workingLang;
    private Locale customFormatLocale;
    private LangPreference customLangPreference;
    private BdfUserPrefs redacBdfUserPrefs;

    public RedacteurLangCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            BdfUserPrefsEditor bdfUserPrefsEditor = session.getBdfServerEditor().getBdfUserPrefsEditor(redacBdfUserPrefs);
            bdfUserPrefsEditor.setWorkingLang(workingLang);
            bdfUserPrefsEditor.setCustomFormatLocale(customFormatLocale);
            bdfUserPrefsEditor.setCustomLangPreference(customLangPreference);
        }
        setDone("_ done.sphere.redacteurlang");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        Redacteur redacteur = requestHandler.getMandatoryRedacteur();
        boolean isSame = BdfUserUtils.isSame(bdfUser, redacteur);
        if (!isSame) {
            checkSubsetAdmin(redacteur.getSphere());
        }
        if (isSame) {
            redacBdfUserPrefs = bdfUser.getPrefs();
        } else {
            redacBdfUserPrefs = bdfServer.createBdfUser(redacteur).getPrefs();
        }
        String workingLangParam = getMandatory(WORKINGLANG_PARAMNAME);
        try {
            workingLang = Lang.parse(workingLangParam);
        } catch (ParseException pe) {
            throw BdfErrors.wrongParameterValue(WORKINGLANG_PARAMNAME, workingLangParam);
        }
        if (!ConfigurationUtils.containsWorkingLang(bdfServer.getLangConfiguration(), workingLang)) {
            throw BdfErrors.unknownParameterValue(WORKINGLANG_PARAMNAME, workingLangParam);
        }
        String formatLocaleString = getMandatory(CUSTOMFORMATLOCALE_PARAMNAME).trim();
        if (formatLocaleString.length() > 0) {
            try {
                Lang formatLang = Lang.parse(formatLocaleString);
                customFormatLocale = formatLang.toLocale();
            } catch (ParseException lie) {
                throw BdfErrors.error("_ error.wrong.customformatlocale", formatLocaleString);
            }
        }
        String preferenceString = getMandatory(CUSTOMLANGPREFERENCE_PARAMNAME).trim();
        if (preferenceString.length() > 0) {
            LangPreferenceBuilder langPreferenceBuilder = new LangPreferenceBuilder();
            String[] tokens = StringUtils.getTechnicalTokens(preferenceString, true);
            for (String token : tokens) {
                try {
                    Lang lang = Lang.parse(token);
                    langPreferenceBuilder.addLang(lang);
                } catch (ParseException pe) {

                }
            }
            if (!langPreferenceBuilder.isEmpty()) {
                customLangPreference = langPreferenceBuilder.toLangPreference();
            } else {
                throw BdfErrors.error("_ error.wrong.customlangpreference");
            }
        }
    }

}
