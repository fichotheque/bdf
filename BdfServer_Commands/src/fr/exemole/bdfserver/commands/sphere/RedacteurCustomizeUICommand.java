/* BdfServer_Commands - Copyright (c) 2020-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.namespaces.BdfUserSpace;
import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurCustomizeUICommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurCustomizeUI";
    public final static String COMMANDKEY = "_ USR-06";
    public final static String NOTAB_PARAMNAME = "notab";
    public final static String HIDEEMPTY_PARAMNAME = "hideempty";
    public final static String SYNTAXACTIVE_PARAMNAME = "syntaxactive";
    private BdfUserPrefs redacBdfUserPrefs;
    private boolean noTab;
    private boolean hideEmpty;
    private boolean syntaxActive;

    public RedacteurCustomizeUICommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            session.getBdfServerEditor().getBdfUserPrefsEditor(redacBdfUserPrefs)
                    .setBoolean(BdfUserSpace.NOTAB_KEY, noTab)
                    .setBoolean(BdfUserSpace.HIDEEMPTY_KEY, hideEmpty)
                    .setBoolean(BdfUserSpace.SYNTAXACTIVE_KEY, syntaxActive);
        }
        setDone("_ done.sphere.redacteurcustomizeui");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        Redacteur redacteur = requestHandler.getMandatoryRedacteur();
        boolean isSame = BdfUserUtils.isSame(bdfUser, redacteur);
        if (!isSame) {
            checkSubsetAdmin(redacteur.getSphere());
        }
        if (isSame) {
            redacBdfUserPrefs = bdfUser.getPrefs();
        } else {
            redacBdfUserPrefs = bdfServer.createBdfUser(redacteur).getPrefs();
        }
        this.noTab = requestHandler.isTrue(NOTAB_PARAMNAME);
        this.hideEmpty = requestHandler.isTrue(HIDEEMPTY_PARAMNAME);
        this.syntaxActive = requestHandler.isTrue(SYNTAXACTIVE_PARAMNAME);
    }

}
