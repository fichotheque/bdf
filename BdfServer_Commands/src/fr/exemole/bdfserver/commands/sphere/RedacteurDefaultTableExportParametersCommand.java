/* BdfServer_Commands - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.exportation.table.TableExportParameters;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.namespaces.BdfUserSpace;
import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import fr.exemole.bdfserver.api.users.BdfUserPrefsEditor;
import fr.exemole.bdfserver.tools.exportation.table.TableExportParametersBuilder;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.util.List;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurDefaultTableExportParametersCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurDefaultTableExportParameters";
    public final static String COMMANDKEY = "_ USR-05";
    public final static String STATUSSHEET_PARAMNAME = "statussheet";
    private BdfUserPrefs redacBdfUserPrefs;
    private TableExportParameters tableExportParameters;
    private boolean withStatusSheet;

    public RedacteurDefaultTableExportParametersCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            BdfUserPrefsEditor bdfUserPrefsEditor = session.getBdfServerEditor().getBdfUserPrefsEditor(redacBdfUserPrefs)
                    .setDefaultHeaderType(tableExportParameters.getHeaderType())
                    .putAttribute(AttributeBuilder.toAttribute(BdfUserSpace.TABLEEXPORT_PATTERNMODE_KEY, tableExportParameters.getDefaulFicheTableParameters().getPatternMode()));
            List<String> list = tableExportParameters.getDefaulFicheTableParameters().getWithList();
            boolean withDone = false;
            if (!list.isEmpty()) {
                AttributeBuilder builder = new AttributeBuilder(BdfUserSpace.TABLEEXPORT_WITH_KEY);
                for (String value : list) {
                    builder.addValue(value);
                }
                Attribute withAttribute = builder.toAttribute();
                if (withAttribute != null) {
                    withDone = true;
                    bdfUserPrefsEditor.putAttribute(withAttribute);
                }
            }
            if (!withDone) {
                bdfUserPrefsEditor.removeAttribute(BdfUserSpace.TABLEEXPORT_WITH_KEY);
            }
            if (withStatusSheet) {
                Attribute statussheetAttribute = AttributeBuilder.toAttribute(BdfUserSpace.TABLEEXPORT_STATUSSHEET_KEY, "1");
                bdfUserPrefsEditor.putAttribute(statussheetAttribute);
            } else {
                bdfUserPrefsEditor.removeAttribute(BdfUserSpace.TABLEEXPORT_STATUSSHEET_KEY);
            }
        }
        setDone("_ done.sphere.redacteurdefaulttableexportparameters");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        Redacteur redacteur = requestHandler.getMandatoryRedacteur();
        boolean isSame = BdfUserUtils.isSame(bdfUser, redacteur);
        if (!isSame) {
            checkSubsetAdmin(redacteur.getSphere());
        }
        if (isSame) {
            redacBdfUserPrefs = bdfUser.getPrefs();
        } else {
            redacBdfUserPrefs = bdfServer.createBdfUser(redacteur).getPrefs();
        }
        TableExportParametersBuilder builder = TableExportParametersBuilder.init()
                .setFicheTableParameters(requestHandler.getFicheTableParameters())
                .setHeaderType(requestHandler.getHeaderType());
        tableExportParameters = builder.toTableExportParameters();
        this.withStatusSheet = requestHandler.isTrue(STATUSSHEET_PARAMNAME);
    }

}
