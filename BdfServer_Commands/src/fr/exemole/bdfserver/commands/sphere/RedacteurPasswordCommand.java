/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.PasswordManager;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurPasswordCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurPassword";
    public final static String COMMANDKEY = "_ USR-03";
    public final static String PASSWORD1_PARAMNAME = "password1";
    public final static String PASSWORD2_PARAMNAME = "password2";
    private Redacteur redacteur;
    private String password;

    public RedacteurPasswordCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        PasswordManager passwordManager = bdfServer.getPasswordManager();
        passwordManager.setPassword(redacteur.getGlobalId(), password);
        setDone("_ done.sphere.passwordchange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        redacteur = requestHandler.getMandatoryRedacteur();
        checkSphereSupervisor(redacteur.getSubsetName());
        boolean isSame = BdfUserUtils.isSame(bdfUser, redacteur);
        if ((!isSame) || (!bdfServer.getPolicyManager().getUserAllow().isDataChangeAllowed())) {
            checkSubsetAdmin(redacteur.getSphere());
        }
        String mdp1 = getMandatory(PASSWORD1_PARAMNAME);
        String mdp2 = getMandatory(PASSWORD2_PARAMNAME);
        if (!mdp1.equals(mdp2)) {
            throw BdfErrors.error("_ error.wrong.password_different");
        }
        password = mdp1;
        if (password.length() == 0) {
            throw BdfErrors.error("_ error.empty.password");
        }
        if (password.length() < 4) {
            throw BdfErrors.error("_ error.wrong.password_tooshort");
        }
    }

}
