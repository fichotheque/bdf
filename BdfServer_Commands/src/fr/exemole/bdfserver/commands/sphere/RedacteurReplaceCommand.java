/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.util.List;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.corpus.CorpusEditor;
import net.fichotheque.corpus.FicheMeta;
import net.fichotheque.corpus.fiche.Fiche;
import net.fichotheque.corpus.fiche.FicheItem;
import net.fichotheque.corpus.fiche.Item;
import net.fichotheque.corpus.fiche.Personne;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.tools.corpus.CorpusTools;
import net.fichotheque.tools.corpus.FieldGenerationEngine;
import net.fichotheque.tools.parsers.FicheParser;
import net.fichotheque.tools.parsers.TypoParser;
import net.fichotheque.tools.sphere.RedacteurStats;
import net.fichotheque.utils.CorpusUtils;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.TypoOptions;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurReplaceCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurReplace";
    public final static String COMMANDKEY = "_ SPH-07";
    public final static String REPLACETYPE_PARAMNAME = "replacetype";
    public final static String OTHER_REPLACETYPE_PARAMVALUE = "other";
    public final static String LITTERAL_REPLACETYPE_PARAMVALUE = "litteral";
    public final static String OTHER_PARAMNAME = "other";
    public final static String LITTERAL_PARAMNAME = "litteral";
    private Redacteur redacteur;
    private boolean otherRedac = false;
    private Redacteur otherRedacteur = null;
    private FicheItem ficheItem = null;

    public RedacteurReplaceCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        RedacteurStats redacteurStats = new RedacteurStats(redacteur);
        int count = redacteurStats.getFicheCount();
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            FicheItem oldFicheItem = new Personne(redacteur.getGlobalId());
            FicheItem newFicheItem;
            if (otherRedacteur != null) {
                newFicheItem = new Personne(otherRedacteur.getGlobalId());
            } else {
                newFicheItem = ficheItem;
            }
            List<RedacteurStats.ByCorpusStats> byCorpusStatsList = redacteurStats.getByCorpusStatsList();
            for (RedacteurStats.ByCorpusStats byCorpusStats : byCorpusStatsList) {
                Corpus corpus = byCorpusStats.getCorpus();
                CorpusEditor corpusEditor = session.getFichothequeEditor().getCorpusEditor(corpus);
                List<RedacteurStats.ByFicheStats> list = byCorpusStats.getByFicheStatsList();
                CorpusUtils.FieldSelection fieldSelection = CorpusUtils.getPersonneFieldSelection(corpus);
                FieldGenerationEngine engine = BdfCommandUtils.buildEngine(this, corpus);
                for (RedacteurStats.ByFicheStats byFicheStats : list) {
                    int id = byFicheStats.getId();
                    FicheMeta ficheMeta = corpus.getFicheMetaById(id);
                    if (ficheMeta != null) {
                        Fiche fiche = corpus.getFiche(ficheMeta);
                        boolean done = CorpusUtils.replaceInField(fiche, fieldSelection, oldFicheItem, newFicheItem);
                        if (done) {
                            CorpusTools.saveFiche(corpusEditor, ficheMeta, fiche, engine, false);
                        }
                    }
                }
            }
        }
        setDone("_ done.sphere.redacteurreplace", count);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        redacteur = requestHandler.getMandatoryRedacteur();
        String type = getMandatory(REPLACETYPE_PARAMNAME);
        if (!checkConfirmation()) {
            throw BdfErrors.error("_ error.empty.confirmationcheck_remove");
        }
        try {
            otherRedac = replaceTypeToBoolean(type);
        } catch (IllegalArgumentException iae) {
            throw BdfErrors.unknownParameterValue(REPLACETYPE_PARAMNAME, type);
        }
        if (otherRedac) {
            String otherString = getMandatory(OTHER_PARAMNAME);
            otherString = otherString.trim();
            if (otherString.length() == 0) {
                throw BdfErrors.error("_ error.empty.input");
            }
            try {
                otherRedacteur = SphereUtils.parse(fichotheque, otherString, redacteur.getSubsetKey());
            } catch (SphereUtils.RedacteurLoginException rle) {
                throw BdfErrors.error("_ error.unknown.redacteur", otherString);
            }
        } else {
            String litteralString = getMandatory(LITTERAL_PARAMNAME);
            litteralString = litteralString.trim();
            if (litteralString.length() == 0) {
                throw BdfErrors.error("_ error.empty.input");
            }
            TypoOptions typoOptions = BdfUserUtils.getTypoOptions(bdfUser);
            Personne personne = FicheParser.parsePersonne(litteralString, typoOptions);
            if (personne != null) {
                ficheItem = personne;
            } else {
                ficheItem = new Item(TypoParser.parseTypo(litteralString, typoOptions));
            }
        }
    }

    public static boolean replaceTypeToBoolean(String replaceTypeValue) {
        if (replaceTypeValue.equals(OTHER_REPLACETYPE_PARAMVALUE)) {
            return true;
        } else if (replaceTypeValue.equals(LITTERAL_REPLACETYPE_PARAMVALUE)) {
            return false;
        } else {
            throw new IllegalArgumentException("replaceTypeValue = " + replaceTypeValue);
        }
    }

}
