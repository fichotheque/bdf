/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.sphere.SphereEditor;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurCreationCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurCreation";
    public final static String COMMANDKEY = "_ SPH-04";
    public final static String NEWLOGIN_PARAMNAME = "newlogin";
    private String newLogin;
    private Sphere sphere;

    public RedacteurCreationCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Redacteur redacteur;
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            SphereEditor sphereEditor = session.getFichothequeEditor().getSphereEditor(sphere.getSubsetKey());
            try {
                redacteur = sphereEditor.createRedacteur(-1, newLogin);
            } catch (ExistingIdException iee) {
                throw new ShouldNotOccurException("test before ExistingIdException");
            } catch (ParseException pe) {
                throw new ShouldNotOccurException("test before ParseException");
            }
        }
        putResultObject(SPHERE_OBJ, redacteur.getSphere());
        putResultObject(REDACTEUR_OBJ, redacteur);
        setDone("_ done.sphere.redacteurcreation", newLogin);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        checkFichothequeAdmin();
        sphere = requestHandler.getMandatorySphere();
        checkSphereSupervisor(sphere.getSubsetName());
        String param_new = getMandatory(NEWLOGIN_PARAMNAME).trim();
        if (param_new.isEmpty()) {
            throw BdfErrors.error("_ error.empty.login", param_new);
        }
        if (!SphereUtils.testLogin(param_new)) {
            throw BdfErrors.error("_ error.wrong.login", param_new);
        }
        if (sphere.getRedacteurByLogin(param_new) != null) {
            throw BdfErrors.error("_ error.existing.login", param_new);
        }
        this.newLogin = param_new;
    }

}
