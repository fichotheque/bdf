/* BdfServer_Commands - Copyright (c) 2014-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.sphere.SphereEditor;
import net.fichotheque.utils.SphereUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class LoginChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "LoginChange";
    public final static String COMMANDKEY = "_ SPH-10";
    public static String NEWLOGIN_PARAMNAME = "newlogin";
    private String newLogin;
    private Redacteur redacteur;

    public LoginChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Sphere sphere = redacteur.getSphere();
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            SphereEditor sphereEditor = session.getFichothequeEditor().getSphereEditor(sphere.getSubsetKey());
            try {
                sphereEditor.setLogin(redacteur, newLogin);

            } catch (ExistingIdException iee) {
                throw new ShouldNotOccurException("test before ExistingIdException");
            } catch (ParseException pe) {
                throw new ShouldNotOccurException("test before ParseException");
            }
        }
        putResultObject(SPHERE_OBJ, sphere);
        putResultObject(REDACTEUR_OBJ, redacteur);
        setDone("_ done.sphere.loginchange", newLogin);
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        redacteur = requestHandler.getMandatoryRedacteur();
        checkSphereSupervisor(redacteur.getSubsetName());
        checkSubsetAdmin(redacteur.getSphere());
        String param_new = getMandatory(NEWLOGIN_PARAMNAME);
        param_new = param_new.trim();
        if (param_new.length() == 0) {
            throw BdfErrors.error("_ error.empty.login", param_new);
        }
        if (!SphereUtils.testLogin(param_new)) {
            throw BdfErrors.error("_ error.wrong.login", param_new);
        }
        if (redacteur.getSphere().getRedacteurByLogin(param_new) != null) {
            throw BdfErrors.error("_ error.existing.login", param_new);
        }
        this.newLogin = param_new;
    }

}
