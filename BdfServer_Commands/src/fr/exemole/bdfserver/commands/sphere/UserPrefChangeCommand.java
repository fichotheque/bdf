/* BdfServer_Commands - Copyright (c) 2022-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public class UserPrefChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "UserPrefChange";
    public final static String ATTRIBUTE_PARAMNAME = "attribute";
    public final static String VALUE_PARAMNAME = "value";
    private AttributeKey attributeKey;
    private CleanedString value;

    public UserPrefChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        Attribute currentAttribute = bdfUser.getPrefs().getAttributes().getAttribute(attributeKey);
        Attribute newAttribute;
        if (currentAttribute == null) {
            newAttribute = AttributeBuilder.toAttribute(attributeKey, value);
        } else {
            String newValue = value.toString();
            String oppositeValue = getOppositeValue(newValue);
            AttributeBuilder attributeBuilder = new AttributeBuilder(attributeKey);
            boolean here = false;
            for (String currentValue : currentAttribute) {
                if (!currentValue.equals(oppositeValue)) {
                    if (currentValue.equals(newValue)) {
                        here = true;
                    }
                    attributeBuilder.addValue(currentValue);
                }
            }
            if (!here) {
                attributeBuilder.addValue(value);
            }
            newAttribute = attributeBuilder.toAttribute();
        }
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            session.getBdfServerEditor().getBdfUserPrefsEditor(bdfUser.getPrefs()).putAttribute(newAttribute);
        }
        setDone("_ done.sphere.userprefchange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        String attributeString = getMandatory(ATTRIBUTE_PARAMNAME);
        value = CleanedString.newInstance(getMandatory(VALUE_PARAMNAME));
        try {
            attributeKey = AttributeKey.parse(attributeString);
        } catch (ParseException pe) {
            throw BdfErrors.wrongParameterValue(ATTRIBUTE_PARAMNAME, attributeString);
        }
        if (value == null) {
            throw BdfErrors.emptyMandatoryParameter(VALUE_PARAMNAME);
        }
    }

    private static String getOppositeValue(String newValue) {
        if (newValue.startsWith("!")) {
            return newValue.substring(1);
        } else {
            return "!" + newValue;
        }
    }

}
