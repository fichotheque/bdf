/* BdfServer_Commands - Copyright (c) 2013-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurAttributeChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurAttributeChange";
    public final static String COMMANDKEY = "_ SPH-09";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    private Redacteur redacteur;
    private AttributeChange attributeChange;

    public RedacteurAttributeChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            session.getFichothequeEditor().changeAttributes(redacteur, attributeChange);
        }
        putResultObject(SPHERE_OBJ, redacteur.getSphere());
        putResultObject(REDACTEUR_OBJ, redacteur);
        setDone("_ done.global.attributechange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        redacteur = requestHandler.getMandatoryRedacteur();
        checkSubsetAdmin(redacteur.getSphere());
        String attributes = getMandatory(ATTRIBUTES_PARAMNAME);
        this.attributeChange = AttributeParser.parse(attributes);
    }

}
