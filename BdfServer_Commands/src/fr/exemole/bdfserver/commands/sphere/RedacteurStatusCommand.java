/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.roles.PermissionSummaryBuilder;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.permission.PermissionSummary;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurStatusCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurStatus";
    public final static String COMMANDKEY = "_ SPH-05";
    public static final String STATUS_PARAMNAME = "status";
    private Redacteur redacteur;
    private String newStatus;

    public RedacteurStatusCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done;
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            SphereEditor sphereEditor = session.getFichothequeEditor().getSphereEditor(redacteur.getSubsetKey());
            done = sphereEditor.setStatus(redacteur, newStatus);
        }
        putResultObject(SPHERE_OBJ, redacteur.getSphere());
        putResultObject(REDACTEUR_OBJ, redacteur);
        if (done) {
            setDone("_ done.sphere.redacteurstatus");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        redacteur = requestHandler.getMandatoryRedacteur();
        checkSubsetAdmin(redacteur.getSphere());
        try {
            newStatus = FichothequeConstants.checkRedacteurStatus(getMandatory(STATUS_PARAMNAME));
        } catch (IllegalArgumentException iae) {
            throw BdfErrors.wrongParameterValue(STATUS_PARAMNAME, newStatus);
        }
        PermissionSummary redacteurPermissionSummary = PermissionSummaryBuilder.build(bdfServer.getPermissionManager(), redacteur);
        if (redacteurPermissionSummary.isSubsetAdmin(redacteur.getSubsetKey())) {
            throw BdfErrors.error("_ error.unsupported.inactiveadmin");
        }
    }

}
