/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.commands.AbstractPhrasesCommand;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.sphere.metadata.SphereMetadataEditor;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class SpherePhrasesCommand extends AbstractPhrasesCommand {

    public final static String COMMANDNAME = "SpherePhrases";
    public final static String COMMANDKEY = "_ SPH-02";
    private Sphere sphere;

    public SpherePhrasesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        boolean done = false;
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            SphereMetadataEditor sphereMetadataEditor = session.getFichothequeEditor().getSphereEditor(sphere.getSubsetKey()).getSphereMetadataEditor();
            if (update(sphereMetadataEditor)) {
                done = true;
            }
        }
        putResultObject(SPHERE_OBJ, sphere);
        if (done) {
            setDone("_ done.sphere.spherephrases");
        }
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        sphere = requestHandler.getMandatorySphere();
        checkSubsetAdmin(sphere);
        checkPhrasesParameters();
    }

}
