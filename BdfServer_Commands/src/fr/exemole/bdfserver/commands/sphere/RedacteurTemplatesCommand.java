/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.namespaces.BdfUserSpace;
import fr.exemole.bdfserver.api.users.BdfUserPrefs;
import fr.exemole.bdfserver.api.users.BdfUserPrefsEditor;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import net.fichotheque.corpus.Corpus;
import net.fichotheque.exportation.transformation.TemplateKey;
import net.fichotheque.exportation.transformation.TransformationKey;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurTemplatesCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurTemplates";
    public final static String COMMANDKEY = "_ USR-04";
    private BdfUserPrefs redacBdfUserPrefs;

    public RedacteurTemplatesCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            saveKeys(session.getBdfServerEditor().getBdfUserPrefsEditor(redacBdfUserPrefs));
        }
        setDone("_ done.sphere.redacteurtemplates");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        Redacteur redacteur = requestHandler.getMandatoryRedacteur();
        boolean isSame = BdfUserUtils.isSame(bdfUser, redacteur);
        if (!isSame) {
            checkSubsetAdmin(redacteur.getSphere());
        }
        if (isSame) {
            redacBdfUserPrefs = bdfUser.getPrefs();
        } else {
            redacBdfUserPrefs = bdfServer.createBdfUser(redacteur).getPrefs();
        }
    }

    private void saveKeys(BdfUserPrefsEditor bdfUserPrefsEditor) throws ErrorMessageException {
        saveTransformationKey(bdfUserPrefsEditor, TransformationKey.COMPILATION_INSTANCE);
        saveTransformationKey(bdfUserPrefsEditor, TransformationKey.STATTHESAURUS_INSTANCE);
        saveTransformationKey(bdfUserPrefsEditor, TransformationKey.INVERSETHESAURUS_INSTANCE);
        for (Corpus corpus : fichotheque.getCorpusList()) {
            TransformationKey transformationKey = new TransformationKey(corpus.getSubsetKey());
            saveTransformationKey(bdfUserPrefsEditor, transformationKey);
        }
    }

    private void saveTransformationKey(BdfUserPrefsEditor bdfUserPrefsEditor, TransformationKey transformationKey) throws ErrorMessageException {
        if (requestHandler.hasParameter(transformationKey.getKeyString())) {
            String value = requestHandler.getTrimedParameter(transformationKey.getKeyString());
            AttributeKey attributeKey = BdfUserSpace.toSimpleTemplateAttributeKey(transformationKey);
            if (value.equals(TemplateKey.DEFAULT_NAME)) {
                bdfUserPrefsEditor.removeAttribute(attributeKey);
            } else {
                Attribute attribute = AttributeBuilder.toAttribute(attributeKey, value);
                if (attribute == null) {
                    bdfUserPrefsEditor.removeAttribute(attributeKey);
                } else {
                    bdfUserPrefsEditor.putAttribute(attribute);
                }
            }
        }
        String prefix = transformationKey + "/";
        for (String paramName : requestHandler.getParameterNameSet()) {
            if (paramName.startsWith(prefix)) {
                String extension = paramName.substring(prefix.length());
                String value2 = requestHandler.getTrimedParameter(paramName);
                AttributeKey attributeKey = BdfUserSpace.toStreamTemplateAttributeKey(transformationKey, extension);
                if (value2.equals(TemplateKey.DEFAULT_NAME)) {
                    bdfUserPrefsEditor.removeAttribute(attributeKey);
                } else {
                    Attribute attribute = AttributeBuilder.toAttribute(attributeKey, value2);
                    if (attribute == null) {
                        bdfUserPrefsEditor.removeAttribute(attributeKey);
                    } else {
                        bdfUserPrefsEditor.putAttribute(attribute);
                    }
                }
            }
        }
    }

}
