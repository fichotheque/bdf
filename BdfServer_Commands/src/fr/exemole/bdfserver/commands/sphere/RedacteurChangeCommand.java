/* BdfServer_Commands - Copyright (c) 2010-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands.sphere;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.tools.instruction.AbstractBdfCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.users.BdfUserUtils;
import java.text.ParseException;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.EmailCoreUtils;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.models.PersonCoreUtils;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class RedacteurChangeCommand extends AbstractBdfCommand {

    public final static String COMMANDNAME = "RedacteurChange";
    public final static String COMMANDKEY = "_ USR-01";
    public final static String SURNAME_PARAMNAME = "surname";
    public final static String NONLATIN_PARAMNAME = "nonlatin";
    public final static String FORENAME_PARAMNAME = "forename";
    public final static String EMAIL_PARAMNAME = "email";
    public final static String SURNAMEFIRST_PARAMNAME = "surnamefirst";
    private PersonCore person;
    private EmailCore email;
    private Redacteur redacteur;

    public RedacteurChangeCommand(BdfServer bdfServer, RequestMap requestMap) {
        super(bdfServer, requestMap);
    }

    @Override
    public boolean needSynchronisation() {
        return true;
    }

    @Override
    protected void doCommand() throws ErrorMessageException {
        try (EditSession session = startEditSession(Domains.SPHERE, COMMANDNAME)) {
            SphereEditor sphereEditor = session.getFichothequeEditor().getSphereEditor(redacteur.getSubsetKey());
            sphereEditor.setPerson(redacteur, person);
            sphereEditor.setEmail(redacteur, email);
        }
        setDone("_ done.sphere.redacteurchange");
    }

    @Override
    protected void checkParameters() throws ErrorMessageException {
        redacteur = requestHandler.getMandatoryRedacteur();
        checkSphereSupervisor(redacteur.getSubsetName());
        boolean isSame = BdfUserUtils.isSame(bdfUser, redacteur);
        if ((!isSame) || (!bdfServer.getPolicyManager().getUserAllow().isDataChangeAllowed())) {
            checkSubsetAdmin(redacteur.getSphere());
        }
        initEmail();
        initPerson();
    }

    private void initEmail() throws ErrorMessageException {
        String emailString = StringUtils.cleanString(requestHandler.getTrimedParameter(EMAIL_PARAMNAME));
        if (emailString.isEmpty()) {
            email = null;
        } else {
            try {
                email = EmailCoreUtils.parse(emailString);
            } catch (ParseException pe) {
                throw BdfErrors.error("_ error.wrong.email", emailString);
            }
        }
    }

    private void initPerson() throws ErrorMessageException {
        String surname = StringUtils.cleanString(getMandatory(SURNAME_PARAMNAME));
        String forename = StringUtils.cleanString(requestHandler.getTrimedParameter(FORENAME_PARAMNAME));
        String nonlatin = StringUtils.cleanString(requestHandler.getTrimedParameter(NONLATIN_PARAMNAME));
        boolean surnameFirst = requestHandler.isTrue(SURNAMEFIRST_PARAMNAME);
        person = PersonCoreUtils.toPersonCore(surname, forename, nonlatin, surnameFirst);
    }

}
