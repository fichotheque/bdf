/* BdfServer_Commands - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.commands;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.instruction.BdfCommand;
import fr.exemole.bdfserver.api.instruction.BdfCommandParameters;
import fr.exemole.bdfserver.api.interaction.Domain;
import fr.exemole.bdfserver.api.interaction.Domains;
import fr.exemole.bdfserver.api.providers.BdfCommandProvider;
import fr.exemole.bdfserver.commands.addenda.AddendaCommandFactory;
import fr.exemole.bdfserver.commands.administration.AdministrationCommandFactory;
import fr.exemole.bdfserver.commands.album.AlbumCommandFactory;
import fr.exemole.bdfserver.commands.configuration.ConfigurationCommandFactory;
import fr.exemole.bdfserver.commands.corpus.CorpusCommandFactory;
import fr.exemole.bdfserver.commands.edition.EditionCommandFactory;
import fr.exemole.bdfserver.commands.exportation.ExportationCommandFactory;
import fr.exemole.bdfserver.commands.importation.ImportationCommandFactory;
import fr.exemole.bdfserver.commands.mailing.MailingCommandFactory;
import fr.exemole.bdfserver.commands.selection.SelectionCommandFactory;
import fr.exemole.bdfserver.commands.sphere.SphereCommandFactory;
import fr.exemole.bdfserver.commands.thesaurus.ThesaurusCommandFactory;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CoreBdfCommandProvider implements BdfCommandProvider {

    public final static BdfCommandProvider UNIQUE_INSTANCE = new CoreBdfCommandProvider();

    @Override
    public BdfCommand getBdfCommand(BdfCommandParameters bdfCommandParameters) {
        BdfServer bdfServer = bdfCommandParameters.getBdfServer();
        RequestMap requestMap = bdfCommandParameters.getRequestMap();
        String commandName = bdfCommandParameters.getCommandName();
        Domain domain = bdfCommandParameters.getDomain();
        switch (domain.getFirstPart()) {
            case Domains.ADMINISTRATION:
                return AdministrationCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.ALBUM:
                return AlbumCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.CONFIGURATION:
                return ConfigurationCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.CORPUS:
                return CorpusCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.MAILING:
                return MailingCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.EXPORTATION:
                return ExportationCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.EDITION:
                return EditionCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.SELECTION:
                return SelectionCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.SPHERE:
                return SphereCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.THESAURUS:
                return ThesaurusCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.ADDENDA:
                return AddendaCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            case Domains.IMPORTATION:
                return ImportationCommandFactory.getBdfCommand(bdfServer, requestMap, commandName);
            default:
                return null;
        }
    }

}
