/* OdLib_Elements - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;


/**
 *
 * @author Vincent Calame
 */
public final class OdElementUtils {

    private OdElementUtils() {

    }

    /**
     * Transforme le nom du style pour son affichage dans l'interface de
     * LibreOffice. Remplace les soulignés par des espaces.
     *
     * @param styleNameValue
     * @return
     */
    public static String toDisplayNameValue(String styleNameValue) {
        StringBuilder displayBuf = new StringBuilder();
        int length = styleNameValue.length();
        char previous = styleNameValue.charAt(0);
        displayBuf.append(previous);
        for (int i = 1; i < length; i++) {
            char carac = styleNameValue.charAt(i);
            if (carac == '_') {
                displayBuf.append(" ");
            } else {
                displayBuf.append(carac);
            }
        }
        return displayBuf.toString();
    }

    /**
     * Modifie éventuellement le nom du style pour être conforme à la syntaxe
     * d'un nom de style dans le format OpenDocument. H, h1 jusqu'à h10 sont
     * transformés en Heading et le souligné est transformé en
     *
     * @param styleNameValue
     * @return
     */
    public static String checkStyleNameValue(String styleNameValue) {
        int hLevel = getHLevel(styleNameValue);
        if (hLevel == 0) {
            return "Heading";
        }
        if (hLevel > 0) {
            return "Heading_20_" + String.valueOf(hLevel);
        }
        int length = styleNameValue.length();
        if (length == 0) {
            return "";
        }
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char carac = styleNameValue.charAt(i);
            if (carac == '_') {
                buf.append("_20_");
            } else {
                buf.append(carac);
            }
        }
        return buf.toString();
    }

    public static boolean charTest(char carac) {
        if ((carac >= '0') && (carac <= '9')) {
            return true;
        }
        if (Character.isUpperCase(carac)) {
            return true;
        }
        return false;
    }

    /**
     * Retourne une valeur entre 0 et 10 si la valeur en argument correspond à
     * un niveau de titre (h, h1 jusqu'à h10), retourne un nombre négatif sinon.
     *
     *
     * @param value
     * @return
     */
    public static int getHLevel(String value) {
        if (!value.startsWith("h")) {
            return -1;
        }
        int length = value.length();
        if (length == 1) {
            return 0;
        }
        try {
            int level = Integer.parseInt(value.substring(1));
            if (level < 1) {
                return -2;
            }
            if (level > 10) {
                return -3;
            }
            return level;
        } catch (NumberFormatException nfe) {
            return -4;
        }
    }

    /**
     * Indique si l'attribut dont le nom est en argument sert stocker le nom
     * d'un style.
     *
     * @param attributeName
     * @return
     */
    public static boolean isStyleNameAttribute(String attributeName) {
        switch (attributeName) {
            case "parent-style-name":
            case "next-style-name":
            case "style-name":
            case "list-style-name":
            case "master-page-name":
                return true;
            default:
                return false;

        }
    }

}
