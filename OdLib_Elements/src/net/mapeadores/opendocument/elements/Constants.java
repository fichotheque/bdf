/* OdLib_Elements - Copyright (c) 2007-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;


/**
 *
 * @author Vincent Calame
 */
public interface Constants {

    public static final int ATTRIBUTES_LENGTH = 12;
    public static final int STYLE_ATTRIBUTES = 1;
    public static final int TEXT_PROPERTIES_ATTRIBUTES = 2;
    public static final int PARAGRAPH_PROPERTIES_ATTRIBUTES = 3;
    public static final int PAGELAYOUT_PROPERTIES_ATTRIBUTES = 4;
    public static final int TABLE_PROPERTIES_ATTRIBUTES = 5;
    public static final int LISTLEVEL_PROPERTIES_ATTRIBUTES = 6;
    public static final int TABLECOLUMN_PROPERTIES_ATTRIBUTES = 7;
    public static final int TABLECELL_PROPERTIES_ATTRIBUTES = 8;
    public static final int TABLEROW_PROPERTIES_ATTRIBUTES = 9;
    public static final int LISTLEVEL_ATTRIBUTES = 10;
    public static final int LISTSTYLE_ATTRIBUTES = 11;
    public static final int GRAPHIC_PROPERTIES_ATTRIBUTES = 12;
    public final static short FO_NAMESPACE = 11;
    public final static short STYLE_NAMESPACE = 12;
    public final static short TEXT_NAMESPACE = 13;
    public final static short TABLE_NAMESPACE = 14;
    public final static short SVG_NAMESPACE = 15;
    public final static short DRAW_NAMESPACE = 16;
}
