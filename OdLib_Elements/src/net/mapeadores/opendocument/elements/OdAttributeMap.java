/* OdLib_Elements - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public class OdAttributeMap implements Constants {

    private final static Map<String, OdAttribute> map = new HashMap<String, OdAttribute>();

    static {
        init();
    }

    private OdAttributeMap() {
    }

    public static short nameSpaceToShort(String nameSpace) {
        if (nameSpace.equals("fo")) {
            return FO_NAMESPACE;
        } else if (nameSpace.equals("style")) {
            return STYLE_NAMESPACE;
        } else if (nameSpace.equals("text")) {
            return TEXT_NAMESPACE;
        } else if (nameSpace.equals("table")) {
            return TABLE_NAMESPACE;
        } else if (nameSpace.equals("svg")) {
            return SVG_NAMESPACE;
        } else if (nameSpace.equals("draw")) {
            return DRAW_NAMESPACE;
        } else {
            throw new IllegalArgumentException("unknown nameSpace =" + nameSpace);
        }
    }

    public static OdAttribute getAttribute(int[] typeOrder, String name) {
        for (int i = 0; i < typeOrder.length; i++) {
            OdAttribute attribute = getAttribute(typeOrder[i], name);
            if (attribute != null) {
                return attribute;
            }
        }
        return null;
    }

    private static OdAttribute getAttribute(int type, String name) {
        return map.get(toKey(type, name));
    }

    private static void init() {
        init("style.txt", STYLE_ATTRIBUTES);
        init("listlevel.txt", LISTLEVEL_ATTRIBUTES);
        init("liststyle.txt", LISTSTYLE_ATTRIBUTES);
        for (int i = 0; i < PropertiesElementUtils.count(); i++) {
            int type = PropertiesElementUtils.getAttributesType(i);
            String fileName = PropertiesElementUtils.getFileName(i);
            init(fileName, type);
        }
    }

    private static void init(String ressource, int type) {
        try {
            InputStream inputStream = OdAttribute.class.getResourceAsStream("resources/" + ressource);
            if (inputStream == null) {
                throw new IllegalStateException("missing : " + ressource);
            }
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String ligne;
            short currentNameSpace = FO_NAMESPACE;
            while ((ligne = reader.readLine()) != null) {
                ligne = ligne.trim();
                if (ligne.length() == 0) {
                    continue;
                }
                if (ligne.charAt(0) == '#') {
                    currentNameSpace = nameSpaceToShort(ligne.substring(1));
                } else {
                    String key = toKey(type, ligne);
                    OdAttribute attr = new OdAttribute(type, ligne, currentNameSpace);
                    map.put(key, attr);
                }
            }
        } catch (IOException ioe) {
            throw new OdLibException(ioe);
        }
    }

    public static String nameSpaceToString(short nameSpace) {
        switch (nameSpace) {
            case OdAttribute.FO_NAMESPACE:
                return "fo";
            case OdAttribute.STYLE_NAMESPACE:
                return "style";
            case OdAttribute.TEXT_NAMESPACE:
                return "text";
            case OdAttribute.TABLE_NAMESPACE:
                return "table";
            case OdAttribute.SVG_NAMESPACE:
                return "svg";
            case OdAttribute.DRAW_NAMESPACE:
                return "draw";
            default:
                throw new IllegalArgumentException("unknown nameSpace =" + nameSpace);
        }
    }

    private static String toKey(int type, String name) {
        return type + ":" + name;
    }

}
