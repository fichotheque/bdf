/* OdLib_Elements - Copyright (c) 2007-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;

import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public class StyleElement extends OdElement {

    public final static String PARAGRAPH_FAMILY = "paragraph";
    public final static String TEXT_FAMILY = "text";
    public final static String TABLE_FAMILY = "table";
    public final static String TABLECELL_FAMILY = "table-cell";
    public final static String TABLEROW_FAMILY = "table-row";
    public final static String TABLECOLUMN_FAMILY = "table-column";
    public final static String GRAPHIC_FAMILY = "graphic";
    private final static Map<String, int[]> availableMap = new HashMap<String, int[]>();

    static {
        int[] avprg = {STYLE_ATTRIBUTES, PARAGRAPH_PROPERTIES_ATTRIBUTES, TEXT_PROPERTIES_ATTRIBUTES};
        availableMap.put(PARAGRAPH_FAMILY, avprg);
        int[] avtxt = {STYLE_ATTRIBUTES, TEXT_PROPERTIES_ATTRIBUTES};
        availableMap.put(TEXT_FAMILY, avtxt);
        int[] avtb = {STYLE_ATTRIBUTES, TABLE_PROPERTIES_ATTRIBUTES};
        availableMap.put(TABLE_FAMILY, avtb);
        int[] avtd = {STYLE_ATTRIBUTES, TABLECELL_PROPERTIES_ATTRIBUTES, PARAGRAPH_PROPERTIES_ATTRIBUTES, TEXT_PROPERTIES_ATTRIBUTES};
        availableMap.put(TABLECELL_FAMILY, avtd);
        int[] avtr = {STYLE_ATTRIBUTES, TABLEROW_PROPERTIES_ATTRIBUTES};
        availableMap.put(TABLEROW_FAMILY, avtr);
        int[] avcol = {STYLE_ATTRIBUTES, TABLECOLUMN_PROPERTIES_ATTRIBUTES};
        availableMap.put(TABLECOLUMN_FAMILY, avcol);
        int[] avgraph = {STYLE_ATTRIBUTES, GRAPHIC_PROPERTIES_ATTRIBUTES};
        availableMap.put(GRAPHIC_FAMILY, avgraph);
    }

    private final String styleFamily;
    private final int[] availableTypeArray;
    private final String styleName;

    private StyleElement(String styleFamily, String styleName, int[] availableTypeArray) {
        this.styleFamily = styleFamily;
        this.availableTypeArray = availableTypeArray;
        this.styleName = styleName;
    }

    public String getStyleName() {
        return styleName;
    }

    @Override
    public int[] getAvalaibleTypeArray() {
        return availableTypeArray;
    }

    public String getStyleFamily() {
        return styleFamily;
    }

    public static StyleElement newInstance(String styleFamily, String styleName) {
        String displayName = OdElementUtils.toDisplayNameValue(styleName);
        styleName = OdElementUtils.checkStyleNameValue(styleName);
        int[] result = availableMap.get(styleFamily);
        StyleElement styleElement = new StyleElement(styleFamily, styleName, result);
        styleElement
                .putAttribute("display-name", displayName);
        return styleElement;
    }

    public static StyleElement newHInstance(int level) {
        String levelString = String.valueOf(level);
        String styleName = "Heading_20_" + levelString;
        int[] result = {STYLE_ATTRIBUTES, PARAGRAPH_PROPERTIES_ATTRIBUTES, TEXT_PROPERTIES_ATTRIBUTES};
        StyleElement styleElement = new StyleElement(PARAGRAPH_FAMILY, styleName, result);
        styleElement
                .putAttribute("default-outline-level", levelString)
                .putAttribute("parent-style-name", "Heading")
                .putAttribute("display-name", "Heading " + levelString);
        return styleElement;
    }

    public static String getMatchingStyleFamily(String elementName) {
        switch (elementName) {
            case "p":
                return StyleElement.PARAGRAPH_FAMILY;
            case "span":
                return StyleElement.TEXT_FAMILY;
            case "table":
                return StyleElement.TABLE_FAMILY;
            case "col":
            case "column":
                return StyleElement.TABLECOLUMN_FAMILY;
            case "td":
            case "cell":
                return StyleElement.TABLECELL_FAMILY;
            case "tr":
            case "row":
                return StyleElement.TABLEROW_FAMILY;
            case "graphic":
                return StyleElement.GRAPHIC_FAMILY;
            default:
                return null;
        }
    }

}
