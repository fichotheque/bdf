/* OdLib_Elements- Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;


/**
 *
 * @author Vincent Calame
 */
public class ListLevelElement extends OdElement {

    public final static short BULLET_TYPE = 1;
    public final static short NUMBER_TYPE = 2;
    private final short type;
    private int level = 0;

    public ListLevelElement(short type, int level) {
        this.type = type;
        this.level = level;
    }

    @Override
    public int[] getAvalaibleTypeArray() {
        int[] result = {LISTLEVEL_ATTRIBUTES, LISTLEVEL_PROPERTIES_ATTRIBUTES, TEXT_PROPERTIES_ATTRIBUTES};
        return result;
    }

    public short getType() {
        return type;
    }

    public int getLevel() {
        return level;
    }

}
