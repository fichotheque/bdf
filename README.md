# Logiciel BDF

Ce dépôt contient le code source du logiciel BDF, logiciel de gestion de fichothèques (BDF sont les initiales de Base De Fiches). BDF est écrit en Java et nécessite un serveur Tomcat.

## Compilation

La compilation se fait avec Ant. Le première chose à faire est donc de l'installer, il est présent dans tous les dépôts des distributions.

`sudo apt install ant`

Dans le répertoire de votre choix, clonez le dépôt de BDF

`git clone https://framagit.org/fichotheque/bdf.git`

Rendez-vous dans le répertoire *bdf* ainsi créé, vérifier que *ant-bdf.sh* est exécutable et le lancer :

`./ant-bdf.sh`

Le résultat de la compilation est placé dans le sous-répertoire target/dist, vous y trouverez en particulier le fichier *bdf##dev.war* qui est prêt à être déployé dans un serveur Tomcat.

### Personnalisation de la compilation

Pour personnaliser la compilation, le mieux est de copier le fichier *ant-bdf.sh* dans un emplacement hors du dépôt Git et de modifier les variables qui se trouvent au début du fichier.


*  *Sources* : chemin du dépôt des sources
*  *Target* : répertoire de destination de la compilation
*  *Version* : nom de la version, il se retrouve dans le nom du fichier war après ## (voir la documentation de Tomcat pour plus de détail : https://tomcat.apache.org/tomcat-9.0-doc/config/context.html#Naming)
*  *Repository* : information libre sur le dépôt d'origine des sources
*  *WebappName* : nom de l'application dans Tomcat, elle sera accessible via http://localhost:8080/{WebappName}/
*  *ParamAuthSharing* : true ou false, indique la possibilité de partager les informations de connexion entre différentes fichothèques, utile de mettre à true si l'application est destinée à une organisaiton unique (défaut: false)
*  *ParamDisablePassword* : true ou false, permet de se connecter sans indiquer de mot de passe, à n'utiliser bien sûr qu'en local et pour le développement (défaut: false)
*  *ParamConfFile* : chemin du fichier de configuration, lorsqu'il est absent (ce qui est le cas tant que vous ne le créez pas vous-même), bdf##dev.war comprend une configuration par défaut
*  *ParamSmtpFile* : chemin du fichier de configuration de l'envoi via SMTP, lorsqu'il est absent, l'envoi par courriel n'est pas possible
*  *ParamMultiBdf* : true ou false, indique que l'instance va permettre de gérer des fichothèques multiples (défaut: true)
*  *ParamCentralSphereList* : liste des noms des sphères centrales (défaut: vide)
*  *AntTargets* : Cibles possibles de Ant, les valeurs disponibles sont webinf (compilation proprement dite), zip (création de deux fichiers zip pour le binaire et les sources) et war (création d'un fichier war déployable)


*ParamAuthSharing*, *ParamDisablePassword*, *ParamConfFile*, *ParamSmtpFile*, *ParamMultiBdf*, *ParamCentralSphereList* correspondent aux six paramètres d'initialisation du contexte Tomcat décrits dans la fiche : https://www.fichotheque.net/fiche-119.html

Ils seront placés dans des fichiers par défaut context.xml et bdf-conf.xml situés dans *target/dist/bdf-dev/META-INF/*. Ils ne sont utilisés que pour la version *.war* en proposant une configuration prête à l'emploi au premier déploiement. Comme l'indique la documentation de Tomcat (https://tomcat.apache.org/tomcat-9.0-doc/config/context.html#Defining_a_context), il existe plusieurs manières de définir un contexte. La plus sure est de placer un fichier dans *$CATALINA_BASE/conf/[enginename]/[hostname]/*, la configuration ne sera pas écrasée lors des déploiements suivants d'une version .war. 

On pourra tester le fichier *.war* en suivant la procédure indiquée dans la fiche : https://www.fichotheque.net/fiche-91.html

## Téléchargement d'une version compilée

Une étiquette est crée à chaque version jugée stable (qu'elle soit une version de développement utilisée pour un nombre réduit de fichothèques ou une version en production plus large) : https://framagit.org/fichotheque/bdf/-/tags

Les versions compilées correspondant à ces étiquettes sont disponibles à cette adresse : https://bdf.exemole.fr/archives/. Trois fichiers sont fournis : les sources complètes, le binaire installable et fichier .war pour un déploiement automatique dans Tomcat.

La toute dernière version compilée est disponible à l'adresse : https://bdf.exemole.fr/latest/, elle n'est souvent utilisée que sur des fichothèques « d'avant-garde » pour des tests. Elle est donc à utiliser avec précaution.

## Déploiement avec Docker

Le répertoire /docker/bdf contient un fichier Dockerfile permettant de construire une image Docker avec la dernière version de BDF. La construction de l'image se fait avec le script /docker/bdf/build-bdf.sh et le lancement de l'image avec le script /docker/bdf/run-bdf.sh. Pour en savoir plus : https://www.fichotheque.net/fiche-132.html

