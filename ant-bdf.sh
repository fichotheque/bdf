#!/bin/bash
Sources="$PWD"
Target="$PWD/target"
Version="dev"
Repository="local"
WebappName="bdf"
ParamAuthSharing="false"
ParamDisablePassword="false"
ParamConfFile="/etc/bdf/bdf-conf.xml"
ParamSmtpFile="/etc/bdf/smtp.ini"
ParamMultiBdf="true"
ParamCentralSphereList=""
AntTargets="webinf war"

ant -Ddeploy.path=$Target -Dproject.version="$Version"  -Dproject.repository="$Repository" -Dproject.webapp_name="$WebappName" -Dproject.param_auth_sharing="$ParamAuthSharing" -Dproject.param_disable_password="$ParamDisablePassword" -Dproject.param_conf_file="$ParamConfFile" -Dproject.param_smtp_file="$ParamSmtpFile" -Dproject.param_multi_bdf="$ParamMultiBdf" -Dproject.param_central_sphere_list="$ParamCentralSphereList" -f $Sources/ant/BDF/build.xml $AntTargets
