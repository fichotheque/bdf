/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.xml;

import java.io.IOException;
import net.fichotheque.Metadata;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class CentralSphereMetadataXMLPart extends XMLPart {

    public CentralSphereMetadataXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendCentralSphereMetadata(Metadata metadata) throws IOException {
        openTag("central-sphere-metadata");
        FichothequeXMLUtils.write(metadata, this);
        closeTag("central-sphere-metadata");
    }

}
