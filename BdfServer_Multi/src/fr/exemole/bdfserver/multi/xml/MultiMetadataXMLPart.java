/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.xml;

import fr.exemole.bdfserver.multi.api.MultiMetadata;
import java.io.IOException;
import net.fichotheque.utils.FichothequeXMLUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class MultiMetadataXMLPart extends XMLPart {

    public MultiMetadataXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendMultiMetadata(MultiMetadata multiMetadata) throws IOException {
        openTag("multi-metadata");
        FichothequeXMLUtils.write(multiMetadata, this);
        openTag("working-langs");
        for (Lang lang : multiMetadata.getWorkingLangs()) {
            addSimpleElement("lang", lang.toString());
        }
        closeTag("working-langs");
        addSimpleElement("authority", multiMetadata.getAuthority());
        closeTag("multi-metadata");
    }

}
