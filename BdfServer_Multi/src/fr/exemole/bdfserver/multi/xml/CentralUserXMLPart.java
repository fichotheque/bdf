/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.xml;

import fr.exemole.bdfserver.multi.api.central.CentralUser;
import java.io.IOException;
import java.util.Locale;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class CentralUserXMLPart extends XMLPart {

    public CentralUserXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendCentralUser(CentralUser centralUser) throws IOException {
        startOpenTag("central-user");
        addAttribute("status", centralUser.getStatus());
        endOpenTag();
        appendPersonCore(centralUser.getPerson());
        EmailCore emailCore = centralUser.getEmail();
        if (emailCore != null) {
            appendEmailCore(emailCore);
        }
        addSimpleElement("password", centralUser.getEncryptedPassword());
        appendLangContext(centralUser);
        AttributeUtils.addAttributes(this, centralUser.getAttributes());
        closeTag("central-user");
    }

    private void appendPersonCore(PersonCore personCore) throws IOException {
        startOpenTag("person");
        if (personCore.isSurnameFirst()) {
            addAttribute("surname-first", "true");
        }
        addAttribute("surname", personCore.getSurname());
        addAttribute("forename", personCore.getForename());
        addAttribute("nonlatin", personCore.getNonlatin());
        closeEmptyTag();
    }

    private void appendEmailCore(EmailCore emailCore) throws IOException {
        startOpenTag("email");
        addAttribute("addr-spec", emailCore.getAddrSpec());
        addAttribute("real-name", emailCore.getRealName());
        closeEmptyTag();
    }

    private void appendLangContext(CentralUser centralUser) throws IOException {
        UserLangContext userLangContext = centralUser.getUserLangContext();
        addSimpleElement("lang", userLangContext.getWorkingLang().toString());
        Locale customFormatLocale = centralUser.getCustomFormatLocale();
        if (customFormatLocale != null) {
            addSimpleElement("format-locale", Lang.toISOString(customFormatLocale));
        }
        LangPreference customLangPreference = centralUser.getCustomLangPreference();
        if (customLangPreference != null) {
            openTag("lang-preference");
            for (Lang lang : customLangPreference) {
                addSimpleElement("lang", lang.toString());
            }
            closeTag("lang-preference");
        }
    }

}
