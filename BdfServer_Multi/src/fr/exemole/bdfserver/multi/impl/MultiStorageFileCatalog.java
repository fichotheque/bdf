/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.impl;

import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageFile;
import java.io.File;
import net.fichotheque.sphere.LoginKey;


/**
 *
 * @author Vincent Calame
 */
public final class MultiStorageFileCatalog {

    private MultiStorageFileCatalog() {

    }

    public static StorageFile multiMetadata(StorageDirectories storageDirectories) {
        String path = "metadata.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile centralSphereMetadata(StorageDirectories storageDirectories, String name) {
        String path = "spheres" + File.separator + name + File.separator + "metadata.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static StorageFile centralUser(StorageDirectories storageDirectories, LoginKey loginKey) {
        String path = "spheres/" + loginKey.getSphereName() + "/users/" + loginKey.getLogin() + ".xml";
        return StorageFile.build(storageDirectories, path);
    }

}
