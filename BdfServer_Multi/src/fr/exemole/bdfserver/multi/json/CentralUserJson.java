/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.json;

import fr.exemole.bdfserver.multi.api.central.CentralUser;
import java.io.IOException;
import java.util.Locale;
import net.fichotheque.sphere.LoginKey;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
public final class CentralUserJson {

    private CentralUserJson() {

    }

    public static void coreProperties(JSONWriter jw, CentralUser centralUser) throws IOException {
        LoginKey loginKey = centralUser.getLoginKey();
        UserLangContext userLangContext = centralUser.getUserLangContext();
        jw.key("sphere")
                .value(loginKey.getSphereName());
        jw.key("login")
                .value(loginKey.getLogin());
        jw.key("name")
                .value(centralUser.getCompleteName());
        jw.key("lang")
                .value(userLangContext.getWorkingLang());
        jw.key("locale")
                .value(userLangContext.getISOFormatLocaleString());
        jw.key("status")
                .value(centralUser.getStatus());
    }

    public static void allProperties(JSONWriter jw, CentralUser centralUser) throws IOException {
        coreProperties(jw, centralUser);
        PersonCore person = centralUser.getPerson();
        jw.key("person");
        jw.object();
        {
            jw.key("surname")
                    .value(person.getSurname());
            jw.key("forename")
                    .value(person.getForename());
            String nonlatin = person.getNonlatin();
            if (nonlatin.length() > 0) {
                jw.key("nonlatin")
                        .value(nonlatin);
            }
            boolean surnameFirst = person.isSurnameFirst();
            if (surnameFirst) {
                jw.key("surnamefirst")
                        .value(surnameFirst);
            }
        }
        jw.endObject();
        EmailCore email = centralUser.getEmail();
        if (email != null) {
            jw.key("email");
            jw.object();
            {
                jw.key("complete")
                        .value(email.toCompleteString());
                jw.key("address")
                        .value(email.getAddrSpec());
                jw.key("name")
                        .value(email.getRealName());
            }
            jw.endObject();
        }
        jw.key("langcontext");
        jw.object();
        {
            jw.key("workinglang")
                    .value(centralUser.getUserLangContext().getWorkingLang());
            Locale customFormatLocale = centralUser.getCustomFormatLocale();
            if (customFormatLocale != null) {
                jw.key("customformatlocale")
                        .value(Lang.toISOString(customFormatLocale));
            }
            LangPreference customLangPreference = centralUser.getCustomLangPreference();
            if (customLangPreference != null) {
                jw.key("customlangpreference");
                jw.array();
                for (Lang lang : customLangPreference) {
                    jw.value(lang.toString());
                }
                jw.endArray();
            }
        }
        jw.endObject();

    }

    public static void populate(JsObject jsObject, CentralUser centralUser) {
        LoginKey loginKey = centralUser.getLoginKey();
        UserLangContext userLangContext = centralUser.getUserLangContext();
        jsObject
                .put("sphere", loginKey.getSphereName())
                .put("login", loginKey.getLogin())
                .put("name", centralUser.getCompleteName())
                .put("lang", userLangContext.getWorkingLang())
                .put("locale", userLangContext.getISOFormatLocaleString())
                .put("status", centralUser.getStatus());
    }


}
