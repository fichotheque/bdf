/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.json;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import fr.exemole.bdfserver.multi.tools.FichothequeInfo;
import fr.exemole.bdfserver.storage.directory.tools.MetadataExtractor;
import java.io.IOException;
import net.fichotheque.Fichotheque;
import net.fichotheque.utils.FichothequeMetadataUtils;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public final class FichothequeJson {

    private FichothequeJson() {

    }

    public static void phrasesProperties(JSONWriter jw, FichothequeInfo fichothequeInfo, Lang lang) throws IOException {
        if (fichothequeInfo.isInit()) {
            FichothequeJson.phrasesProperties(jw, fichothequeInfo.getBdfServer(), lang);
        } else {
            phrasesProperties(jw, fichothequeInfo.getBdfServerDirs(), lang);
        }
    }

    public static void phrasesProperties(JSONWriter jw, BdfServer bdfServer, Lang lang) throws IOException {
        Fichotheque fichotheque = bdfServer.getFichotheque();
        jw.key("title")
                .value(FichothequeMetadataUtils.getTitle(fichotheque, lang));
        jw.key("phrases");
        CommonJson.object(jw, fichotheque.getFichothequeMetadata().getPhrases(), lang);
    }

    public static void phrasesProperties(JSONWriter jw, BdfServerDirs bdfServerDirs, Lang lang) throws IOException {
        MetadataExtractor.Result result = MetadataExtractor.getPhrases(bdfServerDirs);
        if (result == null) {
            return;
        }
        String title = result.getTitleLabels().seekLabelString(lang, null);
        if (title != null) {
            jw.key("title")
                    .value(title);
        }
        jw.key("phrases");
        CommonJson.object(jw, result.getPhrases(), lang);
    }

}
