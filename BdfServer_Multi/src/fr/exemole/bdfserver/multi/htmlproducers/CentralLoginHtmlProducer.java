/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.html.jslib.AdminJsLibs;
import fr.exemole.bdfserver.multi.api.Multi;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.localisation.LangPreference;


/**
 *
 * @author Vincent Calame
 */
public class CentralLoginHtmlProducer extends MultiHtmlProducer {

    private final String sphereName;

    public CentralLoginHtmlProducer(Multi multi, String sphereName, LangPreference langPreference) {
        super(multi, langPreference);
        this.sphereName = sphereName;
        addJsLib(AdminJsLibs.CENTRALLOGIN);
        addThemeCss("centrallogin.css");
    }

    @Override
    public void printHtml() {
        String title = getTitle(multi.getServletContextName());
        JsObject args = JsObject.init()
                .put("sphereName", sphereName)
                .put("title", title);
        start(title);
        this
                .SCRIPT()
                .__jsObject("CentralLogin.ARGS", args)
                ._SCRIPT();
        end();
    }

}
