/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.html.jslib.MultiJsLibs;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.multi.json.CentralUserJson;
import fr.exemole.bdfserver.tools.storage.StorageUtils;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class IndexHtmlProducer extends MultiHtmlProducer {

    private final CentralUser centralUser;

    public IndexHtmlProducer(Multi multi, CentralUser centralUser) {
        super(multi, centralUser.getUserLangContext().getLangPreference());
        this.centralUser = centralUser;
        addJsLib(MultiJsLibs.INDEX_CLIENT);
        addThemeCss("index.css");
    }

    @Override
    public void printHtml() {
        JsObject centralUserJsObject = new JsObject();
        CentralUserJson.populate(centralUserJsObject, centralUser);
        boolean withLogo = multi.getWebappsResourceStorages().containsResource(StorageUtils.LOGO);
        String title = getTitle(multi.getServletContextName());
        JsObject args = JsObject.init()
                .put("sphereName", centralUser.getLoginKey().getSphereName())
                .put("title", title)
                .put("withLogo", withLogo)
                .put("centralUser", centralUserJsObject);
        start(title);
        this
                .SCRIPT()
                .__jsObject("Index.Client.ARGS", args)
                ._SCRIPT();
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

}
