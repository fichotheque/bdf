/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.multi.api.Multi;
import net.mapeadores.util.html.HtmlPrinterUtils;
import net.mapeadores.util.logging.CommandMessage;


/**
 *
 * @author Vincent Calame
 */
public class MultiMessageHtmlProducer extends MultiHtmlProducer {

    private final CommandMessage commandMessage;

    public MultiMessageHtmlProducer(Multi multi, CommandMessage commandMessage) {
        super(multi);
        this.commandMessage = commandMessage;
    }

    @Override
    public void printHtml() {
        startLoc("_ title.global.message");
        if (commandMessage != null) {
            this
                    .__(HtmlPrinterUtils.printCommandMessage(this, commandMessage, "global-DoneMessage", "global-ErrorMessage"));
        }
        end();
    }

}
