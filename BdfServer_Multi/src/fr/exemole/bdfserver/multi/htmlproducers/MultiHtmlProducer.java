/* BdfServer_Multi - Copyright (c) 2015-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.html.BdfHtmlProducer;
import fr.exemole.bdfserver.html.BdfJsLibManager;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.tools.storage.IconScanEngine;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.LangsUtils;


/**
 *
 * @author Vincent Calame
 */
public abstract class MultiHtmlProducer extends BdfHtmlProducer {

    protected final Multi multi;
    protected final Lang workingLang;

    public MultiHtmlProducer(Multi multi) {
        this(multi.getAdminLang(), multi);
        addMessageLocalisation(multi.getAdminMessageLocalisation());
        setBodyCssClass("global-body-Default");
    }

    public MultiHtmlProducer(Multi multi, Lang lang) {
        this(lang, multi);
        addMessageLocalisation(multi.getMessageLocalisationProvider().getMessageLocalisation(lang));
    }

    public MultiHtmlProducer(Multi multi, LangPreference langPreference) {
        this(LangsUtils.getPreferredAvailableLang(multi.getMultiMetadata().getWorkingLangs(), langPreference), multi);
        addMessageLocalisation(multi.getMessageLocalisationProvider().getMessageLocalisation(langPreference));
    }

    private MultiHtmlProducer(Lang lang, Multi multi) {
        super(multi.getWebappsJsAnalyser(), multi.getBuildInfo());
        this.multi = multi;
        this.workingLang = lang;
        setWithJavascript(true);
        scanIcons();
        setReversePath("multi-rsc/");
    }


    @Override
    public void start(String title) {
        resolveJavascript();
        resolveCss();
        setIcons();
        super.start(workingLang, title);
    }

    @Override
    public void end() {
        BdfJsLibManager jsLibManager = getJsLibManager();
        if (jsLibManager != null) {
            jsLibManager.end(this, multi.getWebappsResourceStorages(), null, null, workingLang);
        }
        super.end();
    }

    private void scanIcons() {
        setSupplementaryIconAttributesList(IconScanEngine.run(multi.getWebappsResourceStorages(), multi.getMimeTypeResolver()));
    }

    public String getTitle(String defaultTitle) {
        return multi.getMultiMetadata().getTitleLabels().seekLabelString(workingLang, defaultTitle);
    }

    public JsObject getLangConfigurationObject() {
        return JsObject.init()
                .put("workingLangArray", multi.getMultiMetadata().getWorkingLangs().toStringArray())
                .put("supplementaryLangArray", new String[0]);
    }

}
