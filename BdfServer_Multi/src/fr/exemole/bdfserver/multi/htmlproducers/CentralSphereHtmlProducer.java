/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.html.jslib.MultiJsLibs;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralSphere;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class CentralSphereHtmlProducer extends MultiHtmlProducer {

    private final CentralSphere centralSphere;

    public CentralSphereHtmlProducer(Multi multi, CentralSphere centralSphere) {
        super(multi);
        this.centralSphere = centralSphere;
        addJsLib(MultiJsLibs.CENTRALSPHERE);
        addThemeCss("pane.css", "multi.css");
    }

    @Override
    public void printHtml() {
        JsObject centralsphereArgs = JsObject.init()
                .put("sphereName", centralSphere.getName())
                .put("langConfiguration", getLangConfigurationObject());
        startLoc("_ title.multi.centralsphere", centralSphere.getName());
        this
                .SCRIPT()
                .__jsObject("CentralSphere.ARGS", centralsphereArgs)
                ._SCRIPT();
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

}
