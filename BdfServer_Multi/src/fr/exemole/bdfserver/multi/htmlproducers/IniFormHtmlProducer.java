/* BdfServer_Multi - Copyright (c) 2019-2025 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.html.consumers.Button;
import fr.exemole.bdfserver.html.consumers.Grid;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.html.consumers.attributes.Deploy;
import fr.exemole.bdfserver.html.consumers.attributes.InputPattern;
import fr.exemole.bdfserver.html.jslib.BdfJsLibs;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.MultiConstants;
import fr.exemole.bdfserver.multi.commands.IniCommand;
import fr.exemole.bdfserver.multi.commands.fichotheque.FichothequeCreationCommand;
import java.util.Set;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.request.RequestConstants;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.request.RequestUtils;


/**
 *
 * @author Vincent Calame
 */
public class IniFormHtmlProducer extends MultiHtmlProducer {

    private final RequestMap requestMap;

    public IniFormHtmlProducer(Multi multi, RequestMap requestMap) {
        super(multi, RequestUtils.getDefaultLang(requestMap));
        this.requestMap = requestMap;
        addJsLib(BdfJsLibs.DEPLOY);
        addThemeCss("multiini.css");
    }

    @Override
    public void printHtml() {
        startLoc("_ title.multi.ini");
        this
                .MAIN("multiini-Main")
                .__(PageUnit.start("action-Init", "_ title.multi.ini"))
                .__(printCommandMessage())
                .__(printForm())
                .__(PageUnit.END)
                ._MAIN();
        end();
    }

    private boolean printForm() {
        Set<String> centralSphereNameSet = getCentralSphereNameSet();
        this
                .FORM_post("multi-admin")
                .INPUT_hidden(RequestConstants.COMMAND_PARAMETER, IniCommand.COMMAND_NAME)
                .__(Grid.START)
                .__(Grid.textInputRow("_ label.multi.adminlogin", input(IniCommand.LOGIN_PARAMNAME).populate(InputPattern.LOGIN).required(true)))
                .__(Grid.passwordInputRow("_ label.multi.adminpassword_1", name(IniCommand.PASSWORD1_PARAMNAME).size("15").required(true)))
                .__(Grid.passwordInputRow("_ label.multi.adminpassword_2", name(IniCommand.PASSWORD2_PARAMNAME).size("15").required(true)))
                .__(Grid.textInputRow("_ label.multi.defaultlang", input(IniCommand.LANG_PARAMNAME).populate(InputPattern.LANG).required(true)))
                .__(Grid.textInputRow("_ label.multi.authority", input(IniCommand.AUTHORITY_PARAMNAME).populate(InputPattern.AUTHORITY).required(true)))
                .__(printCentralSphereRow(centralSphereNameSet))
                .__(Grid.END)
                .__(printFichothequeTest())
                .__(Button.COMMAND,
                        Button.submit("action-Init", "_ submit.multi.ini"))
                ._FORM();
        return true;

    }

    private boolean printCentralSphereRow(Set<String> centralSphereNameSet) {
        if ((centralSphereNameSet == null) || (centralSphereNameSet.isEmpty())) {
            return false;
        }
        StringBuilder buf = new StringBuilder();
        for (String name : centralSphereNameSet) {
            if (buf.length() > 0) {
                buf.append(", ");
            }
            buf.append(name);
        }
        this
                .__(Grid.START_ROW)
                .__(Grid.labelCells("_ label.multi.centralspheres"))
                .__(Grid.START_INPUTCELL)
                .__escape(buf.toString())
                .__(Grid.END_INPUTCELL)
                .__(Grid.END_ROW);
        return true;
    }

    private boolean printFichothequeTest() {
        switch (multi.getAuthentificationSharing()) {
            case MultiConstants.STRICT_SHARING: {
                Set<String> centralSphereNameSet = multi.getCentralSphereNameSet();
                if (centralSphereNameSet.isEmpty()) {
                    this
                            .P("global-WarningMessage")
                            .__localize("_ warning.multi.nocentralsphere")
                            ._P();
                    printFichothequeCreation();
                    return true;
                } else {
                    return false;
                }
            }
            default:
                return printFichothequeCreation();
        }
    }

    private boolean printFichothequeCreation() {
        if (!multi.isEmpty()) {
            return false;
        }
        boolean checked = requestMap.isTrue(IniCommand.CREATEFICHOTHEQUE_PARAMNAME);
        String targetId = generateId();
        this
                .DIV("multiini-Fichotheque")
                .__(Grid.START)
                .__(Grid.checkboxRow("_ label.multi.createfichotheque",
                        name(IniCommand.CREATEFICHOTHEQUE_PARAMNAME).checked(checked).value("1").populate(Deploy.checkbox(targetId)),
                        () -> {
                            this
                                    .DIV(Grid.detailPanelTable().id(targetId).addClass(!checked, "hidden"))
                                    .__(Grid.textInputRow("_ label.multi.fichothequename", input(FichothequeCreationCommand.NEWFICHOTHEQUE_PARAMNAME).populate(InputPattern.TECHNICAl_UNDERSCORE).populate(Deploy.REQUIRED)))
                                    .__(Grid.textInputRow("_ label.multi.firstsphere", input(FichothequeCreationCommand.FIRSTSPHERE_PARAMNAME).populate(InputPattern.TECHNICAL_STRICT).populate(Deploy.REQUIRED)))
                                    .__(Grid.textInputRow("_ label.multi.firstuser", input(FichothequeCreationCommand.FIRSTUSER_PARAMNAME).populate(InputPattern.LOGIN).populate(Deploy.REQUIRED)))
                                    .__(Grid.passwordInputRow("_ label.multi.firstpassword_1", name(FichothequeCreationCommand.FIRSTPASSWORD1_PARAMNAME).size("15").populate(Deploy.REQUIRED)))
                                    .__(Grid.passwordInputRow("_ label.multi.firstpassword_2", name(FichothequeCreationCommand.FIRSTPASSWORD2_PARAMNAME).size("15").populate(Deploy.REQUIRED)))
                                    ._DIV();
                        }))
                .__(Grid.END)
                ._DIV();

        return true;
    }

    private Set<String> getCentralSphereNameSet() {
        switch (multi.getAuthentificationSharing()) {
            case MultiConstants.STRICT_SHARING: {
                return multi.getCentralSphereNameSet();
            }
            default:
                return null;
        }
    }

    private HtmlAttributes input(String name) {
        HtmlAttributes attr = name(name);
        String value = requestMap.getParameter(name);
        if (value == null) {
            value = getDefault(name);
        } else {
            value = value.trim();
        }
        return attr.value(value).size(getSize(name));
    }

    private String getDefault(String name) {
        switch (name) {
            case IniCommand.AUTHORITY_PARAMNAME:
                return "localhost";
            case IniCommand.LOGIN_PARAMNAME:
                return "admin";
            case FichothequeCreationCommand.FIRSTSPHERE_PARAMNAME:
                return "admin";
            case FichothequeCreationCommand.FIRSTUSER_PARAMNAME:
                return "ADMIN";
            case IniCommand.LANG_PARAMNAME:
                return RequestUtils.getDefaultLang(requestMap).toString();
            default:
                return "";
        }
    }

    private String getSize(String name) {
        switch (name) {
            case IniCommand.LANG_PARAMNAME:
                return "5";
            default:
                return "15";
        }
    }

}
