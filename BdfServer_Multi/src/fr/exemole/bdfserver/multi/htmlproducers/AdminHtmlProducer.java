/* BdfServer_Multi - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.html.jslib.MultiJsLibs;
import fr.exemole.bdfserver.multi.MultiUtils;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralSphere;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;


/**
 *
 * @author Vincent Calame
 */
public class AdminHtmlProducer extends MultiHtmlProducer {

    public AdminHtmlProducer(Multi multi) {
        super(multi);
        addJsLib(MultiJsLibs.ADMIN);
        addThemeCss("pane.css", "multi.css");
    }

    @Override
    public void printHtml() {
        Set<String> centralSphereNameSet = multi.getCentralSphereNameSet();
        List<JsObject> centralSphereList = new ArrayList<JsObject>();
        for (String sphereName : centralSphereNameSet) {
            CentralSphere centralSphere = multi.getCentralSphere(sphereName);
            centralSphereList.add(JsObject.init()
                    .put("name", sphereName)
                    .put("title", MultiUtils.getTitle(centralSphere.getMetadata(), workingLang)));
        }
        JsObject adminArgs = JsObject.init()
                .put("authentificationSharing", multi.getAuthentificationSharing())
                .put("personManagementAllowed", multi.isPersonManagementAllowed())
                .put("centralSphereArray", centralSphereList);
        startLoc("_ title.multi.index");
        this
                .SCRIPT()
                .__jsObject("MultiAdmin.ARGS", adminArgs)
                ._SCRIPT();
        this
                .DIV(HA.id("layout"))._DIV();
        end();
    }

}
