/* BdfServer_Multi - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.htmlproducers;

import fr.exemole.bdfserver.api.interaction.InteractionConstants;
import fr.exemole.bdfserver.html.consumers.PageUnit;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.commands.IniCommand;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlConstants;


/**
 *
 * @author Vincent Calame
 */
public class IniResultHtmlProducer extends MultiHtmlProducer {

    private final IniCommand.Result result;

    public IniResultHtmlProducer(Multi multi, IniCommand.Result result) {
        super(multi);
        this.result = result;
        addThemeCss("multiini.css");
    }

    @Override
    public void printHtml() {
        startLoc("_ title.multi.iniresult");
        this
                .MAIN("multiini-Main")
                .__(PageUnit.start("_ title.multi.iniresult"))
                .__(printCommandMessage())
                .__(printAdminLink())
                .__(printFirstFichothequeLink())
                .__(PageUnit.END)
                ._MAIN();
        end();
    }

    private boolean printAdminLink() {
        String target = null;
        if (result.isWithNewFichotheque()) {
            target = HtmlConstants.BLANK_TARGET;
        }
        this
                .P()
                .A(HA.href("multi-admin?login=" + result.getAdminLogin()).target(target))
                .__localize("_ link.multi.admin")
                ._A()
                ._P();
        return true;
    }

    private boolean printFirstFichothequeLink() {
        if (!result.isWithNewFichotheque()) {
            return false;
        }
        String newFichothequeName = result.getNewFichothequeName();
        this
                .P()
                .A(HA.href(newFichothequeName + "/session?" + InteractionConstants.BDF_DEFAULT_LOGIN_PARAMNAME + "=" + result.getStartValues().firstuser()).target(HtmlConstants.BLANK_TARGET))
                .__localize("_ link.multi.fichothequecreated", newFichothequeName)
                ._A()
                ._P();
        return true;
    }

}
