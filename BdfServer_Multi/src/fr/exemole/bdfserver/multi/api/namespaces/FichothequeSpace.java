/* BdfServer_Multi - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api.namespaces;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class FichothequeSpace {

    public final static CheckedNameSpace FICHOTHEQUE_NAMESPACE = CheckedNameSpace.build("fichotheque");
    public final static AttributeKey INACTIVE_KEY = AttributeKey.build(FICHOTHEQUE_NAMESPACE, "inactive");

    private FichothequeSpace() {

    }

}
