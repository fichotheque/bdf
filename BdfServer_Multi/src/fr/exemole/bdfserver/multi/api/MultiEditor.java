/* BdfServer_Multi - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api;

import fr.exemole.bdfserver.multi.api.central.CentralSphereEditor;
import net.fichotheque.EditOrigin;


/**
 *
 * @author Vincent Calame
 */
public interface MultiEditor {

    public Multi getMulti();

    public EditOrigin getEditOrigin();

    public MultiMetadataEditor getMultiMetadataEditor();

    public CentralSphereEditor getCentralSphereEditor(String sphereName);

    public void saveChanges();

}
