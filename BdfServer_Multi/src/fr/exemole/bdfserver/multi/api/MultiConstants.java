/* BdfServer_Multi - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api;


/**
 *
 * @author Vincent Calame
 */
public interface MultiConstants {

    public final static String NONE_SHARING = "none";
    public final static String LAX_SHARING = "lax";
    public final static String STRICT_SHARING = "strict";
    public final static String LOGIN_PAGE = "login";
    public final static String INDEX_PAGE = "index";
    public final static String FICHOTHEQUE_ARRAY_JSON = "fichotheque-array";
    public final static String MULTIMETADATA_JSON = "multimetadata";
    public final static String CENTRALSPHEREMETADATA_JSON = "centralspheremetadata";
    public final static String CENTRALSPHERESTATS_JSON = "centralspherestats";
    public final static String PERSON_JSON = "person";
    public final static String PING_JSON = "ping";
    public final static String SYNTHESIS_STREAM = "synthesis";
    public final static String RELOAD_PARAMNAME = "reload";
    public final static String PRESENCE_JSON = "presence";
    public final static String USER_ARRAY_JSON = "user-array";
    public final static String USER_JSON = "user";
    public final static String USERSTATE_JSON = "userstate";
    public final static String UNKNOWN_FICHOTHEQUE_ERROR = "_ error.unknown.fichotheque";
    public final static String INACTIVE_FICHOTHEQUE_ERROR = "_ error.unsupported.inactivefichotheque";
}
