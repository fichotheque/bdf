/* BdfServer_Multi - Copyright (c) 2018-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import fr.exemole.bdfserver.conf.ConfDirs;
import fr.exemole.bdfserver.multi.MultiConf;
import fr.exemole.bdfserver.multi.api.central.CentralSphere;
import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.multi.commands.CreationParameters;
import java.util.Set;
import net.fichotheque.EditOrigin;
import net.fichotheque.sphere.LoginKey;
import net.mapeadores.util.buildinfo.BuildInfo;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.jslib.JsAnalyser;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.localisation.MessageLocalisationProvider;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.mimetype.MimeTypeResolver;


/**
 *
 * @author Vincent Calame
 */
public interface Multi {

    public final static String ACTIVE_STATE = "active";
    public final static String INACTIVE_STATE = "inactive";
    public final static String NOT_INIT_STATE = "not_init";
    public final static String NOT_EXISTING_STATE = "not_existing";


    public MultiConf getMultiConf();

    public String getServletContextName();

    public String getAuthentificationSharing();

    public ResourceStorages getWebappsResourceStorages();

    public MimeTypeResolver getMimeTypeResolver();

    public JsAnalyser getWebappsJsAnalyser();

    public Set<String> getExistingNameSet();

    public String getState(String fichothequeName);

    public BdfServer getBdfServer(String fichothequeName) throws ErrorMessageException;

    public BdfServerDirs getBdfServerDirs(String fichothequeName);

    public ConfDirs getMultiDirs();

    public void createFichotheque(CreationParameters creationParameters);

    public BuildInfo getBuildInfo();

    public MultiMetadata getMultiMetadata();

    public MultiEditor getMultiEditor(EditOrigin editOrigin);

    public MessageLocalisationProvider getMessageLocalisationProvider();

    public Set<String> getCentralSphereNameSet();

    public CentralSphere getCentralSphere(String name);

    public default boolean isInit(String fichothequeName) {
        return (getState(fichothequeName).equals(ACTIVE_STATE));
    }

    public default Lang getAdminLang() {
        return getMultiMetadata().getWorkingLangs().get(0);
    }

    public default MessageLocalisation getAdminMessageLocalisation() {
        return getMessageLocalisationProvider().getMessageLocalisation(getAdminLang());
    }

    public default boolean isEmpty() {
        return getExistingNameSet().isEmpty();
    }

    public default boolean containsFichotheque(String fichothequeName) {
        return getExistingNameSet().contains(fichothequeName);
    }

    public default boolean isAuthentificationSharingAllowed(LoginKey loginKey) {
        switch (getAuthentificationSharing()) {
            case MultiConstants.STRICT_SHARING: {
                CentralSphere centralSphere = getCentralSphere(loginKey.getSphereName());
                if (centralSphere != null) {
                    CentralUser centralUser = centralSphere.getCentralUser(loginKey.getLogin());
                    if ((centralUser != null) && (!centralUser.isInactive())) {
                        return true;
                    }
                }
                return false;
            }
            case MultiConstants.LAX_SHARING:
                return true;
            default:
                return false;
        }
    }

    public default boolean isPersonManagementAllowed() {
        switch (getAuthentificationSharing()) {
            case MultiConstants.STRICT_SHARING:
            case MultiConstants.LAX_SHARING:
                return true;
            default:
                return false;
        }
    }

    public default BdfServer getInitBdfServer(String name) {
        if (isInit(name)) {
            try {
                return getBdfServer(name);
            } catch (ErrorMessageException eme) {
                throw new IllegalStateException("Test done before");
            }
        } else {
            return null;
        }
    }

}
