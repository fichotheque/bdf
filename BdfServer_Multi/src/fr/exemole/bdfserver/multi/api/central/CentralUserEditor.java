/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api.central;

import java.util.Locale;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
public interface CentralUserEditor {

    public boolean setStatus(String status);

    public boolean setPerson(PersonCore person);

    public boolean setEmail(EmailCore email);

    public boolean setLangContext(Lang workingLang, Locale customFormatLocale, LangPreference customLangPreference);

    public boolean setEncryptedPassword(String encryptedPassword);

    public boolean putAttribute(Attribute attribute);

    public boolean removeAttribute(AttributeKey attributeKey);

}
