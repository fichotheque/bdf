/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api.central;

import java.util.Locale;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.sphere.LoginKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.UserLangContext;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.PersonCore;


/**
 *
 * @author Vincent Calame
 */
public interface CentralUser {

    public LoginKey getLoginKey();

    public UserLangContext getUserLangContext();

    public PersonCore getPerson();

    public EmailCore getEmail();

    public String getStatus();

    public String getEncryptedPassword();

    public Attributes getAttributes();

    public Locale getCustomFormatLocale();

    public LangPreference getCustomLangPreference();

    public default String getCompleteName() {
        return getPerson().toStandardStyle();
    }

    public default boolean isInactive() {
        return getStatus().equals(FichothequeConstants.INACTIVE_STATUS);
    }

    public default String getLogin() {
        return getLoginKey().getLogin();
    }

}
