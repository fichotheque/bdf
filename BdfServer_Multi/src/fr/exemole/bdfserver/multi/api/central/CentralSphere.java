/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api.central;

import java.util.Set;
import net.fichotheque.Metadata;
import net.fichotheque.SubsetKey;


/**
 *
 * @author Vincent Calame
 */
public interface CentralSphere {

    public default String getName() {
        return getSphereKey().getSubsetName();
    }

    public SubsetKey getSphereKey();

    public Metadata getMetadata();

    public Set<String> getLoginSet();

    public default boolean hasCentralUser(String login) {
        return (getCentralUser(login) != null);
    }

    public CentralUser getCentralUser(String login);

}
