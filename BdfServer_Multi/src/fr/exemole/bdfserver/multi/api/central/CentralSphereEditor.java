/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api.central;

import net.fichotheque.MetadataEditor;


/**
 *
 * @author Vincent Calame
 */
public interface CentralSphereEditor {

    public CentralUserEditor createCentralUser(String login);

    public MetadataEditor getMetadataEditor();

    public CentralUserEditor getCentralUserEditor(String login);

}
