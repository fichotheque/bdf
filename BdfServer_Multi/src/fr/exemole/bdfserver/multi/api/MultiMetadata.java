/* BdfServer_Multi - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.api;

import net.fichotheque.Metadata;
import net.mapeadores.util.localisation.Langs;


/**
 *
 * @author Vincent Calame
 */
public interface MultiMetadata extends Metadata {

    public Langs getWorkingLangs();

    public String getAuthority();

}
