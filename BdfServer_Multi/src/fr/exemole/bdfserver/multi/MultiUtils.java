/* BdfServer_Multi - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.conf.ConfUtils;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralSphere;
import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageDirectories;
import fr.exemole.bdfserver.storage.directory.bdfdata.StorageFile;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import fr.exemole.bdfserver.tools.synthesis.SphereSynthesis;
import fr.exemole.bdfserver.tools.synthesis.Synthesis;
import net.fichotheque.Metadata;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public final class MultiUtils {

    private MultiUtils() {

    }

    public static String getTitle(Metadata metadata, Lang lang) {
        Label label = metadata.getTitleLabels().getLangPartCheckedLabel(lang);
        if (label != null) {
            return label.getLabelString();
        }
        return null;
    }

    public static String getMandatory(RequestMap requestMap, String paramName) throws ErrorMessageException {
        return getMandatory(requestMap, paramName, false);
    }

    public static String getMandatory(RequestMap requestMap, String paramName, boolean allowEmpty) throws ErrorMessageException {
        String paramValue = requestMap.getParameter(paramName);
        if (paramValue == null) {
            throw BdfErrors.emptyMandatoryParameter(paramName);
        } else if (allowEmpty) {
            return paramValue;
        } else {
            paramValue = paramValue.trim();
            if (paramValue.length() == 0) {
                throw BdfErrors.emptyMandatoryParameter(paramName);
            }
            return paramValue;
        }
    }

    public static String getFichothequeName(RequestMap requestMap, String paramName) throws ErrorMessageException {
        String paramValue = requestMap.getParameter(paramName);
        if (paramValue == null) {
            throw BdfErrors.emptyMandatoryParameter(paramName);
        }
        paramValue = paramValue.trim();
        if (paramValue.length() == 0) {
            throw new ErrorMessageException("_ error.empty.fichothequename");
        }
        if (!ConfUtils.isValidMultiName(paramValue)) {
            throw new ErrorMessageException("_ error.wrong.basename_fichotheque", paramName);
        }
        return paramValue;
    }

    public static Synthesis getSynthesis(Multi multi) {
        Synthesis.Builder builder = new Synthesis.Builder();
        for (String name : multi.getExistingNameSet()) {
            BdfServer bdfServer = multi.getInitBdfServer(name);
            if (bdfServer != null) {
                builder.addFichotheque(bdfServer);
            }
        }
        return builder.toSynthesis();
    }

    public static SphereSynthesis getSphereSynthesis(Multi multi, SubsetKey sphereKey) {
        SphereSynthesis sphereSynthesis = new SphereSynthesis(sphereKey);
        for (String name : multi.getExistingNameSet()) {
            BdfServer bdfServer = multi.getInitBdfServer(name);
            if (bdfServer != null) {
                Sphere sphere = (Sphere) bdfServer.getFichotheque().getSubset(sphereKey);
                if (sphere != null) {
                    sphereSynthesis.addSphere(bdfServer, sphere);
                }
            }
        }
        return sphereSynthesis;
    }

    public static StorageFile getMetadataStorageFile(StorageDirectories storageDirectories) {
        String path = "metadata.xml";
        return StorageFile.build(storageDirectories, path);
    }

    public static CentralSphere getCentralSphere(Multi multi, RequestMap requestMap) throws ErrorMessageException {
        String sphereName = getMandatory(requestMap, "sphere");
        CentralSphere centralSphere = multi.getCentralSphere(sphereName);
        if (centralSphere != null) {
            return centralSphere;
        } else {
            throw BdfErrors.unknownParameterValue("sphere", sphereName);
        }
    }

    public static CentralUser getCentralUser(Multi multi, RequestMap requestMap) throws ErrorMessageException {
        return getCentralUser(multi, requestMap, getCentralSphere(multi, requestMap));
    }

    public static CentralUser getCentralUser(Multi multi, RequestMap requestMap, CentralSphere centralSphere) throws ErrorMessageException {
        String login = getMandatory(requestMap, "login");
        CentralUser centralUser = centralSphere.getCentralUser(login);
        if (centralUser != null) {
            return centralUser;
        } else {
            throw BdfErrors.unknownParameterValue("login", login);
        }
    }

}
