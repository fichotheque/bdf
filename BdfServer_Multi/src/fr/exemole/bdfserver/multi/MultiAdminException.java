/* BdfServer_Multi - Copyright (c) 2015-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi;

import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LogUtils;


/**
 *
 * @author Vincent Calame
 */
public class MultiAdminException extends Exception {

    public final static String MISSING_PARAMETER = "_ ";
    public final static String WRONG_PARAMETER = "_ ";
    private final CommandMessage commandMessage;

    public MultiAdminException(String messageKey, Object value) {
        super(messageKey);
        commandMessage = LogUtils.error(messageKey, value);
    }

    public CommandMessage getCommandMessage() {
        return commandMessage;
    }

}
