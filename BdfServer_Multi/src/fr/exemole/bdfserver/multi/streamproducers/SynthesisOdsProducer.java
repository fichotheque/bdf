/* BdfServer_Multi - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.streamproducers;

import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.tools.BdfServerUtils;
import fr.exemole.bdfserver.tools.synthesis.Synthesis;
import fr.exemole.bdfserver.tools.synthesis.SynthesisOds;
import java.io.IOException;
import java.io.OutputStream;
import net.mapeadores.opendocument.io.odtable.OdsOptions;
import net.mapeadores.util.io.StreamProducer;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class SynthesisOdsProducer implements StreamProducer {

    private final Multi multi;
    private final Synthesis synthesis;

    public SynthesisOdsProducer(Multi multi, Synthesis synthesis) {
        this.multi = multi;
        this.synthesis = synthesis;
    }

    @Override
    public String getMimeType() {
        return MimeTypeConstants.ODS;
    }

    @Override
    public String getCharset() {
        return null;
    }

    @Override
    public String getFileName() {
        return "synthesis.ods";
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        OdsOptions odsOptions = BdfServerUtils.buildOdsOptions(multi.getWebappsResourceStorages(), RelativePath.build("css/ods/synthesis.css"));
        SynthesisOds synthesisOds = new SynthesisOds(synthesis, odsOptions, multi.getAdminLang(), multi.getAdminMessageLocalisation());
        synthesisOds.write(outputStream);
    }

}
