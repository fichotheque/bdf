/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.tools.PresenceInfo;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.util.List;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.security.PasswordChecker;


/**
 *
 * @author Vincent Calame
 */
public class CentralPasswordChangeCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "CentralPasswordChange";
    public final static String PASSWORD1_PARAMNAME = "password1";
    public final static String PASSWORD2_PARAMNAME = "password2";

    public CentralPasswordChangeCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initCentralUser();
        String password = getPassword();
        String encryptedPassword = PasswordChecker.getHash(PasswordChecker.PBKDF2, password);
        initCentralEditors();
        boolean done;
        synchronized (getCentralSphere()) {
            done = getCentralUserEditor().setEncryptedPassword(encryptedPassword);
            saveChanges();
        }
        if (done) {
            List<PresenceInfo> presenceInfoList = PresenceInfo.scan(multi, centralUser, false);
            for (PresenceInfo presenceInfo : presenceInfoList) {
                Redacteur redacteur = presenceInfo.getRedacteur();
                if (redacteur != null) {
                    BdfServer currentBdfServer = presenceInfo.getBdfServer();
                    synchronized (currentBdfServer) {
                        currentBdfServer.getPasswordManager().setEncryptedPassword(redacteur.getGlobalId(), encryptedPassword);
                    }
                }
            }
        }
        if (done) {
            return done("_ done.sphere.passwordchange");
        } else {
            return null;
        }
    }

    private String getPassword() throws ErrorMessageException {
        String mdp1 = getMandatory(PASSWORD1_PARAMNAME);
        String mdp2 = getMandatory(PASSWORD2_PARAMNAME);
        if (!mdp1.equals(mdp2)) {
            throw BdfErrors.error("_ error.wrong.password_different");
        }
        String password = mdp1;
        if (password.length() == 0) {
            throw BdfErrors.error("_ error.empty.password");
        }
        if (password.length() < 4) {
            throw BdfErrors.error("_ error.wrong.password_tooshort");
        }
        return password;
    }

}
