/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.multi.api.Multi;
import static fr.exemole.bdfserver.multi.commands.central.CentralUserCreationCommand.EMAIL_PARAMNAME;
import fr.exemole.bdfserver.multi.tools.PresenceInfo;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import java.util.List;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.EmailCoreUtils;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class CentralEmailChangeCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "CentralEmailChange";

    public CentralEmailChangeCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initCentralUser();
        EmailCore newEmail = getEmail();
        initCentralEditors();
        boolean done;
        synchronized (getCentralSphere()) {
            done = getCentralUserEditor().setEmail(newEmail);
            saveChanges();
        }
        if (done) {
            List<PresenceInfo> presenceInfoList = PresenceInfo.scan(multi, centralUser, false);
            for (PresenceInfo presenceInfo : presenceInfoList) {
                Redacteur redacteur = presenceInfo.getRedacteur();
                if (redacteur != null) {
                    BdfServer currentBdfServer = presenceInfo.getBdfServer();
                    synchronized (currentBdfServer) {
                        try (EditSession editSession = newEditSession(currentBdfServer, "CentralEmailChange")) {
                            SphereEditor sphereEditor = editSession.getFichothequeEditor().getSphereEditor(redacteur.getSubsetKey());
                            sphereEditor.setEmail(redacteur, newEmail);
                        }
                    }
                }
            }
        }
        if (done) {
            return done("_ done.sphere.emailchange");
        } else {
            return null;
        }
    }


    private EmailCore getEmail() throws ErrorMessageException {
        String email = StringUtils.cleanString(requestMap.getParameter(EMAIL_PARAMNAME));
        if ((email == null) || (email.length() == 0)) {
            return null;
        } else {
            try {
                return EmailCoreUtils.parse(email);
            } catch (ParseException pe) {
                throw BdfErrors.error("_ error.wrong.email", email);
            }
        }
    }

}
