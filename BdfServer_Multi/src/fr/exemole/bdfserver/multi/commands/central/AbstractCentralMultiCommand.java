/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.MultiEditor;
import fr.exemole.bdfserver.multi.api.central.CentralSphere;
import fr.exemole.bdfserver.multi.api.central.CentralSphereEditor;
import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.multi.api.central.CentralUserEditor;
import fr.exemole.bdfserver.multi.commands.AbstractMultiCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.EditOrigin;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.sphere.SphereEditor;
import net.fichotheque.utils.EditOriginUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractCentralMultiCommand extends AbstractMultiCommand {

    public final static String SPHERE_PARAMNAME = "sphere";
    public final static String LOGIN_PARAMNAME = "login";
    public final static String FICHOTHEQUE_PARAMNAME = "fichotheque";
    public final static String STATUS_PARAMNAME = "status";
    public final static String ADMIN_STATUS_PARAMVALUE = "admin";
    public final static String USER_STATUS_PARAMVALUE = "user";
    public final static String READONLY_STATUS_PARAMVALUE = "readonly";
    public final static String INACTIVE_STATUS_PARAMVALUE = "inactive";
    protected CentralSphere centralSphere;
    protected CentralUser centralUser;
    protected MultiEditor multiEditor;
    protected CentralSphereEditor centralSphereEditor;
    protected BdfServer bdfServer;

    public AbstractCentralMultiCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    public void initCentralSphere() throws ErrorMessageException {
        String sphereName = getMandatory(SPHERE_PARAMNAME);
        this.centralSphere = multi.getCentralSphere(sphereName);
        if (centralSphere == null) {
            throw new ErrorMessageException("_ error.unknown.sphere", sphereName);
        }
    }

    public void initCentralUser() throws ErrorMessageException {
        String login = getMandatory(LOGIN_PARAMNAME);
        this.centralUser = centralSphere.getCentralUser(login);
        if (centralUser == null) {
            throw BdfErrors.unknownParameterValue("login", login);
        }
    }

    public void initCentralEditors() {
        EditOrigin editOrigin = EditOriginUtils.newEditOrigin("multi-admin/centralsphere");
        this.multiEditor = multi.getMultiEditor(editOrigin);
        this.centralSphereEditor = multiEditor.getCentralSphereEditor(centralSphere.getName());
    }

    public CentralUserEditor getCentralUserEditor() {
        return centralSphereEditor.getCentralUserEditor(centralUser.getLogin());
    }

    public void initBdfServer(boolean spherePresence) throws ErrorMessageException {
        String fichothequeName = getMandatory(FICHOTHEQUE_PARAMNAME);
        bdfServer = multi.getBdfServer(fichothequeName);
        boolean here = bdfServer.getFichotheque().containsSubset(centralSphere.getSphereKey());
        if (spherePresence) {
            if (!here) {
                throw BdfErrors.error("_ error.unknown.sphere", centralSphere.getName());
            }
        } else {
            if (here) {
                throw BdfErrors.error("_ error.existing.sphere", centralSphere.getName());
            }
        }
    }

    public void saveChanges() {
        this.multiEditor.saveChanges();
    }

    public CentralSphere getCentralSphere() {
        return centralSphere;
    }

    public BdfServer getBdfServer() {
        return bdfServer;
    }

    public Redacteur getRedacteur() throws ErrorMessageException {
        Sphere sphere = (Sphere) bdfServer.getFichotheque().getSubset(centralSphere.getSphereKey());
        Redacteur redacteur = sphere.getRedacteurByLogin(centralUser.getLogin());
        if (redacteur == null) {
            throw BdfErrors.error("_ error.unknown.redacteur", centralUser.getLogin());
        }
        return redacteur;
    }

    public EditSession newEditSession(BdfServer bdfServer, String source) {
        return bdfServer.initEditSession(EditOriginUtils.newEditOrigin(null, "_admin", source));
    }

    public EditSession newEditSession(String source) {
        return newEditSession(bdfServer, source);
    }

    public void changeStatus(Redacteur redacteur, SphereEditor sphereEditor, EditSession editSession) {
        String status = requestMap.getParameter(STATUS_PARAMNAME);
        if (status != null) {
            changeStatus(status, redacteur, sphereEditor, editSession);
        }
    }

    public void changeStatus(String status, Redacteur redacteur, SphereEditor sphereEditor, EditSession editSession) {
        boolean isAdmin = editSession.getBdfServer().getPermissionManager().isAdmin(redacteur);
        if (isAdmin) {
            if (status.equals(ADMIN_STATUS_PARAMVALUE)) {
                return;
            } else {
                editSession.getBdfServerEditor().setAdmin(redacteur.getGlobalId(), false);
            }
        }
        switch (status) {
            case READONLY_STATUS_PARAMVALUE:
                sphereEditor.setStatus(redacteur, FichothequeConstants.READONLY_STATUS);
                break;
            case INACTIVE_STATUS_PARAMVALUE:
                sphereEditor.setStatus(redacteur, FichothequeConstants.INACTIVE_STATUS);
                break;
            case USER_STATUS_PARAMVALUE:
                sphereEditor.setStatus(redacteur, FichothequeConstants.ACTIVE_STATUS);
                break;
            case ADMIN_STATUS_PARAMVALUE:
                sphereEditor.setStatus(redacteur, FichothequeConstants.ACTIVE_STATUS);
                editSession.getBdfServerEditor().setAdmin(redacteur.getGlobalId(), true);
                break;
        }

    }


}
