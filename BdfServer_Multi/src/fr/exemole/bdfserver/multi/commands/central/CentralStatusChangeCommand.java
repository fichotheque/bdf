/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.managers.PermissionManager;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.tools.PresenceInfo;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.sphere.Redacteur;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CentralStatusChangeCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "CentralStatusChange";

    public CentralStatusChangeCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initCentralUser();
        String status = getStatus();
        List<PresenceInfo> presenceInfoList = PresenceInfo.scan(multi, centralUser, false);
        if (status.equals(FichothequeConstants.INACTIVE_STATUS)) {
            checkUniqueAdmin(presenceInfoList);
        }
        initCentralEditors();
        boolean done;
        synchronized (getCentralSphere()) {
            done = getCentralUserEditor().setStatus(status);
            saveChanges();
        }
        if ((done) && (status.equals(FichothequeConstants.INACTIVE_STATUS))) {
            for (PresenceInfo presenceInfo : presenceInfoList) {
                Redacteur redacteur = presenceInfo.getRedacteur();
                if ((redacteur != null) && (!redacteur.getStatus().equals(status))) {
                    BdfServer currentBdfServer = presenceInfo.getBdfServer();
                    synchronized (currentBdfServer) {
                        try (EditSession editSession = newEditSession(currentBdfServer, "CentralStatusChange")) {
                            if (currentBdfServer.getPermissionManager().isAdmin(redacteur)) {
                                editSession.getBdfServerEditor().setAdmin(redacteur.getGlobalId(), false);
                            }
                            editSession.getFichothequeEditor().getSphereEditor(redacteur.getSubsetKey()).setStatus(redacteur, status);
                        }
                    }
                }
            }
        }
        if (done) {
            return done("_ done.sphere.redacteurstatus");
        } else {
            return null;
        }
    }

    private String getStatus() throws ErrorMessageException {
        String status = getMandatory(STATUS_PARAMNAME);
        switch (status) {
            case FichothequeConstants.ACTIVE_STATUS:
            case FichothequeConstants.READONLY_STATUS:
            case FichothequeConstants.INACTIVE_STATUS:
                return status;
            default:
                throw BdfErrors.unknownParameterValue(STATUS_PARAMNAME, status);
        }
    }

    private void checkUniqueAdmin(List<PresenceInfo> presenceInfoList) throws ErrorMessageException {
        for (PresenceInfo presenceInfo : presenceInfoList) {
            Redacteur redacteur = presenceInfo.getRedacteur();
            if (redacteur != null) {
                BdfServer currentBdfServer = presenceInfo.getBdfServer();
                PermissionManager permissionManager = currentBdfServer.getPermissionManager();
                if ((permissionManager.isAdmin(redacteur) && (permissionManager.getAdminRedacteurList().size() < 2))) {
                    throw new ErrorMessageException("_ error.unsupported.lastadmin_fichotheque", currentBdfServer.getName());
                }
            }
        }
    }

}
