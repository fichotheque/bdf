/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.api.managers.PermissionManager;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class StatusInFichothequeChangeCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "StatusInFichothequeChange";

    public StatusInFichothequeChangeCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);

    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initCentralUser();
        initBdfServer(true);
        Redacteur redacteur = getRedacteur();
        checkLastAdmin(redacteur);
        synchronized (getBdfServer()) {
            try (EditSession editSession = newEditSession(COMMAND_NAME)) {
                SphereEditor sphereEditor = editSession.getFichothequeEditor().getSphereEditor(centralSphere.getSphereKey());
                changeStatus(redacteur, sphereEditor, editSession);
            }
        }
        return done("_ done.sphere.redacteurstatus");
    }

    private void checkLastAdmin(Redacteur redacteur) throws ErrorMessageException {
        PermissionManager permissionManager = bdfServer.getPermissionManager();
        if (permissionManager.isAdmin(redacteur)) {
            if (permissionManager.getAdminRedacteurList().size() == 1) {
                throw BdfErrors.error("_ error.unsupported.lastadmin", redacteur.getLogin());
            }
        }
    }

}
