/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import net.fichotheque.ExistingIdException;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class AddToFichothequeCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "AddToFichotheque";

    public AddToFichothequeCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initCentralUser();
        initBdfServer(true);
        synchronized (getBdfServer()) {
            try (EditSession editSession = newEditSession(COMMAND_NAME)) {
                SphereEditor sphereEditor = editSession.getFichothequeEditor().getSphereEditor(centralSphere.getSphereKey());
                Redacteur newRedacteur = createRedacteur(sphereEditor, centralUser);
                sphereEditor.setPerson(newRedacteur, centralUser.getPerson());
                sphereEditor.setEmail(newRedacteur, centralUser.getEmail());
                bdfServer.getPasswordManager().setEncryptedPassword(newRedacteur.getGlobalId(), centralUser.getEncryptedPassword());
                changeStatus(newRedacteur, sphereEditor, editSession);
            }
        }
        return done("_ done.sphere.redacteurcreation", centralUser.getLogin());
    }

    private Redacteur createRedacteur(SphereEditor sphereEditor, CentralUser centralUser) throws ErrorMessageException {
        try {
            return sphereEditor.createRedacteur(-1, centralUser.getLogin());
        } catch (ExistingIdException eie) {
            throw BdfErrors.error("_ error.existing.login", centralUser.getLogin());
        } catch (ParseException pe) {
            throw new ShouldNotOccurException("Login from central user");
        }
    }

}
