/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.tools.PresenceInfo;
import java.util.List;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.SphereEditor;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.models.PersonCore;
import net.mapeadores.util.models.PersonCoreUtils;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class CentralPersonChangeCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "CentralPersonChange";
    public final static String SURNAME_PARAMNAME = "surname";
    public final static String NONLATIN_PARAMNAME = "nonlatin";
    public final static String FORENAME_PARAMNAME = "forename";
    public final static String SURNAMEFIRST_PARAMNAME = "surnamefirst";

    public CentralPersonChangeCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initCentralUser();
        PersonCore newPerson = getPerson();
        initCentralEditors();
        boolean done;
        synchronized (getCentralSphere()) {
            done = getCentralUserEditor().setPerson(newPerson);
            saveChanges();
        }
        if (done) {
            List<PresenceInfo> presenceInfoList = PresenceInfo.scan(multi, centralUser, false);
            for (PresenceInfo presenceInfo : presenceInfoList) {
                Redacteur redacteur = presenceInfo.getRedacteur();
                if (redacteur != null) {
                    BdfServer currentBdfServer = presenceInfo.getBdfServer();
                    synchronized (currentBdfServer) {
                        try (EditSession editSession = newEditSession(currentBdfServer, "CentralPersonChange")) {
                            SphereEditor sphereEditor = editSession.getFichothequeEditor().getSphereEditor(redacteur.getSubsetKey());
                            sphereEditor.setPerson(redacteur, newPerson);
                        }
                    }
                }
            }
        }
        if (done) {
            return done("_ done.sphere.redacteurchange");
        } else {
            return null;
        }
    }

    private PersonCore getPerson() throws ErrorMessageException {
        String surname = getMandatory(SURNAME_PARAMNAME);
        surname = StringUtils.cleanString(surname);
        String forename = StringUtils.cleanString(requestMap.getParameter(FORENAME_PARAMNAME));
        if (forename == null) {
            forename = "";
        }
        String nonlatin = StringUtils.cleanString(requestMap.getParameter(NONLATIN_PARAMNAME));
        if (nonlatin == null) {
            nonlatin = "";
        }
        boolean surnameFirst = requestMap.isTrue(SURNAMEFIRST_PARAMNAME);
        return PersonCoreUtils.toPersonCore(surname, forename, nonlatin, surnameFirst);
    }

}
