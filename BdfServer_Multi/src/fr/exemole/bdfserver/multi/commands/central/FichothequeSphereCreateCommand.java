/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.ExistingSubsetException;
import net.fichotheque.sphere.SphereEditor;
import net.fichotheque.sphere.metadata.SphereMetadataEditor;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeSphereCreateCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "FichothequeSphereCreate";

    public FichothequeSphereCreateCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);

    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initBdfServer(false);
        synchronized (getBdfServer()) {
            try (EditSession editSession = newEditSession(COMMAND_NAME)) {
                try {
                    SphereEditor sphereEditor = editSession.getFichothequeEditor().createSphere(centralSphere.getSphereKey());
                    SphereMetadataEditor sphereMetadataEditor = sphereEditor.getSphereMetadataEditor();
                    for (Label label : centralSphere.getMetadata().getTitleLabels()) {
                        sphereMetadataEditor.putLabel(null, label);
                    }
                } catch (ExistingSubsetException ese) {
                    throw BdfErrors.error("_ error.existing.sphere", centralSphere.getName());
                }
            }
        }
        return done("done.sphere.spherecreation", centralSphere.getName());
    }

}
