/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import java.util.HashMap;
import java.util.Map;
import net.fichotheque.MetadataEditor;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.LabelChange;


/**
 *
 * @author Vincent Calame
 */
public class CentralSphereMetadataCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "CentralSphereMetadata";
    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";

    public CentralSphereMetadataCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initCentralSphere();
        initCentralEditors();
        LabelChange titleLabelChange = BdfCommandUtils.getLabelChange(requestMap, TITLE_PARAMPREFIX, true);
        Map<String, LabelChange> phraseChangeMap = new HashMap<String, LabelChange>();
        BdfCommandUtils.populatePhraseLabelChange(requestMap, phraseChangeMap);
        String attributes = requestMap.getParameter(ATTRIBUTES_PARAMNAME);
        AttributeChange attributeChange = null;
        if (attributes != null) {
            attributeChange = AttributeParser.parse(attributes);
        }
        boolean done;
        synchronized (getCentralSphere()) {
            done = update(centralSphereEditor.getMetadataEditor(), titleLabelChange, phraseChangeMap, attributeChange);
            if (done) {
                saveChanges();
            }
        }
        if (done) {
            return done("_ done.multi.metadata");
        } else {
            return null;
        }
    }

    private boolean update(MetadataEditor metadataEditor, LabelChange titleLabelChange, Map<String, LabelChange> phraseChangeMap, AttributeChange attributeChange) {
        boolean done = false;
        if (metadataEditor.changeLabels(null, titleLabelChange)) {
            done = true;
        }
        for (Map.Entry<String, LabelChange> entry : phraseChangeMap.entrySet()) {
            if (metadataEditor.changeLabels(entry.getKey(), entry.getValue())) {
                done = true;
            }
        }
        if (attributeChange != null) {
            if (metadataEditor.changeAttributes(attributeChange)) {
                done = true;
            }
        }
        return done;
    }

}
