/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.central;

import fr.exemole.bdfserver.api.users.BdfUser;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralUserEditor;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.util.LinkedHashSet;
import java.util.Set;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class CopyFromFichothequeCommand extends AbstractCentralMultiCommand {

    public final static String COMMAND_NAME = "CopyFromFichotheque";
    public final static String LOGINS_PARAMNAME = "logins";

    public CopyFromFichothequeCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        Set<String> loginSet = getLoginSet();
        initCentralSphere();
        initBdfServer(true);
        initCentralEditors();
        synchronized (getCentralSphere()) {
            copy(loginSet);
            saveChanges();
        }
        return done("_ done.multi.copyfromfichotheque");
    }

    private void copy(Set<String> loginSet) {
        Sphere sphere = (Sphere) bdfServer.getFichotheque().getSubset(centralSphere.getSphereKey());
        for (Redacteur redacteur : sphere.getRedacteurList()) {
            String login = redacteur.getLogin();
            if ((loginSet.contains(login)) && (!centralSphere.hasCentralUser(login))) {
                createCentralUser(redacteur, login);
            }
        }
    }

    private void createCentralUser(Redacteur redacteur, String login) {
        CentralUserEditor centralUserEditor = centralSphereEditor.createCentralUser(login);
        centralUserEditor.setStatus(redacteur.getStatus());
        centralUserEditor.setPerson(redacteur.getPersonCore());
        centralUserEditor.setEmail(redacteur.getEmailCore());
        centralUserEditor.setEncryptedPassword(bdfServer.getPasswordManager().getEncryptedPassword(redacteur.getGlobalId()));
        BdfUser bdfUser = bdfServer.createBdfUser(redacteur);
        centralUserEditor.setLangContext(bdfUser.getWorkingLang(), bdfUser.getPrefs().getCustomFormatLocale(), bdfUser.getPrefs().getCustomLangPreference());
    }

    private Set<String> getLoginSet() throws ErrorMessageException {
        String[] logins = requestMap.getParameterTokens(LOGINS_PARAMNAME, true);
        if (logins == null) {
            throw BdfErrors.emptyMandatoryParameter(LOGINS_PARAMNAME);
        }
        Set<String> set = new LinkedHashSet<String>();
        for (String login : logins) {
            set.add(login);
        }
        return set;
    }

}
