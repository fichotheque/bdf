/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands;

import fr.exemole.bdfserver.multi.MultiUtils;
import fr.exemole.bdfserver.multi.api.Multi;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractMultiCommand {

    protected final Multi multi;
    protected final RequestMap requestMap;

    public AbstractMultiCommand(Multi multi, RequestMap requestMap) {
        this.multi = multi;
        this.requestMap = requestMap;
    }

    @Nullable
    public abstract CommandMessage doCommand() throws ErrorMessageException;

    public String getMandatory(String paramName) throws ErrorMessageException {
        return MultiUtils.getMandatory(requestMap, paramName, false);
    }

    public String getMandatory(String paramName, boolean allowEmpty) throws ErrorMessageException {
        return MultiUtils.getMandatory(requestMap, paramName, allowEmpty);
    }

    public String getFichothequeName(String paramName) throws ErrorMessageException {
        return MultiUtils.getFichothequeName(requestMap, paramName);
    }

    public CommandMessage done(String messageLocKey) {
        return LogUtils.done(messageLocKey);
    }

    public CommandMessage done(String messageLocKey, Object... values) {
        return LogUtils.done(messageLocKey, values);
    }

}
