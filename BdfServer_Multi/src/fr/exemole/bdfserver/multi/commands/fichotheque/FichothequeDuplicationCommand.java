/* BdfServer_Multi - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.fichotheque;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.commands.AbstractMultiCommand;
import fr.exemole.bdfserver.multi.commands.CreationParameters;
import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeDuplicationCommand extends AbstractMultiCommand {

    public final static String COMMAND_NAME = "FichothequeDuplication";
    public final static String NEWFICHOTHEQUE_PARAMNAME = "newfichotheque";
    public final static String SOURCE_PARAMNAME = "source";
    public final static String SUBSETS_PARAMNAME = "subsets";
    public final static String ITEMS_PARAMPREFIX = "items/";

    public FichothequeDuplicationCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        String name = run();
        return done("_ done.multi.fichothequecreation", name);
    }

    private String run() throws ErrorMessageException {
        String name = getFichothequeName(NEWFICHOTHEQUE_PARAMNAME);
        if (multi.containsFichotheque(name)) {
            throw new ErrorMessageException("_ error.existing.fichotheque", name);
        }
        multi.createFichotheque(getCreationParameters(name));
        return name;
    }

    private CreationParameters getCreationParameters(String name) throws ErrorMessageException {
        BdfServer sourceBdfServer = getSourceBdfServer();
        CreationSubsetEligibility creationSubsetEligibility = initCreationSubsetEligibility(sourceBdfServer);
        return CreationParameters.initDuplicate(name, sourceBdfServer, creationSubsetEligibility);
    }

    private BdfServer getSourceBdfServer() throws ErrorMessageException {
        String source = getMandatory(SOURCE_PARAMNAME);
        return multi.getBdfServer(source);
    }

    private CreationSubsetEligibility initCreationSubsetEligibility(BdfServer sourceBdfServer) {
        CreationSubsetEligibility creationSubsetEligibility = new CreationSubsetEligibility(sourceBdfServer.getFichotheque());
        String[] tokens = requestMap.getParameterTokens(SUBSETS_PARAMNAME, true);
        if (tokens == null) {
            return creationSubsetEligibility;
        }
        for (String token : tokens) {
            try {
                SubsetKey subsetKey = SubsetKey.parse(token);
                String itemsPolicy = checkItemsPolicy(requestMap.getParameter(ITEMS_PARAMPREFIX + subsetKey));
                creationSubsetEligibility.add(subsetKey, itemsPolicy);
            } catch (ParseException pe) {
            }
        }
        return creationSubsetEligibility;
    }

    private static String checkItemsPolicy(String value) {
        if (value == null) {
            return CreationSubsetEligibility.ITEMS_NONE;
        }
        switch (value) {
            case CreationSubsetEligibility.ITEMS_NONE:
            case CreationSubsetEligibility.ITEMS_ALL:
            case CreationSubsetEligibility.ITEMS_SELECTION:
                return value;
            default:
                return CreationSubsetEligibility.ITEMS_NONE;
        }
    }


}
