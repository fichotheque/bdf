/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.fichotheque;

import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.MultiEditor;
import fr.exemole.bdfserver.multi.api.MultiMetadata;
import fr.exemole.bdfserver.multi.api.MultiMetadataEditor;
import fr.exemole.bdfserver.multi.api.namespaces.FichothequeSpace;
import fr.exemole.bdfserver.multi.commands.AbstractMultiCommand;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import net.fichotheque.EditOrigin;
import net.fichotheque.utils.EditOriginUtils;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeBuilder;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeActionCommand extends AbstractMultiCommand {

    public final static String COMMAND_NAME = "FichothequeAction";
    public final static String NAME_PARAMNAME = "name";
    public final static String ACTION_PARAMNAME = "action";

    public FichothequeActionCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        String name = getFichothequeName(NAME_PARAMNAME);
        String action = getMandatory(ACTION_PARAMNAME);
        CommandMessage commandMessage;
        switch (action) {
            case "start":
                commandMessage = start(name);
                break;
            case "stop":
                commandMessage = stop(name);
                break;
            default:
                throw BdfErrors.unknownParameterValue(ACTION_PARAMNAME, action);
        }
        return commandMessage;
    }

    private CommandMessage start(String name) throws ErrorMessageException {
        Attribute currentAttribute = multi.getMultiMetadata().getAttributes().getAttribute(FichothequeSpace.INACTIVE_KEY);
        if (currentAttribute == null) {
            return done("_ info.multi.activefichotheque", name);
        }
        AttributeBuilder attributeBuilder = new AttributeBuilder(FichothequeSpace.INACTIVE_KEY);
        boolean done = false;
        for (String value : currentAttribute) {
            if (value.equals(name)) {
                done = true;
            } else {
                attributeBuilder.addValue(value);
            }
        }
        if (!done) {
            return done("_ info.multi.activefichotheque", name);
        }
        updateInactiveAttribute("start", attributeBuilder.toAttribute());
        return done("_ done.multi.action_start", name);
    }

    private CommandMessage stop(String name) throws ErrorMessageException {
        Attribute currentAttribute = multi.getMultiMetadata().getAttributes().getAttribute(FichothequeSpace.INACTIVE_KEY);
        AttributeBuilder attributeBuilder = new AttributeBuilder(FichothequeSpace.INACTIVE_KEY);
        boolean here = false;
        if (currentAttribute != null) {
            for (String value : currentAttribute) {
                if (value.equals(name)) {
                    here = true;
                    break;
                } else {
                    attributeBuilder.addValue(value);
                }
            }
        }
        if (here) {
            return done("_ info.multi.inactivefichotheque", name);
        }
        attributeBuilder.addValue(name);
        updateInactiveAttribute("stop", attributeBuilder.toAttribute());
        return done("_ done.multi.action_stop", name);
    }

    private void updateInactiveAttribute(String action, Attribute attribute) {
        EditOrigin editOrigin = EditOriginUtils.newEditOrigin("multi-admin/action-" + action);
        MultiMetadata metadata = multi.getMultiMetadata();
        synchronized (metadata) {
            MultiEditor multiEditor = multi.getMultiEditor(editOrigin);
            MultiMetadataEditor multiMetadataEditor = multiEditor.getMultiMetadataEditor();
            if (attribute == null) {
                multiMetadataEditor.removeAttribute(FichothequeSpace.INACTIVE_KEY);
            } else {
                multiMetadataEditor.putAttribute(attribute);
            }
            multiEditor.saveChanges();
        }
    }

}
