/* BdfServer_Multi - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.fichotheque;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.commands.AbstractMultiCommand;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeInitCommand extends AbstractMultiCommand {

    public final static String COMMAND_NAME = "FichothequeInit";
    public final static String NAME_PARAMNAME = "name";

    public FichothequeInitCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        String name = run();
        return done("_ done.multi.fichothequeinit", name);
    }

    private String run() throws ErrorMessageException {
        String name = getFichothequeName(NAME_PARAMNAME);
        BdfServer bdfServer = multi.getBdfServer(name);
        return name;
    }

}
