/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands;

import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.MultiEditor;
import fr.exemole.bdfserver.multi.api.MultiMetadata;
import fr.exemole.bdfserver.multi.api.MultiMetadataEditor;
import fr.exemole.bdfserver.tools.instruction.BdfCommandUtils;
import java.util.HashMap;
import java.util.Map;
import net.fichotheque.EditOrigin;
import net.fichotheque.utils.EditOriginUtils;
import net.mapeadores.util.attr.AttributeChange;
import net.mapeadores.util.attr.AttributeParser;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.LabelChange;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class MultiMetadataCommand extends AbstractMultiCommand {

    public final static String COMMAND_NAME = "MultiMetadata";
    public final static String TITLE_PARAMPREFIX = "title/";
    public final static String ATTRIBUTES_PARAMNAME = "attributes";
    public final static String WORKINGLANGS_PARAMNAME = "workinglangs";
    public final static String AUTHORITY = "authority";

    public MultiMetadataCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        Langs workingLangs = getWorkingLangs();
        String authority = getAuthority();
        LabelChange titleLabelChange = BdfCommandUtils.getLabelChange(requestMap, TITLE_PARAMPREFIX, true);
        Map<String, LabelChange> phraseChangeMap = new HashMap<String, LabelChange>();
        BdfCommandUtils.populatePhraseLabelChange(requestMap, phraseChangeMap);
        String attributes = requestMap.getParameter(ATTRIBUTES_PARAMNAME);
        AttributeChange attributeChange = null;
        if (attributes != null) {
            attributeChange = AttributeParser.parse(attributes);
        }
        EditOrigin editOrigin = EditOriginUtils.newEditOrigin("multi-admin/metadata");
        MultiMetadata metadata = multi.getMultiMetadata();
        boolean done;
        synchronized (metadata) {
            MultiEditor multiEditor = multi.getMultiEditor(editOrigin);
            MultiMetadataEditor multiMetadataEditor = multiEditor.getMultiMetadataEditor();
            done = update(multiMetadataEditor, titleLabelChange, phraseChangeMap, attributeChange);
            if (workingLangs != null) {
                if (multiMetadataEditor.setWorkingLangs(workingLangs)) {
                    done = true;
                }
            }
            if (authority != null) {
                if (multiMetadataEditor.setAuthority(authority)) {
                    done = true;
                }
            }
            if (done) {
                multiEditor.saveChanges();
            }
        }
        if (done) {
            return LogUtils.done("_ done.multi.metadata");
        } else {
            return null;
        }
    }

    private boolean update(MultiMetadataEditor metadataEditor, LabelChange titleLabelChange, Map<String, LabelChange> phraseChangeMap, AttributeChange attributeChange) {
        boolean done = false;
        if (metadataEditor.changeLabels(null, titleLabelChange)) {
            done = true;
        }
        for (Map.Entry<String, LabelChange> entry : phraseChangeMap.entrySet()) {
            if (metadataEditor.changeLabels(entry.getKey(), entry.getValue())) {
                done = true;
            }
        }
        if (attributeChange != null) {
            if (metadataEditor.changeAttributes(attributeChange)) {
                done = true;
            }
        }
        return done;
    }

    private String getAuthority() throws ErrorMessageException {
        String authority = requestMap.getParameter(AUTHORITY);
        if (authority != null) {
            authority = authority.trim();
            if (authority.isEmpty()) {
                throw new ErrorMessageException("_ error.empty.authority");
            } else if (!StringUtils.isAuthority(authority)) {
                throw new ErrorMessageException("_ error.wrong.authority", authority);
            }
        }
        return authority;
    }

    private Langs getWorkingLangs() throws ErrorMessageException {
        String workingLangsParam = requestMap.getParameter(WORKINGLANGS_PARAMNAME);
        if (workingLangsParam != null) {
            Langs workingLangs = LangsUtils.toCleanLangs(workingLangsParam);
            if (workingLangs.isEmpty()) {
                throw new ErrorMessageException("_ error.empty.workinglang");
            }
            return workingLangs;
        } else {
            return null;
        }
    }

}
