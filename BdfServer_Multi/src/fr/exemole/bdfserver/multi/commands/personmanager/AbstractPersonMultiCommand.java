/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.personmanager;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.commands.AbstractMultiCommand;
import java.text.ParseException;
import net.fichotheque.SubsetKey;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractPersonMultiCommand extends AbstractMultiCommand {

    public final static String SPHERE_PARAMNAME = "sphere";
    public final static String LOGIN_PARAMNAME = "login";
    public final static String COPYORIGIN_PARAMNAME = "copyorigin";
    protected SubsetKey sphereKey;
    protected String login;

    public AbstractPersonMultiCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    public void initPersonParameters() throws ErrorMessageException {
        if (!multi.isPersonManagementAllowed()) {
            throw new ErrorMessageException("_ error.unsupported.personmanagementnotallowed");
        }
        String sphereName = getMandatory(SPHERE_PARAMNAME);
        this.login = getMandatory(LOGIN_PARAMNAME);
        try {
            this.sphereKey = SubsetKey.parse(SubsetKey.CATEGORY_SPHERE, sphereName.trim());
        } catch (ParseException pe) {
            throw new ErrorMessageException("_ error.unknown.sphere", sphereName);
        }
    }

    public CopyOrigin getCopyOrigin() throws ErrorMessageException {
        String copyOrigin = getMandatory(COPYORIGIN_PARAMNAME);
        if (!multi.isInit(copyOrigin)) {
            throw new ErrorMessageException("_ error.unknown.parametervalue", COPYORIGIN_PARAMNAME, copyOrigin);
        }
        BdfServer bdfServer = multi.getBdfServer(copyOrigin);
        Sphere sphere = (Sphere) bdfServer.getFichotheque().getSubset(sphereKey);
        if (sphere != null) {
            Redacteur redacteur = sphere.getRedacteurByLogin(login);
            if (redacteur != null) {
                return new CopyOrigin(bdfServer, redacteur);
            } else {
                throw new ErrorMessageException("_ error.unknown.parametervalue", COPYORIGIN_PARAMNAME, copyOrigin);
            }
        }
        throw new ErrorMessageException("_ error.unknown.sphere", sphereKey.getSubsetName());
    }


    public static class CopyOrigin {

        private final BdfServer originBdfServer;
        private final Redacteur redacteur;
        private final String encryptedPassword;

        private CopyOrigin(BdfServer originBdfServer, Redacteur redacteur) {
            this.originBdfServer = originBdfServer;
            this.redacteur = redacteur;
            this.encryptedPassword = originBdfServer.getPasswordManager().getEncryptedPassword(redacteur.getGlobalId());
        }

        public String getOriginName() {
            return originBdfServer.getName();
        }

        public BdfServer getOriginBdfServer() {
            return originBdfServer;
        }

        public Redacteur getRedacteur() {
            return redacteur;
        }

        public String getEncryptedPassword() {
            return encryptedPassword;
        }

    }

}
