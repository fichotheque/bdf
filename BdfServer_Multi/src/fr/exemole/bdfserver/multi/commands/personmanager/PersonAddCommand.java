/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.commands.personmanager;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.EditSession;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.tools.instruction.BdfErrors;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.ExistingIdException;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;
import net.fichotheque.sphere.SphereEditor;
import net.fichotheque.utils.EditOriginUtils;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class PersonAddCommand extends AbstractPersonMultiCommand {

    public final static String COMMAND_NAME = "PersonAdd";
    public final static String TARGET_PARAMNAME = "target";
    public final static String FICHOTHEQUES_PARAMNAME = "fichotheques";
    public final static String ALL_PARAMVALUE = "all";
    public final static String SELECTION_PARAMVALUE = "selection";


    public PersonAddCommand(Multi multi, RequestMap requestMap) {
        super(multi, requestMap);
    }

    @Override
    public CommandMessage doCommand() throws ErrorMessageException {
        initPersonParameters();
        return runAdd();
    }

    private CommandMessage runAdd() throws ErrorMessageException {
        CopyOrigin copyOrigin = getCopyOrigin();
        List<BdfServer> bdfServerList = getBdfServerList();
        if (bdfServerList.isEmpty()) {
            return null;
        }
        for (BdfServer bdfServer : bdfServerList) {
            createRedacteur(bdfServer, copyOrigin);
        }
        return done("_ done.multi.personadd");
    }

    private void createRedacteur(BdfServer bdfServer, CopyOrigin copyOrigin) {
        Redacteur originRedacteur = copyOrigin.getRedacteur();
        synchronized (bdfServer) {
            try (EditSession editSession = bdfServer.initEditSession(EditOriginUtils.newEditOrigin(null, "_admin", "PersonAdd"))) {
                SphereEditor sphereEditor = editSession.getFichothequeEditor().getSphereEditor(sphereKey);
                try {
                    Redacteur newRedacteur = sphereEditor.createRedacteur(-1, login);
                    sphereEditor.setPerson(newRedacteur, originRedacteur.getPersonCore());
                    sphereEditor.setEmail(newRedacteur, originRedacteur.getEmailCore());
                    bdfServer.getPasswordManager().setEncryptedPassword(newRedacteur.getGlobalId(), copyOrigin.getEncryptedPassword());
                } catch (ExistingIdException | ParseException e) {
                    throw new ShouldNotOccurException("Test done before");
                }
            }
        }
    }

    private List<BdfServer> getBdfServerList() throws ErrorMessageException {
        String targetValue = requestMap.getParameter(TARGET_PARAMNAME);
        List<BdfServer> bdfServerList = new ArrayList<BdfServer>();
        if ((targetValue != null) && (targetValue.equals(SELECTION_PARAMVALUE))) {
            String[] fichotheques = requestMap.getParameterValues(FICHOTHEQUES_PARAMNAME);
            if (fichotheques == null) {
                throw BdfErrors.emptyMandatoryParameter(FICHOTHEQUES_PARAMNAME);
            }
            for (String name : fichotheques) {
                populateList(bdfServerList, name);
            }
        } else {
            for (String name : multi.getExistingNameSet()) {
                populateList(bdfServerList, name);
            }
        }
        return bdfServerList;
    }

    private void populateList(List<BdfServer> bdfServerList, String name) {
        BdfServer bdfServer = multi.getInitBdfServer(name);
        if (bdfServer != null) {
            Sphere sphere = (Sphere) bdfServer.getFichotheque().getSubset(sphereKey);
            if (sphere != null) {
                Redacteur existing = sphere.getRedacteurByLogin(login);
                if (existing == null) {
                    bdfServerList.add(bdfServer);
                }
            }
        }
    }


}
