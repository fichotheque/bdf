/* BdfServer_Multi - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class MultiJsonProducer implements JsonProducer {

    private final MessageLocalisation messageLocalisation;
    private final List<JsonProperty> jsonPropertyList = new ArrayList<JsonProperty>();
    private final boolean authentified;
    private CommandMessage commandMessage;

    public MultiJsonProducer(boolean authentified, MessageLocalisation messageLocalisation) {
        this.authentified = authentified;
        this.messageLocalisation = messageLocalisation;

    }

    public MultiJsonProducer setCommandMessage(CommandMessage commandMessage) {
        this.commandMessage = commandMessage;
        return this;
    }

    public MultiJsonProducer add(JsonProperty jsonProperty) {
        jsonPropertyList.add(jsonProperty);
        return this;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("authentified")
                    .value(authentified);
            for (JsonProperty jsonProperty : jsonPropertyList) {
                jsonProperty.write(jw);
            }
            if (commandMessage != null) {
                jw.key("commandMessage");
                CommonJson.object(jw, commandMessage, messageLocalisation);
            }
        }
        jw.endObject();
    }


}
