/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.admin;

import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.MultiMetadata;
import java.io.IOException;
import net.fichotheque.json.MetadataJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class MultiMetadataJsonProperty implements JsonProperty {

    private final Multi multi;

    public MultiMetadataJsonProperty(Multi multi) {
        this.multi = multi;
    }

    @Override
    public String getName() {
        return "metadata";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        MultiMetadata metadata = multi.getMultiMetadata();
        jw.object();
        {
            MetadataJson.properties(jw, metadata);
            jw.key("langConfiguration");
            jw.object();
            {
                jw.key("workingLangArray");
                jw.array();
                for (Lang lang : metadata.getWorkingLangs()) {
                    jw.value(lang);
                }
                jw.endArray();
                jw.key("supplementaryLangArray");
                jw.array();
                jw.endArray();
            }
            jw.endObject();
            jw.key("authority")
                    .value(metadata.getAuthority());
        }
        jw.endObject();
    }

}
