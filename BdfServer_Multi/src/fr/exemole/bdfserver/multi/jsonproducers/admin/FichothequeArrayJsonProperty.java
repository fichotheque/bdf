/* BdfServer_Multi - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.admin;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.json.SubsetTreeJson;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.json.FichothequeJson;
import java.io.IOException;
import java.util.Set;
import net.fichotheque.SubsetKey;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeArrayJsonProperty implements JsonProperty {

    private final Multi multi;
    private final MessageLocalisation messageLocalisation;
    private final Lang lang;

    public FichothequeArrayJsonProperty(Multi multi, MessageLocalisation messageLocalisation, Lang lang) {
        this.multi = multi;
        this.messageLocalisation = messageLocalisation;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "fichothequeArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        Set<String> set = multi.getExistingNameSet();
        jw.array();
        for (String name : set) {
            String state = multi.getState(name);
            BdfServer bdfServer = multi.getInitBdfServer(name);
            jw.object();
            {
                jw.key("name")
                        .value(name);
                jw.key("state")
                        .value(state);
                jw.key("init")
                        .value(state.equals(Multi.ACTIVE_STATE));
                if (bdfServer != null) {
                    fichothequeProperties(jw, bdfServer);
                } else {
                    FichothequeJson.phrasesProperties(jw, multi.getBdfServerDirs(name), lang);
                }
            }
            jw.endObject();
        }
        jw.endArray();
    }

    private void fichothequeProperties(JSONWriter jw, BdfServer bdfServer) throws IOException {
        FichothequeJson.phrasesProperties(jw, bdfServer, lang);
        SubsetTreeJson.Parameters parameters = new SubsetTreeJson.Parameters(bdfServer, lang, messageLocalisation);
        SubsetTreeJson.properties(jw, parameters, SubsetKey.CATEGORY_CORPUS);
        SubsetTreeJson.properties(jw, parameters, SubsetKey.CATEGORY_THESAURUS);
        SubsetTreeJson.properties(jw, parameters, SubsetKey.CATEGORY_SPHERE);
        SubsetTreeJson.properties(jw, parameters, SubsetKey.CATEGORY_ALBUM);
        SubsetTreeJson.properties(jw, parameters, SubsetKey.CATEGORY_ADDENDA);
    }

}
