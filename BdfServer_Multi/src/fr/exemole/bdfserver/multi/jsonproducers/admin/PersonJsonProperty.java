/* BdfServer_Multi - Copyright (c) 2021-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.admin;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.tools.synthesis.RedacteurSynthesis;
import fr.exemole.bdfserver.tools.synthesis.SphereSynthesis;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public class PersonJsonProperty implements JsonProperty {

    private final Multi multi;
    private final SphereSynthesis sphereSynthesis;
    private final RedacteurSynthesis redacteurSynthesis;
    private final Lang lang;

    public PersonJsonProperty(Multi multi, SphereSynthesis sphereSynthesis, RedacteurSynthesis redacteurSynthesis, Lang lang) {
        this.multi = multi;
        this.sphereSynthesis = sphereSynthesis;
        this.redacteurSynthesis = redacteurSynthesis;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "person";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        String[] fichothequeNameArray = sphereSynthesis.getFichothequeNameArray();
        jw.object();
        jw.key("sphereName")
                .value(sphereSynthesis.getSphereName());
        jw.key("login")
                .value(redacteurSynthesis.getLogin());
        jw.key("completeName")
                .value(redacteurSynthesis.getCompleteName());
        jw.key("email")
                .value(redacteurSynthesis.getEmail());
        jw.key("statusArray");
        jw.array();
        for (String fichothequeName : fichothequeNameArray) {
            RedacteurSynthesis.FichothequeStatus status = redacteurSynthesis.getFichothequeStatus(fichothequeName);
            if (status == null) {
                continue;
            }
            jw.object();
            {
                jw.key("fichothequeName")
                        .value(fichothequeName);
                jw.key("fichothequeTitle")
                        .value(getFichothequeTitle(fichothequeName));
                jw.key("level")
                        .value(getLevelStringCell(status.getLevel()));
                if (status.hasSpecificName()) {
                    jw.key("specificName")
                            .value(status.getSpecificName());
                }
                if (status.hasSpecificEmail()) {
                    jw.key("specificEmail")
                            .value(status.getSpecificEmail());
                }
            }
            jw.endObject();
        }
        jw.endArray();
        jw.endObject();
    }

    private String getFichothequeTitle(String fichothequeName) {
        try {
            BdfServer bdfServer = multi.getBdfServer(fichothequeName);
            return bdfServer.getFichotheque().getFichothequeMetadata().getTitleLabels().seekLabelString(lang, "");
        } catch (ErrorMessageException eme) {
            return "";
        }
    }

    private static String getLevelStringCell(String level) {
        switch (level) {
            case RedacteurSynthesis.A_ADMIN_LEVEL:
                return "admin";
            case RedacteurSynthesis.C_READONLY_LEVEL:
                return "readonly";
            case RedacteurSynthesis.D_INACTIVE_LEVEL:
                return "inactive";
            default:
                return "user";
        }
    }

}
