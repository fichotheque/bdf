/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.central;

import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.multi.tools.PresenceInfo;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class UserStateJsonProperty implements JsonProperty {

    private final Multi multi;
    private final CentralUser centralUser;
    private final Lang lang;

    public UserStateJsonProperty(Multi multi, CentralUser centralUser, Lang lang) {
        this.multi = multi;
        this.centralUser = centralUser;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "userstate";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        List<PresenceInfo> presenceInfoList = PresenceInfo.scan(multi, centralUser, true);
        jw.object();
        {
            jw.key("statusArray");
            jw.array();
            for (PresenceInfo presenceInfo : presenceInfoList) {
                write(jw, presenceInfo);
            }
            jw.endArray();
        }
        jw.endObject();
    }

    private void write(JSONWriter jw, PresenceInfo presenceInfo) throws IOException {
        jw.object();
        {
            jw.key("fichothequeName")
                    .value(presenceInfo.getName());
            jw.key("fichothequeTitle")
                    .value(presenceInfo.getFichothequeTitle(lang));
            jw.key("fichothequeInit")
                    .value(presenceInfo.isInit());
            jw.key("level")
                    .value(presenceInfo.getLevel());
        }
        jw.endObject();
    }

}
