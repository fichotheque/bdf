/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.central;

import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.multi.json.CentralUserJson;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class CentralUserJsonProperty implements JsonProperty {

    private final CentralUser centralUser;

    public CentralUserJsonProperty(CentralUser centralUser) {
        this.centralUser = centralUser;
    }

    @Override
    public String getName() {
        return "user";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        CentralUserJson.allProperties(jw, centralUser);
        jw.endObject();
    }

}
