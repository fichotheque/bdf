/* BdfServer_Multi - Copyright (c) 2022-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.central;

import fr.exemole.bdfserver.multi.json.FichothequeJson;
import fr.exemole.bdfserver.multi.tools.PresenceInfo;
import java.io.IOException;
import java.util.Collection;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class PresenceJsonProperty implements JsonProperty {

    private final Collection<PresenceInfo> presences;
    private final Lang lang;

    public PresenceJsonProperty(Collection<PresenceInfo> presences, Lang lang) {
        this.presences = presences;
        this.lang = lang;
    }

    @Override
    public String getName() {
        return "presenceMap";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.object();
        for (PresenceInfo presenceInfo : presences) {
            jw.key(presenceInfo.getName());
            jw.object();
            {
                jw.key("name")
                        .value(presenceInfo.getName());
                jw.key("init")
                        .value(presenceInfo.isInit());
                jw.key("level")
                        .value(presenceInfo.getLevel());
                FichothequeJson.phrasesProperties(jw, presenceInfo, lang);
            }
            jw.endObject();
        }
        jw.endObject();
    }

}
