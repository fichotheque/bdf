/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.central;

import fr.exemole.bdfserver.multi.api.central.CentralSphere;
import java.io.IOException;
import net.fichotheque.Metadata;
import net.fichotheque.json.MetadataJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class CentralSphereMetadataJsonProperty implements JsonProperty {

    private final CentralSphere centralSphere;

    public CentralSphereMetadataJsonProperty(CentralSphere centralSphere) {
        this.centralSphere = centralSphere;
    }

    @Override
    public String getName() {
        return "metadata";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        Metadata metadata = centralSphere.getMetadata();
        jw.object();
        {
            MetadataJson.properties(jw, metadata);
        }
        jw.endObject();
    }

}
