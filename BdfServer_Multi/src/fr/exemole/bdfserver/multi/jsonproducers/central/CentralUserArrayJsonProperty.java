/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.jsonproducers.central;

import fr.exemole.bdfserver.multi.api.central.CentralSphere;
import fr.exemole.bdfserver.multi.json.CentralUserJson;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProperty;


/**
 *
 * @author Vincent Calame
 */
public class CentralUserArrayJsonProperty implements JsonProperty {

    private final CentralSphere centralSphere;

    public CentralUserArrayJsonProperty(CentralSphere centralSphere) {
        this.centralSphere = centralSphere;
    }

    @Override
    public String getName() {
        return "userArray";
    }

    @Override
    public void writeValue(JSONWriter jw) throws IOException {
        jw.array();
        for (String login : centralSphere.getLoginSet()) {
            jw.object();
            CentralUserJson.coreProperties(jw, centralSphere.getCentralUser(login));
            jw.endObject();
        }
        jw.endArray();
    }

}
