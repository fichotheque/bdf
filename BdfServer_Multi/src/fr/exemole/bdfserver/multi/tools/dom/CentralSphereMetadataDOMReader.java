/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.tools.dom;

import java.util.function.Consumer;
import net.fichotheque.MetadataEditor;
import net.fichotheque.tools.dom.AbstractMetadataDOMReader;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class CentralSphereMetadataDOMReader extends AbstractMetadataDOMReader {

    public CentralSphereMetadataDOMReader(MetadataEditor metadataEditor, MessageHandler messageHandler) {
        super(metadataEditor, messageHandler);
    }

    public void read(Element element) {
        AttributesBuilder attributesBuilder = new AttributesBuilder();
        RootConsumer rootConsumer = new RootConsumer(attributesBuilder, "/" + element.getTagName());
        DOMUtils.readChildren(element, rootConsumer);
        flush(attributesBuilder);
    }


    private class RootConsumer implements Consumer<Element> {

        private final AttributesBuilder attributesBuilder;
        private final String parentXpath;

        private RootConsumer(AttributesBuilder attributesBuilder, String parentXpath) {
            this.attributesBuilder = attributesBuilder;
            this.parentXpath = parentXpath;
        }

        @Override
        public void accept(Element element) {
            if (readCommonElement(element, attributesBuilder, parentXpath)) {
            }
        }

    }

}
