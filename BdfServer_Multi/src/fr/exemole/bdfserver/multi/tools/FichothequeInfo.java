/* BdfServer_Multi - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.tools;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import net.fichotheque.utils.FichothequeMetadataUtils;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class FichothequeInfo {

    private final String name;
    private final BdfServer bdfServer;
    private final BdfServerDirs bdfServerDirs;

    public FichothequeInfo(String name, BdfServer bdfServer) {
        this.name = name;
        this.bdfServer = bdfServer;
        this.bdfServerDirs = null;
    }

    public FichothequeInfo(String name, BdfServerDirs bdfServerDirs) {
        this.name = name;
        this.bdfServer = null;
        this.bdfServerDirs = bdfServerDirs;
    }

    public String getName() {
        return name;
    }

    public BdfServer getBdfServer() {
        return bdfServer;
    }

    public BdfServerDirs getBdfServerDirs() {
        return bdfServerDirs;
    }

    public boolean isInit() {
        return (bdfServer != null);
    }

    public String getFichothequeTitle(Lang lang) {
        if (bdfServer != null) {
            return FichothequeMetadataUtils.getTitle(bdfServer.getFichotheque(), lang);
        } else {
            return "";
        }
    }

}
