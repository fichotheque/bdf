/* BdfServer_Multi - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.bdfserver.multi.tools;

import fr.exemole.bdfserver.api.BdfServer;
import fr.exemole.bdfserver.api.managers.PermissionManager;
import fr.exemole.bdfserver.conf.BdfServerDirs;
import fr.exemole.bdfserver.multi.api.Multi;
import fr.exemole.bdfserver.multi.api.central.CentralUser;
import fr.exemole.bdfserver.storage.directory.tools.SphereChecker;
import java.util.ArrayList;
import java.util.List;
import net.fichotheque.FichothequeConstants;
import net.fichotheque.sphere.LoginKey;
import net.fichotheque.sphere.Redacteur;
import net.fichotheque.sphere.Sphere;


/**
 *
 * @author Vincent Calame
 */
public class PresenceInfo extends FichothequeInfo {

    public final static String NO_SPHERE = "nosphere";
    public final static String NO_USER = "nouser";
    public final static String HERE = "here";
    public final static String INACTIVE = "inactive";
    public final static String READONLY = "readonly";
    public final static String USER = "user";
    public final static String ADMIN = "admin";
    private final String level;
    private final Redacteur redacteur;

    public PresenceInfo(String name, BdfServer bdfServer, String level, Redacteur redacteur) {
        super(name, bdfServer);
        this.level = level;
        this.redacteur = redacteur;
    }

    public PresenceInfo(String name, BdfServerDirs bdfServerDirs, String level) {
        super(name, bdfServerDirs);
        this.level = level;
        this.redacteur = null;
    }

    public String getLevel() {
        return level;
    }

    public Redacteur getRedacteur() {
        return redacteur;
    }

    public static List<PresenceInfo> scan(Multi multi, CentralUser centralUser, boolean all) {
        List<PresenceInfo> list = new ArrayList<PresenceInfo>();
        LoginKey loginKey = centralUser.getLoginKey();
        for (String name : multi.getExistingNameSet()) {
            BdfServer bdfServer = multi.getInitBdfServer(name);
            PresenceInfo presenceInfo;
            if (bdfServer != null) {
                presenceInfo = getPresenceInfo(name, bdfServer, loginKey, all);
            } else {
                presenceInfo = getPresenceInfo(name, multi.getBdfServerDirs(name), loginKey, all);
            }
            if (presenceInfo != null) {
                list.add(presenceInfo);
            }
        }
        return list;
    }

    private static PresenceInfo getPresenceInfo(String name, BdfServer bdfServer, LoginKey loginKey, boolean all) {
        String level = null;
        Sphere sphere = (Sphere) bdfServer.getFichotheque().getSubset(loginKey.getSphereKey());
        Redacteur redacteur = null;
        if (sphere != null) {
            redacteur = sphere.getRedacteurByLogin(loginKey.getLogin());
            if (redacteur == null) {
                level = NO_USER;
            }
        } else {
            level = NO_SPHERE;
        }
        if (redacteur != null) {
            switch (redacteur.getStatus()) {
                case FichothequeConstants.INACTIVE_STATUS:
                    level = INACTIVE;
                    break;
                case FichothequeConstants.READONLY_STATUS:
                    level = READONLY;
                    break;
                default:
                    PermissionManager permissionManager = bdfServer.getPermissionManager();
                    if (permissionManager.isAdmin(redacteur)) {
                        level = ADMIN;
                    } else {
                        level = USER;
                    }
            }
        }
        if (!all) {
            switch (level) {
                case NO_USER:
                case INACTIVE:
                    return null;
            }
        }
        return new PresenceInfo(name, bdfServer, level, redacteur);
    }

    private static PresenceInfo getPresenceInfo(String name, BdfServerDirs bdfServerDirs, LoginKey loginKey, boolean all) {
        String level;
        if (!SphereChecker.containsSphere(loginKey.getSphereKey(), bdfServerDirs)) {
            level = NO_SPHERE;
        } else if (SphereChecker.containsRedacteur(loginKey, bdfServerDirs)) {
            level = HERE;
        } else {
            level = NO_USER;
        }
        if (!all) {
            if (!level.equals(HERE)) {
                return null;
            }
        }
        return new PresenceInfo(name, bdfServerDirs, level);
    }

}
